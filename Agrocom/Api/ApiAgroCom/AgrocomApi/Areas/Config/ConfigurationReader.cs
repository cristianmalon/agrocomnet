﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace AgrocomApi.Areas.Config
{
    public class ConfigurationReader
    {
        //public static IConfigurationRoot ConectWithFileConfig(string fileJsonAppSet)
        //{
        //    var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
        //                   .AddJsonFile(fileJsonAppSet, false).Build();
        //    return configuration;
        //}

        public static string GetKeyValueAppsetting(string appjsonfile, string section, string key)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(appjsonfile, false).Build();
            var key_value = configuration[section + ":" + key];
            return key_value;
        }
    }
}
