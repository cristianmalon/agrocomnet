﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrocomApi.Areas.Config
{
    public struct FileJson
    {
        public const string Agrocom = "appsettings.json";
    }

    public struct SettingConexion
    {
        public const string Agrocom_Section_ConexionBd = "connectionStrings";
        public const string Agrocom_Key_StringConexion = "ConexBdAgroCom";
        public const string Netsuite_Section_Appsetting = "Netsuite";
        public const string Netsuite_Key_Appsetting = "appsetting_name";
    }
}
