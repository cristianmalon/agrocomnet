﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using System.Collections.Generic;
using AgrocomApi.Mapper;
using AgrocomApi.Exceptions;
using Agrocom.BusinessCore.BL.Interface;
using Agrocom.BusinessCore.BL.Implementation;
using Microsoft.Extensions.Configuration;
using Common.Notification;

namespace AgrocomApi.Areas.Home.Controllers
{
    [Route("api/home/notificacion")]
    [ApiController]
    public class NotificationController : Controller
    {
        private readonly IConfiguration _configuration;
        private NotificationLogic _notificationLogic;

        public NotificationController(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _notificationLogic = new NotificationLogic(_configuration);
        }

        /// <summary>
        /// Generic method to list notifications
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListNotificationsSplash")]
        [ProducesResponseType(200, Type = typeof(List<ENotificationResponse>))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListNotifications([Required][FromHeader] string TypeUsr, [Required][FromHeader] string CropId)
        {
            if (string.IsNullOrEmpty(TypeUsr) || string.IsNullOrEmpty(CropId)) return Unauthorized(HttpStatusCode.BadRequest);
            List<ENotification> lstNotification;
            if (!_notificationLogic.GetNotificationsSplash(TypeUsr, CropId, out lstNotification))
            {
                return StatusCode((int)HttpStatusCode.NoContent);
            }
            var lstnotificationresponse = LogicEntityMapper.Mapper.Map<List<ENotificationResponse>>(lstNotification);

            return StatusCode((int)HttpStatusCode.OK, lstnotificationresponse);
        }
    }
}
