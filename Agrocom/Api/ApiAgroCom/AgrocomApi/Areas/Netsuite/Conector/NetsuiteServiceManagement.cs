﻿using Microsoft.Extensions.Configuration;
using System.IO;
using AgrocomApi.Areas.Netsuite.Constants;
using AgrocomApi.Areas.Config;
using System.Threading;
using AgrocomApi.Util;

namespace AgrocomApi.Areas.Netsuite.Conector
{
    public class NetsuiteServiceManagement
    {
        #region "variables"
        #endregion

        #region "metodos"
        public static string GetEndPointResponse(HttpMethod httpMethod, string urlEndPoint,
            CancellationToken cancellationToken = default, string jsonBody = "", int time_out = -1)
        {
            var netsuiteAppsetting = ConfigurationReader.GetKeyValueAppsetting(FileJson.Agrocom, SettingConexion.Netsuite_Section_Appsetting, SettingConexion.Netsuite_Key_Appsetting);
            var url = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, urlEndPoint);
            var consumer_id = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, SettingEndPoint.NetSuite_Key_Consumer_id);
            var consumer_secret = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, SettingEndPoint.NetSuite_Key_Consumer_secret);
            var token_id = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, SettingEndPoint.NetSuite_Key_Token_id);
            var token_secret = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, SettingEndPoint.NetSuite_Key_Token_secret);
            var realm = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, SettingEndPoint.NetSuite_Key_Realm);

            var sResponse = string.Empty;
            switch (httpMethod)
            {
                case (HttpMethod)0: sResponse = Kiss.Core.Business.NetSuite.GetDataNetsuite(url,
                    consumer_id, consumer_secret, token_id, token_secret, realm, time_out); break;
                case (HttpMethod)2: 
                    var sResponseAsync = Kiss.Core.Business.NetSuite.PutDataNetsuiteAsync(url,
                    consumer_id, consumer_secret, token_id, token_secret, realm, cancellationToken, jsonBody, time_out);
                    sResponse = sResponseAsync.Result;
                    break;
                default: break;
            }

            return sResponse;
        }

        public static bool CheckForInternetConnection()
        {
            var netsuiteAppsetting = ConfigurationReader.GetKeyValueAppsetting(FileJson.Agrocom, SettingConexion.Netsuite_Section_Appsetting, SettingConexion.Netsuite_Key_Appsetting);
            var timeOutMilisecond = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, ConstantsVerifyConnection.TimeOutMilisecond);
            var url = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, ConstantsVerifyConnection.Url);

            return UVerifyConnection.CheckForInternetConnection(int.Parse(timeOutMilisecond), url);
        }

        public static int RequestTimeout()
        {
            var netsuiteAppsetting = ConfigurationReader.GetKeyValueAppsetting(FileJson.Agrocom, SettingConexion.Netsuite_Section_Appsetting, SettingConexion.Netsuite_Key_Appsetting);
            var timeOutMilisecond = ConfigurationReader.GetKeyValueAppsetting(netsuiteAppsetting, SettingEndPoint.NetSuite_Section_Uri, ConstantsRequestTimeout.Valor01);
            
            return int.Parse(timeOutMilisecond);
        }
        #endregion

    }
}
