﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrocomApi.Areas.Netsuite.Constants
{
    public static class ConstantsError
    {
        public const string ERROR_SIN_CONTENIDO_CODIGO = "00001";
        public const string ERROR_SIN_CONTENIDO_MENSAJE = "END POINT WITHOUT TOKEN";

        public const string ERROR_MALA_PETICION_CODIGO = "00002";
        public const string ERROR_MALA_PETICION_MENSAJE = "REQUEST COULD NOT BE UNDERSTOOD";

        public const string ERROR_GENERICO_CODIGO = "00003";
        public const string ERROR_GENERICO_MENSAJE = "GENERIC ERROR HAS OCCURRED";

        public const string ERROR_EXCEPCION_CODIGO = "00004";
        public const string ERROR_EXCEPCION_MENSAJE = "ERRORS THAT OCCUR DURING EXECUTION";

        public const string ERROR_SIN_CONEXION_INTERNET_CODIGO = "00005";
        public const string ERROR_SIN_CONEXION_INTERNET_MENSAJE = "WITHOUT INTERNET CONNECTION";

        public const string ERROR_TIEMPO_FUERA_CODIGO = "00006";
        public const string ERROR_TIEMPO_FUERA_MENSAJE = "TIME OUT";

        public const string ERROR_NO_HUBO_RESPUESTA_CODIGO = "00007";
        public const string ERROR_NO_HUBO_RESPUESTA_MENSAJE = "NO RESPONSE RETURNED";

        public const string ERROR_DESCONOCIDO_CODIGO = "00008";
        public const string ERROR_DESCONOCIDO_MENSAJE = "UNKNOWN ERROR";
    }

    public enum HttpMethod
    {
        GET,
        POST,
        PUT
    }

    public struct SettingEndPoint
    {
        public const string NetSuite_Section_Uri = "UriNetSuit";
        public const string NetSuite_Key_Consumer_id = "consumer_id";
        public const string NetSuite_Key_Consumer_secret = "consumer_secret";
        public const string NetSuite_Key_Token_id = "token_id";
        public const string NetSuite_Key_Token_secret = "token_secret";
        public const string NetSuite_Key_Realm = "realm";
        public const string NetSuite_Key_Product_consulta = "Url01";
        public const string NetSuite_Key_Profit_center_consulta = "Url02";
        public const string NetSuite_Key_Location_consulta = "Url03";
        public const string NetSuite_Key_Grower_consulta = "Url04";
        public const string NetSuite_Key_Client_registro = "Url05";
        public const string NetSuite_Key_Client_consulta = "Url06";
        public const string NetSuite_Key_Commercial_plan_registro = "Url07";
        public const string NetSuite_Key_Sale_order_registro = "Url08";
        public const string NetSuite_Key_Instruccion_embarque_registro = "Url09";
        public const string NetSuite_Key_Logistic_Operator_consulta = "Url10";
    }

    public static class ConstantsVerifyConnection{
        public const string TimeOutMilisecond = "time_out_milisecond";
        public const string Url = "verify_Conexion_url";
    }

    public static class ConstantsRequestTimeout
    {
        public const string Valor01 = "request_timeout_valor01";
        public const string Valor02 = "request_timeout_valor02";
        public const string Valor03 = "request_timeout_valor03";        
    }
}
