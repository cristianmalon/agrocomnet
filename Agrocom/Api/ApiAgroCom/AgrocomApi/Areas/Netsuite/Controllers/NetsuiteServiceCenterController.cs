﻿using System;
using System.Net;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Common.Entities;
using Agrocom.Util;
using AgrocomApi.Context;
using AgrocomApi.Areas.Netsuite.Constants;
using AgrocomApi.Areas.Netsuite.Conector;
using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.BL.Implementation;
using Agrocom.Netsuite.BE.Master;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Agrocom.Netsuite.BE.Mixed;
using AgrocomApi.Exceptions;
using System.Linq;
using AgrocomApi.Areas.Netsuite.Models;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using AgrocomApi.Util;

namespace AgrocomApi.Areas.Netsuite.Controllers
{
    [Route("api/netsuite/netsuitservicecenter")]
    [ApiController]
    public class NetsuiteServiceCenterController : ControllerBase
    {
        #region "variables"
        private IProductLogic _productLogic;
        private IProfitCenterLogic _profitCenterLogic;
        private ILocationLogic _locationLogic;
        private IGrowerLogic _growerLogic;
        private IClientLogic _clientLogic;
        private ICommercialPlanLogic _commercialPlanLogic;
        private ISaleOrderLogic _saleOrderLogic;
        private IInstruccionEmbarqueLogic _instruccionEmbarqueLogic;
        private ILogisticOperatorLogic _logisticOperatorLogic;
        private readonly ApplicationDbContext context;
        private readonly IConfiguration _configuration;
        #endregion

        #region "constructor"
        public NetsuiteServiceCenterController(ApplicationDbContext context, IConfiguration configuration)
        {
            this.context = context;
            _configuration = configuration;
            _productLogic = new ProductLogic(_configuration);
            _profitCenterLogic = new ProfitCenterLogic(_configuration);
            _locationLogic = new LocationLogic(_configuration);
            _growerLogic = new GrowerLogic(_configuration);
            _clientLogic = new ClientLogic(_configuration);
            _commercialPlanLogic = new CommercialPlanLogic(_configuration);
            _saleOrderLogic = new SaleOrderLogic(_configuration);
            _instruccionEmbarqueLogic = new InstruccionEmbarqueLogic(_configuration);
            _logisticOperatorLogic = new LogisticOperatorLogic(_configuration);
        }
        #endregion

        #region "metodos"
        #region "apis"

        /// <summary>
        /// Method to get Product
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetMasterProduct")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult ListMasterProduct()
        {
            var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.GET, SettingEndPoint.NetSuite_Key_Product_consulta);
            var serializedresult = JObject.Parse(sResponse);

            if (!serializedresult.HasValues)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONTENIDO_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONTENIDO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.NoContent, objErrorAnswer);
            }
            var jsonerror = serializedresult["error"];
            if (!string.IsNullOrEmpty(jsonerror.ToString()))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var jsonarray = (JArray)serializedresult["response"];
            if (jsonarray.Count == 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var lstProduct = new List<EProduct>();
            var lstProductSubsidiary = new List<EProductSubsidiary>();
            EProduct objProduct = null;
            EProductSubsidiary objProductSubsidiary = null;
            JObject aItem = null;
            JArray aSubsidiary = null;
            for (int i = 0; i < jsonarray.Count; i++)
            {
                aItem = (JObject)jsonarray[i];
                objProduct = new EProduct()
                {
                    internalid = (int)aItem["internalid"],
                    type = (string)aItem["type"],
                    item = (string)aItem["item"],
                    displayname = (string)aItem["displayname"],
                    description = (string)aItem["description"],
                    color1 = (string)aItem["color1"],
                    calibre1 = (string)aItem["calibre1"],
                    categoria1 = (string)aItem["categoria1"],
                    marca_comercial = (string)aItem["marca_comercial1"],
                    comprador = (string)aItem["comprador"],
                    //color = (string)aItem["color"],
                    //calibre = (string)aItem["calibre"],                        
                    //categoria = (string)aItem["categoria"],
                    familia = (string)aItem["familia"],
                    grupo = (string)aItem["grupo"],
                    parent = (string)aItem["parent"],
                    id_subsidiary = (Convert.IsDBNull(aItem["id_subsidiary"])) ? string.Empty : aItem["id_subsidiary"].ToString(),
                    subsidiary = (Convert.IsDBNull(aItem["subsidiary"])) ? string.Empty : aItem["subsidiary"].ToString(),
                    codigoantiguo = (string)aItem["codigoantiguo"],
                    familianombre = (string)aItem["familianombre"],
                    gruponombre = (string)aItem["gruponombre"],
                };
                aSubsidiary = (JArray)aItem["subsdiaries"];
                for (int j = 0; j < aSubsidiary.Count; j++)
                {
                    aItem = (JObject)aSubsidiary[j];
                    objProductSubsidiary = new EProductSubsidiary()
                    {
                        internalid = objProduct.internalid,
                        id_subsidiary = (int)aItem["id"],
                        subsidiary = (string)aItem["name"],
                    };
                    lstProductSubsidiary.Add(objProductSubsidiary);
                }

                lstProduct.Add(objProduct);
            }

            if (!_productLogic.Insertar(UConvert.ToDataTable(lstProduct), UConvert.ToDataTable(lstProductSubsidiary)))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_EXCEPCION_CODIGO,
                        mensaje = ConstantsError.ERROR_EXCEPCION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }

            return StatusCode((int)HttpStatusCode.OK);

        }

        /// <summary>
        /// Method to get Profit center
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetMasterProfitCenter")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult ListMasterProfitCenter()
        {
            var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.GET, SettingEndPoint.NetSuite_Key_Profit_center_consulta);
            var serializedresult = JObject.Parse(sResponse);

            if (!serializedresult.HasValues)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONTENIDO_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONTENIDO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.NoContent, objErrorAnswer);
            }

            var jsonerror = serializedresult["error"];
            if (!string.IsNullOrEmpty(jsonerror.ToString()))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var jsonarray = (JArray)serializedresult["response"];
            if (jsonarray.Count == 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }

            var lstProfitCenter = new List<EProfitCenter>();
            EProfitCenter objProfitCenter = null;
            JObject aItem = null;
            for (int i = 0; i < jsonarray.Count; i++)
            {
                aItem = (JObject)jsonarray[i];
                objProfitCenter = new EProfitCenter()
                {
                    internalid = (int)aItem["internalid"],
                    name = (string)aItem["name"],
                    inactive = (string)aItem["inactive"],
                    subsidiary = (string)aItem["subsidiary"],
                    id_subsidiary = (string)aItem["id_subsidiary"],
                };
                lstProfitCenter.Add(objProfitCenter);
            }
            if (!_profitCenterLogic.Insertar(UConvert.ToDataTable(lstProfitCenter)))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }
            return StatusCode((int)HttpStatusCode.OK);
        }

        /// <summary>
        /// Method to get Location
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">Bad Request - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetMasterLocation")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult ListMasterLocation()
        {
            var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.GET, SettingEndPoint.NetSuite_Key_Location_consulta);
            var serializedresult = JObject.Parse(sResponse);

            if (!serializedresult.HasValues)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONTENIDO_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONTENIDO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.NoContent, objErrorAnswer);
            }

            var jsonerror = serializedresult["error"];
            if (!string.IsNullOrEmpty(jsonerror.ToString()))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var jsonarray = (JArray)serializedresult["response"];
            if (jsonarray.Count == 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }

            var lstLocation = new List<ELocation>();
            ELocation objLocation = null;
            JObject aItem = null;
            for (int i = 0; i < jsonarray.Count; i++)
            {
                aItem = (JObject)jsonarray[i];
                objLocation = new ELocation()
                {
                    internalid = (int)aItem["internalid"],
                    name = (string)aItem["name"],
                    inactive = (string)aItem["inactive"],
                    subsidiary = (string)aItem["subsidiary"],
                    id_subsidiary = (string)aItem["id_subsidiary"],
                };
                lstLocation.Add(objLocation);
            }
            if (!_locationLogic.Insertar(UConvert.ToDataTable(lstLocation)))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            return StatusCode((int)HttpStatusCode.OK);

        }

        /// <summary>
        /// Method to get Grower
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetMasterGrower")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult ListMasterGrower()
        {
            var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.GET, SettingEndPoint.NetSuite_Key_Grower_consulta);
            var serializedresult = JObject.Parse(sResponse);

            if (!serializedresult.HasValues)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONTENIDO_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONTENIDO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.NoContent, objErrorAnswer);
            }

            var jsonerror = serializedresult["error"];
            if (!string.IsNullOrEmpty(jsonerror.ToString()))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var jsonarray = (JArray)serializedresult["response"];
            if (jsonarray.Count == 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }

            var lstGrower = new List<EGrower>();
            EGrower objGrower = null;
            JObject aItem = null;
            for (int i = 0; i < jsonarray.Count; i++)
            {
                aItem = (JObject)jsonarray[i];
                objGrower = new EGrower()
                {
                    internalid = (string)aItem["internalid"],
                    id = (string)aItem["id"],
                    name = (string)aItem["name"],
                    email = (string)aItem["email"],
                    phone = (string)aItem["phone"],
                    office_phone = (string)aItem["office_phone"],
                    fax = (string)aItem["fax"],
                    alt_email = (string)aItem["alt_email"],
                    uen = (string)aItem["uen"],
                    brn = (string)aItem["brn"],
                    banck_account_type = (string)aItem["banck_account_type"],
                    vendor_origin = (string)aItem["vendor_origin"],
                    subsidiary = (string)aItem["subsidiary"],
                    id_subsidiary = (string)aItem["id_subsidiary"],
                };
                lstGrower.Add(objGrower);
            }
            if (!_growerLogic.Insertar(UConvert.ToDataTable(lstGrower)))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }
            return StatusCode((int)HttpStatusCode.OK);
        }

        /// <summary>
        /// Method to get Client
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetClient")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult ListClient()
        {
            var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.GET, SettingEndPoint.NetSuite_Key_Client_consulta);
            var serializedresult = JObject.Parse(sResponse);

            if (!serializedresult.HasValues)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONTENIDO_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONTENIDO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.NoContent, objErrorAnswer);
            }

            var jsonerror = serializedresult["error"];
            if (!string.IsNullOrEmpty(jsonerror.ToString()))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var jsonarray = (JArray)serializedresult["response"];
            if (jsonarray.Count == 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }

            var lstClient = new List<EClient>();
            EClient objClient = null;
            JObject aItem = null;
            for (int i = 0; i < jsonarray.Count; i++)
            {
                aItem = (JObject)jsonarray[i];
                objClient = new EClient()
                {
                    internalid = (string)aItem["internalid"],
                    isperson = (string)aItem["isperson"],
                    firstname = (string)aItem["firstname"],
                    lastname = (string)aItem["lastname"],
                    companyname = (string)aItem["companyname"],
                    externalid = (string)aItem["externalid"],
                    email = (string)aItem["email"],
                    id_subsidiary = (string)aItem["id_subsidiary"],
                    subsidiary = (string)aItem["subsidiary"],
                    tipodocumento = (string)aItem["tipodocumento"],
                    nodocumento = (string)aItem["nodocumento"],
                };
                lstClient.Add(objClient);
            }
            /**/
            if (!_clientLogic.Compare(UConvert.ToDataTable(lstClient)))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }
            /**/
            return StatusCode((int)HttpStatusCode.OK, serializedresult);

        }

        /// <summary>
        /// Method to put Client
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPut("PutMasterClient")]
        [ProducesResponseType(200, Type = typeof(List<EClientResponse>))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult InsertMasterClient()
        {
            DataTable objDtt = null;
            if (!_clientLogic.List(out objDtt))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }
            var jArray = JArray.FromObject(objDtt, JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            var jsonBody = string.Empty;
            var lstClientSyncCorrect = new List<EClientDTO>();
            var lstClientSyncIncorrect = new List<EClientResponse>();
            for (int i = 0; i < objDtt.Rows.Count; i++)
            {
                var rowJToken = jArray[i];
                jsonBody = rowJToken.ToString(Formatting.Indented);
                var jsonBddyParse = JObject.Parse(jsonBody);
                var subsidiaryId = jsonBddyParse["subsidiary"].ToString();
                var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.PUT, SettingEndPoint.NetSuite_Key_Client_registro,default , jsonBody);
                if (string.IsNullOrEmpty(sResponse))
                {
                    lstClientSyncIncorrect.Add(new EClientResponse()
                    {
                        id = ConstantsError.ERROR_NO_HUBO_RESPUESTA_CODIGO,
                        error = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                        trama = jsonBody
                    });
                    continue;
                }
                var serializedresult = JObject.Parse(sResponse);
                var jsonerror = serializedresult["error"];
                if (!string.IsNullOrEmpty(jsonerror.ToString()))
                {
                    lstClientSyncIncorrect.Add(new EClientResponse()
                    {
                        id = objDtt.Rows[i]["externalid"].ToString(),
                        error = jsonerror.ToString(),
                        trama = jsonBody
                    });
                    continue;
                }
                var aExternalId = objDtt.Rows[i]["externalid"].ToString().Split('-');
                lstClientSyncCorrect.Add(new EClientDTO()
                {
                    externalid = aExternalId[0],
                    internalid = Convert.ToString(serializedresult["id"]),
                    id_subsidiary = Convert.ToInt32(subsidiaryId)
                });
            }

            if (lstClientSyncCorrect.Count > 0)
                if (!_clientLogic.Update(UConvert.ToDataTable(lstClientSyncCorrect)))
                {
                    var objErrorAnswer = new ErrorAnswer()
                    {
                        error = new ErrorAnswerDetail()
                        {
                            idtransaccion = "",
                            titulo = "ERROR",
                            codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                            mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                        }
                    };
                    return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
                }

            if (lstClientSyncIncorrect.Count > 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, lstClientSyncIncorrect);
            }

            return StatusCode((int)HttpStatusCode.OK, lstClientSyncCorrect);

        }

        /// <summary>
        /// Method to get Logistic Operator
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetMasterLogisticOperator")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult ListMasterLogisticOperator()
        {
            var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.GET, SettingEndPoint.NetSuite_Key_Logistic_Operator_consulta);
            var serializedresult = JObject.Parse(sResponse);

            if (!serializedresult.HasValues)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONTENIDO_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONTENIDO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.NoContent, objErrorAnswer);
            }
            var jsonerror = serializedresult["error"];
            if (!string.IsNullOrEmpty(jsonerror.ToString()))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var jsonarray = (JArray)serializedresult["response"];
            if (jsonarray.Count == 0)
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_MALA_PETICION_CODIGO,
                        mensaje = ConstantsError.ERROR_MALA_PETICION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.BadRequest, objErrorAnswer);
            }
            var lstLogisticOperator = new List<ELogisticOperator>();
            ELogisticOperator objLogisticOperator = null;
            JObject aItem = null;
            for (int i = 0; i < jsonarray.Count; i++)
            {
                aItem = (JObject)jsonarray[i];
                objLogisticOperator = new ELogisticOperator()
                {
                    internalid = (string)aItem["internalid"],
                    id = (string)aItem["id"],
                    name = (string)aItem["name"],

                };

                lstLogisticOperator.Add(objLogisticOperator);
            }

            if (!_logisticOperatorLogic.Insert(UConvert.ToDataTable(lstLogisticOperator)))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_EXCEPCION_CODIGO,
                        mensaje = ConstantsError.ERROR_EXCEPCION_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }

            return StatusCode((int)HttpStatusCode.OK);

        }

        /// <summary>
        /// Method to put Commercial Plan
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPut("PutTransaccionalCommercialPlan")]
        [ProducesResponseType(200, Type = typeof(ECommercialPlanConflict))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult InsertTransaccionalCommercialPlan()
        {
            DataSet objDts = null;
            if (!_commercialPlanLogic.List(out objDts))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }

            ECommercialPlan objPlnCom = null;
            ECommercialPlanDetalle objPlnComDet = null;
            ECommercialPlanCampaign eCommercialPlanCampaign = null;
            var lstPlnComToSync = new List<ECommercialPlan>();
            var lstPlnComNoSync = new List<ECommercialPlanNoSync>();
            var lstPlnComSync = new List<ECommercialPlanSync>();
            var lstPlanCampaign = new List<ECommercialPlanCampaign>();
            var objPlnComConflict = new ECommercialPlanConflict();
            for (int i = 0; i < objDts.Tables[0].Rows.Count; i++)
            {
                eCommercialPlanCampaign = new ECommercialPlanCampaign()
                {
                    planCustomerVarietyID = Convert.ToInt32(objDts.Tables[0].Rows[i]["doc_id"].ToString().Trim()),
                    campaignID = Convert.ToInt32(objDts.Tables[0].Rows[i]["campaignID"].ToString().Trim())
                };
                lstPlanCampaign.Add(eCommercialPlanCampaign);
                objPlnCom = new ECommercialPlan()
                {
                    doc_id = objDts.Tables[0].Rows[i]["doc_id"].ToString().Trim(),
                    fecha_creacion = objDts.Tables[0].Rows[i]["fecha_creacion"].ToString().Trim(),
                    fecha_aprobacion = objDts.Tables[0].Rows[i]["fecha_aprobacion"].ToString().Trim(),
                    estado = objDts.Tables[0].Rows[i]["estado"].ToString().Trim(),
                    subsidiaria = objDts.Tables[0].Rows[i]["subsidiaria"].ToString().Trim(),
                };
                objPlnCom.detalle = new List<ECommercialPlanDetalle>();
                for (int j = 0; j < objDts.Tables[1].Rows.Count; j++)
                {
                    if (objPlnCom.doc_id.Equals(objDts.Tables[1].Rows[j]["doc_id"].ToString().Trim()))
                    {
                        objPlnComDet = new ECommercialPlanDetalle()
                        {
                            customer = Convert.ToInt32(objDts.Tables[1].Rows[j]["customer"].ToString().Trim()),
                            codigo_articulo = Convert.ToInt32(objDts.Tables[1].Rows[j]["codigo_articulo"].ToString().Trim()),
                            cantidad = Convert.ToDouble(objDts.Tables[1].Rows[j]["cantidad"].ToString().Trim())
                        };

                        objPlnCom.detalle.Add(objPlnComDet);
                    }
                }
                lstPlnComToSync.Add(objPlnCom);
            }

            var jArray = JArray.FromObject(lstPlnComToSync, JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            var jsonBody = string.Empty;
            for (int i = 0; i < jArray.Count; i++)
            {
                var aItem = (JObject)jArray[i];
                var rowJToken = jArray[i];
                var objJson = (JObject)jArray[i];
                jsonBody = rowJToken.ToString(Formatting.Indented);
                var sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.PUT, SettingEndPoint.NetSuite_Key_Commercial_plan_registro, default, jsonBody);
                var campaignID = (from objPlan in lstPlanCampaign
                                  where objPlan.planCustomerVarietyID == Convert.ToInt32(aItem["doc_id"])
                                  select objPlan.campaignID).Single();
                /*
                DataTable dttPlan = _commercialPlanLogic.GetPlanCustomerVariety(campaignID, "cam", 0);
                var planes = from plan in dttPlan.AsEnumerable()
                                where plan.Field<int>("planCustomerVarietyID") == Convert.ToInt32(aItem["doc_id"])
                                select new
                                {
                                    
                                    brand = plan.Field<string>("brand"),
                                    variety = plan.Field<string>("variety"),
                                    size = plan.Field<string>("size"),
                                    format = plan.Field<string>("format"),
                                    customer = plan.Field<string>("variety"),
                                    program = plan.Field<string>("program"),
                                    destination = plan.Field<string>("destination"),
                                    via = plan.Field<string>("via"),
                                    priority = plan.Field<string>("priority"),
                                    statusConfirm = plan.Field<string>("statusConfirm"),
                                    //projectedWeekID = plan.Field<string>("projectedWeekID"),
                                    codePack = plan.Field<string>("codePack")
                                };
                */
                if (string.IsNullOrEmpty(sResponse))
                {
                    lstPlnComNoSync.Add(new ECommercialPlanNoSync()
                    {
                        id = "[No response returned]",
                        error = "[No response returned]",
                        objJson = objJson,
                        idPlan = Convert.ToInt32(aItem["doc_id"]),
                        campaignID = campaignID,
                        dataPlan = "",
                        mensaje = "[No response returned]",
                    });
                    continue;
                }
                var serializedresult = JObject.Parse(sResponse);
                var jsonerror = serializedresult["error"];
                if (!string.IsNullOrEmpty(jsonerror.ToString()))
                {
                    lstPlnComNoSync.Add(new ECommercialPlanNoSync()
                    {
                        id = "",
                        error = jsonerror.ToString(),
                        objJson = objJson,
                        idPlan = Convert.ToInt32(aItem["doc_id"]),
                        campaignID = campaignID,
                        dataPlan = "",
                        mensaje = JObject.Parse(jsonerror.ToString()).ContainsKey("message") ? JObject.Parse(jsonerror.ToString())["message"].ToString() : string.Empty,
                    });
                    continue;
                }
                var jsonid = serializedresult["id"];
                lstPlnComSync.Add(new ECommercialPlanSync()
                {
                    id = jsonid.ToString(),
                    error = string.Empty,
                    objJson = objJson,
                    idPlan = Convert.ToInt32(aItem["doc_id"]),
                    campaignID = campaignID,
                    dataPlan = "",
                });
            }

            List<ECommercialPlanSummary> lstPlnSummary = new List<ECommercialPlanSummary>();
            ECommercialPlanSummary eCommercialPlanSummary = null;
            for (int i = 0; i < objDts.Tables[2].Rows.Count; i++)
            {
                eCommercialPlanSummary = new ECommercialPlanSummary()
                {
                    campaignID = Convert.ToInt32(objDts.Tables[2].Rows[i]["campaignID"].ToString().Trim()),
                    campaign = objDts.Tables[2].Rows[i]["campaign"].ToString().Trim(),
                    cropID = objDts.Tables[2].Rows[i]["cropID"].ToString().Trim(),
                    weekIni = objDts.Tables[2].Rows[i]["weekIni"].ToString().Trim(),
                    weekFin = objDts.Tables[2].Rows[i]["weekFin"].ToString().Trim()

                };
                lstPlnSummary.Add(eCommercialPlanSummary);
            }
            objPlnComConflict.lstPlnComSync = lstPlnComSync;
            objPlnComConflict.lstPlnComNoSync = lstPlnComNoSync;
            objPlnComConflict.lstPlnSummary = lstPlnSummary;

            if (lstPlnComSync.Count > 0)
            {
                List<EPlanSync> ePlanSyncs = new List<EPlanSync>();
                lstPlnComSync.ForEach(item =>
                {
                    ePlanSyncs.Add(new EPlanSync()
                    {
                        internalid = item.id,
                        planCustomerVarietyID = item.idPlan
                    });
                });
                if (!_commercialPlanLogic.Insert(UConvert.ToDataTable(ePlanSyncs)))
                {
                    var objErrorAnswer = new ErrorAnswer()
                    {
                        error = new ErrorAnswerDetail()
                        {
                            idtransaccion = "",
                            titulo = "ERROR",
                            codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                            mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                        }
                    };
                    return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
                }
            }
            return StatusCode((int)HttpStatusCode.OK, objPlnComConflict);

        }

        /// <summary>
        /// Method to put Commercial Plan
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPut("PutTransaccionalCommercialPlanV2")]
        [ProducesResponseType(200, Type = typeof(ECommercialPlanConflict))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult InsertTransaccionalCommercialPlanV2()
        {
            DataSet objDts = null;
            if (!_commercialPlanLogic.List(out objDts))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }

            ECommercialPlan objPlnCom = null;
            ECommercialPlanDetalle objPlnComDet = null;
            ECommercialPlanCampaign eCommercialPlanCampaign = null;
            var lstPlnComToSync = new List<ECommercialPlan>();
            var lstPlnComNoSync = new List<ECommercialPlanNoSync>();
            var lstPlnComSync = new List<ECommercialPlanSync>();
            var lstPlanCampaign = new List<ECommercialPlanCampaign>();
            var objPlnComConflict = new ECommercialPlanConflict();
            for (int i = 0; i < objDts.Tables[0].Rows.Count; i++)
            {
                eCommercialPlanCampaign = new ECommercialPlanCampaign()
                {
                    planCustomerVarietyID = Convert.ToInt32(objDts.Tables[0].Rows[i]["doc_id"].ToString().Trim()),
                    campaignID = Convert.ToInt32(objDts.Tables[0].Rows[i]["campaignID"].ToString().Trim())
                };
                lstPlanCampaign.Add(eCommercialPlanCampaign);
                objPlnCom = new ECommercialPlan()
                {
                    doc_id = objDts.Tables[0].Rows[i]["doc_id"].ToString().Trim(),
                    fecha_creacion = objDts.Tables[0].Rows[i]["fecha_creacion"].ToString().Trim(),
                    fecha_aprobacion = objDts.Tables[0].Rows[i]["fecha_aprobacion"].ToString().Trim(),
                    estado = objDts.Tables[0].Rows[i]["estado"].ToString().Trim(),
                    subsidiaria = objDts.Tables[0].Rows[i]["subsidiaria"].ToString().Trim(),
                };
                objPlnCom.detalle = new List<ECommercialPlanDetalle>();
                for (int j = 0; j < objDts.Tables[1].Rows.Count; j++)
                {
                    if (objPlnCom.doc_id.Equals(objDts.Tables[1].Rows[j]["doc_id"].ToString().Trim()))
                    {
                        objPlnComDet = new ECommercialPlanDetalle()
                        {
                            customer = Convert.ToInt32(objDts.Tables[1].Rows[j]["customer"].ToString().Trim()),
                            codigo_articulo = Convert.ToInt32(objDts.Tables[1].Rows[j]["codigo_articulo"].ToString().Trim()),
                            week =   objDts.Tables[1].Rows[j]["week"].ToString().Trim(),
                            date_ini = objDts.Tables[1].Rows[j]["date_ini"].ToString().Trim(),
                            date_end = objDts.Tables[1].Rows[j]["date_end"].ToString().Trim(),
                            cantidad = Convert.ToDouble(objDts.Tables[1].Rows[j]["cantidad"].ToString().Trim()),
                            doc_week_date_id = objDts.Tables[1].Rows[j]["doc_week_date_id"].ToString().Trim(),

                        };

                        objPlnCom.detalle.Add(objPlnComDet);
                    }
                }
                lstPlnComToSync.Add(objPlnCom);
            }

            var jArray = JArray.FromObject(lstPlnComToSync, JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            if (!NetsuiteServiceManagement.CheckForInternetConnection())
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONEXION_INTERNET_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONEXION_INTERNET_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.RequestTimeout, objErrorAnswer);
            }
            var jsonBody = string.Empty;
            for (int i = 0; i < jArray.Count; i++)
            {
                var aItem = (JObject)jArray[i];
                var rowJToken = jArray[i];
                var objJson = (JObject)jArray[i];
                jsonBody = rowJToken.ToString(Formatting.Indented);

                var campaignID = (from objPlan in lstPlanCampaign
                                  where objPlan.planCustomerVarietyID == Convert.ToInt32(aItem["doc_id"])
                                  select objPlan.campaignID).Single();

                var sResponse = string.Empty;
                try
                {
                    sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.PUT, SettingEndPoint.NetSuite_Key_Commercial_plan_registro, default, jsonBody);
                }
                catch (Exception e)
                {
                    lstPlnComNoSync.Add(new ECommercialPlanNoSync()
                    {
                        id = ConstantsError.ERROR_TIEMPO_FUERA_CODIGO,
                        error = ConstantsError.ERROR_TIEMPO_FUERA_MENSAJE,
                        objJson = objJson,
                        idPlan = Convert.ToInt32(aItem["doc_id"]),
                        campaignID = campaignID,
                        dataPlan = "",
                        mensaje = ConstantsError.ERROR_TIEMPO_FUERA_MENSAJE,
                    });
                    continue;
                }                
                
                /*
                DataTable dttPlan = _commercialPlanLogic.GetPlanCustomerVariety(campaignID, "cam", 0);
                var planes = from plan in dttPlan.AsEnumerable()
                                where plan.Field<int>("planCustomerVarietyID") == Convert.ToInt32(aItem["doc_id"])
                                select new
                                {
                                    
                                    brand = plan.Field<string>("brand"),
                                    variety = plan.Field<string>("variety"),
                                    size = plan.Field<string>("size"),
                                    format = plan.Field<string>("format"),
                                    customer = plan.Field<string>("variety"),
                                    program = plan.Field<string>("program"),
                                    destination = plan.Field<string>("destination"),
                                    via = plan.Field<string>("via"),
                                    priority = plan.Field<string>("priority"),
                                    statusConfirm = plan.Field<string>("statusConfirm"),
                                    //projectedWeekID = plan.Field<string>("projectedWeekID"),
                                    codePack = plan.Field<string>("codePack")
                                };
                */
                if (string.IsNullOrEmpty(sResponse))
                {
                    lstPlnComNoSync.Add(new ECommercialPlanNoSync()
                    {
                        id = ConstantsError.ERROR_NO_HUBO_RESPUESTA_CODIGO,
                        error = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                        objJson = objJson,
                        idPlan = Convert.ToInt32(aItem["doc_id"]),
                        campaignID = campaignID,
                        dataPlan = "",
                        mensaje = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                    });
                    continue;
                }
                var serializedresult = JObject.Parse(sResponse);
                var jsonerror = serializedresult["error"];
                if (!string.IsNullOrEmpty(jsonerror.ToString()))
                {
                    lstPlnComNoSync.Add(new ECommercialPlanNoSync()
                    {
                        id = "",
                        error = jsonerror.ToString(),
                        objJson = objJson,
                        idPlan = Convert.ToInt32(aItem["doc_id"]),
                        campaignID = campaignID,
                        dataPlan = "",
                        mensaje = JObject.Parse(jsonerror.ToString()).ContainsKey("message") ? JObject.Parse(jsonerror.ToString())["message"].ToString() : string.Empty,
                    });
                    continue;
                }
                var jsonid = serializedresult["id"];
                lstPlnComSync.Add(new ECommercialPlanSync()
                {
                    id = jsonid.ToString(),
                    error = string.Empty,
                    objJson = objJson,
                    idPlan = Convert.ToInt32(aItem["doc_id"]),
                    campaignID = campaignID,
                    dataPlan = "",
                });
            }

            List<ECommercialPlanSummary> lstPlnSummary = new List<ECommercialPlanSummary>();
            ECommercialPlanSummary eCommercialPlanSummary = null;
            for (int i = 0; i < objDts.Tables[2].Rows.Count; i++)
            {
                eCommercialPlanSummary = new ECommercialPlanSummary()
                {
                    campaignID = Convert.ToInt32(objDts.Tables[2].Rows[i]["campaignID"].ToString().Trim()),
                    campaign = objDts.Tables[2].Rows[i]["campaign"].ToString().Trim(),
                    cropID = objDts.Tables[2].Rows[i]["cropID"].ToString().Trim(),
                    weekIni = objDts.Tables[2].Rows[i]["weekIni"].ToString().Trim(),
                    weekFin = objDts.Tables[2].Rows[i]["weekFin"].ToString().Trim()

                };
                lstPlnSummary.Add(eCommercialPlanSummary);
            }
            objPlnComConflict.lstPlnComSync = lstPlnComSync;
            objPlnComConflict.lstPlnComNoSync = lstPlnComNoSync;
            objPlnComConflict.lstPlnSummary = lstPlnSummary;
            
            return StatusCode((int)HttpStatusCode.OK, objPlnComConflict);

        }

        /// <summary>
        /// Method to Put Sales Order
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPut("PutTransaccionalSalesOrder")]
        [ProducesResponseType(200, Type = typeof(ESaleOrderConflict))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult InsertTransaccionalSalesOrder()
        {
            DataSet objDts = null;
            if (!_saleOrderLogic.List(out objDts))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }

            ESaleOrder objSO = null;
            ESaleOrderDetalle objSODet = null;
            ESaleOrder_RequestNumber saleOrder_RequestNumber = null;
            var lstSOToSync = new List<ESaleOrder>();
            var lstSONoSync = new List<ESaleOrderNoSync>();
            var lstSOSync = new List<ESaleOrderSync>();
            var lstSORequestNumber = new List<ESaleOrder_RequestNumber>();
            var objSOConflict = new ESaleOrderConflict();
            for (int i = 0; i < objDts.Tables[0].Rows.Count; i++)
            {
                saleOrder_RequestNumber = new ESaleOrder_RequestNumber()
                {
                    idOS = Convert.ToInt32(objDts.Tables[0].Rows[i]["externalid"].ToString().Trim()),
                    campaignID = Convert.ToInt32(objDts.Tables[0].Rows[i]["campaignID"].ToString().Trim()),
                    projectedWeekID = Convert.ToInt32(objDts.Tables[0].Rows[i]["projectedWeekID"].ToString().Trim()),
                    requestNumber = objDts.Tables[0].Rows[i]["requestNumber"].ToString().Trim()
                };
                lstSORequestNumber.Add(saleOrder_RequestNumber);
                objSO = new ESaleOrder()
                {
                    externalid = objDts.Tables[0].Rows[i]["externalid"].ToString().Trim(),
                    entity = Convert.ToInt32(objDts.Tables[0].Rows[i]["entity"].ToString().Trim()),
                    tipo_documento = Convert.ToInt32(objDts.Tables[0].Rows[i]["tipo_documento"].ToString().Trim()),
                    nro_documento = objDts.Tables[0].Rows[i]["nro_documento"].ToString().Trim(),
                    tipo_cambio = objDts.Tables[0].Rows[i]["tipo_cambio"].ToString().Trim(),
                    ubicacion = Convert.ToInt32(objDts.Tables[0].Rows[i]["ubicacion"].ToString().Trim()),
                    nivel_de_prioridad = Convert.ToInt32(objDts.Tables[0].Rows[i]["nivel_de_prioridad"].ToString().Trim()),
                    correlativo = objDts.Tables[0].Rows[i]["correlativo"].ToString().Trim(),
                    semana_proyectada = objDts.Tables[0].Rows[i]["semana_proyectada"].ToString().Trim(),
                    creado_por = objDts.Tables[0].Rows[i]["creado_por"].ToString().Trim(),
                    campania = objDts.Tables[0].Rows[i]["campania"].ToString().Trim(),
                    aprobado_por = objDts.Tables[0].Rows[i]["aprobado_por"].ToString().Trim(),
                    productor = objDts.Tables[0].Rows[i]["productor"].ToString().Trim(),
                    instruccion_embarque = objDts.Tables[0].Rows[i]["instruccion_embarque"].ToString().Trim(),
                    destino = objDts.Tables[0].Rows[i]["destino"].ToString().Trim(),
                    via = objDts.Tables[0].Rows[i]["via"].ToString().Trim(),
                    //aplica_plu = objDts.Tables[0].Rows[i]["aplica_plu"].ToString().Trim(),
                    notificante = objDts.Tables[0].Rows[i]["notificante"].ToString().Trim(),
                    estado = objDts.Tables[0].Rows[i]["status"].ToString().Trim(),
                    fecha_so = objDts.Tables[0].Rows[i]["fecha_so"].ToString().Trim(),
                };
                objSO.articulos = new List<ESaleOrderDetalle>();
                for (int j = 0; j < objDts.Tables[1].Rows.Count; j++)
                {
                    if (objSO.externalid.Equals(objDts.Tables[1].Rows[j]["externalid"].ToString().Trim()))
                    {
                        objSODet = new ESaleOrderDetalle()
                        {
                            item = Convert.ToInt32(objDts.Tables[1].Rows[j]["item"].ToString().Trim()),
                            rate = Convert.ToDouble(objDts.Tables[1].Rows[j]["rate"].ToString().Trim()),
                            quantity = Convert.ToInt32(objDts.Tables[1].Rows[j]["quantity"].ToString().Trim()),
                            createwo = objDts.Tables[1].Rows[j]["createwo"].ToString().Trim(),
                            no_workorder = objDts.Tables[1].Rows[i]["no_workorder"].ToString().Trim(),
                        };

                        objSO.articulos.Add(objSODet);
                    }
                }
                lstSOToSync.Add(objSO);
            }

            var jArray = JArray.FromObject(lstSOToSync, JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            if (!NetsuiteServiceManagement.CheckForInternetConnection())
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONEXION_INTERNET_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONEXION_INTERNET_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.RequestTimeout, objErrorAnswer);
            }
            var jsonBody = string.Empty;
            for (int i = 0; i < jArray.Count; i++)
            {
                var aItem = (JObject)jArray[i];
                var rowJToken = jArray[i];
                var objJson = (JObject)jArray[i];
                jsonBody = rowJToken.ToString(Formatting.Indented);

                var campaignID = (from _objOS in lstSORequestNumber
                                  where _objOS.idOS == Convert.ToInt32(aItem["externalid"])
                                  select _objOS.campaignID).First();
                var requestNumber = (from _objOS in lstSORequestNumber
                                     where _objOS.idOS == Convert.ToInt32(aItem["externalid"])
                                     select _objOS.requestNumber).First();
                var projectedWeekID = (from _objOS in lstSORequestNumber
                                       where _objOS.idOS == Convert.ToInt32(aItem["externalid"])
                                       select _objOS.projectedWeekID).First();

                var sResponse = string.Empty;
                try
                {
                    CancellationTokenSource cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;
                    cts.CancelAfter(NetsuiteServiceManagement.RequestTimeout());
                    token.ThrowIfCancellationRequested();
                    sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.PUT,SettingEndPoint.NetSuite_Key_Sale_order_registro, token, jsonBody);
                }
                catch (OperationCanceledException e)
                {
                    lstSONoSync.Add(new ESaleOrderNoSync()
                    {
                        id = ConstantsError.ERROR_TIEMPO_FUERA_CODIGO,
                        error = ConstantsError.ERROR_TIEMPO_FUERA_MENSAJE,
                        objJson = objJson,
                        idOS = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        projectedWeekID = projectedWeekID,
                        requestNumber = requestNumber,
                        dataOS = "",
                        mensaje = ConstantsError.ERROR_TIEMPO_FUERA_MENSAJE,
                    });
                    continue;
                }
                catch (Exception e)
                {
                    lstSONoSync.Add(new ESaleOrderNoSync()
                    {
                        id = ConstantsError.ERROR_DESCONOCIDO_CODIGO,
                        error = ConstantsError.ERROR_DESCONOCIDO_MENSAJE,
                        objJson = objJson,
                        idOS = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        projectedWeekID = projectedWeekID,
                        requestNumber = requestNumber,
                        dataOS = "",
                        mensaje = ConstantsError.ERROR_DESCONOCIDO_MENSAJE,
                    });
                    continue;
                }
                
                if (string.IsNullOrEmpty(sResponse))
                {
                    lstSONoSync.Add(new ESaleOrderNoSync()
                    {
                        id = ConstantsError.ERROR_NO_HUBO_RESPUESTA_CODIGO,
                        error = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                        objJson = objJson,
                        idOS = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        projectedWeekID = projectedWeekID,
                        requestNumber = requestNumber,
                        dataOS = "",
                        mensaje = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                    });
                    continue;
                }
                var serializedresult = JObject.Parse(sResponse);
                var jsonerror = serializedresult["error"];
                if (!string.IsNullOrEmpty(jsonerror.ToString()))
                {
                    lstSONoSync.Add(new ESaleOrderNoSync()
                    {
                        id = "",
                        error = jsonerror.ToString(),
                        objJson = objJson,
                        idOS = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        projectedWeekID = projectedWeekID,
                        requestNumber = requestNumber,
                        dataOS = "",
                        mensaje = JObject.Parse(jsonerror.ToString()).ContainsKey("message") ? JObject.Parse(jsonerror.ToString())["message"].ToString() : string.Empty,
                    });
                    continue;
                }
                var jsonid = serializedresult["id"];
                lstSOSync.Add(new ESaleOrderSync()
                {
                    id = jsonid.ToString(),
                    error = string.Empty,
                    objJson = objJson,
                    idOS = Convert.ToInt32(aItem["externalid"]),
                    campaignID = campaignID,
                    projectedWeekID = projectedWeekID,
                    requestNumber = requestNumber,
                    dataOS = ""
                });
            }

            List<ESaleOrderSummary> lstSOSummary = new List<ESaleOrderSummary>();
            ESaleOrderSummary eSaleOrderSummary = null;
            for (int i = 0; i < objDts.Tables[2].Rows.Count; i++)
            {
                eSaleOrderSummary = new ESaleOrderSummary()
                {
                    campaignID = Convert.ToInt32(objDts.Tables[2].Rows[i]["campaignID"].ToString().Trim()),
                    campaign = objDts.Tables[2].Rows[i]["campaign"].ToString().Trim(),
                    cropID = objDts.Tables[2].Rows[i]["cropID"].ToString().Trim(),
                    weekIni = objDts.Tables[2].Rows[i]["weekIni"].ToString().Trim(),
                    weekFin = objDts.Tables[2].Rows[i]["weekFin"].ToString().Trim()

                };
                lstSOSummary.Add(eSaleOrderSummary);
            }
            objSOConflict.lstSOSync = lstSOSync;
            objSOConflict.lstSONoSync = lstSONoSync;
            objSOConflict.lstSOSummary = lstSOSummary;

            if (lstSOSync.Count > 0)
            {
                List<EOrderSync> eOrderSyncs = new List<EOrderSync>();
                lstSOSync.ForEach(item =>
                {
                    eOrderSyncs.Add(new EOrderSync()
                    {
                        internalid = item.id,
                        orderProductionID = item.idOS
                    });
                });
                if (!_saleOrderLogic.Insert(UConvert.ToDataTable(eOrderSyncs)))
                {
                    var objErrorAnswer = new ErrorAnswer()
                    {
                        error = new ErrorAnswerDetail()
                        {
                            idtransaccion = "",
                            titulo = "ERROR",
                            codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                            mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                        }
                    };
                    return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
                }
            }
            return StatusCode((int)HttpStatusCode.OK, objSOConflict);

        }

        /// <summary>
        /// Method to Put Shipping Instruction
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPut("PutTransaccionalLoadingInstruction")]
        [ProducesResponseType(200, Type = typeof(EShippingInstructionConflict))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ErrorHandlingMiddleware]
        public ActionResult InsertTransaccionalShippingInstruction()
        {
            DataSet objDts = null;
            if (!_instruccionEmbarqueLogic.List(out objDts))
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                        mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
            }

            EInstruccionEmbarque objIE = null;
            EInstruccionEmbarqueDetalle objIEDet = null;
            EShippingInstruction_PackingNumber embarque_PackingNumber = null;
            var lstIEToSync = new List<EInstruccionEmbarque>();
            var lstIENoSync = new List<EShippingInstructionNoSync>();
            var lstIESync = new List<EShippingInstructionSync>();
            var lstIEPackingNumber = new List<EShippingInstruction_PackingNumber>();
            var objIEConflict = new EShippingInstructionConflict();
            for (int i = 0; i < objDts.Tables[0].Rows.Count; i++)
            {
                embarque_PackingNumber = new EShippingInstruction_PackingNumber()
                {
                    idIE = Convert.ToInt32(objDts.Tables[0].Rows[i]["externalid"].ToString().Trim()),
                    packingNumber = objDts.Tables[0].Rows[i]["num_packing_list"].ToString().Trim(),
                    campaignID = Convert.ToInt32(objDts.Tables[0].Rows[i]["campaignID"].ToString().Trim())
                };
                lstIEPackingNumber.Add(embarque_PackingNumber);
                objIE = new EInstruccionEmbarque()
                {
                    externalid = objDts.Tables[0].Rows[i]["externalid"].ToString().Trim(),
                    num_contenedor = objDts.Tables[0].Rows[i]["num_contenedor"].ToString().Trim(),
                    operador_logistico = Convert.ToInt32(objDts.Tables[0].Rows[i]["operador_logistico"].ToString().Trim()),
                    persona_encargada = Convert.ToInt32(objDts.Tables[0].Rows[i]["persona_encargada"].ToString().Trim()),
                    fecha_ingreso_terminal = objDts.Tables[0].Rows[i]["fecha_ingreso_terminal"].ToString().Trim(),
                    naviera = objDts.Tables[0].Rows[i]["naviera"].ToString().Trim(),
                    num_packing_list = objDts.Tables[0].Rows[i]["num_packing_list"].ToString().Trim(),
                    fecha_contacto = objDts.Tables[0].Rows[i]["fecha_contacto"].ToString().Trim(),
                    contacto_agente = objDts.Tables[0].Rows[i]["contacto_agente"].ToString().Trim(),
                    num_booking = objDts.Tables[0].Rows[i]["num_booking"].ToString().Trim(),
                    nave_viaje = objDts.Tables[0].Rows[i]["nave_viaje"].ToString().Trim(),
                    regimen = Convert.ToInt32(objDts.Tables[0].Rows[i]["regimen"].ToString().Trim()),
                    condicion_flete = Convert.ToInt32(objDts.Tables[0].Rows[i]["condicion_flete"].ToString().Trim()),
                    puerto_origen_embarque = Convert.ToInt32(objDts.Tables[0].Rows[i]["puerto_origen_embarque"].ToString().Trim()),
                    puerto_destino = objDts.Tables[0].Rows[i]["puerto_destino"].ToString().Trim(),
                    etd_aduana = objDts.Tables[0].Rows[i]["etd_aduana"].ToString().Trim(),
                    eta = objDts.Tables[0].Rows[i]["eta"].ToString().Trim(),
                    certificado_fitosanitario = Convert.ToInt32(objDts.Tables[0].Rows[i]["certificado_fitosanitario"].ToString().Trim()),
                    cold_treatment = Convert.ToBoolean(objDts.Tables[0].Rows[i]["cold_treatment"].ToString().Trim()),
                    atmosfera_controlada = Convert.ToBoolean(objDts.Tables[0].Rows[i]["atmosfera_controlada"].ToString().Trim()),
                    temperatura = objDts.Tables[0].Rows[i]["temperatura"].ToString().Trim(),
                    ventilacion = objDts.Tables[0].Rows[i]["ventilacion"].ToString().Trim(),
                    humedad = objDts.Tables[0].Rows[i]["humedad"].ToString().Trim(),
                    quest = objDts.Tables[0].Rows[i]["quest"].ToString().Trim(),
                    lugar_inicio_despacho = objDts.Tables[0].Rows[i]["lugar_inicio_despacho"].ToString().Trim(),
                    lugar_final_despacho = objDts.Tables[0].Rows[i]["lugar_final_despacho"].ToString().Trim(),
                    //total_cajas  = 100,
                    tipo_flete = Convert.ToInt32(objDts.Tables[0].Rows[i]["tipo_flete"].ToString().Trim()),
                    medio_transporte = Convert.ToInt32(objDts.Tables[0].Rows[i]["medio_transporte"].ToString().Trim()),
                    num_instruccion_embarque = objDts.Tables[0].Rows[i]["num_instruccion_embarque"].ToString().Trim(),
                    ggn = objDts.Tables[0].Rows[i]["ggn"].ToString().Trim(),
                    fda = objDts.Tables[0].Rows[i]["fda"].ToString().Trim(),
                    fecha_ingreso_planta = objDts.Tables[0].Rows[i]["fecha_ingreso_planta"].ToString().Trim(),
                    fecha_salida_planta = objDts.Tables[0].Rows[i]["fecha_salida_planta"].ToString().Trim(),
                    incoterm= objDts.Tables[0].Rows[i]["incoterm"].ToString().Trim(),
                    peso_bruto = Convert.ToDecimal(objDts.Tables[0].Rows[i]["peso_bruto"].ToString().Trim()),
                    peso_neto = Convert.ToDecimal(objDts.Tables[0].Rows[i]["peso_neto"].ToString().Trim()),
                    ano_dua_exportacion = objDts.Tables[0].Rows[i]["ano_dua_exportacion"].ToString().Trim(),
                    num_dua_exportacion = objDts.Tables[0].Rows[i]["num_dua_exportacion"].ToString().Trim(),
                    ubicacion = Convert.ToInt32(objDts.Tables[0].Rows[i]["ubicacion"].ToString().Trim()),
                    ingreso_terminal = Convert.ToInt32(objDts.Tables[0].Rows[i]["ingreso_terminal"].ToString().Trim()),
                    orden_venta = Convert.ToInt32(objDts.Tables[0].Rows[i]["orden_venta"].ToString().Trim()),
                    consignatario = Convert.ToInt32(objDts.Tables[0].Rows[i]["consignatario"].ToString().Trim()),
                    notificante = Convert.ToInt32(objDts.Tables[0].Rows[i]["notificante"].ToString().Trim()),
                    notificante2 = Convert.ToInt32(objDts.Tables[0].Rows[i]["notificante2"].ToString().Trim()),
                    cropID = objDts.Tables[0].Rows[i]["cropID"].ToString().Trim(),
                    bajo_promesa = objDts.Tables[0].Rows[i]["bajo_promesa"].ToString().Trim()
                };
                objIE.detalle = new List<EInstruccionEmbarqueDetalle>();
                for (int j = 0; j < objDts.Tables[1].Rows.Count; j++)
                {
                    if (objIE.externalid.Equals(objDts.Tables[1].Rows[j]["externalid"].ToString().Trim()))
                    {
                        objIEDet = new EInstruccionEmbarqueDetalle()
                        {
                            externalid = Convert.ToInt32(objDts.Tables[1].Rows[j]["externalid"].ToString().Trim()),
                            orden_venta = Convert.ToInt32(objDts.Tables[1].Rows[j]["orden_venta"].ToString().Trim()),
                            total_cajas = Convert.ToInt32(objDts.Tables[1].Rows[j]["total_cajas"].ToString().Trim()),
                            item = Convert.ToInt32(objDts.Tables[1].Rows[j]["item"].ToString().Trim())
                        };

                        objIE.detalle.Add(objIEDet);
                    }
                }
                lstIEToSync.Add(objIE);
            }

            var jArray = JArray.FromObject(lstIEToSync, JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            if (!NetsuiteServiceManagement.CheckForInternetConnection())
            {
                var objErrorAnswer = new ErrorAnswer()
                {
                    error = new ErrorAnswerDetail()
                    {
                        idtransaccion = "",
                        titulo = "ERROR",
                        codigo = ConstantsError.ERROR_SIN_CONEXION_INTERNET_CODIGO,
                        mensaje = ConstantsError.ERROR_SIN_CONEXION_INTERNET_MENSAJE
                    }
                };
                return StatusCode((int)HttpStatusCode.RequestTimeout, objErrorAnswer);
            }
            var jsonBody = string.Empty;
            for (int i = 0; i < jArray.Count; i++)
            {
                var aItem = (JObject)jArray[i];
                var rowJToken = jArray[i];
                var objJson = (JObject)jArray[i];
                jsonBody = rowJToken.ToString(Formatting.Indented);

                var packingNumber = (from _objIE in lstIEPackingNumber
                                     where _objIE.idIE == Convert.ToInt32(aItem["externalid"])
                                     select _objIE.packingNumber).Single();
                var campaignID = (from _objIE in lstIEPackingNumber
                                  where _objIE.idIE == Convert.ToInt32(aItem["externalid"])
                                  select _objIE.campaignID).Single();

                var sResponse = string.Empty;
                try
                {
                    CancellationTokenSource cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;
                    cts.CancelAfter(NetsuiteServiceManagement.RequestTimeout());
                    token.ThrowIfCancellationRequested();
                    sResponse = NetsuiteServiceManagement.GetEndPointResponse(HttpMethod.PUT, SettingEndPoint.NetSuite_Key_Instruccion_embarque_registro, token, jsonBody);

                }
                catch (OperationCanceledException e)
                {
                    lstIENoSync.Add(new EShippingInstructionNoSync()
                    {
                        id = ConstantsError.ERROR_TIEMPO_FUERA_CODIGO,
                        error = ConstantsError.ERROR_TIEMPO_FUERA_MENSAJE,
                        objJson = objJson,
                        idIE = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        packingNumber = packingNumber,
                        dataIE = "",
                        mensaje = ConstantsError.ERROR_TIEMPO_FUERA_MENSAJE,
                    });
                    continue;
                }
                catch (Exception e)
                {
                    lstIENoSync.Add(new EShippingInstructionNoSync()
                    {
                        id = ConstantsError.ERROR_DESCONOCIDO_CODIGO,
                        error = ConstantsError.ERROR_DESCONOCIDO_MENSAJE,
                        objJson = objJson,
                        idIE = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        packingNumber = packingNumber,
                        dataIE = "",
                        mensaje = ConstantsError.ERROR_DESCONOCIDO_MENSAJE,
                    });
                    continue;
                }
                
                if (string.IsNullOrEmpty(sResponse))
                {
                    lstIENoSync.Add(new EShippingInstructionNoSync()
                    {
                        id = ConstantsError.ERROR_NO_HUBO_RESPUESTA_CODIGO,
                        error = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                        objJson = objJson,
                        idIE = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        packingNumber = packingNumber,
                        dataIE = "",
                        mensaje = ConstantsError.ERROR_NO_HUBO_RESPUESTA_MENSAJE,
                    });
                    continue;
                }
                var serializedresult = JObject.Parse(sResponse);
                var jsonerror = serializedresult["error"];
                if (!string.IsNullOrEmpty(jsonerror.ToString()))
                {
                    lstIENoSync.Add(new EShippingInstructionNoSync()
                    {
                        id = "",
                        error = jsonerror.ToString(),
                        objJson = objJson,
                        idIE = Convert.ToInt32(aItem["externalid"]),
                        campaignID = campaignID,
                        packingNumber = packingNumber,
                        dataIE = "",
                        mensaje = JObject.Parse(jsonerror.ToString()).ContainsKey("message") ? JObject.Parse(jsonerror.ToString())["message"].ToString() : string.Empty,
                    });
                    continue;
                }
                var jsonid = serializedresult["id"];
                lstIESync.Add(new EShippingInstructionSync()
                {
                    id = jsonid.ToString(),
                    error = string.Empty,
                    objJson = objJson,
                    idIE = Convert.ToInt32(aItem["externalid"]),
                    campaignID = campaignID,
                    packingNumber = packingNumber,
                    dataIE = ""
                });
            }

            List<EShippingInstructionSummary> lstSISummary = new List<EShippingInstructionSummary>();
            EShippingInstructionSummary eShippingInstructionSummary = null;
            for (int i = 0; i < objDts.Tables[2].Rows.Count; i++)
            {
                eShippingInstructionSummary = new EShippingInstructionSummary()
                {
                    campaignID = Convert.ToInt32(objDts.Tables[2].Rows[i]["campaignID"].ToString().Trim()),
                    campaign = objDts.Tables[2].Rows[i]["campaign"].ToString().Trim(),
                    cropID = objDts.Tables[2].Rows[i]["cropID"].ToString().Trim(),
                    weekIni = objDts.Tables[2].Rows[i]["weekIni"].ToString().Trim(),
                    weekFin = objDts.Tables[2].Rows[i]["weekFin"].ToString().Trim()

                };
                lstSISummary.Add(eShippingInstructionSummary);
            }
            objIEConflict.lstIESync = lstIESync;
            objIEConflict.lstIENoSync = lstIENoSync;
            objIEConflict.lstPlnSummary = lstSISummary;

            if(lstIESync.Count > 0)
            {
                List<EInstructionSync> eInstructionSyncs = new List<EInstructionSync>();
                lstIESync.ForEach(item =>
                {
                    eInstructionSyncs.Add(new EInstructionSync()
                    {
                        internalid = item.id,
                        shippingLoadingID = item.idIE
                    });
                });
                if (!_instruccionEmbarqueLogic.Insert(UConvert.ToDataTable(eInstructionSyncs)))
                {
                    var objErrorAnswer = new ErrorAnswer()
                    {
                        error = new ErrorAnswerDetail()
                        {
                            idtransaccion = "",
                            titulo = "ERROR",
                            codigo = ConstantsError.ERROR_GENERICO_CODIGO,
                            mensaje = ConstantsError.ERROR_GENERICO_MENSAJE
                        }
                    };
                    return StatusCode((int)HttpStatusCode.InternalServerError, objErrorAnswer);
                }
            }
            return StatusCode((int)HttpStatusCode.OK, objIEConflict);

        }

        #endregion

        #endregion
    }
}