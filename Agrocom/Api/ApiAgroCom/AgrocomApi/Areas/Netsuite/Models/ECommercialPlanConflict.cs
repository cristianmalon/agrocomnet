﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace AgrocomApi.Areas.Netsuite.Models
{
    public class ECommercialPlanConflict
    {
        public List<ECommercialPlanSync> lstPlnComSync { set; get; }
        public List<ECommercialPlanNoSync> lstPlnComNoSync { set; get; }
        public List<ECommercialPlanSummary> lstPlnSummary { set; get; }
    }

    public class ECommercialPlanNoSync
    {
        public string id { set; get; }
        public string error { set; get; }
        public JObject objJson { set; get; }
        public int idPlan { set; get; }
        public int campaignID { set; get; }
        public string dataPlan { set; get; }
        public string mensaje { set; get; }
    }

    public class ECommercialPlanSync
    {
        public string id { set; get; }
        public string error { set; get; }
        public JObject objJson { set; get; }
        public int idPlan { set; get; }
        public int campaignID { set; get; }
        public string dataPlan { set; get; }
    }

    public class ECommercialPlanCampaign
    {
        public int planCustomerVarietyID { set; get; }
        public int campaignID { set; get; }
    }

    public class ECommercialPlanSummary
    {
        public int campaignID { set; get; }
        public string campaign { set; get; }
        public string cropID { set; get; }
        public string weekIni { set; get; }
        public string weekFin { set; get; }
    }

    public class EPlanSync
    {
        public string internalid { set; get; }
        public int planCustomerVarietyID { set; get; }
    }
}
