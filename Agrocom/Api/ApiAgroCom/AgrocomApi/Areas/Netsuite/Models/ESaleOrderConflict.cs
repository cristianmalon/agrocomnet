﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace AgrocomApi.Areas.Netsuite.Models
{
    public class ESaleOrderConflict
    {
        public List<ESaleOrderSync> lstSOSync { set; get; }
        public List<ESaleOrderNoSync> lstSONoSync { set; get; }
        public List<ESaleOrderSummary> lstSOSummary { set; get; }
    }

    public class ESaleOrderNoSync
    {
        public string id { set; get; }
        public string error { set; get; }
        public JObject objJson { set; get; }
        public int idOS { set; get; }
        public int campaignID { set; get; }
        public int projectedWeekID { set; get; }
        public string requestNumber { set; get; }
        public string dataOS { set; get; }
        public string mensaje { set; get; }
    }

    public class ESaleOrderSync
    {
        public string id { set; get; }
        public string error { set; get; }
        public JObject objJson { set; get; }
        public int idOS { set; get; }
        public int campaignID { set; get; }
        public int projectedWeekID { set; get; }
        public string requestNumber { set; get; }
        public string dataOS { set; get; }
    }

    public class ESaleOrder_RequestNumber
    {
        public int idOS { set; get; }
        public int campaignID { set; get; }
        public int projectedWeekID { set; get; }
        public string requestNumber { set; get; }
    }

    public class ESaleOrderSummary
    {
        public int campaignID { set; get; }
        public string campaign { set; get; }
        public string cropID { set; get; }
        public string weekIni { set; get; }
        public string weekFin { set; get; }
    }

    public class EOrderSync
    {
        public string internalid { set; get; }
        public int orderProductionID { set; get; }
    }
}
