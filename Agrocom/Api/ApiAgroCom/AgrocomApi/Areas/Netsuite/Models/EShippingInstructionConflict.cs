﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrocomApi.Areas.Netsuite.Models
{
    public class EShippingInstructionConflict
    {
        public List<EShippingInstructionNoSync> lstIENoSync { set; get; }
        public List<EShippingInstructionSync> lstIESync { set; get; }
        public List<EShippingInstructionSummary> lstPlnSummary { set; get; }
    }

    public class EShippingInstructionNoSync
    {
        public string id { set; get; }
        public string error { set; get; }
        public JObject objJson { set; get; }
        public int idIE { set; get; }
        public int campaignID { set; get; }
        public string packingNumber { set; get; }
        public string dataIE { set; get; }
        public string mensaje { set; get; }
    }

    public class EShippingInstructionSync
    {
        public string id { set; get; }
        public string error { set; get; }
        public JObject objJson { set; get; }
        public int idIE { set; get; }
        public int campaignID { set; get; }
        public string packingNumber { set; get; }
        public string dataIE { set; get; }
    }

    public class EShippingInstruction_PackingNumber
    {
        public int idIE { set; get; }
        public int campaignID { set; get; }
        public string packingNumber { set; get; }
    }

    public class EShippingInstructionSummary
    {
        public int campaignID { set; get; }
        public string campaign { set; get; }
        public string cropID { set; get; }
        public string weekIni { set; get; }
        public string weekFin { set; get; }
    }

    public class EInstructionSync
    {
        public string internalid { set; get; }
        public int shippingLoadingID { set; get; }
    }
}
