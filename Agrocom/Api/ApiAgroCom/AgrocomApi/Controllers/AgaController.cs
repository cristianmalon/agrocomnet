﻿using System;using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;
using Common.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using Newtonsoft.Json;
using AgrocomApi.Context;
using AgrocomApi.Models;
using Microsoft.EntityFrameworkCore.Storage;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgaController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PODAO poDAO;
        private readonly Models.Aga agaDAO;
        private readonly Models.WeekDAO weekDAO;
        private readonly Models.PackingListPODAO packingListPODAO;

        public AgaController(ApplicationDbContext context)
        {
            this.context = context;
            poDAO = new Models.PODAO(context);
            agaDAO = new Models.Aga(context);
            weekDAO = new Models.WeekDAO(context);
            packingListPODAO = new Models.PackingListPODAO(context);
        }

        [HttpGet("Demo")]
        public ActionResult Demo()
        {

            List<PO> listWeekPending = poDAO.GetAGA("aga", 0);

            foreach (PO item in listWeekPending)
            {
                var client = new RestClient("http://200.37.7.75:8882/COMEX/api/sce/GetControlEmbarque");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", "{\r\n  \"NroPackingList\": \"CAL-ARA 0010/2020\",\r\n  \"Semana\": 34,\r\n  \"IdCampana\": \"20-1\"\r\n}\r\n", ParameterType.RequestBody);
                request.AddParameter("application/json", "{\r\n  \"NroPackingList\": \"\",\r\n  \"Semana\":" + item.week + " ,\r\n  \"IdCampana\": \"" + item.campaing + "\"\r\n}\r\n", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                //var rpt = client.Execute < Dictionary<string,List<AGAControlEmbarque>>>(request);

                var ListRecovery = JsonConvert.DeserializeObject<List<AGAControlEmbarque>>(response.Content);

                foreach (var item2 in ListRecovery)
                {
                    agaDAO.SaveCE(item2);
                }
            }
            //Console.WriteLine(response.Content);
            return Ok("ok");
        }

        [HttpGet("GetPackingListAga")]
        public ActionResult GetPackingListAga()
        {
            List<PK> ListPackingToConsulting = agaDAO.GetAGAPacking("pkl");
            
            foreach(var item in ListPackingToConsulting)
            {
                var client = new RestClient("http://200.37.7.75:8882/COMEX/api/sce/GetPackingList");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", "{\r\n  \"NroPackingList\": \""+ item.nroPackingList + "\"\r\n}\r\n", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                PackingListNisira o = JsonConvert.DeserializeObject<PackingListNisira>(response.Content);

                if (o.id != null)
                {
                    o.NroPackingList = item.nroPackingList;
                    agaDAO.SavePK(o);
                    
                    foreach (var pallet in o.PackingListDetalle)
                    {
                        //exec dao ins pl detalle
                        var x = pallet;
                        agaDAO.SavePKDetail(pallet, item.nroPackingList);
                    }
                }
                                
            }

            return Ok("ok");
        }

        [HttpGet("Real")]
        public ActionResult Real()
        {

            List<Real> listWeek = weekDAO.GetprojectReal("rea", "");

            foreach (Real item in listWeek)
            {
                var client = new RestClient("http://200.37.7.75:8882/COMEX/api/sce/GetAcopios");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", "{\r\n    \"Panio\": " + item.year + ",\r\n    \"Psemana\": " + item.number + "\r\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                //Console.WriteLine(response.Content);

                var ListRecovery = JsonConvert.DeserializeObject<List<projectReal>>(response.Content);

                foreach (var item2 in ListRecovery)
                {                    
                    agaDAO.SaveProjReal(item2,item.year,item.number);                                                                                   
                }
            }
            
            return Ok("ok");
        }

        
    }
}