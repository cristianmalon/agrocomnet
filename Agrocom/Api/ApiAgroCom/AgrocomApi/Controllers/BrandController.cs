﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.BrandDAO brandDAO;

        public BrandController(ApplicationDbContext context)
        {
            this.context = context;
            brandDAO = new Models.BrandDAO(context);
        }

        /// <summary>
        /// Method to list size by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="growerID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListBrandByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string growerID)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            growerID = growerID ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, brandDAO.GetBrand(opt, id, growerID));
        }

        /// <summary>
        /// Method to list category by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListCategoryByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCategoryByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string cropID)
        {
            opt = opt ?? string.Empty;
            cropID = cropID ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, brandDAO.GetCategory(opt, cropID));
        }


    }
}