﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.CategoryDAO categoryDAO;

        public CategoryController(ApplicationDbContext context)
        {
            this.context = context;
            categoryDAO = new Models.CategoryDAO(context);
        }

        /// <summary>
        /// Generic method to list brand by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="gro"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListBrandByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListBrandProductByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string gro)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            gro = gro ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, categoryDAO.GetBrand(opt, id, gro));
        }

        /// <summary>
        /// Generic method to list category by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListCategoryByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCategoryProductByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, categoryDAO.GetCategory(opt, id));
        }
    }
}
