﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;
using AgrocomApi.Filters;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CertificationController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.CertificationDAO certificationDAO;

        public CertificationController(ApplicationDbContext context)
        {
            this.context = context;
            certificationDAO = new Models.CertificationDAO(context);
        }

        /// <summary>
        /// Generic method to list certification by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCertificationByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, certificationDAO.GetCertification(opt, id));
        }
    }
}
