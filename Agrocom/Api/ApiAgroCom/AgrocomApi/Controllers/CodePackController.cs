﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodePackController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.CodePackDao codePackDao;

        public CodePackController(ApplicationDbContext context)
        {
            this.context = context;
            codePackDao = new Models.CodePackDao(context);
        }

        [HttpPost("GetCreate")]
        public ActionResult GetCreate(string description, int? clamshellPerBox, double? weight, int? boxesPerPallet, string layersPerPallet, int? clamshellPerContainer, int? boxPerContainer, double? netWeightContainer, int viaID, int statusID, string cropID)
        {
            return Ok(codePackDao.GetCodePackCreate(description, clamshellPerBox, weight, boxesPerPallet, layersPerPallet, clamshellPerContainer, boxPerContainer, netWeightContainer, viaID, statusID, cropID));
        }
        [HttpPost("GetUpdate")]
        public ActionResult GetUpdate(int codePackID, string description, int? clamshellPerBox, double? weight, int? boxesPerPallet, string layersPerPallet, int? clamshellPerContainer, int? boxPerContainer, double? netWeightContainer)
        {
            return Ok(codePackDao.GetCodePackUpdate(codePackID, description, clamshellPerBox, weight, boxesPerPallet, layersPerPallet, clamshellPerContainer, boxPerContainer, netWeightContainer));
        }

        /// <summary>
        /// Generic method to list codepack by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListPackageByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListPackageProductByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? "";
            id = id ?? "";
            return StatusCode((int)HttpStatusCode.OK, codePackDao.GetCodepack(opt, id));
        }

        /// <summary>
        /// Generic method to list format by crop
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListFormatByCrop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListFormatProductByCrop([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, codePackDao.GetCodepack2(opt, id));
        }

        /// <summary>
        /// Generic method to list format by crop
        /// </summary>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetCodepackSizeByCrop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetCodepackSizeByCrop([Required][FromHeader(Name = "Authorization")] string token, string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, codePackDao.GetCodepackSizeByCrop(cropID));
        }

        /// <summary>
        /// Generic method to list format by crop
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="viaid"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListFormatByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListFormatByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string viaid, string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, codePackDao.GetCodepackVic(opt, viaid, cropID));
        }

        /// <summary>
        /// Generic method to list format by crop
        /// </summary>
        /// <param name="formatID"></param>
        /// <param name="packageProductID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListSizeBoxByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListSizeBoxByFilter([Required][FromHeader(Name = "Authorization")] string token, string formatID, string packageProductID)
        {
            return StatusCode((int)HttpStatusCode.OK, codePackDao.ListSizeBoxByFilter(formatID, packageProductID));
        }

    }
}
