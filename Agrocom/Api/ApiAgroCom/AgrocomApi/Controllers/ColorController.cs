﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColorController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.ColorDAO colorDAO;

        public ColorController(ApplicationDbContext context)
        {
            this.context = context;
            colorDAO = new Models.ColorDAO(context);
        }

        /// <summary>
        /// Method to list size by filter
        /// </summary>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListColorByFilter([Required][FromHeader(Name = "Authorization")] string token, [Required] string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, colorDAO.GetColor(cropID));
        }
    }
}
