﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsigneeController:ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.ConsigneeDAO consigneeDAO;
        public ConsigneeController(ApplicationDbContext context)
        {
            this.context = context;
            consigneeDAO = new Models.ConsigneeDAO(context);
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            return Ok(consigneeDAO.GetConsignee("all", "", ""));
        }

        [HttpGet("GetConsDestByCust")]
        public ActionResult GetConsDestByCust(string customerID, string cropID)
        {
            return Ok(consigneeDAO.GetConsignee("ccd", customerID, cropID));
        }

        [HttpGet("GetSpecsConsDest")]
        public ActionResult GetSpecsConsDest(string customerConsigneeDestinationID)
        {
            return Ok(consigneeDAO.GetConsignee("ccs", customerConsigneeDestinationID, ""));
        }

        [HttpPost("saveConsgineeDestination")]
        public ActionResult saveConsgineeDestination(int consigneeDestinationID, int customerID,int destinationID, int consigneeID, string cropID)
        {
            return Ok(consigneeDAO.GetConsDestCreate(consigneeDestinationID, customerID, destinationID, consigneeID, cropID));
        }

        [HttpPost("saveSpecsConsgineeDestination")]
        public ActionResult saveSpecsConsgineeDestination(int customerConsigneeDestinationSpecsID, int consigneeDestinationID, int varietyID, int formatID, int packageProductID, int generic, string categoryID, int brandID, int presentationID, string sizesID, decimal price, string codepackName, string file, int principal, int statusID, string comments, string notes)
        {
            return Ok(consigneeDAO.GetSpecsConsDestCreate(customerConsigneeDestinationSpecsID, consigneeDestinationID, varietyID, formatID, packageProductID, generic, categoryID, brandID, presentationID, sizesID, price, codepackName, file, principal, statusID, comments, notes));
        }
    }
}
