﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CropController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.CropDAO cropDAO;

        public CropController(ApplicationDbContext context)
        {
            this.context = context;
            cropDAO = new Models.CropDAO(context);
        }

        /// <summary>
        /// Method to get image by crop
        /// </summary>
        /// <param name="idCrop"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetImageByCrop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetImageByCrop([Required][FromHeader(Name = "Authorization")] string token, string idCrop)
        {
            idCrop = idCrop ?? string.Empty;
            return Ok(cropDAO.GetImageByCrop(idCrop));
        }

        /// <summary>
        /// Generic method to list crop by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListCropByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, cropDAO.GetCrop(opt, id));
        }

    }
}
