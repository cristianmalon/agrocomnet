﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using System.Collections.Generic;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.CustomerDAO customerDAO;

        public CustomerController(ApplicationDbContext context)
        {
            this.context = context;
            customerDAO = new Models.CustomerDAO(context);
        }

        [HttpPost("GetCreate")]
        public ActionResult GetCreate(int userID, string name, string address, string contact, string phone, string email, string abbreviation, string idOrigin, string moreInfo, int customer, int consignee, int notify, string ruc, string subsidiaryID, List<CustomerDestination> destinations)
        {
            return Ok(customerDAO.GetCreate(userID, name, address, contact, phone, email, abbreviation, idOrigin, moreInfo, customer, consignee, notify, ruc, subsidiaryID, destinations));
        }

        [HttpPost("GetUpdate")]
        public ActionResult GetUpdate(int userID, string customerID, string name, string address, string contact, string phone, string email, string abbreviation, string moreInfo, int customer, int consignee, int notify, string ruc, string subsidiaryID, List<CustomerDestination> destinations)
        {
            return Ok(customerDAO.GetUpdate(userID, customerID, name, address, contact, phone, email, abbreviation, moreInfo, customer, consignee, notify, ruc, subsidiaryID, destinations));
        }

        [HttpPost("GetCustDestCreate")]
        public ActionResult GetCustDestCreate(string customerDestinationID, string destinationID, string customerID, string cropID)
        {
            return Ok(customerDAO.GetDestCustCreate(customerDestinationID, destinationID, customerID, cropID));
        }

        [HttpPost("GetCustDestDelete")]
        public ActionResult GetCustDestDelete(string destinationID, string customerID)
        {
            return Ok(customerDAO.GetDestCustDelete(destinationID, customerID));
        }

        /// <summary>
        /// Generic method to list customer by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCustomerByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, customerDAO.GetCustomer(opt, id));
        }

        /// <summary>
        /// Generic method to list customer by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="searchName"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter2")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCustomerByFilter2([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string searchName)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            searchName = searchName ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, customerDAO.GetCustomer(opt, id, searchName));
        }
    }
}
