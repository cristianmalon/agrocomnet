﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DestinationController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.DestinationDAO destinationDAO;

        public DestinationController(ApplicationDbContext context)
        {
            this.context = context;
            destinationDAO = new Models.DestinationDAO(context);
        }

        /// <summary>
        /// Generic method to list destination by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="mar"></param>
        /// <param name="via"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListDestinationByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string mar, string via)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            mar = mar ?? string.Empty;
            via = via ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, destinationDAO.GetDestination(opt, id, mar, via));
        }

    }
}
