﻿using AgrocomApi.Context;
using AgrocomApi.Crypto;
using AgrocomApi.Exceptions;
using AgrocomApi.Filters;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace AgrocomApi.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/shared/documents")]
    [ApiController]
    public class DocumentExportController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.DocumentExportDAO documentExportDAO;

        public DocumentExportController(ApplicationDbContext context)
        {
            this.context = context;
            documentExportDAO = new Models.DocumentExportDAO(context);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDocumentExport"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetById")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetById(string idDocumentExport)
        {
            return StatusCode((int)HttpStatusCode.OK, documentExportDAO.GetDocumentExport("id", idDocumentExport));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="description"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetDes")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetDes(string description)
        {
            return Ok(documentExportDAO.GetDocumentExport("des", description));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="description"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetAll")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetAll()
        {
            return Ok(documentExportDAO.GetDocumentExport("all", ""));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetAllPO")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetAllPO(int campaignID)
        {
            return Ok(documentExportDAO.DocumentsExport(campaignID));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderProductionID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetDetailPO")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetDetailPO(string orderProductionID)
        {
            var orderproductionid = int.Parse(AESEncrytDecry.DecryptStringAES(orderProductionID));
            var documentResponse = documentExportDAO.GetDetailPO("pod", orderproductionid);
            var jsonResponse = JsonConvert.SerializeObject(documentResponse);
            return StatusCode((int)HttpStatusCode.OK, AESEncrytDecry.EncryptStringAES(jsonResponse));
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderProductionID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GeDocumentsByPOID")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GeDocumentsByPOID(int orderProductionID)
        {
            return Ok(documentExportDAO.GeDocumentsByPOID("poi", orderProductionID));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("SaveShippingProgramPO")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveShippingProgramPO(ENDocumentOrderProduction o)
        {
            o.orderproductionID = documentExportDAO.SaveShippingProgramPO(o);
            return StatusCode((int)HttpStatusCode.OK, o.orderproductionID);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("SaveShippingDocumentPO")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveShippingDocumentPO(ENDocumentShippingDocumentPO o)
        {
            
            o.orderProductionDocumentID = documentExportDAO.SaveShippingDocumentPO(o);

            return StatusCode((int)HttpStatusCode.OK, o.orderProductionDocumentID);
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <param name="userID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetAllPOForCustomer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetAllPOForCustomer(string campaignID, string userID)
        {
            var campaignid = int.Parse(AESEncrytDecry.DecryptStringAES(campaignID));
            var userid = int.Parse(AESEncrytDecry.DecryptStringAES(userID));
            var dttResponse = documentExportDAO.GetAllPOForCustomer(campaignid, userid);
            var jsonResponse = JsonConvert.SerializeObject(dttResponse);
            return StatusCode((int)HttpStatusCode.OK,AESEncrytDecry.EncryptStringAES(jsonResponse));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("SaveShippingProgramPOforCustomer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveShippingProgramPOforCustomer(ENDocumentOrderProduction o)
        {
            
            o.orderproductionID = documentExportDAO.SaveShippingProgramPOforCustomer(o);

            return Ok(o.orderproductionID);           

        }

        [HttpPost("SaveShippingDocumentPOForCustomer")]
        public ActionResult SaveShippingDocumentPOForCustomer(ENDocumentShippingDocumentPO o)
        {           
            o.orderproductionID = documentExportDAO.SaveShippingDocumentPOForCustomer(o);

            return Ok(o.orderproductionID); 
        }


    }
}