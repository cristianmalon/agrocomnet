﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using AgrocomApi.Filters;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FinanceController:ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.FinanceDAO financeDAO;
        public FinanceController(ApplicationDbContext context)
        {
            this.context = context;
            financeDAO = new Models.FinanceDAO(context);
        }

        [HttpGet("GetPaymentByPOID")]
        public ActionResult GetPaymentByPOID(string orderProductionID)
        {
            return Ok(financeDAO.GetFinancePO("csi", orderProductionID));
        }

        [HttpGet("GetPOFinanceMC")]
        public ActionResult GetPOFinanceMC(int orderProductionID, int campaignID, int userID)
        {
            try
            {
                return Ok(financeDAO.GetFinanceCustomerSettlement(orderProductionID, campaignID, userID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, orderProductionID + "," + campaignID + "," + userID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet("GetDocumentsByPOID")]
        public ActionResult GetDocumentsByPOID(string orderProductionID)
        {
            return Ok(financeDAO.GetFinancePO("dpo", orderProductionID));
            //return Ok(new ENFinanceCustomerInvoice());
        }                

        [HttpGet("GetGrowerSettlementByPOID")]
        public ActionResult GetGrowerSettlementByPOID(string orderProductionID)
        {
            return Ok(financeDAO.GetFinancePO("gsp", orderProductionID));
        }

        [HttpGet("GetGrowerSettlementDetailByID")]
        public ActionResult GetGrowerSettlementDetailByID(string growerSettlementID)
        {
            return Ok(financeDAO.GetFinanceGrowerSettlement("id", growerSettlementID));
        }

        /// <summary>
        /// Method to list documents
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("GetDocumentsDetailByID")]
        [ProducesResponseType(200, Type = typeof(ENFinanceCustomerInvoice))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]        
        public ActionResult GetDocumentsByID(string orderProductionID, int customerInvoiceID)
        {
            var enfinancecustomerinvoice = financeDAO.GetFinanceInvoice("ci", orderProductionID, customerInvoiceID);
            return StatusCode((int)HttpStatusCode.OK,enfinancecustomerinvoice);

        }

        [HttpGet("GetPaymentsDetailByID")]
        public ActionResult GetPaymentsDetailByID(string orderProductionID, int customerPaymentID)
        {
            return Ok(financeDAO.GetFinancePayment("cp", orderProductionID, customerPaymentID));
        }

        /// <summary>
        /// Method to save invoice
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPost("SaveFinanceInvoice")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveGetFinanceInvoice(ENFinanceCustomerInvoice oFinanceCustomerInvoice)
        {
            var customerinvoiceid = financeDAO.CreateUpdateFinanceInvoice(oFinanceCustomerInvoice);
            if (customerinvoiceid == 0)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
            oFinanceCustomerInvoice.customerInvoiceID = customerinvoiceid;
            foreach (var item in oFinanceCustomerInvoice.details)
            {
                financeDAO.CreateUpdateFinanceInvoiceDetail(item, oFinanceCustomerInvoice.customerInvoiceID);
            }
            return StatusCode((int)HttpStatusCode.OK, oFinanceCustomerInvoice.customerInvoiceID);

        }

        [HttpPost("SaveFinancePayment")]
        public ActionResult SaveFinancePayment(ENFinanceCustomerPayments o)
        {
            try
            {
                o.customerPaymentID = financeDAO.CreateUpdateFinancePayment(o);
                if (o.customerPaymentID > 0)
                {
                    foreach (var item in o.details)
                    {
                        financeDAO.CreateUpdateFinancePaymentDetails(item, o.customerPaymentID);
                    }

                }
                return Ok(o.customerPaymentID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [HttpPost("SaveFinanceGrower")]
        public ActionResult SaveFinanceGrower(ENFinanceGrowerSettlement o)
        {
            //try
            {
                o.growerSettlementID = financeDAO.CreateUpdateFinanceGrower(o);
                if (o.growerSettlementID > 0)
                {
                    foreach (var item in o.details)
                    {
                        financeDAO.CreateUpdateFinanceGrowerDetail(item, o.growerSettlementID);
                    }

                }
                return Ok(o.growerSettlementID);
            }
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        [HttpPost("SaveFinanceGrowerWithoutDetail")]
        public ActionResult SaveFinanceGrowerWithoutDetail(ENFinanceGrowerSettlement o)
        {
           try
            {
                o.growerSettlementID = financeDAO.CreateUpdateFinanceGrower(o);
                
                return Ok(o.growerSettlementID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet("GetFinanceGrowerDelete")]
        public ActionResult GetFinanceGrowerDelete(int growerSetlementID)
        {
            return Ok(financeDAO.GetFinanceGrowerDelete(growerSetlementID));
        }

        [HttpPost("SaveFinanceCustomerSettlement")]
        public ActionResult SaveFinanceCustomerSettlement(ENFinanceCustomerSettlement o)
        {
            try
            {
                o.customerSettlementID = financeDAO.CreateUpdateFinanceCustomerSettlement(o);

                if (o.customerSettlementID > 0)
                {
                    foreach (var item in o.details)
                    {
                        financeDAO.CreateUpdateFinanceCustomerSettlementDetail(item, o.customerSettlementID);
                    }

                    foreach (var item in o.detailcosts )
                    {
                        financeDAO.CreateUpdateFinanceCostDistribution(item, o.customerSettlementID);
                    }


                }
                return Ok(o.customerSettlementID);
            }
            catch (Exception e)
            {
                throw e;            
            }
            
        }

        [HttpGet("GetFinanceMC")]
        public ActionResult GetFinanceMC(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin, string periodo, string statusid)
        {
            try
            {
                return Ok(financeDAO.GetFinance(opt, campaignID, nroPackinglist, container, dayini, dayfin, periodo, statusid));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, opt + "," + campaignID + "," + nroPackinglist + "," + container + "," + dayini + "," + dayfin);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet("GetCostDist")]
        public ActionResult GetCostDist(int orderProductionID, int campaignID, int userID)
        {
            try
            {
                return Ok(financeDAO.GetCostDist(orderProductionID, campaignID, userID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, orderProductionID + "," + campaignID + "," + userID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet("GetPackingList")]
        public ActionResult GetPackingList(string orderProductionID)
        {
            try
            {
                return Ok(financeDAO.GetPackingList(orderProductionID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, orderProductionID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

    }
}
