﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using System.Collections.Generic;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForecastController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.ForecastDAO forecastDAO;

        public ForecastController(ApplicationDbContext context)
        {
            this.context = context;
            forecastDAO = new Models.ForecastDAO(context);
        }

        [HttpGet("GetStockByWeek")]
        public ActionResult GetStockByWeek(string growerID, string Weeks)
        {
            return Ok(forecastDAO.GetStockByWeek(growerID, Weeks));
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll(string login, string DateIni, string DateFin, int loteId, string growerId, string categoryId, string sizeId)
        {
            return Ok(forecastDAO.GetSizeComList(login, DateIni, DateFin, loteId, growerId, categoryId, sizeId));
        }

        [HttpGet("GetSave")]
        public ActionResult GetSave(string login, string loteID, string projectedWeekID, string categoryID, int quantityTotal, string commentary, string percentTotalSize_JSON, string percentBySize_JSON)
        {
            return Ok(forecastDAO.GetpercentListUpdate(login, loteID, projectedWeekID, categoryID, quantityTotal, commentary, percentTotalSize_JSON, percentBySize_JSON));
        }

        /// <summary>
        /// Method to list commercial plan by filter
        /// </summary>
        /// <param name="growerId"></param>
        /// <param name="seasonId"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListForecastUnified")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListForecastUnified([Required][FromHeader(Name = "Authorization")] string token, string growerID, string farmID, string cropID, string originID)
        {
            growerID = growerID ?? string.Empty;
            farmID = farmID ?? string.Empty;
            cropID = cropID ?? string.Empty;
            originID = originID ?? string.Empty;
            return Ok(forecastDAO.ListForecastUnified(growerID, farmID, cropID, originID));
        }

        /// <summary>
        /// Method to list commercial plan by filter
        /// </summary>
        /// <param name="growerId"></param>
        /// <param name="seasonId"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListCommercialPlanByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCommercialPlanByFilter([Required][FromHeader(Name = "Authorization")] string token, string growerId, int seasonId)
        {
            growerId = growerId ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.ListForCommercePlan(growerId, seasonId));
        }

        [HttpPost("SaveModify")]
        public ActionResult SaveModify(int userID, int lotID, int projectedWeekID, int quantityTotal, string commentary, List<typecollectionLarge> percentBySize_table)
        {
            return Ok(forecastDAO.PercentSaveUpdate(userID, lotID, projectedWeekID, quantityTotal, commentary, percentBySize_table));
        }
        [HttpPost("PostSaveForecast")]
        public ActionResult PostSaveForecast(int userID, int lotID, int projectedWeekID, decimal quantityTotal, string commentary, List<typecollectionLarge> percentBySize_table)
        {
            try
            {
                return Ok(forecastDAO.SaveForecast(userID, lotID, projectedWeekID, quantityTotal, commentary, percentBySize_table));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName, "");
                return Unauthorized(HttpStatusCode.BadRequest);
            }

        }
        [HttpGet("GetProjectedProductionBySize")]
        public ActionResult GetProjectedProductionBySize(string userID, string dateIni, string dateFin, string growerID)
        {
            return Ok(forecastDAO.GetProjectedProductionBySizeList(userID, dateIni, dateFin, growerID));
        }
        [HttpGet("GetProjecteProductionDetails")]
        public ActionResult GetProjecteProductionDetails(string userID, string dateIni, string dateFin, string growerID)
        {
            return Ok(forecastDAO.GetProjecteProductionDetailsList(userID, dateIni, dateFin, growerID));
        }
        [HttpGet("GetProjectedProductionBySizePercent")]
        public ActionResult GetProjectedProductionBySizePercent(string login, string projectedWeekID, string loteID)
        {
            return Ok(forecastDAO.GetProjectedProductionBySizePercentList(login, projectedWeekID, loteID));
        }
        [HttpPost("SaveMasiveForecast")]
        public ActionResult SaveMasiveForecast(int LotID, string BrandID, string SizeID, int WeekStart, int campaign, float mount1, float mount2, float mount3, float mount4)
        {
            return Ok(forecastDAO.GetSaveMasiveForecast(LotID, BrandID, SizeID, WeekStart, campaign, mount1, mount2, mount3, mount4));
        }

        [HttpGet("GetForecastBySaleID")]
        public ActionResult GetForecastBySaleID(int saleID)
        {
            return Ok(forecastDAO.GetForecastBySaleID(saleID));
        }

        /// <summary>
        /// Method to list forecast by sale
        /// </summary>
        /// <param name="saleID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListForecastBySaleID")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListForecastBySaleID([Required][FromHeader(Name = "Authorization")] string token, int saleID)
        {
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetForecastBySaleID(saleID));
        }

        [HttpGet("GetVariationFor")]
        public ActionResult GetVariationFor()
        {
            return Ok(forecastDAO.GetVariationFor());
        }

        [HttpGet("GetFirstForecast")]
        public ActionResult GetFirstForecast()
        {
            return Ok(forecastDAO.GetReportForecast("ffo"));
        }

        [HttpGet("GetCurrentlyForecast")]
        public ActionResult GetCurrentlyForecast()
        {
            return Ok(forecastDAO.GetReportForecast("for"));
        }

        [HttpGet("GetRealForecast")]
        public ActionResult GetRealForecast()
        {
            return Ok(forecastDAO.GetReportForecast("rea"));
        }

        [HttpGet("GetForecastByCropID")]
        public ActionResult GetForecastByCropID(string cropID, int seasonID)
        {
            return Ok(forecastDAO.GetForecastByCropID(cropID, seasonID));
        }

        /// <summary>
        /// Method to list forecast by filter
        /// </summary>
        /// <param name="projectedWeekID"></param>
        /// <param name="loteID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListForecastByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListForecastByFilter([Required][FromHeader(Name = "Authorization")] string token, [Required]int projectedWeekID, [Required]int loteID)
        {
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetforecastList(projectedWeekID, loteID));
        }

        /// <summary>
        /// Method to list form's title by filter
        /// </summary>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListTitleFormByCrop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListTitleFormByCrop([Required][FromHeader(Name = "Authorization")] string token, [Required]string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetTitleFormByCropID(cropID));
        }

        /// <summary>
        /// Method to list massive forecast by filter
        /// </summary>
        /// <param name="growerID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListMasiveByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListMasiveForecastByFilter([Required][FromHeader(Name = "Authorization")] string token, string growerID, string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetForecastMasive(growerID, cropID));
        }

        /// <summary>
        /// Method to list projected production by filter
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="dateIni"></param>
        /// <param name="dateFin"></param>
        /// <param name="campaignID"></param>
        /// <param name="farmID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListProjectedProductionByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListProjectedProductionByFilter([Required][FromHeader(Name = "Authorization")] string token, string userID, string dateIni, string dateFin, int campaignID, string farmID)
        {
            userID = userID ?? string.Empty;
            dateIni = dateIni ?? string.Empty;
            dateFin = dateFin ?? string.Empty;
            farmID = farmID ?? "";
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetProjectedProductionByVarietyList(userID, dateIni, dateFin, campaignID, farmID));
        }

        /// <summary>
        /// Method to list percent by filter
        /// </summary>
        /// <param name="login"></param>
        /// <param name="projectedWeekID"></param>
        /// <param name="loteID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListPercentByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListPercentByFilter([Required][FromHeader(Name = "Authorization")] string token, [Required]string login, [Required] int projectedWeekID, [Required] int loteID)
        {
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetpercentList(login, projectedWeekID, loteID));
        }

        /// <summary>
        /// Method to save forecast by sale
        /// </summary>
        /// <param name="objMassiveUpload"></param>
        /// <param name="userID"></param>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("SaveMasiveUpload")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveMasiveUpload([Required][FromHeader(Name = "Authorization")] string token, MassiveUploadCab objMassiveUpload, string userID, int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.SaveMasiveUploadForecast(objMassiveUpload, userID, campaignID));
        }

        /// <summary>
        /// Method to list forecast by sale
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListBySale")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListForecastBySale([Required][FromHeader(Name = "Authorization")] string token, string opt, int campaignID)
        {
            opt = opt ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, forecastDAO.GetForecastBySales(opt, campaignID));
        }

    }

}
