﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GrowerController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.GrowerDAO growerDAO;

        public GrowerController(ApplicationDbContext context)
        {
            this.context = context;
            growerDAO = new Models.GrowerDAO(context);
        }

        /// <summary>
        /// Generic method to list format by crop
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListGrowerByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string cropID)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            cropID = cropID ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, growerDAO.GetGrower(opt, id, cropID));
        }

    }
}