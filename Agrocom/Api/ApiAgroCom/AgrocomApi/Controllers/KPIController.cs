﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Mapper;
using Common.KPI;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KPIController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.KPI.KpiDAO _kpiDAO;

        public KPIController(ApplicationDbContext context)
        {
            this.context = context;
            _kpiDAO = new Models.KPI.KpiDAO(context);
        }

        /// <summary>
        /// Generic method to list KPIs
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListKPIsSplash")]
        [ProducesResponseType(200, Type = typeof(EKpi))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListKPIs([Required][FromHeader] string cropID)
        {
            EKpi objKPI;
            if (!_kpiDAO.GetKpiSplash(cropID, out objKPI))
            {
                return StatusCode((int)HttpStatusCode.NoContent, "");
            }
            var objkpiresponse = LogicEntityMapper.Mapper.Map<EKpiResponsive>(objKPI);

            return StatusCode((int)HttpStatusCode.OK, objkpiresponse);
        }
    }
}