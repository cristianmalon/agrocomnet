﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LotController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.LotDAO lotDAO;

        public LotController(ApplicationDbContext context)
        {
            this.context = context;
            lotDAO = new Models.LotDAO(context);
        }

        /// <summary>
        /// Method to list lot by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="idstage"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListLotByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string idstage)
        {
            opt = opt ?? string.Empty;
            idstage = idstage ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, lotDAO.GetLot(opt, idstage));
        }

        /// <summary>
        /// Method to list lot by stage
        /// </summary>
        /// <param name="stageID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByStage")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListLotByFilter([Required][FromHeader(Name = "Authorization")] string token, string stageID)
        {
            return StatusCode((int)HttpStatusCode.OK, lotDAO.ListLotByFilter(stageID));
        }
    }
}
