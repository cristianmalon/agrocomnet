﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.MarketDAO marketDAO;

        public MarketController(ApplicationDbContext context)
        {
            this.context = context;
            marketDAO = new Models.MarketDAO(context);
        }

        [HttpGet("GetUpdate")]
        public ActionResult GetUpdate(string marketID, string name, string abbreviation, string percentage, int statusID)
        {
            return Ok(marketDAO.GetMarketUpdate(marketID, name, abbreviation, percentage, statusID));
        }
        [HttpGet("GetMarketCodepackCreate")]
        public ActionResult GetMarketCodepackCreate(string marketID, string codepackID)
        {
            return Ok(marketDAO.GetMarketCodepackCreate(marketID, codepackID));

        }
        [HttpGet("GetMarketCodepackDisable")]
        public ActionResult GetMarketCodepackDisable(string marketID, string codepackID)
        {
            return Ok(marketDAO.GetMarketCodepackDisable(marketID, codepackID));
        }

        /// <summary>
        /// Generic method to list market by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListMarketByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, marketDAO.GetMarket(opt, id));
        }

    }
}
