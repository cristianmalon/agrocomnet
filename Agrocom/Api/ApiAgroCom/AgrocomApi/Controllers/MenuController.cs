﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.MenuDAO menuDAO;

        public MenuController(ApplicationDbContext context)
        {
            this.context = context;
            menuDAO = new Models.MenuDAO(context);
        }

        /// <summary>
        /// Generic method to list optionMenu by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="val"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListMenuByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string val, string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, menuDAO.GetMenu(opt, val, cropID));
        }
    }
}