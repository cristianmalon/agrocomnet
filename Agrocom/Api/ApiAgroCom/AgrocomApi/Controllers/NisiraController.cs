﻿using System;
using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NisiraController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PackingListDAO packingListDAO;
        private readonly IConfiguration _configuration;

        public NisiraController(ApplicationDbContext context, IConfiguration configuration)
        {
            this.context = context;
            packingListDAO = new Models.PackingListDAO(context);
            _configuration = configuration;
        }

        [HttpGet("GetNisira")]
        public ActionResult GetNisira(string campana, string nroPacking)
        {
            try
            {
                var url = _configuration.GetValue<string>("UrlSispacking");
                var client = new RestClient(url + "/COMEX/api/sce/GetLiqVentasExterior");
                Packing packing = new Packing();
                packing.CAMPANA = campana;
                packing.NROPACKINGLIST = nroPacking;

                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                var sz = JsonConvert.SerializeObject(packing);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", sz, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string result = response.Content;

                ENFinanceNisira finance = JsonConvert.DeserializeObject<ENFinanceNisira>(result.Substring(1, result.Length - 2));
                return Ok(result);
            }
            catch (Exception)
            {
                //throw;
                return Ok(0);
            }

        }
    }
    public class Packing
    {
        public Packing()
        {
            this.CAMPANA = CAMPANA;
            this.NROPACKINGLIST = NROPACKINGLIST;
        }

        public string CAMPANA { get; set; }
        public string NROPACKINGLIST { get; set; }

    }
}
