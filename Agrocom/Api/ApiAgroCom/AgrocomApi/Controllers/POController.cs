﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System;
using System.Net;
using System.Collections.Generic;
using AgrocomApi.Filters;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class POController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PODAO poDAO;

        public POController(ApplicationDbContext context)
        {
            this.context = context;
            poDAO = new Models.PODAO(context);
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll(int WIni, int WFin)
        {
            return Ok(poDAO.GetAll(WIni, WFin));
        }

        [HttpGet("GetListForConfirm")]
        public ActionResult GetListForConfirm(int WIni, int WFin, int userID)
        {
            return Ok(poDAO.GetListForConfirm(WIni, WFin, userID));
        }

        [HttpGet("GetDetailConfirmById")]
        public ActionResult GetDetailConfirmById(int IdPO)
        {
            return Ok(poDAO.GetDetailConfirmById("Det", IdPO));
        }
        [HttpGet("GetSalesPendingByWeek")]
        public ActionResult GetSalesPendingByWeek(int weekID)
        {
            return Ok(poDAO.GetSalesPendingByWeek(weekID));
        }

        [HttpGet("GetVarietyByForecast")]
        public ActionResult GetVarietyByForecast(string saleDetailID)
        {
            return Ok(poDAO.GetVarietyForecast(saleDetailID));
        }

        [HttpGet("GetVarietyByForecastDetail")]
        public ActionResult GetVarietyByForecastDetail(string growerID, string farmID, string saleDetailID)
        {
            return Ok(poDAO.GetVarietyForecastDetail(growerID, farmID, saleDetailID));
        }
        [HttpGet("ENGetVarietyForecastDetail")]
        public ActionResult ENGetVarietyForecastDetail(string growerID, string farmID, string saleDetailID)
        {
            return Ok(poDAO.ENGetVarietyForecastDetail(growerID, farmID, saleDetailID));
        }
        [HttpGet("ENGetVarietyForecastDetailForPOEdit")]
        public ActionResult ENGetVarietyForecastDetailForPOEdit(int saleDetailID)
        {
            return Ok(poDAO.ENGetVarietyForecastDetailForPOEdit("EDI", "", saleDetailID));
        }

        [HttpPost("GetCreateUpdatePO")]
        public ActionResult GetCreateUpdatePO(int orderProductionID, string projectedweekID, string saleID, string specificationID, string commentary, string userCreated, int campaignID)
        {
            try
            {
                return Ok(poDAO.CreateUpdateProductionOrder(orderProductionID, projectedweekID, saleID, specificationID, commentary, userCreated, campaignID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, orderProductionID + "," + projectedweekID + "," + saleID + "," + specificationID + "," + commentary + "," + userCreated + "," + campaignID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }
        [HttpPost("GetCreateUpdatePODetail")]
        public ActionResult GetCreateUpdatePODetail(string orderProductionDetailID, string orderProductionID, string saleDetailID, string projectedProductionDetailID, string Quantity, string userCreated, string sizeID)
        {
            try
            {
                return Ok(poDAO.CreateUpdateProductionOrderDetail(orderProductionDetailID, orderProductionID, saleDetailID, projectedProductionDetailID, Quantity, userCreated, sizeID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, orderProductionDetailID + "," + orderProductionID + "," + saleDetailID + "," + projectedProductionDetailID + "," + Quantity + "," + userCreated + "," + sizeID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet("GetConfirmById")]
        public ActionResult GetConfirmById(int IdPO)
        {
            return Ok(poDAO.GetConfirmById("det", IdPO));
        }

        [HttpGet("GetALLPO")]
        public ActionResult GetALLPO()
        {
            return Ok(poDAO.GetConfirmById("GetAllPO", 0));
        }

        [HttpPost("OrderProductionConfirm")]
        public ActionResult OrderProductionConfirm(int orderProductionID, string statusConfirm, string dateBoardEstimate, List<typecollection> quantityConfirm, int userID, string reason)
        {
            return Ok(poDAO.OrderProductionConfirm(orderProductionID, statusConfirm, dateBoardEstimate, quantityConfirm, userID, reason));
        }

        [HttpGet("GetPOrej")]
        public ActionResult GetPOrej(int orderProductionID)
        {
            return Ok(poDAO.GetPOrejected(orderProductionID));
        }

        [HttpGet("GetPOpen")]
        public ActionResult GetPOpen()
        {
            return Ok(poDAO.GetPOpending());
        }

        [HttpGet("GetPOnotAt")]
        public ActionResult GetPOnotAt()
        {
            return Ok(poDAO.GetPOnotAttended());
        }

        [HttpGet("GetDetailForPOEdit")]
        public ActionResult GetDetailForPOEdit(int IdPO)
        {
            return Ok(poDAO.GetDetailConfirmById("DTE", IdPO));
        }

        [HttpGet("GetPOForImportIE")]
        public ActionResult GetPOForImportIE()
        {
            return Ok(poDAO.GetConfirmById("aga", 0));
        }

        [HttpGet("POModify")]
        public ActionResult POModify(int orderProductionID, int userID, int statusID)
        {
            return Ok(poDAO.POModify(orderProductionID, userID, statusID));
        }

        [HttpGet("ENGetVarietyForecastDetailForPONew")]
        public ActionResult ENGetVarietyForecastDetailForPOEdit(string growerID, int saleDetailID)
        {
            return Ok(poDAO.ENGetVarietyForecastDetailForPOEdit("NEW", growerID, saleDetailID));
        }

        [HttpGet("GetAllForEditFiltered")]
        public ActionResult GetAllForEditFiltered(string marketID, int weekini, int weekFin, string statusID, string growerID, int campaignID)
        {
            return Ok(poDAO.POfilterList("det", marketID, weekini, weekFin, statusID, growerID, campaignID));
        }

        [HttpGet("GetMarketAll")]
        public ActionResult GetMarketAll()
        {
            return Ok(poDAO.POfilterList("MAR", "", 0, 0, "", "", 0));
        }

        [HttpGet("GetGrowerAll")]
        public ActionResult GetGrowerAll(int campaignID)
        {
            return Ok(poDAO.POfilterList("GRO", "", 0, 0, "", "", campaignID));
        }

        [HttpGet("GetStatus")]
        public ActionResult GetStatus(int campaignID)
        {
            return Ok(poDAO.POfilterList("STA", "", 0, 0, "", "", campaignID));
        }

        [HttpGet("GetPOConfirm")]
        public ActionResult GetPOConfirm(string plantID, int weekProduction, int weekProductionEnd, int statusConfirmID, int campaignID, int userID)
        {
            return Ok(poDAO.GetPOConfirm("lis", plantID, weekProduction, weekProductionEnd, statusConfirmID, campaignID, userID));
        }
        [HttpGet("GetPOConfirmResumen")]
        public ActionResult GetPOConfirmResumen(string plantID, int campaignID)
        {
            return Ok(poDAO.GetPOConfirm("res", plantID, 0, 0, 0, campaignID, 0));
        }

        /// <summary>
        /// Confirm sale order
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPost("OrderProductionConfirmStatus")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]        
        public ActionResult OrderProductionConfirmStatus(string cropID, int orderProductionID, int statusConfirmID,
            string completedDate, string loadingDate, decimal vgm, string day, string userID, string comments,
            string packingID, int editAttemptsPacking, int PromiseCompliance, OrderProductionCab objOrderProduction)
        {
            return Ok(poDAO.OrderProductionConfirmStatus(cropID, orderProductionID, statusConfirmID,
                completedDate, loadingDate, vgm, day, userID, comments,
                packingID, editAttemptsPacking, PromiseCompliance, objOrderProduction));
        }

        /// <summary>
        /// Save sale order modified
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">No Content - Respuesta en blanco</response> 
        /// <response code="400">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpPost("SavePOModified")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]        
        public ActionResult SavePOModified(ENPOModified o)
        {
            return Ok(poDAO.SavePOModified(o));
        }

        [HttpPost("POGenerateGrapes")]
        public ActionResult POGenerateGrapes(ENPOGenerate o)
        {
            return Ok(poDAO.POGenerateGrapes(o));
        }

        [HttpGet("GetBoardingProgramming")]
        public ActionResult GetBoardingProgramming(int customerID, int weekID, int destinationID, int statusID, int campaignID)
        {
            return Ok(poDAO.GetBoardingProgramming("lis", customerID, weekID, destinationID, statusID, campaignID));
        }

        [HttpGet("GetBoardingStatus")]
        public ActionResult GetBoardingStatus(int campaignID)
        {
            return Ok(poDAO.GetBoardingProgramming("stc", 0, 0, 0, 0, campaignID));
        }

        [HttpGet("GetPOstatusConfirm")]
        public ActionResult GetPOstatusConfirm(int statusConfirmID, int campaignID)
        {
            return Ok(poDAO.GetPOConfirm("act", "", 0, 0, statusConfirmID, campaignID, 0));
        }

        [HttpGet("GetDestinationByCampaign")]
        public ActionResult GetDestinationByCampaign(int campaignID)
        {
            return Ok(poDAO.GetBoardingProgramming("des", 0, 0, 0, 0, campaignID));
        }

        [HttpGet("GetStatusQuan")]
        public ActionResult GetStatusQuan(int customerID, int weekID, int destinationID, int campaignID)
        {
            return Ok(poDAO.GetBoardingProgramming("stq", customerID, weekID, destinationID, 0, campaignID));
        }

        [HttpPost("GetUpdStatus")]
        public ActionResult GetUpdStatus(int orderProductionID)
        {
            return Ok(poDAO.GetBoardingProgramming("upd", orderProductionID, 0, 0, 0, 0));
        }

        [HttpGet("GetListPOReview")]
        public ActionResult GetListPOReview(string marketID, int weekini, int weekFin, string statusID, string growerID, string campaignID)
        {
            return Ok(poDAO.POfilterList("RVW", marketID, weekini, weekFin, statusID, growerID, int.Parse(campaignID)));
        }

        [HttpPost("GetPObyPlant")]
        public ActionResult GetPObyPlant(int userID, ENPObyPlant PObyPlantt)
        {
            return Ok(poDAO.GetPObyPlant(userID, PObyPlantt));

        }

        /// <summary>
        /// Method to list production orders confirmed by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="plantID"></param>
        /// <param name="weekProduction"></param>
        /// <param name="statusConfirmID"></param>
        /// <param name="campaignID"></param>
        /// <param name="userID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListPOByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListPOByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string plantID, int weekProduction, int weekProductionEnd, int? statusConfirmID, int campaignID, int userID)
        {
            plantID = plantID ?? "";
            statusConfirmID = statusConfirmID ?? 0;
            return Ok(poDAO.GetPOConfirm(opt, plantID, weekProduction, weekProductionEnd, statusConfirmID, campaignID, userID));
        }

        /// <summary>
        /// Method to list production orders confirmed by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="orderProductionID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListPOconfirmedByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListPOconfirmedByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, int orderProductionID)
        {
            opt = opt ?? string.Empty;
            return Ok(poDAO.GetConfirmById(opt, orderProductionID));
        }

        [HttpGet("GetPOFilterMC")]
        public ActionResult GetPOFilterMC(string opt, string marketID, int weekini, int weekFin, string statusID, string growerID, int campaignID)
        {
            try
            {
                return Ok(poDAO.POfilterList(opt, marketID, weekini, weekFin, statusID, growerID, campaignID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, opt + "," + marketID + "," + weekini + "," + weekFin + "," + statusID + "," + growerID + "," + campaignID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Method to list forecast by production order
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="growerID"></param>
        /// <param name="saleDetailID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ENGetForecastForPOMC")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ENGetForecastForPOMC([Required][FromHeader(Name = "Authorization")] string token, string opt, string growerID, int saleDetailID)
        {
            growerID = growerID ?? string.Empty;
            return Ok(poDAO.ENGetVarietyForecastDetailForPOEdit(opt, growerID, saleDetailID));
        }

        [HttpGet("GetDetailConfirmByIdMC")]
        public ActionResult GetDetailConfirmById_MC(string Option, int IdPO)
        {
            try
            {
                return Ok(poDAO.GetDetailConfirmById(Option, IdPO));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "Option=" + Option + "," + "IdPO=" + IdPO);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Method to get information of production order
        /// </summary>
        /// <param name="IdPO"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetPOInfoForPdf")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult GetPOInfoForPdf([Required][FromHeader(Name = "Authorization")] string token, int IdPO)
        {
            return Ok(poDAO.GetForPdf(IdPO));
        }

        /// <summary>
        /// Method to verify if sale order is in process
        /// </summary>
        /// <param name="orderProduction"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("OrderInProcess")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult VerifySaleOrderInProcess(string correlativeSaleOrder)
        {
            return Ok(poDAO.VerifySaleOrderInProcess(correlativeSaleOrder));
        }

    }
}