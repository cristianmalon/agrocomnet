﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackingListController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PackingListDAO packingListDAO;

        public PackingListController(ApplicationDbContext context)
        {
            this.context = context;
            packingListDAO = new Models.PackingListDAO(context);
        }

        [HttpGet("GetById")]
        public ActionResult GetById(string idPackingList)
        {
            return Ok(packingListDAO.GetPackingList("id", idPackingList));
        }

        [HttpGet("GetDes")]
        public ActionResult GetDes(string description)
        {
            return Ok(packingListDAO.GetPackingList("des", description));
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            return Ok(packingListDAO.GetPackingList("all", ""));
        }

        [HttpPost("SaveQA")]
        public ActionResult SaveQA(ENPackingList o)
        {
            try
            {
                o.packingListID = packingListDAO.SavePackingListQA(o);
                if (o.packingListID > 0)
                {
                    foreach (var item in o.packingListDetails)
                    {
                        packingListDAO.SavePackingListDetailQA(item);
                    }
                };
                return Ok(o.packingListID);
            }
            catch(Exception e)
            {
                throw e;
                return Ok(0);
            }
            
        }

        [HttpGet("GetAllForQA")]
        public ActionResult GetAllForQA()
        {
            return Ok(packingListDAO.GetForQA("all", 0));
        }

        [HttpGet("GetByIDForQA")]
        public ActionResult GetByIDForQA(int orderProductionID)
        {
            return Ok(packingListDAO.GetForQA("ID", orderProductionID));
        }
    }
}
