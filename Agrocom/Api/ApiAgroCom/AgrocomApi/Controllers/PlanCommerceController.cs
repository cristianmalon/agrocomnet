﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;
using Common.CommercialPlan;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanCommerceController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PlanCommerceDAO planCommerceDAO;

        public PlanCommerceController(ApplicationDbContext context)
        {
            this.context = context;
            planCommerceDAO = new Models.PlanCommerceDAO(context);
        }

        [HttpPost("Save")]
        public ActionResult Save(string planID, string plan, string campaignID, string userID)
        {
            return Ok(planCommerceDAO.SavePlan(planID, plan, campaignID, userID));
        }

        /// <summary>
        /// Generic method to list crop by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("SavePlanOriginGrower")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SavePlanOriginGrower([Required][FromHeader(Name = "Authorization")] string token, string planID, string plan, string campaignID, string originID, string growerID, string nonExport, string userID)
        {
            return StatusCode((int)HttpStatusCode.OK, planCommerceDAO.SavePlanOriginGrower(planID, plan, campaignID, originID, growerID, nonExport, userID));
        }

        [HttpPost("SavePlanCustomerRequest")]
        public ActionResult SavePlanCustomerRequest(string planCustomerRequestID, string planCustomerRequest, string planID, string customerID, string destinationID, string codepackID, string marketID, string userID, string cropID)
        {
            return Ok(planCommerceDAO.SavePlanCustomerRequest(planCustomerRequestID, planCustomerRequest, planID, customerID, destinationID, codepackID, marketID, userID, cropID));
        }

        [HttpPost("SavePlanCustomerRequestWeek")]
        public ActionResult SavePlanCustomerRequestWeek(string planCustomerRequestWeekID, string planCustomerRequestID, string projectedWeekID, string amount, string userID)
        {
            return Ok(planCommerceDAO.SavePlanCustomerRequestWeek(planCustomerRequestWeekID, planCustomerRequestID, projectedWeekID, amount, userID));
        }

        [HttpPost("SavePlanAlternative")]
        public ActionResult SavePlanAlternative(string planAlternativeID, string planAlternative, string planOriginID, string userID)
        {
            return Ok(planCommerceDAO.SavePlanAlternative(planAlternativeID, planAlternative, planOriginID, userID));
        }

        [HttpPost("SaveCustomerFormat")]
        public ActionResult SaveCustomerFormat(string codepackID, string alternativeID, string marketID, int destinationID, int customerID, int priorityID, int wildcard, int order, string userID)
        {
            return Ok(planCommerceDAO.SaveCustomerFormat(codepackID, alternativeID, marketID, destinationID, customerID, priorityID, wildcard, order, userID));
        }

        [HttpPost("SavePlanCustomerWeek")]
        public ActionResult SavePlanCustomerWeek(string planCustomerWeekID, string planCustomerFormatID, string projectedWeekID, string amount, string userID)
        {
            return Ok(planCommerceDAO.SavePlanCustomerWeek(planCustomerWeekID, planCustomerFormatID, projectedWeekID, amount, userID));
        }

        [HttpGet("ListPlanCustomerWeekByCustomer")]
        public ActionResult ListPlanCustomerWeekByCustomer(int planCustomerFormatID)
        {
            return Ok(planCommerceDAO.ListPlanCustomerWeek("cid", planCustomerFormatID));
        }

        [HttpPost("CustomerRequestDelete")]
        public ActionResult CustomerRequestDelete(int planCustomerRequestID, int userID)
        {
            return Ok(planCommerceDAO.CustomerRequestDelete(planCustomerRequestID, userID));
        }

        [HttpPost("PlanCustomerDelete")]
        public ActionResult PlanCustomerDelete(int planCustomerFormatID, int userID)
        {
            return Ok(planCommerceDAO.PlanCustomerDelete(planCustomerFormatID, userID));
        }


        [HttpPost("PlanCustomerVariety")]
        public ActionResult PlanCustomerVariety(ENPlanCustomerVariety o)
        {
            return Ok(planCommerceDAO.PlanCustomerVariety(o));
        }

        [HttpPost("RegisterCommercialPlanMultiVarietySize")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult InsertCommercialPlanMultiVarietySize(ENPlanCustomerBrand o)
        {
            var dtt = planCommerceDAO.RegisterCommercialPlanWithMultipleVarietiesAndSizes(o);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetPlanCustomerVariety")]
        public ActionResult GetPlanCustomerVariety(string campaignID)
        {
            try
            {
                return Ok(planCommerceDAO.GetPlanCustomerVariety(int.Parse(campaignID), "cam", 0));
            }
            catch (System.Exception ex)
            {

                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var sCurrentMethodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, sCurrentMethodName, "");//, "opt=" + opt + "," + "id=" + id + "," + "cropId=" + cropId + "," + "seasonId=" + seasonId + "," + "originId=" + originId + "growerId=" + growerId);
                return null;
            }
                    
            
        }
        
        [HttpGet("GetPlanCustomerByFilters")]
        public ActionResult GetPlanCustomerByFilters(string campaignID, int customerID, int destinationID, int programID, string categoryID, int varietyID)
        {
            return Ok(planCommerceDAO.GetPlanCustomerByFilters(int.Parse(campaignID), customerID, destinationID, programID, categoryID, varietyID));
        }

        [HttpGet("GetPlanCustomerVarietyByWeekID")]
        public ActionResult GetPlanCustomerVariety(string campaignID, int projectedWeekID)
        {
            return Ok(planCommerceDAO.GetPlanCustomerVariety(int.Parse(campaignID), "wek", projectedWeekID));
        }
        [HttpPost("CreateSaleRequestAndPO")]
        public ActionResult CreateSaleRequestAndPO(string cropId, int campaignID, int userCreated, int projectedWeekID, PlanComercialCab objPlanComercial)
        {
            return Ok(planCommerceDAO.CreateSaleRequestAndPO(cropId, campaignID, userCreated, projectedWeekID, objPlanComercial));
        }

        [HttpPost("CreateSaleRequestAndPOBLU")]
        public ActionResult CreateSaleRequestAndPOBLU(string cropId, int campaignID, int userCreated, int projectedWeekID, PlanComercialBlu objPlanComercial)
        {
           return Ok(planCommerceDAO.CreateSaleRequestAndPOBLU(cropId, campaignID, userCreated, projectedWeekID, objPlanComercial));
          
        }

        [HttpGet("ListPlanAlternativeMC")]
        public ActionResult ListPlanAlternative_MC(string opt, int planOriginID)
        {
            return Ok(planCommerceDAO.ListPlanAlternative(opt, planOriginID));
        }

        [HttpGet("GetPlanBlueberryByFilters")]
        public ActionResult GetPlanBlueberryByFilters(int campaignID, int customerID, int destinationID, int programID, string brandID, int viaID, int formatID)
        {
            return Ok(planCommerceDAO.GetPlanBlueberryByFilters(campaignID, customerID, destinationID, programID, brandID, viaID, formatID));
        }

        /// <summary>
        /// Generic method to list plan by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="cropId"></param>
        /// <param name="seasonId"></param>
        /// <param name="originId"></param>
        /// <param name="growerId"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListPlanByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string cropId, string seasonId, string originId, string growerId)
        {
            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            cropId = cropId ?? string.Empty;
            seasonId = seasonId ?? string.Empty;
            originId = originId ?? string.Empty;
            growerId = growerId ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, planCommerceDAO.GetPlan(opt, id, cropId, seasonId, originId, growerId));
        }

        /// <summary>
        /// Generic method to list customer by filter
        /// </summary>
        /// <param name="searchOpt"></param>
        /// <param name="planID"></param>
        /// <param name="marketID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListCustomerRequestByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCustomerRequestByFilter([Required][FromHeader(Name = "Authorization")] string token, string searchOpt, string planID, string marketID)
        {
            searchOpt = searchOpt ?? string.Empty;
            planID = planID ?? string.Empty;
            marketID = marketID ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK, planCommerceDAO.ListCustomerRequest(searchOpt, planID, marketID));
        }

        /// <summary>
        /// Generic method to list alternative plan by filter
        /// </summary>
        /// <param name="searchOpt"></param>
        /// <param name="planOriginID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListPlanAlternativeByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListPlanAlternativeByFilter([Required][FromHeader(Name = "Authorization")] string token, [Required]string searchOpt, [Required]int planOriginID)
        {
            return StatusCode((int)HttpStatusCode.OK, planCommerceDAO.ListPlanAlternative(searchOpt, planOriginID));
        }

        /// <summary>
        /// Generic method to list plan customer by filter 
        /// </summary>
        /// <param name="searchOpt"></param>
        /// <param name="planAlternativeID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListPlanCustomerByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListPlanCustomerByFilter([Required][FromHeader(Name = "Authorization")] string token, [Required]string searchOpt, [Required]int planAlternativeID)
        {
            return StatusCode((int)HttpStatusCode.OK, planCommerceDAO.ListPlanCustomer(searchOpt, planAlternativeID));
        }


        /// <summary>
        /// Generic method to list customer request week by filter 
        /// </summary>
        /// <param name="searchOpt"></param>
        /// <param name="planCustomerRequestID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListCustomerRequestWeekByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListCustomerRequestWeekByFilter([Required][FromHeader(Name = "Authorization")] string token, string searchOpt, int planCustomerRequestID)
        {
            return StatusCode((int)HttpStatusCode.OK, planCommerceDAO.ListCustomerRequestWeek(searchOpt, planCustomerRequestID));
        }
    }
}