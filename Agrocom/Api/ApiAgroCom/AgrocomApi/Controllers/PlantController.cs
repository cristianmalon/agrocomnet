﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlantController:ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PlantDAO plantDAO;

        public PlantController(ApplicationDbContext context)
        {
            this.context = context;
            plantDAO = new Models.PlantDAO(context);
        }

        [HttpGet("GetById")]
        public ActionResult GetById(string plantID)
        {
            return Ok(plantDAO.GetPlant("id", plantID));
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            return Ok(plantDAO.GetPlant("all", ""));
        }

        [HttpGet("GetPlantMC")]
        public ActionResult GetPlantMC(string opt, string id)
        {
            try
            {
                return Ok(plantDAO.GetPlant(opt,id));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName, "");
                //, "cropID=" + cropID + "," + "growerID=" + growerID + "," + "farmID=" + farmID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }
    }
}
