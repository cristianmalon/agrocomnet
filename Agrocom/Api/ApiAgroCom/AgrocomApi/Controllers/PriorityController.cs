﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriorityController:ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PriorityDAO priorityDAO;

        public PriorityController(ApplicationDbContext context)
        {
            this.context = context;
            priorityDAO = new Models.PriorityDAO(context);
        }

        [HttpGet("GetById")]
        public ActionResult GetById(string idPriority)
        {
            return Ok(priorityDAO.GetPriority("id", idPriority));
        }

        [HttpGet("GetDes")]
        public ActionResult GetDes(string description)
        {
            return Ok(priorityDAO.GetPriority("des", description));
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            return Ok(priorityDAO.GetPriority("all", ""));
        }

        [HttpGet("GetMC")]
        public ActionResult GetAll(string opt, string value)
        {
            return Ok(priorityDAO.GetPriority(opt, value));
        }
    }
}
