﻿using AgrocomApi.Filters;
using Microsoft.AspNetCore.Mvc;
using AgrocomApi.Context;
using Common.Entities;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.ReportsDAO reportsDAO;
        
        public ReportsController(ApplicationDbContext context)
        {
            this.context = context;
            reportsDAO = new Models.ReportsDAO(context);
        }

        [HttpGet("summaryList")]
        public ActionResult summaryList(int campaignID)
        {
            return Ok(reportsDAO.summaryList("all", campaignID));
        }

        [HttpGet("summaryListByPO")]
        public ActionResult summaryListByPO(int campaignID)
        {
            return Ok(reportsDAO.summaryList("so", campaignID));
        }

        [HttpGet("summaryListByWeek")]
        public ActionResult summaryListByWeek(int campaignID)
        {
            return Ok(reportsDAO.summaryList("wek", campaignID));
        }

        [HttpGet("summaryListByPacking")]
        public ActionResult summaryListByPacking(int campaignID)
        {
            return Ok(reportsDAO.summaryList("pak", campaignID));
        }

        [HttpGet("summaryListByCustomer")]
        public ActionResult summaryListByCustomer(int campaignID)
        {
            return Ok(reportsDAO.summaryList("cus", campaignID));
        }

        [HttpGet("summaryListByStatus")]
        public ActionResult summaryListByStatus(int campaignID)
        {
            return Ok(reportsDAO.summaryList("sta", campaignID));
        }

        [HttpGet("summaryListByDestination")]
        public ActionResult summaryListByDestination(int campaignID)
        {
            return Ok(reportsDAO.summaryList("des", campaignID));
        }

        [HttpGet("expoList")]
        public ActionResult expoList()
        {
            return Ok(reportsDAO.expoMastersList("all"));
        }

        [HttpGet("expoListByPO")]
        public ActionResult expoListByPO()
        {
            return Ok(reportsDAO.expoMastersList("po"));
        }

        [HttpGet("expoListByGrower")]
        public ActionResult expoListByGrower()
        {
            return Ok(reportsDAO.expoMastersList("gro"));
        }

        [HttpGet("expoListByCustomer")]
        public ActionResult expoListByCustomer()
        {
            return Ok(reportsDAO.expoMastersList("cus"));
        }

        [HttpGet("expoListByDestination")]
        public ActionResult expoListByDestination()
        {
            return Ok(reportsDAO.expoMastersList("des"));
        }

        [HttpGet("shippingIListByPO")]
        public ActionResult shippingIListByPO(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("po", campaignID));
        }

        [HttpGet("shippingIListByPL")]
        public ActionResult shippingIListByPL(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("pl", campaignID));
        }

        [HttpGet("shippingIListByCustomer")]
        public ActionResult shippingIListByCustomer(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("cus", campaignID));
        }

        [HttpGet("shippingIListByManager")]
        public ActionResult shippingIListByManager(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("man", campaignID));
        }

        [HttpGet("shippingIListByLogistic")]
        public ActionResult shippingIListByLogistic(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("log", campaignID));
        }

        [HttpGet("shippingIListByWeek")]
        public ActionResult shippingIListByWeek(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("wek", campaignID));
        }

        [HttpGet("shippingIListByDestination")]
        public ActionResult shippingIListByDestination(int campaignID)
        {
            return Ok(reportsDAO.shippingInstructionList("des", campaignID));
        }

        [HttpGet("GetreportVsFor")]
        public ActionResult GetreportVsFor()
        {
            return Ok(reportsDAO.GetreportVersus("for"));
        }

        [HttpGet("GetreportVsSR")]
        public ActionResult GetreportVsSR()
        {
            return Ok(reportsDAO.GetreportVersus("sal"));
        }

        [HttpGet("GetreportVsPO")]
        public ActionResult GetreportVsPO()
        {
            return Ok(reportsDAO.GetreportVersus("pod"));
        }

        [HttpGet("GePackinglistPO")]
        public ActionResult GePackinglistPO(string nroPackingList)
        {
            return Ok(reportsDAO.GePackinglistPO(nroPackingList));
        }

        [HttpGet("GePackinglistApache")]
        public ActionResult GePackinglistApache(string opt)
        {
            return Ok(reportsDAO.ApacheReportPackingList(opt));
        }

        /// <summary>
        /// Method to list loaded instruction
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListInstructionByFilter(string opt, int campaignID)
        {
            var dttInstruction = reportsDAO.shippingInstructionList(opt, campaignID);
            return StatusCode((int)HttpStatusCode.OK, dttInstruction);
        }
    }
}
