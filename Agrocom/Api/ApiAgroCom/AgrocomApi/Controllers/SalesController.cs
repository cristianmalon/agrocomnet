﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using System.Collections.Generic;
using System.Text;
using AgrocomApi.Filters;using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.SalesDAO salesDAO;

        public SalesController(ApplicationDbContext context)
        {
            this.context = context;
            salesDAO = new Models.SalesDAO(context);
        }

        [HttpPost("SaveDetail")]
        public ActionResult SaveDetail(int saleDetailID, int destinationID, int projectedWeekID, int customerID, double quantity, double price, string paymentTerm, int variedadID, string categoryID, int codePackID, int conditionPaymentID, int incotermID, int userCreated)
        {
            return Ok(salesDAO.SaveDetail(saleDetailID, destinationID, projectedWeekID, customerID, quantity, price, paymentTerm, variedadID, categoryID, codePackID, conditionPaymentID, incotermID, userCreated));
        }

        [HttpPost("SaveSale")]
        public ActionResult SaveSale(int destinationID, int projectedWeekID, int customerID, int userCreated)
        {
            return Ok(salesDAO.SaveSale(destinationID, projectedWeekID, customerID, userCreated));
        }

        [HttpPost("SaveSaleDetail")]
        public ActionResult SaveSaleDetail(int saleID, double quantity, double price, string paymentTerm, int varietyID, string categoryID, int codePackID, int conditionPaymentID, int incotermID, int userCreated)
        {
            return Ok(salesDAO.SaveSaleDetail(saleID, quantity, price, paymentTerm, varietyID, categoryID, codePackID, conditionPaymentID, incotermID, userCreated));
        }
        [HttpPost("SaveSaleBulk")]
        public ActionResult SaveSaleBulk(int saleID, int destinationID, int projectedWeekID, int customerID, int userCreated, int wowID, string comments, int status, List<typecollectionLarge> datails_table)
        {
            return Ok(salesDAO.SaveSaleBulk(saleID, destinationID, projectedWeekID, customerID, userCreated, wowID, comments, status, datails_table));
        }

        /// <summary>
        /// Method to list via by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("KamListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult KamListByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            var dtt = salesDAO.GetKam(opt, id);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpPost("SaveSaleEN")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveSaleEN(ENSales o)
        {
            var dtt = salesDAO.SaveSaleEN(o);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpPost("SaveSOwithoutProgram")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaleOrderWithoutCommercialPlanInsert(ENSales o)
        {
            var dtt = salesDAO.SaleOrderWithoutCommercialPlanInsert(o);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetSRren")]
        public ActionResult GetSRren()
        {
            return Ok(salesDAO.GetSalesPending());
        }

        [HttpGet("GetSRuncom")]
        public ActionResult GetSRuncom()
        {
            return Ok(salesDAO.GetSalesUncommitted());
        }

        [HttpGet("ListReport")]
        public ActionResult ListReport(int campaignID)
        {
            return Ok(salesDAO.ListReport("all", campaignID));
        }

        [HttpGet("ListReportCustomer")]
        public ActionResult ListReportCustomer(int campaignID)
        {
            return Ok(salesDAO.ListReport("cus", campaignID));
        }

        [HttpGet("ListReportVia")]
        public ActionResult ListReportVia(int campaignID)
        {
            return Ok(salesDAO.ListReport("via", campaignID));
        }

        [HttpGet("ListReportCodepack")]
        public ActionResult ListReportCodepack(int campaignID)
        {
            return Ok(salesDAO.ListReport("pre", campaignID));
        }

        [HttpGet("ListReportDestination")]
        public ActionResult ListReportDestination(int campaignID)
        {
            return Ok(salesDAO.ListReport("des", campaignID));
        }

        [HttpGet("ListReportBox")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListReportBox(int campaignID)
        {
            var dtt = salesDAO.ListReportBox(campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("ListReportSum")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListReportSum(int campaignID)
        {
            var dtt = salesDAO.ListReportSum(campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("ListReportBoxSum")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListReportBoxSum(int campaignID)
        {
            var dtt = salesDAO.ListReportBoxSum(campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpPost("DeleteSales")]
        public ActionResult DeleteSales(int saleID, int userID)
        {
            return Ok(salesDAO.DeleteSales(saleID, userID));
        }

        [HttpGet("ListReportPO")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListReportPO(int campaignID)
        {
            var dtt = salesDAO.ListReportPO(campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetPaymentTermByCustomer")]
        public ActionResult GetPaymentTermByCustomer(int customerID, string cropiD)
        {
            return Ok(salesDAO.GetPaymentTermByCustomer("CUS", customerID, cropiD));
        }

        [HttpGet("GetAllWeek")]
        public ActionResult GetAllWeek(int campaignID)
        {
            return Ok(salesDAO.ListByCampaign("LWI", "", 0, 0, 0, campaignID));
        }

        [HttpGet("GetSRrej")]
        public ActionResult GetSalesRejected(int saleID)
        {
            return Ok(salesDAO.salesRejected(saleID));
        }

        [HttpGet("GetsalesAllocation")]
        public ActionResult GetsalesAllocation(int projectedWeekIni, int projectedWeekEnd, string growerID, string marketID, int customerID)
        {
            return Ok(salesDAO.salesAllocation("RPT", projectedWeekIni, projectedWeekEnd, growerID, marketID, customerID));
        }

        [HttpGet("GetsalesAllocationWeek")]
        public ActionResult GetsalesAllocationWeek(int projectedWeekIni, int projectedWeekEnd, string growerID, string marketID, int customerID)
        {
            return Ok(salesDAO.salesAllocation("LIW", projectedWeekIni, projectedWeekEnd, growerID, marketID, customerID));
        }

        [HttpGet("GetMountByStatus")]
        public ActionResult GetMountByStatus(int campaignID)
        {
            return Ok(salesDAO.ListByCampaign("stf", "", 0, 0, 0, campaignID));
        }

        [HttpGet("GetsalesOutSpecs")]
        public ActionResult GetsalesOutSpecs(int orderProductionID)
        {
            return Ok(salesDAO.salesOutSpecs(orderProductionID));
        }

        [HttpPost("SaveSalePC")]
        public ActionResult SaveSalePC(ENSalesPC o)
        {
            return Ok(salesDAO.SaveSalePC(o));
        }

        [HttpGet("GetPaymentTermByCrop")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetPaymentTermByCrop(string cropID)
        {
            var dtt = salesDAO.GetPaymentTermByCustomer("cro", 0, cropID);
            return StatusCode((int)HttpStatusCode.OK, dtt);

        }

        [HttpGet("GetIncotermByCrop")]
        public ActionResult GetIncotermByCrop(string cropID)
        {
            return Ok(salesDAO.GetIncotermList("cro", cropID, 0));
        }

        [HttpGet("GetIncotermByVia")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetIncotermByVia(string cropID, int viaID)
        {
            var dtt = salesDAO.GetIncotermList("cvi", cropID, viaID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetListWeekByCampaignID")]
        public ActionResult GetListWeekByCampaignID(int campaignID)
        {
            return Ok(salesDAO.ListByCampaign("LWC", "", 0, 0, 0, campaignID));
        }

        [HttpGet("GetSaleDetailList")]
        public ActionResult GetSaleDetailList(string saleID)
        {
            return Ok(salesDAO.ListByCampaign("DEC", saleID, 0, 0, 0, 0));
        }

        [HttpGet("GetSalesPendingForPONewByCampaignID")]
        public ActionResult GetSalesPendingForPONewByCampaignID(string projectedWeekID, int campaignID)
        {
            return Ok(salesDAO.ListByCampaign("PON", projectedWeekID, 0, 0, 0, campaignID));
        }
        [HttpGet("GetSalesDetailModifiedPO")]
        public ActionResult GetSalesDetailModifiedPO(int saleID)
        {
            return Ok(salesDAO.ListByCampaign("MOD", saleID.ToString(), 0, 0, 0, 0));
        }

        [HttpGet("GetStatusByRej")]
        public ActionResult GetStatusByRej(int saleID, string commentary)
        {
            return Ok(salesDAO.ListByCampaign("rej", commentary, 0 ,0, saleID, 0));
        }

        [HttpGet("GetStatusByMod")]
        public ActionResult GetStatusByMod(int saleID)
        {
            return Ok(salesDAO.ListByCampaign("upd", "", 0, 0, saleID, 0));
        }

        /// <summary>
        /// Method to list sales by campaign
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="val"></param>
        /// <param name="weekIni"></param>
        /// <param name="weekEnd"></param>
        /// <param name="userID"></param>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByCampaign")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListSaleByCampaign([Required][FromHeader(Name = "Authorization")] string token, string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            opt = opt ?? "";
            val = val ?? "";
            var dtt = salesDAO.ListByCampaign(opt, val, weekIni, weekEnd, userID, campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        /// <summary>
        /// Method to list payment terms by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="customerID"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListPaymentTermByCustomer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListPaymentTermByCustomer([Required][FromHeader(Name = "Authorization")] string token, [Required]string opt, [Required] int customerID, [Required] string cropID)
        {
            var dtt = salesDAO.GetPaymentTermByCustomer(opt, customerID, cropID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetSalesReportMC")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetSalesReportMC(string opt, int campaignID)
        {
            var dtt = salesDAO.ListReport(opt, campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetIncotermMC")]
        public ActionResult GetIncoterm_MC(string opt, string cropID, int viaID)
        {
            return Ok(salesDAO.GetIncotermList(opt, cropID, viaID));
        }

        /// <summary>
        /// Method to list status by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="val"></param>
        /// <param name="weekIni"></param>
        /// <param name="weekEnd"></param>
        /// <param name="userID"></param>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListStatusByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListStatusByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            opt = opt ?? string.Empty;
            val = val ?? string.Empty;
            var dtt = salesDAO.List(opt, val, weekIni, weekEnd, userID, campaignID);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetDocument")]
        public ActionResult GetDocument(string opt, string value)
        {
            return Ok(salesDAO.GetDocument(opt, value));
        }


    }
}