﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;using System.Net;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeasonController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.SeasonDAO seasonDAO;

        public SeasonController(ApplicationDbContext context)
        {
            this.context = context;
            seasonDAO = new Models.SeasonDAO(context);
        }

        /// <summary>
        /// Generic method to list season by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListSeasonByFilter(string opt, string id)
        {
            try
            {
                opt = opt ?? string.Empty;
                id = id ?? string.Empty;
                return Ok(seasonDAO.GetSeason(opt, id));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "opt=" + opt + "," + "id=" + id);
                return Unauthorized(HttpStatusCode.BadRequest);

            }
        }
    }
}
