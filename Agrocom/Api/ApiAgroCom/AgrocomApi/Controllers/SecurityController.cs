﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using Agrocom.Util;
using Common.Security;
using AgrocomApi.Exceptions;
using System.Data;
using System.Collections.Generic;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        #region "variables"
        private readonly IConfiguration configuration;
        private readonly Models.UsersDAO usersDAO;
        private readonly Models.SecurityDAO securityDAO;
        ApplicationDbContext context;
        #endregion

        #region "constructor"
        public SecurityController(IConfiguration configuration, ApplicationDbContext context)
        {
            this.configuration = configuration;
            this.context = context;
            usersDAO = new Models.UsersDAO(context);
            securityDAO = new Models.SecurityDAO(context);
        }
        #endregion

        #region "metodos"
        #region "apis"
        /// <summary>
        /// Generic method to verify user authentication
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="contrasenia"></param>
        /// <response code="200">Ok</response>
        /// <response code="401">Requires authentication - El acceso requiere autenticación</response> 
        [HttpGet("AuthenticateUser")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [AllowAnonymous]
        [ErrorHandlingMiddleware]
        public ActionResult AuthenticateIdentity([Required]string usuario, [Required]string contrasenia)
        {

            var _userInfo = AuthenticateUser(usuario, contrasenia);
            if (_userInfo == null) return StatusCode((int)HttpStatusCode.Unauthorized);
            var oUserResponse = new EUserResponse()
            {
                token = _userInfo.token,
                Login = _userInfo.Login,
                Nombre = _userInfo.Nombre,
                userID = _userInfo.userID,
                usertypeID = _userInfo.usertypeID,
                SOwithoutProgram = _userInfo.SOwithoutProgram,
                CustomerID_SOwithoutProgram = _userInfo.CustomerID_SOwithoutProgram,
                ConsigneeID_SOwithoutProgram = _userInfo.ConsigneeID_SOwithoutProgram,
                menu = _userInfo.menu,
            };
            return StatusCode((int)HttpStatusCode.OK, oUserResponse);
        }

        /// <summary>
        /// Request authorization to get token
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="contrasenia"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetTokenForAuthorization")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [AllowAnonymous]
        [ErrorHandlingMiddleware]
        public ActionResult RequestAuthorizationGetToken([Required]string usuario, [Required]string contrasenia)
        {
            contrasenia = UEncrypt.EncryptTripleDES(contrasenia);
            var dttUsr = usersDAO.InsertRequestAuthorization(usuario, contrasenia);
            if (dttUsr == null || dttUsr.Rows.Count == 0) return StatusCode((int)HttpStatusCode.Unauthorized);
            var objUsr = new EUserResponse()
            {
                userID = dttUsr.Rows[0]["userID"].ToString(),
                Login = usuario,
                Nombre = (dttUsr.Rows[0]["firstname"] == null) ? "" : dttUsr.Rows[0]["firstname"].ToString(),
                Apellidos = (dttUsr.Rows[0]["paternalLastName"] == null) ? "" : dttUsr.Rows[0]["paternalLastName"].ToString(),
                Email = (dttUsr.Rows[0]["email"] == null) ? "" : dttUsr.Rows[0]["email"].ToString(),
            };
            var sToken = GenerateToken(objUsr);
            if (string.IsNullOrEmpty(sToken)) return StatusCode((int)HttpStatusCode.BadRequest);
            var iResultado = new Models.PackingListDAO(context).InsertToken(int.Parse(objUsr.userID), sToken);
            if (iResultado == 0) return StatusCode((int)HttpStatusCode.BadRequest);
            objUsr.token = sToken;
            return StatusCode((int)HttpStatusCode.OK, new { token = objUsr.token, codigo = dttUsr.Rows[0]["codigo"].ToString() });
        }
        #endregion

        #region "metodos privados"

        private EUserResponse AuthenticateUser(string usuario, string contrasenia)
        {
            try
            {
                contrasenia = contrasenia.Replace(" ", "+");
                var dttUsr = usersDAO.GetAuthenticateUser(usuario, contrasenia);
                if (dttUsr == null || dttUsr.Rows.Count == 0) return null;
                var objUsr = new EUserResponse()
                {
                    userID = dttUsr.Rows[0]["userID"].ToString(),
                    Login = usuario,
                    Nombre = (dttUsr.Rows[0]["firstname"] == null) ? "" : dttUsr.Rows[0]["firstname"].ToString(),
                    Apellidos = (dttUsr.Rows[0]["paternalLastName"] == null) ? "" : dttUsr.Rows[0]["paternalLastName"].ToString(),
                    Email = (dttUsr.Rows[0]["email"] == null) ? "" : dttUsr.Rows[0]["email"].ToString(),
                    usertypeID = dttUsr.Rows[0]["usertypeID"].ToString(),
                    SOwithoutProgram = dttUsr.Rows[0]["SOwithoutProgram"].ToString(),
                    CustomerID_SOwithoutProgram = dttUsr.Rows[0]["CustomerID_SOwithoutProgram"].ToString(),
                    ConsigneeID_SOwithoutProgram = dttUsr.Rows[0]["ConsigneeID_SOwithoutProgram"].ToString(),
                };
                var sToken = GenerateToken(objUsr);
                if (string.IsNullOrEmpty(sToken)) return null;

                var iResultado = securityDAO.InsertToken(int.Parse(objUsr.userID), sToken);
                if (iResultado == 0) return null;
                objUsr.token = sToken;

                var dttMenu = usersDAO.GetMenuByUserIDCropID(int.Parse(objUsr.userID));
                objUsr.menu = new List<Common.Security.EUserMenuResponse>();
                foreach (DataRow obj in dttMenu.Rows)
                {
                    objUsr.menu.Add(new Common.Security.EUserMenuResponse() { 
                        optionMenu = obj["menu"].ToString(),
                        controller = obj["controller"].ToString(),
                        action = obj["action"].ToString(),
                        cropid = obj["cropid"].ToString(),
                    });
                }
                return objUsr;
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName, "");//, "usuario=" + usuario + ", " + "contrasenia=" + contrasenia);
                return null;
            }
        }

        private string GenerateToken(EUserResponse usuarioInfo)
        {
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["JWT:ClaveSecreta"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, usuarioInfo.userID.ToString()),
                new Claim("_name_", usuarioInfo.Nombre),
                new Claim("_lastname_", usuarioInfo.Apellidos),
                //new Claim(JwtRegisteredClaimNames.Email, usuarioInfo.Email),
                //new Claim(ClaimTypes.Role, usuarioInfo.Rol)
            };

            var _Payload = new JwtPayload(
                    issuer: configuration["JWT:Issuer"],
                    audience: configuration["JWT:Audience"],
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    // Exipra a la 1.5 horas.
                    expires: DateTime.UtcNow.AddMinutes(Convert.ToDouble(configuration["JWT:Hour"]))
                );

            var _Token = new JwtSecurityToken(
                    _Header,
                    _Payload
                );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }

        #endregion
        #endregion
    }
}
