﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using AgrocomApi.Filters;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShippingStatusController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.ShippingStatusDAO shippingStatusDAO;
        
        public ShippingStatusController(ApplicationDbContext context)
        {
            this.context = context;
            shippingStatusDAO = new Models.ShippingStatusDAO(context);
        }

        [HttpPost("InserNewPODocument")]
        public ActionResult InserNewPODocument(string id, int typeDocId, string nameFile, int statusId)
        {
            return Ok(shippingStatusDAO.InserNewPODocument("INS", id, typeDocId, nameFile, statusId));
        }

        [HttpPost("SetDeletePODocument")]
        public ActionResult SetDeletePODocument(string id, string typeDocId)
        {
            return Ok(shippingStatusDAO.DeletePODocument("DEL", id, typeDocId));
        }

        [HttpGet("GetShipmentStPOByWeek")]
        public ActionResult GetShipmentStPOByWeek(int idUser, int WIni, int WFin)
        {
            return Ok(shippingStatusDAO.GetShipmentStPOByWeek("all", idUser, WIni, WFin));
        }
        [HttpGet("GetShipmentStDocuments")]
        public ActionResult GetShipmentStDocuments()
        {
            return Ok(shippingStatusDAO.GetDocumentsShipmentStatus("PODOC", "order.orderProductionDocuments", "documents"));
        }

        [HttpGet("GetLoadPODocsByID")]
        public ActionResult GetLoadPODocsByID(string typeID)
        {
            return Ok(shippingStatusDAO.GetLoadPODocsByID("SEL", typeID));
        }

        [HttpPost("SetConfirmPODocs")]
        public ActionResult SetConfirmPODocs(int id, int poStatusId)
        {
            return Ok(shippingStatusDAO.ConfirmPODocs("UPO", id, poStatusId));
        }        

        [HttpGet("GetTariffList")]
        public ActionResult GetTariffList(string cropID)
        {
            return Ok(shippingStatusDAO.GetShippingComex("tar", 0, cropID));
        }        

        [HttpGet("GetDestinationType")]
        public ActionResult GetDestinationType()
        {
            return Ok(shippingStatusDAO.GetShippingComex("des", 0, ""));
        }

        [HttpGet("GetDestinationArrive")]
        public ActionResult GetDestinationArrive()
        {
            return Ok(shippingStatusDAO.GetShippingComex("arr", 0, ""));
        }

        [HttpGet("GetCustomerType")]
        public ActionResult GetCustomerType()
        {
            return Ok(shippingStatusDAO.GetShippingComex("cus", 0, ""));
        }

        [HttpGet("GetConsigneeType")]
        public ActionResult GetConsigneeType()
        {
            return Ok(shippingStatusDAO.GetShippingComex("con", 0, ""));
        }

        [HttpGet("GetNotifyType")]
        public ActionResult GetNotifyType()
        {
            return Ok(shippingStatusDAO.GetShippingComex("not", 0, ""));
        }

        [HttpGet("GetExporterType")]
        public ActionResult GetExporterType(string cropID)
        {
            return Ok(shippingStatusDAO.GetShippingComex("exp", 0, cropID));
        }

        /// <summary>
        /// Method to save shipping instruction
        /// </summary>
        /// <param name="oShippingLoading"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("SaveShippingLoading")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveShippingLoading([FromBody]ENShippingLoading oShippingLoading)
        {
            var dtt = shippingStatusDAO.SaveShippingLoading(oShippingLoading);
            return StatusCode((int)HttpStatusCode.OK, dtt);
        }

        [HttpGet("GetNroPacking")]
        public ActionResult GetNroPacking(int campaignID)
        {
            return Ok(shippingStatusDAO.GetShippingComex("pak", campaignID, ""));
        }

        [HttpGet("GetShippingByID")]
        public ActionResult GetShippingByID(int shippingLoadingID, string cropID)
        {
            return Ok(shippingStatusDAO.GetShippingPDF(shippingLoadingID, cropID));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <param name="customerID"></param>
        /// <param name="destinationID"></param>
        /// <param name="nroPackingList"></param>
        /// <param name="managerID"></param>
        /// <param name="logisticOperatorID"></param>
        /// <param name="processPlantID"></param>
        /// <param name="projectedWeekID"></param>
        /// <param name="viaID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingTrayList")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingTrayList(int campaignID, int customerID, int destinationID, string nroPackingList, string managerID, int logisticOperatorID, int processPlantID, int projectedWeekID, int viaID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("tra", campaignID, customerID, destinationID, nroPackingList, managerID, logisticOperatorID, processPlantID, projectedWeekID, viaID));            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByCustomer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByCustomer(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("cus", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByDestination")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByDestination(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("des", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingPackingList")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingPackingList(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("pak", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByManager")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByManager(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("man", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByOperator")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByOperator(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("ope", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByPlant")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByPlant(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("pla", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByWeek")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByWeek(int campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("wek", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("GetShippingByVia")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingByVia(int campaignID)
        {
            return Ok(shippingStatusDAO.GetShippinglist("via", campaignID, 0, 0, "", "", 0, 0, 0, 0));
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="shippingLoadingID"></param>
        /// <param name="etaReal"></param>
        /// <param name="bl"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("GetShippingUpdate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetShippingUpdate(int shippingLoadingID, string etaReal, string bl)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippinglist("upd", shippingLoadingID, 0, 0, etaReal, bl, 0, 0, 0));
        }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="data1"></param>
        /// <param name="data2"></param>
        /// <param name="data3"></param>
        /// <param name="data4"></param>
        /// <param name="data5"></param>
        /// <param name="data6"></param>
        /// <param name="data7"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("GetDateUpdate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult GetDateUpdate(string opt, int id, string data1, string data2, string data3, string data4, string data5, string data6, string data7)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetDateUpdate(opt, id, data1, data2, data3, data4, data5, data6, data7));
        }

        /// <summary>
        /// Method to list week by season
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="orderPoductionID"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListShippingInstructionByFilter(string opt, int orderPoductionID, string cropID)
        {
            opt = opt ?? string.Empty;
            cropID = cropID ?? string.Empty;
            return StatusCode((int)HttpStatusCode.OK,shippingStatusDAO.GetShippingComex(opt, orderPoductionID, cropID));
        }

        /// <summary>
        /// Method to list sales order with request loading
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListRequestLoading")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListSalesOrdersWithRequestLoading(string campaignID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetSalesOrdersWithRequestLoading(campaignID));
        }

        /// <summary>
        /// Method to list request loading detail
        /// </summary>
        /// <param name="orderPoductionID"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListDetailRequestByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListDetailRequestLoadingByFilter(int orderPoductionID, string cropID)
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetDetailRequestLoading(orderPoductionID, cropID));
        }

        [HttpGet("GetShippingParameter")]
        public ActionResult GetShippingParameter(string cropID, int destinationID, int coldTreatment)
        {
            return Ok(shippingStatusDAO.GetShippingParameter(cropID, destinationID, coldTreatment));
        }

        /// <summary>
        /// Method to list request loading detail
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListBL")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListShippingLoadingBL()
        {
            return StatusCode((int)HttpStatusCode.OK, shippingStatusDAO.GetShippingLoadingBL());
        }
    }
}