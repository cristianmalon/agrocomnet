﻿using System;
using System.Net;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using AgrocomApi.Exceptions;
using AgrocomApi.Filters;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SisPackingController : ControllerBase
    {
        #region "variables"
        private readonly ApplicationDbContext context;
        private readonly Models.PackingListDAO packingListDAO;
        private readonly IConfiguration _configuration;
        #endregion

        #region "constructor"
        public SisPackingController(ApplicationDbContext context, IConfiguration configuration)
        {
            this.context = context;
            packingListDAO = new Models.PackingListDAO(context);
            _configuration = configuration;
        }
        #endregion

        #region "metodos"
        #region "apis"
        [HttpGet("GetSisPacking")]
        public ActionResult GetSisPacking(string campana, string nroPacking)
        {
            try
            {
                nroPacking = nroPacking ?? "";
                var url = _configuration.GetValue<string>("UrlSispacking");
                var client = new RestClient(url + "/COMEX/api/sce/GetShipmentCarrizales");
                Packing packing = new Packing();
                packing.CAMPANA = campana;
                packing.NROPACKINGLIST = nroPacking;

                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                var sz = JsonConvert.SerializeObject(packing);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", sz, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string result = response.Content;
                List<ENSispacking> listENSispacking = new List<ENSispacking>();
                listENSispacking = listaPacking(result, campana, nroPacking);

                if (listENSispacking.Count > 0)
                {
                    foreach (ENSispacking item in listENSispacking)
                    {
                        packingListDAO.SaveMasivePackingList(item);
                    }
                }

                return Ok(200);
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName, "");//, "campana=" + campana + "," + "nroPacking=" + nroPacking);
                return Unauthorized(HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Method to list sales order information
        /// </summary>
        /// <param name="codeAuthorized"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListSalesOrdersInformation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult PackingListSalesOrdersInformation([Required][FromHeader(Name = "AuthorizationCode")] string codeAuthorized)
        {
            string sToken = HttpContext.Request.Headers["Authorization"];
            if (!sToken.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                var mensaje = "Incorrect format of Authorization parameter in request header, 'Bearer ' is required.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }
            else sToken = sToken.Substring("Bearer ".Length).Trim();

            if (string.IsNullOrEmpty(sToken))
            {
                var mensaje = "Token not found in Request header.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }

            return StatusCode((int)HttpStatusCode.OK, packingListDAO.PackingListSalesOrdersInformation(sToken, codeAuthorized));
        }

        /// <summary>
        /// Method to list sales order information with o without shipping instruction
        /// </summary>
        /// <param name="codeAuthorized"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListSalesOrdersInformationSeparateShippingInstruction")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult PackingListSalesOrdersInformationSeparateShippingInstruction([Required][FromHeader(Name = "AuthorizationCode")] string codeAuthorized)
        {
            string sToken = HttpContext.Request.Headers["Authorization"];
            if (!sToken.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                var mensaje = "Incorrect format of Authorization parameter in request header, 'Bearer ' is required.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }
            else sToken = sToken.Substring("Bearer ".Length).Trim();

            if (string.IsNullOrEmpty(sToken))
            {
                var mensaje = "Token not found in Request header.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }

            return StatusCode((int)HttpStatusCode.OK, packingListDAO.ListSalesOrdersInformationSeparateShippingInstruction(sToken, codeAuthorized));
        }

        /// <summary>
        /// Method to list shipping instruction information
        /// </summary>
        /// <param name="codeAuthorized"></param>
        /// <param name="shippingLoadingID"></param>
        /// <param name="processPlantID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListShippingInstructionDetail")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult PackingListShippingLoadingDetail([Required][FromHeader(Name = "AuthorizationCode")] string codeAuthorized,
            int shippingLoadingID, string processPlantID)
        {
            string sToken = HttpContext.Request.Headers["Authorization"];
            if (!sToken.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                var mensaje = "Incorrect format of Authorization parameter in request header, 'Bearer ' is required.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }
            else sToken = sToken.Substring("Bearer ".Length).Trim();

            if (string.IsNullOrEmpty(sToken))
            {
                var mensaje = "Token not found in Request header.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }

            return StatusCode((int)HttpStatusCode.OK, packingListDAO.ListShippingLoadingDetail(shippingLoadingID, processPlantID, sToken, codeAuthorized));
        }

        /// <summary>
        /// Method to list shipping instruction information by packing list number
        /// </summary>
        /// <param name="codeAuthorized"></param>
        /// <param name="nroPackingList"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListofLoadDetail")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult PackingListShippingInstructionDetail([Required][FromHeader(Name = "AuthorizationCode")] string codeAuthorized, string nroPackingList)
        {
            string sToken = HttpContext.Request.Headers["Authorization"];
            if (!sToken.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                var mensaje = "Incorrect format of Authorization parameter in request header, 'Bearer ' is required.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }
            else sToken = sToken.Substring("Bearer ".Length).Trim();

            if (string.IsNullOrEmpty(sToken))
            {
                var mensaje = "Token not found in Request header.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }

            return StatusCode((int)HttpStatusCode.OK, packingListDAO.ListShippingInstructionDetail(nroPackingList, sToken, codeAuthorized));
        }
        #endregion

        #region "metodos privados"
        List<ENSispacking> listaPacking(string json, string campana, string nroPacking)
        {
            var array = JArray.Parse(json);
            List<ENSispacking> listENSispacking = new List<ENSispacking>();
            foreach (JObject item in array) // <-- Note that here we used JObject instead of usual JProperty
            {
                ENSispacking objENSispacking = new ENSispacking();

                objENSispacking.Campana = campana;
                objENSispacking.NroPackingList = item.GetValue("packingListNumber").ToString();
                objENSispacking.SenasaSeal = item.GetValue("SenasaSeal").ToString();
                objENSispacking.CustomSeal = item.GetValue("CustomSeal").ToString();
                objENSispacking.Thermographs = item.GetValue("Thermographs").ToString();
                objENSispacking.ThermographsLocation = item.GetValue("ThermographsLocation").ToString();
                objENSispacking.Sensors = item.GetValue("Sensors").ToString();
                objENSispacking.SensorLocation = item.GetValue("SensorLocation").ToString();
                objENSispacking.TotalBoxes = item.GetValue("TotalBoxes").ToString();
                objENSispacking.PackingLoadingDate = item.GetValue("PackingLoadingDate").ToString();
                objENSispacking.Container = item.GetValue("Container").ToString();
                objENSispacking.ShipmentID = item.GetValue("ShipmentID").ToString();
                var detailshipments = item.GetValue("ShipmentDetails").ToString();
                var listShipmentDetails = JsonConvert.DeserializeObject<List<ShipmentDetails>>(detailshipments);
                objENSispacking.ShipmentDetails = new List<ShipmentDetails>();

                foreach (ShipmentDetails item2 in listShipmentDetails)
                {
                    ShipmentDetails objShipmentDetails = new ShipmentDetails();
                    objShipmentDetails.Pallet = item2.Pallet;
                    objShipmentDetails.Variety = item2.Variety.Replace("'", "");
                    objShipmentDetails.CodePack = item2.CodePack;
                    objShipmentDetails.Weight = item2.Weight;
                    objShipmentDetails.Label = item2.Label;
                    objShipmentDetails.Pack = item2.Pack;
                    objShipmentDetails.Size = item2.Size;
                    objShipmentDetails.Color = item2.Color;
                    objShipmentDetails.Cat = item2.Cat;
                    objShipmentDetails.Planta = item2.Planta;
                    objShipmentDetails.Package = item2.Package;
                    objShipmentDetails.Brand = item2.Brand;
                    objShipmentDetails.Presentation = item2.Presentation;
                    objShipmentDetails.PackingDate = item2.PackingDate;
                    objShipmentDetails.NumberBoxes = item2.NumberBoxes;

                    objENSispacking.ShipmentDetails.Add(objShipmentDetails);
                }

                listENSispacking.Add(objENSispacking);
            }

            return listENSispacking;
        }
        #endregion
        #endregion
    }

}
