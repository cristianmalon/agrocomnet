﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SizeController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.SizeDAO sizeDAO;

        public SizeController(ApplicationDbContext context)
        {
            this.context = context;
            sizeDAO = new Models.SizeDAO(context);
        }

        /// <summary>
        /// Method to list size by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListSizeByFilter([Required][FromHeader(Name = "Authorization")] string token, [Required]string opt, [Required] string id)
        {
            return StatusCode((int)HttpStatusCode.OK, sizeDAO.GetSize(opt, id));
        }

        /// <summary>
        /// Method to list size by crop
        /// </summary>
        /// <param name="cropID"></param>
        /// <param name="growerID"></param>
        /// <param name="lotID"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByCrop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListByCrop([Required][FromHeader(Name = "Authorization")] string token, string cropID, string growerID, int lotID)
        {
            return StatusCode((int)HttpStatusCode.OK, sizeDAO.ListSizeByFilter(cropID, growerID, lotID));
        }
    }
}