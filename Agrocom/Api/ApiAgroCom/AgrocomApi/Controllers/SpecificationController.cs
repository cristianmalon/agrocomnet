﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using System.Collections.Generic;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecificationController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.SpecificationDAO specificationDAO;

        public SpecificationController(ApplicationDbContext context)
        {
            this.context = context;
            specificationDAO = new Models.SpecificationDAO(context);
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            return Ok(specificationDAO.GetSpecification("all", ""));
        }

        [HttpGet("GetById")]
        public ActionResult GetById(string idSpecification)
        {
            return Ok(specificationDAO.GetSpecification("id", idSpecification));
        }

        [HttpGet("GetBySaleId")]
        public ActionResult GetBySaleId(string idSale)
        {
            return Ok(specificationDAO.GetSpecification("sal", idSale));
        }

        [HttpGet("GetListByForecast")]
        public ActionResult GetListByForecast(int idForecast)
        {
            return Ok(specificationDAO.GetListByForecast(idForecast));
        }

        [HttpGet("GetDisable")]
        public ActionResult GetDisable(int idSpecification)
        {
            return Ok(specificationDAO.GetSpecificationDisable(idSpecification));
        }

        [HttpGet("GetCreate")]
        public ActionResult GetCreate(int idSpecification, int idCustomer, int idDestionation, string miniumbrix, string acidity, string bloom, string traceability, string commentary, int usercreatedID)
        {
            return Ok(specificationDAO.GetSpecificationCreate(idSpecification,idCustomer,idDestionation,miniumbrix,acidity,bloom,traceability,commentary,usercreatedID));
        }

        [HttpGet("GetSpecsClientList")]
        public ActionResult GetSpecsClientList(string opt, int campaignID)
        {
            return Ok(specificationDAO.GetSpecsClientList(opt,campaignID));
        }

        [HttpPost("GetClientSaveUpdate")]
        public ActionResult GetClientSaveUpdate(int campaignID, string cropID, int userID, List<ClientSpecs> customers)
        {
            return Ok(specificationDAO.GetClientSaveUpdate(campaignID, cropID, userID, customers));
        }

        /// <summary>
        /// Method to save forecast by sale
        /// </summary>
        /// <param name="campaignID"></param>
        /// <param name="userID"></param>
        /// <param name="file"></param>
        /// /// <param name="objMassiveUpload"></param>
        /// <response code="200">Ok</response>
        /// <response code="204">Not Conten - Respuesta en blanco</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpPost("SaveProductUpdate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult SaveProductUpdate([Required][FromHeader(Name = "Authorization")] string token, int campaignID, int userID, string file, MassLoadProduct objMassiveUpload)
        {
            return StatusCode((int)HttpStatusCode.OK, specificationDAO.SaveProductUpdate(campaignID, userID, file, objMassiveUpload));
        }

    }
}