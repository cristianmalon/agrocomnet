﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;
using System.Net;
using AgrocomApi.Exceptions;


namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubsidiaryController:ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.SubsidiaryDAO subsidiaryDAO;

        public SubsidiaryController(ApplicationDbContext context)
        {
            this.context = context;
            subsidiaryDAO = new Models.SubsidiaryDAO(context);
        }

        /// <summary>
        /// Generic method to list brand by filter
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListSubsidiary")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        [ErrorHandlingMiddleware]
        public ActionResult ListSubsidiary([Required][FromHeader(Name = "Authorization")] string token)
        {
            return StatusCode((int)HttpStatusCode.OK, subsidiaryDAO.GetSubsidiary());
        }
    }
}
