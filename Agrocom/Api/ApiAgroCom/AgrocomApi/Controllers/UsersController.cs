﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.UsersDAO usersDAO;

        public UsersController(ApplicationDbContext context)
        {
            this.context = context;
            usersDAO = new Models.UsersDAO(context);
        }

    }
}