﻿using System;using System.Net;

namespace AgrocomApi.Controllers
{
    internal class UsuarioInfo
    {
        public string userID { get; set; }
        public string Login { get; set; }

        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Rol { get; set; }
        public string passwdHash { get; set; }
        public string statusID { get; set; }
        public string token { get; set; }
        public string usertypeID { get; set; }
    }
}