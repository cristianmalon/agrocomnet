﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;using System.Net;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VarietyController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.VarietyDAO varietyDAO;

        public VarietyController(ApplicationDbContext context)
        {
            this.context = context;
            varietyDAO = new Models.VarietyDAO(context);
        }

        /// <summary>
        /// Generic method to list variety by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="log"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListVarietyByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string log)
        {
            try
            {
                log = log ?? string.Empty;
                return Ok(varietyDAO.GetVariety(opt, id, log));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "opt=" + opt + "," + "id=" + id + "," + "log=" + log);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Generic method to list variety and category by crop
        /// </summary>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListVarietyCategoryByCrop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListVarietyCategoryByCrop([Required][FromHeader(Name = "Authorization")] string token, string cropID)
        {
            try
            {
                return Ok(varietyDAO.GetVarietyCategoryByCrop(cropID));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "cropID=" + cropID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }
    }
}
