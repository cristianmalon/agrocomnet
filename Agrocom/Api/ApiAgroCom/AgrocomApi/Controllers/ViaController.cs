﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;using System.Net;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViaController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.ViaDAO viaDAO;

        public ViaController(ApplicationDbContext context)
        {
            this.context = context;
            viaDAO = new Models.ViaDAO(context);
        }

        [HttpGet("Getmail")]
        public ActionResult Getmail()
        {
            return Ok(viaDAO.Getmail());
        }

        /// <summary>
        /// Method to list via by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListViaByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id)
        {
            try
            {
                return Ok(viaDAO.Getvia(opt, id));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName, "");
                //, "cropID=" + cropID + "," + "growerID=" + growerID + "," + "farmID=" + farmID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet("TittleByVia")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult TittleByVia([Required][FromHeader(Name = "Authorization")] string token, int viaID)
        {
            try
            {
                return Ok(viaDAO.GetTittle(viaID));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName, "");
                //, "cropID=" + cropID + "," + "growerID=" + growerID + "," + "farmID=" + farmID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

    }
}
