﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Common.Entities;
using AgrocomApi.Filters;using System.Net;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeekController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.WeekDAO weekDAO;

        public WeekController(ApplicationDbContext context)
        {
            this.context = context;
            weekDAO = new Models.WeekDAO(context);
        }

        /// <summary>
        /// Generic method to list week by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="cropID"></param>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        //[Authorize]
        public ActionResult ListWeekByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string cropID, int campaignID)
        {
            try
            {
                return Ok(weekDAO.GetWeek(opt, id, cropID, campaignID));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "opt=" + opt + "," + "id=" + id + "," + "cropID=" + cropID + "," + "campaignID=" + campaignID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Method to list week by season
        /// </summary>
        /// <param name="campaignID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response>
        [HttpGet("ListBySeason")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListWeekBySeason([Required][FromHeader(Name = "Authorization")] string token, int campaignID)
        {
            try
            {
                return Ok(weekDAO.ListWeekBySeasonId(campaignID));
            }
            catch (System.Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "campaignID=" + campaignID.ToString());
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

    }
}
