﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Primitives;
using System;using System.Net;
using System.Collections.Generic;
using System.Text;
using AgrocomApi.Filters;using System.Net;
using System.Net;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WowController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.WowDAO wowDAO;
        public WowController(ApplicationDbContext context)
        {
            this.context = context;
            wowDAO = new Models.WowDAO(context);
        }

        [HttpGet("GetById")]
        public ActionResult GetById(string idWow, string cropID)
        {
            return Ok(wowDAO.GetWow("id", idWow, cropID));
        }

        [HttpGet("GetForSpecs")]
        public ActionResult GetForSpecs(string idPO, string cropID)
        {
            return Ok(wowDAO.GetWow("spe", idPO, cropID));
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll(string cropID)
        {
            return Ok(wowDAO.GetWow("all", "", cropID));
        }

        [HttpGet("GetDisable")]
        public ActionResult GetDisable(int idWow)
        {
            return Ok(wowDAO.GetWowDisable(idWow));
        }


        [HttpPost("Create")]
        public ActionResult Create(int wowID, int customerID, int destinationID, int directInvoice, string certificationID
        , int documents, int documentsMoreInfo, string documentsInfo, int packingListID, int insuranceCertificate
        , int phytosanitary, int phytosanitaryMoreInfo, string phytosanitaryInfo, int certificateOrigin, int certificateOriginMoreInfo
        , string certificateOriginInfo, string moreInfo, int consigneeID, string consigneeAddress, string consigneeContact
        , string consigneePhone, int notifyID, string notifyIDAddress, string notifyIDContact, string notifyIDPhone
        , int notifyID2, string notifyID2Address, string notifyID2Contact, string notifyID2Phone, int notifyID3
        , string notifyID3Address, string notifyID3Contact, string notifyID3Phone, string docsCopyEmailInfo, string noted
        , string price, string accountSale, string finalPaymentConditions, string advanceCondition, int userID
        , string documentsExportSea, string documentsExportAir)
        {
            return Ok(wowDAO.WowCreate(wowID, customerID, destinationID, directInvoice, certificationID
            , documents, documentsMoreInfo, documentsInfo, packingListID, insuranceCertificate
            , phytosanitary, phytosanitaryMoreInfo, phytosanitaryInfo, certificateOrigin, certificateOriginMoreInfo
            , certificateOriginInfo, moreInfo, consigneeID, consigneeAddress, consigneeContact
            , consigneePhone, notifyID, notifyIDAddress, notifyIDContact, notifyIDPhone
            , notifyID2, notifyID2Address, notifyID2Contact, notifyID2Phone, notifyID3
            , notifyID3Address, notifyID3Contact, notifyID3Phone, docsCopyEmailInfo, noted
            , price, accountSale, finalPaymentConditions, advanceCondition, userID
            , documentsExportSea, documentsExportAir));
        }

        [HttpPost("Save")]
        public ActionResult Save(ENWow o)
        {
            return Ok(wowDAO.wowSave(o));
        }

        [HttpGet("ListByCustomer")]
        public ActionResult ListByCustomer(int customerID, string cropID)
        {
            return Ok(wowDAO.GetWow("cus", customerID.ToString(), cropID));
        }

        [HttpGet("GetWowMC")]
        public ActionResult GetWowMC(string opt, string id, string cropID)
        {
            try
            {
                return Ok(wowDAO.GetWow(opt, id, cropID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, opt + "," + id + "," + cropID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Generic method to list wow by filter
        /// </summary>
        /// <param name="opt"></param>
        /// <param name="id"></param>
        /// <param name="cropID"></param>
        /// <response code="200">Ok</response>
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="404">Not Found - No se encontro información</response>   
        /// <response code="500">Server Error - Errores no controlados</response> 
        [HttpGet("ListByFilter")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(404, Type = typeof(ErrorAnswer))]
        [ProducesResponseType(500, Type = typeof(ErrorAnswer))]
        [ValidateAuthenticationRequest]
        public ActionResult ListWowByFilter([Required][FromHeader(Name = "Authorization")] string token, string opt, string id, string cropID)
        {
            try
            {
                return Ok(wowDAO.GetWow(opt, id, cropID));
            }
            catch (Exception ex)
            {
                var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                var scurrentControllerAndMethodName = ControllerContext.ActionDescriptor.ControllerName + "-" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                new Models.LogErrorDAO(context).LogErrorInsert(sProjectName, ex.Message, scurrentControllerAndMethodName,"");//, "opt=" + opt + "," + "id=" + id + ", " + "cropID=" + cropID);
                return Unauthorized(HttpStatusCode.BadRequest);
            }
        }
    }
}
