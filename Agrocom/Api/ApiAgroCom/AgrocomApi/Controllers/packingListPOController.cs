﻿using AgrocomApi.Context;
using Microsoft.AspNetCore.Mvc;

namespace AgrocomApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class packingListPOController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly Models.PackingListPODAO packingListPODAO;
        
        public packingListPOController(ApplicationDbContext context)
        {
            this.context = context;
            packingListPODAO = new Models.PackingListPODAO(context);
        }

        [HttpPost("SavePkPO")]
        public ActionResult SavePkPO(int packingListID, int orderProductionID, string nroPackingList, string growerID, string invoiceGrower, string nroGuide, int customerID, string container, string senasaSeal, string customSeal, string thermogRegisters, string thermogRegistersLocation, string sensors, string sensorsLocation, string packingLoadDate, string booking, string originPort, string destinationPort, string shippingLine, string vessel, string BLAWB, string ETD, string ETA, int viaID, int totalBoxes)
        {
            return Ok(packingListPODAO.packingSave(packingListID, orderProductionID, nroPackingList, growerID, invoiceGrower, nroGuide, customerID, container, senasaSeal, customSeal, thermogRegisters, thermogRegistersLocation, sensors, sensorsLocation, packingLoadDate, booking, originPort, destinationPort, shippingLine, vessel, BLAWB, ETD, ETA, viaID, totalBoxes));
        }

        [HttpPost("SaveDetailPkPO")]
        public ActionResult SaveDetailPkPO(int packingListDetailID, int packingListID, string numberPallet, int orderPallet, string farmID, int codePackID, int packageProductID, string categoryID, int varietyID, string sizeID, string packingDate, int quantityBoxes, string description, int packingListDetailByVarietyID)
        {
            return Ok(packingListPODAO.packingSaveDetail(packingListDetailID, packingListID, numberPallet, orderPallet, farmID, codePackID, packageProductID, categoryID, varietyID, sizeID, packingDate, quantityBoxes, description, packingListDetailByVarietyID));
        }

        [HttpGet("GetById")]
        public ActionResult GetById(string orderProductionID)
        {
            return Ok(packingListPODAO.GetPackingPO("id", orderProductionID));
        }

        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            return Ok(packingListPODAO.GetPackingPO("all", ""));
        }

        [HttpGet("GetDet")]
        public ActionResult GetDet(string packingListID)
        {
            return Ok(packingListPODAO.GetPackingPO("iddet", packingListID));
        }

        [HttpGet("GetDetSum")]
        public ActionResult GetDetSum(string packingListID)
        {
            return Ok(packingListPODAO.GetPackingPO("idsum", packingListID));
        }

        [HttpGet("GetPackingMissing")]
        public ActionResult GetPackingMissing()
        {
            return Ok(packingListPODAO.GetPackingMissing());
        }

        [HttpPost("SaveSAllocated")]
        public ActionResult SaveSAllocated(string nroPackingList)
        {
            return Ok(packingListPODAO.GetsalesAllocated(nroPackingList));
        }

        [HttpPost("GetDataUpdate")]
        public ActionResult GetDataUpdate(string opt, int id, string data1, string data2, string data3, string data4)
        {
            return Ok(packingListPODAO.GetDataUpdate(opt, id, data1, data2, data3, data4));
        }
    }
}
