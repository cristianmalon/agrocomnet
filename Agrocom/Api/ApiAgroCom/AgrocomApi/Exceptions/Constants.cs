﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrocomApi.Exceptions
{
    public static class Error
    {
        public const int ERROR_SERVIDOR_CODIGO = 500;
        public const string ERROR_SERVIDOR_MENSAJE = "INTERNAL SERVER ERROR";
    }

    public struct ContextItem
    {
        public const string ItemValue = "_parameters_";
    }
}
