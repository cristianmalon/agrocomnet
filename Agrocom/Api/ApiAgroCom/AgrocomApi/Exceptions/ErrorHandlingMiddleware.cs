﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Agrocom.BusinessCore.BL.Implementation;
using AgrocomApi.Areas.Config;
using System.Collections.Generic;

namespace AgrocomApi.Exceptions
{
    public class ErrorHandlingMiddleware : ExceptionFilterAttribute, IAsyncExceptionFilter
    {
        public override Task OnExceptionAsync(ExceptionContext filterContext)
        {
            var exceptionMessage = filterContext.Exception.Message;
            var stackTrace = filterContext.Exception.StackTrace;
            var controllerName = filterContext.RouteData.Values["controller"].ToString();
            var actionName = filterContext.RouteData.Values["action"].ToString();

            var sProjectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            var sCurrentControllerAndMethodName = "Controller: " + controllerName + ", Action:" + actionName;
            var sMessage = "Error Message : " + exceptionMessage + Environment.NewLine + "Stack Trace : " + stackTrace;

            //Saving the data in a text file called Log.txt
            //File.AppendAllText(HttpContext.Current.Server.MapPath("~/Log/Log.txt"), Message);

            var sParameter = string.Empty;
            var items = filterContext.HttpContext.Items;
            if (items.ContainsKey(ContextItem.ItemValue) && !string.IsNullOrWhiteSpace(items[ContextItem.ItemValue].ToString()))
            {
                var dictionary = (Dictionary<string, object>)items[ContextItem.ItemValue];
                foreach (KeyValuePair<string, object> entry in dictionary)
                {
                    sParameter += entry.Key + "=" + entry.Value.ToString() + ",";
                }
            }
            var sDbConnection = ConfigurationReader.GetKeyValueAppsetting(FileJson.Agrocom, SettingConexion.Agrocom_Section_ConexionBd ,SettingConexion.Agrocom_Key_StringConexion);
            new ErrorLogic(sDbConnection).LogErrorInsert(sProjectName, sMessage, sCurrentControllerAndMethodName, sParameter);

            var error = new ErrorDetails()
            {
                StatusCode = Error.ERROR_SERVIDOR_CODIGO,
                Message = Error.ERROR_SERVIDOR_MENSAJE
            };

            filterContext.Result = new JsonResult(error);
            return Task.CompletedTask;
        }

    }
}
