﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Data.SqlClient;
using AgrocomApi.Areas.Config;
using Agrocom.BusinessCore.BL.Implementation;
using Common.Entities;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Newtonsoft.Json;

namespace AgrocomApi.Filters
{
    public class ValidateAuthenticationRequestAttribute : ActionFilterAttribute
    {

        public ValidateAuthenticationRequestAttribute()
        {

        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string sToken = context.HttpContext.Request.Headers["Authorization"];
            if (!sToken.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                var mensaje = "Incorrect format of Authorization parameter in request header, 'Bearer ' is required.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }
            else sToken = sToken.Substring("Bearer ".Length).Trim();

            if (string.IsNullOrEmpty(sToken))
            {
                var mensaje = "Token not found in Request header.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }

            var sDbConnection = ConfigurationReader.GetKeyValueAppsetting(FileJson.Agrocom, SettingConexion.Agrocom_Section_ConexionBd, SettingConexion.Agrocom_Key_StringConexion);
            if (!new TokenLogic(sDbConnection).ExistToken(sToken))
            {
                var mensaje = "Token not found.";
                throw new Exception(mensaje + "-" + (int)HttpStatusCode.Unauthorized);
            }

            var dictionary = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> entry in context.ActionArguments)
            {
                string argumentName = null;
                if (entry.Value != null){
                    if (entry.Value.GetType().IsPrimitive
                    || entry.Value.GetType() == typeof(decimal)
                    || entry.Value.GetType() == typeof(string))
                    {
                        argumentName = entry.Key;
                    }
                    //Linearize(dictionary, entry.Value, argumentName, null);
                    dictionary.Add(entry.Key,JsonConvert.SerializeObject(entry.Value));
                }
                
            }
            context.HttpContext.Items.Add(ContextItem.ItemValue, dictionary);
        }        

    }
}
