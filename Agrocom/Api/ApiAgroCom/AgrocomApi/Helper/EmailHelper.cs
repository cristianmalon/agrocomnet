﻿using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;using System.Net;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;


namespace AgrocomApi.Helper
{
    public class EmailHelper
    {
        private string _host;
        private string _from;
        private string _alias; 
        private string _pwd;
        private int _port;

        public EmailHelper(IConfiguration iConfiguration)
        {
            _host = iConfiguration.GetSection("SMTP").GetSection("Host").Value;
            _from = iConfiguration.GetSection("SMTP").GetSection("From").Value;
            _alias = iConfiguration.GetSection("SMTP").GetSection("Alias").Value;
            _pwd = iConfiguration.GetSection("SMTP").GetSection("Pwd").Value;
            _port = Int32.Parse(iConfiguration.GetSection("SMTP").GetSection("Port").Value);
        }

        //public void SendEmail(EmailModel emailModel)
        //{
        //    try
        //    {
        //        using (SmtpClient client = new SmtpClient(_host))
        //        {
                    
        //            MailMessage mailMessage = new MailMessage();
        //            //mailMessage.From = new MailAddress(_from, _alias);
        //            mailMessage.From = new MailAddress(_from, _alias);

        //            mailMessage.BodyEncoding = Encoding.UTF8;

        //            mailMessage.To.Add(emailModel.To);
        //            mailMessage.Body = emailModel.Message;
        //            mailMessage.Subject = emailModel.Subject;
        //            mailMessage.IsBodyHtml = emailModel.IsBodyHtml;
                    

        //            client.UseDefaultCredentials = false;
        //            client.Credentials = new System.Net.NetworkCredential(_from, _pwd);
        //            client.Port = _port; // You can use Port 25 if 587 is blocked (mine is!)
        //            client.Host = _host;//"smtp.office365.com";
        //            client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //            client.EnableSsl = true;

        //            client.Send(mailMessage);
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //}

        public async Task SendEmail(EmailModel mailRequest)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_from);
            email.To.Add(MailboxAddress.Parse(mailRequest.To));
            email.Cc.Add(MailboxAddress.Parse(mailRequest.Cc));
            //email.Bcc.Add(MailboxAddress.Parse(mailRequest.Ccc));
            email.Subject = mailRequest.Subject;
            var builder = new BodyBuilder();
            if (mailRequest.Attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }
            builder.HtmlBody = mailRequest.Message;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            smtp.Connect(_host, _port, SecureSocketOptions.StartTls);
            smtp.Authenticate(_from, _pwd);

            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }

    }
}
