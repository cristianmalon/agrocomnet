﻿using Microsoft.AspNetCore.Http;
using System;using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrocomApi.Helper
{
    public class EmailModel
    {
        public EmailModel() { }
        public EmailModel(string to, string subject, string message, bool isBodyHtml,string cc,string ccc, List<IFormFile> attachments)
        {
            To = to;
            Subject = subject;
            Message = message;
            IsBodyHtml = isBodyHtml;
            Attachments = attachments;
            Cc = cc;
            Ccc = ccc;
        }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public bool IsBodyHtml { get; set; }
        public string  Cc { get; set; }
        public string Ccc { get; set; }

        public List<IFormFile> Attachments { get; set; }
    }
}
