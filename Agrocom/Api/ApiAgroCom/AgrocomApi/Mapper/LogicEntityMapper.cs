﻿using System;
using AutoMapper;
using Common.Notification;
using Common.KPI;

namespace AgrocomApi.Mapper
{
    public static class LogicEntityMapper
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                // This line ensures that internal properties are also mapped over.
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AllowNullCollections = true;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ENotificationResponse, ENotification>();
            CreateMap<ENotification, ENotificationResponse>();

            CreateMap<EScheduleBoardingResponse, EScheduleBoarding>();
            CreateMap<EScheduleBoarding, EScheduleBoardingResponse>();

            CreateMap<EKoreanDestinationResponse, EKoreanDestination>();
            CreateMap<EKoreanDestination, EKoreanDestinationResponse>();

            CreateMap<EShippingCompaniesResponse, EShippingCompanies>();
            CreateMap<EShippingCompanies, EShippingCompaniesResponse>();

            CreateMap<EKpiResponsive, EKpi>();
            CreateMap<EKpi, EKpiResponsive>();
        }
    }
}
