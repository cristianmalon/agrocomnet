﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace AgrocomApi.Models
{
    public class Aga
    {
        private readonly ApplicationDbContext context;

        public Aga(ApplicationDbContext context)
        {
            this.context = context;
        }
        //public void SaveIE(AGAControlEmbarque IE)
        //{
        //    using (var command = context.Database.GetDbConnection().CreateCommand())
        //    {
        //        SP
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //        var pam1 = IE.SCEINstruccion.IdInstruccion;
        //    }
        //}

        //public DataTable SaveCE(int shippingInstructionID,string referencePO, int instructionID,int week,string nroPackingList,string processingPlant,string loadingDay,string uploadDate,string chargingTime,string logisticOperator,string shippingCompany,int TT,string vessel,string ETD,string ETA,string ETAreal,string booking,string customer,string originPort,string destinationPort,string official,string brand,string category,string package,string color,decimal weight,string variety,int quantity,string container,string BL,string bill,decimal totalInvoice,string ediFileDate,string originCertificateDate,string phytosanitaryCertificateDate,string documentTransmissionDate,string approvalDate,string courrierDate,string guideNro,int dayIndicator,string DAM,string receptionDateDAM,string operatorInvoice,decimal dollarAmount,string collectionDocument,decimal solesAmount,string invoiceOtherExpenses,decimal dollar,decimal soles,decimal rebate,string state,decimal VGM)
        public DataTable SaveCE(AGAControlEmbarque item)
        {

            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "exec [package].[sp_shippingInstructionCreateUpdate] @PorderProductionID,@PinstructionID,@Pweek,@PnroPackingList,@PprocessingPlant,@PloadingDay,@PuploadDate,@PchargingTime,@PlogisticOperator,@PshippingCompany,@Pvessel,@PETD,@PETA,@PETAreal,@Pbooking,@Pcustomer,@PoriginPort,@PdestinationPort,@Pofficial,@Pbrand,@Pcategory,@Ppackage,@Pcolor,@Pweight,@Pvariety,@Pquantity,@Pcontainer,@PBL,@Pbill,@PtotalInvoice,@PediFileDate,@PoriginCertificateDate,@PphytosanitaryCertificateDate,@PdocumentTransmissionDate,@PapprovalDate,@PcourrierDate,@PguideNro,@PdayIndicator,@PDAM,@PreceptionDateDAM,@PoperatorInvoice,@PdollarAmount,@PcollectionDocument,@PsolesAmount,@PinvoiceOtherExpenses,@Pdollar,@Psoles,@Prebate,@Pstate,@PVGM,@Pnotify";

                //DbParameter PshippingInstructionID = command.CreateParameter();
                //PshippingInstructionID.ParameterName = "@PshippingInstructionID";
                //PshippingInstructionID.Value = item.;
                //command.Parameters.Add(PshippingInstructionID);

                DbParameter PreferencePO = command.CreateParameter();
                PreferencePO.ParameterName = "@PorderProductionID";
                PreferencePO.Value = item.SCEINstruccion.NroReferencia;
                command.Parameters.Add(PreferencePO);

                DbParameter PinstructionID = command.CreateParameter();
                PinstructionID.ParameterName = "@PinstructionID";
                PinstructionID.Value = item.IdInstruccion;
                command.Parameters.Add(PinstructionID);

                DbParameter Pweek = command.CreateParameter();
                Pweek.ParameterName = "@Pweek";
                Pweek.Value = item.Semana;
                command.Parameters.Add(Pweek);

                item.SCEINstruccion.NroPackingList = item.SCEINstruccion.NroPackingList ?? "";
                DbParameter PnroPackingList = command.CreateParameter();
                PnroPackingList.ParameterName = "@PnroPackingList";
                PnroPackingList.Value = item.SCEINstruccion.NroPackingList;
                command.Parameters.Add(PnroPackingList);

                item.SCEINstruccion.SCESucursal.Descripcion = item.SCEINstruccion.SCESucursal.Descripcion ?? "";
                DbParameter PprocessingPlant = command.CreateParameter();
                PprocessingPlant.ParameterName = "@PprocessingPlant";
                PprocessingPlant.Value = item.SCEINstruccion.SCESucursal.Descripcion;
                command.Parameters.Add(PprocessingPlant);

                DbParameter PloadingDay = command.CreateParameter();
                PloadingDay.ParameterName = "@PloadingDay";
                PloadingDay.Value = item.SCEINstruccion.DiadeCarga;
                command.Parameters.Add(PloadingDay);

                DbParameter PuploadDate = command.CreateParameter();
                PuploadDate.ParameterName = "@PuploadDate";
                PuploadDate.Value = item.SCEINstruccion.SalidaIca;
                command.Parameters.Add(PuploadDate);

                DbParameter PchargingTime = command.CreateParameter();
                PchargingTime.ParameterName = "@PchargingTime";
                PchargingTime.Value = item.SCEINstruccion.HoraLLenado;
                command.Parameters.Add(PchargingTime);

                DbParameter PlogisticOperator = command.CreateParameter();
                PlogisticOperator.ParameterName = "@PlogisticOperator";
                PlogisticOperator.Value = item.SCEINstruccion.SCEOperadorLogistico.Descripcion;
                command.Parameters.Add(PlogisticOperator);

                DbParameter PshippingCompany = command.CreateParameter();
                PshippingCompany.ParameterName = "@PshippingCompany";
                PshippingCompany.Value = item.SCEINstruccion.SCENaviera.Descripcion;
                command.Parameters.Add(PshippingCompany);

                DbParameter Pvessel = command.CreateParameter();
                Pvessel.ParameterName = "@Pvessel";
                Pvessel.Value = item.Vessel;
                command.Parameters.Add(Pvessel);

                DbParameter PETD = command.CreateParameter();
                PETD.ParameterName = "@PETD";
                PETD.Value = item.SCEINstruccion.Etd;
                command.Parameters.Add(PETD);

                DbParameter PETA = command.CreateParameter();
                PETA.ParameterName = "@PETA";
                PETA.Value = item.SCEINstruccion.Eta;
                command.Parameters.Add(PETA);

                DbParameter PETAreal = command.CreateParameter();
                PETAreal.ParameterName = "@PETAreal";
                PETAreal.Value = item.EtaReal;
                command.Parameters.Add(PETAreal);

                DbParameter Pbooking = command.CreateParameter();
                Pbooking.ParameterName = "@Pbooking";
                Pbooking.Value = item.SCEINstruccion.Booking;
                command.Parameters.Add(Pbooking);

                DbParameter Pcustomer = command.CreateParameter();
                Pcustomer.ParameterName = "@Pcustomer";
                Pcustomer.Value = item.SCEINstruccion.SCEClienteProveedor3.RazonSocial;
                command.Parameters.Add(Pcustomer);

                DbParameter PoriginPort = command.CreateParameter();
                PoriginPort.ParameterName = "@PoriginPort";
                PoriginPort.Value = item.SCEINstruccion.SCEPuerto1.Pais;
                command.Parameters.Add(PoriginPort);

                DbParameter PdestinationPort = command.CreateParameter();
                PdestinationPort.ParameterName = "@PdestinationPort";
                PdestinationPort.Value = item.SCEINstruccion.SCEPuerto2.Pais;
                command.Parameters.Add(PdestinationPort);

                DbParameter Pofficial = command.CreateParameter();
                Pofficial.ParameterName = "@Pofficial";
                Pofficial.Value = item.SCEINstruccion.SCEEncargado.Descripcion;
                command.Parameters.Add(Pofficial);

                DbParameter Pbrand = command.CreateParameter();
                Pbrand.ParameterName = "@Pbrand";
                Pbrand.Value = item.SCEINstruccion.SCEInstruccionDetalleM.SCEMarca.Descripcion;
                command.Parameters.Add(Pbrand);

                DbParameter Pcategory = command.CreateParameter();
                Pcategory.ParameterName = "@Pcategory";
                Pcategory.Value = item.SCEINstruccion.SCEInstruccionDetalleM.SCECategoria.Descripcion;
                command.Parameters.Add(Pcategory);

                DbParameter Ppackage = command.CreateParameter();
                Ppackage.ParameterName = "@Ppackage";
                Ppackage.Value = item.SCEINstruccion.SCEInstruccionDetalleM.Envase;
                command.Parameters.Add(Ppackage);

                DbParameter Pcolor = command.CreateParameter();
                Pcolor.ParameterName = "@Pcolor";
                Pcolor.Value = item.SCEINstruccion.SCEInstruccionDetalleM.Color;
                command.Parameters.Add(Pcolor);

                DbParameter Pweight = command.CreateParameter();
                Pweight.ParameterName = "@Pweight";
                Pweight.Value = item.SCEINstruccion.SCEInstruccionDetalleM.Peso;
                command.Parameters.Add(Pweight);

                DbParameter Pvariety = command.CreateParameter();
                Pvariety.ParameterName = "@Pvariety";
                Pvariety.Value = item.SCEINstruccion.SCEInstruccionDetalleM.SCEVariedad.Descripcion;
                command.Parameters.Add(Pvariety);

                DbParameter Pquantity = command.CreateParameter();
                Pquantity.ParameterName = "@Pquantity";
                Pquantity.Value = item.SCEINstruccion.SCEInstruccionDetalleM.Cantidad;
                command.Parameters.Add(Pquantity);

                DbParameter Pcontainer = command.CreateParameter();
                Pcontainer.ParameterName = "@Pcontainer";
                Pcontainer.Value = item.Contenedor;
                command.Parameters.Add(Pcontainer);

                item.BL = item.BL ?? "";
                DbParameter PBL = command.CreateParameter();
                PBL.ParameterName = "@PBL";
                PBL.Value = item.BL;
                command.Parameters.Add(PBL);

                item.Factura = item.Factura ?? "";
                DbParameter Pbill = command.CreateParameter();
                Pbill.ParameterName = "@Pbill";
                Pbill.Value = item.Factura;
                command.Parameters.Add(Pbill);

                DbParameter PtotalInvoice = command.CreateParameter();
                PtotalInvoice.ParameterName = "@PtotalInvoice";
                PtotalInvoice.Value = item.CFR;
                command.Parameters.Add(PtotalInvoice);

                DbParameter PediFileDate = command.CreateParameter();
                PediFileDate.ParameterName = "@PediFileDate";
                PediFileDate.Value = item.EdiFile;
                command.Parameters.Add(PediFileDate);

                DbParameter PoriginCertificateDate = command.CreateParameter();
                PoriginCertificateDate.ParameterName = "@PoriginCertificateDate";
                PoriginCertificateDate.Value = item.CertificadoOrigen;
                command.Parameters.Add(PoriginCertificateDate);

                DbParameter PphytosanitaryCertificateDate = command.CreateParameter();
                PphytosanitaryCertificateDate.ParameterName = "@PphytosanitaryCertificateDate";
                PphytosanitaryCertificateDate.Value = item.CertificadoFito;
                command.Parameters.Add(PphytosanitaryCertificateDate);

                DbParameter PdocumentTransmissionDate = command.CreateParameter();
                PdocumentTransmissionDate.ParameterName = "@PdocumentTransmissionDate";
                PdocumentTransmissionDate.Value = item.TransmisionDoc;
                command.Parameters.Add(PdocumentTransmissionDate);

                DbParameter PapprovalDate = command.CreateParameter();
                PapprovalDate.ParameterName = "@PapprovalDate";
                PapprovalDate.Value = item.Aprobacion;
                command.Parameters.Add(PapprovalDate);

                DbParameter PcourrierDate = command.CreateParameter();
                PcourrierDate.ParameterName = "@PcourrierDate";
                PcourrierDate.Value = item.Courrier;
                command.Parameters.Add(PcourrierDate);

                item.Guia = item.Guia ?? "";
                DbParameter PguideNro = command.CreateParameter();
                PguideNro.ParameterName = "@PguideNro";
                PguideNro.Value = item.Guia;
                command.Parameters.Add(PguideNro);

                DbParameter PdayIndicator = command.CreateParameter();
                PdayIndicator.ParameterName = "@PdayIndicator";
                PdayIndicator.Value = item.IndicadorDia;
                command.Parameters.Add(PdayIndicator);

                item.DAM = item.DAM ?? "";
                DbParameter PDAM = command.CreateParameter();
                PDAM.ParameterName = "@PDAM";
                PDAM.Value = item.DAM;
                command.Parameters.Add(PDAM);

                DbParameter PreceptionDateDAM = command.CreateParameter();
                PreceptionDateDAM.ParameterName = "@PreceptionDateDAM";
                PreceptionDateDAM.Value = item.FechaRecepcionDAM;
                command.Parameters.Add(PreceptionDateDAM);

                item.FacturaOperador = item.FacturaOperador ?? "";
                DbParameter PoperatorInvoice = command.CreateParameter();
                PoperatorInvoice.ParameterName = "@PoperatorInvoice";
                PoperatorInvoice.Value = item.FacturaOperador;
                command.Parameters.Add(PoperatorInvoice);

                DbParameter PdollarAmount = command.CreateParameter();
                PdollarAmount.ParameterName = "@PdollarAmount";
                PdollarAmount.Value = item.MontoD;
                command.Parameters.Add(PdollarAmount);

                item.DocumentoCobranza = item.DocumentoCobranza ?? "";
                DbParameter PcollectionDocument = command.CreateParameter();
                PcollectionDocument.ParameterName = "@PcollectionDocument";
                PcollectionDocument.Value = item.DocumentoCobranza;
                command.Parameters.Add(PcollectionDocument);

                DbParameter PsolesAmount = command.CreateParameter();
                PsolesAmount.ParameterName = "@PsolesAmount";
                PsolesAmount.Value = item.MontoS;
                command.Parameters.Add(PsolesAmount);

                item.FactOtrosGastos = item.FactOtrosGastos ?? "";
                DbParameter PinvoiceOtherExpenses = command.CreateParameter();
                PinvoiceOtherExpenses.ParameterName = "@PinvoiceOtherExpenses";
                PinvoiceOtherExpenses.Value = item.FactOtrosGastos;
                command.Parameters.Add(PinvoiceOtherExpenses);

                DbParameter Pdollar = command.CreateParameter();
                Pdollar.ParameterName = "@Pdollar";
                Pdollar.Value = item.D;
                command.Parameters.Add(Pdollar);

                DbParameter Psoles = command.CreateParameter();
                Psoles.ParameterName = "@Psoles";
                Psoles.Value = item.S;
                command.Parameters.Add(Psoles);

                DbParameter Prebate = command.CreateParameter();
                Prebate.ParameterName = "@Prebate";
                Prebate.Value = item.Rebate;
                command.Parameters.Add(Prebate);

                item.Estado = item.Estado ?? "";
                DbParameter Pstate = command.CreateParameter();
                Pstate.ParameterName = "@Pstate";
                Pstate.Value = item.Estado;
                command.Parameters.Add(Pstate);

                //item.SCEINstruccion.VGM = item.SCEINstruccion.VGM =0;
                DbParameter PVGM = command.CreateParameter();
                PVGM.ParameterName = "@PVGM";
                PVGM.Value = item.SCEINstruccion.VGM;
                command.Parameters.Add(PVGM);

                DbParameter Pnotify = command.CreateParameter();
                Pnotify.ParameterName = "@Pnotify";
                Pnotify.Value = item.SCEINstruccion.Notificante;
                command.Parameters.Add(Pnotify);

                context.Database.OpenConnection();
                var result = command.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(result);
                return (dataTable);
            }
        }

        public DataTable shippingAGAList(string opt)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "exec [package].[sp_shippingInstructionAGAList] @PsearchOpt";

                DbParameter PsearchOpt = command.CreateParameter();
                PsearchOpt.ParameterName = "@PsearchOpt";
                PsearchOpt.Value = opt;
                command.Parameters.Add(PsearchOpt);

                context.Database.OpenConnection();
                var result = command.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(result);
                return (dataTable);
            }
        }

        public List<PK> GetAGAPacking(string searchOpt)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "exec [package].[sp_shippingInstructionAGAList] @PsearchOpt";

                DbParameter PsearchOpt = command.CreateParameter();
                PsearchOpt.ParameterName = "@PsearchOpt";
                PsearchOpt.Value = searchOpt;
                command.Parameters.Add(PsearchOpt);

                context.Database.OpenConnection();
                var result = command.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(result);

                var Lista = new List<PK>();

                foreach (DataRow item in dataTable.Rows)
                {
                    var o = new PK();
                    o.nroPackingList = item["nroPackingList"].ToString();
                    Lista.Add(o);
                }
                return (Lista);

            }
        }

        public DataTable SavePK(PackingListNisira item)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "exec [package].[sp_packingListPOAGACreateUpdate] @PnroPackingList,@Pgrower,@Pinvoice,@Pguide,@Pcustomer,@Pcontainer,@Psenasa,@Pcustom,@Pthermo,@PpackingDate,@Porigin,@Pdestination,@PETD,@PETA";

                DbParameter PnroPackingList = command.CreateParameter();
                PnroPackingList.ParameterName = "@PnroPackingList";
                PnroPackingList.Value = item.NroPackingList;
                command.Parameters.Add(PnroPackingList);

                DbParameter Pgrower = command.CreateParameter();
                Pgrower.ParameterName = "@Pgrower";
                Pgrower.Value = item.Productor;
                command.Parameters.Add(Pgrower);

                DbParameter Pinvoice = command.CreateParameter();
                Pinvoice.ParameterName = "@Pinvoice";
                Pinvoice.Value = item.invoice;
                command.Parameters.Add(Pinvoice);

                DbParameter Pguide = command.CreateParameter();
                Pguide.ParameterName = "@Pguide";
                Pguide.Value = item.remision;
                command.Parameters.Add(Pguide);

                DbParameter Pcustomer = command.CreateParameter();
                Pcustomer.ParameterName = "@Pcustomer";
                Pcustomer.Value = item.cliente;
                command.Parameters.Add(Pcustomer);

                item.container = item.container ?? "";
                DbParameter Pcontainer = command.CreateParameter();
                Pcontainer.ParameterName = "@Pcontainer";
                Pcontainer.Value = item.container;
                command.Parameters.Add(Pcontainer);

                item.precintosenasa = item.precintosenasa ?? "";
                DbParameter Psenasa = command.CreateParameter();
                Psenasa.ParameterName = "@Psenasa";
                Psenasa.Value = item.precintosenasa;
                command.Parameters.Add(Psenasa);

                item.precintoaduana = item.precintoaduana ?? "";
                DbParameter Pcustom = command.CreateParameter();
                Pcustom.ParameterName = "@Pcustom";
                Pcustom.Value = item.precintoaduana;
                command.Parameters.Add(Pcustom);

                item.thermoregistro = item.thermoregistro ?? "";
                DbParameter Pthermo = command.CreateParameter();
                Pthermo.ParameterName = "@Pthermo";
                Pthermo.Value = item.thermoregistro;
                command.Parameters.Add(Pthermo);

                DbParameter PpackingDate = command.CreateParameter();
                PpackingDate.ParameterName = "@PpackingDate";
                PpackingDate.Value = item.fechatraslado;
                command.Parameters.Add(PpackingDate);

                DbParameter Porigin = command.CreateParameter();
                Porigin.ParameterName = "@Porigin";
                Porigin.Value = item.ptoorigen;
                command.Parameters.Add(Porigin);

                DbParameter Pdestination = command.CreateParameter();
                Pdestination.ParameterName = "@Pdestination";
                Pdestination.Value = item.ptodestino;
                command.Parameters.Add(Pdestination);

                DbParameter PETD = command.CreateParameter();
                PETD.ParameterName = "@PETD";
                PETD.Value = item.fechaembarque;
                command.Parameters.Add(PETD);

                DbParameter PETA = command.CreateParameter();
                PETA.ParameterName = "@PETA";
                PETA.Value = item.fechaarribo;
                command.Parameters.Add(PETA);

                context.Database.OpenConnection();
                var result = command.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(result);
                return (dataTable);
            }
        }
        
        public DataTable SavePKDetail(packingListDetalle item,string NroPackingList)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "exec [package].[sp_packingDetailListPOAGACreateUpdate] @PnroPackingList,@Porder,@Ppaleta,@Pvariety,@Ppackage,@Pbrand,@Ppresentation,@Pembalaje,@Psize,@Ptotal,@PprocessingDate,@Pfarm";

                DbParameter PnroPackingList = command.CreateParameter();
                PnroPackingList.ParameterName = "@PnroPackingList";
                PnroPackingList.Value = NroPackingList;
                command.Parameters.Add(PnroPackingList);

                DbParameter Porder = command.CreateParameter();
                Porder.ParameterName = "@Porder";
                Porder.Value = item.item;
                command.Parameters.Add(Porder);

                DbParameter Ppaleta = command.CreateParameter();
                Ppaleta.ParameterName = "@Ppaleta";
                Ppaleta.Value = item.nropaleta;
                command.Parameters.Add(Ppaleta);

                DbParameter Pvariety = command.CreateParameter();
                Pvariety.ParameterName = "@Pvariety";
                Pvariety.Value = item.variedad;
                command.Parameters.Add(Pvariety);

                DbParameter Ppackage = command.CreateParameter();
                Ppackage.ParameterName = "@Ppackage";
                Ppackage.Value = item.peso;
                command.Parameters.Add(Ppackage);

                DbParameter Pbrand = command.CreateParameter();
                Pbrand.ParameterName = "@Pbrand";
                Pbrand.Value = item.etiqueta;
                command.Parameters.Add(Pbrand);

                DbParameter Ppresentation = command.CreateParameter();
                Ppresentation.ParameterName = "@Ppresentation";
                Ppresentation.Value = item.desc_emb;
                command.Parameters.Add(Ppresentation);
               
                DbParameter Pembalaje = command.CreateParameter();
                Pembalaje.ParameterName = "@Pembalaje";
                Pembalaje.Value = item.desc_emb_la;
                command.Parameters.Add(Pembalaje);

                DbParameter Psize = command.CreateParameter();
                Psize.ParameterName = "@Psize";
                Psize.Value = item.calibre;
                command.Parameters.Add(Psize);

                DbParameter Ptotal = command.CreateParameter();
                Ptotal.ParameterName = "@Ptotal";
                Ptotal.Value = item.total;
                command.Parameters.Add(Ptotal);

                DbParameter Pdate = command.CreateParameter();
                Pdate.ParameterName = "@PprocessingDate";
                Pdate.Value = item.fecha_proceso;
                command.Parameters.Add(Pdate);

                DbParameter Pfundo = command.CreateParameter();
                Pfundo.ParameterName = "@Pfarm";
                Pfundo.Value = item.fundo;
                command.Parameters.Add(Pfundo);

                context.Database.OpenConnection();
                var result = command.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(result);
                return (dataTable);
            }
        }

        public DataTable SaveProjReal(projectReal item, int year, int week)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "exec [package].[sp_projectedRealCreateUpdate] @Pyear,@Pweek,@Pcondition,@Ptotal,@Pfarm,@Pvariety,@Pday";

                DbParameter Pyear = command.CreateParameter();
                Pyear.ParameterName = "@Pyear";
                Pyear.Value = year;
                command.Parameters.Add(Pyear);

                DbParameter Pweek = command.CreateParameter();
                Pweek.ParameterName = "@Pweek";
                Pweek.Value = week;
                command.Parameters.Add(Pweek);

                DbParameter Pcondition = command.CreateParameter();
                Pcondition.ParameterName = "@Pcondition";
                Pcondition.Value = item.condicion;
                command.Parameters.Add(Pcondition);

                DbParameter Ptotal = command.CreateParameter();
                Ptotal.ParameterName = "@Ptotal";
                Ptotal.Value = item.totalKilos;
                command.Parameters.Add(Ptotal);

                item.fundo = item.fundo ?? "";
                DbParameter Pfarm = command.CreateParameter();
                Pfarm.ParameterName = "@Pfarm";
                Pfarm.Value = item.fundo;
                command.Parameters.Add(Pfarm);

                DbParameter Pvariety = command.CreateParameter();
                Pvariety.ParameterName = "@Pvariety";
                Pvariety.Value = item.variedad;
                command.Parameters.Add(Pvariety);

                DbParameter Pday = command.CreateParameter();
                Pday.ParameterName = "@Pday";
                Pday.Value = item.diaSemana;
                command.Parameters.Add(Pday);

                context.Database.OpenConnection();
                var result = command.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(result);
                return (dataTable);
            }
        }

        
    }
}
