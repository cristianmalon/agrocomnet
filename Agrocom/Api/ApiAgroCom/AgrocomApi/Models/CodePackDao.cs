﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class CodePackDao
    {
        private readonly ApplicationDbContext context;

        public CodePackDao(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetCodepack(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_codepackList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        
        public DataTable GetCodepackSizeByCrop( string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_codepackCommercial]  @PcropID";

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetCodepackVic(string opt, string viaid, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_codepackList] @PsearchOpt, @PsearchValue, @PcropID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = viaid;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListSizeBoxByFilter(string formatID, string packageProductID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_CommercialPlan_ListSizeBoxByFilter] @PformatID, @PpackageProductID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PformatID";
                    PsearchOpt.Value = formatID;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PpackageProductID";
                    PsearchValue.Value = packageProductID;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetCodePackCreate(string description, int? clamshellPerBox, double? weight, int? boxesPerPallet, string layersPerPallet, int? clamshellPerContainer, int? boxPerContainer, double? netWeightContainer, int viaID, int statusID, string cropID)
        {
            description = description ?? "";
            clamshellPerBox = clamshellPerBox ?? 0;
            weight = weight ?? 0.0;
            boxesPerPallet = boxesPerPallet ?? 0;
            layersPerPallet = layersPerPallet ?? "";
            clamshellPerContainer = clamshellPerContainer ?? 0;
            boxPerContainer = boxPerContainer ?? 0;
            netWeightContainer = netWeightContainer ?? 0.0;
            DbDataReader result = null;

            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_codepackCreate] @Pdescription	,@PclamshellPerBox, @Pweight," +
                        " @PboxesPerPallet, @PlayersPerPallet, @PclamshellPerContainer, @PboxPerContainer, @PnetWeightContainer ," +
                        "@PviaID, @PstatusID, @PcropID";

                    DbParameter Pdescription = command.CreateParameter();
                    Pdescription.ParameterName = "@Pdescription";
                    Pdescription.Value = description;
                    command.Parameters.Add(Pdescription);

                    DbParameter PclamshellPerBox = command.CreateParameter();
                    PclamshellPerBox.ParameterName = "@PclamshellPerBox";
                    PclamshellPerBox.Value = clamshellPerBox;
                    command.Parameters.Add(PclamshellPerBox);

                    DbParameter Pweight = command.CreateParameter();
                    Pweight.ParameterName = "@Pweight";
                    Pweight.Value = weight;
                    command.Parameters.Add(Pweight);

                    DbParameter PboxesPerPallet = command.CreateParameter();
                    PboxesPerPallet.ParameterName = "@PboxesPerPallet";
                    PboxesPerPallet.Value = boxesPerPallet;
                    command.Parameters.Add(PboxesPerPallet);

                    DbParameter PlayersPerPallet = command.CreateParameter();
                    PlayersPerPallet.ParameterName = "@PlayersPerPallet";
                    PlayersPerPallet.Value = layersPerPallet;
                    command.Parameters.Add(PlayersPerPallet);

                    DbParameter PclamshellPerContainer = command.CreateParameter();
                    PclamshellPerContainer.ParameterName = "@PclamshellPerContainer";
                    PclamshellPerContainer.Value = clamshellPerContainer;
                    command.Parameters.Add(PclamshellPerContainer);

                    DbParameter PboxPerContainer = command.CreateParameter();
                    PboxPerContainer.ParameterName = "@PboxPerContainer";
                    PboxPerContainer.Value = boxPerContainer;
                    command.Parameters.Add(PboxPerContainer);

                    DbParameter PnetWeightContainer = command.CreateParameter();
                    PnetWeightContainer.ParameterName = "@PnetWeightContainer";
                    PnetWeightContainer.Value = netWeightContainer;
                    command.Parameters.Add(PnetWeightContainer);

                    DbParameter PviaID = command.CreateParameter();
                    PviaID.ParameterName = "@PviaID";
                    PviaID.Value = viaID;
                    command.Parameters.Add(PviaID);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetCodePackUpdate(int codePackID, string description, int? clamshellPerBox, double? weight, int? boxesPerPallet, string layersPerPallet, int? clamshellPerContainer, int? boxPerContainer, double? netWeightContainer)
        {
            description = description ?? "";
            clamshellPerBox = clamshellPerBox ?? 0;
            weight = weight ?? 0.0;
            boxesPerPallet = boxesPerPallet ?? 0;
            layersPerPallet = layersPerPallet ?? "";
            clamshellPerContainer = clamshellPerContainer ?? 0;
            boxPerContainer = boxPerContainer ?? 0;
            netWeightContainer = netWeightContainer ?? 0.0;
            DbDataReader result = null;

            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_codepackModify] @PcodePackID, @Pdescription, @PclamshellPerBox, @Pweight	,@PboxesperPallet, @PlayersPerPallet ,@PclamshellPerContainer, @PboxPerContainer, @PnetWeightContainer";

                    DbParameter PcodePackID = command.CreateParameter();
                    PcodePackID.ParameterName = "@PcodePackID";
                    PcodePackID.Value = codePackID;
                    command.Parameters.Add(PcodePackID);

                    DbParameter Pdescription = command.CreateParameter();
                    Pdescription.ParameterName = "@Pdescription";
                    Pdescription.Value = description;
                    command.Parameters.Add(Pdescription);

                    DbParameter PclamshellPerBox = command.CreateParameter();
                    PclamshellPerBox.ParameterName = "@PclamshellPerBox";
                    PclamshellPerBox.Value = clamshellPerBox;
                    command.Parameters.Add(PclamshellPerBox);

                    DbParameter Pweight = command.CreateParameter();
                    Pweight.ParameterName = "@Pweight";
                    Pweight.Value = weight;
                    command.Parameters.Add(Pweight);

                    DbParameter PboxesPerPallet = command.CreateParameter();
                    PboxesPerPallet.ParameterName = "@PboxesPerPallet";
                    PboxesPerPallet.Value = boxesPerPallet;
                    command.Parameters.Add(PboxesPerPallet);

                    DbParameter PlayersPerPallet = command.CreateParameter();
                    PlayersPerPallet.ParameterName = "@PlayersPerPallet";
                    PlayersPerPallet.Value = layersPerPallet;
                    command.Parameters.Add(PlayersPerPallet);

                    DbParameter PclamshellPerContainer = command.CreateParameter();
                    PclamshellPerContainer.ParameterName = "@PclamshellPerContainer";
                    PclamshellPerContainer.Value = clamshellPerContainer;
                    command.Parameters.Add(PclamshellPerContainer);

                    DbParameter PboxPerContainer = command.CreateParameter();
                    PboxPerContainer.ParameterName = "@PboxPerContainer";
                    PboxPerContainer.Value = boxPerContainer;
                    command.Parameters.Add(PboxPerContainer);

                    DbParameter PnetWeightContainer = command.CreateParameter();
                    PnetWeightContainer.ParameterName = "@PnetWeightContainer";
                    PnetWeightContainer.Value = netWeightContainer;
                    command.Parameters.Add(PnetWeightContainer);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetCodepack2(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_codepackList2] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
