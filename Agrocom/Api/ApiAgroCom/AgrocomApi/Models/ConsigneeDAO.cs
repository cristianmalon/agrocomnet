﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;


namespace AgrocomApi.Models
{
    public class ConsigneeDAO
    {
        private readonly ApplicationDbContext context;

        public ConsigneeDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetConsignee(string opt, string id, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_consigneeList] @PsearchOpt, @PsearchValue, @PcropID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetConsDestCreate(int consigneeDestinationID, int customertID, int destinationID, int consigneeID, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_saveConsigneeDestination] @PconsigneeDestinationID, @PcustomertID, @PdestinationID, @PconsigneeID, @PcropID";

                    DbParameter PconsigneeDestinationID = command.CreateParameter();
                    PconsigneeDestinationID.ParameterName = "@PconsigneeDestinationID";
                    PconsigneeDestinationID.Value = consigneeDestinationID;
                    command.Parameters.Add(PconsigneeDestinationID);

                    DbParameter PcustomertID = command.CreateParameter();
                    PcustomertID.ParameterName = "@PcustomertID";
                    PcustomertID.Value = customertID;
                    command.Parameters.Add(PcustomertID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PconsigneeID = command.CreateParameter();
                    PconsigneeID.ParameterName = "@PconsigneeID";
                    PconsigneeID.Value = consigneeID;
                    command.Parameters.Add(PconsigneeID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetSpecsConsDestCreate(int customerConsigneeDestinationSpecsID, int consigneeDestinationID, int varietyID, int formatID, int packageProductID, int generic, string categoryID, int brandID, int presentationID, string sizesID, decimal price, string codepackName, string file, int principal, int statusID, string comments, string notes)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_saveConsigneeDestinationSpecs] @PcustomerConsigneeDestinationSpecsID, @PconsigneeDestinationID, @PvarietyID, @PformatID, @PpackageProductID, @Pgeneric, @PcategoryID, @PbrandID, @PpresentationID, @PsizesID, @Pprice, @PcodepackName, @Pfile, @Pprincipal, @PstatusID, @Pcomments, @Pnotes";

                    DbParameter PcustomerConsigneeDestinationSpecsID = command.CreateParameter();
                    PcustomerConsigneeDestinationSpecsID.ParameterName = "@PcustomerConsigneeDestinationSpecsID";
                    PcustomerConsigneeDestinationSpecsID.Value = customerConsigneeDestinationSpecsID;
                    command.Parameters.Add(PcustomerConsigneeDestinationSpecsID);

                    DbParameter PconsigneeDestinationID = command.CreateParameter();
                    PconsigneeDestinationID.ParameterName = "@PconsigneeDestinationID";
                    PconsigneeDestinationID.Value = consigneeDestinationID;
                    command.Parameters.Add(PconsigneeDestinationID);

                    DbParameter PvarietyID = command.CreateParameter();
                    PvarietyID.ParameterName = "@PvarietyID";
                    PvarietyID.Value = varietyID;
                    command.Parameters.Add(PvarietyID);

                    DbParameter PformatID = command.CreateParameter();
                    PformatID.ParameterName = "@PformatID";
                    PformatID.Value = formatID;
                    command.Parameters.Add(PformatID);

                    DbParameter PpackageProductID = command.CreateParameter();
                    PpackageProductID.ParameterName = "@PpackageProductID";
                    PpackageProductID.Value = packageProductID;
                    command.Parameters.Add(PpackageProductID);

                    DbParameter Pgeneric = command.CreateParameter();
                    Pgeneric.ParameterName = "@Pgeneric";
                    Pgeneric.Value = generic;
                    command.Parameters.Add(Pgeneric);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = categoryID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PbrandID = command.CreateParameter();
                    PbrandID.ParameterName = "@PbrandID";
                    PbrandID.Value = brandID;
                    command.Parameters.Add(PbrandID);

                    DbParameter PpresentationID = command.CreateParameter();
                    PpresentationID.ParameterName = "@PpresentationID ";
                    PpresentationID.Value = presentationID;
                    command.Parameters.Add(PpresentationID);

                    DbParameter PsizesID = command.CreateParameter();
                    PsizesID.ParameterName = "@PsizesID";
                    PsizesID.Value = sizesID;
                    command.Parameters.Add(PsizesID);

                    DbParameter Pprice = command.CreateParameter();
                    Pprice.ParameterName = "@Pprice";
                    Pprice.Value = price;
                    command.Parameters.Add(Pprice);

                    DbParameter PcodepackName = command.CreateParameter();
                    PcodepackName.ParameterName = "@PcodepackName";
                    PcodepackName.Value = codepackName;
                    command.Parameters.Add(PcodepackName);

                    DbParameter Pfile = command.CreateParameter();
                    Pfile.ParameterName = "@Pfile";
                    Pfile.Value = file;
                    command.Parameters.Add(Pfile);

                    DbParameter Pprincipal = command.CreateParameter();
                    Pprincipal.ParameterName = "@Pprincipal";
                    Pprincipal.Value = principal;
                    command.Parameters.Add(Pprincipal);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter Pnotes = command.CreateParameter();
                    Pnotes.ParameterName = "@Pnotes";
                    Pnotes.Value = notes;
                    command.Parameters.Add(Pnotes);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
