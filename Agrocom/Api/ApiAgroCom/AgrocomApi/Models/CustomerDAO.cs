﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace AgrocomApi.Models
{
    public class CustomerDAO
    {
        private readonly ApplicationDbContext context;

        public CustomerDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetCustomer(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetCustomer(string opt, string id, string searchName)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerList] @PsearchOpt, @PsearchValue, @PsearchName";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PsearchName = command.CreateParameter();
                    PsearchName.ParameterName = "@PsearchName";
                    PsearchName.Value = searchName;
                    command.Parameters.Add(PsearchName);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetCreate(int userID, string name, string address, string contact, string phone, string email, string abbreviation, string idOrigin,string moreInfo, int customer, int consignee, int notify, string ruc, string subsidiaryID, List<CustomerDestination> destinations)
        {
            name = name ?? "";
            address = address ?? "";
            contact = contact ?? "";
            phone = phone ?? "";
            email = email ?? "";
            idOrigin = idOrigin ?? "";
            abbreviation = abbreviation ?? "";
            moreInfo = moreInfo ?? "";
            ruc = ruc ?? "";
            subsidiaryID = subsidiaryID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerCreate] @PuserID,@Pname,@Paddress,@Pcontact,@Pphone,@Pemail,@Pabbreviation,@PoriginID,@PmoreInfo,@Pcustomer,@Pconsignee,@Pnotify,@Pruc,@PsubsidiaryID,@Pdestination_table";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter Pname = command.CreateParameter();
                    Pname.ParameterName = "@Pname";
                    Pname.Value = name;
                    command.Parameters.Add(Pname);

                    DbParameter Paddress = command.CreateParameter();
                    Paddress.ParameterName = "@Paddress";
                    Paddress.Value = address;
                    command.Parameters.Add(Paddress);                    

                    DbParameter Pcontact = command.CreateParameter();
                    Pcontact.ParameterName = "@Pcontact";
                    Pcontact.Value = contact;
                    command.Parameters.Add(Pcontact);

                    DbParameter Pphone = command.CreateParameter();
                    Pphone.ParameterName = "@Pphone";
                    Pphone.Value = phone;
                    command.Parameters.Add(Pphone);

                    DbParameter Pemail = command.CreateParameter();
                    Pemail.ParameterName = "@Pemail";
                    Pemail.Value = email;
                    command.Parameters.Add(Pemail);

                    DbParameter Pabbreviation = command.CreateParameter();
                    Pabbreviation.ParameterName = "@Pabbreviation";
                    Pabbreviation.Value = abbreviation;
                    command.Parameters.Add(Pabbreviation);

                    DbParameter PoriginID = command.CreateParameter();
                    PoriginID.ParameterName = "@PoriginID";
                    PoriginID.Value = idOrigin;
                    command.Parameters.Add(PoriginID);

                    DbParameter PmoreInfo = command.CreateParameter();
                    PmoreInfo.ParameterName = "@PmoreInfo";
                    PmoreInfo.Value = moreInfo;
                    command.Parameters.Add(PmoreInfo);                    

                    DbParameter Pcustomer = command.CreateParameter();
                    Pcustomer.ParameterName = "@Pcustomer";
                    Pcustomer.Value = customer;
                    command.Parameters.Add(Pcustomer);

                    DbParameter Pconsignee = command.CreateParameter();
                    Pconsignee.ParameterName = "@Pconsignee";
                    Pconsignee.Value = consignee;
                    command.Parameters.Add(Pconsignee);

                    DbParameter Pnotify = command.CreateParameter();
                    Pnotify.ParameterName = "@Pnotify";
                    Pnotify.Value = notify;
                    command.Parameters.Add(Pnotify);

                    DbParameter Pruc = command.CreateParameter();
                    Pruc.ParameterName = "@Pruc";
                    Pruc.Value = ruc;
                    command.Parameters.Add(Pruc);

                    DbParameter PsubsidiaryID = command.CreateParameter();
                    PsubsidiaryID.ParameterName = "@PsubsidiaryID";
                    PsubsidiaryID.Value = subsidiaryID;
                    command.Parameters.Add(PsubsidiaryID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("destinationID");
                    tbl.Columns.Add("cropID");
                    tbl.Columns.Add("brix");
                    tbl.Columns.Add("acidity");
                    tbl.Columns.Add("bloom");
                    tbl.Columns.Add("traceability");
                    tbl.Columns.Add("comments");
                    tbl.Columns.Add("comments2");
                    tbl.Columns.Add("tolerance");

                    foreach (var item in destinations)
                    {
                        tbl.Rows.Add(item.destinationID, item.cropID, item.brix, item.acidity, item.bloom, item.traceability, item.comments, item.comments2, item.tolerance);
                    }

                    var Pdestination_table = new SqlParameter("@Pdestination_table", SqlDbType.Structured);
                    Pdestination_table.Value = tbl;
                    Pdestination_table.TypeName = "[sales].[customDestin]";
                    command.Parameters.Add(Pdestination_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetUpdate(int userID, string customerID, string name, string address, string contact, string phone, string email, string abbreviation, string moreInfo, int customer, int consignee, int notify, string ruc, string subsidiaryID, List<CustomerDestination> destinations)
        {
            name = name ?? "";
            address = address ?? "";
            contact = contact ?? "";
            phone = phone ?? "";
            email = email ?? "";
            abbreviation = abbreviation ?? "";
            moreInfo = moreInfo ?? "";
            ruc = ruc ?? "";
            subsidiaryID = subsidiaryID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerModify] @PuserID,@PcustomerID,@Pname,@Paddress,@Pcontact,@Pphone,@Pemail,@Pabbreviation,@PmoreInfo,@Pcustomer,@Pconsignee,@Pnotify,@Pruc,@PsubsidiaryID,@Pdestination_table";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter Pname = command.CreateParameter();
                    Pname.ParameterName = "@Pname";
                    Pname.Value = name;
                    command.Parameters.Add(Pname);

                    DbParameter Paddress = command.CreateParameter();
                    Paddress.ParameterName = "@Paddress";
                    Paddress.Value = address;
                    command.Parameters.Add(Paddress);

                    DbParameter Pcontact = command.CreateParameter();
                    Pcontact.ParameterName = "@Pcontact";
                    Pcontact.Value = contact;
                    command.Parameters.Add(Pcontact);

                    DbParameter Pphone = command.CreateParameter();
                    Pphone.ParameterName = "@Pphone";
                    Pphone.Value = phone;
                    command.Parameters.Add(Pphone);

                    DbParameter Pemail = command.CreateParameter();
                    Pemail.ParameterName = "@Pemail";
                    Pemail.Value = email;
                    command.Parameters.Add(Pemail);

                    DbParameter Pabbreviation = command.CreateParameter();
                    Pabbreviation.ParameterName = "@Pabbreviation";
                    Pabbreviation.Value = abbreviation;
                    command.Parameters.Add(Pabbreviation);

                    DbParameter PmoreInfo = command.CreateParameter();
                    PmoreInfo.ParameterName = "@PmoreInfo";
                    PmoreInfo.Value = moreInfo;
                    command.Parameters.Add(PmoreInfo);

                    DbParameter Pcustomer = command.CreateParameter();
                    Pcustomer.ParameterName = "@Pcustomer";
                    Pcustomer.Value = customer;
                    command.Parameters.Add(Pcustomer);

                    DbParameter Pconsignee = command.CreateParameter();
                    Pconsignee.ParameterName = "@Pconsignee";
                    Pconsignee.Value = consignee;
                    command.Parameters.Add(Pconsignee);

                    DbParameter Pnotify = command.CreateParameter();
                    Pnotify.ParameterName = "@Pnotify";
                    Pnotify.Value = notify;
                    command.Parameters.Add(Pnotify);

                    DbParameter Pruc = command.CreateParameter();
                    Pruc.ParameterName = "@Pruc";
                    Pruc.Value = ruc;
                    command.Parameters.Add(Pruc);

                    DbParameter PsubsidiaryID = command.CreateParameter();
                    PsubsidiaryID.ParameterName = "@PsubsidiaryID";
                    PsubsidiaryID.Value = subsidiaryID;
                    command.Parameters.Add(PsubsidiaryID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("destinationID");
                    tbl.Columns.Add("cropID");
                    tbl.Columns.Add("brix");
                    tbl.Columns.Add("acidity");
                    tbl.Columns.Add("bloom");
                    tbl.Columns.Add("traceability");
                    tbl.Columns.Add("comments");
                    tbl.Columns.Add("comments2");
                    tbl.Columns.Add("tolerance");

                    foreach (var item in destinations)
                    {
                        tbl.Rows.Add(item.destinationID, item.cropID, item.brix, item.acidity, item.bloom, item.traceability, item.comments, item.comments2, item.tolerance);
                    }

                    var Pdestination_table = new SqlParameter("@Pdestination_table", SqlDbType.Structured);
                    Pdestination_table.Value = tbl;
                    Pdestination_table.TypeName = "[sales].[customDestin]";
                    command.Parameters.Add(Pdestination_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }

        }
        public DataTable GetDestCustCreate(string customerDestinationID, string destinationID, string customerID, string cropID)
        {
            customerDestinationID = customerDestinationID ?? "";
            destinationID = destinationID ?? "";
            customerID = customerID ?? "";
            cropID = cropID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerDestinationCreate] @PcustomerDestinationID, @PdestinationID, @PcustomerID, @PcropID";

                    DbParameter PcustomerDestinationID = command.CreateParameter();
                    PcustomerDestinationID.ParameterName = "@PcustomerDestinationID";
                    PcustomerDestinationID.Value = customerDestinationID;
                    command.Parameters.Add(PcustomerDestinationID);


                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetDestCustDelete( string destinationID, string customerID)
        {
            destinationID = destinationID ?? "";
            customerID = customerID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerDestinationDisable] @PdestinationID, @PcustomerID";



                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
