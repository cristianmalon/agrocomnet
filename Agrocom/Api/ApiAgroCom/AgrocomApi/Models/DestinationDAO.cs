﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using System.Net;

namespace AgrocomApi.Models
{
    public class DestinationDAO
    {
        private readonly ApplicationDbContext context;

        public DestinationDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetDestination(string opt, string id, string mar, string via)
        {

            opt = opt ?? string.Empty;
            id = id ?? string.Empty;
            mar = mar ?? string.Empty;
            via = via ?? string.Empty;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_destinationList] @PsearchOpt, @PsearchValue,@PmarketId, @PviaId";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PmarketId = command.CreateParameter();
                    PmarketId.ParameterName = "@PmarketId";
                    PmarketId.Value = mar;
                    command.Parameters.Add(PmarketId);

                    DbParameter PviaId = command.CreateParameter();
                    PviaId.ParameterName = "@PviaId";
                    PviaId.Value = via;
                    command.Parameters.Add(PviaId);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
