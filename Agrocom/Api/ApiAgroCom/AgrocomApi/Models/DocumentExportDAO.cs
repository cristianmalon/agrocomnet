﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class DocumentExportDAO
    {
        private readonly ApplicationDbContext context;

        public DocumentExportDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetDocumentExport(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_documentExportList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable DocumentsExport(int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_documentStatusPOList] @PcampaignID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public ENDocumentShippingProgram GetDetailPO(string opt, int id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingProgramList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENDocumentShippingProgram o = new ENDocumentShippingProgram();
                    o.details = new List<ENDocumentShippingProgramDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        o.orderProductionID = int.Parse(row["orderProductionID"].ToString());
                        o.orderProduction = row["orderProduction"].ToString();
                        o.packingList = row["packingList"].ToString();
                        o.originPort = row["originPort"].ToString();
                        o.container = row["container"].ToString();
                        o.customer = row["customer"].ToString();
                        o.grower = row["grower"].ToString();
                        o.destinationPort = row["destinationPort"].ToString();
                        o.vessel = row["vessel"].ToString();
                        o.incoterm = row["incoterm"].ToString();
                        o.origin = row["origin"].ToString();
                        o.shippingLine = row["shippingLine"].ToString();
                        o.etd = row["etd"].ToString();
                        o.eta = row["eta"].ToString();
                        o.saleTerm = row["saleTerm"].ToString();
                        o.via = row["via"].ToString();
                        o.bl = row["bl"].ToString();
                        o.responsable = row["responsable"].ToString();
                        o.open = int.Parse(row["open"].ToString());
                        o.statusRevision = int.Parse(row["statusRevision"].ToString());
                        o.commentaryDocumentsOBM = row["commentaryDocumentsOBM"].ToString();
                        o.commentaryDocumentsCustomer = row["commentaryDocumentsCustomer"].ToString();
                        o.userModify = row["userModify"].ToString();
                        o.dateModify = row["dateModify"].ToString();

                        var det = new ENDocumentShippingProgramDetail();
                        det.brand = row["brand"].ToString();
                        det.variety = row["variety"].ToString();
                        det.codepack = row["codepack"].ToString();
                        det.sizeInfo = row["sizeInfo"].ToString();
                        det.boxes = int.Parse(row["boxes"].ToString());
                        det.pallets = decimal.Parse(row["pallets"].ToString());
                        det.netWeight = decimal.Parse(row["netWeight"].ToString());
                        det.grossWeight = decimal.Parse(row["grossWeight"].ToString());

                        o.details.Add(det);
                    }
                    return o;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public List<ENDocumentShippingProgramByPO> GeDocumentsByPOID(string opt, int id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingProgramList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    List<ENDocumentShippingProgramByPO> lista = new List<ENDocumentShippingProgramByPO>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        ENDocumentShippingProgramByPO o = new ENDocumentShippingProgramByPO();
                        o.typeDocumentID = int.Parse(row["typeDocumentID"].ToString());
                        o.typeDocument = row["typeDocument"].ToString();
                        o.number = row["number"].ToString();
                        o.file = row["file"].ToString();
                        o.file2 = row["file2"].ToString();
                        o.file3 = row["file3"].ToString();
                        o.notesOBM = row["notesOBM"].ToString();
                        o.userOBM = row["userOBM"].ToString();
                        o.dateModifyOBM = row["dateModifyOBM"].ToString();
                        o.enabled = int.Parse(row["enabled"].ToString());
                        o.statusID = int.Parse(row["statusID"].ToString());
                        o.status = row["status"].ToString();
                        o.fileCustomer = row["fileCustomer"].ToString();
                        o.notesCustomer = row["notesCustomer"].ToString();
                        o.userCustomer = row["userCustomer"].ToString();
                        o.dateCustomer = row["dateCustomer"].ToString();
                        lista.Add(o);
                    }
                    return lista;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int SaveShippingProgramPO(ENDocumentOrderProduction o)
        {
            DbDataReader result = null;
            o.ReplaceNull();
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingProgramPOSave] " +
                        "@PorderProductionID,@PopenShippmentDocuments,@PcommentsShippmentDocumentsOBM,@PuserID";

                    DbParameter PorderproductionID = command.CreateParameter();
                    PorderproductionID.ParameterName = "@PorderProductionID";
                    PorderproductionID.Value = o.orderproductionID;
                    command.Parameters.Add(PorderproductionID);

                    DbParameter Popen = command.CreateParameter();
                    Popen.ParameterName = "@PopenShippmentDocuments";
                    Popen.Value = o.open;
                    command.Parameters.Add(Popen);

                    DbParameter PcommentsOBM = command.CreateParameter();
                    PcommentsOBM.ParameterName = "@PcommentsShippmentDocumentsOBM";
                    PcommentsOBM.Value = o.commentsOBM;
                    command.Parameters.Add(PcommentsOBM);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int SaveShippingDocumentPO(ENDocumentShippingDocumentPO o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingDocumentPOSave] " +
                        "@PorderProductionDocumentID,@PorderProductionID,@PtypeDocumentId,@Penable," +
                        "@PnameFile,@PnameFile2,@PnameFile3,@PnotesOBM,@PstatusId,@PuserID";

                    DbParameter PorderProductionDocumentID = command.CreateParameter();
                    PorderProductionDocumentID.ParameterName = "@PorderProductionDocumentID";
                    PorderProductionDocumentID.Value = o.orderProductionDocumentID;
                    command.Parameters.Add(PorderProductionDocumentID);

                    DbParameter PorderproductionID = command.CreateParameter();
                    PorderproductionID.ParameterName = "@PorderProductionID";
                    PorderproductionID.Value = o.orderproductionID;
                    command.Parameters.Add(PorderproductionID);

                    DbParameter PtypeDocumentId = command.CreateParameter();
                    PtypeDocumentId.ParameterName = "@PtypeDocumentId";
                    PtypeDocumentId.Value = o.typeDocumentId;
                    command.Parameters.Add(PtypeDocumentId);

                    DbParameter Penabled = command.CreateParameter();
                    Penabled.ParameterName = "@Penable";
                    Penabled.Value = o.enabled;
                    command.Parameters.Add(Penabled);

                    DbParameter Pfile = command.CreateParameter();
                    Pfile.ParameterName = "@PnameFile";
                    Pfile.Value = o.file;
                    command.Parameters.Add(Pfile);

                    DbParameter Pfile2 = command.CreateParameter();
                    Pfile2.ParameterName = "@PnameFile2";
                    Pfile2.Value = o.file2;
                    command.Parameters.Add(Pfile2);

                    DbParameter Pfile3 = command.CreateParameter();
                    Pfile3.ParameterName = "@PnameFile3";
                    Pfile3.Value = o.file3;
                    command.Parameters.Add(Pfile3);

                    DbParameter PnotesOBM = command.CreateParameter();
                    PnotesOBM.ParameterName = "@PnotesOBM";
                    PnotesOBM.Value = o.notesOBM;
                    command.Parameters.Add(PnotesOBM);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusId";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetAllPOForCustomer(int campaignID, int userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_documentStatusPOforCustomerList] @PcampaignID, @PuserID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int SaveShippingProgramPOforCustomer(ENDocumentOrderProduction o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingProgramPOforCustomerSave] " +
                        "@PorderProductionID,@PcommentsShippmentDocumentsCustomer,@PstatusRevision,@PuserID";

                    DbParameter PorderproductionID = command.CreateParameter();
                    PorderproductionID.ParameterName = "@PorderProductionID";
                    PorderproductionID.Value = o.orderproductionID;
                    command.Parameters.Add(PorderproductionID);

                    DbParameter PcommentsCustomer = command.CreateParameter();
                    PcommentsCustomer.ParameterName = "@PcommentsShippmentDocumentsCustomer";
                    PcommentsCustomer.Value = o.commentsCustomer;
                    command.Parameters.Add(PcommentsCustomer);

                    DbParameter Pstatus = command.CreateParameter();
                    Pstatus.ParameterName = "@PstatusRevision";
                    Pstatus.Value = o.statusRevision;
                    command.Parameters.Add(Pstatus);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int SaveShippingDocumentPOForCustomer(ENDocumentShippingDocumentPO o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingDocumentPOforCustomerSave] " +
                        "@PorderProductionDocumentID,@PorderProductionID,@PtypeDocumentId,@PfileCustomer," +
                        "@PstatusId,@PnotesCustomer,@PuserID";

                    DbParameter PorderProductionDocumentID = command.CreateParameter();
                    PorderProductionDocumentID.ParameterName = "@PorderProductionDocumentID";
                    PorderProductionDocumentID.Value = o.orderProductionDocumentID;
                    command.Parameters.Add(PorderProductionDocumentID);

                    DbParameter PorderproductionID = command.CreateParameter();
                    PorderproductionID.ParameterName = "@PorderProductionID";
                    PorderproductionID.Value = o.orderproductionID;
                    command.Parameters.Add(PorderproductionID);

                    DbParameter PtypeDocumentId = command.CreateParameter();
                    PtypeDocumentId.ParameterName = "@PtypeDocumentId";
                    PtypeDocumentId.Value = o.typeDocumentId;
                    command.Parameters.Add(PtypeDocumentId);

                    DbParameter PfileCustomer = command.CreateParameter();
                    PfileCustomer.ParameterName = "@PfileCustomer";
                    PfileCustomer.Value = o.fileCustomer;
                    command.Parameters.Add(PfileCustomer);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusId";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PnotesCustomer = command.CreateParameter();
                    PnotesCustomer.ParameterName = "@PnotesCustomer";
                    PnotesCustomer.Value = o.notesCustomer;
                    command.Parameters.Add(PnotesCustomer);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
