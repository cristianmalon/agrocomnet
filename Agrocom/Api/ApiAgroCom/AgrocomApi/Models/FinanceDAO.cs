﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace AgrocomApi.Models
{
    public class FinanceDAO
    {
        private readonly ApplicationDbContext context;

        public FinanceDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetFinance(string opt, int? campaignID, string nroPackinglist, string container, string dayini, string dayfin, string periodo, String statusid)
        {
            campaignID = campaignID ?? 0;
            nroPackinglist = nroPackinglist ?? "";
            container = container ?? "";
            dayini = dayini ?? "";
            dayfin = dayfin ?? "";
            periodo = periodo ?? "";
            statusid = statusid ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeList] @PsearchOpt, @PcampaignID, @PnroPackinglist, @Pcontainer, @Pdayini, @Pdayfin,@Pperiodo , @Pstatusid";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PnroPackinglist = command.CreateParameter();
                    PnroPackinglist.ParameterName = "@PnroPackinglist";
                    PnroPackinglist.Value = nroPackinglist;
                    command.Parameters.Add(PnroPackinglist);

                    DbParameter Pcontainer = command.CreateParameter();
                    Pcontainer.ParameterName = "@Pcontainer";
                    Pcontainer.Value = container;
                    command.Parameters.Add(Pcontainer);

                    DbParameter Pdayini = command.CreateParameter();
                    Pdayini.ParameterName = "@Pdayini";
                    Pdayini.Value = dayini;//DateTime.Parse(dayini) ;
                    command.Parameters.Add(Pdayini);

                    DbParameter Pdayfin = command.CreateParameter();
                    Pdayfin.ParameterName = "@Pdayfin";
                    Pdayfin.Value = dayfin;//DateTime.Parse(dayfin);
                    command.Parameters.Add(Pdayfin);

                    DbParameter Pperiodo = command.CreateParameter();
                    Pperiodo.ParameterName = "@Pperiodo";
                    Pperiodo.Value = periodo;
                    command.Parameters.Add(Pperiodo);

                    DbParameter Pstatusid = command.CreateParameter();
                    Pstatusid.ParameterName = "@Pstatusid";
                    Pstatusid.Value = statusid;
                    command.Parameters.Add(Pstatusid);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPackingList(string orderProductionID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeChangeTypeSettlement] @PorderProductionID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetCostDist(int orderProductionID, int? campaignID, int? userID)
        {
            campaignID = campaignID ?? 0;
            userID = userID ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeCustomerCostDistribution] @PorderProductionID, @PcampaignID, @PuserID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }

        }

        public ENFinanceCustomerSettlement GetFinanceCustomerSettlement(int orderProductionID, int? campaignID, int? userID)
        {
            campaignID = campaignID ?? 0;
            userID = userID ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeCustomerSettlementPOList] @PorderProductionID, @PcampaignID, @PuserID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENFinanceCustomerSettlement o = new ENFinanceCustomerSettlement();
                    o.details = new List<ENFinanceCustomerSettlementDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        o.orderProductionID = int.Parse(row["orderProductionID"].ToString());
                        o.customerSettlementID = int.Parse(row["customerSettlementID"].ToString());
                        o.orderProduction = row["orderProduction"].ToString();
                        o.container = row["container"].ToString();
                        o.customer = row["customer"].ToString();
                        o.incotermID = int.Parse(row["incotermID"].ToString());
                        o.incoterm = row["incoterm"].ToString();
                        o.localCurrencyID = int.Parse(row["localCurrencyID"].ToString());
                        o.localCurrency = row["localCurrency"].ToString();
                        o.localCurrencySymbol = row["localCurrencySymbol"].ToString();
                        o.exchangeRate = decimal.Parse(row["exchangeRate"].ToString());
                        o.flagExchange = int.Parse(row["flagExchange"].ToString());
                        o.date = row["date"].ToString();
                        o.comments = row["comments"].ToString();

                        o.comissionPercent = decimal.Parse(row["comissionPercent"].ToString());
                        o.comission = decimal.Parse(row["comission"].ToString());
                        o.oceanFreight = decimal.Parse(row["oceanFreight"].ToString());
                        o.coldTratment = decimal.Parse(row["coldTratment"].ToString());
                        o.importDuty = decimal.Parse(row["importDuty"].ToString());
                        o.forwardingCharge = decimal.Parse(row["forwardingCharge"].ToString());
                        o.marketCharge = decimal.Parse(row["marketCharge"].ToString());
                        o.transportCost = decimal.Parse(row["transportCost"].ToString());
                        o.detentionCharge = decimal.Parse(row["detentionCharge"].ToString());
                        o.coldStorage = decimal.Parse(row["coldStorage"].ToString());
                        o.otherCharge = decimal.Parse(row["otherCharge"].ToString());
                        o.repacking = decimal.Parse(row["repacking"].ToString());

                        o.userCreated = row["userCreated"].ToString();
                        o.userModify = row["userModify"].ToString();
                        o.dateCreated = row["dateCreated"].ToString();
                        o.dateModified = row["dateModified"].ToString();

                        var det = new ENFinanceCustomerSettlementDetail();
                        det.customerSettlementDetailID = int.Parse(row["customerSettlementDetailID"].ToString());
                        det.customerSettlementID = int.Parse(row["customerSettlementID"].ToString());
                        det.quantityBoxes = int.Parse(row["quantityBoxes"].ToString());
                        det.description = row["description"].ToString();
                        det.brand = row["brand"].ToString();
                        det.variety = row["variety"].ToString();
                        det.sizeInfo = row["sizeInfo"].ToString();
                        det.pricePerBox = decimal.Parse(row["pricePerBox"].ToString());
                        det.totalSettlement = decimal.Parse(row["totalSettlement"].ToString());
                        det.expenseFixed = decimal.Parse(row["expenseFixed"].ToString());
                        det.expenseVariable = decimal.Parse(row["expenseVariable"].ToString());
                        det.netSettlement = decimal.Parse(row["netSettlement"].ToString());
                        det.totalSettlementUSD = decimal.Parse(row["totalSettlementUSD"].ToString());
                        det.totalInvoiceUSD = decimal.Parse(row["totalInvoiceUSD"].ToString());
                        det.totalDiferenceUSD = decimal.Parse(row["totalDiferenceUSD"].ToString());
                        det.totalExpensesUSD = decimal.Parse(row["totalExpensesUSD"].ToString());
                        det.totalToColletUSD = decimal.Parse(row["totalToColletUSD"].ToString());
                        det.balancePayemntesUSD = decimal.Parse(row["balancePayemntesUSD"].ToString());
                        det.advancePaymentsUSD = decimal.Parse(row["advancePaymentsUSD"].ToString());
                        det.finalPriceUSD = decimal.Parse(row["finalPriceUSD"].ToString());
                        det.kgCaja = decimal.Parse(row["kgCaja"].ToString());
                        det.finalPriceCaja = decimal.Parse(row["finalPriceCaja"].ToString());
                        o.details.Add(det);

                    }
                    return o;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetFinancePO(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financePOList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public ENFinanceGrowerSettlement GetFinanceGrowerSettlement(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeGrowerSettlementList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENFinanceGrowerSettlement o = new ENFinanceGrowerSettlement();
                    o.details = new List<ENFinanceGrowerSettlementDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        o.growerSettlementID = int.Parse(row["growerSettlementID"].ToString());
                        o.number = int.Parse(row["number"].ToString());
                        o.reference = row["reference"].ToString();
                        o.date = row["date"].ToString();
                        o.qc = decimal.Parse(row["qc"].ToString());
                        o.insurance = decimal.Parse(row["insurance"].ToString());
                        o.others = decimal.Parse(row["others"].ToString());
                        o.comissionPercent = decimal.Parse(row["comissionPercent"].ToString());
                        o.comission = decimal.Parse(row["comission"].ToString());
                        o.promotionFee = decimal.Parse(row["promotion"].ToString());
                        o.promotionFeePercent = decimal.Parse(row["promotionFeePercent"].ToString());

                        o.userCreated = row["userCreated"].ToString();
                        o.userModify = row["userModify"].ToString();
                        o.dateCreated = row["dateCreated"].ToString();
                        o.dateModified = row["dateModified"].ToString();

                        var det = new ENFinanceGrowerSettlementDetail();
                        det.growerSettlementDetailID = int.Parse(row["growerSettlementDetailID"].ToString());
                        det.growerSettlementID = int.Parse(row["growerSettlementID"].ToString());
                        det.customerSettlementDetailID = int.Parse(row["customerSettlementDetailID"].ToString());
                        det.quantityBoxes = int.Parse(row["quantityBoxes"].ToString());
                        det.description = row["description"].ToString();
                        det.brand = row["brand"].ToString();
                        det.variety = row["variety"].ToString();
                        det.sizeInfo = row["sizeInfo"].ToString();
                        det.pricePerBox = decimal.Parse(row["pricePerBox"].ToString());
                        det.total = decimal.Parse(row["total"].ToString());


                        det.totalSettlementUSD = decimal.Parse(row["totalSettlementUSD"].ToString());
                        det.totalSettlement = decimal.Parse(row["totalSettlement"].ToString());
                        det.pricePerBoxSettlementUSD = decimal.Parse(row["pricePerBoxSettlementUSD"].ToString());
                        det.pricePerBoxSettlement = decimal.Parse(row["pricePerBoxSettlement"].ToString());
                        det.pricePerBoxSettlementNetUSD = decimal.Parse(row["pricePerBoxSettlementNetUSD"].ToString());
                        det.weight = row["weight"].ToString();

                        o.details.Add(det);

                    }
                    return o;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public ENFinanceCustomerInvoice GetFinanceInvoice(string opt, string id, int invoiceID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeInvoiceList] @PsearchOpt, @PsearchValue, @PinvoiceID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PinvoiceID = command.CreateParameter();
                    PinvoiceID.ParameterName = "@PinvoiceID";
                    PinvoiceID.Value = invoiceID;
                    command.Parameters.Add(PinvoiceID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENFinanceCustomerInvoice o = new ENFinanceCustomerInvoice();
                    o.details = new List<ENFinanceCustomerInvoiceDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        o.customerInvoiceID = int.Parse(row["customerInvoiceID"].ToString());
                        o.documentTypeID = int.Parse(row["documentTypeID"].ToString());
                        o.documentType = row["documentType"].ToString();
                        o.date = row["date"].ToString();
                        o.localCurrencyID = int.Parse(row["localCurrencyID"].ToString());
                        o.localCurrency = row["localCurrency"].ToString();
                        o.localCurrencySymbol = row["localCurrencySymbol"].ToString();
                        o.number = row["number"].ToString();
                        o.exchangeRate = decimal.Parse(row["exchangeRate"].ToString());
                        o.incoterm = row["incoterm"].ToString();
                        o.totalAmount = decimal.Parse(row["totalAmount"].ToString());
                        o.totalAmountUSD = decimal.Parse(row["totalAmountUSD"].ToString());
                        o.comments = row["comments"].ToString();
                        o.freightBL = decimal.Parse(row["freightBL"].ToString());
                        o.insuranceBL = decimal.Parse(row["insuranceBL"].ToString());
                        o.userCreated = row["userCreated"].ToString();
                        o.userModify = row["userModify"].ToString();
                        o.dateCreated = row["dateCreated"].ToString();
                        o.dateModified = row["dateModified"].ToString();

                        var det = new ENFinanceCustomerInvoiceDetail();
                        det.customerInvoiceDetailID = int.Parse(row["customerInvoiceDetailID"].ToString());
                        det.customerInvoiceID = int.Parse(row["customerInvoiceID"].ToString());
                        det.customerSettlementDetailID = int.Parse(row["customerSettlementDetailID"].ToString());
                        det.quantityBoxes = int.Parse(row["quantityBoxes"].ToString());
                        det.description = row["description"].ToString();
                        det.brand = row["brand"].ToString();
                        det.variety = row["variety"].ToString();
                        det.sizeInfo = row["sizeInfo"].ToString();
                        det.pricePerBox = decimal.Parse(row["pricePerBox"].ToString());
                        det.total = decimal.Parse(row["total"].ToString());
                        det.weight = decimal.Parse(row["weight"].ToString());
                        det.pricePerBoxFOB = decimal.Parse(row["pricePerBoxFOB"].ToString());
                        det.totalFOB = decimal.Parse(row["subTotalFOB"].ToString());
                        o.details.Add(det);

                    }
                    return o;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public ENFinanceCustomerPayments GetFinancePayment(string opt, string id, int customerPaymentID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financePaymentList] @PsearchOpt, @PsearchValue, @PcustomerPaymentID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PcustomerPaymentID = command.CreateParameter();
                    PcustomerPaymentID.ParameterName = "@PcustomerPaymentID";
                    PcustomerPaymentID.Value = customerPaymentID;
                    command.Parameters.Add(PcustomerPaymentID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENFinanceCustomerPayments o = new ENFinanceCustomerPayments();
                    o.details = new List<ENFinanceCustomerPaymentsDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        o.customerSettlementID = int.Parse(row["customerSettlementID"].ToString());
                        o.customerPaymentID = int.Parse(row["customerPaymentID"].ToString());
                        o.paymentTypeID = int.Parse(row["paymentTypeID"].ToString());
                        o.paymentType = row["paymentType"].ToString();
                        o.numberOperation = row["numberOperation"].ToString();
                        o.localCurrencyID = int.Parse(row["localCurrencyID"].ToString());
                        o.localCurrency = row["localCurrency"].ToString();
                        o.localCurrencySymbol = row["localCurrencySymbol"].ToString();
                        o.exchangeRate = decimal.Parse(row["exchangeRate"].ToString());
                        o.amount = decimal.Parse(row["amount"].ToString());
                        o.amountUSD = decimal.Parse(row["amountUSD"].ToString());
                        o.comments = row["comments"].ToString();

                        o.userCreated = row["userCreated"].ToString();
                        o.userModify = row["userModify"].ToString();
                        o.dateCreated = row["dateCreated"].ToString();
                        o.dateModified = row["dateModified"].ToString();

                        var det = new ENFinanceCustomerPaymentsDetail();
                        det.customerPaymentsDetailID = int.Parse(row["customerPaymentsDetailID"].ToString());
                        det.customerPaymentID = int.Parse(row["customerPaymentID"].ToString());
                        det.customerSettlementDetailID = int.Parse(row["customerSettlementDetailID"].ToString());
                        det.quantityBoxes = int.Parse(row["quantityBoxes"].ToString());
                        det.description = row["description"].ToString();
                        det.variety = row["variety"].ToString();
                        det.brand = row["brand"].ToString();
                        det.sizeInfo = row["sizeInfo"].ToString();
                        det.pricePerBox = decimal.Parse(row["pricePerBox"].ToString());
                        det.total = decimal.Parse(row["total"].ToString());
                        det.totalUSD = decimal.Parse(row["totalUSD"].ToString());

                        o.details.Add(det);

                    }
                    return o;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceInvoice(ENFinanceCustomerInvoice o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeInvoiceCreateUpdate] " +
                        "@PcustomerInvoiceID,@PcustomerSettlementID,@PdocumentTypeID,@Pdate," +
                        "@PlocalCurrencyID,@Pnumber,@PtotalAmount,@PexchangeRate," +
                        "@PtotalAmountUSD,@Pcomments,@PinsuranceBL,@PfreightBL," +
                        "@PstatusID,@PuserID";

                    DbParameter PcustomerInvoiceID = command.CreateParameter();
                    PcustomerInvoiceID.ParameterName = "@PcustomerInvoiceID";
                    PcustomerInvoiceID.Value = o.customerInvoiceID;
                    command.Parameters.Add(PcustomerInvoiceID);

                    DbParameter PcustomerSetlementID = command.CreateParameter();
                    PcustomerSetlementID.ParameterName = "@PcustomerSettlementID";
                    PcustomerSetlementID.Value = o.customerSettlementID;
                    command.Parameters.Add(PcustomerSetlementID);

                    DbParameter PdocumentTypeID = command.CreateParameter();
                    PdocumentTypeID.ParameterName = "@PdocumentTypeID";
                    PdocumentTypeID.Value = o.documentTypeID;
                    command.Parameters.Add(PdocumentTypeID);

                    DbParameter Pdate = command.CreateParameter();
                    Pdate.ParameterName = "@Pdate";
                    Pdate.Value = o.date;
                    command.Parameters.Add(Pdate);

                    DbParameter PlocalCurrencyID = command.CreateParameter();
                    PlocalCurrencyID.ParameterName = "@PlocalCurrencyID";
                    PlocalCurrencyID.Value = o.localCurrencyID;
                    command.Parameters.Add(PlocalCurrencyID);

                    DbParameter Pnumber = command.CreateParameter();
                    Pnumber.ParameterName = "@Pnumber";
                    Pnumber.Value = o.number;
                    command.Parameters.Add(Pnumber);

                    DbParameter PtotalAmount = command.CreateParameter();
                    PtotalAmount.ParameterName = "@PtotalAmount";
                    PtotalAmount.Value = o.totalAmount;
                    command.Parameters.Add(PtotalAmount);

                    DbParameter PexchangeRate = command.CreateParameter();
                    PexchangeRate.ParameterName = "@PexchangeRate";
                    PexchangeRate.Value = o.exchangeRate;
                    command.Parameters.Add(PexchangeRate);

                    DbParameter PtotalAmountUSD = command.CreateParameter();
                    PtotalAmountUSD.ParameterName = "@PtotalAmountUSD";
                    PtotalAmountUSD.Value = o.totalAmountUSD;
                    command.Parameters.Add(PtotalAmountUSD);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter PinsuranceBL = command.CreateParameter();
                    PinsuranceBL.ParameterName = "@PinsuranceBL";
                    PinsuranceBL.Value = o.insuranceBL;
                    command.Parameters.Add(PinsuranceBL);

                    DbParameter PfreightBL = command.CreateParameter();
                    PfreightBL.ParameterName = "@PfreightBL";
                    PfreightBL.Value = o.freightBL;
                    command.Parameters.Add(PfreightBL);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceInvoiceDetail(ENFinanceCustomerInvoiceDetail o, int customerInvoiceID)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeInvoiceDetailCreateUpdate] " +
                        "@PcustomerInvoiceDetailID,@PcustomerInvoiceID,@PcustomerSettlementDetailID,@PquantityBoxes," +
                        "@PpricePerBox,@Ptotal,@PstatusID,@PuserID,@pricePerBoxFOB,@SubTotalFOB ";


                    DbParameter PcustomerInvoiceDetailID = command.CreateParameter();
                    PcustomerInvoiceDetailID.ParameterName = "@PcustomerInvoiceDetailID";
                    PcustomerInvoiceDetailID.Value = o.customerInvoiceDetailID;
                    command.Parameters.Add(PcustomerInvoiceDetailID);

                    DbParameter PcustomerInvoiceID = command.CreateParameter();
                    PcustomerInvoiceID.ParameterName = "@PcustomerInvoiceID";
                    PcustomerInvoiceID.Value = customerInvoiceID;
                    command.Parameters.Add(PcustomerInvoiceID);

                    DbParameter PcustomerSettlementDetailID = command.CreateParameter();
                    PcustomerSettlementDetailID.ParameterName = "@PcustomerSettlementDetailID";
                    PcustomerSettlementDetailID.Value = o.customerSettlementDetailID;
                    command.Parameters.Add(PcustomerSettlementDetailID);

                    DbParameter PquantityBoxes = command.CreateParameter();
                    PquantityBoxes.ParameterName = "@PquantityBoxes";
                    PquantityBoxes.Value = o.quantityBoxes;
                    command.Parameters.Add(PquantityBoxes);

                    DbParameter PpricePerBox = command.CreateParameter();
                    PpricePerBox.ParameterName = "@PpricePerBox";
                    PpricePerBox.Value = o.pricePerBox;
                    command.Parameters.Add(PpricePerBox);

                    DbParameter Ptotal = command.CreateParameter();
                    Ptotal.ParameterName = "@Ptotal";
                    Ptotal.Value = o.total;
                    command.Parameters.Add(Ptotal);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PpricePerBoxFOB = command.CreateParameter();
                    PpricePerBoxFOB.ParameterName = "@pricePerBoxFOB";
                    PpricePerBoxFOB.Value = o.pricePerBoxFOB;
                    command.Parameters.Add(PpricePerBoxFOB);

                    DbParameter PtotalFOB = command.CreateParameter();
                    PtotalFOB.ParameterName = "@SubTotalFOB";
                    PtotalFOB.Value = o.totalFOB;
                    command.Parameters.Add(PtotalFOB);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinancePayment(ENFinanceCustomerPayments o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financePaymentCreateUpdate] " +
                        "@PcustomerPaymentID,@PcustomerSetlementID,@PpaymentTypeID,@PnumberOperation," +
                        "@Pdate,@PlocalCurrencyID,@PexchangeRate,@Pamount," +
                        "@PamountUSD,@Pcomments,@PstatusID,@PuserID";

                    DbParameter PcustomerPaymentID = command.CreateParameter();
                    PcustomerPaymentID.ParameterName = "@PcustomerPaymentID";
                    PcustomerPaymentID.Value = o.customerPaymentID;
                    command.Parameters.Add(PcustomerPaymentID);

                    DbParameter PcustomerSetlementID = command.CreateParameter();
                    PcustomerSetlementID.ParameterName = "@PcustomerSetlementID";
                    PcustomerSetlementID.Value = o.customerSettlementID;
                    command.Parameters.Add(PcustomerSetlementID);

                    DbParameter PpaymentTypeID = command.CreateParameter();
                    PpaymentTypeID.ParameterName = "@PpaymentTypeID";
                    PpaymentTypeID.Value = o.paymentTypeID;
                    command.Parameters.Add(PpaymentTypeID);

                    DbParameter PnumberOperation = command.CreateParameter();
                    PnumberOperation.ParameterName = "@PnumberOperation";
                    PnumberOperation.Value = o.numberOperation;
                    command.Parameters.Add(PnumberOperation);

                    DbParameter Pdate = command.CreateParameter();
                    Pdate.ParameterName = "@Pdate";
                    Pdate.Value = o.date;
                    command.Parameters.Add(Pdate);

                    DbParameter PlocalCurrencyID = command.CreateParameter();
                    PlocalCurrencyID.ParameterName = "@PlocalCurrencyID";
                    PlocalCurrencyID.Value = o.localCurrencyID;
                    command.Parameters.Add(PlocalCurrencyID);

                    DbParameter PexchangeRate = command.CreateParameter();
                    PexchangeRate.ParameterName = "@PexchangeRate";
                    PexchangeRate.Value = o.exchangeRate;
                    command.Parameters.Add(PexchangeRate);

                    DbParameter Pamount = command.CreateParameter();
                    Pamount.ParameterName = "@Pamount";
                    Pamount.Value = o.amount;
                    command.Parameters.Add(Pamount);

                    DbParameter PamountUSD = command.CreateParameter();
                    PamountUSD.ParameterName = "@PamountUSD";
                    PamountUSD.Value = o.amountUSD;
                    command.Parameters.Add(PamountUSD);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinancePaymentDetails(ENFinanceCustomerPaymentsDetail o, int customerPaymentID)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financePaymentDetailCreateUpdate] " +
                        "@PcustomerPaymentsDetailID,@PcustomerPaymentID,@PcustomerSettlementDetailID," +
                        "@PpricePerBox,@Ptotal,@PstatusID,@PuserID";

                    DbParameter PcustomerPaymentsDetailID = command.CreateParameter();
                    PcustomerPaymentsDetailID.ParameterName = "@PcustomerPaymentsDetailID";
                    PcustomerPaymentsDetailID.Value = o.customerPaymentsDetailID;
                    command.Parameters.Add(PcustomerPaymentsDetailID);

                    DbParameter PcustomerPaymentID = command.CreateParameter();
                    PcustomerPaymentID.ParameterName = "@PcustomerPaymentID";
                    PcustomerPaymentID.Value = customerPaymentID;
                    command.Parameters.Add(PcustomerPaymentID);

                    DbParameter PcustomerSettlementDetailID = command.CreateParameter();
                    PcustomerSettlementDetailID.ParameterName = "@PcustomerSettlementDetailID";
                    PcustomerSettlementDetailID.Value = o.customerSettlementDetailID;
                    command.Parameters.Add(PcustomerSettlementDetailID);

                    DbParameter PpricePerBox = command.CreateParameter();
                    PpricePerBox.ParameterName = "@PpricePerBox";
                    PpricePerBox.Value = o.pricePerBox;
                    command.Parameters.Add(PpricePerBox);

                    DbParameter Ptotal = command.CreateParameter();
                    Ptotal.ParameterName = "@Ptotal";
                    Ptotal.Value = o.total;
                    command.Parameters.Add(Ptotal);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceGrower(ENFinanceGrowerSettlement o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeGrowerCreateUpdate]" +
                        "@PgrowerSetlementID,@PcustomerSetlementID,@Pnumber,@Preference," +
                        "@Pdate,@Pqc,@Pinsurance,@Pothers," +
                        "@PcomissionPercent,@Pcomission,@PpromotionFeePercent,@Ppromotion," +
                        "@PstatusConfirm,@PstatusID,@PuserID";

                    DbParameter PgrowerSetlementID = command.CreateParameter();
                    PgrowerSetlementID.ParameterName = "@PgrowerSetlementID";
                    PgrowerSetlementID.Value = o.growerSettlementID;
                    command.Parameters.Add(PgrowerSetlementID);

                    DbParameter PcustomerSetlementID = command.CreateParameter();
                    PcustomerSetlementID.ParameterName = "@PcustomerSetlementID";
                    PcustomerSetlementID.Value = o.customerSettlementID;
                    command.Parameters.Add(PcustomerSetlementID);

                    DbParameter Pnumber = command.CreateParameter();
                    Pnumber.ParameterName = "@Pnumber";
                    Pnumber.Value = o.number;
                    command.Parameters.Add(Pnumber);

                    DbParameter Preference = command.CreateParameter();
                    Preference.ParameterName = "@Preference";
                    Preference.Value = o.reference;
                    command.Parameters.Add(Preference);
                    //
                    DbParameter Pdate = command.CreateParameter();
                    Pdate.ParameterName = "@Pdate";
                    Pdate.Value = o.date;
                    command.Parameters.Add(Pdate);

                    DbParameter Pqc = command.CreateParameter();
                    Pqc.ParameterName = "@Pqc";
                    Pqc.Value = o.qc;
                    command.Parameters.Add(Pqc);

                    DbParameter Pinsurance = command.CreateParameter();
                    Pinsurance.ParameterName = "@Pinsurance";
                    Pinsurance.Value = o.insurance;
                    command.Parameters.Add(Pinsurance);

                    DbParameter Pothers = command.CreateParameter();
                    Pothers.ParameterName = "@Pothers";
                    Pothers.Value = o.others;
                    command.Parameters.Add(Pothers);
                    //
                    DbParameter PcomissionPercent = command.CreateParameter();
                    PcomissionPercent.ParameterName = "@PcomissionPercent";
                    PcomissionPercent.Value = o.comissionPercent;
                    command.Parameters.Add(PcomissionPercent);

                    DbParameter Pcomission = command.CreateParameter();
                    Pcomission.ParameterName = "@Pcomission";
                    Pcomission.Value = o.comission;
                    command.Parameters.Add(Pcomission);

                    DbParameter PpromotionFeePercent = command.CreateParameter();
                    PpromotionFeePercent.ParameterName = "@PpromotionFeePercent";
                    PpromotionFeePercent.Value = o.promotionFeePercent;
                    command.Parameters.Add(PpromotionFeePercent);

                    DbParameter Ppromotion = command.CreateParameter();
                    Ppromotion.ParameterName = "@Ppromotion";
                    Ppromotion.Value = o.promotionFee;
                    command.Parameters.Add(Ppromotion);
                    //
                    DbParameter PstatusConfirm = command.CreateParameter();
                    PstatusConfirm.ParameterName = "@PstatusConfirm";
                    PstatusConfirm.Value = o.statusConfirm;
                    command.Parameters.Add(PstatusConfirm);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int GetFinanceGrowerDelete(int growerSetlementID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeGrowerDelete] @PgrowerSetlementID";
                    DbParameter PgrowerSetlementID = command.CreateParameter();
                    PgrowerSetlementID.ParameterName = "@PgrowerSetlementID";
                    PgrowerSetlementID.Value = growerSetlementID;
                    command.Parameters.Add(PgrowerSetlementID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int iRsl = 0;
                    while (result.Read())
                    {
                        iRsl = result.GetInt32(0);
                    }
                    return (iRsl);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceGrowerDetail(ENFinanceGrowerSettlementDetail o, int growerSettlementID)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeGrowerDetailCreateUpdate]" +
                        "@PgrowerSettlementDetailID,@PgrowerSettlementID,@PcustomerSettlementDetailID,@PpricePerBox," +
                        "@Ptotal,@PstatusConfirm,@PstatusID,@PuserID";

                    DbParameter PgrowerSettlementDetailID = command.CreateParameter();
                    PgrowerSettlementDetailID.ParameterName = "@PgrowerSettlementDetailID";
                    PgrowerSettlementDetailID.Value = o.growerSettlementDetailID;
                    command.Parameters.Add(PgrowerSettlementDetailID);

                    DbParameter PgrowerSettlementID = command.CreateParameter();
                    PgrowerSettlementID.ParameterName = "@PgrowerSettlementID";
                    PgrowerSettlementID.Value = growerSettlementID;
                    command.Parameters.Add(PgrowerSettlementID);

                    DbParameter PcustomerSettlementDetailID = command.CreateParameter();
                    PcustomerSettlementDetailID.ParameterName = "@PcustomerSettlementDetailID";
                    PcustomerSettlementDetailID.Value = o.customerSettlementDetailID;
                    command.Parameters.Add(PcustomerSettlementDetailID);

                    DbParameter PpricePerBox = command.CreateParameter();
                    PpricePerBox.ParameterName = "@PpricePerBox";
                    PpricePerBox.Value = o.pricePerBox;
                    command.Parameters.Add(PpricePerBox);

                    DbParameter Ptotal = command.CreateParameter();
                    Ptotal.ParameterName = "@Ptotal";
                    Ptotal.Value = o.total;
                    command.Parameters.Add(Ptotal);
                    
                    DbParameter PstatusConfirm = command.CreateParameter();
                    PstatusConfirm.ParameterName = "@PstatusConfirm";
                    PstatusConfirm.Value = o.statusConfirm;
                    command.Parameters.Add(PstatusConfirm);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceCustomerSettlement(ENFinanceCustomerSettlement o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeCustomerCreateUpdate] " +
                        "@PcustomerSetlementID,@PorderProductionID,@Pdate,@PlocalCurrencyID," +
                        "@PexhangeRate,@PtotalLocalCurrency,@PtotalUSD,@PincotermID," +
                        "@Pcomments,@PcomissionPercent,@Pcomission,@PoceanFreight," +
                        "@PcoldTratment,@PimportDuty,@PforwardingCharge,@PmarketCharge," +
                        "@PtransportCost,@PdetentionCharge,@PcoldStorage,@PotherChange," +
                        "@Prepacking,@PexpenseFixed,@PexpenseVariable,@PstatusID,@PuserID," +
                        "@POcean_Freight, @PCold_Tratment, @PImport_Duty, @PForwarding_Charge, " +
                        "@PMarket_Charge, @PTransport_Cost, @PDetention_Charge, @PCold_Storage, " +
                        "@POther_Charge, @PRepacking_Charges,@PflagExchange";

                    
                    DbParameter PcustomerSetlementID = command.CreateParameter();
                    PcustomerSetlementID.ParameterName = "@PcustomerSetlementID";
                    PcustomerSetlementID.Value = o.customerSettlementID;
                    command.Parameters.Add(PcustomerSetlementID);

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = o.orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter Pdate = command.CreateParameter();
                    Pdate.ParameterName = "@Pdate";
                    Pdate.Value = o.date;
                    command.Parameters.Add(Pdate);

                    DbParameter PlocalCurrencyID = command.CreateParameter();
                    PlocalCurrencyID.ParameterName = "@PlocalCurrencyID";
                    PlocalCurrencyID.Value = o.localCurrencyID;
                    command.Parameters.Add(PlocalCurrencyID);
                    
                    DbParameter PexhangeRate = command.CreateParameter();
                    PexhangeRate.ParameterName = "@PexhangeRate";
                    PexhangeRate.Value = o.exchangeRate;
                    command.Parameters.Add(PexhangeRate);

                    DbParameter PtotalLocalCurrency = command.CreateParameter();
                    PtotalLocalCurrency.ParameterName = "@PtotalLocalCurrency";
                    PtotalLocalCurrency.Value = o.totalLocalCurrency;
                    command.Parameters.Add(PtotalLocalCurrency);

                    DbParameter PtotalUSD = command.CreateParameter();
                    PtotalUSD.ParameterName = "@PtotalUSD";
                    PtotalUSD.Value = o.totalUSD;
                    command.Parameters.Add(PtotalUSD);

                    DbParameter Pincoterm = command.CreateParameter();
                    Pincoterm.ParameterName = "@PincotermID";
                    Pincoterm.Value = o.incotermID;
                    command.Parameters.Add(Pincoterm);
                    
                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter PcomissionPercent = command.CreateParameter();
                    PcomissionPercent.ParameterName = "@PcomissionPercent";
                    PcomissionPercent.Value = o.comissionPercent;
                    command.Parameters.Add(PcomissionPercent);

                    DbParameter Pcomission = command.CreateParameter();
                    Pcomission.ParameterName = "@Pcomission";
                    Pcomission.Value = o.comission;
                    command.Parameters.Add(Pcomission);

                    DbParameter PoceanFreight = command.CreateParameter();
                    PoceanFreight.ParameterName = "@PoceanFreight";
                    PoceanFreight.Value = o.oceanFreight;
                    command.Parameters.Add(PoceanFreight);
                    
                    DbParameter PcoldTratment = command.CreateParameter();
                    PcoldTratment.ParameterName = "@PcoldTratment";
                    PcoldTratment.Value = o.coldTratment;
                    command.Parameters.Add(PcoldTratment);

                    DbParameter PimportDuty = command.CreateParameter();
                    PimportDuty.ParameterName = "@PimportDuty";
                    PimportDuty.Value = o.importDuty;
                    command.Parameters.Add(PimportDuty);

                    DbParameter PforwardingCharge = command.CreateParameter();
                    PforwardingCharge.ParameterName = "@PforwardingCharge";
                    PforwardingCharge.Value = o.forwardingCharge;
                    command.Parameters.Add(PforwardingCharge);

                    DbParameter PmarketCharge = command.CreateParameter();
                    PmarketCharge.ParameterName = "@PmarketCharge";
                    PmarketCharge.Value = o.marketCharge;
                    command.Parameters.Add(PmarketCharge);

                    DbParameter PtransportCost = command.CreateParameter();
                    PtransportCost.ParameterName = "@PtransportCost";
                    PtransportCost.Value = o.transportCost;
                    command.Parameters.Add(PtransportCost);

                    DbParameter PdetentionCharge = command.CreateParameter();
                    PdetentionCharge.ParameterName = "@PdetentionCharge";
                    PdetentionCharge.Value = o.detentionCharge;
                    command.Parameters.Add(PdetentionCharge);

                    DbParameter PcoldStorage = command.CreateParameter();
                    PcoldStorage.ParameterName = "@PcoldStorage";
                    PcoldStorage.Value = o.coldStorage;
                    command.Parameters.Add(PcoldStorage);

                    DbParameter PotherChange = command.CreateParameter();
                    PotherChange.ParameterName = "@PotherChange";
                    PotherChange.Value = o.otherCharge;
                    command.Parameters.Add(PotherChange);
                    
                    DbParameter Prepacking = command.CreateParameter();
                    Prepacking.ParameterName = "@Prepacking";
                    Prepacking.Value = o.repacking;
                    command.Parameters.Add(Prepacking);

                    DbParameter PexpenseFixed = command.CreateParameter();
                    PexpenseFixed.ParameterName = "@PexpenseFixed";
                    PexpenseFixed.Value = o.expenseFixed;
                    command.Parameters.Add(PexpenseFixed);

                    DbParameter PexpenseVariable = command.CreateParameter();
                    PexpenseVariable.ParameterName = "@PexpenseVariable";
                    PexpenseVariable.Value = o.expenseVariable;
                    command.Parameters.Add(PexpenseVariable);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserID";
                    PuserCreated.Value = o.userID;
                    command.Parameters.Add(PuserCreated);

                    DbParameter POcean_Freight = command.CreateParameter();
                    POcean_Freight.ParameterName = "@POcean_Freight";
                    POcean_Freight.Value = o.Ocean_Freight;
                    command.Parameters.Add(POcean_Freight);

                    DbParameter PCold_Tratment = command.CreateParameter();
                    PCold_Tratment.ParameterName = "@PCold_Tratment";
                    PCold_Tratment.Value = o.Cold_Tratment;
                    command.Parameters.Add(PCold_Tratment);

                    DbParameter PImport_Duty = command.CreateParameter();
                    PImport_Duty.ParameterName = "@PImport_Duty";
                    PImport_Duty.Value = o.Import_Duty;
                    command.Parameters.Add(PImport_Duty);

                    DbParameter PForwarding_Charge = command.CreateParameter();
                    PForwarding_Charge.ParameterName = "@PForwarding_Charge";
                    PForwarding_Charge.Value = o.Forwarding_Charge;
                    command.Parameters.Add(PForwarding_Charge);

                    DbParameter PMarket_Charge = command.CreateParameter();
                    PMarket_Charge.ParameterName = "@PMarket_Charge";
                    PMarket_Charge.Value = o.Market_Charge;
                    command.Parameters.Add(PMarket_Charge);

                    DbParameter PTransport_Cost = command.CreateParameter();
                    PTransport_Cost.ParameterName = "@PTransport_Cost";
                    PTransport_Cost.Value = o.Transport_Cost;
                    command.Parameters.Add(PTransport_Cost);

                    DbParameter PDetention_Charge = command.CreateParameter();
                    PDetention_Charge.ParameterName = "@PDetention_Charge";
                    PDetention_Charge.Value = o.Detention_Charge;
                    command.Parameters.Add(PDetention_Charge);

                    DbParameter PCold_Storage = command.CreateParameter();
                    PCold_Storage.ParameterName = "@PCold_Storage";
                    PCold_Storage.Value = o.Cold_Storage;
                    command.Parameters.Add(PCold_Storage);

                    DbParameter POther_Charge = command.CreateParameter();
                    POther_Charge.ParameterName = "@POther_Charge";
                    POther_Charge.Value = o.Other_Charge;
                    command.Parameters.Add(POther_Charge);

                    DbParameter PRepacking_Charges = command.CreateParameter();
                    PRepacking_Charges.ParameterName = "@PRepacking_Charges";
                    PRepacking_Charges.Value = o.Repacking_Charges;
                    command.Parameters.Add(PRepacking_Charges);

                    DbParameter PflagExchange = command.CreateParameter();
                    PflagExchange.ParameterName = "@PflagExchange";
                    PflagExchange.Value = o.flagExchange;
                    command.Parameters.Add(PflagExchange);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceCostDistribution(ENCostDistributiom o, int customerSettlementID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeCostdistributionUpdate] " +
                           "@PsettlementExpensesID, @PcustomerSettlementID, @Pamount, @PtypeOfExpenses";

                    DbParameter PsettlementExpensesID = command.CreateParameter();
                    PsettlementExpensesID.ParameterName = "@PsettlementExpensesID";
                    PsettlementExpensesID.Value = o.settlementExpensesID;
                    command.Parameters.Add(PsettlementExpensesID);

                    DbParameter PcustomerSettlementID = command.CreateParameter();
                    PcustomerSettlementID.ParameterName = "@PcustomerSettlementID";
                    PcustomerSettlementID.Value = customerSettlementID;
                    command.Parameters.Add(PcustomerSettlementID);

                    DbParameter Pamount = command.CreateParameter();
                    Pamount.ParameterName = "@Pamount";
                    Pamount.Value = o.amount;
                    command.Parameters.Add(Pamount);

                    DbParameter PtypeOfExpenses = command.CreateParameter();
                    PtypeOfExpenses.ParameterName = "@PtypeOfExpenses";
                    PtypeOfExpenses.Value = o.typeOfExpenses;
                    command.Parameters.Add(PtypeOfExpenses);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public int CreateUpdateFinanceCustomerSettlementDetail(ENFinanceCustomerSettlementDetail o, int customerSettlementID)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_financeCustomerDetailCreateUpdate] " +
                        "@PcustomerSettlementDetailID,@PcustomerSettlementID," +
                        "@Pdescription,@PquantityBoxes," +
                        "@PpricePerBox,@PtotalSettlement,@PexpenseFixed," +
                        "@PexpenseVariable,@PtotalSettlementUSD,@PnetSettlement,@PstatusID,@PuserID";

                    DbParameter PcustomerSettlementDetailID = command.CreateParameter();
                    PcustomerSettlementDetailID.ParameterName = "@PcustomerSettlementDetailID";
                    PcustomerSettlementDetailID.Value = o.customerSettlementDetailID;
                    command.Parameters.Add(PcustomerSettlementDetailID);

                    DbParameter PcustomerSetlementID = command.CreateParameter();
                    PcustomerSetlementID.ParameterName = "@PcustomerSettlementID";
                    PcustomerSetlementID.Value = customerSettlementID;
                    command.Parameters.Add(PcustomerSetlementID);

                    DbParameter Pdescription = command.CreateParameter();
                    Pdescription.ParameterName = "@Pdescription";
                    Pdescription.Value = o.description;
                    command.Parameters.Add(Pdescription);

                    DbParameter PquantityBoxes = command.CreateParameter();
                    PquantityBoxes.ParameterName = "@PquantityBoxes";
                    PquantityBoxes.Value = o.quantityBoxes;
                    command.Parameters.Add(PquantityBoxes);

                    DbParameter PpricePerBox = command.CreateParameter();
                    PpricePerBox.ParameterName = "@PpricePerBox";
                    PpricePerBox.Value = o.pricePerBox;
                    command.Parameters.Add(PpricePerBox);

                    DbParameter PtotalSettlement = command.CreateParameter();
                    PtotalSettlement.ParameterName = "@PtotalSettlement";
                    PtotalSettlement.Value = o.totalSettlement;
                    command.Parameters.Add(PtotalSettlement);

                    DbParameter PexpenseFixed = command.CreateParameter();
                    PexpenseFixed.ParameterName = "@PexpenseFixed";
                    PexpenseFixed.Value = o.expenseFixed;
                    command.Parameters.Add(PexpenseFixed);

                    DbParameter PexpenseVariable = command.CreateParameter();
                    PexpenseVariable.ParameterName = "@PexpenseVariable";
                    PexpenseVariable.Value = o.expenseVariable;
                    command.Parameters.Add(PexpenseVariable);

                    DbParameter PtotalSettlementUSD = command.CreateParameter();
                    PtotalSettlementUSD.ParameterName = "@PtotalSettlementUSD";
                    PtotalSettlementUSD.Value = o.totalSettlementUSD;
                    command.Parameters.Add(PtotalSettlementUSD);

                    DbParameter PnetSettlement = command.CreateParameter();
                    PnetSettlement.ParameterName = "@PnetSettlement";
                    PnetSettlement.Value = o.netSettlement;
                    command.Parameters.Add(PnetSettlement);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;

                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

    }
}
