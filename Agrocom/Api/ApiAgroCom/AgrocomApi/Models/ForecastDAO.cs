﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace AgrocomApi.Models
{
    public class ForecastDAO
    {
        private readonly ApplicationDbContext context;
        public ForecastDAO(ApplicationDbContext context)
        {
            this.context = context;
        }
        public DataTable GetStockByWeek(string growerID, string Weeks)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projectedProductionListByGrowerWeek] @PgrowerID, @PweekID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PgrowerID";
                    PsearchOpt.Value = growerID;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PweekID";
                    PsearchValue.Value = Weeks;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetSizeComList(string login, string DateIni, string DateFin, int loteId, string growerId, string categoryId, string sizeId)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionBySizeCommercial2List] @Plogin,@PdateIni,@PdateFin,@PloteID,@PgrowerID,@PcategoryID,@PsizeID";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = login;
                    command.Parameters.Add(Plogin);

                    DbParameter PdateIni = command.CreateParameter();
                    PdateIni.ParameterName = "@PdateIni";
                    PdateIni.Value = DateIni;
                    command.Parameters.Add(PdateIni);

                    DbParameter PdateFin = command.CreateParameter();
                    PdateFin.ParameterName = "@PdateFin";
                    PdateFin.Value = DateFin;
                    command.Parameters.Add(PdateFin);

                    DbParameter Plote = command.CreateParameter();
                    Plote.ParameterName = "@PloteID";
                    Plote.Value = loteId;
                    command.Parameters.Add(Plote);

                    DbParameter Pgrower = command.CreateParameter();
                    Pgrower.ParameterName = "@PgrowerID";
                    Pgrower.Value = growerId;
                    command.Parameters.Add(Pgrower);

                    DbParameter Pcategory = command.CreateParameter();
                    Pcategory.ParameterName = "@PcategoryID";
                    Pcategory.Value = categoryId;
                    command.Parameters.Add(Pcategory);

                    DbParameter Psize = command.CreateParameter();
                    Psize.ParameterName = "@PsizeID";
                    Psize.Value = sizeId;
                    command.Parameters.Add(Psize);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetforecastList(int projectedWeekID, int loteID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projectedProductionList] @PprojectedWeekID, @PloteID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PprojectedWeekID";
                    PsearchOpt.Value = projectedWeekID;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PloteID";
                    PsearchValue.Value = loteID;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetpercentList(string login, int projectedWeekID, int loteID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionBySizepPercentList] @Plogin, @PprojectedWeekID, @PloteID";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = login;
                    command.Parameters.Add(Plogin);

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PprojectedWeekID";
                    PsearchOpt.Value = projectedWeekID;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PloteID";
                    PsearchValue.Value = loteID;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetpercentListUpdate(string login, string loteID, string projectedWeekID, string categoryID, int quantityTotal, string commentary, string percentTotalSize_JSON, string percentBySize_JSON)
        {
            commentary = commentary ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionBySizeModify] @Plogin, @PloteID, @PprojectedWeekID, @PcategoryID, @PquantityTotal,@Pcommentary, @PpercentTotalSize_JSON, @PpercentBySize_JSON";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = login;
                    command.Parameters.Add(Plogin);

                    DbParameter PloteID = command.CreateParameter();
                    PloteID.ParameterName = "@PloteID";
                    PloteID.Value = loteID;
                    command.Parameters.Add(PloteID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = categoryID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PquantityTotal = command.CreateParameter();
                    PquantityTotal.ParameterName = "@PquantityTotal";
                    PquantityTotal.Value = quantityTotal;
                    command.Parameters.Add(PquantityTotal);

                    DbParameter Pcommentary = command.CreateParameter();
                    Pcommentary.ParameterName = "@Pcommentary";
                    Pcommentary.Value = commentary;
                    command.Parameters.Add(Pcommentary);

                    DbParameter PpercentTotalSize_JSON = command.CreateParameter();
                    PpercentTotalSize_JSON.ParameterName = "@PpercentTotalSize_JSON";
                    PpercentTotalSize_JSON.Value = percentTotalSize_JSON;
                    command.Parameters.Add(PpercentTotalSize_JSON);

                    DbParameter PpercentBySize_JSON = command.CreateParameter();
                    PpercentBySize_JSON.ParameterName = "@PpercentBySize_JSON";
                    PpercentBySize_JSON.Value = percentBySize_JSON;
                    command.Parameters.Add(PpercentBySize_JSON);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataSet ListForecastUnified(string growerID, string farmID, string cropID, string originID) 
        {
            DbDataReader result = null;
            try
            {
 
                //using (var command = context.Database.GetDbConnection().CreateCommand())
                //{
                    //command.CommandText = "exec [package].[sp_massiveForecastUnified] @PgrowerID, @PcropID, @farmID, @POriginID";

                    //DbParameter PgrowerID = command.CreateParameter();
                    //PgrowerID.ParameterName = "@PgrowerID";
                    //PgrowerID.Value = growerID;
                    //command.Parameters.Add(PgrowerID);

                    //DbParameter PcropID = command.CreateParameter();
                    //PcropID.ParameterName = "@PcropID";
                    //PcropID.Value = cropID;
                    //command.Parameters.Add(PcropID);

                    //DbParameter PfarmID = command.CreateParameter();
                    //PfarmID.ParameterName = "@farmID";
                    //PfarmID.Value = farmID;
                    //command.Parameters.Add(PfarmID);

                    //DbParameter POriginID = command.CreateParameter();
                    //POriginID.ParameterName = "@POriginID";
                    //POriginID.Value = originID;
                    //command.Parameters.Add(POriginID);

                    //context.Database.OpenConnection();

                    //result = command. ExecuteReader();
                    //var dataTable = new DataTable();

                    //DataSet ds = new DataSet();
                    //ds.Tables.Add(dataTable);


                    //return (ds);
                    DataSet dataset = new DataSet();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", false).Build();

                    var sDbConnection = configuration["connectionStrings:ConexBdAgroCom"];
                    SqlConnection conn = new SqlConnection(sDbConnection);

                    SqlCommand command = new SqlCommand("package.sp_massiveForecastUnified", conn);
                    command.Parameters.AddWithValue("@PgrowerID", growerID);
                    command.Parameters.AddWithValue("@PcropID", cropID);
                    command.Parameters.AddWithValue("@farmID", farmID);
                    command.Parameters.AddWithValue("@POriginID", originID);

                    adapter.SelectCommand = command;
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapter.Fill(dataset);
                    return dataset;
                //}
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }

        }

        public DataTable ListForCommercePlan(string growerId, int seasonID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commerceForecastbyGrowerList2] @PgrowerID, @PcampaignID";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@PgrowerID";
                    Plogin.Value = growerId;
                    command.Parameters.Add(Plogin);

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PcampaignID";
                    PsearchOpt.Value = seasonID;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable PercentSaveUpdate(int userID, int lotID, int projectedWeekID, int quantityTotal, string commentary, List<typecollectionLarge> percentBySize_table)
        {

            commentary = commentary ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionBySizeModify2] @PUserID, @PlotID, @PprojectedWeekID, @PquantityTotal, @Pcommentary,@PpercentBySize_table";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PUserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PlotID = command.CreateParameter();
                    PlotID.ParameterName = "@PlotID";
                    PlotID.Value = lotID;
                    command.Parameters.Add(PlotID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PquantityTotal = command.CreateParameter();
                    PquantityTotal.ParameterName = "@PquantityTotal";
                    PquantityTotal.Value = quantityTotal;
                    command.Parameters.Add(PquantityTotal);

                    DbParameter Pcommentary = command.CreateParameter();
                    Pcommentary.ParameterName = "@Pcommentary";
                    Pcommentary.Value = commentary;
                    command.Parameters.Add(Pcommentary);

                    var tbl = new DataTable();
                    tbl.Columns.Add("int1");
                    tbl.Columns.Add("int2");
                    tbl.Columns.Add("int3");
                    tbl.Columns.Add("int4");
                    tbl.Columns.Add("int5");
                    tbl.Columns.Add("int6");
                    tbl.Columns.Add("description1");
                    tbl.Columns.Add("description2");
                    tbl.Columns.Add("description3");
                    tbl.Columns.Add("description4");
                    tbl.Columns.Add("description5");
                    tbl.Columns.Add("decimal1");
                    tbl.Columns.Add("decimal2");
                    tbl.Columns.Add("decimal3");
                    tbl.Columns.Add("decimal4");


                    foreach (var item in percentBySize_table)
                    {
                        tbl.Rows.Add(0, 0, 0, 0, 0, 0, item.description1, item.description2, "", "", "", item.decimal1, 0, 0, 0);
                    }

                    var PpercentBySize_table = new SqlParameter("@PpercentBySize_table", SqlDbType.Structured);
                    PpercentBySize_table.Value = tbl;
                    PpercentBySize_table.TypeName = "typecollectionlarge";
                    command.Parameters.Add(PpercentBySize_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable SaveForecast(int userID, int lotID, int projectedWeekID, decimal quantityTotal, string commentary, List<typecollectionLarge> percentBySize_table)
        {
            commentary = commentary ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionInsert] @PUserID, @PlotID, @PprojectedWeekID, @PquantityTotal, @Pcommentary, @PpercentBySize_table";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PUserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PlotID = command.CreateParameter();
                    PlotID.ParameterName = "@PlotID";
                    PlotID.Value = lotID;
                    command.Parameters.Add(PlotID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PquantityTotal = command.CreateParameter();
                    PquantityTotal.ParameterName = "@PquantityTotal";
                    PquantityTotal.Value = quantityTotal;
                    command.Parameters.Add(PquantityTotal);

                    DbParameter Pcommentary = command.CreateParameter();
                    Pcommentary.ParameterName = "@Pcommentary";
                    Pcommentary.Value = commentary;
                    command.Parameters.Add(Pcommentary);

                    var tbl = new DataTable();
                    tbl.Columns.Add("int1");
                    tbl.Columns.Add("int2");
                    tbl.Columns.Add("int3");
                    tbl.Columns.Add("int4");
                    tbl.Columns.Add("int5");
                    tbl.Columns.Add("int6");
                    tbl.Columns.Add("description1");
                    tbl.Columns.Add("description2");
                    tbl.Columns.Add("description3");
                    tbl.Columns.Add("description4");
                    tbl.Columns.Add("description5");
                    tbl.Columns.Add("decimal1");
                    tbl.Columns.Add("decimal2");
                    tbl.Columns.Add("decimal3");
                    tbl.Columns.Add("decimal4");


                    foreach (var item in percentBySize_table)
                    {
                        tbl.Rows.Add(0, 0, 0, 0, 0, 0, item.description1, item.description2, item.description3, "", "", item.decimal1, 0, 0, 0);
                    }

                    var PpercentBySize_table = new SqlParameter("@PpercentBySize_table", SqlDbType.Structured);
                    PpercentBySize_table.Value = tbl;
                    PpercentBySize_table.TypeName = "typecollectionLarge";
                    command.Parameters.Add(PpercentBySize_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetProjectedProductionBySizeList(string userID, string dateIni, string dateFin, string growerID)
        {
            growerID = growerID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionBySizeList] @Plogin, @PdateIni, @PdateFin, @PgrowerID";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = userID;
                    command.Parameters.Add(Plogin);

                    DbParameter PdateIni = command.CreateParameter();
                    PdateIni.ParameterName = "@PdateIni";
                    PdateIni.Value = dateIni;
                    command.Parameters.Add(PdateIni);

                    DbParameter PdateFin = command.CreateParameter();
                    PdateFin.ParameterName = "@PdateFin";
                    PdateFin.Value = dateFin;
                    command.Parameters.Add(PdateFin);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetProjecteProductionDetailsList(string login, string dateIni, string dateFin, string growerID)
        {
            growerID = growerID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionDetails] @Plogin, @PdateIni, @PdateFin, @PgrowerID";
                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = login;
                    command.Parameters.Add(Plogin);

                    DbParameter PdateIni = command.CreateParameter();
                    PdateIni.ParameterName = "@PdateIni";
                    PdateIni.Value = dateIni;
                    command.Parameters.Add(PdateIni);

                    DbParameter PdateFin = command.CreateParameter();
                    PdateFin.ParameterName = "@PdateFin";
                    PdateFin.Value = dateFin;
                    command.Parameters.Add(PdateFin);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetProjectedProductionBySizePercentList(string login, string projectedWeekID, string loteID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionBySizepPercentList] @Plogin, @PprojectedWeekID, @PloteID";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = login;
                    command.Parameters.Add(Plogin);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PloteID = command.CreateParameter();
                    PloteID.ParameterName = "@PloteID";
                    PloteID.Value = loteID;
                    command.Parameters.Add(PloteID);
                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetSaveMasiveForecast(int LotID, string BrandID, string SizeID, int WeekStart, int campaign, float mount1, float mount2, float mount3, float mount4)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projectedProduction_bulkImport] @PlotID, @PcategoryID, @PsizeID, @Pyear, @PnumberWeek, @Pquantity, @Pquantity2, @Pquantity3, @Pquantity4";

                    DbParameter PlotID = command.CreateParameter();
                    PlotID.ParameterName = "@PlotID";
                    PlotID.Value = LotID;
                    command.Parameters.Add(PlotID);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = BrandID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PsizeID = command.CreateParameter();
                    PsizeID.ParameterName = "@PsizeID";
                    PsizeID.Value = SizeID;
                    command.Parameters.Add(PsizeID);

                    DbParameter Pyear = command.CreateParameter();
                    Pyear.ParameterName = "@Pyear";
                    Pyear.Value = campaign;
                    command.Parameters.Add(Pyear);

                    DbParameter PnumberWeek = command.CreateParameter();
                    PnumberWeek.ParameterName = "@PnumberWeek";
                    PnumberWeek.Value = WeekStart;
                    command.Parameters.Add(PnumberWeek);

                    DbParameter Pquantity = command.CreateParameter();
                    Pquantity.ParameterName = "@Pquantity";
                    Pquantity.Value = mount1;
                    command.Parameters.Add(Pquantity);

                    DbParameter Pquantity2 = command.CreateParameter();
                    Pquantity2.ParameterName = "@Pquantity2";
                    Pquantity2.Value = mount2;
                    command.Parameters.Add(Pquantity2);

                    DbParameter Pquantity3 = command.CreateParameter();
                    Pquantity3.ParameterName = "@Pquantity3";
                    Pquantity3.Value = mount3;
                    command.Parameters.Add(Pquantity3);

                    DbParameter Pquantity4 = command.CreateParameter();
                    Pquantity4.ParameterName = "@Pquantity4";
                    Pquantity4.Value = mount4;
                    command.Parameters.Add(Pquantity4);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public bool SaveMasiveUploadForecast(MassiveUploadCab objMassiveUpload, string userID, int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_projectedProduction_massiveupload]";
                    command.CommandType = CommandType.StoredProcedure;
                    //command.CommandTimeout = 30000000;

                    SqlParameter PMassiveUpload = new SqlParameter("@PMassiveUpload", SqlDbType.Structured);
                    command.Parameters.Add(PMassiveUpload);

                    SqlParameter PUsuario = new SqlParameter("@PUsuario", SqlDbType.VarChar);
                    PUsuario.Value = userID;
                    command.Parameters.Add(PUsuario);

                    SqlParameter PcampaignID = new SqlParameter("@PcampaignID", SqlDbType.Int);
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    SqlParameter PResult = new SqlParameter("@PResult", SqlDbType.Bit) { Direction = ParameterDirection.Output };
                    command.Parameters.Add(PResult);

                    DataTable dttProjectedProduction = new DataTable("MassiveUpload");

                    DataColumn dc = new DataColumn();
                    dc.ColumnName = "ID";
                    dc.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc);

                    DataColumn dc1 = new DataColumn();
                    dc1.ColumnName = "lotID";
                    dc1.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc1);

                    DataColumn dc2 = new DataColumn();
                    dc2.ColumnName = "categoryID";
                    dc2.DataType = typeof(string);
                    dttProjectedProduction.Columns.Add(dc2);

                    DataColumn dc3 = new DataColumn();
                    dc3.ColumnName = "sizeID";
                    dc3.DataType = typeof(string);
                    dttProjectedProduction.Columns.Add(dc3);

                    DataColumn dc4 = new DataColumn();
                    dc4.ColumnName = "year";
                    dc4.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc4);

                    DataColumn dc5 = new DataColumn();
                    dc5.ColumnName = "numberWeek";
                    dc5.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc5);

                    DataColumn dc6 = new DataColumn();
                    dc6.ColumnName = "quantity";
                    dc6.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc6);

                    DataColumn dc7 = new DataColumn();
                    dc7.ColumnName = "quantity2";
                    dc7.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc7);

                    DataColumn dc8 = new DataColumn();
                    dc8.ColumnName = "quantity3";
                    dc8.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc8);

                    DataColumn dc9 = new DataColumn();
                    dc9.ColumnName = "quantity4";
                    dc9.DataType = typeof(int);
                    dttProjectedProduction.Columns.Add(dc9);

                    foreach (MassiveUploadDet o in objMassiveUpload.massiveUploadDetail)
                    {
                        dttProjectedProduction.Rows.Add(new object[] {

                        o.ID, o.LotID, o.CategoryID, o.SizeID,  o.Campaign, o.WeekStart,o.mount1,o.mount2,o.mount3,o.mount4

                    });
                    }

                    PMassiveUpload.Value = dttProjectedProduction;
                    context.Database.OpenConnection();
                    command.ExecuteNonQuery();
                    return bool.Parse(PResult.Value.ToString());

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetForecastMasive(string growerID, string cropID)
        {
            cropID = cropID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_forecastMasiveUploadList] @PgrowerID, @PcropID";

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetForecastBySales(string opt, int? campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_ForecastbySalesList] @PsearchOpt,@PcampaignID";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@PsearchOpt";
                    Popt.Value = opt;
                    command.Parameters.Add(Popt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetForecastBySaleID(int saleID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_forecastByVarietySalesList] @PsaleID";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@PsaleID";
                    Popt.Value = saleID;
                    command.Parameters.Add(Popt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetVariationFor()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_forecastVariationByWeekList] ";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetReportForecast(string opt)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_forecastRealReportList] @PsearchOpt";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@PsearchOpt";
                    Popt.Value = opt;
                    command.Parameters.Add(Popt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetProjectedProductionByVarietyList(string userID, string dateIni, string dateFin, int campaignID, string farmID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_projecteProductionByVarietyList] @Plogin, @PdateIni, @PdateFin, @PcampaignID, @PfarmID";

                    DbParameter Plogin = command.CreateParameter();
                    Plogin.ParameterName = "@Plogin";
                    Plogin.Value = userID;
                    command.Parameters.Add(Plogin);

                    DbParameter PdateIni = command.CreateParameter();
                    PdateIni.ParameterName = "@PdateIni";
                    PdateIni.Value = dateIni;
                    command.Parameters.Add(PdateIni);

                    DbParameter PdateFin = command.CreateParameter();
                    PdateFin.ParameterName = "@PdateFin";
                    PdateFin.Value = dateFin;
                    command.Parameters.Add(PdateFin);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@PfarmID";
                    PfarmID.Value = farmID;
                    command.Parameters.Add(PfarmID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetForecastByCropID(string cropID, int seasonID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commerceForecastbyCropList] @PcropID, @PcampaignID";

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PseasonID = command.CreateParameter();
                    PseasonID.ParameterName = "@PcampaignID";
                    PseasonID.Value = seasonID;
                    command.Parameters.Add(PseasonID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetTitleFormByCropID(string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_TitleFormByCrop] @PcropId";

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropId";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}