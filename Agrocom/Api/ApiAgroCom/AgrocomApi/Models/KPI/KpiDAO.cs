﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using Common.KPI;

namespace AgrocomApi.Models.KPI
{
    public class KpiDAO
    {
        private readonly ApplicationDbContext context;

        public KpiDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool GetKpiSplash(string cropID,out EKpi objKPI)
        {
            SqlDataAdapter objDta = null;
            SqlConnection objCnx = null;
            var bRsl = false;

            try
            {
                objCnx = new SqlConnection(context.Database.GetDbConnection().ConnectionString);
                using (var objCmd = new SqlCommand("[package].[sp_KPI_GetComercialComexKPIs]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@cropID", SqlDbType.Char,3).Value = cropID;

                    objDta = new SqlDataAdapter();
                    objDta.SelectCommand = objCmd;
                    var objDts = new DataSet();
                    objDta.Fill(objDts);

                    DataTable dttScheduleBoarding = objDts.Tables[0];
                    DataTable dttKoreanDestination = objDts.Tables[1];
                    DataTable dttShippingCompanies = objDts.Tables[2];

                    objKPI = null;
                    objKPI = new EKpi();
                    objKPI.lstScheduleBoarding = new List<EScheduleBoarding>();
                    foreach(DataRow objDtr in dttScheduleBoarding.Rows)
                    {
                        objKPI.lstScheduleBoarding.Add(new EScheduleBoarding()
                        {
                            customerID = objDtr["customerID"].ToString(),
                            customer = objDtr["customer"].ToString(),
                            projectedWeekID = objDtr["projectedWeekID"].ToString(),
                            number = objDtr["number"].ToString(),
                            kpi = objDtr["kpi"].ToString(),
                            cropID = objDtr["cropID"].ToString(),
                            campaign = objDtr["campaign"].ToString(),
                        });
                    }

                    objKPI.lstKoreanDestination = new List<EKoreanDestination>();
                    foreach (DataRow objDtr in dttKoreanDestination.Rows)
                    {
                        objKPI.lstKoreanDestination.Add(new EKoreanDestination()
                        {
                            customerID = objDtr["customerID"].ToString(),
                            customer = objDtr["customer"].ToString(),
                            kpi = objDtr["kpi"].ToString(),
                            cropID = objDtr["cropID"].ToString(),
                            campaign = objDtr["campaign"].ToString(),
                        });
                    }

                    objKPI.lstShippingCompanies = new List<EShippingCompanies>();
                    foreach (DataRow objDtr in dttShippingCompanies.Rows)
                    {
                        objKPI.lstShippingCompanies.Add(new EShippingCompanies()
                        {
                            shippingCompanyID = objDtr["shippingCompanyID"].ToString(),
                            shippingCompany = objDtr["shippingCompany"].ToString(),
                            projectedWeekID = objDtr["projectedWeekID"].ToString(),
                            number = objDtr["number"].ToString(),
                            kpi = objDtr["kpi"].ToString(),
                            cropID = objDtr["cropID"].ToString(),
                            campaign = objDtr["campaign"].ToString(),
                        });
                    }
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        objCnx.Close();
                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
