﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class LogErrorDAO
    {
        private readonly ApplicationDbContext context;
        public LogErrorDAO(ApplicationDbContext context)
        {
            this.context = context;
        }
        public bool LogErrorInsert(
            string LogErrorProyect,
            string LogErrorDescription,
            string LogErrorService,
            string LogErrorParams
            )
        {
            bool bResult = false;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_LogErrorInsert] @PLogErrorProyect, @PLogErrorDescription,@PLogErrorService,@PLogErrorParams";

                    DbParameter PLogErrorProyect = command.CreateParameter();
                    PLogErrorProyect.ParameterName = "@PLogErrorProyect";
                    PLogErrorProyect.Value = LogErrorProyect;
                    command.Parameters.Add(PLogErrorProyect);

                    DbParameter PLogErrorDescription = command.CreateParameter();
                    PLogErrorDescription.ParameterName = "@PLogErrorDescription";
                    PLogErrorDescription.Value = LogErrorDescription;
                    command.Parameters.Add(PLogErrorDescription);

                    DbParameter PLogErrorService = command.CreateParameter();
                    PLogErrorService.ParameterName = "@PLogErrorService";
                    PLogErrorService.Value = LogErrorService;
                    command.Parameters.Add(PLogErrorService);

                    DbParameter PLogErrorParams = command.CreateParameter();
                    PLogErrorParams.ParameterName = "PLogErrorParams";
                    PLogErrorParams.Value = LogErrorParams;
                    command.Parameters.Add(PLogErrorParams);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    if (!result.HasRows) return bResult;
                    while (result.Read())
                        bResult = bool.Parse(result[0].ToString());

                }
            }
            catch (System.Exception ex)
            {
                //throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bResult;

        }
    }
}
