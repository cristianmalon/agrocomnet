﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class MarketDAO
    {
        private readonly ApplicationDbContext context;

        public MarketDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetMarket(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_marketList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetMarketUpdate(string marketID, string name, string abbreviation, string percentage, int statusID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_marketCreate] @PmarketID,@Pname,@Pabbreviation,@Ppercentage,@PstatusID";

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);

                    DbParameter Pname = command.CreateParameter();
                    Pname.ParameterName = "@Pname";
                    Pname.Value = name;
                    command.Parameters.Add(Pname);

                    DbParameter Pabbreviation = command.CreateParameter();
                    Pabbreviation.ParameterName = "@Pabbreviation";
                    Pabbreviation.Value = abbreviation;
                    command.Parameters.Add(Pabbreviation);

                    DbParameter Ppercentage = command.CreateParameter();
                    Ppercentage.ParameterName = "@Ppercentage";
                    Ppercentage.Value = percentage;
                    command.Parameters.Add(Ppercentage);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = statusID;
                    command.Parameters.Add(PstatusID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetMarketCodepackCreate(string marketID, string codepackID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_marketCodepackCreate] @PmarketID, @PcodepackID";

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);

                    DbParameter PcodepackID = command.CreateParameter();
                    PcodepackID.ParameterName = "@PcodepackID";
                    PcodepackID.Value = codepackID;
                    command.Parameters.Add(PcodepackID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetMarketCodepackDisable(string marketID, string codepackID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_marketCodepackDisable] @PmarketID, @PcodePackID";

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);

                    DbParameter PcodepackID = command.CreateParameter();
                    PcodepackID.ParameterName = "@PcodePackID";
                    PcodepackID.Value = codepackID;
                    command.Parameters.Add(PcodepackID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
