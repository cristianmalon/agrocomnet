﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using System.Net;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using Common.Notification;

namespace AgrocomApi.Models.Notifications
{
    public class NotificationDAO
    {
        private readonly ApplicationDbContext context;

        public NotificationDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool GetNotificationsSplash(string sUsertypeID,string sCropId,out List<ENotification> lstNotification)
        {
            DbDataReader result = null;
            SqlConnection objCnx = null;
            var bRsl = false;

            try
            {
                objCnx = new SqlConnection(context.Database.GetDbConnection().ConnectionString);
                using (var objCmd = new SqlCommand("[package].[sp_Notification_GetPendientsByTypeUser]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@vUsertypeID", SqlDbType.Char,3).Value = sUsertypeID;
                    objCmd.Parameters.Add("@vCropId", SqlDbType.Char, 3).Value = sCropId;

                    objCnx.Open();
                    result = objCmd.ExecuteReader();
                    lstNotification = null;
                    if (!result.HasRows) return bRsl;
                    lstNotification = new List<ENotification>();
                    while (result.Read())
                    {
                        lstNotification.Add(new ENotification()
                        {
                            requestNumber   = result.GetString("RequestNumber"),
                            mailSubject     = result.GetString("MailSubject"),
                            mailDescription = result.GetString("MailDescription"),
                            toEmail         = result.GetString("toEmail"),
                            nextProcess     = result.GetString("nextProcess"),
                        });
                    }
                    
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
