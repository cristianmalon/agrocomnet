﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class PODAO
    {
        private readonly ApplicationDbContext context;

        public PODAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetAll(int WIni, int WFin)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionDetailByDate] @PINI, @PFIN";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PINI";
                    PsearchOpt.Value = WIni;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PFIN";
                    PsearchValue.Value = WFin;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetSalesPendingByWeek(int weekID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesByWeekWithoutOP] @PprojectedWeekID";

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = weekID;
                    command.Parameters.Add(PprojectedWeekID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetVarietyForecast(string saleDetailID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_forecastByVariety] @PsaleDetailID";

                    DbParameter PsaleDetailID = command.CreateParameter();
                    PsaleDetailID.ParameterName = "@PsaleDetailID";
                    PsaleDetailID.Value = saleDetailID;
                    command.Parameters.Add(PsaleDetailID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetVarietyForecastDetail(string growerID, string farmID, string saleDetailID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_forecastByVarietyDetail] @PgrowerID, @PfarmID, @PsaleDetailID";

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@PfarmID";
                    PfarmID.Value = farmID;
                    command.Parameters.Add(PfarmID);

                    DbParameter PsaleDetailID = command.CreateParameter();
                    PsaleDetailID.ParameterName = "@PsaleDetailID";
                    PsaleDetailID.Value = saleDetailID;
                    command.Parameters.Add(PsaleDetailID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public List<ForecastByVarietyDetail> ENGetVarietyForecastDetail(string growerID, string farmID, string saleDetailID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    var lista = new List<ForecastByVarietyDetail>();
                    command.CommandText = "exec [package].[sp_forecastByVarietyDetail] @PgrowerID, @PfarmID, @PsaleDetailID";

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@PfarmID";
                    PfarmID.Value = farmID;
                    command.Parameters.Add(PfarmID);

                    DbParameter PsaleDetailID = command.CreateParameter();
                    PsaleDetailID.ParameterName = "@PsaleDetailID";
                    PsaleDetailID.Value = saleDetailID;
                    command.Parameters.Add(PsaleDetailID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var o = new ForecastByVarietyDetail();
                        o.grower = row["GROWER"].ToString();
                        o.farm = row["FARM"].ToString();
                        o.brand = row["BRAND"].ToString();
                        o.varietyID = row["VARIETYID"].ToString();
                        o.variety = row["VARIETY"].ToString();
                        o.size = row["SIZE"].ToString();
                        o.year = row["YEAR"].ToString();
                        o.week = row["WEEK"].ToString();
                        o.boxpallet = decimal.Parse(row["BOXPALLET"].ToString());
                        o.palletavailable = decimal.Parse(row["PALLETAVAILABLE"].ToString());
                        o.boxavailable = decimal.Parse(row["BOXAVAILABLE"].ToString());
                        o.available = decimal.Parse(row["AVAILABLE"].ToString());
                        o.kgbox = decimal.Parse(row["KGBOX"].ToString());
                        o.format = row["FORMAT"].ToString();
                        o.projectedProductionSizeCategoryID = int.Parse(row["projectedProductionSizeCategoryID"].ToString());
                        o.growerID = row["growerID"].ToString();
                        o.farmID = row["farmID"].ToString();
                        var oLSizes = new List<Size>();
                        foreach (var item in row["SIZES"].ToString().Split(','))
                        {
                            oLSizes.Add(new Size { sizeID = item, size = item });
                        }
                        var oLVarieties = new List<Variety>();
                        foreach (var item in row["VARIETIES"].ToString().Split(','))
                        {
                            oLVarieties.Add(new Variety { varietyID = int.Parse(item.Split('-')[0]), variety = item.Split('-')[1] });
                        }
                        o.sizes = oLSizes;
                        o.varieties = oLVarieties;
                        lista.Add(o);
                    }

                    return lista;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public List<ForecastByVarietyDetail> ENGetVarietyForecastDetailForPOEdit(string opt, string growerID, int? saleDetailID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    var lista = new List<ForecastByVarietyDetail>();
                    command.CommandText = "exec [package].[sp_forecastByVarietyDetailForPOEdit] @PsearchOpt,@PgrowerID,@PsaleDetailID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PsaleDetailID = command.CreateParameter();
                    PsaleDetailID.ParameterName = "@PsaleDetailID";
                    PsaleDetailID.Value = saleDetailID;
                    command.Parameters.Add(PsaleDetailID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var o = new ForecastByVarietyDetail();
                        o.grower = row["GROWER"].ToString();
                        o.farm = row["FARM"].ToString();
                        o.brand = row["BRAND"].ToString();
                        o.varietyID = row["VARIETYID"].ToString();
                        o.variety = row["VARIETY"].ToString();
                        o.size = row["SIZE"].ToString();
                        o.year = row["YEAR"].ToString();
                        o.week = row["WEEK"].ToString();
                        o.boxpallet = decimal.Parse(row["BOXPALLET"].ToString());
                        o.palletavailable = decimal.Parse(row["PALLETAVAILABLE"].ToString());
                        o.boxavailable = decimal.Parse(row["BOXAVAILABLE"].ToString());
                        o.available = decimal.Parse(row["AVAILABLE"].ToString());
                        o.kgbox = decimal.Parse(row["KGBOX"].ToString());
                        o.format = row["FORMAT"].ToString();
                        o.projectedProductionSizeCategoryID = int.Parse(row["projectedProductionSizeCategoryID"].ToString());
                        o.growerID = row["growerID"].ToString();
                        o.farmID = row["farmID"].ToString();
                        var oLSizes = new List<Size>();
                        foreach (var item in row["SIZES"].ToString().Split(','))
                        {
                            oLSizes.Add(new Size { sizeID = item, size = item });
                        }
                        var oLVarieties = new List<Variety>();
                        foreach (var item in row["VARIETIES"].ToString().Split(','))
                        {
                            oLVarieties.Add(new Variety { varietyID = int.Parse(item.Split('-')[0]), variety = item.Split('-')[1] });
                        }
                        o.sizes = oLSizes;
                        o.varieties = oLVarieties;
                        o.saleDetailID = int.Parse(row["saleDetailID"].ToString());
                        o.categoryID = row["categoryID"].ToString();
                        lista.Add(o);
                    }

                    return lista;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable CreateUpdateProductionOrder(int orderProductionID, string projectedweekID, string saleID, string specificationID, string commentary, string userCreated, int campaignID)
        {
            commentary = commentary ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_orderProductionCreateUpdate] @PorderProductionID, @PprojectedweekID, @PsaleID, @PspecificationID, @Pcommentary, @PuserCreated, @PcampaignID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PprojectedweekID = command.CreateParameter();
                    PprojectedweekID.ParameterName = "@PprojectedweekID";
                    PprojectedweekID.Value = projectedweekID;
                    command.Parameters.Add(PprojectedweekID);

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter PspecificationID = command.CreateParameter();
                    PspecificationID.ParameterName = "@PspecificationID";
                    PspecificationID.Value = specificationID;
                    command.Parameters.Add(PspecificationID);

                    DbParameter Pcommentary = command.CreateParameter();
                    Pcommentary.ParameterName = "@Pcommentary";
                    Pcommentary.Value = commentary;
                    command.Parameters.Add(Pcommentary);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable CreateUpdateProductionOrderDetail(string orderProductionDetailID, string orderProductionID, string saleDetailID, string projectedProductionDetailID, string Quantity, string userCreated, string sizeID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_orderProductionDetailCreateUpdate] @PorderProductionDetailID, @PorderProductionID, @PsaleDetailID, @PprojectedProductionDetailID, @PQuantity, @PuserCreated, @PsizeID";

                    DbParameter PorderProductionDetailID = command.CreateParameter();
                    PorderProductionDetailID.ParameterName = "@PorderProductionDetailID";
                    PorderProductionDetailID.Value = orderProductionDetailID;
                    command.Parameters.Add(PorderProductionDetailID);

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PsaleDetailID = command.CreateParameter();
                    PsaleDetailID.ParameterName = "@PsaleDetailID";
                    PsaleDetailID.Value = saleDetailID;
                    command.Parameters.Add(PsaleDetailID);

                    DbParameter PprojectedProductionDetailID = command.CreateParameter();
                    PprojectedProductionDetailID.ParameterName = "@PprojectedProductionDetailID";
                    PprojectedProductionDetailID.Value = projectedProductionDetailID;
                    command.Parameters.Add(PprojectedProductionDetailID);

                    DbParameter PQuantity = command.CreateParameter();
                    PQuantity.ParameterName = "@PQuantity";
                    PQuantity.Value = Quantity;
                    command.Parameters.Add(PQuantity);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PsizeID = command.CreateParameter();
                    PsizeID.ParameterName = "@PsizeID";
                    PsizeID.Value = sizeID;
                    command.Parameters.Add(PsizeID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetListForConfirm(int WIni, int WFin, int userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionDetailByDateGroupByQuantity] @PINI, @PFIN,@PUSERID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PINI";
                    PsearchOpt.Value = WIni;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PFIN";
                    PsearchValue.Value = WFin;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PUSERID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetDetailConfirmById(string Option, int IdPO)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionDetailList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = Option;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = IdPO;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetForPdf(int orderProductionID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionByID] @PorderProductionID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetConfirmById(string searchOpt, int orderProductionID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = orderProductionID;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }


        public DataTable OrderProductionConfirm(int orderProductionID, string statusConfirm, string dateBoardEstimate, List<typecollection> quantityConfirm, int userID, string reason)
        {
            reason = reason ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionConfirm] @PorderProductionID,@PstatusConfirm,@PdateBoardEstimate,@PuserID, @Preason,@PquantityConfirm";

                    command.Parameters.Add(new SqlParameter("@PorderProductionID", orderProductionID));
                    command.Parameters.Add(new SqlParameter("@PstatusConfirm", statusConfirm));
                    command.Parameters.Add(new SqlParameter("@PdateBoardEstimate", dateBoardEstimate));
                    command.Parameters.Add(new SqlParameter("@PuserID", userID));
                    command.Parameters.Add(new SqlParameter("@Preason", reason));
                    //command.Parameters.Add(new SqlParameter("@PquantityConfirm", quantityConfirm));

                    var tbl = new DataTable();
                    tbl.Columns.Add("int1");
                    tbl.Columns.Add("int2");
                    tbl.Columns.Add("description1");
                    tbl.Columns.Add("description2");

                    foreach (var item in quantityConfirm)
                    {
                        tbl.Rows.Add(item.int1, item.int2, item.description1, item.description2);
                    }

                    var PquantityConfirm_table = new SqlParameter("@PquantityConfirm", SqlDbType.Structured);
                    PquantityConfirm_table.Value = tbl;
                    PquantityConfirm_table.TypeName = "typecollection";
                    command.Parameters.Add(PquantityConfirm_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetPOShipmentStByWeek(string searchOpt, string idUser, int beginDate, int endDate)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_ShipmentStatusProductionOrder] @PsearchOpt, @PsearchValue, @Pini, @Pfin";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = idUser;
                    command.Parameters.Add(PsearchValue);

                    DbParameter Pini = command.CreateParameter();
                    Pini.ParameterName = "@Pini";
                    Pini.Value = beginDate;
                    command.Parameters.Add(Pini);

                    DbParameter Pfin = command.CreateParameter();
                    Pfin.ParameterName = "@Pfin";
                    Pfin.Value = endDate;
                    command.Parameters.Add(Pfin);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPOrejected(int orderProductionID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_POrejectedList] @PorderProductionID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPOpending()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_POpendingList]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPOnotAttended()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_POnotAttended]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public List<PO> GetAGA(string searchOpt, int orderProductionID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = orderProductionID;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    var Lista = new List<PO>();

                    foreach (DataRow item in dataTable.Rows)
                    {
                        var o = new PO();
                        o.week = int.Parse(item["week"].ToString());
                        o.campaing = item["campaign"].ToString();
                        Lista.Add(o);
                    }
                    return (Lista);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable POModify(int orderProductionID, int userID, int statusID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_orderProductionModify] @PorderProductionID,@PuserCreated,@Pstatus";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userID;
                    command.Parameters.Add(PuserCreated);

                    DbParameter Pstatus = command.CreateParameter();
                    Pstatus.ParameterName = "@Pstatus";
                    Pstatus.Value = statusID;
                    command.Parameters.Add(Pstatus);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable POfilterList(string opt, string marketID, int? weekini, int? weekFin, string statusID, string growerID, int? campaignID)
        {
            marketID = marketID ?? "";
            weekini = weekini ?? 0;
            weekFin = weekFin ?? 0;
            statusID = statusID ?? "";
            growerID = growerID ?? "";
            campaignID = campaignID ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_OrderProductionFilterlist] @PsearchOpt,@PmarketID,@Pini,@Pfin,@Pstatus,@PgrowerID,@PcampaignID";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@PsearchOpt";
                    Popt.Value = opt;
                    command.Parameters.Add(Popt);

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);

                    DbParameter Pweekini = command.CreateParameter();
                    Pweekini.ParameterName = "@Pini";
                    Pweekini.Value = weekini;
                    command.Parameters.Add(Pweekini);

                    DbParameter PweekFin = command.CreateParameter();
                    PweekFin.ParameterName = "@Pfin";
                    PweekFin.Value = weekFin;
                    command.Parameters.Add(PweekFin);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@Pstatus";
                    PstatusID.Value = statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);



                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPOvariationBrand()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_POvariationBrandList]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPOConfirm(string opt, string plantID, int? weekProduction, int? weekProductionEnd, int? statusConfirmID, int? campaignID, int? userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionConfirmList] @PsearchOpt,@PplantID,@PweekProduction,@PweekProductionEnd,@PstatusConfirmID,@PcampaignID,@PuserID";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@PsearchOpt";
                    Popt.Value = opt;
                    command.Parameters.Add(Popt);

                    DbParameter PplantID = command.CreateParameter();
                    PplantID.ParameterName = "@PplantID";
                    PplantID.Value = plantID;
                    command.Parameters.Add(PplantID);

                    DbParameter PweekProduction = command.CreateParameter();
                    PweekProduction.ParameterName = "@PweekProduction";
                    PweekProduction.Value = weekProduction;
                    command.Parameters.Add(PweekProduction);

                    DbParameter PweekProductionEnd = command.CreateParameter();
                    PweekProductionEnd.ParameterName = "@PweekProductionEnd";
                    PweekProductionEnd.Value = weekProductionEnd;
                    command.Parameters.Add(PweekProductionEnd);

                    DbParameter PstatusConfirmID = command.CreateParameter();
                    PstatusConfirmID.ParameterName = "@PstatusConfirmID";
                    PstatusConfirmID.Value = statusConfirmID;
                    command.Parameters.Add(PstatusConfirmID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable OrderProductionConfirmStatus(string cropID, int orderProductionID, int statusConfirmID,
            string completedDate, string loadingDate, decimal vgm, string day, string userID, string comments,
            string packingID, int editAttemptsPacking, int PromiseCompliance, OrderProductionCab objOrderProduction)
        {
            completedDate = completedDate ?? "";
            loadingDate = loadingDate ?? "";
            day = day ?? "";
            packingID = packingID ?? string.Empty;
            comments = comments ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionConfirmStatus] @PcropID, @PorderProductionID," +
                        "@PstatusConfirmID,@PcompletedDate,@PloadingDate,@Pvgm,@Pday,@Pcomments,@PuserID,@PpackingID,@PeditAttemptsPacking,@PPromiseCompliance,@PorderProductionDetail";

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PstatusConfirmID = command.CreateParameter();
                    PstatusConfirmID.ParameterName = "@PstatusConfirmID";
                    PstatusConfirmID.Value = statusConfirmID;
                    command.Parameters.Add(PstatusConfirmID); 

                    DbParameter PcompletedDate = command.CreateParameter();
                    PcompletedDate.ParameterName = "@PcompletedDate";
                    PcompletedDate.Value = completedDate;
                    command.Parameters.Add(PcompletedDate);

                    DbParameter PloadingDate = command.CreateParameter();
                    PloadingDate.ParameterName = "@PloadingDate";
                    PloadingDate.Value = loadingDate;
                    command.Parameters.Add(PloadingDate);

                    DbParameter Pvgm = command.CreateParameter();
                    Pvgm.ParameterName = "@Pvgm";
                    Pvgm.Value = vgm;
                    command.Parameters.Add(Pvgm);

                    DbParameter Pday = command.CreateParameter();
                    Pday.ParameterName = "@Pday";
                    Pday.Value = day;
                    command.Parameters.Add(Pday);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PpackingID = command.CreateParameter();
                    PpackingID.ParameterName = "@PpackingID";
                    PpackingID.Value = packingID;
                    command.Parameters.Add(PpackingID);

                    DbParameter PeditAttemptsPacking = command.CreateParameter();
                    PeditAttemptsPacking.ParameterName = "@PeditAttemptsPacking";
                    PeditAttemptsPacking.Value = editAttemptsPacking;
                    command.Parameters.Add(PeditAttemptsPacking);

                    DbParameter PPromiseCompliance = command.CreateParameter();
                    PPromiseCompliance.ParameterName = "@PPromiseCompliance";
                    PPromiseCompliance.Value = PromiseCompliance;
                    command.Parameters.Add(PPromiseCompliance);

                    var dtOPdetail = new DataTable();
                    dtOPdetail.Columns.Add("Quantity", typeof(decimal));
                    dtOPdetail.Columns.Add("categoryID", typeof(string));
                    dtOPdetail.Columns.Add("varietyID", typeof(string));
                    dtOPdetail.Columns.Add("formatID", typeof(int));
                    dtOPdetail.Columns.Add("packageProductID", typeof(int));
                    dtOPdetail.Columns.Add("brandID", typeof(int));
                    dtOPdetail.Columns.Add("presentationID", typeof(int));
                    dtOPdetail.Columns.Add("labelID", typeof(int));
                    dtOPdetail.Columns.Add("codepackID", typeof(int));
                    dtOPdetail.Columns.Add("codepack", typeof(string));
                    dtOPdetail.Columns.Add("sizeID", typeof(string));
                    dtOPdetail.Columns.Add("priceBox", typeof(decimal));
                    dtOPdetail.Columns.Add("WorkOrder", typeof(int));

                    foreach (OrderProductionDet obj in objOrderProduction.orderProductionDetail)
                    {
                        dtOPdetail.Rows.Add(
                            obj.Quantity, obj.categoryID, obj.varietyID, obj.formatID,
                            obj.packageProductID, obj.brandID, obj.presentationID, obj.labelID, 
                            obj.codepackID, obj.codepack, obj.sizeID, obj.priceBox, obj.workOrder);
                    }

                    var PorderProductionDetail = new SqlParameter("@PorderProductionDetail", SqlDbType.Structured);
                    PorderProductionDetail.Value = dtOPdetail;
                    PorderProductionDetail.TypeName = "orderprod.tu_orderProductionDetailBlu";
                    command.Parameters.Add(PorderProductionDetail);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable POGenerateGrapes(ENPOGenerate o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionGenerate] @PsaleID,@Pcomments,@PuserID,@PcropID,@PcampaignID";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = o.saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = o.userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = o.cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = o.campaignID;
                    command.Parameters.Add(PcampaignID);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SavePOModified(ENPOModified o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_POCreateModified] @PorderProductionModifiedID,@PorderProductionID,@PsaleID,@PrequestNumber,@PprojectedWeekID,@Pcommentary,@PuserID,@Pstatus,@PstatusConfirm,@PgrowerID,@PcampaignID,@PfarmID,@PpoDetail_table";

                    DbParameter PorderProductionModifiedID = command.CreateParameter();
                    PorderProductionModifiedID.ParameterName = "@PorderProductionModifiedID";
                    PorderProductionModifiedID.Value = o.orderProductionModifiedID;
                    command.Parameters.Add(PorderProductionModifiedID);

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = o.orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = o.saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter PRequestNumber = command.CreateParameter();
                    PRequestNumber.ParameterName = "@PrequestNumber";
                    PRequestNumber.Value = o.RequestNumber;
                    command.Parameters.Add(PRequestNumber);

                    DbParameter PprojectedweekID = command.CreateParameter();
                    PprojectedweekID.ParameterName = "@PprojectedWeekID";
                    PprojectedweekID.Value = o.projectedweekID;
                    command.Parameters.Add(PprojectedweekID);

                    DbParameter Pcommentary = command.CreateParameter();
                    Pcommentary.ParameterName = "@Pcommentary";
                    Pcommentary.Value = o.commentary;
                    command.Parameters.Add(Pcommentary);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserID";
                    PuserCreated.Value = o.userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@Pstatus";
                    PstatusID.Value = o.statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PstatusConfirm = command.CreateParameter();
                    PstatusConfirm.ParameterName = "@PstatusConfirm";
                    PstatusConfirm.Value = o.statusConfirm;
                    command.Parameters.Add(PstatusConfirm);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = o.growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = o.campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@PfarmID";
                    PfarmID.Value = o.farmID;
                    command.Parameters.Add(PfarmID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("orderProductionDetailModifiedID");
                    tbl.Columns.Add("orderProductionDetailID");
                    tbl.Columns.Add("orderProductionID");
                    tbl.Columns.Add("saleDetailID");

                    tbl.Columns.Add("projectedProductionDetailID");
                    tbl.Columns.Add("Quantity");
                    tbl.Columns.Add("userID");
                    tbl.Columns.Add("statusID");

                    tbl.Columns.Add("quantityConfirm");
                    tbl.Columns.Add("sizeID");
                    tbl.Columns.Add("categoryID");
                    tbl.Columns.Add("varietyID");

                    tbl.Columns.Add("codePackID");
                    tbl.Columns.Add("presentationID");
                    tbl.Columns.Add("formatID");
                    tbl.Columns.Add("packageProductID");

                    tbl.Columns.Add("brandID");
                    tbl.Columns.Add("labelID");
                    tbl.Columns.Add("orderProductionModifiedID");

                    foreach (var item in o.details)
                    {
                        tbl.Rows.Add(item.orderProductionDetailModifiedID, item.orderProductionDetailID, item.orderProductionID, item.saleDetailID,
                            item.projectedProductionDetailID, item.Quantity, item.userID, item.statusID, item.quantityConfirm, item.sizeID, item.categoryID,
                            item.varietyID, item.codePackID, item.presentationID, item.formatID, item.packageProductID, item.brandID,
                            item.labelID, item.orderProductionModifiedID);
                    }

                    var PsaleDetail_table = new SqlParameter("@PpoDetail_table", SqlDbType.Structured);
                    PsaleDetail_table.Value = tbl;
                    PsaleDetail_table.TypeName = "[orderprod].[orderProductionDetailModified]";
                    command.Parameters.Add(PsaleDetail_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetBoardingProgramming(string opt, int customerID, int weekID, int destinationID, int statusID, int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_boardingProgrammingList] @PsearchOpt,@PcustomerID,@PweekID,@PdestinationID,@PstatusID,@PcampaignID";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@PsearchOpt";
                    Popt.Value = opt;
                    command.Parameters.Add(Popt);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PweekID = command.CreateParameter();
                    PweekID.ParameterName = "@PweekID";
                    PweekID.Value = weekID;
                    command.Parameters.Add(PweekID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PstatusID = command.CreateParameter();
                    PstatusID.ParameterName = "@PstatusID";
                    PstatusID.Value = statusID;
                    command.Parameters.Add(PstatusID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPObyPlant(int userID, ENPObyPlant PObyPlantt)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_massivePObyPlant] @PuserID, @PorderProductionPacking";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    var pobyplant = new DataTable();
                    pobyplant.Columns.Add("orderProductionID", typeof(int));
                    pobyplant.Columns.Add("packingID", typeof(string));

                    foreach (PObyPlant obj in PObyPlantt.PObyPlanta)
                    {
                        pobyplant.Rows.Add(obj.orderProductionID, obj.packingID);
                    }

                    var PorderProductionPlant = new SqlParameter("@PorderProductionPacking", SqlDbType.Structured);
                    PorderProductionPlant.Value = pobyplant;
                    PorderProductionPlant.TypeName = "orderprod.massiveByPlant";
                    command.Parameters.Add(PorderProductionPlant);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public bool VerifySaleOrderInProcess(string correlativeSaleOrder)
        {
            bool bResultado = false;
            DbDataReader objReader = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [netsuite].[sp_statusSalesOrder] @Pso_agrocom,@PstatusSalesOrder output";

                    DbParameter Pso_agrocom = command.CreateParameter();
                    Pso_agrocom.ParameterName = "@Pso_agrocom";
                    Pso_agrocom.Value = correlativeSaleOrder;
                    command.Parameters.Add(Pso_agrocom);

                    DbParameter PstatusSalesOrder = command.CreateParameter();
                    PstatusSalesOrder.ParameterName = "@PstatusSalesOrder";
                    PstatusSalesOrder.Direction = ParameterDirection.Output;
                    PstatusSalesOrder.DbType = DbType.Int32;
                    command.Parameters.Add(PstatusSalesOrder);

                    context.Database.OpenConnection();
                    command.ExecuteNonQuery();
                    bResultado = command.Parameters["@PstatusSalesOrder"].Value.ToString().Equals("1") ? true : false;
                    return bResultado;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objReader != null && !objReader.IsClosed) objReader.Close(); 
                    if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
        
    }
}
