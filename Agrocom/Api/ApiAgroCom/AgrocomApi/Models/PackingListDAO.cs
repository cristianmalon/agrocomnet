﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace AgrocomApi.Models
{
    public class PackingListDAO
    {
        private readonly ApplicationDbContext context;

        public PackingListDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetPackingList(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public int SavePackingListDetailQA(ENPackingListDetail o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingListDetailQA] @PpackingListDetailByVarietyID, " +
                        "@PqcOriginAprobe, @PqcOriginOutSpecs, @PqcOriginRejected, @PqcOriginQuantityBoxes, @PqcOriginAdditionalNotes, " +
                        "@PqcDestinationAprobe, @PqcDestinationOutSpecs, @PqcDestinationRejected, @PqcDestinationQuantityBoxes, @PqcDestinationAdditionalNotes, " +
                        "@PqcCustomerAprobe, @PqcCustomerOutSpecs, @PqcCustomerRejected, @PqcCustomerQuantityBoxes, @PqcCustomerAdditionalNotes, " +
                        "@PqcPotentialClaim, @PuserModifyID ";


                    DbParameter PpackingListDetailByVarietyID = command.CreateParameter();
                    PpackingListDetailByVarietyID.ParameterName = "@PpackingListDetailByVarietyID";
                    PpackingListDetailByVarietyID.Value = o.packingListDetailByVarietyID;
                    command.Parameters.Add(PpackingListDetailByVarietyID);

                    DbParameter PqcOriginAprobe = command.CreateParameter();
                    PqcOriginAprobe.ParameterName = "@PqcOriginAprobe";
                    PqcOriginAprobe.Value = o.qcOriginAprobe;
                    command.Parameters.Add(PqcOriginAprobe);

                    DbParameter PqcOriginOutSpecs = command.CreateParameter();
                    PqcOriginOutSpecs.ParameterName = "@PqcOriginOutSpecs";
                    PqcOriginOutSpecs.Value = o.qcOriginOutSpecs;
                    command.Parameters.Add(PqcOriginOutSpecs);

                    DbParameter PqcOriginRejected = command.CreateParameter();
                    PqcOriginRejected.ParameterName = "@PqcOriginRejected";
                    PqcOriginRejected.Value = o.qcOriginRejected;
                    command.Parameters.Add(PqcOriginRejected);

                    DbParameter PqcOriginQuantityBoxes = command.CreateParameter();
                    PqcOriginQuantityBoxes.ParameterName = "@PqcOriginQuantityBoxes";
                    PqcOriginQuantityBoxes.Value = o.qcOriginQuantityBoxes;
                    command.Parameters.Add(PqcOriginQuantityBoxes);

                    DbParameter PqcOriginAdditionalNotes = command.CreateParameter();
                    PqcOriginAdditionalNotes.ParameterName = "@PqcOriginAdditionalNotes";
                    PqcOriginAdditionalNotes.Value = o.qcOriginAdditionalNotes;
                    command.Parameters.Add(PqcOriginAdditionalNotes);

                    DbParameter PqcDestinationAprobe = command.CreateParameter();
                    PqcDestinationAprobe.ParameterName = "@PqcDestinationAprobe";
                    PqcDestinationAprobe.Value = o.qcDestinationAprobe;
                    command.Parameters.Add(PqcDestinationAprobe);

                    DbParameter PqcDestinationOutSpecs = command.CreateParameter();
                    PqcDestinationOutSpecs.ParameterName = "@PqcDestinationOutSpecs";
                    PqcDestinationOutSpecs.Value = o.qcDestinationOutSpecs;
                    command.Parameters.Add(PqcDestinationOutSpecs);

                    DbParameter PqcDestinationRejected = command.CreateParameter();
                    PqcDestinationRejected.ParameterName = "@PqcDestinationRejected";
                    PqcDestinationRejected.Value = o.qcDestinationRejected;
                    command.Parameters.Add(PqcDestinationRejected);

                    DbParameter PqcDestinationQuantityBoxes = command.CreateParameter();
                    PqcDestinationQuantityBoxes.ParameterName = "@PqcDestinationQuantityBoxes";
                    PqcDestinationQuantityBoxes.Value = o.qcDestinationQuantityBoxes;
                    command.Parameters.Add(PqcDestinationQuantityBoxes);

                    DbParameter PqcDestinationAdditionalNotes = command.CreateParameter();
                    PqcDestinationAdditionalNotes.ParameterName = "@PqcDestinationAdditionalNotes";
                    PqcDestinationAdditionalNotes.Value = o.qcDestinationAdditionalNotes;
                    command.Parameters.Add(PqcDestinationAdditionalNotes);

                    DbParameter PqcCustomerAprobe = command.CreateParameter();
                    PqcCustomerAprobe.ParameterName = "@PqcCustomerAprobe";
                    PqcCustomerAprobe.Value = o.qcCustomerAprobe;
                    command.Parameters.Add(PqcCustomerAprobe);

                    DbParameter PqcCustomerOutSpecs = command.CreateParameter();
                    PqcCustomerOutSpecs.ParameterName = "@PqcCustomerOutSpecs";
                    PqcCustomerOutSpecs.Value = o.qcCustomerOutSpecs;
                    command.Parameters.Add(PqcCustomerOutSpecs);

                    DbParameter PqcCustomerRejected = command.CreateParameter();
                    PqcCustomerRejected.ParameterName = "@PqcCustomerRejected";
                    PqcCustomerRejected.Value = o.qcCustomerRejected;
                    command.Parameters.Add(PqcCustomerRejected);

                    DbParameter PqcCustomerQuantityBoxes = command.CreateParameter();
                    PqcCustomerQuantityBoxes.ParameterName = "@PqcCustomerQuantityBoxes";
                    PqcCustomerQuantityBoxes.Value = o.qcCustomerQuantityBoxes;
                    command.Parameters.Add(PqcCustomerQuantityBoxes);

                    DbParameter PqcCustomerAdditionalNotes = command.CreateParameter();
                    PqcCustomerAdditionalNotes.ParameterName = "@PqcCustomerAdditionalNotes";
                    PqcCustomerAdditionalNotes.Value = o.qcCustomerAdditionalNotes;
                    command.Parameters.Add(PqcCustomerAdditionalNotes);

                    DbParameter PqcPotentialClaim = command.CreateParameter();
                    PqcPotentialClaim.ParameterName = "@PqcPotentialClaim";
                    PqcPotentialClaim.Value = o.qcPotentialClaim;
                    command.Parameters.Add(PqcPotentialClaim);

                    DbParameter PuserModifyID = command.CreateParameter();
                    PuserModifyID.ParameterName = "@PuserModifyID";
                    PuserModifyID.Value = o.userModifyID;
                    command.Parameters.Add(PuserModifyID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();

                    int id = 0;
                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }
                    command.Dispose();
                    result.Close();

                    //var dataTable = new DataTable();
                    //dataTable.Load(result);
                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
        public int SavePackingListQA(ENPackingList o)
        {
            o.ValueDates();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingListQA] @PpackingListID, " +
                        "@PqcOrigin, @PqcOriginInspectedBy, @PqcOriginInspectedDate, @PqcOriginReport, @PqcOriginNotes, " +
                        "@PqcDestination, @PqcDestinationInspectedBy, @PqcDestinationInspectedDate, @PqcDestinationReport, @PqcDestinationNotes, " +
                        "@PqcCustomer, @PqcCustomerInspectedBy, @PqcCustomerInspectedDate, @PqcCustomerReport, @PqcCustomerNotes, " +
                        "@PqcOriginInspectedBy2, @PqcOriginInspectedDate2, @PqcOriginReport2, @PqcOriginNotes2, " +
                        "@PqcOriginInspectedBy3, @PqcOriginInspectedDate3, @PqcOriginReport3, @PqcOriginNotes3, " +
                        "@PqcOriginInspectedBy4, @PqcOriginInspectedDate4, @PqcOriginReport4, @PqcOriginNotes4, " +
                        "@PuserModifyID ";

                    DbParameter PpackingListID = command.CreateParameter();
                    PpackingListID.ParameterName = "@PpackingListID";
                    PpackingListID.Value = o.packingListID;
                    command.Parameters.Add(PpackingListID);

                    DbParameter PqcOrigin = command.CreateParameter();
                    PqcOrigin.ParameterName = "@PqcOrigin";
                    PqcOrigin.Value = o.qcOrigin;
                    command.Parameters.Add(PqcOrigin);

                    DbParameter PqcOriginInspectedBy = command.CreateParameter();
                    PqcOriginInspectedBy.ParameterName = "@PqcOriginInspectedBy";
                    PqcOriginInspectedBy.Value = o.qcOriginInspectedBy;
                    command.Parameters.Add(PqcOriginInspectedBy);

                    DbParameter PqcOriginInspectedDate = command.CreateParameter();
                    PqcOriginInspectedDate.ParameterName = "@PqcOriginInspectedDate";
                    PqcOriginInspectedDate.Value = o.qcOriginInspectedDate;
                    command.Parameters.Add(PqcOriginInspectedDate);

                    DbParameter PqcOriginReport = command.CreateParameter();
                    PqcOriginReport.ParameterName = "@PqcOriginReport";
                    PqcOriginReport.Value = o.qcOriginReport;
                    command.Parameters.Add(PqcOriginReport);

                    DbParameter PqcOriginNotes = command.CreateParameter();
                    PqcOriginNotes.ParameterName = "@PqcOriginNotes";
                    PqcOriginNotes.Value = o.qcOriginNotes;
                    command.Parameters.Add(PqcOriginNotes);

                    DbParameter PqcDestination = command.CreateParameter();
                    PqcDestination.ParameterName = "@PqcDestination";
                    PqcDestination.Value = o.qcDestination;
                    command.Parameters.Add(PqcDestination);

                    DbParameter PqcDestinationInspectedBy = command.CreateParameter();
                    PqcDestinationInspectedBy.ParameterName = "@PqcDestinationInspectedBy";
                    PqcDestinationInspectedBy.Value = o.qcDestinationInspectedBy;
                    command.Parameters.Add(PqcDestinationInspectedBy);

                    DbParameter PqcDestinationInspectedDate = command.CreateParameter();
                    PqcDestinationInspectedDate.ParameterName = "@PqcDestinationInspectedDate";
                    PqcDestinationInspectedDate.Value = o.qcDestinationInspectedDate;
                    command.Parameters.Add(PqcDestinationInspectedDate);

                    DbParameter PqcDestinationReport = command.CreateParameter();
                    PqcDestinationReport.ParameterName = "@PqcDestinationReport";
                    PqcDestinationReport.Value = o.qcDestinationReport;
                    command.Parameters.Add(PqcDestinationReport);

                    DbParameter PqcDestinationNotes = command.CreateParameter();
                    PqcDestinationNotes.ParameterName = "@PqcDestinationNotes";
                    PqcDestinationNotes.Value = o.qcDestinationNotes;
                    command.Parameters.Add(PqcDestinationNotes);

                    DbParameter PqcCustomer = command.CreateParameter();
                    PqcCustomer.ParameterName = "@PqcCustomer";
                    PqcCustomer.Value = o.qcCustomer;
                    command.Parameters.Add(PqcCustomer);

                    DbParameter PqcCustomerInspectedBy = command.CreateParameter();
                    PqcCustomerInspectedBy.ParameterName = "@PqcCustomerInspectedBy";
                    PqcCustomerInspectedBy.Value = o.qcCustomerInspectedBy;
                    command.Parameters.Add(PqcCustomerInspectedBy);

                    DbParameter PqcCustomerInspectedDate = command.CreateParameter();
                    PqcCustomerInspectedDate.ParameterName = "@PqcCustomerInspectedDate";
                    PqcCustomerInspectedDate.Value = o.qcCustomerInspectedDate;
                    command.Parameters.Add(PqcCustomerInspectedDate);

                    DbParameter PqcCustomerReport = command.CreateParameter();
                    PqcCustomerReport.ParameterName = "@PqcCustomerReport";
                    PqcCustomerReport.Value = o.qcCustomerReport;
                    command.Parameters.Add(PqcCustomerReport);

                    DbParameter PqcCustomerNotes = command.CreateParameter();
                    PqcCustomerNotes.ParameterName = "@PqcCustomerNotes";
                    PqcCustomerNotes.Value = o.qcCustomerNotes;
                    command.Parameters.Add(PqcCustomerNotes);

                    DbParameter PqcOriginInspectedBy2 = command.CreateParameter();
                    PqcOriginInspectedBy2.ParameterName = "@PqcOriginInspectedBy2";
                    PqcOriginInspectedBy2.Value = o.qcOriginInspectedBy2;
                    command.Parameters.Add(PqcOriginInspectedBy2);

                    DbParameter PqcOriginInspectedDate2 = command.CreateParameter();
                    PqcOriginInspectedDate2.ParameterName = "@PqcOriginInspectedDate2";
                    PqcOriginInspectedDate2.Value = o.qcOriginInspectedDate2;
                    command.Parameters.Add(PqcOriginInspectedDate2);

                    DbParameter PqcOriginReport2 = command.CreateParameter();
                    PqcOriginReport2.ParameterName = "@PqcOriginReport2";
                    PqcOriginReport2.Value = o.qcOriginReport2;
                    command.Parameters.Add(PqcOriginReport2);

                    DbParameter PqcOriginNotes2 = command.CreateParameter();
                    PqcOriginNotes2.ParameterName = "@PqcOriginNotes2";
                    PqcOriginNotes2.Value = o.qcOriginNotes2;
                    command.Parameters.Add(PqcOriginNotes2);


                    DbParameter PqcOriginInspectedBy3 = command.CreateParameter();
                    PqcOriginInspectedBy3.ParameterName = "@PqcOriginInspectedBy3";
                    PqcOriginInspectedBy3.Value = o.qcOriginInspectedBy3;
                    command.Parameters.Add(PqcOriginInspectedBy3);

                    DbParameter PqcOriginInspectedDate3 = command.CreateParameter();
                    PqcOriginInspectedDate3.ParameterName = "@PqcOriginInspectedDate3";
                    PqcOriginInspectedDate3.Value = o.qcOriginInspectedDate3;
                    command.Parameters.Add(PqcOriginInspectedDate3);

                    DbParameter PqcOriginReport3 = command.CreateParameter();
                    PqcOriginReport3.ParameterName = "@PqcOriginReport3";
                    PqcOriginReport3.Value = o.qcOriginReport3;
                    command.Parameters.Add(PqcOriginReport3);

                    DbParameter PqcOriginNotes3 = command.CreateParameter();
                    PqcOriginNotes3.ParameterName = "@PqcOriginNotes3";
                    PqcOriginNotes3.Value = o.qcOriginNotes3;
                    command.Parameters.Add(PqcOriginNotes3);


                    DbParameter PqcOriginInspectedBy4 = command.CreateParameter();
                    PqcOriginInspectedBy4.ParameterName = "@PqcOriginInspectedBy4";
                    PqcOriginInspectedBy4.Value = o.qcOriginInspectedBy4;
                    command.Parameters.Add(PqcOriginInspectedBy4);

                    DbParameter PqcOriginInspectedDate4 = command.CreateParameter();
                    PqcOriginInspectedDate4.ParameterName = "@PqcOriginInspectedDate4";
                    PqcOriginInspectedDate4.Value = o.qcOriginInspectedDate4;
                    command.Parameters.Add(PqcOriginInspectedDate4);

                    DbParameter PqcOriginReport4 = command.CreateParameter();
                    PqcOriginReport4.ParameterName = "@PqcOriginReport4";
                    PqcOriginReport4.Value = o.qcOriginReport4;
                    command.Parameters.Add(PqcOriginReport4);

                    DbParameter PqcOriginNotes4 = command.CreateParameter();
                    PqcOriginNotes4.ParameterName = "@PqcOriginNotes4";
                    PqcOriginNotes4.Value = o.qcOriginNotes4;
                    command.Parameters.Add(PqcOriginNotes4);


                    DbParameter PuserModifyID = command.CreateParameter();
                    PuserModifyID.ParameterName = "@PuserModifyID";
                    PuserModifyID.Value = o.userModifyID;
                    command.Parameters.Add(PuserModifyID);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    int id = 0;


                    while (result.Read())
                    {
                        id = result.GetInt32(0);
                    }

                    command.Dispose();
                    result.Close();

                    return (id);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
        public DataTable GetForQA(string opt, int id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packigListQAList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public bool SaveMasivePackingList(ENSispacking objENSispacking)
        {


            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_packingListServiceCreateUpdate]";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter PshipmentDetail = new SqlParameter("@PshipmentDetail", SqlDbType.Structured);
                    command.Parameters.Add(PshipmentDetail);

                    SqlParameter PcampaignID = new SqlParameter("@PcampaignID", SqlDbType.Char);
                    PcampaignID.Value = objENSispacking.Campana;
                    command.Parameters.Add(PcampaignID);

                    SqlParameter PnroPackingList = new SqlParameter("@PnroPackingList", SqlDbType.VarChar);
                    PnroPackingList.Value = objENSispacking.NroPackingList;
                    command.Parameters.Add(PnroPackingList);

                    SqlParameter Psenasa = new SqlParameter("@Psenasa", SqlDbType.VarChar);
                    Psenasa.Value = objENSispacking.SenasaSeal;
                    command.Parameters.Add(Psenasa);

                    SqlParameter Pcustom = new SqlParameter("@Pcustom", SqlDbType.VarChar);
                    Pcustom.Value = objENSispacking.CustomSeal;
                    command.Parameters.Add(Pcustom);

                    SqlParameter Pthermo = new SqlParameter("@Pthermo", SqlDbType.VarChar);
                    Pthermo.Value = objENSispacking.Thermographs;
                    command.Parameters.Add(Pthermo);

                    SqlParameter PthermoLocation = new SqlParameter("@PthermoLocation", SqlDbType.VarChar);
                    PthermoLocation.Value = objENSispacking.ThermographsLocation;
                    command.Parameters.Add(PthermoLocation);

                    SqlParameter Psensors = new SqlParameter("@Psensors", SqlDbType.VarChar);
                    Psensors.Value = objENSispacking.Sensors;
                    command.Parameters.Add(Psensors);

                    SqlParameter PsensorLocation = new SqlParameter("@PsensorLocation", SqlDbType.VarChar);
                    PsensorLocation.Value = objENSispacking.SensorLocation;
                    command.Parameters.Add(PsensorLocation);

                    SqlParameter PtotalBoxes = new SqlParameter("@PtotalBoxes", SqlDbType.Int);
                    PtotalBoxes.Value = Int32.Parse(objENSispacking.TotalBoxes);
                    command.Parameters.Add(PtotalBoxes);

                    SqlParameter PpackingLoadingDate = new SqlParameter("@PpackingLoadingDate", SqlDbType.DateTime);

                    DateTime date = DateTime.ParseExact(objENSispacking.PackingLoadingDate.Substring(0, 10), "dd/MM/yyyy", null);

                    PpackingLoadingDate.Value = date;
                    command.Parameters.Add(PpackingLoadingDate);

                    SqlParameter Pcontainer = new SqlParameter("@Pcontainer", SqlDbType.VarChar);
                    Pcontainer.Value = objENSispacking.Container;
                    command.Parameters.Add(Pcontainer);

                    SqlParameter PshipmentID = new SqlParameter("@PshipmentID", SqlDbType.VarChar);
                    PshipmentID.Value = objENSispacking.ShipmentID;
                    command.Parameters.Add(PshipmentID);

                    DataTable dttShipmentDetails = new DataTable("ShipmentDetails");

                    DataColumn dc = new DataColumn();
                    dc.ColumnName = "Pallet";
                    dc.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc);

                    DataColumn dc1 = new DataColumn();
                    dc1.ColumnName = "Variety";
                    dc1.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc1);

                    DataColumn dc2 = new DataColumn();
                    dc2.ColumnName = "CodePack";
                    dc2.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc2);

                    DataColumn dc3 = new DataColumn();
                    dc3.ColumnName = "Weight";
                    dc3.DataType = typeof(decimal);
                    dttShipmentDetails.Columns.Add(dc3);

                    DataColumn dc4 = new DataColumn();
                    dc4.ColumnName = "Label";
                    dc4.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc4);

                    DataColumn dc5 = new DataColumn();
                    dc5.ColumnName = "Pack";
                    dc5.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc5);

                    DataColumn dc6 = new DataColumn();
                    dc6.ColumnName = "Size";
                    dc6.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc6);

                    DataColumn dc7 = new DataColumn();
                    dc7.ColumnName = "Color";
                    dc7.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc7);

                    DataColumn dc8 = new DataColumn();
                    dc8.ColumnName = "Cat";
                    dc8.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc8);

                    DataColumn dc9 = new DataColumn();
                    dc9.ColumnName = "Planta";
                    dc9.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc9);

                    DataColumn dc10 = new DataColumn();
                    dc10.ColumnName = "Package";
                    dc10.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc10);

                    DataColumn dc11 = new DataColumn();
                    dc11.ColumnName = "Brand";
                    dc11.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc11);

                    DataColumn dc12 = new DataColumn();
                    dc12.ColumnName = "Presentation";
                    dc12.DataType = typeof(string);
                    dttShipmentDetails.Columns.Add(dc12);

                    DataColumn dc13 = new DataColumn();
                    dc13.ColumnName = "PackingDate";
                    dc13.DataType = typeof(DateTime);
                    dttShipmentDetails.Columns.Add(dc13);

                    DataColumn dc14 = new DataColumn();
                    dc14.ColumnName = "NumberBoxes";
                    dc14.DataType = typeof(int);
                    dttShipmentDetails.Columns.Add(dc14);

                    foreach (ShipmentDetails o in objENSispacking.ShipmentDetails)
                    {
                        string fecha = o.PackingDate.Substring(0, 10) + " " + o.PackingDate.Substring(11, 8);

                        dttShipmentDetails.Rows.Add(new object[] {

                        o.Pallet,
                        o.Variety,
                        o.CodePack,
                        decimal.Parse(o.Weight),
                        o.Label,
                        o.Pack,
                        o.Size,
                        o.Color,
                        o.Cat,
                        o.Planta,
                        o.Package,
                        o.Brand,
                        o.Presentation,
                        DateTime.Parse(fecha),
                        int.Parse(o.NumberBoxes)

                    });
                    }

                    PshipmentDetail.Value = dttShipmentDetails;

                    context.Database.OpenConnection();
                    var result = command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    //if (result != null && !result.IsClosed) result.Close();
                    if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }

        }

        public DataTable PackingListSalesOrdersInformation(string token, string codeAuthorized)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[packing].[sp_PackingList_ListSalesOrdersInformation]";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter PToken = new SqlParameter("@PToken", SqlDbType.VarChar, 8000);
                    PToken.Value = token;
                    command.Parameters.Add(PToken);

                    SqlParameter PcodeAuthorized = new SqlParameter("@PcodeAuthorized", SqlDbType.Char);
                    PcodeAuthorized.Value = codeAuthorized;
                    command.Parameters.Add(PcodeAuthorized);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable ListSalesOrdersInformationSeparateShippingInstruction(string token, string codeAuthorized)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[packing].[sp_PackingList_ListSalesOrdersInformationSeparateShippingInstruction]";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter PToken = new SqlParameter("@PToken", SqlDbType.VarChar, 8000);
                    PToken.Value = token;
                    command.Parameters.Add(PToken);

                    SqlParameter PcodeAuthorized = new SqlParameter("@PcodeAuthorized", SqlDbType.Char);
                    PcodeAuthorized.Value = codeAuthorized;
                    command.Parameters.Add(PcodeAuthorized);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable ListShippingLoadingDetail(int shippingLoadingID, string processPlantID,
            string token, string codeAuthorized)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_PackingList_ListShippingLoadingDetail]";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter PshippingLoadingID = new SqlParameter("@PshippingLoadingID", SqlDbType.Int);
                    PshippingLoadingID.Value = shippingLoadingID;
                    command.Parameters.Add(PshippingLoadingID);

                    SqlParameter PprocessPlantID = new SqlParameter("@PprocessPlantID", SqlDbType.Char,3);
                    PprocessPlantID.Value = processPlantID;
                    command.Parameters.Add(PprocessPlantID);

                    SqlParameter PToken = new SqlParameter("@PToken", SqlDbType.VarChar, 8000);
                    PToken.Value = token;
                    command.Parameters.Add(PToken);

                    SqlParameter PcodeAuthorized = new SqlParameter("@PcodeAuthorized", SqlDbType.Char);
                    PcodeAuthorized.Value = codeAuthorized;
                    command.Parameters.Add(PcodeAuthorized);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable ListShippingInstructionDetail(string nroPackingList,
            string token, string codeAuthorized)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_PackingList_ListShippingInstructionDetail]";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter PnroPackingList = new SqlParameter("@PnroPackingList", SqlDbType.VarChar,50);
                    PnroPackingList.Value = nroPackingList;
                    command.Parameters.Add(PnroPackingList);

                    SqlParameter PToken = new SqlParameter("@PToken", SqlDbType.VarChar, 8000);
                    PToken.Value = token;
                    command.Parameters.Add(PToken);

                    SqlParameter PcodeAuthorized = new SqlParameter("@PcodeAuthorized", SqlDbType.Char);
                    PcodeAuthorized.Value = codeAuthorized;
                    command.Parameters.Add(PcodeAuthorized);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public int InsertToken(int UserID, string Token)
        {

            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [packing].[sp_Security_InsertRequestAuthorization] @PuserID, @Ptoken";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = UserID;
                    command.Parameters.Add(PuserID);

                    DbParameter Ptoken = command.CreateParameter();
                    Ptoken.ParameterName = "@Ptoken";
                    Ptoken.Value = Token;
                    command.Parameters.Add(Ptoken);

                    context.Database.OpenConnection();
                    var result = command.ExecuteNonQuery();
                    return result;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    //if (result != null && !result.IsClosed) result.Close();
                    if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}