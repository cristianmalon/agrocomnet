﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;using System.Net;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace AgrocomApi.Models
{
    public class PackingListPODAO
    {
        private readonly ApplicationDbContext context;

        public PackingListPODAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable packingSave(int packingListID, int orderProductionID, string nroPackingList, string growerID, string invoiceGrower, string nroGuide, int customerID, string container, string senasaSeal, string customSeal, string thermogRegisters, string thermogRegistersLocation, string sensors, string sensorsLocation, string packingLoadDate, string booking, string originPort, string destinationPort, string shippingLine, string vessel, string BLAWB, string ETD, string ETA, int viaID, int totalBoxes)
        {
            thermogRegisters = thermogRegisters ?? "";
            thermogRegistersLocation = thermogRegistersLocation ?? "";
            sensors = sensors ?? "";
            sensorsLocation = sensorsLocation ?? "";
            BLAWB = BLAWB ?? "";
            nroPackingList = nroPackingList ?? "";
            invoiceGrower = invoiceGrower ?? "";
            nroGuide = nroGuide ?? "";
            container = container ?? "";
            senasaSeal = senasaSeal ?? "";
            booking = booking ?? "";
            originPort = originPort ?? "";
            destinationPort = destinationPort ?? "";
            shippingLine = shippingLine ?? "";
            vessel = vessel ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingListCreateUpdate] @PpackingListID,@PorderProductionID,@PnroPackingList,@PgrowerID,@PinvoiceGrower,@PnroGuide,@PcustomerID,@Pcontainer,@PsenasaSeal,@PcustomSeal,@PthermogRegisters,@PthermogRegistersLocation,@Psensors,@PsensorsLocation,@PpackingLoadDate,@Pbooking,@PoriginPort,@PdestinationPort,@PshippingLine,@Pvessel,@PBLAWB,@PETD,@PETA,@PviaID,@PtotalBoxes";

                    DbParameter PpackingListID = command.CreateParameter();
                    PpackingListID.ParameterName = "@PpackingListID";
                    PpackingListID.Value = packingListID;
                    command.Parameters.Add(PpackingListID);

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PnroPackingList = command.CreateParameter();
                    PnroPackingList.ParameterName = "@PnroPackingList";
                    PnroPackingList.Value = nroPackingList;
                    command.Parameters.Add(PnroPackingList);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PinvoiceGrower = command.CreateParameter();
                    PinvoiceGrower.ParameterName = "@PinvoiceGrower";
                    PinvoiceGrower.Value = invoiceGrower;
                    command.Parameters.Add(PinvoiceGrower);

                    DbParameter PnroGuide = command.CreateParameter();
                    PnroGuide.ParameterName = "@PnroGuide";
                    PnroGuide.Value = nroGuide;
                    command.Parameters.Add(PnroGuide);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter Pcontainer = command.CreateParameter();
                    Pcontainer.ParameterName = "@Pcontainer";
                    Pcontainer.Value = container;
                    command.Parameters.Add(Pcontainer);

                    DbParameter PsenasaSeal = command.CreateParameter();
                    PsenasaSeal.ParameterName = "@PsenasaSeal";
                    PsenasaSeal.Value = senasaSeal;
                    command.Parameters.Add(PsenasaSeal);

                    DbParameter PcustomSeal = command.CreateParameter();
                    PcustomSeal.ParameterName = "@PcustomSeal";
                    PcustomSeal.Value = customSeal;
                    command.Parameters.Add(PcustomSeal);

                    DbParameter PthermogRegisters = command.CreateParameter();
                    PthermogRegisters.ParameterName = "@PthermogRegisters";
                    PthermogRegisters.Value = thermogRegisters;
                    command.Parameters.Add(PthermogRegisters);

                    DbParameter PthermogRegistersLocation = command.CreateParameter();
                    PthermogRegistersLocation.ParameterName = "@PthermogRegistersLocation";
                    PthermogRegistersLocation.Value = thermogRegistersLocation;
                    command.Parameters.Add(PthermogRegistersLocation);

                    DbParameter Psensors = command.CreateParameter();
                    Psensors.ParameterName = "@Psensors";
                    Psensors.Value = sensors;
                    command.Parameters.Add(Psensors);

                    DbParameter PsensorsLocation = command.CreateParameter();
                    PsensorsLocation.ParameterName = "@PsensorsLocation";
                    PsensorsLocation.Value = sensorsLocation;
                    command.Parameters.Add(PsensorsLocation);

                    DbParameter PpackingLoadDate = command.CreateParameter();
                    PpackingLoadDate.ParameterName = "@PpackingLoadDate";
                    PpackingLoadDate.Value = packingLoadDate;
                    command.Parameters.Add(PpackingLoadDate);

                    DbParameter Pbooking = command.CreateParameter();
                    Pbooking.ParameterName = "@Pbooking";
                    Pbooking.Value = booking;
                    command.Parameters.Add(Pbooking);

                    DbParameter PoriginPort = command.CreateParameter();
                    PoriginPort.ParameterName = "@PoriginPort";
                    PoriginPort.Value = originPort;
                    command.Parameters.Add(PoriginPort);

                    DbParameter PdestinationPort = command.CreateParameter();
                    PdestinationPort.ParameterName = "@PdestinationPort";
                    PdestinationPort.Value = destinationPort;
                    command.Parameters.Add(PdestinationPort);

                    DbParameter PshippingLine = command.CreateParameter();
                    PshippingLine.ParameterName = "@PshippingLine";
                    PshippingLine.Value = shippingLine;
                    command.Parameters.Add(PshippingLine);

                    DbParameter Pvessel = command.CreateParameter();
                    Pvessel.ParameterName = "@Pvessel";
                    Pvessel.Value = vessel;
                    command.Parameters.Add(Pvessel);

                    DbParameter PBLAWB = command.CreateParameter();
                    PBLAWB.ParameterName = "@PBLAWB";
                    PBLAWB.Value = BLAWB;
                    command.Parameters.Add(PBLAWB);

                    DbParameter PETD = command.CreateParameter();
                    PETD.ParameterName = "@PETD";
                    PETD.Value = ETD;
                    command.Parameters.Add(PETD);

                    DbParameter PETA = command.CreateParameter();
                    PETA.ParameterName = "@PETA";
                    PETA.Value = ETA;
                    command.Parameters.Add(PETA);

                    DbParameter PviaID = command.CreateParameter();
                    PviaID.ParameterName = "@PviaID";
                    PviaID.Value = viaID;
                    command.Parameters.Add(PviaID);

                    DbParameter PtotalBoxes = command.CreateParameter();
                    PtotalBoxes.ParameterName = "@PtotalBoxes";
                    PtotalBoxes.Value = totalBoxes;
                    command.Parameters.Add(PtotalBoxes);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable packingSaveDetail(int packingListDetailID, int packingListID, string numberPallet, int orderPallet, string farmID, int codePackID, int packageProductID, string categoryID, int varietyID, string sizeID, string packingDate, int quantityBoxes, string description, int packingListDetailByVarietyID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingListDetailCreateUpdate] @PpackingListDetailID,@PpackingListID,@PnumberPallet,@PorderPallet,@PfarmID,@PcodePackID,@PpackageProductID,@PcategoryID,@PvarietyID,@PsizeID,@PpackingDate,@PquantityBoxes,@Pdescription,@PpackingListDetailByVarietyID";

                    DbParameter PpackingListDetailID = command.CreateParameter();
                    PpackingListDetailID.ParameterName = "@PpackingListDetailID";
                    PpackingListDetailID.Value = packingListDetailID;
                    command.Parameters.Add(PpackingListDetailID);

                    DbParameter PpackingListID = command.CreateParameter();
                    PpackingListID.ParameterName = "@PpackingListID";
                    PpackingListID.Value = packingListID;
                    command.Parameters.Add(PpackingListID);

                    DbParameter PnumberPallet = command.CreateParameter();
                    PnumberPallet.ParameterName = "@PnumberPallet";
                    PnumberPallet.Value = numberPallet;
                    command.Parameters.Add(PnumberPallet);

                    DbParameter PorderPallet = command.CreateParameter();
                    PorderPallet.ParameterName = "@PorderPallet";
                    PorderPallet.Value = orderPallet;
                    command.Parameters.Add(PorderPallet);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@PfarmID";
                    PfarmID.Value = farmID;
                    command.Parameters.Add(PfarmID);

                    DbParameter PcodePackID = command.CreateParameter();
                    PcodePackID.ParameterName = "@PcodePackID";
                    PcodePackID.Value = codePackID;
                    command.Parameters.Add(PcodePackID);

                    DbParameter PpackageProductID = command.CreateParameter();
                    PpackageProductID.ParameterName = "@PpackageProductID";
                    PpackageProductID.Value = packageProductID;
                    command.Parameters.Add(PpackageProductID);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = categoryID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PvarietyID = command.CreateParameter();
                    PvarietyID.ParameterName = "@PvarietyID";
                    PvarietyID.Value = varietyID;
                    command.Parameters.Add(PvarietyID);

                    DbParameter PsizeID = command.CreateParameter();
                    PsizeID.ParameterName = "@PsizeID";
                    PsizeID.Value = sizeID;
                    command.Parameters.Add(PsizeID);

                    DbParameter PpackingDate = command.CreateParameter();
                    PpackingDate.ParameterName = "@PpackingDate";
                    PpackingDate.Value = packingDate;
                    command.Parameters.Add(PpackingDate);

                    DbParameter PquantityBoxes = command.CreateParameter();
                    PquantityBoxes.ParameterName = "@PquantityBoxes";
                    PquantityBoxes.Value = quantityBoxes;
                    command.Parameters.Add(PquantityBoxes);

                    DbParameter Pdescription = command.CreateParameter();
                    Pdescription.ParameterName = "@Pdescription";
                    Pdescription.Value = description;
                    command.Parameters.Add(Pdescription);

                    DbParameter PpackingListDetailByVarietyID = command.CreateParameter();
                    PpackingListDetailByVarietyID.ParameterName = "@PpackingListDetailByVarietyID";
                    PpackingListDetailByVarietyID.Value = packingListDetailByVarietyID;
                    command.Parameters.Add(PpackingListDetailByVarietyID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPackingPO(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingListPOList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPackingMissing()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingWithMissingDataList] ";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetsalesAllocated(string nroPackingList)
        {
            DbDataReader result = null;
            try
            {
                using (SqlConnection conn = new SqlConnection("Server=tcp:migivaazure.database.windows.net,1433;Initial Catalog=BD_DEV_COMERCIAL;Persist Security Info=False;User ID=sistemas;Password=+-_admin666;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=3600;"))
                {
                    DataSet dataset = new DataSet();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    SqlCommand command = new SqlCommand("package.sp_salesAlocatedCreate", conn);
                    command.Parameters.AddWithValue("@PnroPackingList", nroPackingList.Replace("%20", " ").Replace("%2F", "/"));

                    adapter.SelectCommand = command;
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapter.Fill(dataset);
                    return dataset.Tables[10];
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetDataUpdate(string opt, int id, string data1, string data2, string data3, string data4)
        {
            data1 = data1 ?? "";
            data2 = data2 ?? "";
            data3 = data3 ?? "";
            data4 = data4 ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingDataUpdate] @PsearchOpt, @Pid, @Pdata1, @Pdata2, @Pdata3, @Pdata4";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter Pid = command.CreateParameter();
                    Pid.ParameterName = "@Pid";
                    Pid.Value = id;
                    command.Parameters.Add(Pid);

                    DbParameter Pdata1 = command.CreateParameter();
                    Pdata1.ParameterName = "@Pdata1";
                    Pdata1.Value = data1;
                    command.Parameters.Add(Pid);

                    DbParameter Pdata2 = command.CreateParameter();
                    Pdata2.ParameterName = "@Pdata2";
                    Pdata2.Value = data2;
                    command.Parameters.Add(Pdata2);

                    DbParameter Pdata3 = command.CreateParameter();
                    Pdata3.ParameterName = "@Pdata3";
                    Pdata3.Value = data3;
                    command.Parameters.Add(Pdata3);

                    DbParameter Pdata4 = command.CreateParameter();
                    Pdata4.ParameterName = "@Pdata4";
                    Pdata4.Value = data4;
                    command.Parameters.Add(Pdata4);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}
