﻿using AgrocomApi.Context;
using Common.CommercialPlan;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using System.Net;

namespace AgrocomApi.Models
{
    public class PlanCommerceDAO
    {
        private readonly ApplicationDbContext context;

        public PlanCommerceDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetPlan(string opt, string id, string cropId, string seasonId, string originId, string growerId)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planList] @PsearchOpt, @PsearchValue,@PcropId,@PcampaingId,@PoriginId,@PgrowerId";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PcropId = command.CreateParameter();
                    PcropId.ParameterName = "@PcropId";
                    PcropId.Value = cropId;
                    command.Parameters.Add(PcropId);

                    DbParameter PcampaingId = command.CreateParameter();
                    PcampaingId.ParameterName = "@PcampaingId";
                    PcampaingId.Value = seasonId;
                    command.Parameters.Add(PcampaingId);

                    DbParameter PoriginId = command.CreateParameter();
                    PoriginId.ParameterName = "@PoriginId";
                    PoriginId.Value = originId;
                    command.Parameters.Add(PoriginId);

                    DbParameter PgrowerId = command.CreateParameter();
                    PgrowerId.ParameterName = "@PgrowerId";
                    PgrowerId.Value = growerId;
                    command.Parameters.Add(PgrowerId);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable SavePlan(string planID, string plan, string campaignID, string userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCreate] @PplanID, @Pplan,@PcampaignID ,@PuserID";

                    DbParameter PplanID = command.CreateParameter();
                    PplanID.ParameterName = "@PplanID";
                    PplanID.Value = planID;
                    command.Parameters.Add(PplanID);

                    DbParameter Pplan = command.CreateParameter();
                    Pplan.ParameterName = "@Pplan";
                    Pplan.Value = plan;
                    command.Parameters.Add(Pplan);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SavePlanOriginGrower(string planID, string plan, string campaignID, string originID, string growerID, string nonExport, string userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planOriginGrowerBulkCreate] @PplanID, @Pplan,@PcampaignID ,@POriginID,@PgrowerID,@PnonExport, @PuserID";
                    
                    DbParameter PplanID = command.CreateParameter();
                    PplanID.ParameterName = "@PplanID";
                    PplanID.Value = planID;
                    command.Parameters.Add(PplanID);

                    DbParameter Pplan = command.CreateParameter();
                    Pplan.ParameterName = "@Pplan";
                    Pplan.Value = plan;
                    command.Parameters.Add(Pplan);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter POriginID = command.CreateParameter();
                    POriginID.ParameterName = "@POriginID";
                    POriginID.Value = originID;
                    command.Parameters.Add(POriginID);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PnonExport = command.CreateParameter();
                    PnonExport.ParameterName = "@PnonExport";
                    PnonExport.Value = nonExport;
                    command.Parameters.Add(PnonExport);


                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SavePlanCustomerRequest(string planCustomerRequestID, string planCustomerRequest, string planID, string customerID, string destinationID, string codepackID, string marketID, string userID,
            string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomerRequestCreate] @PplanCustomerRequestID,@PplanCustomerRequest,@PplanID,@PcustomerID,@PdestinationID,@PcodepackID,@PmarketID,@PuserID,@PcropID";

                    DbParameter PplanCustomerRequestID = command.CreateParameter();
                    PplanCustomerRequestID.ParameterName = "@PplanCustomerRequestID";
                    PplanCustomerRequestID.Value = planCustomerRequestID;
                    command.Parameters.Add(PplanCustomerRequestID);

                    DbParameter PplanCustomerRequest = command.CreateParameter();
                    PplanCustomerRequest.ParameterName = "@PplanCustomerRequest";
                    PplanCustomerRequest.Value = planCustomerRequest;
                    command.Parameters.Add(PplanCustomerRequest);

                    DbParameter PplanID = command.CreateParameter();
                    PplanID.ParameterName = "@PplanID";
                    PplanID.Value = planID;
                    command.Parameters.Add(PplanID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PcodepackID = command.CreateParameter();
                    PcodepackID.ParameterName = "@PcodepackID";
                    PcodepackID.Value = codepackID;
                    command.Parameters.Add(PcodepackID);

                    DbParameter @PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = userID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SavePlanCustomerRequestWeek(string planCustomerRequestWeekID, string planCustomerRequestID, string projectedWeekID, string amount, string userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomerRequestWeekCreate] @PplanCustomerRequestWeekID, @PplanCustomerRequestID,@PprojectedWeekID,@Pamount,@PuserID";

                    DbParameter PplanCustomerRequestWeekID = command.CreateParameter();
                    PplanCustomerRequestWeekID.ParameterName = "@PplanCustomerRequestWeekID";
                    PplanCustomerRequestWeekID.Value = planCustomerRequestWeekID;
                    command.Parameters.Add(PplanCustomerRequestWeekID);

                    DbParameter PplanCustomerRequestID = command.CreateParameter();
                    PplanCustomerRequestID.ParameterName = "@PplanCustomerRequestID";
                    PplanCustomerRequestID.Value = planCustomerRequestID;
                    command.Parameters.Add(PplanCustomerRequestID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter Pamount = command.CreateParameter();
                    Pamount.ParameterName = "@Pamount";
                    Pamount.Value = amount;
                    command.Parameters.Add(Pamount);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListCustomerRequest(string searchOpt, string planID, string marketID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerRequest] @PsearchOpt, @PplanID,@PmarketID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PplanID = command.CreateParameter();
                    PplanID.ParameterName = "@PplanID";
                    PplanID.Value = planID;
                    command.Parameters.Add(PplanID);

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable ListCustomerRequestWeek(string searchOpt, int planCustomerRequestID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerRequestWeekList] @PsearchOpt, @PplanCustomerRequestID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PplanCustomerRequestID = command.CreateParameter();
                    PplanCustomerRequestID.ParameterName = "@PplanCustomerRequestID";
                    PplanCustomerRequestID.Value = planCustomerRequestID;
                    command.Parameters.Add(PplanCustomerRequestID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SavePlanAlternative(string planAlternativeID, string planAlternative, string planOriginID, string userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planAlternativeCreate] @PplanAlternativeID, @PplanAlternative,@PplanOriginID,@PuserID";

                    DbParameter PplanAlternativeID = command.CreateParameter();
                    PplanAlternativeID.ParameterName = "@PplanAlternativeID";
                    PplanAlternativeID.Value = planAlternativeID;
                    command.Parameters.Add(PplanAlternativeID);

                    DbParameter PplanAlternative = command.CreateParameter();
                    PplanAlternative.ParameterName = "@PplanAlternative";
                    PplanAlternative.Value = planAlternative;
                    command.Parameters.Add(PplanAlternative);

                    DbParameter PplanOriginID = command.CreateParameter();
                    PplanOriginID.ParameterName = "@PplanOriginID";
                    PplanOriginID.Value = planOriginID;
                    command.Parameters.Add(PplanOriginID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListPlanAlternative(string searchOpt, int planOriginID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planAlternativeList] @PsearchOpt, @PplanOriginID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PplanOriginID = command.CreateParameter();
                    PplanOriginID.ParameterName = "@PplanOriginID";
                    PplanOriginID.Value = planOriginID;
                    command.Parameters.Add(PplanOriginID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveCustomerFormat(string codepackID, string alternativeID, string marketID, int destinationID, int customerID, int priorityID, int wildcard, int order, string userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomerFormatCreate] @PcodepackID, @PalternativeID,@PmarketID,@PdestinationID,@PcustomerID,@PpriorityID,@Pwildcard,@Porder,@PuserID";

                    DbParameter PcodepackID = command.CreateParameter();
                    PcodepackID.ParameterName = "@PcodepackID";
                    PcodepackID.Value = codepackID;
                    command.Parameters.Add(PcodepackID);

                    DbParameter PalternativeID = command.CreateParameter();
                    PalternativeID.ParameterName = "@PalternativeID";
                    PalternativeID.Value = alternativeID;
                    command.Parameters.Add(PalternativeID);

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);
                    
                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PpriorityID = command.CreateParameter();
                    PpriorityID.ParameterName = "@PpriorityID";
                    PpriorityID.Value = priorityID;
                    command.Parameters.Add(PpriorityID);

                    DbParameter Pwildcard = command.CreateParameter();
                    Pwildcard.ParameterName = "@Pwildcard";
                    Pwildcard.Value = wildcard;
                    command.Parameters.Add(Pwildcard);

                    DbParameter Porder = command.CreateParameter();
                    Porder.ParameterName = "@Porder";
                    Porder.Value = order;
                    command.Parameters.Add(Porder);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable SavePlanCustomerWeek(string planCustomerWeekID, string planCustomerFormatID, string projectedWeekID, string amount, string userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomerWeekCreate] @PplanCustomerWeekID, @PplanCustomerFormatID,@PprojectedWeekID,@Pamount,@PuserID";

                    DbParameter PplanCustomerWeekID = command.CreateParameter();
                    @PplanCustomerWeekID.ParameterName = "@PplanCustomerWeekID";
                    @PplanCustomerWeekID.Value = planCustomerWeekID;
                    command.Parameters.Add(PplanCustomerWeekID);

                    DbParameter PplanCustomerFormatID = command.CreateParameter();
                    PplanCustomerFormatID.ParameterName = "@PplanCustomerFormatID";
                    PplanCustomerFormatID.Value = planCustomerFormatID;
                    command.Parameters.Add(PplanCustomerFormatID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter Pamount = command.CreateParameter();
                    Pamount.ParameterName = "@Pamount";
                    Pamount.Value = amount;
                    command.Parameters.Add(Pamount);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListPlanCustomer(string searchOpt, int planAlternativeID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomer] @PsearchOpt, @PplanAlternativeID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PplanAlternativeID = command.CreateParameter();
                    PplanAlternativeID.ParameterName = "@PplanAlternativeID";
                    PplanAlternativeID.Value = planAlternativeID;
                    command.Parameters.Add(PplanAlternativeID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListPlanCustomerWeek(string searchOpt, int planCustomerFormatID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomerWeekList] @PsearchOpt, @PplanCustomerFormatID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PplanCustomerFormatID = command.CreateParameter();
                    PplanCustomerFormatID.ParameterName = "@PplanCustomerFormatID";
                    PplanCustomerFormatID.Value = planCustomerFormatID;
                    command.Parameters.Add(PplanCustomerFormatID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable CustomerRequestDelete(int planCustomerRequestID, int userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_customerRequestDelete] @PplanCustomerRequestID, @PuserID";

                    DbParameter PplanCustomerRequestID = command.CreateParameter();
                    PplanCustomerRequestID.ParameterName = "@PplanCustomerRequestID";
                    PplanCustomerRequestID.Value = planCustomerRequestID;
                    command.Parameters.Add(PplanCustomerRequestID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable PlanCustomerDelete(int planCustomerFormatID, int userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_planCustomerDelete] @PplanCustomerFormatID, @PuserID";

                    DbParameter PplanCustomerFormatID = command.CreateParameter();
                    PplanCustomerFormatID.ParameterName = "@PplanCustomerFormatID";
                    PplanCustomerFormatID.Value = planCustomerFormatID;
                    command.Parameters.Add(PplanCustomerFormatID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetPlanCustomerVariety(int campaignID, string opt, int projectedWeekID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[planCustomerVarietyList] @PcampaignID,@PsearchOpt,@PweekID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PweekID = command.CreateParameter();
                    PweekID.ParameterName = "@PweekID";
                    PweekID.Value = projectedWeekID;
                    command.Parameters.Add(PweekID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }


        public DataTable PlanCustomerVariety(ENPlanCustomerVariety o)
        {
            o.category = o.category ?? string.Empty;
            o.marketID = o.marketID ?? string.Empty;
            o.sizeID = o.sizeID ?? string.Empty;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec package.planCustomerVariety @planCustomerVarietyID,@campaignID,@customerID,@programID," +
                                            "@destinationID,@kamID,@priorityID,@statusConfirmID," +
                                            "@sort,@varietyID,@categoryID,@category," +
                                            "@formatID,@packageProductID,@brandID,@presentationID," +
                                            "@labelID,@sizeID,@codepack,@codepackID,@incotermID," +
                                            "@conditionPaymentID,@paymentTermID,@userID,@statusID," +
                                            "@padlock, @plu, @pviaID, @pconsigneeID, @pmarketID, @planBoxWeek";


                    DbParameter planCustomerVarietyID = command.CreateParameter();
                    planCustomerVarietyID.ParameterName = "@planCustomerVarietyID";
                    planCustomerVarietyID.Value = o.planCustomerVarietyID;
                    command.Parameters.Add(planCustomerVarietyID);

                    DbParameter campaignID = command.CreateParameter();
                    campaignID.ParameterName = "@campaignID";
                    campaignID.Value = o.campaignID;
                    command.Parameters.Add(campaignID);

                    DbParameter customerID = command.CreateParameter();
                    customerID.ParameterName = "@customerID";
                    customerID.Value = o.customerID;
                    command.Parameters.Add(customerID);

                    DbParameter programID = command.CreateParameter();
                    programID.ParameterName = "@programID";
                    programID.Value = o.programID;
                    command.Parameters.Add(programID);

                    DbParameter destinationID = command.CreateParameter();
                    destinationID.ParameterName = "@destinationID";
                    destinationID.Value = o.destinationID;
                    command.Parameters.Add(destinationID);

                    DbParameter kamID = command.CreateParameter();
                    kamID.ParameterName = "@kamID";
                    kamID.Value = o.kamID;
                    command.Parameters.Add(kamID);

                    DbParameter priorityID = command.CreateParameter();
                    priorityID.ParameterName = "@priorityID";
                    priorityID.Value = o.priorityID;
                    command.Parameters.Add(priorityID);

                    DbParameter statusConfirmID = command.CreateParameter();
                    statusConfirmID.ParameterName = "@statusConfirmID";
                    statusConfirmID.Value = o.statusConfirmID;
                    command.Parameters.Add(statusConfirmID);

                    DbParameter sort = command.CreateParameter();
                    sort.ParameterName = "@sort";
                    sort.Value = o.sort;
                    command.Parameters.Add(sort);

                    DbParameter varietyID = command.CreateParameter();
                    varietyID.ParameterName = "@varietyID";
                    varietyID.Value = o.varietyID;
                    command.Parameters.Add(varietyID);

                    DbParameter categoryID = command.CreateParameter();
                    categoryID.ParameterName = "@categoryID";
                    categoryID.Value = o.categoryID;
                    command.Parameters.Add(categoryID);

                    DbParameter category = command.CreateParameter();
                    category.ParameterName = "@category";
                    category.Value = o.category;
                    command.Parameters.Add(category);

                    DbParameter formatID = command.CreateParameter();
                    formatID.ParameterName = "@formatID";
                    formatID.Value = o.formatID;
                    command.Parameters.Add(formatID);

                    DbParameter packageProductID = command.CreateParameter();
                    packageProductID.ParameterName = "@packageProductID";
                    packageProductID.Value = o.packageProductID;
                    command.Parameters.Add(packageProductID);

                    DbParameter brandID = command.CreateParameter();
                    brandID.ParameterName = "@brandID";
                    brandID.Value = o.brandID;
                    command.Parameters.Add(brandID);

                    DbParameter presentationID = command.CreateParameter();
                    presentationID.ParameterName = "@presentationID";
                    presentationID.Value = o.presentationID;
                    command.Parameters.Add(presentationID);

                    DbParameter labelID = command.CreateParameter();
                    labelID.ParameterName = "@labelID";
                    labelID.Value = o.labelID;
                    command.Parameters.Add(labelID);

                    DbParameter sizeID = command.CreateParameter();
                    sizeID.ParameterName = "@sizeID";
                    sizeID.Value = o.sizeID;
                    command.Parameters.Add(sizeID);

                    DbParameter codepack = command.CreateParameter();
                    codepack.ParameterName = "@codepack";
                    codepack.Value = o.codepack;
                    command.Parameters.Add(codepack);

                    DbParameter codepackID = command.CreateParameter();
                    codepackID.ParameterName = "@codepackID";
                    codepackID.Value = o.codepackID;
                    command.Parameters.Add(codepackID);

                    DbParameter incotermID = command.CreateParameter();
                    incotermID.ParameterName = "@incotermID";
                    incotermID.Value = o.incotermID;
                    command.Parameters.Add(incotermID);

                    DbParameter conditionPaymentID = command.CreateParameter();
                    conditionPaymentID.ParameterName = "@conditionPaymentID";
                    conditionPaymentID.Value = o.conditionPaymentID;
                    command.Parameters.Add(conditionPaymentID);

                    DbParameter paymentTermID = command.CreateParameter();
                    paymentTermID.ParameterName = "@paymentTermID";
                    paymentTermID.Value = o.paymentTermID;
                    command.Parameters.Add(paymentTermID);

                    DbParameter userID = command.CreateParameter();
                    userID.ParameterName = "@userID";
                    userID.Value = o.userID;
                    command.Parameters.Add(userID);

                    DbParameter statusID = command.CreateParameter();
                    statusID.ParameterName = "@statusID";
                    statusID.Value = o.statusID;
                    command.Parameters.Add(statusID);

                    DbParameter padlock = command.CreateParameter();
                    padlock.ParameterName = "@padlock";
                    padlock.Value = o.padlock;
                    command.Parameters.Add(padlock);

                    DbParameter plu = command.CreateParameter();
                    plu.ParameterName = "@plu";
                    plu.Value = o.plu;
                    command.Parameters.Add(plu);

                    DbParameter pviaID = command.CreateParameter();
                    pviaID.ParameterName = "@pviaID";
                    pviaID.Value = o.viaID;
                    command.Parameters.Add(pviaID);

                    DbParameter pconsigneeID = command.CreateParameter();
                    pconsigneeID.ParameterName = "@pconsigneeID";
                    pconsigneeID.Value = o.consigneeID;
                    command.Parameters.Add(pconsigneeID);

                    DbParameter pmarketID = command.CreateParameter();
                    pmarketID.ParameterName = "@pmarketID";
                    pmarketID.Value = o.marketID;
                    command.Parameters.Add(pmarketID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("planBoxWeekID");
                    tbl.Columns.Add("planCustomerVarietyID");
                    tbl.Columns.Add("projectedWeekID");
                    tbl.Columns.Add("boxes");
                    tbl.Columns.Add("price");
                    tbl.Columns.Add("userID");
                    tbl.Columns.Add("dateCreated");

                    foreach (var item in o.planBoxWeek)
                    {
                        tbl.Rows.Add(item.planBoxWeekID, item.planCustomerVarietyID, item.projectedWeekID, item.boxes, item.price, o.userID, "");
                    }

                    var planBoxWeek = new SqlParameter("@planBoxWeek", SqlDbType.Structured);
                    planBoxWeek.Value = tbl;
                    planBoxWeek.TypeName = "[commerce].[planBoxWeek]";
                    command.Parameters.Add(planBoxWeek);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable RegisterCommercialPlanWithMultipleVarietiesAndSizes(ENPlanCustomerBrand o)
        {
            o.category = o.category ?? string.Empty;
            o.marketID = o.marketID ?? string.Empty;
            o.sizeID = o.sizeID ?? string.Empty;
            o.labelID = o.labelID ?? 0;
            o.programID = o.programID ?? 0;
            o.sort = o.sort ?? 0;
            o.padlock = o.padlock ?? 0;
            o.plu = o.plu ?? 0;
            o.price = o.price ?? "0";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_CommercialPlan_RegisterPlanCustomer] @planCustomerVarietyID,@campaignID,@customerID," +
                                            "@programID,@destinationID,@kamID,@priorityID,@statusConfirmID,@sort,@varietyID,@categoryID,@category," +
                                            "@formatID,@packageProductID,@brandID,@presentationID,@labelID,@codepack,@codepackID,@incotermID," +
                                            "@conditionPaymentID,@paymentTermID,@userID,@statusID,@padlock,@plu,@pviaID,@pconsigneeID," +
                                            "@pmarketID,@psizeID,@pprice,@planBoxWeek";


                    DbParameter planCustomerVarietyID = command.CreateParameter();
                    planCustomerVarietyID.ParameterName = "@planCustomerVarietyID";
                    planCustomerVarietyID.Value = o.planCustomerVarietyID;
                    command.Parameters.Add(planCustomerVarietyID);

                    DbParameter campaignID = command.CreateParameter();
                    campaignID.ParameterName = "@campaignID";
                    campaignID.Value = o.campaignID;
                    command.Parameters.Add(campaignID);

                    DbParameter customerID = command.CreateParameter();
                    customerID.ParameterName = "@customerID";
                    customerID.Value = o.customerID;
                    command.Parameters.Add(customerID);

                    DbParameter programID = command.CreateParameter();
                    programID.ParameterName = "@programID";
                    programID.Value = o.programID;
                    command.Parameters.Add(programID);

                    DbParameter destinationID = command.CreateParameter();
                    destinationID.ParameterName = "@destinationID";
                    destinationID.Value = o.destinationID;
                    command.Parameters.Add(destinationID);

                    DbParameter kamID = command.CreateParameter();
                    kamID.ParameterName = "@kamID";
                    kamID.Value = o.kamID;
                    command.Parameters.Add(kamID);

                    DbParameter priorityID = command.CreateParameter();
                    priorityID.ParameterName = "@priorityID";
                    priorityID.Value = o.priorityID;
                    command.Parameters.Add(priorityID);

                    DbParameter statusConfirmID = command.CreateParameter();
                    statusConfirmID.ParameterName = "@statusConfirmID";
                    statusConfirmID.Value = o.statusConfirmID;
                    command.Parameters.Add(statusConfirmID);

                    DbParameter sort = command.CreateParameter();
                    sort.ParameterName = "@sort";
                    sort.Value = o.sort;
                    command.Parameters.Add(sort);

                    DbParameter varietyID = command.CreateParameter();
                    varietyID.ParameterName = "@varietyID";
                    varietyID.Value = o.varietyID;
                    command.Parameters.Add(varietyID);

                    DbParameter categoryID = command.CreateParameter();
                    categoryID.ParameterName = "@categoryID";
                    categoryID.Value = o.categoryID;
                    command.Parameters.Add(categoryID);

                    DbParameter category = command.CreateParameter();
                    category.ParameterName = "@category";
                    category.Value = o.category;
                    command.Parameters.Add(category);

                    DbParameter formatID = command.CreateParameter();
                    formatID.ParameterName = "@formatID";
                    formatID.Value = o.formatID;
                    command.Parameters.Add(formatID);

                    DbParameter packageProductID = command.CreateParameter();
                    packageProductID.ParameterName = "@packageProductID";
                    packageProductID.Value = o.packageProductID;
                    command.Parameters.Add(packageProductID);

                    DbParameter brandID = command.CreateParameter();
                    brandID.ParameterName = "@brandID";
                    brandID.Value = o.brandID;
                    command.Parameters.Add(brandID);

                    DbParameter presentationID = command.CreateParameter();
                    presentationID.ParameterName = "@presentationID";
                    presentationID.Value = o.presentationID;
                    command.Parameters.Add(presentationID);

                    DbParameter labelID = command.CreateParameter();
                    labelID.ParameterName = "@labelID";
                    labelID.Value = o.labelID;
                    command.Parameters.Add(labelID);

                    DbParameter codepack = command.CreateParameter();
                    codepack.ParameterName = "@codepack";
                    codepack.Value = o.codepack;
                    command.Parameters.Add(codepack);

                    DbParameter codepackID = command.CreateParameter();
                    codepackID.ParameterName = "@codepackID";
                    codepackID.Value = o.codepackID;
                    command.Parameters.Add(codepackID);

                    DbParameter incotermID = command.CreateParameter();
                    incotermID.ParameterName = "@incotermID";
                    incotermID.Value = o.incotermID;
                    command.Parameters.Add(incotermID);

                    DbParameter conditionPaymentID = command.CreateParameter();
                    conditionPaymentID.ParameterName = "@conditionPaymentID";
                    conditionPaymentID.Value = o.conditionPaymentID;
                    command.Parameters.Add(conditionPaymentID);

                    DbParameter paymentTermID = command.CreateParameter();
                    paymentTermID.ParameterName = "@paymentTermID";
                    paymentTermID.Value = o.paymentTermID;
                    command.Parameters.Add(paymentTermID);

                    DbParameter userID = command.CreateParameter();
                    userID.ParameterName = "@userID";
                    userID.Value = o.userID;
                    command.Parameters.Add(userID);

                    DbParameter statusID = command.CreateParameter();
                    statusID.ParameterName = "@statusID";
                    statusID.Value = o.statusID;
                    command.Parameters.Add(statusID);

                    DbParameter padlock = command.CreateParameter();
                    padlock.ParameterName = "@padlock";
                    padlock.Value = o.padlock;
                    command.Parameters.Add(padlock);

                    DbParameter plu = command.CreateParameter();
                    plu.ParameterName = "@plu";
                    plu.Value = o.plu;
                    command.Parameters.Add(plu);

                    DbParameter pviaID = command.CreateParameter();
                    pviaID.ParameterName = "@pviaID";
                    pviaID.Value = o.viaID;
                    command.Parameters.Add(pviaID);

                    DbParameter pconsigneeID = command.CreateParameter();
                    pconsigneeID.ParameterName = "@pconsigneeID";
                    pconsigneeID.Value = o.consigneeID;
                    command.Parameters.Add(pconsigneeID);

                    DbParameter pmarketID = command.CreateParameter();
                    pmarketID.ParameterName = "@pmarketID";
                    pmarketID.Value = o.marketID;
                    command.Parameters.Add(pmarketID);

                    DbParameter psizeID = command.CreateParameter();
                    psizeID.ParameterName = "@psizeID";
                    psizeID.Value = o.sizeID;
                    command.Parameters.Add(psizeID);

                    DbParameter pprice = command.CreateParameter();
                    pprice.ParameterName = "@pprice";
                    pprice.Value = float.Parse(o.price);
                    command.Parameters.Add(pprice);

                    var tbl = new DataTable();
                    tbl.Columns.Add("planBoxWeekID");
                    tbl.Columns.Add("planCustomerVarietyID");
                    tbl.Columns.Add("projectedWeekID");
                    tbl.Columns.Add("boxes");
                    tbl.Columns.Add("price");
                    tbl.Columns.Add("userID");
                    tbl.Columns.Add("dateCreated");

                    foreach (var item in o.planBoxWeek)
                    {
                        tbl.Rows.Add(item.planBoxWeekID, item.planCustomerVarietyID, item.projectedWeekID, item.boxes, item.price, o.userID, "");
                    }

                    var planBoxWeek = new SqlParameter("@planBoxWeek", SqlDbType.Structured);
                    planBoxWeek.Value = tbl;
                    planBoxWeek.TypeName = "[commerce].[planBoxWeek]";
                    command.Parameters.Add(planBoxWeek);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable CreateSaleRequestAndPO(string cropId, int campaignID, int userCreated, int projectedWeekID, PlanComercialCab objPlanComercial)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_SalesRequestAndPOInsert] @PcropId,@PcampaignID,@PuserCreated,@PprojectedWeekID,@PlanComercialDetail";
                    DbParameter PcropId = command.CreateParameter();
                    PcropId.ParameterName = "@PcropId";
                    PcropId.Value = cropId;
                    command.Parameters.Add(PcropId);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    var dtPCdetail = new DataTable();
                    dtPCdetail.Columns.Add("Id");
                    dtPCdetail.Columns.Add("PlanCustomerVarietyID");
                    dtPCdetail.Columns.Add("VarietyID");
                    dtPCdetail.Columns.Add("CategoryID");
                    dtPCdetail.Columns.Add("CustomerID");
                    dtPCdetail.Columns.Add("Program");
                    dtPCdetail.Columns.Add("Priority");
                    dtPCdetail.Columns.Add("DestinationID");
                    dtPCdetail.Columns.Add("CodePack");
                    dtPCdetail.Columns.Add("BoxesPerPallet");
                    dtPCdetail.Columns.Add("ToProcess");
                    dtPCdetail.Columns.Add("Pallets");
                    dtPCdetail.Columns.Add("Group");

                    foreach (var item in objPlanComercial.planComercialDetail)
                    {
                        dtPCdetail.Rows.Add(item.Id, item.PlanCustomerVarietyID, item.VarietyID, item.CategoryID, item.CustomerID, item.Program,
                            item.Priority, item.DestinationID, item.CodePack, item.BoxesPerPallet, item.ToProcess, item.Pallets, item.Group);
                    }

                    var dtPlanComercialDetail = new SqlParameter("@PlanComercialDetail", SqlDbType.Structured);
                    dtPlanComercialDetail.Value = dtPCdetail;
                    dtPlanComercialDetail.TypeName = "[package].[tu_PlanComercialDetail]";
                    command.Parameters.Add(dtPlanComercialDetail);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable CreateSaleRequestAndPOBLU(string cropId, int campaignID, int userCreated, int projectedWeekID, PlanComercialBlu objPlanComercial)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_SalesRequestAndPOInsertBlue] @PcropId,@PcampaignID,@PuserCreated,@PprojectedWeekID,@PlanComercialDetail";
                    DbParameter PcropId = command.CreateParameter();
                    PcropId.ParameterName = "@PcropId";
                    PcropId.Value = cropId;
                    command.Parameters.Add(PcropId);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    var dtPCdetail = new DataTable();
                    dtPCdetail.Columns.Add("Id");
                    dtPCdetail.Columns.Add("PlanCustomerVarietyID");
                    dtPCdetail.Columns.Add("CustomerID");
                    dtPCdetail.Columns.Add("ProgramID");
                    dtPCdetail.Columns.Add("DestinationID");
                    dtPCdetail.Columns.Add("BrandID");
                    dtPCdetail.Columns.Add("VarietyID");
                    dtPCdetail.Columns.Add("SizeID");
                    dtPCdetail.Columns.Add("CodePack");
                    dtPCdetail.Columns.Add("CodePackID");
                    dtPCdetail.Columns.Add("viaID");
                    dtPCdetail.Columns.Add("MarketID");
                    dtPCdetail.Columns.Add("WowID");
                    dtPCdetail.Columns.Add("price");
                    dtPCdetail.Columns.Add("ToProcess");
                    dtPCdetail.Columns.Add("Pallets");
                    dtPCdetail.Columns.Add("Group");
                    dtPCdetail.Columns.Add("workOrder");

                    foreach (var item in objPlanComercial.planComercialDetail)
                    {
                        dtPCdetail.Rows.Add(item.Id, item.PlanCustomerVarietyID, item.CustomerID, item.Program, item.DestinationID, item.BrandID, item.VarietyID, item.SizeID, item.CodePack,
                            item.CodePackID, item.ViaID, item.MarketID, item.WowID, item.price, item.ToProcess, item.Pallets, item.Group, item.workOrder);
                    }

                    var dtPlanComercialDetail = new SqlParameter("@PlanComercialDetail", SqlDbType.Structured);
                    dtPlanComercialDetail.Value = dtPCdetail;
                    dtPlanComercialDetail.TypeName = "[package].[PlanComercialDetailBlu]";
                    command.Parameters.Add(dtPlanComercialDetail);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPlanCustomerByFilters(int campaignID, int customerID, int destinationID, int programID, string categoryID, int varietyID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[planCustomerByFilter] @PcampaignID, @PcustomerID, @PdestinationID, @PprogramID, @PcategoryID, @PvarietyID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprogramID = command.CreateParameter();
                    PprogramID.ParameterName = "@PprogramID";
                    PprogramID.Value = programID;
                    command.Parameters.Add(PprogramID);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = categoryID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PvarietyID = command.CreateParameter();
                    PvarietyID.ParameterName = "@PvarietyID";
                    PvarietyID.Value = varietyID;
                    command.Parameters.Add(PvarietyID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPlanBlueberryByFilters(int campaignID, int customerID, int destinationID, int programID, string brandID, int viaID, int formatID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[planBlueberryByFilter] @PcampaignID, @PcustomerID, @PdestinationID, @PprogramID, @PbrandID, @PviaID, @PformatID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprogramID = command.CreateParameter();
                    PprogramID.ParameterName = "@PprogramID";
                    PprogramID.Value = programID;
                    command.Parameters.Add(PprogramID);

                    DbParameter PbrandID = command.CreateParameter();
                    PbrandID.ParameterName = "@PbrandID";
                    PbrandID.Value = brandID;
                    command.Parameters.Add(PbrandID);

                    DbParameter PviaID = command.CreateParameter();
                    PviaID.ParameterName = "@PviaID";
                    PviaID.Value = viaID;
                    command.Parameters.Add(PviaID);

                    DbParameter PformatID = command.CreateParameter();
                    PformatID.ParameterName = "@PformatID";
                    PformatID.Value = formatID;
                    command.Parameters.Add(PformatID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
