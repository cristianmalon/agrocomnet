﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;

namespace AgrocomApi.Models
{
    public class ReportsDAO
    {
        private readonly ApplicationDbContext context;

        public ReportsDAO(ApplicationDbContext context)
        {
            this.context = context;
        }
        public DataTable summaryList(string opt, int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_summaryMultiCropList] @PsearchOpt, @PcampaignID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable expoMastersList(string opt)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_expoMasterReportList] @PsearchOpt";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable shippingInstructionList(string opt, int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingInstructionList] @PsearchOpt,@PcampaignID";
                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw;  //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }

        }

        public DataTable GetreportVersus(string opt)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_reportVersusPOSRF] @PsearchOpt";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GePackinglistPO(string nroPackingList)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_serviceSispackinglist] @PnroPackingList";

                    DbParameter PnroPackingList = command.CreateParameter();
                    PnroPackingList.ParameterName = "@PnroPackingList";
                    PnroPackingList.Value = nroPackingList;
                    command.Parameters.Add(PnroPackingList);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ApacheReportPackingList(string opt)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_reportApachePackingList] @PsearchOpt";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
