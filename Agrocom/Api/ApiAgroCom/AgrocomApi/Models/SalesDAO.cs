﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class SalesDAO
    {
        private readonly ApplicationDbContext context;

        public SalesDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable List(string opt, string val, int? weekIni, int? weekEnd, int? userID,int? campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesList] @PsearchOpt, @PsearchValue, @PweekIni,@PweekEnd,@PuserID,@PcampaignID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = val;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PWeekIni = command.CreateParameter();
                    PWeekIni.ParameterName = "@PWeekIni";
                    PWeekIni.Value = weekIni;
                    command.Parameters.Add(PWeekIni);

                    DbParameter PweekEnd = command.CreateParameter();
                    PweekEnd.ParameterName = "@PweekEnd";
                    PweekEnd.Value = weekEnd;
                    command.Parameters.Add(PweekEnd);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveDetail(int saleDetailID, int destinationID, int projectedWeekID, int customerID, double quantity, double price, string paymentTerm, int variedadID, string categoryID, int codePackID, int conditionPaymentID, int incotermID, int userCreated)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesCreate2] @PsaleDetailID, @PdestinationID,@PprojectedWeekID,@PcustomerID,@Pquantity, @Pprice,@PpaymentTerm,@PvariedadID,@PcategoryID,@PcodePackID,@PconditionPaymentID,@PincotermID,@PuserCreated";

                    DbParameter PsaleDetailID = command.CreateParameter();
                    PsaleDetailID.ParameterName = "@PsaleDetailID";
                    PsaleDetailID.Value = saleDetailID;
                    command.Parameters.Add(PsaleDetailID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter Pquantity = command.CreateParameter();
                    Pquantity.ParameterName = "@Pquantity";
                    Pquantity.Value = quantity;
                    command.Parameters.Add(Pquantity);

                    DbParameter Pprice = command.CreateParameter();
                    Pprice.ParameterName = "@Pprice";
                    Pprice.Value = price;
                    command.Parameters.Add(Pprice);

                    DbParameter PpaymentTerm = command.CreateParameter();
                    PpaymentTerm.ParameterName = "@PpaymentTerm";
                    PpaymentTerm.Value = paymentTerm;
                    command.Parameters.Add(PpaymentTerm);

                    DbParameter PvariedadID = command.CreateParameter();
                    PvariedadID.ParameterName = "@PvariedadID";
                    PvariedadID.Value = variedadID;
                    command.Parameters.Add(PvariedadID);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = categoryID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PcodePackID = command.CreateParameter();
                    PcodePackID.ParameterName = "@PcodePackID";
                    PcodePackID.Value = codePackID;
                    command.Parameters.Add(PcodePackID);

                    DbParameter PconditionPaymentID = command.CreateParameter();
                    PconditionPaymentID.ParameterName = "@PconditionPaymentID";
                    PconditionPaymentID.Value = conditionPaymentID;
                    command.Parameters.Add(PconditionPaymentID);

                    DbParameter PincotermID = command.CreateParameter();
                    PincotermID.ParameterName = "@PincotermID";
                    PincotermID.Value = incotermID;
                    command.Parameters.Add(PincotermID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveSale(int destinationID, int projectedWeekID, int customerID, int userCreated)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesCreate] @PdestinationID,@PprojectedWeekID,@PcustomerID,@PuserCreated";

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetKam(string opt, string id)
        {
            id = id ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_kamList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveSaleEN(ENSales o)
        {

            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_saleCreateEN] @PsaleID,@PdestinationID,@PprojectedWeekID,@PcustomerID,@PuserCreated,@PwowID,@Pcomments,@Pstatus,@PcampaignID,@PsaleDetail_table";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = o.saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = o.destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = o.projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = o.customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = o.userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PwowID = command.CreateParameter();
                    PwowID.ParameterName = "@PwowID";
                    PwowID.Value = o.wowID;
                    command.Parameters.Add(PwowID);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.commentary;
                    command.Parameters.Add(Pcomments);

                    DbParameter Pstatus = command.CreateParameter();
                    Pstatus.ParameterName = "@Pstatus";
                    Pstatus.Value = o.statusID;
                    command.Parameters.Add(Pstatus);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = o.campaignID;
                    command.Parameters.Add(PcampaignID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("saleDetailID");
                    tbl.Columns.Add("saleID");
                    tbl.Columns.Add("quantity");
                    tbl.Columns.Add("price");
                    tbl.Columns.Add("paymentTerm");
                    tbl.Columns.Add("productsID");
                    tbl.Columns.Add("paymentTermID");
                    tbl.Columns.Add("conditionPaymentID");
                    tbl.Columns.Add("incotermID");
                    tbl.Columns.Add("statusID");
                    tbl.Columns.Add("categoryID");
                    tbl.Columns.Add("varietyID");
                    tbl.Columns.Add("codePackID");
                    tbl.Columns.Add("presentationID");
                    tbl.Columns.Add("finalCustomerID");                    
                    tbl.Columns.Add("packageProductID");
                    tbl.Columns.Add("flexible");
                    tbl.Columns.Add("quantityMinimun");
                    tbl.Columns.Add("priceBilling");
                    tbl.Columns.Add("varieties");
                    tbl.Columns.Add("sizes");
                    tbl.Columns.Add("codePack");
                    tbl.Columns.Add("workOrder");

                    foreach (var item in o.details)
                    {
                        tbl.Rows.Add(item.saleDetailID, item.saleID, item.quantity, item.price, item.paymentTerm, item.productsID,
                            item.paymentTermID, item.conditionPaymentID, item.incotermID, item.statusID, item.categoryID, item.varietyID, 
                            item.codePackID, item.presentationID, item.finalCustomerID , item.packageProductID, item.flexible, 
                            item.quantityMinimun, item.priceBilling, item.varieties, item.sizes, item.codePack, item.workOrder);
                    }

                    var PsaleDetail_table = new SqlParameter("@PsaleDetail_table", SqlDbType.Structured);
                    PsaleDetail_table.Value = tbl;
                    PsaleDetail_table.TypeName = "[sales].[saleDetails]";
                    command.Parameters.Add(PsaleDetail_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaleOrderWithoutCommercialPlanInsert(ENSales o)
        {

            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_SaleOrderWithoutCommercialPlanInsert] @PsaleID,@PdestinationID,@PprojectedWeekID,@PcustomerID,@PuserCreated,@PwowID,@Pcomments,@Pstatus,@PcampaignID,@PsaleDetail_table";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = o.saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = o.destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = o.projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = o.customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = o.userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PwowID = command.CreateParameter();
                    PwowID.ParameterName = "@PwowID";
                    PwowID.Value = o.wowID;
                    command.Parameters.Add(PwowID);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.commentary;
                    command.Parameters.Add(Pcomments);

                    DbParameter Pstatus = command.CreateParameter();
                    Pstatus.ParameterName = "@Pstatus";
                    Pstatus.Value = o.statusID;
                    command.Parameters.Add(Pstatus);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = o.campaignID;
                    command.Parameters.Add(PcampaignID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("saleDetailID");
                    tbl.Columns.Add("saleID");
                    tbl.Columns.Add("quantity");
                    tbl.Columns.Add("price");
                    tbl.Columns.Add("paymentTerm");
                    tbl.Columns.Add("productsID");
                    tbl.Columns.Add("paymentTermID");
                    tbl.Columns.Add("conditionPaymentID");
                    tbl.Columns.Add("incotermID");
                    tbl.Columns.Add("statusID");
                    tbl.Columns.Add("categoryID");
                    tbl.Columns.Add("varietyID");
                    tbl.Columns.Add("codePackID");
                    tbl.Columns.Add("presentationID");
                    tbl.Columns.Add("finalCustomerID");
                    tbl.Columns.Add("packageProductID");
                    tbl.Columns.Add("flexible");
                    tbl.Columns.Add("quantityMinimun");
                    tbl.Columns.Add("priceBilling");
                    tbl.Columns.Add("varieties");
                    tbl.Columns.Add("sizes");
                    tbl.Columns.Add("codePack");
                    tbl.Columns.Add("workOrder");

                    foreach (var item in o.details)
                    {
                        tbl.Rows.Add(item.saleDetailID, item.saleID, item.quantity, item.price, item.paymentTerm, item.productsID,
                            item.paymentTermID, item.conditionPaymentID, item.incotermID, item.statusID, item.categoryID, item.varietyID,
                            item.codePackID, item.presentationID, item.finalCustomerID, item.packageProductID, item.flexible,
                            item.quantityMinimun, item.priceBilling, item.varieties, item.sizes, item.codePack, item.workOrder);
                    }

                    var PsaleDetail_table = new SqlParameter("@PsaleDetail_table", SqlDbType.Structured);
                    PsaleDetail_table.Value = tbl;
                    PsaleDetail_table.TypeName = "[sales].[saleDetails]";
                    command.Parameters.Add(PsaleDetail_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable SaveSaleBulk(int saleID, int destinationID, int projectedWeekID, int customerID, int userCreated, int wowID, string comments, int status, List<typecollectionLarge> datails_table)
        {
            comments = comments ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_saleCreateBulk] @PsaleID,@PdestinationID,@PprojectedWeekID,@PcustomerID,@PuserCreated,@PwowID,@Pcomments,@Pstatus,@PsaleDetail_table";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = saleID;
                    command.Parameters.Add(PsaleID);


                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PwowID = command.CreateParameter();
                    PwowID.ParameterName = "@PwowID";
                    PwowID.Value = wowID;
                    command.Parameters.Add(PwowID);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter Pstatus = command.CreateParameter();
                    Pstatus.ParameterName = "@Pstatus";
                    Pstatus.Value = status;
                    command.Parameters.Add(Pstatus);

                    var tbl = new DataTable();
                    tbl.Columns.Add("int1");
                    tbl.Columns.Add("int2");
                    tbl.Columns.Add("int3");
                    tbl.Columns.Add("int4");
                    tbl.Columns.Add("int5");
                    tbl.Columns.Add("int6");
                    tbl.Columns.Add("description1");
                    tbl.Columns.Add("description2");
                    tbl.Columns.Add("description3");
                    tbl.Columns.Add("description4");
                    tbl.Columns.Add("description5");
                    tbl.Columns.Add("decimal1");
                    tbl.Columns.Add("decimal2");
                    tbl.Columns.Add("decimal3");
                    tbl.Columns.Add("decimal4");

                    foreach (var item in datails_table)
                    {
                        tbl.Rows.Add(item.int1, item.int2, item.int3, item.int4, item.int5, item.int6, item.description1, item.description2, item.description3, item.description4, item.description5, item.decimal1, item.decimal2, item.decimal3, item.decimal4);
                    }

                    var PsaleDetail_table = new SqlParameter("@PsaleDetail_table", SqlDbType.Structured);
                    PsaleDetail_table.Value = tbl;
                    PsaleDetail_table.TypeName = "TypeCollectionLarge";
                    command.Parameters.Add(PsaleDetail_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveSaleDetail(int saleID, double quantity, double price, string paymentTerm, int varietyID, string categoryID, int codePackID, int conditionPaymentID, int incotermID, int userCreated)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesDetailCreate] @PsaleID, @Pquantity, @Pprice,@PpaymentTerm,@PvarietyID,@PcategoryID,@PcodePackID,@PconditionPaymentID,@PincotermID,@PuserCreated";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter Pquantity = command.CreateParameter();
                    Pquantity.ParameterName = "@Pquantity";
                    Pquantity.Value = quantity;
                    command.Parameters.Add(Pquantity);

                    DbParameter Pprice = command.CreateParameter();
                    Pprice.ParameterName = "@Pprice";
                    Pprice.Value = price;
                    command.Parameters.Add(Pprice);

                    DbParameter PpaymentTerm = command.CreateParameter();
                    PpaymentTerm.ParameterName = "@PpaymentTerm";
                    PpaymentTerm.Value = paymentTerm;
                    command.Parameters.Add(PpaymentTerm);

                    DbParameter PvarietyID = command.CreateParameter();
                    PvarietyID.ParameterName = "@PvariedadID";
                    PvarietyID.Value = varietyID;
                    command.Parameters.Add(PvarietyID);

                    DbParameter PcategoryID = command.CreateParameter();
                    PcategoryID.ParameterName = "@PcategoryID";
                    PcategoryID.Value = categoryID;
                    command.Parameters.Add(PcategoryID);

                    DbParameter PcodePackID = command.CreateParameter();
                    PcodePackID.ParameterName = "@PcodePackID";
                    PcodePackID.Value = codePackID;
                    command.Parameters.Add(PcodePackID);

                    DbParameter PconditionPaymentID = command.CreateParameter();
                    PconditionPaymentID.ParameterName = "@PconditionPaymentID";
                    PconditionPaymentID.Value = conditionPaymentID;
                    command.Parameters.Add(PconditionPaymentID);

                    DbParameter PincotermID = command.CreateParameter();
                    PincotermID.ParameterName = "@PincotermID";
                    PincotermID.Value = incotermID;
                    command.Parameters.Add(PincotermID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = userCreated;
                    command.Parameters.Add(PuserCreated);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetSalesPending()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec  [package].[sp_saleRequestPendingList]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetSalesUncommitted()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_uncommittedSalesRequestList]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable ListReport(string opt, int? campaignID)
        {
            campaignID = campaignID ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commercePlanList] @PsearchOpt, @PcampaignID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListReportBox(int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commercePlanByBoxList] @PcampaignID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListReportSum(int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commercePlanSummaryList] @PcampaignID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListReportBoxSum(int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commercePlanByBoxSummaryList] @PcampaignID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable DeleteSales(int saleID, int userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_salesDelete] @PsaleID,@PuserID";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListReportPO(int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_commercePlanByPOList] @PcampaignID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetPaymentTermByCustomer(string opt, int customerID, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_paymentTerm] @PsearchOpt,@PcustomerID,@PcropID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PcropiD = command.CreateParameter();
                    PcropiD.ParameterName = "@PcropID";
                    PcropiD.Value = cropID;
                    command.Parameters.Add(PcropiD);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable salesRejected(int saleID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_saleRequestRejectedList] @PsaleID";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = saleID;
                    command.Parameters.Add(PsaleID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable salesAllocation(string searchOpt, int projectedWeekIni, int projectedWeekEnd, string growerID, string marketID, int customerID)
        {
            growerID = growerID ?? "";
            marketID = marketID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesAllocation] @PsearchOpt, @PprojectedWeekIni, @PprojectedWeekEnd,@PgrowerID,@PmarketID,@PcustomerID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PprojectedWeekIni = command.CreateParameter();
                    PprojectedWeekIni.ParameterName = "@PprojectedWeekIni";
                    PprojectedWeekIni.Value = projectedWeekIni;
                    command.Parameters.Add(PprojectedWeekIni);

                    DbParameter PprojectedWeekEnd = command.CreateParameter();
                    PprojectedWeekEnd.ParameterName = "@PprojectedWeekEnd";
                    PprojectedWeekEnd.Value = projectedWeekEnd;
                    command.Parameters.Add(PprojectedWeekEnd);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PmarketID = command.CreateParameter();
                    PmarketID.ParameterName = "@PmarketID";
                    PmarketID.Value = marketID;
                    command.Parameters.Add(PmarketID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable salesOutSpecs(int orderProductionID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesOutSpecsList] @PorderProductionID";

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveSalePC(ENSalesPC o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_saleCreatePC] @PsaleID,@PdestinationID,@PprojectedWeekID,@PcustomerID,@PuserCreated,@PwowID,@Pstatus,@PcampaignID,@PsaleDetail_table";

                    DbParameter PsaleID = command.CreateParameter();
                    PsaleID.ParameterName = "@PsaleID";
                    PsaleID.Value = o.saleID;
                    command.Parameters.Add(PsaleID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = o.destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = o.projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = o.customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = o.userCreated;
                    command.Parameters.Add(PuserCreated);

                    DbParameter PwowID = command.CreateParameter();
                    PwowID.ParameterName = "@PwowID";
                    PwowID.Value = o.wowID;
                    command.Parameters.Add(PwowID);

                    DbParameter Pstatus = command.CreateParameter();
                    Pstatus.ParameterName = "@Pstatus";
                    Pstatus.Value = o.statusID;
                    command.Parameters.Add(Pstatus);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = o.campaignID;
                    command.Parameters.Add(PcampaignID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("saleDetailID");
                    tbl.Columns.Add("saleID");
                    tbl.Columns.Add("quantity");
                    tbl.Columns.Add("unitMeasureID");

                    tbl.Columns.Add("price");
                    tbl.Columns.Add("paymentTerm");
                    tbl.Columns.Add("productsID");
                    tbl.Columns.Add("conditionPaymentID");

                    tbl.Columns.Add("incotermID");
                    tbl.Columns.Add("userCreated");
                    //tbl.Columns.Add("dateCreated");
                    tbl.Columns.Add("statusID");

                    tbl.Columns.Add("categoryID");
                    tbl.Columns.Add("varietyID");
                    tbl.Columns.Add("codePackID");
                    tbl.Columns.Add("presentationID");

                    tbl.Columns.Add("finalCustomerID");
                    tbl.Columns.Add("paymentTermID");
                    tbl.Columns.Add("userModifyID");
                    //tbl.Columns.Add("dateModified");

                    tbl.Columns.Add("flexible");
                    tbl.Columns.Add("quantityMinimun");
                    tbl.Columns.Add("priceBilling");

                    tbl.Columns.Add("kamID");
                    tbl.Columns.Add("priorityID");
                    tbl.Columns.Add("formatID");
                    tbl.Columns.Add("packageProductID");
                    tbl.Columns.Add("brandID");
                    tbl.Columns.Add("labelID");
                    tbl.Columns.Add("codepack");

                    tbl.Columns.Add("varieties");
                    tbl.Columns.Add("sizes");

                    foreach (var item in o.details)
                    {
                        tbl.Rows.Add(item.saleDetailID, item.saleID, item.quantity, item.unitMeasureID,
                            item.price, item.paymentTerm, item.productsID, item.conditionPaymentID,
                            item.incotermID, item.userCreated, item.statusID,
                            item.categoryID, item.varietyID, item.codePackID, item.presentationID,
                            item.finalCustomerID, item.paymentTermID, item.userModifyID,
                            item.flexible, item.quantityMinimun, item.priceBilling,
                            item.kamID, item.priorityID, item.formatID, item.packageProductID, item.brandID, item.labelID, item.codepack,
                            item.varieties, item.sizes);
                    }

                    var PsaleDetail_table = new SqlParameter("@PsaleDetail_table", SqlDbType.Structured);
                    PsaleDetail_table.Value = tbl;
                    PsaleDetail_table.TypeName = "sales.saleDetails2";
                    command.Parameters.Add(PsaleDetail_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListByCampaign(string opt, string val, int? weekIni, int? weekEnd, int? userID, int? campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_salesList] @PsearchOpt, @PsearchValue, @PweekIni,@PweekEnd,@PuserID,@PcampaignID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = val;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PWeekIni = command.CreateParameter();
                    PWeekIni.ParameterName = "@PWeekIni";
                    PWeekIni.Value = weekIni;
                    command.Parameters.Add(PWeekIni);

                    DbParameter PweekEnd = command.CreateParameter();
                    PweekEnd.ParameterName = "@PweekEnd";
                    PweekEnd.Value = weekEnd;
                    command.Parameters.Add(PweekEnd);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetIncotermList(string opt, string cropID, int? viaID)
        {
            cropID = cropID?? "";
            viaID = viaID ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec package.sp_incotermList @PsearchOpt,@PcropID,@PviaID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PviaID = command.CreateParameter();
                    PviaID.ParameterName = "@PviaID";
                    PviaID.Value = viaID;
                    command.Parameters.Add(PviaID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetDocument(string opt, string value)
        {
            value = value ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec package.sp_documentList @PsearchOpt,@PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = value;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
