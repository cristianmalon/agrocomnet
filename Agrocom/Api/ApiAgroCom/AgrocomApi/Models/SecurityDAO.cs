﻿using System.Data;
using System.Data.Common;
using System.Net;
using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;


namespace AgrocomApi.Models
{
    public class SecurityDAO
    {
        private readonly ApplicationDbContext context;

        public SecurityDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public int InsertToken(int UserID, string Token)
        {
            
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [audit].[sp_Security_InsertRequestAuthorization] @PuserID, @Ptoken";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = UserID;
                    command.Parameters.Add(PuserID);

                    DbParameter Ptoken = command.CreateParameter();
                    Ptoken.ParameterName = "@Ptoken";
                    Ptoken.Value = Token;
                    command.Parameters.Add(Ptoken);

                    context.Database.OpenConnection();
                    var result = command.ExecuteNonQuery();
                    return result;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    //if (result != null && !result.IsClosed) result.Close();
                    if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {
                    

                }
            }
        }

    }
}
