﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;

namespace AgrocomApi.Models
{
    public class ShippingStatusDAO
    {
        private readonly ApplicationDbContext context;
        public ShippingStatusDAO(ApplicationDbContext context)
        {
            this.context = context;
        }
        public DataTable GetListWeekBySeason(int WIni, int WFin)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_OrderProductionDetailByDate] @PINI, @PFIN";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PINI";
                    PsearchOpt.Value = WIni;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PFIN";
                    PsearchValue.Value = WFin;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetShipmentStPOByWeek(string searchOpt, int idUser, int WIni, int WFin)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_ShipmentStatusProductionOrder] @PsearchOpt, @PsearchValue, @Pini, @Pfin";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = searchOpt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = idUser;
                    command.Parameters.Add(PsearchValue);

                    DbParameter Pini = command.CreateParameter();
                    Pini.ParameterName = "@Pini";
                    Pini.Value = WIni;
                    command.Parameters.Add(Pini);

                    DbParameter Pfin = command.CreateParameter();
                    Pfin.ParameterName = "@Pfin";
                    Pfin.Value = WFin;
                    command.Parameters.Add(Pfin);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetDocumentsShipmentStatus(string type, string tbl, string description)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_paramater] @Ptipo, @Ptbl, @Pdescription";

                    DbParameter Ptipo = command.CreateParameter();
                    Ptipo.ParameterName = "@Ptipo";
                    Ptipo.Value = type;
                    command.Parameters.Add(Ptipo);

                    DbParameter Ptbl = command.CreateParameter();
                    Ptbl.ParameterName = "@Ptbl";
                    Ptbl.Value = tbl;
                    command.Parameters.Add(Ptbl);

                    DbParameter Pdescription = command.CreateParameter();
                    Pdescription.ParameterName = "@Pdescription";
                    Pdescription.Value = description;
                    command.Parameters.Add(Pdescription);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable GetLoadPODocsByID(string type, string typeID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_PODocuments] @Ptipo, @Pid, @PtypeDocId, @PnameFile, @PstatusId, @PpoStatusId";

                    DbParameter Ptipo = command.CreateParameter();
                    Ptipo.ParameterName = "@Ptipo";
                    Ptipo.Value = type;
                    command.Parameters.Add(Ptipo);

                    DbParameter PtypeID = command.CreateParameter();
                    PtypeID.ParameterName = "@Pid";
                    PtypeID.Value = typeID;
                    command.Parameters.Add(PtypeID);

                    DbParameter PtypeDocId = command.CreateParameter();
                    PtypeDocId.ParameterName = "@PtypeDocId";
                    PtypeDocId.Value = 0;
                    command.Parameters.Add(PtypeDocId);

                    DbParameter PnameFile = command.CreateParameter();
                    PnameFile.ParameterName = "@PnameFile";
                    PnameFile.Value = "";
                    command.Parameters.Add(PnameFile);

                    DbParameter PstatusId = command.CreateParameter();
                    PstatusId.ParameterName = "@PstatusId";
                    PstatusId.Value = 0;
                    command.Parameters.Add(PstatusId);

                    DbParameter PpoStatusId = command.CreateParameter();
                    PpoStatusId.ParameterName = "@PpoStatusId";
                    PpoStatusId.Value = 0;
                    command.Parameters.Add(PpoStatusId);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable DeletePODocument(string tipo, string id, string typeDocId)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_PODocuments]  @Ptipo, @Pid, @PtypeDocId, @PnameFile, @PstatusId, @PpoStatusId";

                    DbParameter Ptipo = command.CreateParameter();
                    Ptipo.ParameterName = "@Ptipo";
                    Ptipo.Value = tipo;
                    command.Parameters.Add(Ptipo);

                    DbParameter PpoID = command.CreateParameter();
                    PpoID.ParameterName = "@Pid";
                    PpoID.Value = id;
                    command.Parameters.Add(PpoID);

                    DbParameter PtypeDocId = command.CreateParameter();
                    PtypeDocId.ParameterName = "@PtypeDocId";
                    PtypeDocId.Value = typeDocId;
                    command.Parameters.Add(PtypeDocId);


                    DbParameter PnameFile = command.CreateParameter();
                    PnameFile.ParameterName = "@PnameFile";
                    PnameFile.Value = "";
                    command.Parameters.Add(PnameFile);

                    DbParameter PstatusId = command.CreateParameter();
                    PstatusId.ParameterName = "@PstatusId";
                    PstatusId.Value = 0;
                    command.Parameters.Add(PstatusId);

                    DbParameter PpoStatusId = command.CreateParameter();
                    PpoStatusId.ParameterName = "@PpoStatusId";
                    PpoStatusId.Value = 0;
                    command.Parameters.Add(PpoStatusId);


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable InserNewPODocument(string tipo, string id, int typeDocId, string nameFile, int statusId)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_PODocuments]@Ptipo, @Pid, @PtypeDocId, @PnameFile, @PstatusId, @PpoStatusId";

                    DbParameter Ptipo = command.CreateParameter();
                    Ptipo.ParameterName = "@Ptipo";
                    Ptipo.Value = tipo;
                    command.Parameters.Add(Ptipo);

                    DbParameter PpoID = command.CreateParameter();
                    PpoID.ParameterName = "@Pid";
                    PpoID.Value = id;
                    command.Parameters.Add(PpoID);

                    DbParameter PtypeDocId = command.CreateParameter();
                    PtypeDocId.ParameterName = "@PtypeDocId";
                    PtypeDocId.Value = typeDocId;
                    command.Parameters.Add(PtypeDocId);

                    DbParameter PnameFile = command.CreateParameter();
                    PnameFile.ParameterName = "@PnameFile";
                    PnameFile.Value = nameFile;
                    command.Parameters.Add(PnameFile);

                    DbParameter PstatusId = command.CreateParameter();
                    PstatusId.ParameterName = "@PstatusId";
                    PstatusId.Value = statusId;
                    command.Parameters.Add(PstatusId);

                    DbParameter PpoStatusId = command.CreateParameter();
                    PpoStatusId.ParameterName = "@PpoStatusId";
                    PpoStatusId.Value = 0;
                    command.Parameters.Add(PpoStatusId);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable ConfirmPODocs(string tipo, int? id, int poStatusId)
        {
            id = id ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_PODocuments] @Ptipo, @Pid, @PpoStatusId,@PtypeDocId,@PnameFile,@PstatusId";

                    DbParameter Ptipo = command.CreateParameter();
                    Ptipo.ParameterName = "@Ptipo";
                    Ptipo.Value = tipo;
                    command.Parameters.Add(Ptipo);

                    DbParameter Pid = command.CreateParameter();
                    Pid.ParameterName = "@Pid";
                    Pid.Value = id;
                    command.Parameters.Add(Pid);

                    DbParameter PpoStatusId = command.CreateParameter();
                    PpoStatusId.ParameterName = "@PpoStatusId";
                    PpoStatusId.Value = poStatusId;
                    command.Parameters.Add(PpoStatusId);

                    DbParameter PtypeDocId = command.CreateParameter();
                    PtypeDocId.ParameterName = "@PtypeDocId";
                    PtypeDocId.Value = 0;
                    command.Parameters.Add(PtypeDocId);

                    DbParameter PnameFile = command.CreateParameter();
                    PnameFile.ParameterName = "@PnameFile";
                    PnameFile.Value = "";
                    command.Parameters.Add(PnameFile);

                    DbParameter PstatusId = command.CreateParameter();
                    PstatusId.ParameterName = "@PstatusId";
                    PstatusId.Value = poStatusId;
                    command.Parameters.Add(PstatusId);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetShippingComex(string opt, int orderPoductionID, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingComexList] @PsearchOpt, @PorderProductionID, @PcropID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PorderPoductionID = command.CreateParameter();
                    PorderPoductionID.ParameterName = "@PorderProductionID";
                    PorderPoductionID.Value = orderPoductionID;
                    command.Parameters.Add(PorderPoductionID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetSalesOrdersWithRequestLoading(string campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [shipping].[sp_LoadingInstruction_RequestLoadingList] @PcampaignID";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);


                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public ENShippingLoading GetDetailRequestLoading(int orderPoductionID, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [shipping].[sp_LoadingInstruction_DetailRequestLoadingList] @PorderProductionID, @PcropID";

                    DbParameter PorderPoductionID = command.CreateParameter();
                    PorderPoductionID.ParameterName = "@PorderProductionID";
                    PorderPoductionID.Value = orderPoductionID;
                    command.Parameters.Add(PorderPoductionID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENShippingLoading o = new ENShippingLoading();
                    o.details = new List<ENShippingLoadingDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        o.shippingLoadingID = int.Parse(row["shippingLoadingID"].ToString());
                        o.orderProductionID = int.Parse(row["orderProductionID"].ToString());
                        o.managerID = int.Parse(row["managerID"].ToString());
                        o.manager = row["manager"].ToString();
                        o.logisticOperatorID = int.Parse(row["logisticOperatorID"].ToString());
                        o.logisticOperator = row["logisticOperator"].ToString();
                        o.nroPackingListID = int.Parse(row["nroPackingListID"].ToString());
                        o.abbreviation = row["abbreviation"].ToString();
                        o.abbreviationPacking = row["abbreviationPacking"].ToString();
                        o.nroPackingList = row["nroPackingList"].ToString();
                        o.dateCreated = row["dateCreated"].ToString();
                        o.contact = row["contact"].ToString();
                        o.terminalID = int.Parse(row["terminalID"].ToString());
                        o.terminal = row["terminal"].ToString();
                        o.shippingCompanyID = int.Parse(row["shippingCompanyID"].ToString());
                        o.shippingCompany = row["shippingCompany"].ToString();
                        o.orderProduction = row["orderProduction"].ToString();
                        o.booking = row["booking"].ToString();
                        o.veseel = row["veseel"].ToString();
                        o.freightCondition = row["freightCondition"].ToString();
                        o.regime = row["regime"].ToString();
                        o.tariffHeadingID = row["tariffHeadingID"].ToString();
                        o.tariffHeading = row["tariffHeading"].ToString();
                        o.originDestinationID = int.Parse(row["originDestinationID"].ToString());
                        o.originDestination = row["originDestination"].ToString();
                        o.arriveDestinationID = int.Parse(row["arriveDestinationID"].ToString());
                        o.arriveDestination = row["arriveDestination"].ToString();
                        o.etd = row["etd"].ToString();
                        o.eta = row["eta"].ToString();
                        o.shipperID = int.Parse(row["shipperID"].ToString());
                        o.shipper = row["shipper"].ToString();
                        o.customerID = int.Parse(row["customerID"].ToString());
                        o.customer = row["customer"].ToString();
                        o.consigneeID = int.Parse(row["consigneeID"].ToString());
                        o.consignee = row["consignee"].ToString();
                        o.notifyID = int.Parse(row["notifyID"].ToString());
                        o.notify = row["notify"].ToString();
                        o.processPlantID = row["processPlantID"].ToString();
                        o.processPlant = row["processPlant"].ToString();
                        o.vgm = decimal.Parse(row["vgm"].ToString());
                        o.datetoPlantEntry = row["datetoPlantEntry"].ToString();
                        o.departureDate = row["departureDate"].ToString();
                        o.arrivalTime = row["arrivalTime"].ToString();
                        o.terminalEntryDate = row["terminalEntryDate"].ToString();

                        o.certificates = int.Parse(row["certificates"].ToString());
                        o.coldTreatment = int.Parse(row["coldTreatment"].ToString());
                        o.controlledAtmosphere = int.Parse(row["controlledAtmosphere"].ToString());
                        o.temperature = decimal.Parse(row["temperature"].ToString());
                        o.ventilation = row["ventilation"].ToString();
                        o.humedity = bool.Parse(row["humedity"].ToString());
                        o.quest = bool.Parse(row["quest"].ToString());
                        o.comments = row["comments"].ToString();
                        o.shippingLoadingBLId = int.Parse(row["shippingLoadingBLId"].ToString());
                        o.bl= row["bl"].ToString();

                        o.initial = row["initial"].ToString();
                        o.final = row["final"].ToString();

                        var det = new ENShippingLoadingDetail();
                        det.shippingLoadingDetailID = int.Parse(row["shippingLoadingDetailID"].ToString());
                        det.shippingLoadingID = int.Parse(row["shippingLoadingID"].ToString());
                        det.categoryID = row["categoryID"].ToString();
                        det.category = row["category"].ToString();
                        det.varietyID = row["varietyID"].ToString();
                        det.variety = row["variety"].ToString();
                        det.brandID = int.Parse(row["brandID"].ToString());
                        det.brand = row["brand"].ToString();
                        det.formatID = int.Parse(row["formatID"].ToString());
                        det.format = row["format"].ToString();
                        det.packageProductID = int.Parse(row["packageProductID"].ToString());
                        det.packageProduct = row["packageProduct"].ToString();
                        det.presentationID = int.Parse(row["presentationID"].ToString());
                        det.presentation = row["presentation"].ToString();
                        det.labelID = int.Parse(row["labelID"].ToString());
                        det.label = row["label"].ToString();
                        det.codePackID = int.Parse(row["codePackID"].ToString());
                        det.codePack = row["codePack"].ToString();
                        det.sizeID = row["sizeID"].ToString();
                        det.size = row["size"].ToString();
                        det.codePack2 = row["codePack2"].ToString();
                        det.quantity = decimal.Parse(row["quantity"].ToString());
                        det.marketID = row["marketID"].ToString();
                        det.viaID = int.Parse(row["viaID"].ToString());
                        det.pallets = decimal.Parse(row["pallets"].ToString());
                        o.details.Add(det);

                    }
                    return o;

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable SaveShippingLoading(ENShippingLoading o)
        {
            o.ReplaceNull();
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    var sSP = string.Empty;
                    sSP = sSP + "exec [package].[sp_shippingLoadingCreate] ";
                    sSP = sSP + "@PshippingLoadingID,@PorderProductionID,@PmanagerID,@PlogisticOperatorID,@PabbreviationPacking,";
                    sSP = sSP + "@Pcontact,@PterminalID,@PshippingCompanyID,@Pbooking,@Pveseel,@PfreightCondition,@Pregime,@PtariffHeadingID,";
                    sSP = sSP + "@PoriginDestinationID,@ParriveDestinationID,@Petd,@Peta,@Pcertificates,@PshipperID,@PcustomerID,@PconsigneeID,";
                    sSP = sSP + "@PnotifyID,@PprocessPlantID,@Pvgm,@PcoldTreatment,@PcontrolledAtmosphere,@Ptemperature,@Pventilation,";
                    sSP = sSP + "@Phumedity,@Pquest,@PdatetoPlantEntry,@PdepartureDate,@ParrivalTime,@PterminalEntryDate,@Pcomments,";
                    sSP = sSP + "@PcampaignID,@PnroPackingListID,@PshippingLoadingBLId,@PshippingDetail_table";

                    command.CommandText = sSP;
                    DbParameter PshippingLoadingID = command.CreateParameter();
                    PshippingLoadingID.ParameterName = "@PshippingLoadingID";
                    PshippingLoadingID.Value = o.shippingLoadingID;
                    command.Parameters.Add(PshippingLoadingID);

                    DbParameter PorderProductionID = command.CreateParameter();
                    PorderProductionID.ParameterName = "@PorderProductionID";
                    PorderProductionID.Value = o.orderProductionID;
                    command.Parameters.Add(PorderProductionID);

                    DbParameter PmanagerID = command.CreateParameter();
                    PmanagerID.ParameterName = "@PmanagerID";
                    PmanagerID.Value = o.managerID;
                    command.Parameters.Add(PmanagerID);

                    DbParameter PlogisticOperatorID = command.CreateParameter();
                    PlogisticOperatorID.ParameterName = "@PlogisticOperatorID";
                    PlogisticOperatorID.Value = o.logisticOperatorID;
                    command.Parameters.Add(PlogisticOperatorID);

                    DbParameter PabbreviationPacking = command.CreateParameter();
                    PabbreviationPacking.ParameterName = "@PabbreviationPacking";
                    PabbreviationPacking.Value = o.abbreviationPacking;
                    command.Parameters.Add(PabbreviationPacking);

                    DbParameter Pcontact = command.CreateParameter();
                    Pcontact.ParameterName = "@Pcontact";
                    Pcontact.Value = o.contact;
                    command.Parameters.Add(Pcontact);

                    DbParameter PterminalID = command.CreateParameter();
                    PterminalID.ParameterName = "@PterminalID";
                    PterminalID.Value = o.terminalID;
                    command.Parameters.Add(PterminalID);

                    DbParameter PshippingCompanyID = command.CreateParameter();
                    PshippingCompanyID.ParameterName = "@PshippingCompanyID";
                    PshippingCompanyID.Value = o.shippingCompanyID;
                    command.Parameters.Add(PshippingCompanyID);

                    DbParameter Pbooking = command.CreateParameter();
                    Pbooking.ParameterName = "@Pbooking";
                    Pbooking.Value = o.booking;
                    command.Parameters.Add(Pbooking);

                    DbParameter Pveseel = command.CreateParameter();
                    Pveseel.ParameterName = "@Pveseel";
                    Pveseel.Value = o.veseel;
                    command.Parameters.Add(Pveseel);

                    DbParameter PfreightCondition = command.CreateParameter();
                    PfreightCondition.ParameterName = "@PfreightCondition";
                    PfreightCondition.Value = o.freightCondition;
                    command.Parameters.Add(PfreightCondition);

                    DbParameter Pregime = command.CreateParameter();
                    Pregime.ParameterName = "@Pregime";
                    Pregime.Value = o.regime;
                    command.Parameters.Add(Pregime);

                    DbParameter PtariffHeadingID = command.CreateParameter();
                    PtariffHeadingID.ParameterName = "@PtariffHeadingID";
                    PtariffHeadingID.Value = o.tariffHeadingID;
                    command.Parameters.Add(PtariffHeadingID);

                    DbParameter PoriginDestinationID = command.CreateParameter();
                    PoriginDestinationID.ParameterName = "@PoriginDestinationID";
                    PoriginDestinationID.Value = o.originDestinationID;
                    command.Parameters.Add(PoriginDestinationID);

                    DbParameter ParriveDestinationID = command.CreateParameter();
                    ParriveDestinationID.ParameterName = "@ParriveDestinationID";
                    ParriveDestinationID.Value = o.arriveDestinationID;
                    command.Parameters.Add(ParriveDestinationID);

                    DbParameter Petd = command.CreateParameter();
                    Petd.ParameterName = "@Petd";
                    Petd.Value = o.etd;
                    command.Parameters.Add(Petd);

                    DbParameter Peta = command.CreateParameter();
                    Peta.ParameterName = "@Peta";
                    Peta.Value = o.eta;
                    command.Parameters.Add(Peta);

                    DbParameter Pcertificates = command.CreateParameter();
                    Pcertificates.ParameterName = "@Pcertificates";
                    Pcertificates.Value = o.certificates;
                    command.Parameters.Add(Pcertificates);

                    DbParameter PshipperID = command.CreateParameter();
                    PshipperID.ParameterName = "@PshipperID";
                    PshipperID.Value = o.shipperID;
                    command.Parameters.Add(PshipperID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = o.customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PconsigneeID = command.CreateParameter();
                    PconsigneeID.ParameterName = "@PconsigneeID";
                    PconsigneeID.Value = o.consigneeID;
                    command.Parameters.Add(PconsigneeID);

                    DbParameter PnotifyID = command.CreateParameter();
                    PnotifyID.ParameterName = "@PnotifyID";
                    PnotifyID.Value = o.notifyID;
                    command.Parameters.Add(PnotifyID);

                    DbParameter PprocessPlantID = command.CreateParameter();
                    PprocessPlantID.ParameterName = "@PprocessPlantID";
                    PprocessPlantID.Value = o.processPlantID;
                    command.Parameters.Add(PprocessPlantID);

                    DbParameter Pvgm = command.CreateParameter();
                    Pvgm.ParameterName = "@Pvgm";
                    Pvgm.Value = o.vgm;
                    command.Parameters.Add(Pvgm);

                    DbParameter PcoldTreatment = command.CreateParameter();
                    PcoldTreatment.ParameterName = "@PcoldTreatment";
                    PcoldTreatment.Value = o.coldTreatment;
                    command.Parameters.Add(PcoldTreatment);

                    DbParameter PcontrolledAtmosphere = command.CreateParameter();
                    PcontrolledAtmosphere.ParameterName = "@PcontrolledAtmosphere";
                    PcontrolledAtmosphere.Value = o.controlledAtmosphere;
                    command.Parameters.Add(PcontrolledAtmosphere);

                    DbParameter Ptemperature = command.CreateParameter();
                    Ptemperature.ParameterName = "@Ptemperature";
                    Ptemperature.Value = o.temperature;
                    command.Parameters.Add(Ptemperature);

                    DbParameter Pventilation = command.CreateParameter();
                    Pventilation.ParameterName = "@Pventilation";
                    Pventilation.Value = o.ventilation;
                    command.Parameters.Add(Pventilation);

                    DbParameter Phumedity = command.CreateParameter();
                    Phumedity.ParameterName = "@Phumedity";
                    Phumedity.Value = o.humedity;
                    command.Parameters.Add(Phumedity);

                    DbParameter Pquest = command.CreateParameter();
                    Pquest.ParameterName = "@Pquest";
                    Pquest.Value = o.quest;
                    command.Parameters.Add(Pquest);

                    DbParameter PdatetoPlantEntry = command.CreateParameter();
                    PdatetoPlantEntry.ParameterName = "@PdatetoPlantEntry";
                    PdatetoPlantEntry.Value = o.datetoPlantEntry;
                    command.Parameters.Add(PdatetoPlantEntry);

                    DbParameter PdepartureDate = command.CreateParameter();
                    PdepartureDate.ParameterName = "@PdepartureDate";
                    PdepartureDate.Value = o.departureDate;
                    command.Parameters.Add(PdepartureDate);

                    DbParameter ParrivalTime = command.CreateParameter();
                    ParrivalTime.ParameterName = "@ParrivalTime";
                    ParrivalTime.Value = o.arrivalTime;
                    command.Parameters.Add(ParrivalTime);

                    DbParameter PterminalEntryDate = command.CreateParameter();
                    PterminalEntryDate.ParameterName = "@PterminalEntryDate";
                    PterminalEntryDate.Value = o.terminalEntryDate;
                    command.Parameters.Add(PterminalEntryDate);

                    DbParameter Pcomments = command.CreateParameter();
                    Pcomments.ParameterName = "@Pcomments";
                    Pcomments.Value = o.comments;
                    command.Parameters.Add(Pcomments);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = o.campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PnroPackingListID = command.CreateParameter();
                    PnroPackingListID.ParameterName = "@PnroPackingListID";
                    PnroPackingListID.Value = o.nroPackingListID;
                    command.Parameters.Add(PnroPackingListID);


                    DbParameter PshippingLoadingBLId = command.CreateParameter();
                    PshippingLoadingBLId.ParameterName = "@PshippingLoadingBLId";
                    PshippingLoadingBLId.Value = o.shippingLoadingBLId;
                    command.Parameters.Add(PshippingLoadingBLId);

                    var tbl = new DataTable();
                    tbl.Columns.Add("shippingLoadingDetailID");
                    tbl.Columns.Add("shippingLoadingID");
                    tbl.Columns.Add("categoryID");
                    tbl.Columns.Add("varietyID");
                    tbl.Columns.Add("brandID");
                    tbl.Columns.Add("formatID");
                    tbl.Columns.Add("packageProductID");
                    tbl.Columns.Add("presentationID");
                    tbl.Columns.Add("labelID");
                    tbl.Columns.Add("codePackID");
                    tbl.Columns.Add("sizeID");
                    tbl.Columns.Add("codePack2");
                    tbl.Columns.Add("quantity");
                    tbl.Columns.Add("statusID");

                    foreach (var item in o.details)
                    {
                        tbl.Rows.Add(item.shippingLoadingDetailID, item.shippingLoadingID, item.categoryID, item.varietyID, item.brandID, item.formatID,
                        item.packageProductID, item.presentationID, item.labelID, item.codePackID, item.sizeID, item.codePack2, item.quantity, item.statusID);
                    }

                    var PshippingDetail_table = new SqlParameter("@PshippingDetail_table", SqlDbType.Structured);
                    PshippingDetail_table.Value = tbl;
                    PshippingDetail_table.TypeName = "[shipping].[shippingLoadingDetail]";
                    command.Parameters.Add(PshippingDetail_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public ENShippingPDF GetShippingPDF(int shippingLoadingID, string cropID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingByIDPDF] @PshippingLoadingID, @PcropID";

                    DbParameter PshippingLoadingID = command.CreateParameter();
                    PshippingLoadingID.ParameterName = "@PshippingLoadingID";
                    PshippingLoadingID.Value = shippingLoadingID;
                    command.Parameters.Add(PshippingLoadingID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);

                    ENShippingPDF o = new ENShippingPDF();
                    o.details = new List<ENShippingPDFDetail>();
                    foreach (DataRow row in dataTable.Rows)
                    {

                        o.manager = row["manager"].ToString();
                        o.logisticOperator = row["logisticOperator"].ToString();
                        o.terminal = row["terminal"].ToString();
                        o.shippingLine = row["shippingLine"].ToString();
                        o.nroPackingList = row["nroPackingList"].ToString();
                        o.orderProduction = row["orderProduction"].ToString();
                        o.date = row["date"].ToString();
                        o.contact = row["contact"].ToString();
                        o.booking = row["booking"].ToString();
                        o.shipper = row["shipper"].ToString();
                        o.shipperaddress = row["shipperaddress"].ToString();
                        o.shipperphone = row["shipperphone"].ToString();
                        o.shipperemail = row["shipperemail"].ToString();
                        o.shippercontact = row["shippercontact"].ToString();
                        o.customer = row["customer"].ToString();
                        o.customeraddress = row["customeraddress"].ToString();
                        o.customerphone = row["customerphone"].ToString();
                        o.customeremail = row["customeremail"].ToString();
                        o.customercontact = row["customercontact"].ToString();
                        o.customerUSCI = row["customerUSCI"].ToString();
                        o.consignee = row["consignee"].ToString();
                        o.consigneeaddress = row["consigneeaddress"].ToString();
                        o.consigneephone = row["consigneephone"].ToString();
                        o.consigneeemail = row["consigneeemail"].ToString();
                        o.consigneecontact = row["consigneecontact"].ToString();
                        o.consigneeUSCI = row["consigneeUSCI"].ToString();
                        o.notify = row["notify"].ToString();
                        o.notifyaddress = row["notifyaddress"].ToString();
                        o.notifyphone = row["notifyphone"].ToString();
                        o.notifyemail = row["notifyemail"].ToString();
                        o.notifycontact = row["notifycontact"].ToString();
                        o.notifyUSCI = row["notifyUSCI"].ToString();
                        o.veseel = row["veseel"].ToString();
                        o.regime = row["regime"].ToString();
                        o.freightCondition = row["freightCondition"].ToString();
                        o.tariffHeading = row["tariffHeading"].ToString();
                        o.portShipping = row["portShipping"].ToString();
                        o.portDestination = row["portDestination"].ToString();
                        o.etd = row["etd"].ToString();
                        o.eta = row["eta"].ToString();

                        o.certificates = int.Parse(row["certificates"].ToString());
                        o.datetoPlantEntry = row["datetoPlantEntry"].ToString();
                        o.departureDate = row["departureDate"].ToString();
                        o.arrivalTime = row["arrivalTime"].ToString();
                        o.terminalEntryDate = row["terminalEntryDate"].ToString();
                        o.comments = row["comments"].ToString();
                        o.bl = row["bl"].ToString();

                        o.temperature = decimal.Parse(row["temperature"].ToString());
                        o.ventilation = row["ventilation"].ToString();
                        o.humedity = bool.Parse(row["humedity"].ToString());
                        o.quest = bool.Parse(row["quest"].ToString());
                        o.coldTreatment = int.Parse(row["coldTreatment"].ToString());
                        o.controlledAtmosphere = int.Parse(row["controlledAtmosphere"].ToString());

                        o.processPlant = row["processPlant"].ToString();
                        o.processPlantaddress = row["processPlantaddress"].ToString();

                        var det = new ENShippingPDFDetail();
                        det.variety = row["variety"].ToString();
                        det.brand = row["brand"].ToString();
                        det.codePack = row["codePack"].ToString();
                        det.quantity = decimal.Parse(row["quantity"].ToString());
                        o.details.Add(det);

                    }
                    return o;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetShippinglist(string opt, int campaignID, int customerID, int destinationID, string nroPackingList, string managerID, int logisticOperatorID, int processPlantID, int projectedWeekID, int viaID)
        {
            nroPackingList = nroPackingList ?? "";
            managerID = managerID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingComexTrayList] @PsearchOpt, @PcampaignID, @PcustomerID, @PdestinationID, @PnroPackingList, @PmanagerID, @PlogisticOperatorID, @PprocessPlantID, @PprojectedWeekID, @PviaID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PcustomerID = command.CreateParameter();
                    PcustomerID.ParameterName = "@PcustomerID";
                    PcustomerID.Value = customerID;
                    command.Parameters.Add(PcustomerID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PnroPackingList = command.CreateParameter();
                    PnroPackingList.ParameterName = "@PnroPackingList";
                    PnroPackingList.Value = nroPackingList;
                    command.Parameters.Add(PnroPackingList);

                    DbParameter PmanagerID = command.CreateParameter();
                    PmanagerID.ParameterName = "@PmanagerID";
                    PmanagerID.Value = managerID;
                    command.Parameters.Add(PmanagerID);

                    DbParameter PlogisticOperatorID = command.CreateParameter();
                    PlogisticOperatorID.ParameterName = "@PlogisticOperatorID";
                    PlogisticOperatorID.Value = logisticOperatorID;
                    command.Parameters.Add(PlogisticOperatorID);

                    DbParameter PprocessPlantID = command.CreateParameter();
                    PprocessPlantID.ParameterName = "@PprocessPlantID";
                    PprocessPlantID.Value = processPlantID;
                    command.Parameters.Add(PprocessPlantID);

                    DbParameter PprojectedWeekID = command.CreateParameter();
                    PprojectedWeekID.ParameterName = "@PprojectedWeekID";
                    PprojectedWeekID.Value = projectedWeekID;
                    command.Parameters.Add(PprojectedWeekID);

                    DbParameter PviaID = command.CreateParameter();
                    PviaID.ParameterName = "@PviaID";
                    PviaID.Value = viaID;
                    command.Parameters.Add(PviaID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetShippingParameter(string cropID, int destinationID, int coldTreatment)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_shippingParameters] @PcropID, @PdestinationID, @PcoldTreatment";

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PdestinationID = command.CreateParameter();
                    PdestinationID.ParameterName = "@PdestinationID";
                    PdestinationID.Value = destinationID;
                    command.Parameters.Add(PdestinationID);

                    DbParameter PcoldTreatment = command.CreateParameter();
                    PcoldTreatment.ParameterName = "@PcoldTreatment";
                    PcoldTreatment.Value = coldTreatment;
                    command.Parameters.Add(PcoldTreatment);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetShippingLoadingBL()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [shipping].[sp_LoadingInstruction_ListShippingLoadingBL]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetDateUpdate(string opt, int id, string data1, string data2, string data3, string data4, string data5, string data6, string data7)
        {
            data1 = data1 ?? "";
            data2 = data2 ?? "";
            data3 = data3 ?? "";
            data4 = data4 ?? "";
            data5 = data5 ?? "";
            data6 = data6 ?? "";
            data7 = data7 ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_packingDataUpdate] @PsearchOpt, @Pid, @Pdata1, @Pdata2, @Pdata3, @Pdata4, @Pdata5, @Pdata6, @Pdata7";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter Pid = command.CreateParameter();
                    Pid.ParameterName = "@Pid";
                    Pid.Value = id;
                    command.Parameters.Add(Pid);

                    DbParameter Pdata1 = command.CreateParameter();
                    Pdata1.ParameterName = "@Pdata1";
                    Pdata1.Value = data1;
                    command.Parameters.Add(Pdata1);

                    DbParameter Pdata2 = command.CreateParameter();
                    Pdata2.ParameterName = "@Pdata2";
                    Pdata2.Value = data2;
                    command.Parameters.Add(Pdata2);

                    DbParameter Pdata3 = command.CreateParameter();
                    Pdata3.ParameterName = "@Pdata3";
                    Pdata3.Value = data3;
                    command.Parameters.Add(Pdata3);

                    DbParameter Pdata4 = command.CreateParameter();
                    Pdata4.ParameterName = "@Pdata4";
                    Pdata4.Value = data4;
                    command.Parameters.Add(Pdata4);

                    DbParameter Pdata5 = command.CreateParameter();
                    Pdata5.ParameterName = "@Pdata5";
                    Pdata5.Value = data5;
                    command.Parameters.Add(Pdata5);

                    DbParameter Pdata6 = command.CreateParameter();
                    Pdata6.ParameterName = "@Pdata6";
                    Pdata6.Value = data6;
                    command.Parameters.Add(Pdata6);

                    DbParameter Pdata7 = command.CreateParameter();
                    Pdata7.ParameterName = "@Pdata7";
                    Pdata7.Value = data7;
                    command.Parameters.Add(Pdata7);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}
