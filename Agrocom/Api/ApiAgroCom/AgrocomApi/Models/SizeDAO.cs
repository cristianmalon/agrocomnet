﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using System.Net;

namespace AgrocomApi.Models
{
    public class SizeDAO
    {
        private readonly ApplicationDbContext context;

        public SizeDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetSize(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_siseList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
 
        public DataTable ListSizeByFilter(string cropID, string growerID, int? lotID)
        {
            lotID = lotID ?? 0;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_Forecast_ListSizeByFilter] @PcropID,@PgrowerID,@PlotID";

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@PgrowerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PlotID = command.CreateParameter();
                    PlotID.ParameterName = "@PlotID";
                    PlotID.Value = lotID;
                    command.Parameters.Add(PlotID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
