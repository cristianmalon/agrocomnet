﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace AgrocomApi.Models
{
    public class SpecificationDAO
    {
        private readonly ApplicationDbContext context;
        public SpecificationDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetSpecification(string opt, string id)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_specificationList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetListByForecast(int idForecast)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_specificationPorProduccion] @PprojectedProductionSizeCategoryID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PprojectedProductionSizeCategoryID";
                    PsearchOpt.Value = idForecast;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetSpecificationDisable(int idSpecification)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_specificationDisable] @PspecificationID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PspecificationID";
                    PsearchOpt.Value = idSpecification;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetSpecificationCreate(int idSpecification,int idCustomer,int idDestionation,string miniumbrix,string acidity,string bloom,string traceability,string commentary,int usercreatedID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_specificationCreate] @PspecificationID,@PcustomerID,@PdestinationID,@PminimumBrix,@Pacidity,@Pbloom,@Ptraceability,@Pcommentary,@PuserCreated";

                    DbParameter Pspecification = command.CreateParameter();
                    Pspecification.ParameterName = "@PspecificationID";
                    Pspecification.Value = idSpecification;
                    command.Parameters.Add(Pspecification);

                    DbParameter Pcustomer = command.CreateParameter();
                    Pcustomer.ParameterName = "@PcustomerID";
                    Pcustomer.Value = idCustomer;
                    command.Parameters.Add(Pcustomer);

                    DbParameter Pdestination = command.CreateParameter();
                    Pdestination.ParameterName = "@PdestinationID";
                    Pdestination.Value = idDestionation;
                    command.Parameters.Add(Pdestination);

                    DbParameter Pminiumbrix = command.CreateParameter();
                    Pminiumbrix.ParameterName = "@PminimumBrix";
                    Pminiumbrix.Value = miniumbrix;
                    command.Parameters.Add(Pminiumbrix);

                    DbParameter Pacidity = command.CreateParameter();
                    Pacidity.ParameterName = "@Pacidity";
                    Pacidity.Value = acidity;
                    command.Parameters.Add(Pacidity);

                    DbParameter Pbloom = command.CreateParameter();
                    Pbloom.ParameterName = "@Pbloom";
                    Pbloom.Value = bloom;
                    command.Parameters.Add(Pbloom);

                    DbParameter Ptraceability = command.CreateParameter();
                    Ptraceability.ParameterName = "@Ptraceability";
                    Ptraceability.Value = traceability;
                    command.Parameters.Add(Ptraceability);

                    DbParameter Pcommentary = command.CreateParameter();
                    Pcommentary.ParameterName = "@Pcommentary";
                    Pcommentary.Value = commentary;
                    command.Parameters.Add(Pcommentary);

                    DbParameter PuserCreated = command.CreateParameter();
                    PuserCreated.ParameterName = "@PuserCreated";
                    PuserCreated.Value = usercreatedID;
                    command.Parameters.Add(PuserCreated);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetSpecsClientList(string opt, int campaignID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_specsCustomerCropList] @Popt,@PcampaignID";

                    DbParameter Popt = command.CreateParameter();
                    Popt.ParameterName = "@Popt";
                    Popt.Value = opt;
                    command.Parameters.Add(Popt);

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetClientSaveUpdate(int campaignID, string cropID, int userID, List<ClientSpecs> customers)
        {
            
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_clientSpecsCreate] @PcampaignID,@PcropID,@PuserID,@Pcustomer_table";

                    DbParameter PcampaignID = command.CreateParameter();
                    PcampaignID.ParameterName = "@PcampaignID";
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@PuserID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    var tbl = new DataTable();
                    tbl.Columns.Add("customerID");

                    foreach (var item in customers)
                    {
                        tbl.Rows.Add(item.customerID);
                    }

                    var Pcustomer_table = new SqlParameter("@Pcustomer_table", SqlDbType.Structured);
                    Pcustomer_table.Value = tbl;
                    Pcustomer_table.TypeName = "[specification].[customDestin]";
                    command.Parameters.Add(Pcustomer_table);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public bool SaveProductUpdate(int campaignID, int userID, string file, MassLoadProduct objMassiveUpload)
        {
            file = file ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "[package].[sp_specsCustomer_massiveupload]";
                    command.CommandType = CommandType.StoredProcedure;
                    //command.CommandTimeout = 30000000;

                    SqlParameter PcampaignID = new SqlParameter("@PcampaignID", SqlDbType.Int);
                    PcampaignID.Value = campaignID;
                    command.Parameters.Add(PcampaignID);

                    SqlParameter PuserID = new SqlParameter("@PuserID", SqlDbType.Int);
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    SqlParameter Pfile = new SqlParameter("@Pfile", SqlDbType.VarChar);
                    Pfile.Value = file;
                    command.Parameters.Add(Pfile);

                    SqlParameter PMassiveUpload = new SqlParameter("@Pmassive_Upload", SqlDbType.Structured);
                    command.Parameters.Add(PMassiveUpload);                    

                    DataTable dttSpecsProducts = new DataTable("MassiveUpload");

                    DataColumn dc = new DataColumn();
                    dc.ColumnName = "ID";
                    dc.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc);

                    DataColumn dc1 = new DataColumn();
                    dc1.ColumnName = "specsCustomerCropID";
                    dc1.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc1);

                    DataColumn dc2 = new DataColumn();
                    dc2.ColumnName = "productNetsuiteID";
                    dc2.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc2);

                    DataColumn dc3 = new DataColumn();
                    dc3.ColumnName = "price";
                    dc3.DataType = typeof(decimal);
                    dttSpecsProducts.Columns.Add(dc3);

                    DataColumn dc4 = new DataColumn();
                    dc4.ColumnName = "priority";
                    dc4.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc4);

                    DataColumn dc5 = new DataColumn();
                    dc5.ColumnName = "minimun";
                    dc5.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc5);

                    DataColumn dc6 = new DataColumn();
                    dc6.ColumnName = "maximun";
                    dc6.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc6);

                    DataColumn dc7 = new DataColumn();
                    dc7.ColumnName = "statusID";
                    dc7.DataType = typeof(int);
                    dttSpecsProducts.Columns.Add(dc7);                    

                    foreach (MassLoadProductDet o in objMassiveUpload.MassLoadProductDetail)
                    {
                        dttSpecsProducts.Rows.Add(new object[] {

                        o.ID, o.specsCustomerCropID, o.productNetsuiteID, o.price, o.priority, o.minimun,o.maximun,o.statusID

                    });
                    }

                    PMassiveUpload.Value = dttSpecsProducts;
                    context.Database.OpenConnection();
                    command.ExecuteNonQuery();
                    return bool.Parse(1.ToString());

                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}
