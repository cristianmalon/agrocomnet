﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;using System.Net;


namespace AgrocomApi.Models
{
    public class StageDAO
    {
        private readonly ApplicationDbContext context;

        public StageDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetStage(string opt, string growerid, string farmID)
        {
            farmID = farmID ?? string.Empty;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_stageList] @PsearchOpt, @PsearchValue,@growerID, @farmID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = "";
                    command.Parameters.Add(PsearchValue);


                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@growerID";
                    PgrowerID.Value = growerid;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@farmID";
                    PfarmID.Value = farmID;
                    command.Parameters.Add(PfarmID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable ListStageByFilter(string cropID, string growerID, string farmID)
        {
            farmID = farmID ?? string.Empty;
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_Forecast_ListStageByFilter] @cropID,@growerID, @farmID";

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@cropID";
                    PsearchValue.Value = cropID;
                    command.Parameters.Add(PsearchValue);


                    DbParameter PgrowerID = command.CreateParameter();
                    PgrowerID.ParameterName = "@growerID";
                    PgrowerID.Value = growerID;
                    command.Parameters.Add(PgrowerID);

                    DbParameter PfarmID = command.CreateParameter();
                    PfarmID.ParameterName = "@farmID";
                    PfarmID.Value = farmID;
                    command.Parameters.Add(PfarmID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
    }
}
