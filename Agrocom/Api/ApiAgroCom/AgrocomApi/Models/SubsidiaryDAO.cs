﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;

namespace AgrocomApi.Models
{
    public class SubsidiaryDAO
    {
        private readonly ApplicationDbContext context;

        public SubsidiaryDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetSubsidiary()
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_subsidiaryList]";

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}
