﻿using AgrocomApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;using System.Net;


namespace AgrocomApi.Models
{
    public class UsersDAO
    {
        private readonly ApplicationDbContext context;

        public UsersDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetUser(string opt, string val)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_userList] @PsearchOpt, @PsearchValue";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = val;
                    command.Parameters.Add(PsearchValue);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetAuthenticateUser(string usuario, string contrasenia)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_AuthenticateUser] @UsrAccount, @PwdAccount";

                    DbParameter PUsrAccount = command.CreateParameter();
                    PUsrAccount.ParameterName = "@UsrAccount";
                    PUsrAccount.Value = usuario;
                    command.Parameters.Add(PUsrAccount);

                    DbParameter PwdAccount = command.CreateParameter();
                    PwdAccount.ParameterName = "@PwdAccount";
                    PwdAccount.Value = contrasenia;
                    command.Parameters.Add(PwdAccount);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    if (result == null) return null;
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return dataTable;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable GetMenuByUserIDCropID(int userID)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_Menu_GetMenuByUserIDCropID] @userID";

                    DbParameter PuserID = command.CreateParameter();
                    PuserID.ParameterName = "@userID";
                    PuserID.Value = userID;
                    command.Parameters.Add(PuserID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    if (result == null) return null;
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return dataTable;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close(); if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }

        public DataTable InsertRequestAuthorization(string usuario, string contrasenia)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [audit].[sp_Security_RequestAuthorization] @UsrAccount, @PwdAccount";

                    DbParameter PUsrAccount = command.CreateParameter();
                    PUsrAccount.ParameterName = "@UsrAccount";
                    PUsrAccount.Value = usuario;
                    command.Parameters.Add(PUsrAccount);

                    DbParameter PwdAccount = command.CreateParameter();
                    PwdAccount.ParameterName = "@PwdAccount";
                    PwdAccount.Value = contrasenia;
                    command.Parameters.Add(PwdAccount);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    if (result == null) return null;
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return dataTable;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}
