﻿using AgrocomApi.Context;
using Common.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;using System.Net;

namespace AgrocomApi.Models
{
    public class WowDAO
    {
        private readonly ApplicationDbContext context;

        public WowDAO(ApplicationDbContext context)
        {
            this.context = context;
        }

        public DataTable GetWow(string opt, string id,string cropID)
        {
            id = id ?? "";
            cropID = cropID ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_wowList] @PsearchOpt, @PsearchValue, @PcropID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PsearchOpt";
                    PsearchOpt.Value = opt;
                    command.Parameters.Add(PsearchOpt);

                    DbParameter PsearchValue = command.CreateParameter();
                    PsearchValue.ParameterName = "@PsearchValue";
                    PsearchValue.Value = id;
                    command.Parameters.Add(PsearchValue);

                    DbParameter PcropID = command.CreateParameter();
                    PcropID.ParameterName = "@PcropID";
                    PcropID.Value = cropID;
                    command.Parameters.Add(PcropID);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }

        public DataTable GetWowDisable(int idWow)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_wowDisable] @PwowID";

                    DbParameter PsearchOpt = command.CreateParameter();
                    PsearchOpt.ParameterName = "@PwowID";
                    PsearchOpt.Value = idWow;
                    command.Parameters.Add(PsearchOpt);

                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable wowSave(ENWow o)
        {
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_wowCreate] @PwowID,@PcustomerID,@PdestinationID,@PdirectInvoice,@PdirectInvoiceMoreInfo,@PcertificationID" +
                    ",@Pdocuments,@PdocumentsMoreInfo,@PdocumentsInfo,@PpackingListID,@PpackingListMoreInfo,@PinsuranceCertificate" +
                    ",@Pphytosanitary,@PphytosanitaryMoreInfo,@PphytosanitaryInfo,@PcertificateOrigin,@PcertificateOriginMoreInfo" +
                    ",@PcertificateOriginInfo,@PmoreInfo,@PconsigneeID,@PconsigneeAddress,@PconsigneeContact" +
                    ",@PconsigneePhone,@PconsigneeOther,@PnotifyID,@PnotifyIDAddress,@PnotifyIDContact,@PnotifyIDPhone,@PnotifyIDOther" +
                    ",@PnotifyID2,@PnotifyID2Address,@PnotifyID2Contact,@PnotifyID2Phone,@PnotifyID2Other,@PnotifyID3" +
                    ",@PnotifyID3Address,@PnotifyID3Contact,@PnotifyID3Phone,@PnotifyID3Other,@PdocsCopyEmailInfo,@Pnoted" +

                    ",@PuserID,@PdocumentsExportSea,@PdocumentsExportAir,@PcropID";
                    o.replaceNull();
                    command.Parameters.Add(new SqlParameter("@PwowID", o.wowID));
                    command.Parameters.Add(new SqlParameter("@PcustomerID", o.customerID));
                    command.Parameters.Add(new SqlParameter("@PdestinationID", o.destinationID));
                    command.Parameters.Add(new SqlParameter("@PdirectInvoice", o.directInvoice));
                    command.Parameters.Add(new SqlParameter("@PdirectInvoiceMoreInfo", o.directInvoiceMoreInfo));
                    command.Parameters.Add(new SqlParameter("@PcertificationID", o.certificationID));

                    command.Parameters.Add(new SqlParameter("@Pdocuments", o.documents));
                    command.Parameters.Add(new SqlParameter("@PdocumentsMoreInfo", o.documentsMoreInfo));
                    command.Parameters.Add(new SqlParameter("@PdocumentsInfo", o.documentsInfo));
                    command.Parameters.Add(new SqlParameter("@PpackingListID", o.packingListID));
                    command.Parameters.Add(new SqlParameter("@PpackingListMoreInfo", o.packingListMoreInfo));
                    command.Parameters.Add(new SqlParameter("@PinsuranceCertificate", o.insuranceCertificate));

                    command.Parameters.Add(new SqlParameter("@Pphytosanitary", o.phytosanitary));
                    command.Parameters.Add(new SqlParameter("@PphytosanitaryMoreInfo", o.phytosanitaryMoreInfo));
                    command.Parameters.Add(new SqlParameter("@PphytosanitaryInfo", o.phytosanitaryInfo));
                    command.Parameters.Add(new SqlParameter("@PcertificateOrigin", o.certificateOrigin));
                    command.Parameters.Add(new SqlParameter("@PcertificateOriginMoreInfo", o.certificateOriginMoreInfo));

                    command.Parameters.Add(new SqlParameter("@PcertificateOriginInfo", o.certificateOriginInfo));
                    command.Parameters.Add(new SqlParameter("@PmoreInfo", o.moreInfo));
                    command.Parameters.Add(new SqlParameter("@PconsigneeID", o.consigneeID));
                    command.Parameters.Add(new SqlParameter("@PconsigneeAddress", o.consigneeAddress));
                    command.Parameters.Add(new SqlParameter("@PconsigneeContact", o.consigneeContact));
                    command.Parameters.Add(new SqlParameter("@PconsigneeOther", o.consigneeOther));


                    command.Parameters.Add(new SqlParameter("@PconsigneePhone", o.consigneePhone));
                    command.Parameters.Add(new SqlParameter("@PnotifyID", o.notifyID));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDAddress", o.notifyIDAddress));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDContact", o.notifyIDContact));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDPhone", o.notifyIDPhone));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDOther", o.notifyIDOther));

                    command.Parameters.Add(new SqlParameter("@PnotifyID2", o.notifyID2));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Address", o.notifyID2Address));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Contact", o.notifyID2Contact));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Phone", o.notifyID2Phone));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Other", o.notifyID2Other));

                    command.Parameters.Add(new SqlParameter("@PnotifyID3", o.notifyID3));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3Address", o.notifyID3Address));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3Contact", o.notifyID3Contact));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3Phone", o.notifyID3Phone));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3Other", o.notifyID3Other));

                    command.Parameters.Add(new SqlParameter("@PdocsCopyEmailInfo", o.docsCopyEmailInfo));
                    command.Parameters.Add(new SqlParameter("@Pnoted", o.noted));

                    command.Parameters.Add(new SqlParameter("@PuserID", o.userID));
                    command.Parameters.Add(new SqlParameter("@PdocumentsExportSea", o.documentsExportSea));
                    command.Parameters.Add(new SqlParameter("@PdocumentsExportAir", o.documentsExportAir));
                    command.Parameters.Add(new SqlParameter("@PcropID", o.cropID));


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }
        public DataTable WowCreate//(string wowID)
            (int wowID, int customerID, int destinationID, int directInvoice, string certificationID
        , int documents, int documentsMoreInfo, string documentsInfo, int packingListID, int insuranceCertificate
        , int phytosanitary, int phytosanitaryMoreInfo, string phytosanitaryInfo, int certificateOrigin, int certificateOriginMoreInfo
        , string certificateOriginInfo, string moreInfo, int consigneeID, string consigneeAddress, string consigneeContact
        , string consigneePhone, int notifyID, string notifyIDAddress, string notifyIDContact, string notifyIDPhone
        , int notifyID2, string notifyID2Address, string notifyID2Contact, string notifyID2Phone, int notifyID3
        , string notifyID3Address, string notifyID3Contact, string notifyID3Phone, string docsCopyEmailInfo, string noted
        , string price, string accountSale, string finalPaymentConditions, string advanceCondition, int userID
        , string documentsExportSea, string documentsExportAir)
        {
            documentsInfo = documentsInfo ?? "";
            phytosanitaryInfo = phytosanitaryInfo ?? "";

            moreInfo = moreInfo ?? "";
            consigneeAddress = consigneeAddress ?? "";
            consigneeContact = consigneeContact ?? "";
            consigneePhone = consigneePhone ?? "";
            notifyIDAddress = notifyIDAddress ?? "";
            notifyIDContact= notifyIDContact ?? "";
            notifyIDPhone = notifyIDPhone ?? "";
            notifyID2Address = notifyID2Address ?? "";
            notifyID2Contact = notifyID2Contact ?? "";
            notifyID2Phone = notifyID2Phone ?? "";
            notifyID3Address = notifyID3Address ?? "";
            notifyID3Contact = notifyID3Contact ?? "";
            notifyID3Phone = notifyID3Phone ?? "";
            certificationID = certificationID ?? "";

            certificateOriginInfo = certificateOriginInfo ?? "";

            docsCopyEmailInfo = docsCopyEmailInfo ?? "";
            noted = noted ?? "";
            price = price ?? "";
            accountSale = accountSale ?? "";
            finalPaymentConditions = finalPaymentConditions ?? "";
            advanceCondition = advanceCondition ?? "";

            documentsExportSea = documentsExportSea ?? "";
            documentsExportAir = documentsExportAir ?? "";
            DbDataReader result = null;
            try
            {
                using (var command = context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "exec [package].[sp_wowCreate] @PwowID,@PcustomerID,@PdestinationID,@PdirectInvoice,@PcertificationID" +
                    ",@Pdocuments,@PdocumentsMoreInfo,@PdocumentsInfo,@PpackingListID,@PinsuranceCertificate" +
                    ",@Pphytosanitary,@PphytosanitaryMoreInfo,@PphytosanitaryInfo,@PcertificateOrigin,@PcertificateOriginMoreInfo" +
                    ",@PcertificateOriginInfo,@PmoreInfo,@PconsigneeID,@PconsigneeAddress,@PconsigneeContact" +
                    ",@PconsigneePhone,@PnotifyID,@PnotifyIDAddress,@PnotifyIDContact,@PnotifyIDPhone" +
                    ",@PnotifyID2,@PnotifyID2Address,@PnotifyID2Contact,@PnotifyID2Phone,@PnotifyID3" +
                    ",@PnotifyID3Address,@PnotifyID3Contact,@PnotifyID3Phone,@PdocsCopyEmailInfo,@Pnoted" +
                    ",@Pprice,@PaccountSale,@PfinalPaymentConditions,@PadvanceCondition,@PuserID" +
                    ",@PdocumentsExportSea,@PdocumentsExportAir";

                    command.Parameters.Add(new SqlParameter("@PwowID", wowID));
                    command.Parameters.Add(new SqlParameter("@PcustomerID", customerID));
                    command.Parameters.Add(new SqlParameter("@PdestinationID", destinationID));
                    command.Parameters.Add(new SqlParameter("@PdirectInvoice", directInvoice));
                    command.Parameters.Add(new SqlParameter("@PcertificationID", certificationID));

                    command.Parameters.Add(new SqlParameter("@Pdocuments", documents));
                    command.Parameters.Add(new SqlParameter("@PdocumentsMoreInfo", documentsMoreInfo));
                    command.Parameters.Add(new SqlParameter("@PdocumentsInfo", documentsInfo));
                    command.Parameters.Add(new SqlParameter("@PpackingListID", packingListID));
                    command.Parameters.Add(new SqlParameter("@PinsuranceCertificate", insuranceCertificate));

                    command.Parameters.Add(new SqlParameter("@Pphytosanitary", phytosanitary));
                    command.Parameters.Add(new SqlParameter("@PphytosanitaryMoreInfo", phytosanitaryMoreInfo));
                    command.Parameters.Add(new SqlParameter("@PphytosanitaryInfo", phytosanitaryInfo));
                    command.Parameters.Add(new SqlParameter("@PcertificateOrigin", certificateOrigin));
                    command.Parameters.Add(new SqlParameter("@PcertificateOriginMoreInfo", certificateOriginMoreInfo));

                    command.Parameters.Add(new SqlParameter("@PcertificateOriginInfo", certificateOriginInfo));
                    command.Parameters.Add(new SqlParameter("@PmoreInfo", moreInfo));
                    command.Parameters.Add(new SqlParameter("@PconsigneeID", consigneeID));
                    command.Parameters.Add(new SqlParameter("@PconsigneeAddress", consigneeAddress));
                    command.Parameters.Add(new SqlParameter("@PconsigneeContact", consigneeContact));

                    command.Parameters.Add(new SqlParameter("@PconsigneePhone", consigneePhone));
                    command.Parameters.Add(new SqlParameter("@PnotifyID", notifyID));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDAddress", notifyIDAddress));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDContact", notifyIDContact));
                    command.Parameters.Add(new SqlParameter("@PnotifyIDPhone", notifyIDPhone));

                    command.Parameters.Add(new SqlParameter("@PnotifyID2", notifyID2));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Address", notifyID2Address));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Contact", notifyID2Contact));
                    command.Parameters.Add(new SqlParameter("@PnotifyID2Phone", notifyID2Phone));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3", notifyID3));

                    command.Parameters.Add(new SqlParameter("@PnotifyID3Address", notifyID3Address));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3Contact", notifyID3Contact));
                    command.Parameters.Add(new SqlParameter("@PnotifyID3Phone", notifyID3Phone));
                    command.Parameters.Add(new SqlParameter("@PdocsCopyEmailInfo", docsCopyEmailInfo));
                    command.Parameters.Add(new SqlParameter("@Pnoted", noted));

                    command.Parameters.Add(new SqlParameter("@Pprice", price));
                    command.Parameters.Add(new SqlParameter("@PaccountSale", accountSale));
                    command.Parameters.Add(new SqlParameter("@PfinalPaymentConditions", finalPaymentConditions));
                    command.Parameters.Add(new SqlParameter("@PadvanceCondition", advanceCondition));
                    command.Parameters.Add(new SqlParameter("@PuserID", userID));
                    command.Parameters.Add(new SqlParameter("@PdocumentsExportSea", documentsExportSea));
                    command.Parameters.Add(new SqlParameter("@PdocumentsExportAir", documentsExportAir));


                    context.Database.OpenConnection();
                    result = command.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(result);
                    return (dataTable);
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();if (context.Database != null && context.Database.GetDbConnection().State == ConnectionState.Open)
                    {
                        context.Database.CloseConnection();
                        
                    }
                }
                catch (System.Exception)
                {

                    
                }
            }
        }


    }
}
