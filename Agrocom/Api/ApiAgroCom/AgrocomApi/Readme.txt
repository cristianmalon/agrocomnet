﻿Serialization of objects
========================
1) The project uses .net core 3.1; therefore, the library is used: Newtonsoft.Json
2) In the file "startup.cs" is used: Dependency Injection (DI) referenced the dll:
Microsoft.Extensions.Configuration and making use of the IConfiguration interface;
with this, you can access the settings in the appsettings.json
3) In the same way, in this file: "startup.cs" the context is defined

Register dependency in service container to access configuration data
=====================================================================
Services are registered in the app's Startup.ConfigureServices method:
1)Add a reference to Class library dll to your ASP.NET MVC Core application.
2)Open Startup.cs file from ASP.NET Core or Console application and add the below code. This 
is required for class to become available as a configuration object.

Example:
public void ConfigureServices(IServiceCollection services)
{
    ...
    
    services.AddSingleton<IConfigManager, ConfigManager>();
}
where IConfigManager and ConfigManager belongs to Class library project and is used 
IConfiguration importing Microsoft.Extensions.Configuration dll 

As your configuration will rarely change in the production / development environment, we 
will use services.AddSingleton. This will create a single instance and reuse for each request 
where ever it is required. At start up of your application, you are telling your application 
to inject GeekConfigManager implementation where ever IConfigManager is used.

Async vs Sync
=============
I case of SYNC what happens is that for each request a thread is assigned exclusively 
and this thread is released only upon completion of particular request. 
While in case of ASYNC the thread may be reused by other request.
With asynchronous actions, as soon as a thread accepts an incoming request, it 
immediately passes it along to a background thread that will take care of it, releasing 
the main thread. This is very handy, because it will be available to accept other 
requests.
This is not related to performance, but instead scalability; using asynchronous actions 
allows your application to always be responsive, even if it is still processing requests 
in the background.

ActionFilterAttribute
=====================
To Inject filters at the Controller level (not globally) make sure you remove the line in Startup.cs.
Enable attribute features to the interceptor class. 
To do this derives your class from the ‘ActionFilterAttribute’ class.