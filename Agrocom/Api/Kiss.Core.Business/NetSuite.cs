﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Kiss.Core.Business
{
    public class NetSuite
    {

        public static string GetDataNetsuite(string url,
            string consumer_id, string consumer_secret, string token_id, string token_secret, string realm, int time_out = -1)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            RestSharp.IRestResponse response = null;

            var request = Kiss.Core.Business.Servicio.ApiRestRequest(Kiss.Core.Business.Servicio.Metodo.GET, url,
            consumer_id, consumer_secret, token_id, token_secret, realm);

            var client = new RestSharp.RestClient(url);
            client.Timeout = -1;

            var jSonBody = "";

            request.AddParameter("application/json", jSonBody, RestSharp.ParameterType.RequestBody);
            response = client.Execute(request);
            return response.Content;
        }

        public static async Task<string> PutDataNetsuiteAsync(string url,
            string consumer_id, string consumer_secret, string token_id, string token_secret, string realm,
            CancellationToken cancellationToken = default, string _jSonBody = "", int time_out = -1)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //RestSharp.IRestResponse response = null;

            var request = Kiss.Core.Business.Servicio.ApiRestRequest(Kiss.Core.Business.Servicio.Metodo.PUT, url,
                consumer_id, consumer_secret, token_id, token_secret, realm);

            var client = new RestSharp.RestClient(url);
            client.Timeout = time_out;

            var jSonBody = _jSonBody;
            //var jSonBody = @"{" + "\n" +
            //  @" ""isperson"": ""F""," + "\n" +
            //  @" ""firstname"": """"," + "\n" +
            //  @" ""lastname"": """"," + "\n" +
            //  @" ""companyname"": ""cliente 400""," + "\n" +
            //  @" ""externalid"": ""37""," + "\n" +
            //  @" ""email"": ""ernesto.20@gmail.com""," + "\n" +
            //  @" ""subsidiary"": ""3""," + "\n" +
            //  @" ""tipodocumento"": ""4""," + "\n" +
            //  @" ""nodocumento"": ""20494626897"" " + "\n" +
            //  @"}";

            request.AddParameter("application/json", jSonBody, RestSharp.ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request, cancellationToken);
            return response.Content;
        }
    }
}
