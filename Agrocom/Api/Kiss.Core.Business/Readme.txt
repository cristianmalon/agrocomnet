﻿CONSIDERACIONES:
================
1)En la demo, el proyecto cliente que consume la librería de clases: Kiss.Core.Business, es del tipo consola; por ello,
las urls y keys están un archivo de configuraciones.
2)En este caso, el proyecto cliente es una api; las urls y keys están en el archivo: appsettings.json y se lee usando:
Dependency Injection (DI); para ello, desde un proyecto del tipo "libreria de clases" se referencia la dll 
Microsoft.Extensions.Configuration y se hace uso de la interface IConfiguration.
3)El proyecto cliente usa .net core; por ello, se ha creado proyectos del tipo "librería de clases" .net core.