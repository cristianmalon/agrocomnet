﻿using RestSharp;
using System;
using System.Configuration;

namespace Kiss.Core.Business
{
    public class Servicio
    {

        /// <param name="operation">GET o POST según sea el caso</param>
        public static RestRequest ApiRestRequest(Metodo metodo)
        {
            var operation = "";


            var request = new RestRequest();

            switch (metodo)
            {
                case 0:
                    request = new RestRequest(Method.GET);
                    operation = "GET";
                    break;
                case (Metodo)1:
                    request = new RestRequest(Method.POST);
                    operation = "POST";
                    break;
            }

            try
            {
                String consumer_id = ConfigurationManager.AppSettings["consumer_id"].ToString();
                String consumer_secret = ConfigurationManager.AppSettings["consumer_secret"].ToString();
                String token_id = ConfigurationManager.AppSettings["token_id"].ToString();
                String token_secret = ConfigurationManager.AppSettings["token_secret"].ToString();
                String realm = ConfigurationManager.AppSettings["realm"].ToString();

                string url = ConfigurationManager.AppSettings["url"].ToString().Replace('|', '&');

                Uri uri = new Uri(url);

                //// var request = new RestRequest(Method.POST);

                OAuthBase oAuthBase = new OAuthBase();

                string normalized_url;
                string normalized_params;

                string nonce = oAuthBase.GenerateNonce();
                string time = oAuthBase.GenerateTimeStamp();

                string signature = oAuthBase.GenerateSignature(uri, consumer_id, consumer_secret, token_id, token_secret, operation,
                                         time, nonce, out normalized_url, out normalized_params);

                // URL encode any + characters generated in the signature
                if (signature.Contains("+"))
                {
                    signature = signature.Replace("+", "%2B");
                }

                request.AddHeader("Authorization", "OAuth realm=\"" + realm + "\",oauth_consumer_key=\"" + consumer_id + "\",oauth_token=\"" + token_id + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + time + "\",oauth_nonce=\"" + nonce + "\",oauth_version=\"1.0\",oauth_signature=\"" + signature + "\"");

                request.AddHeader("Content-Type", "application/json");
                //request.AddHeader("Cookie", "NS_ROUTING_VERSION=LEADING; lastUser=6185698_SB1_3139_1013");
                request.AddHeader("Cookie", "NS_ROUTING_VERSION=LEADING; lastUser=4953130_SB1_107_1034");

                // request.AddParameter("application/json", json, ParameterType.RequestBody);
                // response = client.Execute(request);
            }
            catch (Exception)
            {
                throw;
            }
            return request;
        }

        public static RestRequest ApiRestRequest(Metodo metodo, string url, 
            string consumer_id, string consumer_secret, string token_id, string token_secret,string realm)
        {

            var operation = "";

            var request = new RestRequest();

            switch (metodo)
            {
                case 0:
                    request = new RestRequest(Method.GET);
                    operation = "GET";
                    break;
                case (Metodo)1:
                    request = new RestRequest(Method.POST);
                    operation = "POST";
                    break;
                case (Metodo)2:
                    request = new RestRequest(Method.PUT);
                    operation = "PUT";
                    break;
            }

            try
            {
                url = url.Replace('|', '&');

                Uri uri = new Uri(url);

                OAuthBase oAuthBase = new OAuthBase();

                string normalized_url;
                string normalized_params;

                string nonce = oAuthBase.GenerateNonce();
                string time = oAuthBase.GenerateTimeStamp();

                string signature = oAuthBase.GenerateSignature(uri, consumer_id, consumer_secret, token_id, token_secret, operation,time, nonce, out normalized_url, out normalized_params);

                // URL encode any + characters generated in the signature
                if (signature.Contains("+"))
                {
                    signature = signature.Replace("+", "%2B");
                }

                request.AddHeader("Authorization", "OAuth realm=\"" + realm + "\",oauth_consumer_key=\"" + consumer_id + "\",oauth_token=\"" + token_id + "\",oauth_signature_method=\"HMAC-SHA256\",oauth_timestamp=\"" + time + "\",oauth_nonce=\"" + nonce + "\",oauth_version=\"1.0\",oauth_signature=\"" + signature + "\"");

                request.AddHeader("Content-Type", "application/json");

            }
            catch (Exception)
            {
                throw;
            }
            return request;
        }






        public enum Metodo
        {
            GET,
            POST,
            PUT
        }

    }
}
