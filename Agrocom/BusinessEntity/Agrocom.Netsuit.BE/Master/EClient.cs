﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Master
{
    public class lstEClient
    {
        public List<EClient> response { set; get; }
    }

    public class EClient
    {
        public string internalid { set; get; }
        public string isperson { set; get; }
        public string firstname { set; get; }
        public string lastname { set; get; }
        public string companyname { set; get; }
        public string externalid { set; get; }
        public string email { set; get; }
        public string id_subsidiary { set; get; }
        public string subsidiary { set; get; }
        public string tipodocumento { set; get; }
        public string nodocumento { set; get; }
    }

    public class EClientResponse
    {
        public string id { set; get; }
        public string error { set; get; }
        public string trama { set; get; }
    }

    public class EClientDTO
    {
        public string internalid { set; get; }
        public string externalid { set; get; }
        public int id_subsidiary { set; get; }
    }
}
