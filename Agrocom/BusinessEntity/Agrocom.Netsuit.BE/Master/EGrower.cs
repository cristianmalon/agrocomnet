﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Master
{
    public class lstEGrower
    {
        public List<EGrower> response { set; get; }
    }

    public class EGrower
    {
        public string internalid { set; get; }
        public string id { set; get; }
        public string name { set; get; }
        public string email { set; get; }
        public string phone { set; get; }
        public string office_phone { set; get; }
        public string fax { set; get; }
        public string alt_email { set; get; }
        public string uen { set; get; }
        public string brn { set; get; }
        public string banck_account_type { set; get; }
        public string vendor_origin { set; get; }
        public string subsidiary { set; get; }
        public string id_subsidiary { set; get; }
        public string inactive { set; get; }
        public string date_Created { set; get; }
        public string date_Modified { set; get; }
    }
}
