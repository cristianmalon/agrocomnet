﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrocom.Netsuite.BE.Master
{
    public class lstELogisticOperator
    {
        public List<ELogisticOperator> response { set; get; }
    }
    public class ELogisticOperator
    {
        public string internalid { set; get; }
        public string id { set; get; }
        public string name { set; get; }
    }
}
