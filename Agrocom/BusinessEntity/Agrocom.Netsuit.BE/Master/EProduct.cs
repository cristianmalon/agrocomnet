﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Master
{
    public class lstEProduct
    {
        public List<EProduct> response { set; get; }
    }

    public class EProduct
    {
        public int internalid { set; get; }
        public string type { set; get; }
        public string item { set; get; }
        public string displayname { set; get; }
        public string description { set; get; }
        //public string color { set; get; }
        //public string calibre { set; get; }
        public string marca_comercial { set; get; }
        //public string categoria { set; get; }
        public string comprador { set; get; }
        public string categoria1 { set; get; }
        public string calibre1 { set; get; }
        public string color1 { set; get; }
        public string familia { set; get; }
        public string grupo { set; get; }
        public string parent { set; get; }
        public string subsidiary { set; get; }
        public string id_subsidiary { set; get; }
        //public string inactive { set; get; }
        //public string date_Created { set; get; }
        //public string date_Modified { set; get; }
        public string codigoantiguo { set; get; }
        public string familianombre { set; get; }
        public string gruponombre { set; get; }

    }

    public class EProductSubsidiary
    {
        public int internalid { set; get; }
        public int id_subsidiary { set; get; }
        public string subsidiary { set; get; }

    }

    public class lstEProductSubsidiary
    {
        public List<EProductSubsidiary> ListProductSubsidiary { set; get; }
        
    }

    public class EProduct_Subsidiary
    {
        public lstEProduct ListProduct { set; get; }
        public lstEProductSubsidiary ListProductSubsidiary { set; get; }

    }
}
