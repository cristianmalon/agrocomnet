﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Master
{
    public class lstEProfitCenter
    {
        public List<EProfitCenter> response { set; get; }
    }

    public class EProfitCenter
    {
        public int internalid { set; get; }
        public string name { set; get; }
        public string inactive { set; get; }
        public string subsidiary { set; get; }
        public string id_subsidiary { set; get; }
        public string date_Created { set; get; }
        public string date_Modified { set; get; }
    }
}
