﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Mixed
{
    public class ECommercialPlan
    {
        public string doc_id { set; get; }
        public string fecha_creacion { set; get; }
        public string fecha_aprobacion { set; get; }
        public  string estado { set; get; }
        public  string subsidiaria { set; get; }
        public List<ECommercialPlanDetalle> detalle { set; get; }

    }

    public class ECommercialPlanDetalle
    {
        public int customer { set; get; }
        public int codigo_articulo { set; get; }
        public string week { set; get; }
        public string date_ini { set; get; }
        public string date_end { set; get; }
        public double cantidad { set; get; }
        public string doc_week_date_id { set; get; }

    }


}
