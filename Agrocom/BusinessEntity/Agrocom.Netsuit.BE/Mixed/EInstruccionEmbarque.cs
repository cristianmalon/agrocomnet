﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Mixed
{
    public class EInstruccionEmbarque
    {
        public string externalid { get; set; }
        public string num_contenedor { get; set; }
        public int operador_logistico { get; set; }
        public int persona_encargada { get; set; }
        public string fecha_ingreso_terminal { get; set; }
        public string naviera { get; set; }
        public string num_packing_list { get; set; }
        public string fecha_contacto { get; set; }
        public string contacto_agente { get; set; }
        public string num_booking { get; set; }
        public string nave_viaje { get; set; }
        public int regimen { get; set; }
        public int condicion_flete { get; set; }
        public int puerto_origen_embarque { get; set; }
        public string puerto_destino { get; set; }
        public string etd_aduana { get; set; }
        public string eta { get; set; }
        public int certificado_fitosanitario { get; set; }
        public bool cold_treatment { get; set; }
        public bool atmosfera_controlada { get; set; }
        public string temperatura { get; set; }
        public string ventilacion { get; set; }
        public string humedad { get; set; }
        public string quest { get; set; }
        public string lugar_inicio_despacho { get; set; }
        public string lugar_final_despacho { get; set; }
        public int tipo_flete { get; set; }
        public int medio_transporte { get; set; }
        public string num_instruccion_embarque { get; set; }
        public string ggn { get; set; }
        public string fda { get; set; }
        public string fecha_ingreso_planta { get; set; }
        public string fecha_salida_planta { get; set; }
        public decimal peso_bruto { get; set; }
        public string incoterm { get; set; }
        public decimal peso_neto { get; set; }
        public string ano_dua_exportacion { get; set; }
        public string num_dua_exportacion { get; set; }
        public int ubicacion { get; set; }
        public int ingreso_terminal { get; set; }
        public int orden_venta { get; set; }
        public int consignatario { get; set; }
        public int notificante { get; set; }
        public int notificante2 { get; set; }
        public string cropID { get; set; }
        public string bajo_promesa { get; set; }
        public List<EInstruccionEmbarqueDetalle> detalle { set; get; }

    }

    public class EInstruccionEmbarqueDetalle
    {
        public int externalid { set; get; }
        public int orden_venta { set; get; }
        public int total_cajas { set; get; }
        public int item { get; set; }
    }
    
}
