﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.Netsuite.BE.Mixed
{
    public class ESaleOrder
    {
        public string externalid { set; get; }
        public int entity { set; get; }
        public int tipo_documento { set; get; }
        public string nro_documento { set; get; }
        public string tipo_cambio { set; get; }
        public int tipo_identidad { set; get; }
        public int ubicacion { set; get; }
        public int nivel_de_prioridad { set; get; }
        public string correlativo { set; get; }
        public string semana_proyectada { set; get; }
        public string creado_por { set; get; }
        public string campania { set; get; }
        public string aprobado_por { set; get; }
        public string productor { set; get; }
        public string instruccion_embarque { set; get; }
        public string destino { set; get; }
        public string via { set; get; }
        //public string aplica_plu { set; get; }
        public string notificante { set; get; }
        public string estado { set; get; }         
        public string fecha_so { set; get; }
        public List<ESaleOrderDetalle> articulos { set; get; }
    }

    public class ESaleOrderDetalle
    {
        public int item { set; get; }
        public double rate { set; get; }
        public int quantity { set; get; }
        public string createwo { set; get; }
        public string no_workorder { set; get; }
    }

}
