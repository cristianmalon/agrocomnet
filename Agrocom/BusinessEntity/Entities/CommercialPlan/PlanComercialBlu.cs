﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CommercialPlan
{
    public class PlanComercialBlu
    {
        public List<PlanComercialDetBlu> planComercialDetail { set; get; }
    }

    public class PlanComercialDetBlu
    {
        public int Id { set; get; }
        public int PlanCustomerVarietyID { set; get; }
        public int CustomerID { set; get; }
        public string Program { set; get; }
        public int DestinationID { set; get; }
        public string BrandID { set; get; }
        public string VarietyID { set; get; } 
        public string SizeID { set; get; }
        public string CodePack { set; get; }
        public int CodePackID { set; get; }
        public int ViaID { set; get; }
        public string MarketID { set; get; }
        public int WowID { set; get; }
        public string Priority { set; get; }
        public decimal price { set; get; }
        public int BoxesPerPallet { set; get; }
        public int ToProcess { set; get; }
        public int Pallets { set; get; }
        public int Group { set; get; }
        public bool workOrder { get; set; }

    }
}
