﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class AGAControlEmbarque
    {
        public int Semana { get; set; }
        public string Vessel { get; set; }
        public string EtaReal { get; set; }
        public string BL { get; set; }
        public string Contenedor { get; set; }
        public string Factura { get; set; }
        public decimal CFR { get; set; }
        public string EdiFile { get; set; }
        public string CertificadoOrigen { get; set; }
        public string CertificadoFito { get; set; }
        public string TransmisionDoc { get; set; }
        public string Aprobacion { get; set; }
        public string Courrier { get; set; }
        public string Guia { get; set; }
        public string IndicadorDia { get; set; }
        public string DAM { get; set; }
        public string FechaRecepcionDAM { get; set; }
        public string FacturaOperador { get; set; }
        public decimal MontoD { get; set; }
        public string DocumentoCobranza { get; set; }
        public decimal MontoS { get; set; }
        public string FactOtrosGastos { get; set; }
        public decimal D { get; set; }
        public decimal S { get; set; }
        public decimal Rebate { get; set; }
        public string Estado { get; set; }
        public int IdInstruccion { get; set; }
        public SCEINstruccion SCEINstruccion { get; set; }
    }

    public class SCEINstruccion
    {
        public string NroPackingList { get; set; }
        public string NroReferencia { get; set; }
        public string Booking { get; set; }
        public string Notificante { get; set; }
        public string Etd { get; set; }
        public string Eta { get; set; }
        public string HoraLLenado { get; set; }
        public string SalidaIca { get; set; }
        public string DiadeCarga { get; set; }
        public decimal VGM { get; set; }
        public SCESucursal SCESucursal { get; set; }
        public SCEClienteProveedor3 SCEClienteProveedor3 { get; set; }
        public SCEEncargado SCEEncargado { get; set; }
        public SCEInstruccionDetalleM SCEInstruccionDetalleM { get; set; }
        public SCENaviera SCENaviera { get; set; }
        public SCEOperadorLogistico SCEOperadorLogistico { get; set; }
        public SCEPuerto1 SCEPuerto1 { get; set; }
        public SCEPuerto2 SCEPuerto2 { get; set; }
    }
    public class SCESucursal
    {
        public string Descripcion { get; set; }
    }
    public class SCEClienteProveedor3
    { 
        public string RazonSocial { get; set; }
    }
    public class SCEEncargado
    {
        public string Descripcion { get; set; }
    }
    public class SCEInstruccionDetalleM
    {
        public string Envase { get; set; }
        public decimal Peso { get; set; }
        public string Color { get; set; }
        public decimal Cantidad { get; set; }
        public SCEMarca SCEMarca { get; set; }
        public SCEVariedad SCEVariedad { get; set; }
        public SCECategoria SCECategoria { get; set; }
    }
    public class SCEMarca
    {
        public string Descripcion { get; set; }
    }
    public class SCEVariedad
    {
        public string Descripcion { get; set; }
    }
    public class SCECategoria
    {
        public string Descripcion { get; set; }
    }
    public class SCENaviera
    {
        public string Descripcion { get; set; }
    }
    public class SCEOperadorLogistico
    {
        public string Descripcion { get; set; }
    }
    public class SCEPuerto1
    {
        public string Pais { get; set; }
    }
    public class SCEPuerto2
    {
        public string Pais { get; set; }
    }
}
