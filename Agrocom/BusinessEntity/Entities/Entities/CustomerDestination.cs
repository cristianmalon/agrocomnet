﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class CustomerDestination
    {
        public int destinationID { get; set; }
        public string cropID { get; set; }
        public string brix { get; set; }
        public string acidity { get; set; }
        public string bloom { get; set; }
        public string traceability { get; set; }
        public string comments { get; set; }
        public string comments2 { get; set; }
        public int tolerance { get; set; }
    }
}
