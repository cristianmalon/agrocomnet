﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENCommerceSpecification
    {
        public string cropID { get; set; }
        public int customerID { get; set; }
        public int programID { get; set; }
        public int destinationID { get; set; }
        public int varietyID { get; set; }
        public string categoryID { get; set; }
        public int formatID { get; set; }
        public int packageProductID { get; set; }
        public int brandID { get; set; }
        public int presentationID { get; set; }
        public int labelID { get; set; }
        public int labelBrand { get; set; }
        public string stretcher { get; set; }
        public string caliber { get; set; }
        public string dateType { get; set; }
        public string labelModel { get; set; }
        public string labelAdditional { get; set; }
        public string plu { get; set; }
        public string color { get; set; }
        public string codepack { get; set; }
        public string sizes { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public int dateCreated { get; set; }
        public int fileName { get; set; }
    }
}
