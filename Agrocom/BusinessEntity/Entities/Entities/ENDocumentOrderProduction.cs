﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENDocumentOrderProduction
    {
        public void ReplaceNull()
        {
            commentsOBM = commentsOBM ?? "";
            commentsCustomer = commentsCustomer ?? "";
        }
        public int orderproductionID { get; set; }
        public int open { get; set; }
        public string commentsOBM { get; set; }
        public int userID { get; set; }

        public string commentsCustomer { get; set; }
        public int statusRevision { get; set; }
    }
}
