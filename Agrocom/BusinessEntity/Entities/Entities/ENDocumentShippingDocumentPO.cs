﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENDocumentShippingDocumentPO
    {
        public void ReplaceNull()
        {
            file = file ?? "";
            file2 = file2 ?? "";
            file3 = file3 ?? "";
            notesOBM = notesOBM ?? "";
            fileCustomer = fileCustomer ?? "";
            notesCustomer = notesCustomer ?? "";
        }
        public int orderProductionDocumentID { get; set; }
        public int orderproductionID { get; set; }
        public int typeDocumentId { get; set; }
        public int enabled { get; set; }
        public string file { get; set; }
        public string file2 { get; set; }
        public string file3 { get; set; }
        public string notesOBM { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }

        public string fileCustomer { get; set; }
        public string notesCustomer { get; set; }
    }
}
