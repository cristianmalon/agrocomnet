﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENDocumentShippingProgram
    {
        public void ReplaceNull()
        {
            originPort = originPort ?? "";
            container = container ?? "";
            vessel = vessel ?? "";
            bl = bl ?? "";
            commentaryDocumentsOBM = commentaryDocumentsOBM ?? "";
            commentaryDocumentsCustomer = commentaryDocumentsCustomer ?? "";
            open = open ?? 0;
            statusRevision = statusRevision ?? 0;
            userModify = userModify ?? "";
            dateModify = dateModify ?? "";
        }
        public int orderProductionID { get; set; }
        public string orderProduction { get; set; }
        public string packingList { get; set; }
        public string originPort { get; set; }
        public string container { get; set; }
        public string customer { get; set; }
        public string grower { get; set; }
        public string destinationPort { get; set; }
        public string vessel { get; set; }
        public string incoterm { get; set; }
        public string origin { get; set; }
        public string shippingLine { get; set; }
        public string etd { get; set; }
        public string eta { get; set; }
        public string saleTerm { get; set; }
        public string via { get; set; }
        public string bl { get; set; }
        public string responsable { get; set; }
        public int? open { get; set; }
        public int? statusRevision { get; set; }
        public string commentaryDocumentsOBM { get; set; }
        public string commentaryDocumentsCustomer { get; set; }
        public string userModify { get; set; }
        public string dateModify { get; set; }
        public List<ENDocumentShippingProgramDetail> details { get; set; }
    }

    public class ENDocumentShippingProgramDetail
    {
        public void ReplaceNull()
        {

        }
        public string brand { get; set; }
        public string variety { get; set; }
        public string codepack { get; set; }
        public string sizeInfo { get; set; }
        public int boxes { get; set; }
        public decimal pallets { get; set; }
        public decimal netWeight { get; set; }
        public decimal grossWeight { get; set; }
    }
}
