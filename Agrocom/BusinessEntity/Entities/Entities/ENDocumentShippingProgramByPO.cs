﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENDocumentShippingProgramByPO
    {
        public void ReplaceNull()
        {
            file = file ?? "";
            file2 = file2 ?? "";
            file3 = file3 ?? "";
            notesOBM = notesOBM ?? "";
            fileCustomer = fileCustomer ?? "";
            notesCustomer = notesCustomer ?? "";
            dateModifyOBM = dateModifyOBM ?? "19000101";
            dateCustomer = dateCustomer ?? "19000101";
        }
        public int typeDocumentID { get; set; }
        public string typeDocument { get; set; }
        public string number { get; set; }
        public string file { get; set; }
        public string file2 { get; set; }
        public string file3 { get; set; }
        public string notesOBM { get; set; }
        public string userOBM { get; set; }
        public string dateModifyOBM { get; set; }
        public int enabled { get; set; }

        public int statusID { get; set; }
        public string status { get; set; }
        public string fileCustomer { get; set; }
        public string notesCustomer { get; set; }
        public string userCustomer { get; set; }
        public string dateCustomer { get; set; }
    }
}
