﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENFinanceCustomerInvoice
    {
        public void ReplaceNull()
        {
            number = number ?? "";
            comments = comments ?? "";
            date = date ?? "19000101";
            //date = (date == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : date;
        }
        public int customerInvoiceID { get; set; }
        public int customerSettlementID { get; set; }
        public int documentTypeID { get; set; }
        public string documentType { get; set; }
        public string date { get; set; }
        public int localCurrencyID { get; set; } 
        public string localCurrency { get; set; } //
        public string localCurrencySymbol { get; set; }//
        public string number { get; set; }
        public decimal totalAmount { get; set; }
        public decimal exchangeRate { get; set; }
        public string incoterm { get; set; }
        public decimal totalAmountUSD { get; set; }
        public string comments { get; set; }
        public decimal insuranceBL { get; set; }
        public decimal freightBL { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string userCreated { get; set; }
        public string dateCreated { get; set; }        
        public string userModify { get; set; }
        public string dateModified { get; set; }
        public List<ENFinanceCustomerInvoiceDetail> details { get; set; }
    }

    public class ENFinanceCustomerInvoiceDetail
    {
        public void ReplaceNull()
        {

        }
        public int customerInvoiceID { get; set; }
        public int customerInvoiceDetailID { get; set; }
        public int customerSettlementDetailID { get; set; }      
        public int quantityBoxes { get; set; }
        public decimal pricePerBox { get; set; }
        public decimal total { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string dateCreated { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }
        public string brand { get; set; }
        public string description { get; set; }
        public string variety { get; set; }
        public string sizeInfo { get; set; }
        public decimal weight { get; set; }
        public decimal pricePerBoxFOB { get; set; }
        public decimal totalFOB { get; set; }

    }
}
