﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENFinanceCustomerPayments
    {
        public void ReplaceNull()
        {
            numberOperation = numberOperation ?? "";
            comments = comments ?? "";
            date = date ?? "19000101";
            //date = (date == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : date;
        }
        public int customerPaymentID { get; set; }
        public int customerSettlementID { get; set; }
        public int paymentTypeID { get; set; }
        public string paymentType { get; set; } //
        public string numberOperation { get; set; }
        public string date { get; set; }
        public int localCurrencyID { get; set; }
        public string localCurrency { get; set; }//
        public string localCurrencySymbol { get; set; }//
        public decimal exchangeRate { get; set; }
        public decimal amount { get; set; }
        public decimal amountUSD { get; set; }
        public string comments { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string dateCreated { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }
        public string userCreated { get; set; }
        public string userModify { get; set; }
        public List<ENFinanceCustomerPaymentsDetail> details { get; set; }
    }
    public class ENFinanceCustomerPaymentsDetail
    {
        public void ReplaceNull()
        {
        }
        public int customerPaymentID { get; set; }
        public int customerPaymentsDetailID { get; set; }
        public int customerSettlementDetailID { get; set; }
        public decimal pricePerBox { get; set; }
        public decimal total { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string dateCreated { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }
        public string description { get; set; }
        public string variety { get; set; }
        public string sizeInfo { get; set; }
        public string brand { get; set; }
        public int quantityBoxes { get; set; }
        public decimal totalUSD { get; set; }
    }
}
