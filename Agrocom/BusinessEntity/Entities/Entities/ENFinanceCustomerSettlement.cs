﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENFinanceCustomerSettlement
    {
        public void ReplaceNull()
        {
            comments = comments ?? "";
            date = date ?? "19000101";
            //date = (date == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : date;
        }
        public int customerSettlementID { get; set; }
        public int orderProductionID { get; set; }
        public string date { get; set; }
        public int localCurrencyID { get; set; }
        public decimal exchangeRate { get; set; }       
        public decimal totalLocalCurrency { get; set; }
        public decimal totalUSD { get; set; }
        public int incotermID { get; set; }
        public string comments { get; set; }
        public decimal comissionPercent { get; set; }
        public decimal comission { get; set; }
        public decimal oceanFreight { get; set; }
        public decimal coldTratment { get; set; }
        public decimal importDuty { get; set; }
        public decimal forwardingCharge { get; set; }
        public decimal marketCharge { get; set; }
        public decimal transportCost { get; set; }
        public decimal detentionCharge { get; set; }
        public decimal coldStorage { get; set; }
        public decimal otherCharge { get; set; }
        public decimal repacking { get; set; }
        public decimal expenseFixed { get; set; }
        public decimal expenseVariable { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string orderProduction { get; set; }
        public string container { get; set; }
        public string customer { get; set; }
        public string incoterm { get; set; }
        public string localCurrency { get; set; }
        public string localCurrencySymbol { get; set; }
        public string userCreated { get; set; }
        public string userModify { get; set; }
        public string dateCreated { get; set; }
        public string dateModified { get; set; }
        public string Ocean_Freight { get; set; }
        public string Cold_Tratment { get; set; }
        public string Import_Duty { get; set; }
        public string Forwarding_Charge { get; set; }
        public string Market_Charge { get; set; }
        public string Transport_Cost { get; set; }
        public string Detention_Charge { get; set; }
        public string Cold_Storage { get; set; }
        public string Other_Charge { get; set; }
        public string Repacking_Charges { get; set; }
        public int flagExchange { get; set; }
        public decimal totalToColletUSD { get; set; }
        public List<ENFinanceCustomerSettlementDetail> details { get; set; }
        public List<ENCostDistributiom> detailcosts { get; set; }

    }

    public class ENCostDistributiom {
        public int settlementExpensesID { get; set; }
        public decimal amount { get; set; }
        public int typeOfExpenses { get; set; }
    }


    public class ENFinanceCustomerSettlementDetail
    {
        public void ReplaceNull()
        {
            categoryID = categoryID ?? "";
            sizeID = sizeID ?? "";
            description = description ?? "";
        }
        public int customerSettlementDetailID { get; set; }
        public int customerSettlementID { get; set; }
        public int codepackID { get; set; }
        public int varietyID { get; set; }
        public string categoryID { get; set; }
        public string sizeID { get; set; }
        public string description { get; set; }
        public int quantityBoxes { get; set; }
        public int quantityBoxesPL { get; set; }
        public decimal pricePerBox { get; set; }
        public decimal totalSettlement { get; set; }
        public decimal expenseFixed { get; set; }
        public decimal expenseVariable { get; set; }
        public decimal totalSettlementUSD { get; set; }
        public decimal netSettlement { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public int dateCreated { get; set; }
        public int userModifyID { get; set; }
        public int dateModified { get; set; }
        public string brand { get; set; }        
        public string variety { get; set; }
        public string sizeInfo { get; set; }
        public decimal totalInvoiceUSD { get; set; }
        public decimal totalDiferenceUSD { get; set; }
        public decimal totalExpensesUSD { get; set; }
        public decimal totalToColletUSD { get; set; }
        public decimal advancePaymentsUSD { get; set; }
        public decimal balancePayemntesUSD { get; set; }
        public decimal finalPriceUSD { get; set; }
        public decimal kgCaja { get; set; }
        public decimal finalPriceCaja { get; set; }

    }
}
