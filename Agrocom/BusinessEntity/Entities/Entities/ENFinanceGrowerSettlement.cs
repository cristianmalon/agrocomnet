﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENFinanceGrowerSettlement
    {
        public void ReplaceNull()
        {
            reference = reference ?? "";
            date = date ?? "";
            //date = (date == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : date;            
        }
        public int growerSettlementID { get; set; }
        public int customerSettlementID { get; set; }
        public int number { get; set; }
        public string reference { get; set; }
        public string date { get; set; }
        public decimal qc { get; set; }
        public decimal insurance { get; set; }
        public decimal others { get; set; }
        public decimal comissionPercent { get; set; }
        public decimal comission { get; set; }
        public decimal promotionFeePercent { get; set; }
        public decimal promotionFee { get; set; }
        public int statusConfirm { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string dateCreated { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }
        public string userCreated { get; set; }
        public string userModify { get; set; }
        public List<ENFinanceGrowerSettlementDetail> details { get; set; }
    }
    public class ENFinanceGrowerSettlementDetail
    {
        public void ReplaceNull()
        {
        }
        public int growerSettlementDetailID { get; set; }
        public int growerSettlementID { get; set; }
        public int customerSettlementDetailID { get; set; }
        public decimal pricePerBox { get; set; }
        public decimal total { get; set; }
        public int statusConfirm { get; set; }
        public int statusID { get; set; }
        public int userID { get; set; }
        public string dateCreated { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }

        public int quantityBoxes { get; set; }
        public string brand { get; set; }
        public string variety { get; set; }
        public string sizeInfo { get; set; }
        public string description { get; set; }
        public string weight { get; set; }

        public decimal totalSettlementUSD { get; set; }
        public decimal totalSettlement { get; set; }
        public decimal pricePerBoxSettlementUSD { get; set; }
        public decimal pricePerBoxSettlement { get; set; }

        public decimal pricePerBoxSettlementNetUSD { get; set; }

    }
}
