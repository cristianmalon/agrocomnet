﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENFinanceNisira
    {
        public void ReplaceNull()
        {
            REFERENCIA_EMBARQUE = REFERENCIA_EMBARQUE?? "";
            NROPACKINGLIST = NROPACKINGLIST ?? "";
            FECHA_ARRIBO = FECHA_ARRIBO ?? "";
            IDTIPOPRECIO = IDTIPOPRECIO ?? "";
            DOCREFERENCIADEB = DOCREFERENCIADEB ?? "";
            IDREFERENCIADEB = IDREFERENCIADEB ?? "";
            DOCREFERENCIACRED = DOCREFERENCIACRED ?? "";
            IDREFERENCIACRED = IDREFERENCIACRED ?? "";
            TCAMBIO = TCAMBIO ?? 0;
            TCAMBIO_LIQ = TCAMBIO_LIQ ?? 0;
            TOTALLIQUIDADO = TOTALLIQUIDADO ?? 0;
            TOTALDIFERENCIA = TOTALDIFERENCIA ?? 0;
            TOTALGASTOS = TOTALGASTOS ?? 0;
            TOTALFOB = TOTALFOB ?? 0;
        }
        public string IDLIQVENTASEXTERIOR { get; set; }
        public string IDESTADO { get; set; }
        public string IDDOCUMENTO { get; set; }
        public string SERIE { get; set; }
        public string NUMERO { get; set; }
        public string FECHA { get; set; }
        public decimal? TCAMBIO { get; set; }
        public string FECHACREACION { get; set; }
        public string VENTANA { get; set; }
        public decimal TOTALFACTURADO { get; set; }
        public decimal? TOTALLIQUIDADO { get; set; }
        public decimal? TOTALDIFERENCIA { get; set; }
        public decimal TOTALPRODUCTOS { get; set; }
        public decimal? TOTALGASTOS { get; set; }
        public string IDREFERENCIADEB { get; set; }
        public string DOCREFERENCIADEB { get; set; }
        public decimal? TOTALFOB { get; set; }
        public string IDREFERENCIACRED { get; set; }
        public string DOCREFERENCIACRED { get; set; }
        public string IDTIPOPRECIO { get; set; }
        public string FECHA_ARRIBO { get; set; }
        public string NROPACKINGLIST { get; set; }
        public string REFERENCIA_EMBARQUE { get; set; }
        public decimal? TCAMBIO_LIQ { get; set; }
        public List<LiqVentasExteriorDocumentos> dLiqVentasExteriorDocumentos { get; set; }
        public List<CobrarPagarDocINV> cobrarPagarDocINV { get; set; }
        public List<CobrarPagarDocDEB> cobrarPagarDocDEB { get; set; }
        public List<CobrarPagarDocCRED> cobrarPagarDocCRED { get; set; } 
        public List<VMovCtaCteINV> nVMovCtaCteINV { get; set; }
        public List<VMovCtaCteDEB> nVMovCtaCteDEB { get; set; }
        public List<VMovCtaCteCRED> nVMovCtaCteCRED { get; set; }
        public List<LiqVentasExterior> dLiqVentasExterior { get; set; }
        public List<dCobrarPagarDocINV> dCobrarPagarDocINV { get; set; }
        public List<dCobrarPagarDocDEB> dCobrarPagarDocDEB { get; set; }
        public List<dCobrarPagarDocCRED> dCobrarPagarDocCRED { get; set; }

    }

    public class LiqVentasExteriorDocumentos
    {
        public string IDREFERENCIAINV { get; set; }
        public string DOCREFERENCIAINV { get; set; }
        public string FECHA { get; set; }
        public decimal IMPORTE_FACTURADO { get; set; }
    }

    public class CobrarPagarDocINV 
    {
        public void ReplaceNull()
        {
            numOperacionINV = numOperacionINV ?? "";
            voucherINV = voucherINV ?? "";
            awbINV = awbINV ?? "";
            tcambioINV = tcambioINV ?? 0;
            inafectoINV = inafectoINV ?? 0;
        }
        public string idcobrarpagardocINV { get; set; }
        public string numOperacionINV { get; set; }
        public string fechaRegistroINV { get; set; }
        public string voucherINV { get; set; }
        public string vencimientoINV { get; set; }
        public decimal? tcambioINV { get; set; }
        public decimal? inafectoINV { get; set; }
        public decimal importeINV { get; set; }
        public string awbINV { get; set; }
    }

    public class CobrarPagarDocDEB {
        public void ReplaceNull()
        {
            numOperacionDEB = numOperacionDEB ?? "";
            voucherDEB = voucherDEB ?? "";
            awbDEB = awbDEB ?? "";
            tcambioDEB = tcambioDEB ?? 0;
            inafectoDEB = inafectoDEB ?? 0;
        }
        public string idcobrarpagardocDEB { get; set; }
        public string numOperacionDEB { get; set; }
        public string fechaRegistroDEB { get; set; }
        public string voucherDEB { get; set; }
        public string vencimientoDEB { get; set; }
        public decimal? tcambioDEB { get; set; }
        public decimal? inafectoDEB { get; set; }
        public decimal importeDEB { get; set; }
        public string awbDEB { get; set; }
    }

    public class CobrarPagarDocCRED
    {
        public void ReplaceNull()
        {
            numOperacionCRED = numOperacionCRED ?? "";
            voucherCRED = voucherCRED ?? "";
            awbCRED = awbCRED ?? "";
            tcambioCRED = tcambioCRED ?? 0;
            inafectoCRED = inafectoCRED ?? 0;
        }
        public string idcobrarpagardocCRED { get; set; }
        public string numOperacionCRED { get; set; }
        public string fechaRegistroCRED { get; set; }
        public string voucherCRED { get; set; }
        public string vencimientoCRED { get; set; }
        public decimal? tcambioCRED { get; set; }
        public decimal? inafectoCRED { get; set; }
        public decimal importeCRED { get; set; }
        public string awbCRED { get; set; }
    }

    public class VMovCtaCteINV {
        public void ReplaceNull()
        {
            GLOSAINV = GLOSAINV ?? "";
            TCAMBIOINV = TCAMBIOINV ?? 0;
        }
        public string IDMOVCTACTEINV { get; set; }
        public string GLOSAINV { get; set; }
        public decimal? TCAMBIOINV { get; set; }
        public decimal IMPORTEMOFINV { get; set; }
        public string TABLAINV { get; set; }
        public string IDREFERENCIAINV { get; set; }
    }

    public class VMovCtaCteDEB {
        public void ReplaceNull()
        {
            GLOSADEB = GLOSADEB ?? "";
            TCAMBIODEB = TCAMBIODEB ?? 0;
        }
        public string IDMOVCTACTEDEB { get; set; }
        public string GLOSADEB { get; set; }
        public decimal? TCAMBIODEB { get; set; }
        public decimal IMPORTEMOFDEB { get; set; }
        public string TABLADEB { get; set; }
        public string IDREFERENCIADEB { get; set; }
    }

    public class VMovCtaCteCRED
    {
        public void ReplaceNull()
        {
            GLOSACRED = GLOSACRED ?? "";
            TCAMBIOCRED = TCAMBIOCRED ?? 0;
        }
        public string IDMOVCTACTECRED { get; set; }
        public string GLOSACRED { get; set; }
        public decimal? TCAMBIOCRED { get; set; }
        public decimal IMPORTEMOFCRED { get; set; }
        public string TABLACRED { get; set; }
        public string IDREFERENCIACRED { get; set; }
    }

    public class LiqVentasExterior {
        public void ReplaceNull()
        {
            ITEM = ITEM ?? "";
            DESCRIPCION = DESCRIPCION ?? "";
            IMPORTE_FOB = IMPORTE_FOB ?? 0;
            PRECIO_FOB = PRECIO_FOB ?? 0;
            DIFERENCIA = DIFERENCIA ?? 0;
            PRECIO_EXPLANTA = PRECIO_EXPLANTA ?? 0;
        }
        public string IDLIQVENTASEXTERIOR { get; set; }
        public string ITEM { get; set; }
        public string IDPRODUCTO { get; set; }
        public string DESCRIPCION { get; set; }
        public string CANTIDAD_FACTURA { get; set; }
        public decimal IMPORTE_FACTURA { get; set; }
        public decimal IMPORTE { get; set; }
        public decimal PRECIO_FACTURA { get; set; }
        public decimal PRECIO { get; set; }
        public decimal? IMPORTE_FOB { get; set; }
        public decimal? PRECIO_FOB { get; set; }
        public decimal? DIFERENCIA { get; set; }
        public decimal? PRECIO_EXPLANTA { get; set; }
    }

    public class dCobrarPagarDocINV {
        public void ReplaceNull()
        {
            idproductoINV = idproductoINV ?? "";
            preciolistaINV = preciolistaINV ?? 0;
            vventaINV = vventaINV ?? 0;
        }
        public string idcobrarpagardocINV { get; set; }
        public string idproductoINV { get; set; }
        public string cantidadINV { get; set; }
        public decimal precioINV { get; set; }
        public decimal? preciolistaINV { get; set; }
        public decimal? vventaINV { get; set; }
        public decimal importeINV { get; set; }
    }

    public class dCobrarPagarDocDEB {
        public void ReplaceNull()
        {
            idproductoDEB = idproductoDEB ?? "";
            preciolistaDEB = preciolistaDEB ?? 0;
            vventaDEB = vventaDEB ?? 0;
        }
        public string idcobrarpagardocDEB { get; set; }
        public string idproductoDEB { get; set; }
        public string cantidadDEB { get; set; }
        public decimal precioDEB { get; set; }
        public decimal? preciolistaDEB { get; set; }
        public decimal? vventaDEB { get; set; }
        public decimal importeDEB { get; set; }
    }
    public class dCobrarPagarDocCRED
    {
        public void ReplaceNull()
        {
            idproductoCRED = idproductoCRED ?? "";
            preciolistaCRED = preciolistaCRED ?? 0;
            vventaCRED = vventaCRED ?? 0;
        }
        public string idcobrarpagardocCRED { get; set; }
        public string idproductoCRED { get; set; }
        public string cantidadCRED { get; set; }
        public decimal precioCRED { get; set; }
        public decimal? preciolistaCRED { get; set; }
        public decimal? vventaCRED { get; set; }
        public decimal importeCRED { get; set; }
    }

}
