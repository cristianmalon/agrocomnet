﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENPOModified
    {
        public void ReplaceNull()
        {
            RequestNumber = RequestNumber ?? "";
            commentary = commentary ?? "";
        }

        public int orderProductionModifiedID { get; set; }
        public int orderProductionID { get; set; }
        public int saleID { get; set; }
        public string RequestNumber { get; set; }
        public int specificationID { get; set; }
        public int projectedweekID { get; set; }
        public string commentary { get; set; }
        public int userCreated { get; set; }
        public string dateCreated { get; set; }
        public int statusID { get; set; }
        public string statusConfirm { get; set; }
        //public string dateConfirm { get; set; }
        //public string dateBoardEstimate { get; set; }
        public int statusShipment { get; set; }
        public int userConfirm { get; set; }
        public string reason { get; set; }
        public string growerID { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }
        public int statudShippmentDocuments { get; set; }
        public int openShippmentDocuments { get; set; }
        public string commentsShippmentDocumentsOBM { get; set; }
        public string commentsShippmentDocumentsCustomer { get; set; }
        public string dayBoardEstimate { get; set; }
        public int statusConfirmID { get; set; }
        public int campaignID { get; set; }
        public string farmID { get; set; }
        public List<ENPOModifiedDetail> details { get; set; }
    }

    public class ENPOModifiedDetail 
    { 
        public int orderProductionDetailModifiedID { get; set; }
        public int orderProductionDetailID { get; set; }
        public int orderProductionID { get; set; }
        public int wowID { get; set; }
        public int saleDetailID { get; set; }
        public int projectedProductionDetailID { get; set; }
        public string originID { get; set; }
        public decimal Quantity { get; set; }
        public string unitMeasureID { get; set; }
        public string dateCreated { get; set; }
        public int userID { get; set; }
        public int statusID { get; set; }
        public decimal quantityConfirm { get; set; }
        public string sizeID { get; set; }
        public string categoryID { get; set; }
        public int varietyID { get; set; }
        public int codePackID { get; set; }
        public int presentationID { get; set; }
        public int formatID { get; set; }
        public int packageProductID { get; set; }
        public int brandID { get; set; }
        public int labelID { get; set; }
        public int orderProductionModifiedID { get; set; }

    }

    public class ENPOGenerate
    {
        public void ReplaceNull()
        {
            comments = comments ?? "";
            cropID = cropID ?? "";
        }
        public int orderProductionID { get; set; }
        public int saleID { get; set; }
        public string comments { get; set; }
        public int userID { get; set; }
        public string cropID { get; set; }
        public int campaignID { get; set; }
    }
}
