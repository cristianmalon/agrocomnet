﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENPObyPlant
    {
        public List<PObyPlant> PObyPlanta { set; get; }
    }

    public class PObyPlant
    { 
        public int orderProductionID { set; get; }
        public string packingID { set; get; }
    }
}
