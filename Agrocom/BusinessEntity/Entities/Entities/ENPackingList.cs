﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENPackingList
    {

        public int packingListID { get; set; }
        public string orderProduction { get; set; }
        public int orderProductionID { get; set; }
        public string nroPackingList { get; set; }
        public string grower { get; set; }
        public string growerID { get; set; }
        public string invoiceGrower { get; set; }
        public string nroGuide { get; set; }
        public string cropID { get; set; }
        public int customerID { get; set; }
        public string container { get; set; }
        public string senasaSeal { get; set; }
        public string customSeal { get; set; }
        public string thermogRegisters { get; set; }
        public string thermogRegistersLocation { get; set; }
        public string sensors { get; set; }
        public string sensorsLocation { get; set; }
        public string packingLoadDate { get; set; }
        public string booking { get; set; }
        public string originPort { get; set; }
        public string destinationPort { get; set; }
        public string shippingLine { get; set; }
        public string vessel { get; set; }
        public string BLAWB { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public int viaID { get; set; }
        public int totalBoxes { get; set; }
        public int qcOrigin { get; set; }
        public string qcOriginInspectedBy { get; set; }
        public string qcOriginInspectedDate { get; set; }
        public string qcOriginReport { get; set; }
        public string qcOriginNotes { get; set; }
        public string qcOriginInspectedBy2 { get; set; }
        public string qcOriginInspectedDate2 { get; set; }
        public string qcOriginReport2 { get; set; }
        public string qcOriginNotes2 { get; set; }
        public string qcOriginInspectedBy3 { get; set; }
        public string qcOriginInspectedDate3 { get; set; }
        public string qcOriginReport3 { get; set; }
        public string qcOriginNotes3 { get; set; }
        public string qcOriginInspectedBy4 { get; set; }
        public string qcOriginInspectedDate4 { get; set; }
        public string qcOriginReport4 { get; set; }
        public string qcOriginNotes4 { get; set; }

        public int qcDestination { get; set; }
        public string qcDestinationInspectedBy { get; set; }
        public string qcDestinationInspectedDate { get; set; }
        public string qcDestinationReport { get; set; }
        public string qcDestinationNotes { get; set; }
        public int qcCustomer { get; set; }
        public string qcCustomerInspectedBy { get; set; }
        public string qcCustomerInspectedDate { get; set; }
        public string qcCustomerReport { get; set; }
        public string qcCustomerNotes { get; set; }

        public int userModifyID { get; set; }

        public void ValueDates()
        {
            packingLoadDate = packingLoadDate ?? "19000101";
            ETD = ETD?? "";
            ETA = ETA ?? "";

            qcOriginInspectedDate = qcOriginInspectedDate ?? "";
            qcOriginInspectedDate2 = qcOriginInspectedDate2 ?? "";
            qcOriginInspectedDate3 = qcOriginInspectedDate3 ?? "";
            qcOriginInspectedDate4 = qcOriginInspectedDate4 ?? "";
            qcDestinationInspectedDate = qcDestinationInspectedDate ?? "";
            qcCustomerInspectedDate = qcCustomerInspectedDate ?? "";

            //packingLoadDate = (packingLoadDate == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : packingLoadDate;
            //ETD = (ETD == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : ETD;
            //ETA = (ETA == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : ETA;
            //qcOriginInspectedDate = (qcOriginInspectedDate == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : qcOriginInspectedDate;
            //qcOriginInspectedDate2 = (qcOriginInspectedDate2 == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : qcOriginInspectedDate2;
            //qcOriginInspectedDate3 = (qcOriginInspectedDate3 == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : qcOriginInspectedDate3;
            //qcOriginInspectedDate4 = (qcOriginInspectedDate4 == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : qcOriginInspectedDate4;
            //qcDestinationInspectedDate = (qcDestinationInspectedDate == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : qcDestinationInspectedDate;
            //qcCustomerInspectedDate = (qcCustomerInspectedDate == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : qcCustomerInspectedDate;

            orderProduction = orderProduction ?? "";
            nroPackingList = nroPackingList ?? "";

            grower = grower ?? "";
            growerID = growerID ?? "";
            invoiceGrower = invoiceGrower ?? "";
            nroGuide = nroGuide ?? "";
            cropID = cropID ?? "";

            container = container ?? "";
            senasaSeal = senasaSeal ?? "";
            customSeal = customSeal ?? "";
            thermogRegisters = thermogRegisters ?? "";

            thermogRegistersLocation = thermogRegistersLocation ?? "";
            sensors = sensors ?? "";
            sensorsLocation = sensorsLocation ?? "";
            booking = booking ?? "";
            originPort = originPort ?? "";
            destinationPort = destinationPort ?? "";
            shippingLine = shippingLine ?? "";

            vessel = vessel ?? "";
            BLAWB = BLAWB ?? "";
            qcOriginInspectedBy = qcOriginInspectedBy ?? "";
            qcOriginReport = qcOriginReport ?? "";
            qcOriginNotes = qcOriginNotes ?? "";
            qcOriginInspectedBy2 = qcOriginInspectedBy2 ?? "";
            qcOriginReport2 = qcOriginReport2 ?? "";
            qcOriginNotes2 = qcOriginNotes2 ?? "";
            qcOriginInspectedBy3 = qcOriginInspectedBy3 ?? "";
            qcOriginReport3 = qcOriginReport3 ?? "";
            qcOriginNotes3 = qcOriginNotes3 ?? "";

            qcOriginInspectedBy4 = qcOriginInspectedBy4 ?? "";
            qcOriginReport4 = qcOriginReport4 ?? "";
            qcOriginNotes4 = qcOriginNotes4 ?? "";

            qcDestinationInspectedBy = qcDestinationInspectedBy ?? "";
            qcDestinationReport = qcDestinationReport ?? "";
            qcDestinationNotes = qcDestinationNotes ?? "";

            qcCustomerInspectedBy = qcCustomerInspectedBy ?? "";
            qcCustomerReport = qcCustomerReport ?? "";
            qcCustomerNotes = qcCustomerNotes ?? "";
        }

        public List<ENPackingListDetail> packingListDetails { get; set; }
    }

    public class ENPackingListDetail
    {
        public void ReplaceNull()
        {
            packingDate = (packingDate == Convert.ToDateTime("01/01/0001")) ? Convert.ToDateTime("01/01/1900") : packingDate;
            
            farmID = farmID ?? "";
            farm = farm ?? "";
            pack = pack ?? "";
            categoryID = categoryID ?? "";
            category = category ?? "";
            description = description ?? "";
            qcOriginAdditionalNotes = qcOriginAdditionalNotes ?? "";
            qcDestinationAdditionalNotes = qcDestinationAdditionalNotes ?? "";
            qcCustomerAdditionalNotes = qcCustomerAdditionalNotes ?? "";
            qcPotentialClaim = qcPotentialClaim ?? "";

        }
        public int packingListDetailByVarietyID { get; set; }
        public int packingListDetailID { get; set; }
        public string farmID { get; set; }
        public string farm { get; set; }
        public int productsID { get; set; }
        public string pack { get; set; }
        public int codePackID { get; set; }
        public int packageProductID { get; set; }
        public string categoryID { get; set; }
        public string category { get; set; }
        public int varietyID { get; set; }
        public int variety { get; set; }
        public string sizeID { get; set; }
        public DateTime packingDate { get; set; }
        public int quantityBoxes { get; set; }
        public decimal mountKgLocalCurrency { get; set; }
        public decimal mountKgUSD { get; set; }
        public decimal subTotalLocalCurrency { get; set; }
        public decimal subTotalUSD { get; set; }
        public string description { get; set; }
        public int qcOriginAprobe { get; set; }
        public int qcOriginOutSpecs { get; set; }
        public int qcOriginRejected { get; set; }
        public int qcOriginQuantityBoxes { get; set; }
        public string qcOriginAdditionalNotes { get; set; }
        public int qcDestinationAprobe { get; set; }
        public int qcDestinationOutSpecs { get; set; }
        public int qcDestinationRejected { get; set; }
        public int qcDestinationQuantityBoxes { get; set; }
        public string qcDestinationAdditionalNotes { get; set; }
        public int qcCustomerAprobe { get; set; }
        public int qcCustomerOutSpecs { get; set; }
        public int qcCustomerRejected { get; set; }
        public int qcCustomerQuantityBoxes { get; set; }
        public string qcCustomerAdditionalNotes { get; set; }
        public string qcPotentialClaim { get; set; }

        public int userModifyID { get; set; }
    }
}
