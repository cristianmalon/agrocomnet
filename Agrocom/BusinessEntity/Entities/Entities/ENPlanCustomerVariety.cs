﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENPlanCustomerVariety
    {
        public int planCustomerVarietyID { get; set; }
        public int campaignID { get; set; }
        public int customerID { get; set; }        
        public int programID { get; set; }
        public int destinationID { get; set; }
        public int kamID { get; set; }
        public int priorityID { get; set; }
        public int statusConfirmID { get; set; }
        public int sort { get; set; }
        public int varietyID { get; set; }
        public string categoryID { get; set; }
        public string category { get; set; }
        public int formatID { get; set; }
        public int packageProductID { get; set; }
        public int brandID { get; set; }
        public int presentationID { get; set; }
        public int labelID { get; set; }
        public string codepack { get; set; }
        public int codepackID { get; set; }
        public int incotermID { get; set; }
        public int conditionPaymentID { get; set; }
        public int paymentTermID { get; set; }
        public int userID { get; set; }
        public DateTime dateCreated { get; set; }
        public int statusID { get; set; }
        public int padlock { get; set; }
        public int plu { get; set; }
        public int viaID { get; set; }
        public string marketID { get; set; }
        public int consigneeID { get; set; }
        public string sizeID { get; set; }
        public decimal price { get; set; }        
        public List<ENPlanBoxWeek> planBoxWeek { get; set; }
    }

    public class ENPlanBoxWeek
    {
        public int planBoxWeekID { get; set; }
        public int planCustomerVarietyID { get; set; }
        public int projectedWeekID { get; set; }
        public decimal boxes { get; set; }
        public decimal price { get; set; }
        public int userID { get; set; }
    }
}
