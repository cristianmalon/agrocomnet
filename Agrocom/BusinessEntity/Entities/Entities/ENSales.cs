﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENSales
    {
        public void ReplaceNull()
        {
            referenceNumber = referenceNumber ?? "";
            commentary = commentary ?? "";

            if (details.Count > 0)
            {
                foreach (var item in details)
                {
                    item.sizes = item.sizes ?? "";
                    item.codePack = item.codePack ?? "";
                }
            }
        }
        public int saleID { get; set; }
        public string referenceNumber { get; set; }
        public int responsible { get; set; }
        public int userCreated { get; set; }
        public string dateCreated { get; set; }
        public int projectedWeekID { get; set; }
        public int customerID { get; set; }
        public int statusID { get; set; }
        public int destinationID { get; set; }
        public int wowID { get; set; }
        public int destinationPortID { get; set; }
        public int planCustomerWeekID { get; set; }
        public int statusPC { get; set; }
        public int campaignID { get; set; }
        public int originPC { get; set; }
        public string commentary { get; set; }
        public int userModifyID { get; set; }
        public string dateModified { get; set; }
        public List<ENSaleDetail> details { get; set; }
    }

    public class ENSaleDetail
    {
        public int saleDetailID { get; set; }
        public int saleID { get; set; }
        public decimal quantity { get; set; }
        public decimal price { get; set; }
        public string paymentTerm { get; set; }
        public int productsID { get; set; }
        public int paymentTermID { get; set; }
        public int conditionPaymentID { get; set; }
        public int incotermID { get; set; }
        public int statusID { get; set; }
        public string categoryID { get; set; }
        public int varietyID { get; set; }
        public int codePackID { get; set; }
        public int presentationID { get; set; }
        public int finalCustomerID { get; set; }        
        public int packageProductID { get; set; }
        public int flexible { get; set; }
        public decimal quantityMinimun { get; set; }
        public decimal priceBilling { get; set; }
        public string varieties { get; set; }
        public string sizes { get; set; }
        public string codePack { get; set; }
        public bool workOrder { get; set; }

    }
}
