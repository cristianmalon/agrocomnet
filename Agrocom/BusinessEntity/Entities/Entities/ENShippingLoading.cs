﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENShippingLoading
    {
        public void ReplaceNull()
        {
            abbreviation = abbreviation ?? "";
            nroPackingList = nroPackingList ?? "";
            abbreviationPacking = abbreviationPacking ?? "";
            contact = contact ?? "";
            ventilation = ventilation ?? "";
            //humedity = humedity ?? "";
            //quest = quest ?? "";
            comments = comments ?? "";
            if (details.Count > 0)
            {
                foreach (var item in details)
                {
                    item.sizeID = item.sizeID ?? "";
                    item.size = item.size ?? "";
                }
            }
        }
        public int shippingLoadingID { get; set; }
        public int orderProductionID { get; set; }
        public int managerID { get; set; }
        public string manager { get; set; }
        public int logisticOperatorID { get; set; }
        public string logisticOperator { get; set; }
        public int nroPackingListID { get; set; }
        public string abbreviation { get; set; }
        public string abbreviationPacking { get; set; }
        public string nroPackingList { get; set; }
        public string dateCreated { get; set; }
        public string contact { get; set; }
        public int terminalID { get; set; }
        public string terminal { get; set; }
        public int shippingCompanyID { get; set; }
        public string shippingCompany { get; set; }
        public string orderProduction { get; set; }
        public string booking { get; set; }
        public string veseel { get; set; }
        public string freightCondition { get; set; }
        public string regime { get; set; }
        public string tariffHeadingID { get; set; }
        public string tariffHeading { get; set; }
        public int originDestinationID { get; set; }
        public string originDestination { get; set; }
        public int arriveDestinationID { get; set; }
        public string arriveDestination { get; set; }
        public string etd { get; set; }
        public string eta { get; set; }
        public int certificates { get; set; }
        public int shipperID { get; set; }
        public string shipper { get; set; }
        public int customerID { get; set; }
        public string customer { get; set; }
        public int consigneeID { get; set; }
        public string consignee { get; set; }
        public int notifyID { get; set; }
        public string notify { get; set; }
        public string processPlantID { get; set; }
        public string processPlant { get; set; }
        public decimal vgm { get; set; }
        public int coldTreatment { get; set; }
        public int controlledAtmosphere { get; set; }
        public decimal temperature { get; set; }
        public string ventilation { get; set; }
        public bool humedity { get; set; }
        public bool quest { get; set; }
        public string datetoPlantEntry { get; set; }
        public string departureDate { get; set; }
        public string arrivalTime { get; set; }
        public string terminalEntryDate { get; set; }
        public string comments { get; set; }
        public int statusID { get; set; }
        public int campaignID { get; set; }
        public int shippingLoadingBLId { get; set; }
        public string bl { get; set; }
        public string initial { get; set; }
        public string final { get; set; }
        public List<ENShippingLoadingDetail> details { get; set; }
    }

    public class ENShippingLoadingDetail
    {
        public void ReplaceNull()
        {
            sizeID = sizeID ?? "";
            size = size ?? "";
        }
        public int shippingLoadingDetailID { get; set; }
        public int shippingLoadingID { get; set; }
        public string categoryID { get; set; }
        public string category { get; set; }
        public string varietyID { get; set; }
        public string variety { get; set; }
        public int brandID { get; set; }
        public string brand { get; set; }
        public int formatID { get; set; }
        public string format { get; set; }
        public int packageProductID { get; set; }
        public string packageProduct { get; set; }
        public int presentationID { get; set; }
        public string presentation { get; set; }
        public int labelID { get; set; }
        public string label { get; set; }
        public int codePackID { get; set; }
        public string codePack { get; set; }
        public string sizeID { get; set; }
        public string size { get; set; }
        public string codePack2 { get; set; }
        public decimal quantity { get; set; }
        public int statusID { get; set; }
        public string marketID { get; set; }
        public int viaID { get; set; }
        public decimal pallets { get; set; }
    }
}
