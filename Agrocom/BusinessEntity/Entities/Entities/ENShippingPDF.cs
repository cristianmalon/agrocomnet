﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENShippingPDF
    {        
        public string manager { get; set; }
        public string logisticOperator { get; set; }
        public string terminal { get; set; }
        public string shippingLine { get; set; }
        public string nroPackingList { get; set; }
        public string orderProduction { get; set; }
        public string date { get; set; }
        public string contact { get; set; }
        public string booking { get; set; }
        public string shipper { get; set; }
        public string shipperaddress { get; set; }
        public string shipperphone { get; set; }
        public string shipperemail { get; set; }
        public string shippercontact { get; set; }
        public string customer { get; set; }
        public string customeraddress { get; set; }
        public string customerphone { get; set; }
        public string customeremail { get; set; }
        public string customercontact { get; set; }
        public string customerUSCI { get; set; }
        public string consignee { get; set; }
        public string consigneeaddress { get; set; }
        public string consigneephone { get; set; }
        public string consigneeemail { get; set; }
        public string consigneecontact { get; set; }
        public string consigneeUSCI { get; set; }
        public string notify { get; set; }
        public string notifyaddress { get; set; }
        public string notifyphone { get; set; }
        public string notifyemail { get; set; }
        public string notifycontact { get; set; }
        public string notifyUSCI { get; set; }
        public string veseel { get; set; }
        public string regime { get; set; }
        public string freightCondition { get; set; }
        public string tariffHeading { get; set; }
        public string portShipping { get; set; }
        public string portDestination { get; set; }
        public string etd { get; set; }
        public string eta { get; set; }
        public int certificates { get; set; }
        public string datetoPlantEntry { get; set; }
        public string departureDate { get; set; }
        public string arrivalTime { get; set; }
        public string terminalEntryDate { get; set; }
        public string comments { get; set; }
        public string bl { get; set; }
        public decimal temperature { get; set; }
        public string ventilation { get; set; }
        public bool humedity { get; set; }
        public bool quest { get; set; }
        public int coldTreatment { get; set; }
        public int controlledAtmosphere { get; set; }
        public string processPlant { get; set; }
        public string processPlantaddress { get; set; }
        //public decimal vgm { get; set; }               
        public List<ENShippingPDFDetail> details { get; set; }
    }

    public class ENShippingPDFDetail
    {       
        //public string category { get; set; }        
        public string variety { get; set; }        
        public string brand { get; set; }        
        public string codePack { get; set; }
        public decimal quantity { get; set; }
    }
}

