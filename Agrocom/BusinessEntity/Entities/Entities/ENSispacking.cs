﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENSispacking
    {
        public string Campana { get; set;}
        public string NroPackingList { get; set;}
        public string SenasaSeal { get; set;}
        public string CustomSeal { get; set;}
        public string Thermographs { get; set;}
        public string ThermographsLocation { get; set;}
        public string Sensors { get; set;}
        public string SensorLocation { get; set;}
        public string TotalBoxes { get; set;}
        public string PackingLoadingDate { get; set;}
        public string Container { get; set;}
        public string ShipmentID { get; set;}

        public string packingListNumber { get; set; }

        public List<ShipmentDetails> ShipmentDetails { get; set; }

    }

    public class ShipmentDetails {
        public string Pallet { get; set; }
        public string Variety { get; set; }
        public string CodePack { get; set; }
        public string Weight { get; set; }
        public string Label { get; set; }
        public string Pack { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Cat { get; set; }
        public string Planta { get; set; }
        public string Package { get; set; }
        public string Brand { get; set; }
        public string Presentation { get; set; }
        public string PackingDate { get; set; }
        public string NumberBoxes { get; set; }
    }
}
