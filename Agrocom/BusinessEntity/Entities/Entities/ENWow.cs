﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ENWow
    {

        public int wowID { get; set; }
        public int customerID { get; set; }
        public int destinationID { get; set; }
        public int directInvoice { get; set; }
        public string directInvoiceMoreInfo { get; set; }
        public string certificationID { get; set; }
        public int documents { get; set; }

        public int documentsExportSea { get; set; }
        public int documentsExportAir { get; set; }

        public int documentsMoreInfo { get; set; }
        public string documentsInfo { get; set; }
        public string packingListID { get; set; }
        public string packingListMoreInfo { get; set; }
        public int insuranceCertificate { get; set; }

        public int phytosanitary { get; set; }
        public int phytosanitaryMoreInfo { get; set; }
        public string phytosanitaryInfo { get; set; }

        public int certificateOrigin { get; set; }
        public int certificateOriginMoreInfo { get; set; }
        public string certificateOriginInfo { get; set; }



        public string moreInfo { get; set; }
        public string consigneeID { get; set; }
        public string consigneeAddress { get; set; }
        public string consigneeContact { get; set; }
        public string consigneePhone { get; set; }
        public string consigneeOther { get; set; }

        public int notifyID { get; set; }
        public string notifyIDAddress { get; set; }
        public string notifyIDContact { get; set; }
        public string notifyIDPhone { get; set; }
        public string notifyIDOther { get; set; }

        public int notifyID2 { get; set; }
        public string notifyID2Address { get; set; }
        public string notifyID2Contact { get; set; }
        public string notifyID2Phone { get; set; }
        public string notifyID2Other { get; set; }

        public string cropID { get; set; }
        public void replaceNull()
        {
            directInvoiceMoreInfo = directInvoiceMoreInfo ?? "";
            certificationID = certificationID ?? "";
            //directInvoice = directInvoice ?? "";
            //documents = documents ?? "";
            directInvoiceMoreInfo = directInvoiceMoreInfo ?? "";
            //documentsMoreInfo = documentsMoreInfo ?? "";
            documentsInfo = documentsInfo ?? "";
            packingListID = packingListID ?? "";
            packingListMoreInfo = packingListMoreInfo ?? "";
            //insuranceCertificate = insuranceCertificate ?? "";
            //phytosanitary = phytosanitary ?? "";
            //phytosanitaryMoreInfo = phytosanitaryMoreInfo ?? "";
            phytosanitaryInfo = phytosanitaryInfo ?? "";
            //certificateOrigin = certificateOrigin ?? "";
            //certificateOriginMoreInfo = certificateOriginMoreInfo ?? "";
            certificateOriginInfo = certificateOriginInfo ?? "";
            moreInfo = moreInfo ?? "";
            consigneeID = consigneeID ?? "";
            consigneeAddress = consigneeAddress ?? "";
            consigneeContact = consigneeContact ?? "";
            consigneePhone = consigneePhone ?? "";
            consigneeOther = consigneeOther ?? "";

            //notifyID = notifyID ?? "";
            notifyIDAddress = notifyIDAddress ?? "";
            notifyIDContact = notifyIDContact ?? "";
            notifyIDPhone = notifyIDPhone ?? "";
            notifyIDOther = notifyIDOther ?? "";

            //notifyID2 = notifyID2 ?? "";
            notifyID2Address = notifyID2Address ?? "";
            notifyID2Contact = notifyID2Contact ?? "";
            notifyID2Phone = notifyID2Phone ?? "";
            notifyID2Other = notifyID2Other ?? "";

            //notifyID3 = notifyID3 ?? "";
            notifyID3Address = notifyID3Address ?? "";
            notifyID3Contact = notifyID3Contact ?? "";
            notifyID3Phone = notifyID3Phone??"";
            notifyID3Other = notifyID3Other ?? "";

            docsCopyEmailInfo = docsCopyEmailInfo ?? "";
            noted = noted ?? "";
            cropID = cropID ?? "";
        }

        public int notifyID3 { get; set; }
        public string notifyID3Address { get; set; }
        public string notifyID3Contact { get; set; }
        public string notifyID3Phone { get; set; }
        public string notifyID3Other { get; set; }

        public string docsCopyEmailInfo { get; set; }
        public string noted { get; set; }

        public int userID { get; set; }

    }
}
