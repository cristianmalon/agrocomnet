using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class ForecastByVarietyDetail
    {
        public string grower { get;set; }
        public string farm { get; set; }
        public string brand { get; set; }
        public string varietyID { get; set; }
        public string variety { get; set; }
        public string size { get; set; }
        public string year { get; set; }
        public string week { get; set; }
        public decimal boxpallet { get; set; }
        public decimal palletavailable { get; set; }
        public decimal boxavailable { get; set; }
        public decimal available { get; set; }
        public decimal kgbox { get; set; }
        public string format { get; set; }
        public int projectedProductionSizeCategoryID { get; set; }
        public string growerID { get; set; }
        public string farmID { get; set; }
        public List<Variety> varieties { get; set; }
        public List<Size> sizes { get; set; }

        public int saleDetailID { get; set; }
        public string categoryID { get; set; }
    }
}
