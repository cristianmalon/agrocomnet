﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class MassLoadProduct
    {
        public List<MassLoadProductDet> MassLoadProductDetail { set; get; }
    }
    public class MassLoadProductDet
    {
        public string ID { set; get; }
        public int specsCustomerCropID { set; get; }
        public int productNetsuiteID { set; get; }
        public decimal price { set; get; }
        public int priority { set; get; }
        public int minimun { set; get; }
        public int maximun { set; get; }
        public int statusID { set; get; }
    }
}
