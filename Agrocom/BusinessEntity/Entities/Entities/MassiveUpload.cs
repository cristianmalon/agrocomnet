﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class MassiveUploadCab
    {
        public List<MassiveUploadDet> massiveUploadDetail { set; get; }
    }

    public class MassiveUploadDet
    {
        public string ID { set; get; }
        public string LotID { set; get; }
        public string CategoryID { set; get; }
        public string SizeID { set; get; }
        public string Campaign { set; get; }
        public string WeekStart { set; get; }
        public string mount1 { set; get; }
        public string mount2 { set; get; }
        public string mount3 { set; get; }
        public string mount4 { set; get; }
    }
}
