﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Menu
    {
        public int menuID { get; set; }
        public string description { get; set; }
        public int father { get; set; }
        public int order { get; set; }
        public string icon { get; set; }
        public string login { get; set; }
        public string typeUser { get; set; }
        public string controller { get; set; }
        public string action { get; set; }
        public List<Menu> menusChildren { get; set; }
    }
}
