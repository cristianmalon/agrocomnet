﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class OrderProductionCab
    {
        public List<OrderProductionDet> orderProductionDetail { set; get; }
    }

    public class OrderProductionDet
    {
        public string Quantity { set; get; }
        public string categoryID { set; get; }
        public string varietyID { set; get; }
        public string formatID { set; get; }
        public string packageProductID { set; get; }
        public string brandID { set; get; }
        public string presentationID { set; get; }
        public string labelID { set; get; }
        public string codepackID { set; get; }
        public string codepack { set; get; }
        public string sizeID { set; get; }
        public string priceBox { set; get; }
        public string workOrder { set; get; }

    }
}
