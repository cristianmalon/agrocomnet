﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class PackingListNisira
    {
        public string NroPackingList { get; set; }
        public string id { get; set; }
        public string Exportador { get; set; }
        public string Productor { get; set; }
        public string invoice { get; set; }
        public string remision { get; set; }
        public string precintosenasa { get; set; }
        public string precintoaduana { get; set; }
        public string thermoregistro { get; set; }
        public string cliente { get; set; }
        public string fechatraslado { get; set; }
        public string container { get; set; }
        public string ptoorigen { get; set; }
        public string ptodestino { get; set; }
        public string fechaembarque { get; set; }
        public string fechaarribo { get; set; }
        public string transportista { get; set; }
        public List<packingListDetalle> PackingListDetalle { get; set; }
    }

    public class packingListDetalle
    {
        public int item { get; set; }
        public string nropaleta { get; set; }
        public string variedad { get; set; }
        public string peso { get; set; }
        public string etiqueta { get; set; }
        public string desc_emb { get; set; }
        public string desc_emb_la { get; set; }
        public string calibre { get; set; }
        public decimal total { get; set; }
        public string fecha_proceso { get; set; }
        public string fundo { get; set; }
    }
}
