﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class PlanComercialCab
    {
        public List<PlanComercialDet> planComercialDetail { set; get; }
    }

    public class PlanComercialDet
    {
        public int Id { set; get; }
        public int PlanCustomerVarietyID { set; get; }
        public int VarietyID { set; get; }
        public string CategoryID { set; get; }
        public int CustomerID { set; get; }
        public string Program { set; get; }
        public string Priority { set; get; }
        public int DestinationID { set; get; }
        public string CodePack { set; get; }
        public int BoxesPerPallet { set; get; }
        public int ToProcess { set; get; }
        public int Pallets { set; get; }
        public int Group { set; get; }

    }
}
