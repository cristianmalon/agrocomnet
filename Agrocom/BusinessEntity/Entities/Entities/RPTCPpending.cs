﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTCPpending
    {
        public string Grower { get; set; }
        public int year { get; set; }
        public string Week { get; set; }
        public decimal TOTALF { get; set; }
        public decimal TOTALP { get; set; }
    }
}
