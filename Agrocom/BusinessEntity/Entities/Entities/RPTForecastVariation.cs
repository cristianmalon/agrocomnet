﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTForecastVariation
    {
        public string Grower { get; set; }
        public string Farm { get; set; }
        public string WeekActually { get; set; }
        public string Variety { get; set; }
        public string LastUpdate { get; set; }
        public string Brand { get; set; }        
        public string T12 { get; set; }
        public string T14 { get; set; }
        public string T16 { get; set; }
        public string T18 { get; set; }
        public string T20 { get; set; }

    }
}
