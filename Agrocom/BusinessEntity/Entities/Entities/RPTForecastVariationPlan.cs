﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTForecastVariationPlan
    {
        public string Grower { get; set; }
        public int Year { get; set; }
        public int Week { get; set; }
        public decimal TotalForecast { get; set; }
        public decimal CommercialPlan { get; set; }
        public decimal DifferenceinKG { get; set; }
    }
}
