﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTForecastWithoutPO
    {
        public string Origin { get; set; }
        public string Grower { get; set; }
        public string Farm { get; set; }
        public decimal W26 { get; set; }
        public decimal W27 { get; set; }
        public decimal W28 { get; set; }
        public decimal W29 { get; set; }
        public decimal W30 { get; set; }
        public decimal W31 { get; set; }
        public decimal W32 { get; set; }
        public decimal W33 { get; set; }
        public decimal W34 { get; set; }
        public decimal W35 { get; set; }
        public decimal W36 { get; set; }
        public decimal W37 { get; set; }
        public decimal W38 { get; set; }
        public decimal W39 { get; set; }
        public decimal W40 { get; set; }
        public decimal W41 { get; set; }
        public decimal W42 { get; set; }
        public decimal W43 { get; set; }
        public decimal W44 { get; set; }
        public decimal W45 { get; set; }
        public decimal W46 { get; set; }
        public decimal W47 { get; set; }
        public decimal W48 { get; set; }
        public decimal W49 { get; set; }
        public decimal W50 { get; set; }
        public decimal W51 { get; set; }
        public decimal W52 { get; set; }
        public decimal W1 { get; set; }
    }
}
