﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTPLMissing
    {
        public string production_order { get; set; }
        public string packinglist { get; set; }
        public string responsable { get; set; }
        public string validation { get; set; }
    }
}
