﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTPOrejected
    {
        public string Grower { get; set; }
        public string PO { get; set; }
        public string DepartureWeek { get; set; }
        public string Customer { get; set; }
        public string Destination { get; set; }
        public string Via { get; set; }
        public string Brands { get; set; }
        public string Varieties { get; set; }
        public string Sizes { get; set; }
        public decimal TotalKG { get; set; }
        public string RejectionDate { get; set; }
        public string rejectionComments { get; set; }
    }
}
