﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Common.Entities
{
    public class RPTPOvariationBrand
    {
        public string Grower { get; set; }
        public string Brand { get; set; }
        public int week { get; set; }
        public decimal diference { get; set; }
    }
}
