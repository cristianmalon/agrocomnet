﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTSRrejected
    {        
        public string SaleRequest { get; set; }
        public string Market { get; set; }
        public string Customer { get; set; }
        public int DepartureWeek { get; set; }
        public string Via { get; set; }
        public decimal Boxes { get; set; }
        public string rejectionComments { get; set; }
        public string rejectedBy { get; set; }
        public string rejectionDate { get; set; }
    }
}
