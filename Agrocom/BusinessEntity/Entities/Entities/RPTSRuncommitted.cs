﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class RPTSRuncommitted
    {
        public string SaleRequest { get; set; }
        public string DepartureWeek { get; set; }
        public string Customer { get; set; }
        public string Destination { get; set; }
        public string Via { get; set; }
        public string Brands { get; set; }
        public string Varieties { get; set; }
        public string Sizes { get; set; }
        public string Codepack { get; set; }
        public decimal Boxes { get; set; }
        public decimal TotalUSD { get; set; }
        public string RejectedBy { get; set; }
        public string RejectionDate { get; set; }
    }
}
