using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Size
    {
        public string sizeID { get; set; }
        public string size { get; set; }
        public string code { get; set; }
    }
}
