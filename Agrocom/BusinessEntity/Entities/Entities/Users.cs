﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Users
    {
        public string token { set; get; }
        public int userID { get; set; }
        public string login { get; set; }
        public string DNI { get; set; }
        public string paternalLastName { get; set; }
        public string maternalLastName { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public DateTime originalDate { get; set; }
        public DateTime lastEntryDate { get; set; }
        public string email { get; set; }
        public string passwdHash { get; set; }
        public int statusID { get; set; }
        public string usertypeID { get; set; }
        public string SOwithoutProgram { get; set; }
        public string CustomerID_SOwithoutProgram { get; set; }
        public string ConsigneeID_SOwithoutProgram { get; set; }
        public List<EUserMenu> menu { set; get; }
    }

    public class EUserMenu
    {
        public string optionMenu          { set; get; }
        public string controller    { set; get; }
        public string action        { set; get; }
        public string cropid        { set; get; }
    }
}
