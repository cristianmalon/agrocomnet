using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Variety
    {
        public int varietyID { get; set; }
        public string variety { get; set; }
    }
}
