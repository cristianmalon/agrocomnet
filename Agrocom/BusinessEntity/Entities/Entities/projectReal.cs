﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class projectReal
    {
        public string condicion { get; set; }
        public decimal totalKilos { get; set; }
        public string fundo { get; set; }
        public string variedad { get; set; }
        public string diaSemana { get; set; }
    }
}
