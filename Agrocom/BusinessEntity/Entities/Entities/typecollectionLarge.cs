﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class typecollectionLarge
    {
        public int int1 { get; set; }
        public int int2 { get; set; }
        public int int3 { get; set; }
        public int int4 { get; set; }
        public int int5 { get; set; }
        public int int6 { get; set; }

        public string description1 { get; set; }
        public string description2 { get; set; }
        public string description3 { get; set; }
        public string description4 { get; set; }
        public string description5 { get; set; }

        public decimal decimal1 { get; set; }
        public decimal decimal2 { get; set; }
        public decimal decimal3 { get; set; }
        public decimal decimal4 { get; set; }

    }
}
