﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.KPI
{
    public class EKpi
    {
        public List<EScheduleBoarding> lstScheduleBoarding { set; get; }
        public List<EKoreanDestination> lstKoreanDestination { set; get; }
        public List<EShippingCompanies> lstShippingCompanies { set; get; }

    }

    public class EScheduleBoarding
    {
        public string customerID { set; get; }
        public string customer { set; get; }
        public string projectedWeekID { set; get; }
        public string number { set; get; }
        public string kpi { set; get; }
        public string cropID { set; get; }
        public string campaign { set; get; }
    }

    public class EKoreanDestination
    {
        public string customerID { set; get; }
        public string customer { set; get; }
        public string kpi { set; get; }
        public string cropID { set; get; }
        public string campaign { set; get; }
    }

    public class EShippingCompanies
    {
        public string shippingCompanyID { set; get; }
        public string shippingCompany { set; get; }
        public string projectedWeekID { set; get; }
        public string number { set; get; }
        public string kpi { set; get; }
        public string cropID { set; get; }
        public string campaign { set; get; }
    }

    public class EKpiResponsive
    {
        public List<EScheduleBoardingResponse> lstScheduleBoarding { set; get; }
        public List<EKoreanDestinationResponse> lstKoreanDestination { set; get; }
        public List<EShippingCompaniesResponse> lstShippingCompanies { set; get; }

    }


    public class EScheduleBoardingResponse
    {
        public string customerID { set; get; }
        public string customer { set; get; }
        public string projectedWeekID { set; get; }
        public string number { set; get; }
        public string kpi { set; get; }
        public string cropID { set; get; }
        public string campaign { set; get; }
    }

    public class EKoreanDestinationResponse
    {
    }

    public class EShippingCompaniesResponse
    {
        public string shippingCompanyID { set; get; }
        public string shippingCompany { set; get; }
        public string projectedWeekID { set; get; }
        public string number { set; get; }
        public string kpi { set; get; }
        public string cropID { set; get; }
        public string campaign { set; get; }
    }
}
