﻿using System.Collections.Generic;

namespace Common.Security
{
    public class EUserResponse
    {
        public string userID { get; set; }
        public string Login { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        //public string Rol { get; set; }
        //public string passwdHash { get; set; }
        //public string statusID { get; set; }
        public string token { get; set; }
        public string usertypeID { get; set; }
        public string SOwithoutProgram { get; set; }
        public string CustomerID_SOwithoutProgram { get; set; }
        public string ConsigneeID_SOwithoutProgram { get; set; }
        public List<EUserMenuResponse> menu { set; get; }
    }

    public class EUserMenuResponse { 
        public string optionMenu { set; get; }
        public string controller { set; get; }
        public string action { set; get; }
        public string cropid { set; get; }
    }
}
