﻿using Agrocom.BusinessCore.BL.Interface;
using Agrocom.Data.DA.Implementation;
using Agrocom.Data.DA.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrocom.BusinessCore.BL.Implementation
{
    public class ErrorLogic : IErrorLogic
    {
        private IErrorData _errorData;
        private readonly IConfiguration _configuration;

        public ErrorLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _errorData = new ErrorData(_configuration);
        }

        public ErrorLogic(string _DbConexion)
        {
            _errorData = new ErrorData(_DbConexion);
        }

        public bool LogErrorInsert(string LogErrorProyect, string LogErrorDescription, string LogErrorService, string LogErrorParams)
        {
            return _errorData.LogErrorInsert(LogErrorProyect, LogErrorDescription, LogErrorService, LogErrorParams);
        }
    }
}
