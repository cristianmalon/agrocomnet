﻿using Agrocom.BusinessCore.BL.Interface;
using Agrocom.Data.DA.Implementation;
using Common.Notification;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Agrocom.BusinessCore.BL.Implementation
{
    public class NotificationLogic : INotificationLogic
    {
        private NotificationData _notificationData;
        private readonly IConfiguration _configuration;

        public NotificationLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _notificationData = new NotificationData(_configuration);
        }

        public NotificationLogic(string _DbConexion)
        {
            _notificationData = new NotificationData(_DbConexion);
        }

        public bool GetNotificationsSplash(string sUsertypeID, string sCropId, out List<ENotification> lstNotification)
        {
            return _notificationData.GetNotificationsSplash(sUsertypeID, sCropId, out lstNotification);
        }
    }
}
