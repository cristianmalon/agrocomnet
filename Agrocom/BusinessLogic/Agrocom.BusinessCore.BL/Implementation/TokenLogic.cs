﻿using Agrocom.BusinessCore.BL.Interface;
using Agrocom.Data.DA.Implementation;
using Agrocom.Data.DA.Interface;
using Microsoft.Extensions.Configuration;
using System;

namespace Agrocom.BusinessCore.BL.Implementation
{
    public class TokenLogic : ITokenLogic
    {
        private ITokenData _tokenData;
        private readonly IConfiguration _configuration;

        public TokenLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _tokenData = new TokenData(_configuration);
        }

        public TokenLogic(string _DbConexion)
        {
            _tokenData = new TokenData(_DbConexion);
        }

        public bool ExistToken(string Token)
        {
            return _tokenData.ExistToken(Token);
        }
    }
}
