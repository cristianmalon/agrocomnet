﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrocom.BusinessCore.BL.Interface
{
    public interface IErrorLogic
    {
        public bool LogErrorInsert(
            string LogErrorProyect,
            string LogErrorDescription,
            string LogErrorService,
            string LogErrorParams
            );
    }
}
