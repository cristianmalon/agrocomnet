﻿using Common.Notification;
using System.Collections.Generic;

namespace Agrocom.BusinessCore.BL.Interface
{
    interface INotificationLogic
    {
        public bool GetNotificationsSplash(string sUsertypeID, string sCropId, out List<ENotification> lstNotification);
    }
}
