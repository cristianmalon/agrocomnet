﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrocom.BusinessCore.BL.Interface
{
    interface ITokenLogic
    {
        public bool ExistToken(string Token);
    }
}
