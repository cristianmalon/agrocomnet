﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class ClientLogic : IClientLogic
    {
        private IClientData _clientData;
        private readonly IConfiguration _configuration;

        public ClientLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _clientData = new ClientData(_configuration);
        }

        public bool Compare(DataTable dttClient)
        {
            return _clientData.Compare(dttClient);
        }

        public bool List(out DataTable _objDtt)
        {
            return _clientData.List(out _objDtt);
        }

        public bool Update(DataTable dttClient)
        {
            return _clientData.Update(dttClient);
        }
    }
}
