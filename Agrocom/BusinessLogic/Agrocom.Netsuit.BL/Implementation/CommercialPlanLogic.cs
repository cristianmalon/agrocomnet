﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class CommercialPlanLogic : ICommercialPlanLogic
    {
        private ICommercialPlanData _commercialPlanData;
        private readonly IConfiguration _configuration;

        public CommercialPlanLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _commercialPlanData = new CommercialPlanData(_configuration);
        }

        public bool Insert(DataTable dttCommercialPlan)
        {
            return _commercialPlanData.Insert(dttCommercialPlan);
        }

        public bool List(out DataSet _objDts)
        {
            return _commercialPlanData.List(out _objDts);
        }

        public DataTable GetPlanCustomerVariety(int campaignID, string opt, int projectedWeekID)
        {
            return _commercialPlanData.GetPlanCustomerVariety( campaignID,  opt, projectedWeekID);
        }
    }
}
