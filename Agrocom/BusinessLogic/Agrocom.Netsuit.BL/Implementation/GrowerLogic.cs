﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class GrowerLogic : IGrowerLogic
    {
        private IGrowerData _growerData;
        private readonly IConfiguration _configuration;

        public GrowerLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _growerData = new GrowerData(_configuration);
        }

        public bool Insertar(DataTable dttLocation)
        {
            return _growerData.Insertar(dttLocation);
        }
    }
}
