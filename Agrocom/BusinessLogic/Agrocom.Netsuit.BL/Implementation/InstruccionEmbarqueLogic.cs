﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class InstruccionEmbarqueLogic : IInstruccionEmbarqueLogic
    {
        private IInstruccionEmbarqueData _instruccionEmbarqueData;
        private readonly IConfiguration _configuration;

        public InstruccionEmbarqueLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _instruccionEmbarqueData = new InstruccionEmbarqueData(_configuration);
        }

        public bool Insert(DataTable dttInstruccionEmbarque)
        {
            return _instruccionEmbarqueData.Insert(dttInstruccionEmbarque);
        }

        public bool List(out DataSet _objDts)
        {
            return _instruccionEmbarqueData.List(out _objDts);
        }
    }
}
