﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class LocationLogic: ILocationLogic
    {
        private ILocationData _locationData;
        private readonly IConfiguration _configuration;

        public LocationLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _locationData = new LocationData(_configuration);
        }

        public bool Insertar(DataTable dttProduct)
        {
            return _locationData.Insertar(dttProduct);
        }
    }
}
