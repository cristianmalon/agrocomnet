﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Agrocom.Netsuite.DA.Interface;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class LogisticOperatorLogic: ILogisticOperatorLogic
    {
        private ILogisticOperatorData _LogisticOperatorData;
        private readonly IConfiguration _configuration;

        public LogisticOperatorLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _LogisticOperatorData = new LogisticOperatorData(_configuration);
        }
        public bool Insert(DataTable dttLogisticOperator)
        {
            return _LogisticOperatorData.Insert(dttLogisticOperator);
        }
    }
    
}
