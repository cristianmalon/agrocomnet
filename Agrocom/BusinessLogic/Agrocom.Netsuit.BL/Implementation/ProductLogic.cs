﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class ProductLogic : IProductLogic
    {
        private IProductData _productData;
        private readonly IConfiguration _configuration;

        public ProductLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _productData = new ProductData(_configuration);
        }

        public bool Insertar(DataTable dttProduct, DataTable dttProductSubsidiary)
        {
            return _productData.Insertar(dttProduct,  dttProductSubsidiary);
        }
    }
}
