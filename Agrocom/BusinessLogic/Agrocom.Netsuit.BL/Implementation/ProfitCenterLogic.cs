﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class ProfitCenterLogic: IProfitCenterLogic
    {
        private IProfitCenterData _profitCenterData;
        private readonly IConfiguration _configuration;

        public ProfitCenterLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _profitCenterData = new ProfitCenterData(_configuration);
        }

        public bool Insertar(DataTable dttProduct)
        {
            return _profitCenterData.Insertar(dttProduct);
        }
    }
}
