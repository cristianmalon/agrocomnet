﻿using Agrocom.Netsuite.BL.Interface;
using Agrocom.Netsuite.DA.Interface;
using Agrocom.Netsuite.DA.Implementation;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.Netsuite.BL.Implementation
{
    public class SaleOrderLogic:ISaleOrderLogic
    {
        private ISaleOrderData _saleOrderData;
        private readonly IConfiguration _configuration;

        public SaleOrderLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _saleOrderData = new SaleOrderData(_configuration);
        }

        public bool List(out DataSet _objDts)
        {
            return _saleOrderData.List(out _objDts);
        }

        public bool Insert(DataTable dttSaleOrder)
        {
            return _saleOrderData.Insert(dttSaleOrder);
        }
    }
}
