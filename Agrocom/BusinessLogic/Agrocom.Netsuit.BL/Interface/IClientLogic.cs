﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface IClientLogic
    {
        bool List(out DataTable _objDtt);
        bool Update(DataTable dttClient);
        bool Compare(DataTable dttClient);
    }
}
