﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface ICommercialPlanLogic
    {
        bool List(out DataSet _objDts);
        bool Insert(DataTable dttCommercialPlan);
        DataTable GetPlanCustomerVariety(int campaignID, string opt, int projectedWeekID);
    }
}
