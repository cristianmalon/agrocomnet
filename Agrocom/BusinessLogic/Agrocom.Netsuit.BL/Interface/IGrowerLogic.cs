﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface IGrowerLogic
    {
        bool Insertar(DataTable dttGrower);
    }
}
