﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface IInstruccionEmbarqueLogic
    {
        bool List(out DataSet _objDts);
        bool Insert(DataTable dttInstruccionEmbarque);
    }
}
