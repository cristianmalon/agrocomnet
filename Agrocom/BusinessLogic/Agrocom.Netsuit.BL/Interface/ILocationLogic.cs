﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface ILocationLogic
    {
        bool Insertar(DataTable dttLocation);
    }
}
