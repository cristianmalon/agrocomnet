﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface ILogisticOperatorLogic
    {
        bool Insert(DataTable dttLogisticOperator);
    }
}
