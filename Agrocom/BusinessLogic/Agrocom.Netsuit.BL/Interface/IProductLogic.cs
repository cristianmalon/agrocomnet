﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface IProductLogic
    {
        bool Insertar(DataTable dttProduct, DataTable dttProductSubsidiary);
    }
}
