﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface IProfitCenterLogic
    {
        bool Insertar(DataTable dttProduct);
    }
}
