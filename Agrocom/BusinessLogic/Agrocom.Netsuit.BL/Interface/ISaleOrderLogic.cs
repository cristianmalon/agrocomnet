﻿using System.Data;

namespace Agrocom.Netsuite.BL.Interface
{
    public interface ISaleOrderLogic
    {
        bool List(out DataSet _objDts);
        bool Insert(DataTable dttCommercialPlan);
    }
}
