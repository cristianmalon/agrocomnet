﻿using Agrocom.WebJob.BE;
using Agrocom.WebJob.BL.Interface;
using Agrocom.WebJob.DA.Implementation;
using Agrocom.WebJob.DA.Interface;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Agrocom.WebJob.BL.Implementation
{
    public class MailSenderLogic : IMailSenderLogic
    {
        private IMailSenderData _mailSenderData;
        private readonly IConfiguration _configuration;

        public MailSenderLogic(IConfiguration _configuration)
        {
            this._configuration = _configuration;
            _mailSenderData = new MailSenderData(_configuration);
        }

        public MailSenderLogic(string _DbConexion)
        {
            _mailSenderData = new MailSenderData(_DbConexion);
        }

        public void GetInformationMailSender(out List<EInformationMailSender> lstInformationMailSenders)
        {
            _mailSenderData.GetInformationMailSender(out lstInformationMailSenders);
        }
    }
}
