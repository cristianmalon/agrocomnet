﻿using Agrocom.WebJob.BE;
using System.Collections.Generic;

namespace Agrocom.WebJob.BL.Interface
{
    public interface IMailSenderLogic
    {
        void GetInformationMailSender(out List<EInformationMailSender> lstInformationMailSenders);
    }
}
