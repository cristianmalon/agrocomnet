﻿using Agrocom.Data.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Data.DA.Implementation
{
    public class ErrorData : IErrorData
    {
        private readonly string context;

        public ErrorData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public ErrorData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public bool LogErrorInsert(string LogErrorProyect, string LogErrorDescription, string LogErrorService, string LogErrorParams)
        {
            SqlConnection objCnx = null;
            SqlDataReader objDtr = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_LogErrorInsert]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PLogErrorProyect", SqlDbType.VarChar).Value = LogErrorProyect;
                    objCmd.Parameters.Add("@PLogErrorDescription", SqlDbType.VarChar).Value = LogErrorDescription;
                    objCmd.Parameters.Add("@PLogErrorService", SqlDbType.VarChar).Value = LogErrorService;
                    objCmd.Parameters.Add("@PLogErrorParams", SqlDbType.VarChar).Value = LogErrorParams;

                    objCnx.Open();
                    objDtr = objCmd.ExecuteReader();
                    if (!objDtr.HasRows) return bRsl;
                    while (objDtr.Read())
                        bRsl = objDtr[0].ToString().Equals("1") ? true : false;

                }
            }
            catch (System.Exception ex)
            {
                //throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objDtr != null && !objDtr.IsClosed) objDtr.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;

        }
    }
}
