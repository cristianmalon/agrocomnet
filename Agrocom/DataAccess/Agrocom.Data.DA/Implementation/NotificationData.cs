﻿using Agrocom.Data.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;
using Common.Notification;
using System.Collections.Generic;

namespace Agrocom.Data.DA.Implementation
{
    public class NotificationData: INotificationData
    {
        private readonly string context;

        public NotificationData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public NotificationData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public bool GetNotificationsSplash(string sUsertypeID, string sCropId, out List<ENotification> lstNotification)
        {
            SqlDataReader result = null;
            SqlConnection objCnx = null;
            var bRsl = false;

            try
            {
                objCnx = new SqlConnection(context);
                using (var objCmd = new SqlCommand("[package].[sp_Notification_GetPendientsByTypeUser]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@vUsertypeID", SqlDbType.Char, 3).Value = sUsertypeID;
                    objCmd.Parameters.Add("@vCropId", SqlDbType.Char, 3).Value = sCropId;

                    objCnx.Open();
                    result = objCmd.ExecuteReader();
                    lstNotification = null;
                    if (!result.HasRows) return bRsl;
                    lstNotification = new List<ENotification>();
                    while (result.Read())
                    {
                        lstNotification.Add(new ENotification()
                        {
                            requestNumber = result.GetString("RequestNumber"),
                            mailSubject = result.GetString("MailSubject"),
                            mailDescription = result.GetString("MailDescription"),
                            toEmail = result.GetString("toEmail"),
                            nextProcess = result.GetString("nextProcess"),
                        });
                    }

                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (result != null && !result.IsClosed) result.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }        
    }
}
