﻿using Agrocom.Data.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Data.DA.Implementation
{
    public class TokenData : ITokenData
    {
        private readonly string context;

        public TokenData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public TokenData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public bool ExistToken(string Token)
        {
            SqlConnection objCnx = null;
            SqlDataReader objDtr = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[audit].[sp_Security_ExistToken]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@Ptoken", SqlDbType.VarChar).Value = Token;
                    
                    objCnx.Open();
                    objDtr = objCmd.ExecuteReader();
                    if (!objDtr.HasRows) return bRsl;
                    while (objDtr.Read())
                        if (int.Parse(objDtr[0].ToString()).Equals(0)) return bRsl;

                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                //throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objDtr != null && !objDtr.IsClosed) objDtr.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
