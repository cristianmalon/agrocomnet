﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrocom.Data.DA.Interface
{
    public interface IErrorData
    {
        public bool LogErrorInsert(
            string LogErrorProyect,
            string LogErrorDescription,
            string LogErrorService,
            string LogErrorParams
            );
    }
}
