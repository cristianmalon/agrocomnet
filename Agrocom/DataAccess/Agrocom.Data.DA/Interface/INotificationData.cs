﻿using System.Collections.Generic;
using Common.Notification;

namespace Agrocom.Data.DA.Interface
{
    interface INotificationData
    {
        public bool GetNotificationsSplash(string sUsertypeID, string sCropId, out List<ENotification> lstNotification);
    }
}
