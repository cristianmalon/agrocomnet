﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrocom.Data.DA.Interface
{
    public interface ITokenData
    {
        public bool ExistToken(string Token);
    }
}
