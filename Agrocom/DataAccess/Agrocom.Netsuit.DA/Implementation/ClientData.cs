﻿using Agrocom.Netsuite.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Netsuite.DA.Implementation
{
    public class ClientData : IClientData
    {
        private readonly string context;

        public ClientData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public bool List(out DataTable _objDtt)
        {
            SqlConnection objCnx = null;
            SqlDataReader objDtr = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_ListMassiveClient]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    
                    objCnx.Open();
                    objDtr = objCmd.ExecuteReader();
                    var objDtt = new DataTable();
                    objDtt.Load(objDtr);
                    _objDtt = objDtt;
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objDtr != null && !objDtr.IsClosed) objDtr.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();
                        
                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }

        public bool Update(DataTable dttClient)
        {
            SqlConnection objCnx = null;
            var bRsl = false;   
            try
            {
                objCnx = new SqlConnection(context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_UpdateInternalidClient]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PTClientDTO", SqlDbType.Structured).Value = dttClient;

                    objCnx.Open();
                    var iNroFil = objCmd.ExecuteNonQuery();
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                if (objCnx != null && objCnx.State == ConnectionState.Open)
                {
                    objCnx.Close();

                }
            }

            return bRsl;
        }

        public bool Compare(DataTable dttClient)
        {
            SqlConnection objCnx = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_UpdateMassiveClient]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PTClient", SqlDbType.Structured).Value = dttClient;

                    objCnx.Open();
                    var iNroFil = objCmd.ExecuteNonQuery();
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
