﻿using Agrocom.Netsuite.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Netsuite.DA.Implementation
{
    public class CommercialPlanData: ICommercialPlanData
    {
        private readonly string context;

        public CommercialPlanData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public bool List(out DataSet _objDts)
        {
            SqlConnection objCnx = null;
            SqlDataAdapter objDta = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_ListCommercialPlan]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCnx.Open();
                    objDta = new SqlDataAdapter();
                    objDta.SelectCommand = objCmd;
                    var objDts = new DataSet();
                    objDta.Fill(objDts);
                    _objDts = objDts;
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }

        public bool Insert(DataTable dttCommercialPlan)
        {
            SqlConnection objCnx = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_UpdateResponseCommercialPlan]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PTPlan", SqlDbType.Structured).Value = dttCommercialPlan;

                    objCnx.Open();
                    var iNroFil = objCmd.ExecuteNonQuery();
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }

        public DataTable GetPlanCustomerVariety(int campaignID, string opt, int projectedWeekID)
        {
            SqlConnection objCnx = null;            
            DataTable dttResult = new DataTable();
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[planCustomerVarietyList]", objCnx))                                    
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PcampaignID", SqlDbType.Int).Value = campaignID;
                    objCmd.Parameters.Add("@PsearchOpt", SqlDbType.Char,3).Value = opt;
                    objCmd.Parameters.Add("@PweekID", SqlDbType.Int).Value = projectedWeekID;

                    objCnx.Open();
                    var objDta = new SqlDataAdapter();
                    objDta.SelectCommand = objCmd;
                    objDta.Fill(dttResult);
                    
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return dttResult;
        }
    }
}
