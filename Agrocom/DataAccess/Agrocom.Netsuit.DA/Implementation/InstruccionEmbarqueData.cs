﻿using Agrocom.Netsuite.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Netsuite.DA.Implementation
{
    public class InstruccionEmbarqueData: IInstruccionEmbarqueData
    {
        private readonly string context;

        public InstruccionEmbarqueData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public bool List(out DataSet _objDts)
        {
            SqlConnection objCnx = null;
            SqlDataAdapter objDta = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_ListBoardingInstruction]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCnx.Open();
                    objDta = new SqlDataAdapter();
                    objDta.SelectCommand = objCmd;
                    var objDts = new DataSet();
                    objDta.Fill(objDts);
                    _objDts = objDts;
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }

        public bool Insert(DataTable dttInstruccionEmbarque)
        {
            SqlConnection objCnx = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_UpdateResponseLoadingInstruction]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PTLoadingInstrucction", SqlDbType.Structured).Value = dttInstruccionEmbarque;

                    objCnx.Open();
                    var iNroFil = objCmd.ExecuteNonQuery();
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
