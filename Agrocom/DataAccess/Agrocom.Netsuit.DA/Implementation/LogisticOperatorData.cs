﻿using Agrocom.Netsuite.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Netsuite.DA.Implementation
{
    public class LogisticOperatorData: ILogisticOperatorData
    {
        private readonly string context;

        public LogisticOperatorData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public bool Insert(DataTable dttLogisticOperator)
        {
            SqlConnection objCnx = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_InsertMassiveLogisticOperator]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PTLogisticOperator", SqlDbType.Structured).Value = dttLogisticOperator;

                    objCnx.Open();
                    var iNroFil = objCmd.ExecuteNonQuery();
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
