﻿using Agrocom.Netsuite.DA.Interface;
using Agrocom.Config.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Agrocom.Netsuite.DA.Implementation
{
    public class ProductData : IProductData
    {
        private readonly string context;

        public ProductData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public bool Insertar(DataTable dttProduct, DataTable dttProductSubsidiary)
        {
            SqlConnection objCnx = null;
            var bRsl = false;
            try
            {
                objCnx = new SqlConnection(this.context);
                using (var objCmd = new SqlCommand("[package].[sp_Netsuit_InsertMassiveProduct]", objCnx))
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.Add("@PTProduct", SqlDbType.Structured).Value = dttProduct;
                    objCmd.Parameters.Add("@PTProductSubsidiary", SqlDbType.Structured).Value = dttProductSubsidiary;

                    objCnx.Open();
                    var iNroFil = objCmd.ExecuteNonQuery();
                    bRsl = true;
                }
            }
            catch (System.Exception ex)
            {
                throw; //new System.Exception(ex.Message);
            }
            finally
            {
                try
                {
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bRsl;
        }
    }
}
