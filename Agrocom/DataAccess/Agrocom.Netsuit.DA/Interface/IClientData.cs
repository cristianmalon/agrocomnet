﻿using System.Data;


namespace Agrocom.Netsuite.DA.Interface
{
    public interface IClientData
    {
        bool List(out DataTable _objDtt);
        bool Update(DataTable dttClient);
        bool Compare(DataTable dttClient);
    }
}
