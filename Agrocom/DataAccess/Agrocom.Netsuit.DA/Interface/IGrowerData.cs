﻿using System.Data;

namespace Agrocom.Netsuite.DA.Interface
{
    public interface IGrowerData
    {
        bool Insertar(DataTable dttProduct);
    }
}
