﻿using System.Data;

namespace Agrocom.Netsuite.DA.Interface
{
    public interface IInstruccionEmbarqueData
    {
        bool List(out DataSet _objDts);
        bool Insert(DataTable dttInstruccionEmbarque);
    }
}
