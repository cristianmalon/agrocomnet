﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agrocom.Netsuite.DA.Interface
{
    public interface ILogisticOperatorData
    {
        bool Insert(DataTable dttLogisticOperator);
    }
}
