﻿using System.Data;

namespace Agrocom.Netsuite.DA.Interface
{
    public interface IProductData
    {
        bool Insertar(DataTable dttProduct, DataTable dttProductSubsidiary);
    }
}
