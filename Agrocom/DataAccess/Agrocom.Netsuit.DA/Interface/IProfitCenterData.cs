﻿using System.Data;


namespace Agrocom.Netsuite.DA.Interface
{
    public interface IProfitCenterData
    {
        bool Insertar(DataTable dttProduct);
    }
}
