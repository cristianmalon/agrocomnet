﻿using System.Data;

namespace Agrocom.Netsuite.DA.Interface
{
    public interface ISaleOrderData
    {
        bool List(out DataSet _objDts);
        bool Insert(DataTable dttSaleOrder);
    }
}
