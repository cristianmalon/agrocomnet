﻿using Microsoft.Extensions.Configuration;

namespace Agrocom.Config.Interface
{
    public interface IConfigManagerData
    {
        string GetConnectionString(string connectionName);

        IConfigurationSection GetConfigurationSection(string Key);
    }
}
