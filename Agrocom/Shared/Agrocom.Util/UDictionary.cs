﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Agrocom.Util
{
    public class UDictionary
    {
        private void Linearize(Dictionary<string, object> dictionary, object item, string itemName, string parentName = null)
        {
            if (item == null || item.GetType().IsPrimitive || item.GetType() == typeof(decimal) || item.GetType() == typeof(string)
                || item.GetType() == typeof(DateTime) || item.GetType() == typeof(Guid))
            {
                dictionary.Add(parentName == null ? itemName : parentName + "." + itemName, item);
            }
            else if (item.GetType().IsGenericType && item is IEnumerable)
            {
                //no implementado
            }
            else
            {
                item.GetType().GetProperties().ToList().ForEach(property =>
                {
                    Linearize(dictionary, property.GetValue(item), property.Name.ToLower(), parentName != null ? parentName + "." + itemName : itemName);
                });
            }
        }
    }
}
