﻿using System;
using System.Security.Cryptography;
using System.IO;

namespace Agrocom.Util
{
    public class UEncrypt
    {
        private static byte[] KEY_192 = { 40, 50, 60, 89, 92, 6, 217, 30, 15, 16, 44, 60, 65, 25, 14, 12, 2, 14, 10, 20, 19, 9, 14, 17 };
        private static byte[] IV_192 = { 5, 13, 52, 4, 8, 1, 17, 3, 42, 5, 82, 83, 16, 7, 29, 13, 11, 3, 22, 8, 16, 10, 11, 25 };

        public static string EncryptTripleDES(string value)
        {
            string resultado = "";
            if (value != "")
            {
                TripleDESCryptoServiceProvider cryptoProvider = new TripleDESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_192, IV_192), CryptoStreamMode.Write);
                StreamWriter sw = new StreamWriter(cs);
                sw.Write(value);
                sw.Flush();
                cs.FlushFinalBlock();
                ms.Flush();
                resultado = System.Convert.ToBase64String(ms.GetBuffer(), 0, Convert.ToInt32(ms.Length));

            }
            return resultado;
        }

        public static string DecryptTripleDES(string value)
        {
            string resultado = "";
            if (value != "")
            {
                TripleDESCryptoServiceProvider cryptoProvider = new TripleDESCryptoServiceProvider();
                byte[] buffer = Convert.FromBase64String(value);
                MemoryStream ms = new MemoryStream(buffer);
                CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_192, IV_192), CryptoStreamMode.Read);
                StreamReader sr = new StreamReader(cs);
                resultado = sr.ReadToEnd();
            }
            return resultado;
        }
    }
}
