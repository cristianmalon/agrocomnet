﻿using System.Web.Mvc;

namespace Agrocom.Areas.ClientSpecification
{
    public class ClientSpecificationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ClientSpecification";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ClientSpecification_default",
                "ClientSpecification/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}