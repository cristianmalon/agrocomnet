﻿function encryptdata(obj) {
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(obj), key,
        {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

    return encryptedpassword;
}
function decryptdata(obj) {
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    var decrypted = CryptoJS.AES.decrypt(obj, key, { iv: iv });
    return decrypted.toString(CryptoJS.enc.Utf8);
}
