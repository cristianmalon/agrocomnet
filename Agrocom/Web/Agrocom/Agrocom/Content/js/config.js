﻿function getPath() {
    let uriApiAgrocom = '';

    //LOCAL CON PUERTO 80 POR DEFECTO
    //uriApiAgrocom = "http://localhost";
    //QA
    uriApiAgrocom = "https://localhost:44302/";
    //DEV_NETSUITE
    //uriApiAgrocom = "http://apiqaagrocom.azurewebsites.net/";
    //PRODUCCION
    //uriApiAgrocom = "https://apiagrocom.azurewebsites.net/";

    return uriApiAgrocom;
};
const HOST_NETSUITE = "api/netsuite/netsuitservicecenter";
const HOST_APIAGROCOM_SPECIFICATION = "api/Specification";
const HOST_APIAGROCOM_DOCUMENTS = "api/shared/documents";
const ENDPOINT_PUT_COMMERCIAL_PLAN = "/PutTransaccionalCommercialPlanV2";
const ENDPOINT_PUT_SALE_ORDER = "/PutTransaccionalSalesOrder";
const ENDPOINT_PUT_SHIPPING_INSTRUCTION = "/PutTransaccionalLoadingInstruction";
const ENDPOINT_PUT_CLIENT = "/PutMasterClient";
const ENDPOINT_GET_CLIENT_SPECIFICATION = "/GetSpecsClientList";
const ENDPOINT_GET_SALE_ORDER_DOCUMENTS = "/GetAllPOForCustomer";
const ENDPOINT_GET_SALE_ORDER_DETAIL_DOCUMENTS = "/GetDetailPO";
const CropSelected = {
    uva: 'uva',
    blu: 'blu',
    cit: 'cit'
}
const CustomerToBeConfirm = 'To be confirmed';
const ConsigneeToBeConfirm = 'To be confirmed';