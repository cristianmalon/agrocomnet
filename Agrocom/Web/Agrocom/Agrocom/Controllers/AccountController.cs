﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agrocom.Models;

namespace Agrocom.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            Session["Loged"] = false;
            return View();
        }

        [HttpPost]
        public ActionResult Verify(Account acc)
        {
            if (acc.Name=="admin" && acc.Password == "123")
            {
                Session["IdUser"] = 2;
                acc.IdUser = 2;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Session["Loged"] = false;
                ViewBag.Error = "Usuario o contraseña invalida";
                ViewBag.User = acc.Name;
                return View("Login");
            }
            
        }
    }
}