﻿using Agrocom.Models;
using ClosedXML.Excel;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
//using Excel = Microsoft.Office.Interop.Excel;

namespace Agrocom.Controllers
{
    public class AlertsController : Controller
    {
        // GET: Alerts
        public ActionResult Index()
        {
            return View();
        }

        public string MailForecastWithoutPO()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetVarForeWithoutPO");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            //var lista = JsonConvert.DeserializeObject<List<RPTForecastWithoutPO>>(response.Content);
            //createExcelForecastWithoutPO(lista);

            return "ok";
        }
        public void createExcelForecastWithoutPO(List<RPTForecastWithoutPO> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("ForecastWithoutPO");
            ws.Cell("A1").Value = "Forecast Without Production Order Assignment in KG";
            ws.Range("A1:C1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "origin";
            ws.Cell("B2").Value = "Grower";
            ws.Cell("C2").Value = "Farm";
            ws.Cell("D2").Value = "26";
            ws.Cell("E2").Value = "27";
            ws.Cell("F2").Value = "28";
            ws.Cell("G2").Value = "29";
            ws.Cell("H2").Value = "30";
            ws.Cell("I2").Value = "31";
            ws.Cell("J2").Value = "32";
            ws.Cell("K2").Value = "33";
            ws.Cell("L2").Value = "34";
            ws.Cell("M2").Value = "35";
            ws.Cell("N2").Value = "36";
            ws.Cell("O2").Value = "37";
            ws.Cell("P2").Value = "38";
            ws.Cell("Q2").Value = "39";
            ws.Cell("R2").Value = "40";
            ws.Cell("S2").Value = "41";
            ws.Cell("T2").Value = "42";
            ws.Cell("U2").Value = "43";
            ws.Cell("V2").Value = "44";
            ws.Cell("W2").Value = "45";
            ws.Cell("X2").Value = "46";
            ws.Cell("Y2").Value = "47";
            ws.Cell("Z2").Value = "48";
            ws.Cell("AA2").Value = "49";
            ws.Cell("AB2").Value = "50";
            ws.Cell("AC2").Value = "51";
            ws.Cell("AD2").Value = "52";
            ws.Cell("AE2").Value = "01";
            ws.Range("A2:AE2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Origin;
                ws.Cell("B" + (i)).Value = item.Grower;
                ws.Cell("C" + (i)).Value = item.Farm;
                ws.Cell("D" + (i)).Value = item.W26;
                ws.Cell("E" + (i)).Value = item.W27;
                ws.Cell("F" + (i)).Value = item.W28;
                ws.Cell("G" + (i)).Value = item.W29;
                ws.Cell("H" + (i)).Value = item.W30;
                ws.Cell("I" + (i)).Value = item.W31;
                ws.Cell("J" + (i)).Value = item.W32;
                ws.Cell("K" + (i)).Value = item.W33;
                ws.Cell("L" + (i)).Value = item.W34;
                ws.Cell("M" + (i)).Value = item.W35;
                ws.Cell("N" + (i)).Value = item.W36;
                ws.Cell("O" + (i)).Value = item.W37;
                ws.Cell("P" + (i)).Value = item.W38;
                ws.Cell("Q" + (i)).Value = item.W39;
                ws.Cell("R" + (i)).Value = item.W40;
                ws.Cell("S" + (i)).Value = item.W41;
                ws.Cell("T" + (i)).Value = item.W42;
                ws.Cell("U" + (i)).Value = item.W43;
                ws.Cell("V" + (i)).Value = item.W44;
                ws.Cell("W" + (i)).Value = item.W45;
                ws.Cell("X" + (i)).Value = item.W46;
                ws.Cell("Y" + (i)).Value = item.W47;
                ws.Cell("Z" + (i)).Value = item.W48;
                ws.Cell("AA" + (i)).Value = item.W49;
                ws.Cell("AB" + (i)).Value = item.W50;
                ws.Cell("AC" + (i)).Value = item.W51;
                ws.Cell("AD" + (i)).Value = item.W52;
                ws.Cell("AE" + (i)).Value = item.W1;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailForecastWithoutPO(stream1);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailForecastWithoutPO(Stream fileExcel)
        {
            System.Net.Mail.MailMessage Email; ;

            string body = @"
                            <!DOCTYPE html>
                            <html lang='es'>  
                              <head>    
                                <title>Título de la WEB</title>    
                                <meta charset='UTF-8'>
	                            <style>
	                            table {border-collapse: collapse; border-spacing: 0; width: 100%; border: 1px solid #ddd;}
	                            th, td { text-align: left; padding: 8px;}
	                            tr:nth-child(even){background-color: #f2f2f2}
	                            </style>
                              </head>  
                              <body>    
                            <table>
	                            <tr rowspan='3'><td colspan='1'><a href='http://agrocom.azurewebsites.net'><img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='200' style='margin-left:10px;'></a></td>
		                            <td colspan='5'><h1>FORECAST WITHOUT PRODUCTION ORDER ASSIGNMENT </h1></td></tr>
	                            <tr><td colspan='6'>REPORT OF FORECAST WITHOUT PRODUCTION ORDER ASSIGNMENT HAS BEEN GENERATED</td></tr>

                            </table>
                            </h3>
                                <footer><h4>Do not reply to this email because it is automatic</h4><a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></footer>
                              </body>  
                            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert FORECAST WITHOUT PRODUCTION ORDER ASSIGNMENT", body);
            Email.Attachments.Add(new Attachment(fileExcel, "forecastWithoutPO.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);
            smtpMail.Dispose();
            return true;
        }

        public string MailForecastVariation()
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetVariationFor");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTForecastVariation>>(response.Content);
            createExcelForecastVariation(lista);

            return "ok";
        }

        public void createExcelForecastVariation(List<RPTForecastVariation> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("ForecastVariation");
            ws.Cell("A1").Value = "Harvest Forecast Variation By Week";
            ws.Range("A1:K1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Grower";
            ws.Cell("B2").Value = "Farm";
            ws.Cell("C2").Value = "Week Actually";
            ws.Cell("D2").Value = "Brand";
            ws.Cell("E2").Value = "Variety";
            ws.Cell("F2").Value = "Modified Week";
            ws.Cell("G2").Value = "T12UP";
            ws.Cell("H2").Value = "T14UP";
            ws.Cell("I2").Value = "T16UP";
            ws.Cell("J2").Value = "T18UP";
            ws.Cell("K2").Value = "T20UP";
            ws.Range("A2:K2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Grower;
                ws.Cell("B" + (i)).Value = item.Farm;
                ws.Cell("C" + (i)).Value = item.WeekActually;
                ws.Cell("D" + (i)).Value = item.Brand;
                ws.Cell("E" + (i)).Value = item.Variety;
                ws.Cell("F" + (i)).Value = item.LastUpdate;
                ws.Cell("G" + (i)).Value = item.T12;
                ws.Cell("H" + (i)).Value = item.T14;
                ws.Cell("I" + (i)).Value = item.T16;
                ws.Cell("J" + (i)).Value = item.T18;
                ws.Cell("K" + (i)).Value = item.T20;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailVariationForecast(stream1, lista);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailVariationForecast(Stream fileExcel, List<RPTForecastVariation> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Grower + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.Farm + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.WeekActually + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Brand + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Variety + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.LastUpdate + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.T12 + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.T14 + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.T16 + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.T18 + @"</td>	  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.T20 + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>WEEKLY HARVEST FORECAST VARIATION </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF WEEKLY HARVEST FORECAST VARIATION HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The variation of the Weekly Harvest Forecast is show</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Grower</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Farm</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Week Actually</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Brand</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Variety</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Last Update</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>T12</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>T14</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>T16</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>T18</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>T20</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert WEEKLY HARVEST FORECAST VARIATION", body);
            Email.Attachments.Add(new Attachment(fileExcel, "forecastVariation.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);
            smtpMail.Dispose();
            return true;
        }

        public string MailPOrejected()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOrej");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTPOrejected>>(response.Content);
            createExcelPOrejected(lista);

            return "ok";
        }

        public void createExcelPOrejected(List<RPTPOrejected> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("POrejected");
            ws.Cell("A1").Value = "Rejected Production Orders";
            ws.Range("A1:M1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Grower";
            ws.Cell("B2").Value = "PO";
            ws.Cell("C2").Value = "Departure Week";
            ws.Cell("D2").Value = "Customer";
            ws.Cell("E2").Value = "Destination";
            ws.Cell("F2").Value = "Status";
            ws.Cell("G2").Value = "Rejected By";
            ws.Cell("H2").Value = "Rejection Date";
            ws.Cell("I2").Value = "Brand";
            ws.Cell("J2").Value = "Variety";
            ws.Cell("K2").Value = "Size";
            ws.Cell("L2").Value = "Total KG";
            ws.Cell("M2").Value = "Reason for Rejection";
            ws.Range("A2:M2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Grower;
                ws.Cell("B" + (i)).Value = item.PO;
                ws.Cell("C" + (i)).Value = item.DepartureWeek;
                ws.Cell("D" + (i)).Value = item.Customer;
                ws.Cell("E" + (i)).Value = item.Destination;
                ws.Cell("H" + (i)).Value = item.RejectionDate;
                ws.Cell("I" + (i)).Value = item.Brands;
                ws.Cell("J" + (i)).Value = item.Varieties;
                ws.Cell("K" + (i)).Value = item.Sizes;
                ws.Cell("L" + (i)).Value = item.TotalKG;
                ws.Cell("M" + (i)).Value = item.rejectionComments;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailPOrejected(stream1, lista);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailPOrejected(Stream fileExcel, List<RPTPOrejected> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Grower + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.PO + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.DepartureWeek + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Customer + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Destination + @"</td>	  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.RejectionDate + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Brands + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Varieties + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Sizes + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.TotalKG + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.rejectionComments + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>REJECTED PRODUCTION ORDERS </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF REJECTED PRODUCTION ORDERS HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The next production orders have been rejected</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Grower</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Production Order</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Departure Week</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Customer</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Destination</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Status</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejected by</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejection Date</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Brand</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Variety</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Size</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total KG</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Reason for Rejection</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert REJECTED PRODUCTION ORDERS", body);
            Email.Attachments.Add(new Attachment(fileExcel, "rejectedPO.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailSRpending()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSRren");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTSRpending>>(response.Content);
            createExcelSRpending(lista);

            return "ok";
        }

        public void createExcelSRpending(List<RPTSRpending> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("SaleRequestPending");
            ws.Cell("A1").Value = "Sale Request Pending";
            ws.Range("A1:O1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Sale Request";
            ws.Cell("B2").Value = "Year";
            ws.Cell("C2").Value = "Departure Week";
            ws.Cell("D2").Value = "Customer";
            ws.Cell("E2").Value = "Destination";
            ws.Cell("F2").Value = "Brand";
            ws.Cell("G2").Value = "Varieties";
            ws.Cell("H2").Value = "Sizes";
            ws.Cell("I2").Value = "Codepack";
            ws.Cell("J2").Value = "Total Boxes";
            ws.Cell("K2").Value = "Total USD";
            ws.Cell("L2").Value = "Created By";
            ws.Cell("M2").Value = "Price Condition";
            ws.Cell("N2").Value = "Incoterm";
            ws.Cell("O2").Value = "Payment Term";
            ws.Range("A2:O2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.SaleRequest;
                ws.Cell("C" + (i)).Value = item.DepartureWeek;
                ws.Cell("D" + (i)).Value = item.Customer;
                ws.Cell("E" + (i)).Value = item.Destination;
                ws.Cell("F" + (i)).Value = item.Brands;
                ws.Cell("G" + (i)).Value = item.Varieties;
                ws.Cell("H" + (i)).Value = item.Sizes;
                ws.Cell("I" + (i)).Value = item.Codepack;
                ws.Cell("J" + (i)).Value = item.TotalBoxes;
                ws.Cell("K" + (i)).Value = item.TotalUSD;
                ws.Cell("L" + (i)).Value = item.CreatedBy;
                ws.Cell("M" + (i)).Value = item.PriceCondition;
                ws.Cell("N" + (i)).Value = item.Incoterm;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailSRpending(stream1, lista);

            FileStream file = new FileStream("d:\\files.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailSRpending(Stream fileExcel, List<RPTSRpending> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.SaleRequest + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.DepartureWeek + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Customer + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Destination + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Brands + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Varieties + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Sizes + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Codepack + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.TotalBoxes + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.TotalUSD + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.CreatedBy + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.PriceCondition + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Incoterm + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>PENDING SALES REQUESTS </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF PENDING SALES REQUESTS HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The next sales are pending to assign Production Order</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Sale Request</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Departure Week</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Customer</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Destination</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Brand</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Varieties</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Sizes</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Codepack</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total Boxes</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total USD</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Created By</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Price Condition</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Incoterm</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert PENDING SALES REQUESTS", body);
            Email.Attachments.Add(new Attachment(fileExcel, "pendingSaleRequest.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailPlanVariationFor()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetVarPlanFore");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTForecastVariationPlan>>(response.Content);
            createExcelForeVarPlan(lista);

            return "ok";
        }

        public void createExcelForeVarPlan(List<RPTForecastVariationPlan> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("PlanVariationForecast");
            ws.Cell("A1").Value = "Plan Variation Affects Forecast";
            ws.Range("A1:F1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Grower";
            ws.Cell("B2").Value = "Year";
            ws.Cell("C2").Value = "Week";
            ws.Cell("D2").Value = "Total Forecast";
            ws.Cell("E2").Value = "Commercial Plan";
            ws.Cell("F2").Value = "Difference in KG";
            ws.Range("A2:F2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Grower;
                ws.Cell("B" + (i)).Value = item.Year;
                ws.Cell("C" + (i)).Value = item.Week;
                ws.Cell("D" + (i)).Value = item.TotalForecast;
                ws.Cell("E" + (i)).Value = item.CommercialPlan;
                ws.Cell("F" + (i)).Value = item.DifferenceinKG;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailForeVarPlan(stream1);

            FileStream file = new FileStream("e:\\files.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailForeVarPlan(Stream fileExcel)
        {
            System.Net.Mail.MailMessage Email; ;

            string body = @"
                            <!DOCTYPE html>
                            <html lang='es'>  
                              <head>    
                                <title>Título de la WEB</title>    
                                <meta charset='UTF-8'>
	                            <style>
	                            table {border-collapse: collapse; border-spacing: 0; width: 100%; border: 1px solid #ddd;}
	                            th, td { text-align: left; padding: 8px;}
	                            tr:nth-child(even){background-color: #f2f2f2}
	                            </style>
                              </head>  
                              <body>    
                            <table>
	                            <tr rowspan='3'><td colspan='1'><a href='http://agrocom.azurewebsites.net'><img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='200' style='margin-left:10px;'></a></td>
		                            <td colspan='5'><h1>PLAN VARIATION AFFECTS FORECAST </h1></td></tr>
	                            <tr><td colspan='6'>REPORT OF PLAN VARIATION AFFECTS FORECAST HAS BEEN GENERATED</td></tr>

                            </table>
                            </h3>
                                <footer><h4>Do not reply to this email because it is automatic</h4><a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></footer>
                              </body>  
                            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert PLAN VARIATION AFFECTS FORECAST", body);
            Email.Attachments.Add(new Attachment(fileExcel, "planVariationForecast.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailPOpending()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOpen");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTPOpending>>(response.Content);
            createExcelPOpending(lista);

            return "ok";
        }

        public void createExcelPOpending(List<RPTPOpending> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("POpending");
            ws.Cell("A1").Value = "Production Orders Pending Confirmation";
            ws.Range("A1:I1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Grower";
            ws.Cell("B2").Value = "PO";
            ws.Cell("C2").Value = "Departure Week";
            ws.Cell("D2").Value = "Via";
            ws.Cell("E2").Value = "Brands";
            ws.Cell("F2").Value = "Varieties";
            ws.Cell("G2").Value = "Sizes";
            ws.Cell("H2").Value = "Total KG";
            ws.Cell("I2").Value = "Date Issued";
            ws.Range("A2:I2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Grower;
                ws.Cell("B" + (i)).Value = item.PO;
                ws.Cell("C" + (i)).Value = item.DepartureWeek;
                ws.Cell("D" + (i)).Value = item.Via;
                ws.Cell("E" + (i)).Value = item.Brands;
                ws.Cell("F" + (i)).Value = item.Varieties;
                ws.Cell("G" + (i)).Value = item.Sizes;
                ws.Cell("H" + (i)).Value = item.TotalKG;
                ws.Cell("I" + (i)).Value = item.dateIssued;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailPOpending(stream1, lista);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailPOpending(Stream fileExcel, List<RPTPOpending> lista)
        {
            System.Net.Mail.MailMessage Email;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Grower + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.PO + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.DepartureWeek + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Via + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Brands + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Varieties + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.TotalKG + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.dateIssued + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>PRODUCTION ORDERS PENDING CONFIRMATION </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF PRODUCTION ORDERS PENDING CONFIRMATION HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The next production orders are pending confirmation</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Grower</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Production Order</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Departure Week</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Via</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Brands</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Varieties</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total KG</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Date Of issued</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";

            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert PRODUCTION ORDERS PENDING CONFIRMATION", body);
            Email.Attachments.Add(new Attachment(fileExcel, "pendingPO.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailPOnotAttended()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOnotAt");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTPOnotAttended>>(response.Content);
            createExcelPOnotAttended(lista);

            return "ok";
        }

        public void createExcelPOnotAttended(List<RPTPOnotAttended> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("POnotAttended");
            ws.Cell("A1").Value = "Production Orders Pending Attention";
            ws.Range("A1:M1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Grower";
            ws.Cell("B2").Value = "PO";
            ws.Cell("C2").Value = "Departure Week";
            ws.Cell("D2").Value = "Customer";
            ws.Cell("E2").Value = "Destination";
            ws.Cell("H2").Value = "Approval Date";
            ws.Cell("I2").Value = "Brands";
            ws.Cell("J2").Value = "Varieties";
            ws.Cell("K2").Value = "Sizes";
            ws.Cell("L2").Value = "ETD";
            ws.Cell("M2").Value = "Total in KG";
            ws.Range("A2:M2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Grower;
                ws.Cell("B" + (i)).Value = item.PO;
                ws.Cell("C" + (i)).Value = item.DepartureWeek;
                ws.Cell("D" + (i)).Value = item.Customer;
                ws.Cell("E" + (i)).Value = item.Destination;
                ws.Cell("H" + (i)).Value = item.ApprovalDate;
                ws.Cell("I" + (i)).Value = item.Brands;
                ws.Cell("J" + (i)).Value = item.Varieties;
                ws.Cell("K" + (i)).Value = item.Sizes;
                ws.Cell("L" + (i)).Value = item.ETD;
                ws.Cell("M" + (i)).Value = item.Total;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailPOnotAttended(stream1, lista);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailPOnotAttended(Stream fileExcel, List<RPTPOnotAttended> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Grower + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.PO + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.DepartureWeek + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Customer + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Destination + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.ApprovalDate + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Brands + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Varieties + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Sizes + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.ETD + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Total + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>PRODUCTION ORDERS PENDING ATTENTION </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF PRODUCTION ORDERS PENDING ATTENTION HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The next Production Orders are pending to be attendeed</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Grower</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Production Order</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Departure Week</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Customer</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Destination</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Status</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Approved By</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Approval Date</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Brands</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Varieties</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Sizes</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>ETD</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total KG</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert PRODUCTION ORDERS PENDING ATTENTION", body);
            Email.Attachments.Add(new Attachment(fileExcel, "POnotAttended.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malonj@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailSRuncommitted()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSRuncom");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTSRuncommitted>>(response.Content);
            createExcelSRuncommitted(lista);

            return "ok";
        }

        public void createExcelSRuncommitted(List<RPTSRuncommitted> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("UncommittedSalesRequests");
            ws.Cell("A1").Value = "Uncommitted Sales Requests";
            ws.Range("A1:Q1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Sale Request";
            ws.Cell("B2").Value = "Year";
            ws.Cell("C2").Value = "Departure Week";
            ws.Cell("D2").Value = "Customer";
            ws.Cell("E2").Value = "Destination";
            ws.Cell("F2").Value = "Brand";
            ws.Cell("G2").Value = "Varieties";
            ws.Cell("H2").Value = "Sizes";
            ws.Cell("I2").Value = "Codepack";
            ws.Cell("J2").Value = "Total Boxes";
            ws.Cell("K2").Value = "Total USD";
            ws.Cell("P2").Value = "Rejected By";
            ws.Cell("Q2").Value = "Rejection Date";
            ws.Range("A2:Q2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.SaleRequest;
                ws.Cell("C" + (i)).Value = item.DepartureWeek;
                ws.Cell("D" + (i)).Value = item.Customer;
                ws.Cell("E" + (i)).Value = item.Destination;
                ws.Cell("F" + (i)).Value = item.Brands;
                ws.Cell("G" + (i)).Value = item.Varieties;
                ws.Cell("H" + (i)).Value = item.Sizes;
                ws.Cell("I" + (i)).Value = item.Codepack;
                ws.Cell("J" + (i)).Value = item.Boxes;
                ws.Cell("K" + (i)).Value = item.TotalUSD;
                ws.Cell("P" + (i)).Value = item.RejectedBy;
                ws.Cell("Q" + (i)).Value = item.RejectionDate;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailSRuncommitted(stream1, lista);

            FileStream file = new FileStream("d:\\files.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailSRuncommitted(Stream fileExcel, List<RPTSRuncommitted> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.SaleRequest + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.DepartureWeek + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Customer + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Destination + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Brands + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Varieties + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Sizes + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Codepack + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Boxes + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.TotalUSD + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.RejectedBy + @"</td>		  	
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.RejectionDate + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>UNCOMMITTED SALES REQUESTS </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF UNCOMMITTED SALES REQUESTS HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The next Sales Request are uncommitted</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Sale Request</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Departure Week</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Customer</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Destination</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Brand</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Varieties</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Sizes</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Codepack</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total Boxes</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Total USD</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejected By</th>   
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejection Date</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert UNCOMMITTED SALES REQUESTS", body);
            Email.Attachments.Add(new Attachment(fileExcel, "uncommittedSaleRequest.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailCPpending()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetpendingCP");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTCPpending>>(response.Content);
            createExcelCPpending(lista);

            return "ok";
        }

        public void createExcelCPpending(List<RPTCPpending> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("pendingCommercialPrograms");
            ws.Cell("A1").Value = "Pending Commercial Programs";
            ws.Range("A1:E1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Grower";
            ws.Cell("B2").Value = "Year";
            ws.Cell("C2").Value = "Week";
            ws.Cell("D2").Value = "Total KG Forecast";
            ws.Cell("E2").Value = "Total KG Commercial Plan";
            ws.Range("A2:E2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.Grower;
                ws.Cell("B" + (i)).Value = item.year;
                ws.Cell("C" + (i)).Value = item.Week;
                ws.Cell("D" + (i)).Value = item.TOTALF;
                ws.Cell("E" + (i)).Value = item.TOTALP;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailCPpending(stream1);

            FileStream file = new FileStream("e:\\files.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailCPpending(Stream fileExcel)
        {
            System.Net.Mail.MailMessage Email; ;

            string body = @"
                            <!DOCTYPE html>
                            <html lang='es'>  
                              <head>    
                                <title>Título de la WEB</title>    
                                <meta charset='UTF-8'>
	                            <style>
	                            table {border-collapse: collapse; border-spacing: 0; width: 100%; border: 1px solid #ddd;}
	                            th, td { text-align: left; padding: 8px;}
	                            tr:nth-child(even){background-color: #f2f2f2}
	                            </style>
                              </head>  
                              <body>    
                            <table>
	                            <tr rowspan='3'><td colspan='1'><a href='http://agrocom.azurewebsites.net'><img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='200' style='margin-left:10px;'></a></td>
		                            <td colspan='5'><h1>COMMERCIAL PROGRAMS PENDING CREATION</h1></td></tr>
	                            <tr><td colspan='6'>REPORT OF COMMERCIAL PROGRAMS PENDING CREATION HAS BEEN GENERATED</td></tr>

                            </table>
                            </h3>
                                <footer><h4>Do not reply to this email because it is automatic</h4><a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></footer>
                              </body>  
                            </html>";


            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert COMMERCIAL PROGRAMS PENDING CREATION", body);
            Email.Attachments.Add(new Attachment(fileExcel, "pendingCommercialPrograms.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailPLMissing()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetPackingMissing");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTPLMissing>>(response.Content);
            createExcelPLmissing(lista);

            return "ok";
        }

        public void createExcelPLmissing(List<RPTPLMissing> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("PLmissing");
            ws.Cell("A1").Value = "PackingList With Missing Data";
            ws.Range("A1:D1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Production Order";
            ws.Cell("B2").Value = "PackingList";
            ws.Cell("C2").Value = "Responsable";
            ws.Cell("D2").Value = "Validation";

            ws.Range("A2:D2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.production_order;
                ws.Cell("B" + (i)).Value = item.packinglist;
                ws.Cell("C" + (i)).Value = item.responsable;
                ws.Cell("D" + (i)).Value = item.validation;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailPLMissing(stream1, lista);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }
        public bool SendMailPLMissing(Stream fileExcel, List<RPTPLMissing> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.production_order + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.packinglist + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.responsable + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.validation + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>PACKINGLIST WITH INCOMPLETE DATA </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF PACKINGLIST WITH INCOMPLETE DATA HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>The next packinlists have missing fields</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Production Order</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>PackingList</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Responsable</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Validation</th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";

            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert PACKINLIST WITH MISSING DATA", body);
            Email.Attachments.Add(new Attachment(fileExcel, "missingPL.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }

        public string MailSRrejected()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSRrej");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            var lista = JsonConvert.DeserializeObject<List<RPTSRrejected>>(response.Content);
            createExcelSRrejected(lista);

            return "ok";
        }

        public void createExcelSRrejected(List<RPTSRrejected> lista)
        {
            var stream = new MemoryStream();

            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("SRrejected");
            ws.Cell("A1").Value = "Sales Request Rejected";
            ws.Range("A1:H1").Row(1).Merge();
            ws.Cell("A1").Style.Font.FontColor = XLColor.Black;
            ws.Cell("A1").Style.Font.FontSize = 16;
            ws.Cell("A1").Style.Font.Bold = true;
            ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            ws.Cell("A2").Value = "Sale Request";
            ws.Cell("B2").Value = "Market";
            ws.Cell("C2").Value = "Customer";
            ws.Cell("D2").Value = "Departure Week";
            ws.Cell("E2").Value = "Boxes";
            ws.Cell("F2").Value = "Rejection Comments";
            ws.Cell("G2").Value = "Rejected By";
            ws.Cell("H2").Value = "Rejection Date";

            ws.Range("A2:H2").Style.Font.FontSize = 14;

            var i = 3;

            foreach (var item in lista)
            {
                ws.Cell("A" + (i)).Value = item.SaleRequest;
                ws.Cell("B" + (i)).Value = item.Market;
                ws.Cell("C" + (i)).Value = item.Customer;
                ws.Cell("D" + (i)).Value = item.DepartureWeek;
                ws.Cell("E" + (i)).Value = item.Boxes;
                ws.Cell("F" + (i)).Value = item.rejectionComments;
                ws.Cell("G" + (i)).Value = item.rejectedBy;
                ws.Cell("H" + (i)).Value = item.rejectionDate;
                i = i + 1;
            }

            workbook.SaveAs(stream);

            byte[] byteArr = stream.ToArray();
            System.IO.MemoryStream stream1 = new System.IO.MemoryStream(byteArr, true);
            stream1.Write(byteArr, 0, byteArr.Length);
            stream1.Position = 0;

            SendMailSRrejected(stream1, lista);

            FileStream file = new FileStream("d:\\file.xlsx", FileMode.Create, FileAccess.Write);
            stream.WriteTo(file);
            file.Close();
            stream.Close();
        }

        public bool SendMailSRrejected(Stream fileExcel, List<RPTSRrejected> lista)
        {
            System.Net.Mail.MailMessage Email; ;

            string cadenacuerpo = "";
            foreach (var i in lista)
            {
                cadenacuerpo += @"<tr>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.SaleRequest + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;;max-height:200px;min-height:200px'>" + i.Market + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Customer + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.DepartureWeek + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.Boxes + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.rejectionComments + @"</td>
            <td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.rejectedBy + @"</td>
		  	<td style='border: 1px solid #303676;font-family: Arial Narrow;'>" + i.rejectionDate + @"</td>
		    </tr>";
            }

            string body = @"<!DOCTYPE html>
            <html>
            <body>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <tr rowspan='3' style= 'border: 5px solid #303676;background-color: #303676'>
	        <td colspan='1' style= 'border: 1px solid #ddd;background-color: white'><a href='http://agrocom.azurewebsites.net'>
	        <img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='100' style='margin-left:10px;'></a>
	        </td>
	        <td colspan='7' ><h1 style= 'color: white; font-size:30px; font-family: Arial Narrow; max-width:800px; min-width:800px; text-align:center'>SALES REQUEST REJECTED </h1></td>
	        </tr>
	        <tr style= 'background-color: #303676;max-height:4px;min-height:4px'>
	        <td colspan='8'>
	        <hr>
	        <p style= 'text-align:center; color:white; font-family: Arial Narrow;font-size:15px;'>REPORT OF SALES REQUEST REJECTED HAS BEEN GENERATED</p>
		
	        </td>
	        </tr>
	        <tr>
		    <td colspan='8'><h2 style= ' color:black; font-family: Arial Narrow;font-size:15px;'>We have found rejected sales orders, please check the system</h2></td>
	        </tr>
            </table>
            <table style= 'border: 5px solid #303676;max-width:980px; min-width:980px;'>
	        <thead>
		    <tr style='background-color: #C5C9FD'>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;'>Sale Request</th>
			<th style='border: 1px solid #303676;font-family: Arial Narrow;max-height:200px;min-height:200px'>Market</th> 
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Customer</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Departure Week</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Boxes</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejection Comments</th>
            <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejected By</th>
		    <th style='border: 1px solid #303676;font-family: Arial Narrow;'>Rejection Date </th>
  		    </tr>
	        </thead>
	        <tbody>

            " + cadenacuerpo
            + @"	</tbody>
            </table>
            <footer><h4>Do not reply to this email because it is automatic <a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></h4></footer>
            </body>
            </html>";

            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "jose.flores@migiva.com", "alert SALES REQUEST REJECTED", body);
            Email.Attachments.Add(new Attachment(fileExcel, "rejectedSR.xlsx"));
            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("josefloresturpo@gmail.com");
            //MailAddress copy2 = new MailAddress("cristian.malon@migiva.com");

            Email.CC.Add(copy);
            //Email.CC.Add(copy2);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            return true;
        }
    }
}