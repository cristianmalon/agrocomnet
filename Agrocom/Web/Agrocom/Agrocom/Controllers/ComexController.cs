﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class ComexController : Controller
    {
        // GET: Comex
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadingInstruction()
        {
            loadImage();
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        //public string GetManagerList()
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetManagerList");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}


        //public string GetLogisticList()
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetLogisticList");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}


        //public string GetTerminalList()
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetTerminalList");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}


        //public string GetCompanyList()
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetCompanyList");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string GetCustomerType()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetCustomerType");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetConsigneeType()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetConsigneeType");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetNotifyType()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetNotifyType");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDestinationType()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetDestinationType");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDestinationArrive()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetDestinationArrive");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetTariffList(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetTariffList?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string GetPlantList()
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetPlantList");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string VarietyGetByCrop(string opt, string id, string log)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/ListByFilter?opt=" + opt + "&id=" + id + "&log=" + log);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string CategoryGetByCrop(string opt, string id, string gro)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Category/ListBrandByFilter?opt=" + opt + "&id=" + id + "&gro=" + gro);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetFormatByCrop(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetPackageProductAll(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }



        public string GetPresentByCrop(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string BrandGetByCateID(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetLabelByCropID(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string CodepackByFormatIDPackageProductID(string opt, string viaid, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByFilter?opt=" + opt + "&viaid=" + viaid + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string CustomerGetById(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetExporterType(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetExporterType?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDetailRequestLoading(int orderPoductionID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/ListDetailRequestByFilter?orderPoductionID=" + orderPoductionID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetSalesOrdersRequestLoading(string campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/ListRequestLoading?campaignID= " + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveShippingLoading(ENShippingLoading oENShippingLoading)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/SaveShippingLoading");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            var sz = JsonConvert.SerializeObject(oENShippingLoading);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }



        public string GetNroPacking(string campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetNroPacking?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetForPdf(int shippingLoadingID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByID?shippingLoadingID=" + shippingLoadingID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingParameter(string cropID, int destinationID, int coldTreatment)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingParameter?cropID=" + cropID + "&destinationID=" + destinationID + "&coldTreatment=" + coldTreatment);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingStatus(string opt, int orderPoductionID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/ListByFilter?opt=" + opt + "&orderPoductionID=" + orderPoductionID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingLoadingBL()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/ListBL");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string TittleByVia(int viaID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Via/TittleByVia?viaID=" + viaID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }        

    }
}