﻿//using Agrocom.DAORestApi;
using Agrocom.Filters;
using Agrocom.Models;
using Common.CommercialPlan;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CommercialPlanController : Controller
    {

        public ActionResult Index()
        {
            loadImage();
            return View();
        }

        public ActionResult PlanByCustomer()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int planId)
        {
            loadImage();
            ViewBag.planId = planId;
            return View();
        }

        public ActionResult PlanByGrower()
        {
            return View();
        }
        public ActionResult PlanByCrop()
        {
            loadImage();
            return View();
        }

        public ActionResult ProjectedPlan()
        {
            loadImage();
            return View();
        }

        public ActionResult ProjectedPlanByCrop()
        {
            loadImage();
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string ListPlanById(string opt, string id, string cropId, string seasonId, string originId, string growerId)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListByFilter?opt=" + opt + "&id=" + id + "&cropId=" + cropId + "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCrop(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/crop/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListSeason(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/season/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListOriginWithGrower(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Origin/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListGrowerByOriginId(string idOrigin)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/GetByOri?idOrigin=" + idOrigin);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            return response.Content;
        }

        public string GetIfExists(string opt, string id, string cropId, string seasonId, string originId, string growerId)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListByFilter?opt=" + opt + "&id=" + id + "&cropId=" + cropId + "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListGrower(string opt, string id, string cropID)
        {
            string _token = Session["Token"].ToString();
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + _token);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SavePlan(string planID, string plan, string campaignID, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/Save?planID=" + planID + "&plan=" + plan + "&campaignID=" + campaignID + "&userID=" + userID + "");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SavePlanOriginGrower(string planID, string plan, string campaignID, string originID, string growerID, string nonExport, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SavePlanOriginGrower?planID=" + planID + "&plan=" + plan + "&campaignID=" + campaignID + "&originID=" + originID + "&growerID=" + growerID + "&nonExport=" + nonExport + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListForecast(string idGrower, int idSeason)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListCommercialPlanByFilter?growerId=" + idGrower + "&seasonId=" + idSeason + "");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListWeekBySeason(int idSeason)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListBySeason?campaignID=" + idSeason + "");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListMarket(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Market/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCustomer(string marketID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetByMarId?idMarket=" + marketID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListAllWithMarket(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListCodepack(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListCodepackWithVia(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListDestination(string marketID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/GetByMarkId?idmarket=" + marketID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListDestinationAll(string opt, string id, string mar, string via)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListDestinationAllWithCustomer(string opt, string id, string mar, string via)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListOrigin(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Origin/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPlan(string opt, string id, string cropId, string seasonId, string originId, string growerId)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListByFilter?opt=" + opt + "&id=" + id + "&cropId=" + cropId + "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListPlanForGrower(int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetForGrower?userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SavePlanCustomerRequest(string planCustomerRequestID, string planCustomerRequest, string planID, string customerID, string destinationID, string codepackID, string marketID, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SavePlanCustomerRequest?planCustomerRequestID=" + planCustomerRequestID + "&planCustomerRequest=" + planCustomerRequest + "&planID=" + planID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&codepackID=" + codepackID + "&marketID=" + marketID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SavePlanCustomerRequestWeek(string planCustomerRequestWeekID, string planCustomerRequestID, string projectedWeekID, string amount, string userID)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SavePlanCustomerRequestWeek?planCustomerRequestWeekID=" + planCustomerRequestWeekID + "&planCustomerRequestID=" + planCustomerRequestID + "&projectedWeekID=" + projectedWeekID + "&amount=" + amount + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListCustomerRequestByPlanId(string searchOpt, string planID, string marketID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListCustomerRequestByFilter?searchOpt=" + searchOpt + "&planID=" + planID + "&marketID=" + marketID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListCustomerRequestWeekByPlanCustomerId(string searchOpt, int planCustomerRequestID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListCustomerRequestWeekByFilter?searchOpt=" + searchOpt + "&planCustomerRequestID = " + planCustomerRequestID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SavePlanAlternative(string planAlternativeID, string planAlternative, string planOriginID, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SavePlanAlternative?planAlternativeID=" + planAlternativeID + "&planAlternative=" + planAlternative + "&planOriginID=" + planOriginID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPlanAlternative(string searchOpt, int planOriginID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListPlanAlternativeByFilter?searchOpt=" + searchOpt + "&planOriginID=" + planOriginID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SaveCustomerFormat(string codepackID, string alternativeID, string marketID, int destinationID, int customerID, int priorityID, int wildcard, int order, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SaveCustomerFormat?codepackID=" + codepackID + "&alternativeID=" + alternativeID + "&marketID=" + marketID + "&destinationID=" + destinationID + "&customerID=" + customerID + "&priorityID=" + priorityID + "&wildcard=" + wildcard + "&order=" + order + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SavePlanCustomerWeek(string planCustomerWeekID, string planCustomerFormatID, string projectedWeekID, string amount, string userID)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SavePlanCustomerWeek?planCustomerWeekID=" + planCustomerWeekID + "&planCustomerFormatID=" + planCustomerFormatID + "&projectedWeekID=" + projectedWeekID + "&amount=" + amount + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPlanCustomerByPlanAlternative(string searchOpt, int planAlternativeID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListPlanCustomerByFilter?searchOpt=" + searchOpt + "&planAlternativeID=" + planAlternativeID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListPlanCustomerWeekByCustomer(int planCustomerFormatID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListPlanCustomerWeekByCustomer?planCustomerFormatID=" + planCustomerFormatID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string CustomerRequestDelete(int planCustomerRequestID, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/CustomerRequestDelete?planCustomerRequestID=" + planCustomerRequestID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            return response.Content;
        }
        public string PlanCustomerDelete(int planCustomerFormatID, string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/PlanCustomerDelete?planCustomerFormatID=" + planCustomerFormatID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        
        public string CustomerByCrop(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string FormatByCrop(string cropIID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetFormatByCropID?cropID=" + cropIID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string PresentationByFormatID(string formatID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/getPresentationByFormatID?formatID=" + formatID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string CodepackByFormatIDMC(string opt, string viaid, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByFilter?opt=" + opt + "&viaid=" + viaid + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SizeBoxByFilter(string formatID, string packageProductID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListSizeBoxByFilter?formatID=" + formatID + "&packageProductID=" + packageProductID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string PackageProduct()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetPackageProductAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string VarietyByCrop(string opt, string id, string log)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/ListByFilter?opt="+opt+"&id="+id+"&log=" + log);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string BrandByCrop(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string LabelByCrop(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string CategoryByCrop(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetByCrop?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string DestinationByCustomerID(string opt, string id, string mar, string via)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string PresentationByCropID(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string PaymentTermByCropID(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetPaymentTermByCrop?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string IncotermByViaID(string cropID, string viaID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetIncotermByVia?cropID=" + cropID + "&viaID=" + viaID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string PriceCondition(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ConditionPayment/ListByFilter?opt="+opt+"&id="+id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string PlanCustomerVariety(ENPlanCustomerVariety o)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/PlanCustomerVariety");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            var sz = JsonConvert.SerializeObject(o);
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
             return response.Content;
        }

        public string CreateCommercialPlanMultiVariertySize(ENPlanCustomerBrand o)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/RegisterCommercialPlanMultiVarietySize");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            var sz = JsonConvert.SerializeObject(o);
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string PlanCustomerVarietyList(string campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetPlanCustomerVariety?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPlanCustomerByFilters(string campaignID, int customerID, int destinationID, int programID, string categoryID, int varietyID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetPlanCustomerByFilters?campaignID=" + campaignID + "&customerID="+ customerID + "&destinationID=" + destinationID + "&programID=" + programID + "&categoryID="+ categoryID + "&varietyID=" + varietyID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPlanBlueberryByFilters(int campaignID, int customerID, int destinationID, int programID, string brandID, int viaID, int formatID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetPlanBlueberryByFilters?campaignID=" + campaignID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&programID=" + programID + "&brandID=" + brandID + "&viaID=" + viaID + "&formatID=" + formatID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string PlanCustomerVarietyListByWeekID(string campaignID, int projectedWeekID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetPlanCustomerVarietyByWeekID?campaignID=" + campaignID + "&projectedWeekID=" + projectedWeekID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string CreateSaleRequestAndPO(string cropId, int campaignID, int userCreated, int projectedWeekID, PlanComercialCab objPlanComercial)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/CreateSaleRequestAndPO?" +
                "cropId=" + cropId + "&campaignID=" + campaignID +
                "&userCreated=" + userCreated + "&projectedWeekID=" + projectedWeekID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            var sz = JsonConvert.SerializeObject(objPlanComercial);
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string CreateSaleRequestAndPOBLU(string cropId, int campaignID, int userCreated, int projectedWeekID, PlanComercialBlu objPlanComercial)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/CreateSaleRequestAndPOBLU?" +
                "cropId=" + cropId + "&campaignID=" + campaignID +
                "&userCreated=" + userCreated + "&projectedWeekID=" + projectedWeekID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            var sz = JsonConvert.SerializeObject(objPlanComercial);
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string GetVarCatByCrop(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/ListVarietyCategoryByCrop?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListDestinationAllWithCustomerMC(string opt, string id, string mar, string via)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPlanByUser(int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/GetByUserAndCampaign?userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPriority(string opt, string value)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Priority/GetMC?opt=" + opt + "&value=" + value);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPlanAlternativeAprove(string opt, int planOriginID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/ListPlanAlternativeMC?opt=" + opt + "&planOriginID=" + planOriginID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SavePlanAlternativeAprove(ENPlanAlternative obj )
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PlanCommerce/SavePlanOriginGrower?planID=");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListSizeByCrop(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetCodepackSizeByCrop?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListColorByFilter(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Color/ListByFilter?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}