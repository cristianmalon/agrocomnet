﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class FinanceController : Controller
    {
        // GET: Finance
        public ActionResult Index()
        {
            loadImage();
            return View();
        }
        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string GetDocumentsDetailByID(string orderProductionID, int customerInvoiceID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetDocumentsDetailByID?orderProductionID=" + orderProductionID + "&customerInvoiceID=" + customerInvoiceID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetTypeChange(string fecha) {
            var client = new RestClient("https://api.apis.net.pe/v1/tipo-cambio-sunat?fecha=" + fecha);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            //request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPaymentsDetailByID(string orderProductionID, int customerPaymentID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetPaymentsDetailByID?orderProductionID=" + orderProductionID + "&customerPaymentID=" + customerPaymentID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDocumentsByPOID(string orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetDocumentsByPOID?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPaymentByPOID(string orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetPaymentByPOID?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPOForFinance(int orderProductionID, int campaignID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetPOFinanceMC?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetGrowerSettlementByPOID(string orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetGrowerSettlementByPOID?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetGrowerSettlementDetailByID(string growerSettlementID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetLocalCurrency(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceMC?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDocumentType(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin, string periodo, string statusid)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceMC?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin + "&periodo=" + periodo + "&statusid=" + statusid);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPaymentsType(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceMC?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveFinanceInvoice(ENFinanceCustomerInvoice o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/SaveFinanceInvoice");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content;

        }

        public string SaveFinancePayment(ENFinanceCustomerPayments o)
        {


            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/SaveFinancePayment");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content;

        }

        public string SaveFinanceCustomerSettlement(ENFinanceCustomerSettlement o)
        {


            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/SaveFinanceCustomerSettlement");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content;
        }

        public string SaveFinanceGrower(ENFinanceGrowerSettlement o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/SaveFinanceGrower");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content;
        }

        public string SaveFinanceGrowerWithoutDetail(ENFinanceGrowerSettlement o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/SaveFinanceGrowerWithoutDetail");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content;
        }


        public string GetFinanceGrowerDelete(int growerSetlementID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceGrowerDelete?growerSetlementID=" + growerSetlementID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPOList(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin, string periodo, string statusid)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceMC?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin + "&periodo=" + periodo + "&statusid=" + statusid);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPacking(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceMC?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetContainer(string opt, int campaignID, string nroPackinglist, string container, string dayini, string dayfin)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetFinanceMC?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetCostDist(int orderProductionID, int campaignID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetCostDist?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPackingList(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Finance/GetPackingList?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}