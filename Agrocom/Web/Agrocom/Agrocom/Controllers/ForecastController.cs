﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace Agrocom.Controllers
{
    public class ForecastController : Controller
    {
        // GET: Forecast
        public ActionResult Index()
        {
            loadImage();
            return View();
        }

        public ActionResult Crop()
        {
            loadImage();
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string ListSeason()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Season/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        //public string GetCurrentWeek(string opt, string id, string cropID, int campaignID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Week/GetCurrentMC?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        public string ListProjectedWeeks()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/GetNex");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListWeekByCampaign(string campaignID)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListBySeason?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        //public string ListForecastGeneralValues(int projectedWeekID, int loteID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetById?projectedWeekID=" + projectedWeekID + "&loteID=" + loteID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        public string ListSizes()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListBrands()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        //public string ListProjectedProduction(string login, int projectedWeekID, int loteID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetByPercent/?login=" + login + "&projectedWeekID=" + projectedWeekID + "&loteID=" + loteID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        public string SaveForecast(string login, string loteID, string projectedWeekID, string categoryID, int quantityTotal, string commentary, string percentTotalSize_JSON, string percentBySize_JSON)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetSave/?login=" + login + "&loteID=" + loteID + "&projectedWeekID=" + projectedWeekID + "&categoryID=" + categoryID + "&quantityTotal=" + quantityTotal + "&commentary=" + commentary + "&percentTotalSize_JSON=" + percentTotalSize_JSON + "&percentBySize_JSON=" + percentBySize_JSON);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SaveForecast3(string userID, string lotID, string projectedWeekID, string categoryID, int quantityTotal, string commentary, string percentBySize_table)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetSaveModify/?userID=" + userID + "&lotID=" + lotID + "&projectedWeekID=" + projectedWeekID + "&categoryID=" + categoryID + "&quantityTotal=" + quantityTotal + "&commentary=" + commentary + "&percentBySize_table=" + percentBySize_table);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SaveUploadForecast(string userID, string lotID, string projectedWeekID, string categoryID, decimal quantityTotal, string commentary, string percentBySize_table)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/PostSaveForecast?userID=" + userID + "&lotID=" + lotID + "&projectedWeekID=" + projectedWeekID + "&categoryID=" + categoryID + "&quantityTotal=" + quantityTotal + "&commentary=" + commentary);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", percentBySize_table, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListGrower()
        {
            string _token = Session["Token"].ToString();
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListGrowerByUser(string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/GetByUserId/?idUser=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        //public string ListFarm(string opt, string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Farm/GetByGro_MC?opt=" + opt + "&growerID=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        public string ListLotsByFarm(string idFarm)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Lot/GetByFarm?idFarm=" + idFarm);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListLotsByFarmGro(string idFarm, string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Lot/GetByFarmGro?idFarm=" + idFarm + "&growerID=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetWeekByID(string idWeek)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/GetById?idWeek=" + idWeek);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListProjectedProductionSummary(string userID, bool checkbtn, string dateIni, string dateFin, string growerID)
        {
            //string login = "sistemas";
            if (checkbtn)
            {
                var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetProjectedProductionBySize?userID=" + userID + "&dateIni=" + dateIni + "&dateFin=" + dateFin + "&growerID=" + growerID);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            else
            {
                var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetProjecteProductionDetails?userID=" + userID + "&dateIni=" + dateIni + "&dateFin=" + dateFin + "&growerID=" + growerID);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
                IRestResponse response = client.Execute(request);
                return response.Content;
            }

        }
        public string ListProjectedProductionBySizePercent(string login, string projectedWeekID, string loteID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetProjectedProductionBySizePercent?login=" + login + "&projectedWeekID=" + projectedWeekID + "&loteID=" + loteID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListStageByGrower(string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Stage/GetByGrower?idGrower=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListLotByGrower(string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Lot/GetByGrower?idGrower=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListBrandsByGrower(string idOrigin, string idGrower)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetByGro?idOrigin=" + idOrigin + "&idGrower=" + idGrower);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        //public string ListVarietiesByGrower(string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Variety/GetByGro?idGrower=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        //public string ListSizesByGrower(string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Size/GetByGrower?idGrower=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        public string GetGrowerByID(string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/GetById?idGrower=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SaveMasivelyForecast(int LotID, string BrandID, string SizeID, string WeekStart, string campaign, float mount1, float mount2, float mount3, float mount4)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/SaveMasiveForecast?&LotID=" + LotID + "&BrandID=" + BrandID + "&SizeID=" + SizeID + "&WeekStart=" + WeekStart + "&campaign=" + campaign + "&mount1=" + mount1 + "&mount2=" + mount2 + "&mount3=" + mount3 + "&mount4=" + mount4);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        //public string SaveMasiveUploadForecast(MassiveUploadCab objMassiveUpload)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/SaveMasiveUploadForecast");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.POST);
        //    var sz = JsonConvert.SerializeObject(objMassiveUpload);
        //    request.AddHeader("Content-Type", "application/json");
        //    request.AddParameter("application/json", sz, ParameterType.RequestBody);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        //public string GetMasiveForecast(string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetMasiveForecast?&growerID=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string GetFirstForecast()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetFirstForecast");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetCurrentlyForecast()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetCurrentlyForecast");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetRealForecast()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetRealForecast");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string GetProjectedProductionByVariety(string userID, string dateIni, string dateFin, string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetProjectedProductionByVariety?userID=" + userID +
        //    "&dateIni=" + dateIni + "&dateFin=" + dateFin + "&growerID=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string ListGrowerByUserCrop(string opt, string id, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string ListStageByFarm(string growerID, string farmID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Stage/GetByFarm?idGrower=" + growerID + "&farmID=" + farmID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        //public string ListLotByStageID(string stageID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Lot/GetByStaId?idstage=" + stageID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}
        //public string ListCategoryByGrowerID(string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetByGroID?idGrower=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        //public string ListSizeByGrowerID(string growerID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Size/GetByGrower?idGrower=" + growerID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;

        //}

        public string GetForecastForPlan(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListBySale?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        public string ListFarmByGrower(string idGrower)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Farm/GetByGro?idGrower=" + idGrower);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListWeeksMC(string opt, string id, string cropID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string ListTitleFormByCropID(string cropID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetTitleFormByCropID?cropID=" + cropID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string ListGrowerByUserCropMC(string opt, string id, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListForecastGeneralValuesMC(int projectedWeekID, int loteID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListForecastByFilter?projectedWeekID=" + projectedWeekID + "&loteID=" + loteID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListStageByCrop(string cropID, string growerID, string farmID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Stage/ListByCrop?cropID=" + cropID + "&growerID=" + growerID + "&farmID=" + farmID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListFarmByCrop(string growerID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Farm/ListByCrop?growerID=" + growerID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListLotByStage(string stageID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Lot/ListByStage?stageID=" + stageID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string ListWeekByCampaignMC(string campaignID)
        //{

        //    var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListBySeason?campaignID=" + campaignID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string ListSizeByCrop(string cropID, string growerID, int lotID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/ListByCrop?cropID=" + cropID + "&growerID=" + growerID + "&lotID=" + lotID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string ListForecastUnified(string growerID, string farmID, string cropID, string originID) {

            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListForecastUnified?growerID=" + growerID + "&farmID=" + farmID + "&cropID=" + cropID + "&originID" + originID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCategoryByGrowerIDMC(string opt, string id, string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListByFilter?opt=" + opt + "&id=" + id + "&growerID=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListTitleFormByCropIDMC(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListTitleFormByCrop?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListVarietiesByGrowerMC(string opt, string id, string log)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/ListByFilter?opt=" + opt + "&id=" + id + "&log=" + log);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListSizesByGrowerMC(string opt, string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/ListByFilter?opt=" + opt + "&id=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetMasiveForecastMC(string growerID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListMasiveByFilter?growerID=" + growerID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetProjectedProductionByVarietyMC(string userID, string dateIni, string dateFin, int campaignID, string farmID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListProjectedProductionByFilter?userID=" + userID +
            "&dateIni=" + dateIni + "&dateFin=" + dateFin + "&campaignID=" + campaignID + "&farmID=" + farmID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListProjectedProductionMC(string login, int projectedWeekID, int loteID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListPercentByFilter?login=" + login + "&projectedWeekID=" + projectedWeekID + "&loteID=" + loteID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveMasiveUploadForecastMC(MassiveUploadCab objMassiveUpload, int campaignID)
        {
            var user = Session["IdUser"].ToString();
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/SaveMasiveUpload?userID=" + user + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(objMassiveUpload);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListFarmByCampaign(string opt, int id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Farm/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}
