﻿using Agrocom.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class HarvestForecastWController : Controller
    {
        // GET: HarvestForecastW
        public ActionResult Index()
        {
            loadImage();
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string ListBySeasonID(int seasonID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListBySeasonID?seasonID=" + seasonID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetForecastByCropID(string cropID, int seasonID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/GetForecastByCropID?cropID=" + cropID + "&seasonID=" + seasonID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }




    }
}