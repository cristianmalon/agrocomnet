﻿using Agrocom.Filters;
using Agrocom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using Common.Entities;

namespace Agrocom.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            Account account = new Account();

            if (Session["IdUser"] != null)
            {
                account.IdUser = int.Parse(Session["IdUser"].ToString());
                ViewBag.Menu = Session["Menu"];
                ViewBag.Token = Session["Token"];
                ViewBag.UsrType = Session["TypeUser"];
                ViewBag.CreateSONoneProgram = Session["CreateSONoneProgram"];
                ViewBag.CustomerToSelectSONoneProgram = Session["CustomerToSelect_SOwithoutProgram"];
                ViewBag.ConsigneeToSelectSONoneProgram = Session["ConsigneeToSelect_SOwithoutProgram"];                
            }

            if (!loadImages())
            {
                ViewBag.Message = "Images not configured for crop.";
                return View();
            }

            var oUserMenuResponse = (List<EUserMenu>)Session["Option"];
            var jsUserMenu = JsonConvert.SerializeObject(oUserMenuResponse);
            ViewBag.Option = jsUserMenu;
            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public bool loadImages()
        {
            bool bool_response = false;
            string cropId = Session["CropID"].ToString();
            var client = new RestClient(GlobalVars.UriApi + "/api/Crop/GetImageByCrop?idCrop=" + cropId);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            string data = response.Content;

            if (string.IsNullOrEmpty(data)) { return bool_response; }

            var imagearray = JsonConvert.DeserializeObject<List<Image>>(data);

            foreach (var item in imagearray)
            {
                switch (item.settingDetailKey)
                {
                    case "key_content-wrapper":
                        ViewBag.contentwrapper = item.settingValue;
                        Session["contentwrapper"] = item.settingValue; break;
                    case "key_imgfondo":
                        ViewBag.imgfondo = item.settingValue;
                        Session["imgfondo"] = item.settingValue; break;
                    case "key_logo":
                        ViewBag.logo = item.settingValue;
                        Session["logo"] = item.settingValue; break;
                    case "key_linealogo":
                        ViewBag.linealogo = item.settingValue;
                        Session["linealogo"] = item.settingValue; break;
                    case "key_colorFooter":
                        ViewBag.colorFooter = item.settingValue;
                        Session["colorFooter"] = item.settingValue; break;
                    case "key_info_foot":
                        ViewBag.infofoot = item.settingValue;
                        Session["infofoot"] = item.settingValue; break;
                }
            }
            bool_response = true;

            return bool_response;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public string getCropByUserID(string opt, string id, string token)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Crop/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}