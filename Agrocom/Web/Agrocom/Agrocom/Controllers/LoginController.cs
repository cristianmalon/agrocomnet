﻿using Agrocom.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using Common.Entities;
using System.ComponentModel.DataAnnotations;
using Agrocom.Util;
using Common.Security;

namespace Agrocom.Controllers
{
    public class LoginController : Controller
    {

        #region "actions"
        [HttpGet]
        public ActionResult Index()
        {
            Session["User"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Login([Required]string Name, [Required]string Password)
        {
            ViewBag.User = Name;
            Password = UEncrypt.EncryptTripleDES(Password);
            var objUsr = VerifyUser(Name, Password);
            if (objUsr == null || objUsr.userID == 0)
            {
                ViewBag.Error = "User does not exist";
                return View("Index");
            }
            Session["User"] = objUsr;
            Session["IdUser"] = objUsr.userID;
            Session["Token"] = objUsr.token;
            Session["TypeUser"] = objUsr.usertypeID;
            Session["CreateSONoneProgram"] = objUsr.SOwithoutProgram;
            Session["CustomerToSelect_SOwithoutProgram"] = objUsr.CustomerID_SOwithoutProgram;
            Session["ConsigneeToSelect_SOwithoutProgram"] = objUsr.ConsigneeID_SOwithoutProgram;
            Session["Option"] = objUsr.menu;

            var id = "cro";
            var sCrop = getCropByUsr(id, objUsr.userID.ToString());
            if (string.IsNullOrEmpty(sCrop))
            {
                ViewBag.Error = "A system problem has occurred, try again!";
                return View("Index");
            }
            var aCrop = JsonConvert.DeserializeObject<List<Crop>>(sCrop);
            Session["CropID"] = aCrop[0].cropID.ToString();

            var errorResponse = string.Empty;
            var opt = "id";
            if (!GetMenus(opt, objUsr.userID.ToString(), aCrop[0].cropID, out errorResponse))
            {
                ViewBag.Error = errorResponse;
                return View("Index");
            }
            ViewBag.Menu = Session["Menu"];
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult LoadMenu(int userID, string cropID)
        {
            var errorResponse = string.Empty;
            var opt = "id";
            if (!GetMenus(opt, userID.ToString(), cropID, out errorResponse))
            {
                ViewBag.Error = errorResponse;
                return View("Index");
            }
            Session["CropID"] = cropID;
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region "methods"

        private Users VerifyUser(string usuario, string contrasenia)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Security/AuthenticateUser?usuario=" + usuario + "&contrasenia=" + contrasenia);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var objUsrResponse = JsonConvert.DeserializeObject<EUserResponse>(response.Content);
            if (objUsrResponse == null) return null;
            var ojbUsr = new Users();
            ojbUsr.token = objUsrResponse.token;
            ojbUsr.login = objUsrResponse.Login;
            ojbUsr.firstName = objUsrResponse.Nombre;
            ojbUsr.userID = string.IsNullOrEmpty(objUsrResponse.userID) ? 0 : int.Parse(objUsrResponse.userID);
            ojbUsr.usertypeID = objUsrResponse.usertypeID;
            ojbUsr.SOwithoutProgram = objUsrResponse.SOwithoutProgram;
            ojbUsr.CustomerID_SOwithoutProgram = objUsrResponse.CustomerID_SOwithoutProgram;
            ojbUsr.ConsigneeID_SOwithoutProgram = objUsrResponse.ConsigneeID_SOwithoutProgram;
            ojbUsr.menu = new List<EUserMenu>();
            foreach(EUserMenuResponse obj in objUsrResponse.menu)
            {
                ojbUsr.menu.Add(new EUserMenu()
                {
                    optionMenu = obj.optionMenu,
                    controller = obj.controller,
                    action = obj.action,
                    cropid = obj.cropid,
                });
            }

            return ojbUsr;
        }

        private bool GetMenus(string opt, string val, string cropID, out string errorResponse)
        {
            var bRsl = false;
            try
            {
                var client = new RestClient(GlobalVars.UriApi + "/api/Menu/ListByFilter?opt=" + opt + "&val=" + val + "&cropID=" + cropID);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
                IRestResponse response = client.Execute(request);
                var lstmenu = JsonConvert.DeserializeObject<List<Menu>>(response.Content);

                if (!lstmenu.Any())
                {
                    errorResponse = "A system problem has occurred, try again!";
                    return bRsl;
                }
                var MenuParent = lstmenu.Where(x => x.father == 0).OrderBy(x => x.order).ToList();
                if (!MenuParent.Any())
                {
                    errorResponse = "A system problem has occurred, try again!";
                    return bRsl;
                }
                foreach (var item in MenuParent)
                {
                    item.menusChildren = lstmenu.Where(x => x.father == item.menuID).OrderBy(x => x.order).ToList();
                }
                Session["Menu"] = MenuParent;
                errorResponse = string.Empty;
                bRsl = true;
            }
            catch (System.Exception ex)
            {
                errorResponse = ex.Message;
            }

            return bRsl;
        }

        private string getCropByUsr(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Crop/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        #endregion
    }
}