﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class MailController : Controller
    {
        // GET: Mail
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Export(MailData data)
        {
            //create pdf
            var pdfBinaryPO = Convert.FromBase64String(data.Attachment1);
            var pdfBinarySI = Convert.FromBase64String(data.Attachment2);
            //var dir = System.Web.Hosting.HostingEnvironment.MapPath("~/DataDump");//Server.MapPath("~/DataDump");

            //if (!Directory.Exists(dir))
            //    Directory.CreateDirectory(dir);

            //var fileName = dir + "\\PDFnMail-" + DateTime.Now.ToString("yyyyMMdd-HHMMss") + ".pdf";

            //// write content to the pdf
            //using (var fs = new FileStream(fileName, FileMode.Create))
            //using (var writer = new BinaryWriter(fs))
            //{
            //    writer.Write(pdfBinary, 0, pdfBinary.Length);
            //    writer.Close();
            //}

            //Send mail
            var status = SendMail(data.To, new MemoryStream(pdfBinaryPO), new MemoryStream(pdfBinarySI),data.Cadena1);
            //Thread.Sleep(10000);
            //Delete file from file system
            //System.IO.File.Delete(fileName);

            //Return result to client
            return Json(status ? new { result = "success" } : new { result = "failed" });
        }


        private static bool SendMail(string recipient,Stream dataPO, Stream dataSI,string PO)
        {

            System.Net.Mail.MailMessage Email; ;

            string body = @"
                            <!DOCTYPE html>
                            <html lang='es'>  
                              <head>    
                                <title>Título de la WEB</title>    
                                <meta charset='UTF-8'>
	                            <style>
	                            table {border-collapse: collapse; border-spacing: 0; width: 100%; border: 1px solid #ddd;}
	                            th, td { text-align: left; padding: 8px;}
	                            tr:nth-child(even){background-color: #f2f2f2}
	                            </style>
                              </head>  
                              <body>    
                            <table>
	                            <tr rowspan='3'><td colspan='1'><a href='http://agrocom.azurewebsites.net'><img src='http://agrocom.azurewebsites.net/images/ozBlu_centrado_login.png' width='200' style='margin-left:10px;'></a></td>
		                            <td colspan='5'><h1>PRODUCTION ORDER No. "+ PO + @"</h1></td></tr>
	                            <tr><td colspan='6'>A PRODUCTION ORDER HAS BEEN GENERATED</td></tr>

                            </table>
                            </h3>
                                <footer><h4>Do not reply to this email because it is automatic</h4><a href='http://agrocom.azurewebsites.net'>http://agrocom.azurewebsites.net</a></footer>
                              </body>  
                            </html>";

            /*
             	                            <tr><td colspan='2'>GROWER :</td><td colspan='4'>DANPER</td></tr>
	                            <tr><td colspan='2'>Format :</td><td colspan='4'>C441 12x125g_4X5 (Without bag)</td></tr>
	                            <tr><td colspan='2'>Destination :</td><td colspan='4'>Hong Kong, HK</td></tr>
	                            <tr><td colspan='2'>ETD :</td><td colspan='4'>Week 2019-31 - PERÚ</td></tr>
	                            <tr><td colspan='6'>CHECK THE ATTACHED FILE FOR MORE DETAILS</td></tr>
             * */

            Email = new System.Net.Mail.MailMessage("agrocom@ozblu.pe", "cristian.malon@migiva.com", "NEW PRODUCTION ORDER ", body);
            //var attachment = new Attachment(filePath);
            //Email.Attachments.Add(attachment);
            //Email.Attachments.Add(attachment);
            //Email.Attachments.Add(attachment);
            Email.Attachments.Add(new Attachment(dataPO, "PO.pdf"));
            Email.Attachments.Add(new Attachment(dataSI, "ShippingInstructions.pdf"));

            Email.IsBodyHtml = true; //definimos si el contenido sera html
            Email.From = new MailAddress("agrocom@ozblu.pe"); //definimos la direccion de procedencia

            MailAddress copy = new MailAddress("cristian.malonj@gmail.com");

            Email.CC.Add(copy);
            System.Net.Mail.SmtpClient smtpMail = new System.Net.Mail.SmtpClient("merlin.migivagroup.com");

            //smtpMail.EnableSsl = true;//le definimos si es conexión ssl
            //smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            //smtpMail.Host = "smtp.gmail.com"; //agregamos el servidor smtp
            //smtpMail.Port = 587;
            //smtpMail.Credentials = new System.Net.NetworkCredential("cristian.malonj@gmail.com", "********"); //agregamos nuestro usuario y pass de gmail

            smtpMail.EnableSsl = false;//le definimos si es conexión ssl
            smtpMail.UseDefaultCredentials = false; //le decimos que no utilice la credencial por defecto
            smtpMail.Host = "merlin.migivagroup.com"; //agregamos el servidor smtp
            smtpMail.Port = 26;
            smtpMail.Credentials = new System.Net.NetworkCredential("agrocom@ozblu.pe", "Agro2020+-"); //agregamos nuestro usuario y pass de gmail

            smtpMail.Send(Email);

            //eliminamos el objeto
            smtpMail.Dispose();
            //attachment.Dispose();

            //regresamos true
            return true;
        }
    }
}