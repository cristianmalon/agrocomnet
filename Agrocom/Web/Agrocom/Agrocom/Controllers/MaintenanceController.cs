﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Http.Cors;
using RestSharp;
using Agrocom.Models;

namespace Agrocom.Controllers
{
  public class MaintenanceController : Controller
  {
    // GET: CodePack
    public ActionResult Codepack()
    {
      return View();
    }
    public ActionResult Market()
    {
      return View();
    }
    public ActionResult Customer()
    {
      return View();
    }
    public ActionResult Client()
    {
            loadImage();
            return View();
    }
    public void loadImage()
    {
        ViewBag.contentwrapper = Session["contentwrapper"];
        ViewBag.imgfondo = Session["imgfondo"];
        ViewBag.logo = Session["logo"];
        ViewBag.linealogo = Session["linealogo"];
        ViewBag.colorFooter = Session["colorFooter"];
        ViewBag.infofoot = Session["infofoot"];
    }


        public ActionResult Destination()
    {
      return View();
    }

    public string ListVia()
    {
      var client = new RestClient(GlobalVars.UriApi + "/api/Via/GetAll");
      client.Timeout = -1;
      var request = new RestRequest(Method.GET);
      request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
      IRestResponse response = client.Execute(request);
      return response.Content;
    }

    public string ListCode(string idVia, string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetByVic/?viaID=" + idVia + "&cropID=" + cropID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListCrop()
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Crop/GetAll");
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListCodePacks()
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetAll");
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetCodePackById(int idCodepack)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetById?idCodepack=" + idCodepack);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ListCodePack(string idCboVia, string idCboCrop)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetByVic/?viaID=" + idCboVia + "&cropID=" + idCboCrop);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetCreateCodepack(string description, int clamshellPerBox, double weight, int boxesPerPallet, string layersPerPallet, int clamshellPerContainer, int boxPerContainer, double netWeightContainer, int viaID, int statusID, string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetCreate/?description=" + description + "&clamshellPerBox=" + clamshellPerBox + "&weight=" + weight + "&boxesPerPallet=" + boxesPerPallet + "&layersPerPallet=" + layersPerPallet + "&clamshellPerContainer=" + clamshellPerContainer + "&boxPerContainer=" + boxPerContainer + "&netWeightContainer=" + netWeightContainer + "&viaID=" + viaID + "&statusID=" + statusID + "&cropID=" + cropID);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetUpdateCodepack(int codePackID, string description, int clamshellPerBox, double weight, int boxesPerPallet, string layersPerPallet, int clamshellPerContainer, int boxPerContainer, double netWeightContainer)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetUpdate/?codePackID=" + codePackID + "&description=" + description + "&clamshellPerBox=" + clamshellPerBox + "&weight=" + weight + "&boxesPerPallet=" + boxesPerPallet + "&layersPerPallet=" + layersPerPallet + "&clamshellPerContainer=" + clamshellPerContainer + "&boxPerContainer=" + boxPerContainer + "&netWeightContainer=" + netWeightContainer);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListMarket()
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Market/GetAll");
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListMarketById(string idMarket)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Market/GetById/?idMarket=" + idMarket);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string UpdateMarketById(string marketID, string name, string abbreviation, string percentage, int statusID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Market/GetUpdate/?marketID=" + marketID + "&name=" + name + "&abbreviation=" + abbreviation + "&percentage=" + percentage + "&statusID=" + statusID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListCustomer(string opt, string id, string searchName)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter2?opt=" + opt + "&id=" + id + "&searchName=" + searchName);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListCodepackMarket(string idMarket)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetByMar/?idMarket=" + idMarket);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string CreateMarketCodepack(string marketID, string codepackID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Market/GetMarketCodepackCreate/?marketID=" + marketID + "&codepackID=" + codepackID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string DisableMarketCodepack(string marketID, string codepackID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Market/GetMarketCodepackDisable/?marketID=" + marketID + "&codepackID=" + codepackID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetCustomerByID(string opt, string id)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter/?opt=" + opt + "&id=" + id);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string GetDestinationById(string idDestination)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Destination/GetById/?iddestination=" + idDestination);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string GetConsDestByCust(string customerID, string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Consignee/GetConsDestByCust?customerID=" + customerID + "&cropID=" + cropID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string GetOrigin(string opt, string id)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Origin/ListByFilter?opt=" + opt + "&id=" + id);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string ListDestinationByCustomer(string opt, string id, string mar, string via)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ListBrandGetByCropID(string idcrop)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetByCrop?idcrop=" + idcrop);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ListCodePackGetByCropID(string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetByCropID?cropID=" + cropID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ListSizeGetByCrop(string opt, string id)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Size/ListByFilter?opt=" + opt + "&id=" + id);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string VarietyGetByCrop(string opt, string id, string log)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Variety/ListByFilter?opt=" + opt + "&id=" + id + "&log=" + log);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string GetPackageProductAll(string opt, string id)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    //revisar es por categoryID COMBO AÑADIDO?
    public string CategoryGetByCateg()
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Category/GetByCateg");
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }


    public string GetPresentByCrop(string opt, string id)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }



    public string GetFormatByCrop(string opt, string id)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string CategoryGetByCrop(string opt, string id, string gro)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Category/ListBrandByFilter?opt=" + opt + "&id=" + id + "&gro=" + gro);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string BrandGetByCateID(string opt, string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ListDestination(string opt, string id, string mar, string via)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ListConsignee()
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Consignee/GetAll");
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string ConsigneeGetSpecsConsDest(string customerConsigneeDestinationID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Consignee/GetSpecsConsDest?customerConsigneeDestinationID=" + customerConsigneeDestinationID);
        client.Timeout = -1;
        var request = new RestRequest(Method.GET);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetCreateCustomer(int userID, string name, string address, string contact, string phone, string email, string abbreviation, string idOrigin, string moreinfo, int customer, int consignee, int notify, string ruc, string subsidiaryID, string destinations)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetCreate/?userID=" + userID + "&name=" + name + "&address=" + address + "&contact=" + contact + "&phone=" + phone + "&email=" + email + "&abbreviation=" + abbreviation + "&idOrigin=" + idOrigin + "&moreinfo=" + moreinfo + "&customer=" + customer + "&consignee=" + consignee + "&notify=" + notify + "&ruc=" + ruc + "&subsidiaryID=" + subsidiaryID);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        request.AddHeader("Content-Type", "application/json");
        request.AddParameter("application/json", destinations, ParameterType.RequestBody);
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    //public string SaveConsigneeDestinationID2(string consigneeDestinationID, string customerID, string destinationID, string consigneeID, string cropID) { 
    //  return consigneeDestinationID;
    //}
    public string SaveConsigneeDestinationID(string consigneeDestinationID, string customerID, string destinationID, string consigneeID, string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Consignee/saveConsgineeDestination?consigneeDestinationID=" + consigneeDestinationID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&consigneeID=" + consigneeID + "&cropID=" + cropID);      
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetUpdateCustomer(int userID, string customerID, string name, string address, string contact, string phone, string email, string abbreviation, string moreinfo, int customer, int consignee, int notify, string ruc, string subsidiaryID, string destinations)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetUpdate/?userID=" + userID + "&customerID=" + customerID + "&name=" + name + "&address=" + address + "&contact=" + contact + "&phone=" + phone + "&email=" + email + "&abbreviation=" + abbreviation + "&moreinfo=" + moreinfo + "&customer=" + customer + "&consignee=" + consignee + "&notify=" + notify + "&ruc=" + ruc + "&subsidiaryID=" + subsidiaryID);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        request.AddHeader("Content-Type", "application/json");
        request.AddParameter("application/json", destinations, ParameterType.RequestBody);
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetCustDestCreate(string customerDestinationID, string destinationID, string customerID, string cropID)
    {
        var client = new RestClient(GlobalVars.UriApi+"/api/Customer/GetCustDestCreate/?customerDestinationID=" + customerDestinationID + "&destinationID=" + destinationID + "&customerID=" + customerID + "&cropID=" + cropID);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
    public string GetCustDestDelete(string destinationID, string customerID)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetCustDestDelete/?destinationID=" + destinationID + "&customerID=" + customerID);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }

    public string saveSpecsConsgineeDestination(int customerConsigneeDestinationSpecsID, int consigneeDestinationID, int varietyID, int formatID, int packageProductID, int generic, string categoryID, int brandID, int presentationID, string sizesID, decimal price, string codepackName, string file, int principal, int statusID, string comments, string notes)
    {
        var client = new RestClient(GlobalVars.UriApi + "/api/Consignee/saveSpecsConsgineeDestination?customerConsigneeDestinationSpecsID=" + customerConsigneeDestinationSpecsID + "&consigneeDestinationID=" + consigneeDestinationID + "&varietyID=" + varietyID + "&formatID=" + formatID + "&packageProductID=" + packageProductID + "&generic=" + generic + "&categoryID=" + categoryID + "&brandID=" + brandID + "&presentationID=" + presentationID + "&sizesID=" + sizesID + "&price=" + price + "&codepackName=" + codepackName + "&file=" + file + "&principal=" + principal + "&statusID=" + statusID + "&comments=" + comments + "&notes=" + notes);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
        IRestResponse response = client.Execute(request);
        return response.Content;
    }
        public string ListSubsidiary()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Subsidiary/ListSubsidiary");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}