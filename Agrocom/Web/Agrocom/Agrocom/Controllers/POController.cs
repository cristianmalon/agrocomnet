﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class POController : Controller
    {
        // GET: PO
        //[AuthorizeUser(idMenu: 15)]
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //[AuthorizeUser(idMenu: 15)]
        public ActionResult NewPO()
        {
            loadImage();
            return View();
        }
        public ActionResult NewPoGrapes()
        {
            loadImage();
            return View();
        }
        public ActionResult Confirm()
        {
            loadImage();
            ViewBag.flagcrop = "";
            ViewBag.flagprice = "";
            string cropId = Session["CropID"].ToString();
            ViewBag.flagcrop = (cropId == "BLU") ? "none" : "block";
            ViewBag.flagprice = "none"; //(cropId == "BLU") ? "block" : "none";
            return View();
        }

        public ActionResult ShipmentStatus()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit()
        {
            loadImage();
            return View();
        }

        public ActionResult BoardingProgramming()
        {
            return View();
        }

        public ActionResult ShippingProgram()
        {
            loadImage();
            return View();
        }

        public ActionResult Review()
        {
            loadImage();
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string ConfirmPODocs(int id, int poStatusId)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/SetConfirmPODocs?id=" + id + "&poStatusId=" + poStatusId);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string InserPODocument(string id, string typeDocId, string nameFile, string statusId)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/InserNewPODocument?id=" + id + "&typeDocId=" + typeDocId + "&nameFile=" + nameFile + "&statusId=" + statusId);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string DeletePODocument(string id, string typeDocId)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/SetDeletePODocument?id=" + id + "&typeDocId=" + typeDocId);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListSeason()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Season/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListWeekByCampaign(string idCampaign)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListBySeasonID?seasonID=" + idCampaign);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListWeekBySeason(string campID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/POController/GetListWeekBySeason?campID=" + campID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListWeekByCamp(string campID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/ListWeekByCamp?campID=" + campID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetPOShippingStatusById(string typeID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetConfirmById?IdPO=" + typeID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string LoadPODocsByID(string typeID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetLoadPODocsByID?typeID=" + typeID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string LoadDocuments()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShipmentStDocuments");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ShipmentStPOByWeekId(string idUser, int beginDate, int endDate)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShipmentStPOByWeek?idUser=" + idUser + "&WIni=" + beginDate + "&WFin=" + endDate);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListForConfirm(int WIni, int WFin, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetListForConfirm?WIni=" + WIni + "&WFin=" + WFin + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListFarm(string idGrower)
        {
            var client = new RestClient("http://apiagroamigo.azurewebsites.net" + "/api/Farm/GetByGro?idGrower=" + idGrower);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string GetDetailConfirmById(int IdPO)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetDetailConfirmById?IdPO=" + IdPO);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string GetForPdf(int IdPO)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOInfoForPdf?IdPO=" + IdPO);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListWeekPending(string opt, string id, string cropID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListSalesPendingByWeek(int weekID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetSalesPendingByWeek/?weekID=" + weekID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListVarietyByForecast(string saleDetailID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetVarietyByForecast/?saleDetailID=" + saleDetailID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListVarietyByForecastDetail(string growerID, string farmID, string saleDetailID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetVarietyByForecastDetail/?growerID=" + growerID + "&farmID=" + farmID + "&saleDetailID=" + saleDetailID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListVarietyByForecastDetail2(string growerID, string farmID, string saleDetailID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ENGetVarietyForecastDetail/?growerID=" + growerID + "&farmID=" + farmID + "&saleDetailID=" + saleDetailID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string CreateUpdatePO(int orderProductionID, string projectedweekID, string saleID, string specificationID, string commentary, string userCreated, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetCreateUpdatePO/?orderProductionID =" + orderProductionID + "&projectedweekID=" + projectedweekID + "&saleID=" + saleID + "&specificationID=" + specificationID + "&commentary=" + commentary.Replace("#", "") + "&userCreated=" + userCreated + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string CreateUpdatePODetail(string orderProductionDetailID, string orderProductionID, string saleDetailID, string projectedProductionDetailID, string Quantity, string userCreated, string sizeID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetCreateUpdatePODetail/?orderProductionDetailID=" + orderProductionDetailID + "&orderProductionID=" + orderProductionID + "&saleDetailID=" + saleDetailID + "&projectedProductionDetailID=" + projectedProductionDetailID + "&Quantity=" + Quantity + "&userCreated=" + userCreated + "&sizeID=" + sizeID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetConfirmById(string opt, int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ListPOconfirmedByFilter?opt=" + opt + "&orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string OrderProductionConfirm(string orderProductionID, string statusConfirm, string dateBoardEstimate, string quantityConfirm, string userID, string reasonDecline)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/OrderProductionConfirm?orderProductionID=" + orderProductionID + "&statusConfirm=" + statusConfirm + "&dateBoardEstimate=" + dateBoardEstimate + "&userID=" + userID + "&reasonDecline" + reasonDecline);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", quantityConfirm, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetListCodepackByVia(string viaID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetWithPresentationVia?viaID=" + viaID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string getGrowerData(string userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Grower/GetByUserId?idUser=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDetailByIDForPOEdit(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListByCampaign?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ENGetVarietyForecastDetailForPOEdit(string opt, string growerID, int saleDetailID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ENGetForecastForPOMC?opt=" + opt + "&growerID=" + growerID + "&saleDetailID=" + saleDetailID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDetailForPOEdit(int IdPO)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetDetailForPOEdit?IdPO=" + IdPO);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListVariety()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListLabel()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SavePkPO(int packingListID, int orderProductionID, string nroPackingList, int customerID, string growerID, int totalBoxes, string invoiceGrower, string packingLoadDate, string nroGuide, string vessel, string senasaSeal, string container, string customSeal, string shippingLine, string thermogRegisters, string originPort, string thermogRegistersLocation, string destinationPort, string sensors, string ETA, string sensorsLocation, string ETD, int viaID, string booking, string BLAWB)
        {
            //var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/SavePkPO?packingListID=" + packingListID + "&orderProductionID=" + orderProductionID + "&nroPackingList=" + nroPackingList + "&growerID=" + growerID + "&totalBoxes=" + totalBoxes + "&invoiceGrower=" + invoiceGrower + "&packingLoadDate=" + packingLoadDate + "&nroGuide=" + nroGuide + "&vessel=" + vessel + "&senasaSeal=" + senasaSeal + "&container=" + container + "&customSeal=" + customSeal + "&shippingLine=" + shippingLine + "&thermogRegisters=" + thermogRegisters + "&originPort=" + originPort + "&thermogRegistersLocation=" + thermogRegistersLocation + "&destinationPort=" + destinationPort + "&sensors=" + sensors + "&ETA=" + ETA + "&sensorsLocation=" + sensorsLocation + "&ETD=" + ETD + "&viaID=" +viaID + "&totalBoxes"+ totalBoxes );
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/SavePkPO?packingListID=" + packingListID + "&orderProductionID=" + orderProductionID +
                                    "&nroPackingList=" + nroPackingList + "&growerID=" + growerID + "&invoiceGrower=" + invoiceGrower + "&nroGuide=" + nroGuide + "&customerID=" + customerID +
                                    "&container=" + container + "&senasaSeal=" + senasaSeal + "&customSeal=" + customSeal + "&thermogRegisters=" + thermogRegisters +
                                    "&thermogRegistersLocation=" + thermogRegistersLocation + "&sensors=" + sensors + "&sensorsLocation=" + sensorsLocation + "&packingLoadDate=" + packingLoadDate +
                                    "&booking=" + booking + " &originPort=" + originPort + "&destinationPort=" + destinationPort + "&shippingLine=" + shippingLine + "&vessel=" + vessel + "&BLAWB=" + BLAWB + "&ETD=" + ETD +
                                    "&ETA=" + ETA + "&viaID=" + viaID + "&totalBoxes=" + totalBoxes);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            //request.AddParameter("application/json", datails_table, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SaveDetailPkPO(int packingListDetailID, int packingListID, string numberPallet, int orderPallet, string farmID, int codePackID, int packageProductID, string categoryID, int varietyID, string sizeID, string packingDate, int quantityBoxes, string description, int packingListDetailByVarietyID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/SaveDetailPkPO?packingListDetailID=" + packingListDetailID + "&packingListID=" + packingListID + "&numberPallet=" + numberPallet + "&orderPallet=" + orderPallet +
                    "&farmID=" + farmID + "&codePackID=" + codePackID + "&packageProductID=" + packageProductID + "&categoryID=" + categoryID + "&varietyID=" + varietyID + "&sizeID=" + sizeID + "&packingDate=" + packingDate + "&quantityBoxes=" + quantityBoxes + "&description=" + description + "&packingListDetailByVarietyID=" + packingListDetailByVarietyID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            //request.AddParameter("application/json", datails_table, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string ListGetAll()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListHeaderAll(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetById?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListDetailsAll(int packingListID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetDet?packingListID=" + packingListID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string ListGetDetSum(int packingListID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetDetSum?packingListID=" + packingListID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListSize()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListSalesPendingByWeekForPONew(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string POModify(int orderProductionID, int userID, int statusID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/POModify?orderProductionID=" + orderProductionID + "&userID=" + userID + "&statusID=" + statusID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetForecastBySaleID(int saleID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListForecastBySaleID?saleID=" + saleID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ENGetVarietyForecastDetailForPONew(string opt, string growerID, int saleDetailID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ENGetForecastForPOMC?opt=" + opt + "&growerID=" + growerID + "&saleDetailID=" + saleDetailID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetMarketAll(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Market/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllWeek()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetAllWeek");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetCurrentWeek(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListByCampaign?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetStatus(string opt, string marketID, int weekini, int weekFin, string statusID, string growerID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOFilterMC?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllForEditFiltered(string opt, string marketID, int weekini, int weekFin, string statusID, string growerID, int? campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOFilterMC?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetGrowerAll(string opt, string marketID, int weekini, int weekFin, string statusID, string growerID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOFilterMC?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPOConfirm(string opt, string plantID, int weekProduction, int weekProductionEnd, int statusConfirmID, int campaignID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ListPOByFilter?opt=" + opt + "&plantID=" + plantID +
            "&weekProduction=" + weekProduction + "&weekProductionEnd=" + weekProductionEnd + "&statusConfirmID=" + statusConfirmID +
            "&campaignID=" + campaignID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string AllPlant(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Plant/GetPlantMC?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllWeekConfirm()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetAllWeek");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetGrowerAllConfirm()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetGrowerAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetPOConfirmResumen(string opt, string plantID, int weekProduction, int weekProductionEnd, int statusConfirmID, int campaignID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ListPOByFilter?opt=" + opt + "&plantID=" + plantID +
            "&weekProduction=" + weekProduction + "&weekProductionEnd=" + weekProductionEnd + "&statusConfirmID=" + statusConfirmID +
            "&campaignID=" + campaignID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string OrderProductionConfirmStatus(string cropID, string orderProductionID, string statusConfirmID,
            string completedDate, string loadingDate, int vgm, string day, string userID, string comments,
            string packingID, int editAttemptsPacking, int PromiseCompliance, OrderProductionCab objOrderProduction)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/OrderProductionConfirmStatus?cropID=" + cropID + "&orderProductionID=" + orderProductionID +
                "&statusConfirmID=" + statusConfirmID + "&completedDate=" + completedDate + "&loadingDate=" + loadingDate + "&vgm=" + vgm + "&day=" + day +
                "&userID=" + userID + "&comments=" + comments + "&packingID=" + packingID + "&editAttemptsPacking=" + editAttemptsPacking + "&PromiseCompliance=" + PromiseCompliance);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var sz = JsonConvert.SerializeObject(objOrderProduction);
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        public string ListWeekPendingByCampaignID(string campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/GetOpsByCampaingID?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListSalesPendingByWeekForPONewByCampaignID(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSalesMC/?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetByCampaign(string campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetByCampaign?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetBoardingProgramming(int customerID, int weekID, int destinationID, int statusID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetBoardingProgramming?customerID=" + customerID + "&weekID=" + weekID + "&destinationID=" + destinationID + "&statusID=" + statusID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetBoardingStatus(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetBoardingStatus?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetPOstatusConfirm(string opt, string plantID, int weekProduction, int weekProductionEnd, int statusConfirmID, int campaignID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/ListPOByFilter?opt=" + opt + "&plantID=" + plantID + "&weekProduction=" + weekProduction + "&weekProductionEnd=" + weekProductionEnd + "&statusConfirmID=" + statusConfirmID + "&campaignID=" + campaignID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string POGenerateGrapes(ENPOGenerate o)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/POGenerateGrapes");
            client.Timeout = -1;
            var data = JsonConvert.SerializeObject(o);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetDestinationByCampaign(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetDestinationByCampaign?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetStatusQuan(int customerID, int weekID, int destinationID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetStatusQuan?customerID=" + customerID + "&weekID=" + weekID + "&destinationID=" + destinationID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetUpdStatus(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetUpdStatus?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetListPOReview(string opt, string marketID, int weekini, int weekFin, string statusID, string growerID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOFilterMC?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPObyPlant(int userID, ENPObyPlant PObyPlantt)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPObyPlant?userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var sz = JsonConvert.SerializeObject(PObyPlantt);
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDetailConfirmByIdMC(string Option, int IdPO)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetDetailConfirmByIdMC?Option=" + Option + "&IdPO=" + IdPO);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetForPdfMC(int IdPO)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetPOInfoForPdf?IdPO=" + IdPO);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string VerifySaleOrderInProcess(string correlativeSaleOrder)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/OrderInProcess?correlativeSaleOrder=" + correlativeSaleOrder);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}