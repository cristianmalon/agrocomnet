﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
//using Excel = Microsoft.Office.Interop.Excel;

namespace Agrocom.Controllers
{
    public class PackingListController : Controller
    {
        // GET: PackingList
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Quality()
        {
            return View();
        }

        public string SaveDetailPkPO(int packingListDetailID, int packingListID, string numberPallet, int orderPallet, string farmID, int codePackID, int packageProductID, string categoryID, int varietyID, string sizeID, string packingDate, int quantityBoxes, string description, int packingListDetailByVarietyID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/SaveDetailPkPO?packingListDetailID=" + packingListDetailID + "&packingListID=" + packingListID + "&numberPallet=" + numberPallet + "&orderPallet=" + orderPallet +
                "&farmID=" + farmID + "&codePackID=" + codePackID + "&packageProductID=" + packageProductID + "&categoryID=" + categoryID + "&varietyID=" + varietyID + "&sizeID=" + sizeID + "&packingDate=" + packingDate + "&quantityBoxes=" + quantityBoxes + "&description=" + description + "&packingListDetailByVarietyID=" + packingListDetailByVarietyID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            //request.AddParameter("application/json", datails_table, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string ListSize()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPacking()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListHeader(string packingListID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetById?packingListID=" + packingListID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListDetails(string packingListID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetDet?packingListID=" + packingListID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveQA(ENPackingList o)
        {
            //return o.packingListID.ToString();
            //var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetDet?packingListID=" + packingListID);
            //client.Timeout = -1;
            //var request = new RestRequest(Method.GET);
            //IRestResponse response = client.Execute(request);
            //return response.Content;

            var client = new RestClient(GlobalVars.UriApi + "/api/packingList/SaveQA");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content;
        }

        public string GetAllForQA()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingList/GetAllForQA");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            return response.Content;
        }

        public string GeByIDForQA(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingList/GetByIDForQA?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


    }
}