﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using System.Web.Http.Cors;
using Agrocom.Filters;
using Agrocom.Models;
using Common.Entities;

namespace Agrocom.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports Power BI
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShipmentSummary()
        {
            loadImage();
            return View();
        }

        public ActionResult AmericaSalesPlan()
        {
            return View();
        }

        public ActionResult SalesPlanCountry()
        {
            return View();
        }

        public ActionResult ExpoMaster()
        {
            return View();
        }

        public ActionResult Forecast()
        {
            return View();
        }

        public ActionResult ShippingAGA()
        {
            loadImage();
            return View();
        }
        public ActionResult VariationForecast()
        {
            return View();
        }

        public ActionResult ProjectedvsReal()
        {
            return View();
        }
        //Reportes C#
        public ActionResult Summary()
        {
            loadImage();
            return View();
        }

        public ActionResult Expo()
        {
            return View();
        }
        public ActionResult ShippingI()
        {
            loadImage();
            return View();
        }

        public ActionResult VarietyByCampaign()
        {
            return View();
        }

        public ActionResult CatByPlant()
        {
            return View();
        }
        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string SummaryList(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryList?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SummaryListByPO(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryListByPO?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string summaryListByWeek(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryListByWeek?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string summaryListByPacking(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryListByPacking?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string summaryListByCustomer(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryListByCustomer?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string summaryListByStatus(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryListByStatus?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SummaryListByDestination(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/summaryListByDestination?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ExpoList()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/expoList");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ExpoListByPO()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/expoListByPO");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ExpoListByGrower()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/expoListByGrower");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ExpoListByCustomer()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/expoListByCustomer");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ExpoListByDestination()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/expoListByDestination");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ShippingIList(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/ListByFilter?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ShippingIListByPO(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByPO?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string shippingIListByPL(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByPL?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string shippingIListByCustomer(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByCustomer?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string shippingIListByManager(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByManager?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string shippingIListByLogistic(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByLogistic?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string shippingIListByWeek(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByWeek?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string shippingIListByDestination(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/shippingIListByDestination?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetSincroIE()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Aga/Demo");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetSincroPL()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Aga/GetPackingListAga");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveSAllocated(string nroPackingList)
        {
      //var nroPackingList1 = System.Web.HttpUtility.HtmlEncode(nroPackingList);
          nroPackingList = nroPackingList.Replace(" ", "%20").Replace("/", "%2F");
          var url = GlobalVars.UriApi + "/api/packingListPO/SaveSAllocated?nroPackingList=" + nroPackingList;
          var client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //CANVAS

        public ActionResult HarvestForecast()
        {
            return View();
        }

        public ActionResult ReportVersus()
        {
            return View();
        }

        public string GetreportVsFor()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/GetreportVsFor");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetreportVsSR()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/GetreportVsSR");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetreportVsPO()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/GetreportVsPO");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GePackinglistApache(string opt)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Reports/GePackinglistApache?opt=" + opt);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDataUpdate(string opt, int id, string data1, string data2, string data3, string data4)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/packingListPO/GetDataUpdate?opt=" + opt + "&id=" + id + "&data1=" + data1 + "&data2=" + data2 + "&data3=" + data3 + "&data4=" + data4);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}