﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using System.Web.Http.Cors;
using Agrocom.Filters;
using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;

namespace Agrocom.Controllers
{
    public class SalesController : Controller
    {
        // GET: Sales
        // [AuthorizeUser(idMenu: 7)]
        public ActionResult Index()
        {
            loadImage();
            return View();
        }

        public ActionResult Grapes()
        {
            loadImage();
            return View();
        }

        //public ActionResult Index2()
        //{
        //    return View();
        //}

        public ActionResult SalesReport()
        {
            loadImage();
            return View();
        }

        public ActionResult SalesAllocation()
        {
            return View();
        }
        public ActionResult SalesAllocationCreate()
        {
            return View();
        }

        public ActionResult SalesQuotation()
        {
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string ListSales(int weekIni, int weekEnd, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/List?weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListSaleDetailsByID(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListByCampaign?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetSaleDetailList(int saleID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSaleDetailList?saleID=" + saleID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListMarket(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Market/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCustomer(string marketID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListAllWithMarket(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListWeekBySeason(int idSeason)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Week/ListBySeason?campaignID=" + idSeason + "");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListDestination(string marketID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/GetByMarkId?idmarket=" + marketID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListDestinationSales()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/GetSales");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCodepackWithVia(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetByCropID?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListVia(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Via/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListKam(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/KamListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCodepack()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string VarietyGetByCrop(string idCrop)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/GetByCrop?idCrop=" + idCrop);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string BrandGetByCateID(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetLabelByCropID(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/GetLabelByCropID?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string getCodepackByFormatID(string formatID, string packageProductID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/getCodepackByFormatID?formatID=" + formatID + "&packageProductID=" + packageProductID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string GetFormatByCrop(string cropID)
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/GetFormatByCropID?cropID=" + cropID);
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string ListSizeGetByCrop(string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/GetByCrop?cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPresentByCrop(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPackageProductAll(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string CategoryGetByCrop(string opt, string id, string gro)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Category/ListBrandByFilter?opt=" + opt + "&id=" + id + "&gro" + gro);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListBrand(string opt, string id, string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListByFilter?opt=" + opt + "&id=" + id + "&growerID" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListVariety()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListConditionPayment(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ConditionPayment/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveDetail(int saleDetailID, int destinationID, int projectedWeekID, int customerID, double quantity, double price, string paymentTerm, int variedadID, string categoryID, int codePackID, int conditionPaymentID, int incotermID, int userCreated)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveDetail?saleDetailID=" + saleDetailID + "&destinationID=" + destinationID + "&projectedWeekID=" + projectedWeekID + "&customerID=" + customerID + "&quantity=" + quantity + "&price=" + price + "&paymentTerm=" + paymentTerm + "&variedadID=" + variedadID + "&categoryID=" + categoryID + "&codePackID=" + codePackID + "&conditionPaymentID=" + conditionPaymentID + "&incotermID=" + incotermID + "&userCreated=" + userCreated);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveSale(int destinationID, int projectedWeekID, int customerID, int userCreated)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveSale?destinationID=" + destinationID + "&projectedWeekID=" + projectedWeekID + "&customerID=" + customerID + "&userCreated=" + userCreated);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveSaleDetail(int saleID, double quantity, double price, string paymentTerm, int varietyID, string categoryID, int codePackID, int conditionPaymentID, int incotermID, int userCreated)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveSaleDetail?saleID=" + saleID + "&quantity=" + quantity + "&price=" + price + "&paymentTerm=" + paymentTerm + "&variedadID=" + varietyID + "&categoryID=" + categoryID + "&codePackID=" + codePackID + "&conditionPaymentID=" + conditionPaymentID + "&incotermID=" + incotermID + "&userCreated=" + userCreated);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaleDetailSaveUpdate(int destinationID, int projectedWeekID, int customerID, int userCreated, string saleDetail_table)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaleDetailSaveUpdate?destinationID=" + destinationID + "&projectedWeekID=" + projectedWeekID + "&customerID=" + customerID + "&userCreated=" + userCreated);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", saleDetail_table, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveSaleBulk(int saleID, int destinationID, int projectedWeekID, int customerID, int userCreated, int wowID, string comments, int status, string datails_table)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveSaleBulk?saleID=" + saleID + "&destinationID=" + destinationID + "&projectedWeekID=" + projectedWeekID + "&customerID=" + customerID + "&userCreated=" + userCreated + "&wowID=" + wowID + "&comments=" + comments + "&status=" + status);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", datails_table, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;

        }

        public string SaveSaleEN(ENSales o)
        {
            o.ReplaceNull();
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveSaleEN");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveSOwithoutProgram(ENSales o)
        {
            o.ReplaceNull();
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveSOwithoutProgram");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListWowByCustomer(int customerID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/ListByCustomer?customerID=" + customerID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        //public string GetAllPort()
        //{
        //    var client = new RestClient(GlobalVars.UriApi + "/api/Wow/GetAllPort");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.GET);
        //    IRestResponse response = client.Execute(request);
        //    return response.Content;
        //}

        public string GetAllSizes(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetCodepackWithPresentation(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListReport(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListReport?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListReportCustomer(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSalesReportMC?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListReportVia(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSalesReportMC?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListReportCodepack(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSalesReportMC?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListReportDestination(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetSalesReportMC?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListReportBox(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListReportBox?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListReportSum(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListReportSum?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListReportBoxSum(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListReportBoxSum?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string DeleteSales(int saleID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/DeleteSales?saleID=" + saleID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCustomerGetBySales()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetBySales");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCustomerGetAllPresentations(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListForecastBySales(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListBySale?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListForecastBySalesVar(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Forecast/ListBySale?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string ListReportPO(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListReportPO?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPaymentTermByCustomer(int customerID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetPaymentTermByCustomer?customerID=" + customerID.ToString() + "&cropiD=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string CodepackByFormatIDPackageProductID(string opt, string viaid, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByFilter?opt=" + opt + "&viaid=" + viaid + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetMarketByUserID(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllWeek(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListByCampaign?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetCurrentWeek(string opt, string val, int weekIni, int weekEnd, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetNextWeek(string opt, string val, int weekIni, int weekEnd, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllFiltered(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetSalesAllocation(int projectedWeekIni, int projectedWeekEnd, string growerID, string marketID, int customerID)
        {
            growerID = growerID ?? "";
            marketID = marketID ?? "";
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetsalesAllocation?projectedWeekIni=" + projectedWeekIni + "&projectedWeekEnd=" + projectedWeekEnd + "&growerID=" + growerID + "&marketID=" + marketID + "&customerID=" + customerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetsalesAllocationWeek(int projectedWeekIni, int projectedWeekEnd, string growerID, string marketID, int customerID)
        {
            growerID = growerID ?? "";
            marketID = marketID ?? "";
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetsalesAllocationWeek?projectedWeekIni=" + projectedWeekIni + "&projectedWeekEnd=" + projectedWeekEnd + "&growerID=" + growerID + "&marketID=" + marketID + "&customerID=" + customerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetGrowerAll()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/PO/GetGrowerAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetMarketAll()
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Market/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetMountByStatus(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetMountByStatus?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetStatusByCampaign(string campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetStatusByCampaign?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        public string GetListByCampaignID(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListByCampaign?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetStatusRejectPO(int saleID, string commentary)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetStatusByRej?saleID=" + saleID + "&commentary=" + commentary);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetStatusByMod(int saleID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetStatusByMod?saleID=" + saleID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetsalesOutSpecs(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetsalesOutSpecs?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveSalePC(ENSalesPC o)
        {
            o.ReplaceNull();
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/SaveSalePC");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListWowByCustomerCrop(int customerID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/ListByCustomer?customerID=" + customerID + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetSalesDetailModifiedPO(int saleID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/sales/GetSalesDetailModifiedPO?saleID=" + saleID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetNextWeekMC(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListPaymentTermByCustomerMC(string opt, int customerID, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListPaymentTermByCustomer?opt=" + opt + "&customerID=" + customerID + "&cropiD=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListViaMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Via/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListConditionPaymentMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ConditionPayment/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPackageProductAllMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListPackageByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListMarketMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Market/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListAllWithMarketMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetFormatByCropIDMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string CategoryGetByCropMC(string opt, string id, string gro)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Category/ListBrandByFilter?opt=" + opt + "&id=" + id + "&gro= " + gro);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListSizeGetByCropMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetStatusByFilterMC(string opt, string val, int weekIni, int weekEnd, int userID, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string VarietyGetByCropMC(string opt, string id, string log)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Variety/ListByFilter?opt=" + opt + "&id=" + id + "&log=" + log);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetPresentByCropMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByCrop?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetLabelByCropIDMC(string opt, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListCategoryByFilter?opt=" + opt + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string CodepackByFormatIDPackageProductIDMC(string opt, string viaid, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/CodePack/ListFormatByFilter?opt=" + opt + "&viaid=" + viaid + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetStatusMC(string opt, string val, int? weekIni, int? weekEnd, int? userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/ListStatusByFilter?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDocument(string opt, string value)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetDocument?opt=" + opt + "&value=" + value);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetIncotermMC(string opt, string cropID, int viaID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Sales/GetIncotermMC?opt=" + opt + "&cropID=" + cropID + "&viaID=" + viaID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListBrandMC(string opt, string id, string growerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Brand/ListByFilter?opt=" + opt + "&id=" + id + "&growerID=" + growerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllSizesMC(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Size/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}