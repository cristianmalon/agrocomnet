﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class ShippingProgramController : Controller
    {
        // GET: ShippingProgram
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Review()
        {
            return View();
        }
        public string GetAllPO(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/GetAllPO?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDetailPO(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/GetDetailPO?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GeDocumentsByPOID(int orderProductionID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/GeDocumentsByPOID?orderProductionID=" + orderProductionID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveShippingProgramPO(ENDocumentOrderProduction o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/SaveShippingProgramPO");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveShippingDocumentPO(ENDocumentShippingDocumentPO o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/SaveShippingDocumentPO");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllPOForCustomer(int campaignID, int userID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/GetAllPOForCustomer?campaignID=" + campaignID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveShippingProgramPOforCustomer(ENDocumentOrderProduction o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/SaveShippingProgramPOforCustomer");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveShippingDocumentPOForCustomer(ENDocumentShippingDocumentPO o)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/DocumentExport/SaveShippingDocumentPOForCustomer");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingTraylist(int campaignID, int customerID, int destinationID, string nroPackingList, int managerID, int logisticOperatorID, int processPlantID, int projectedWeekID, int viaID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingTrayList?campaignID=" + campaignID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&nroPackingList=" + nroPackingList + "&managerID=" + managerID + "&logisticOperatorID=" + logisticOperatorID + "&processPlantID=" + processPlantID + "&projectedWeekID=" + projectedWeekID + "&viaID=" + viaID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingByCustomer(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByCustomer?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingByDestination(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByDestination?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingPackingList(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingPackingList?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingByManager(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByManager?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingByOperator(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByOperator?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetShippingByPlant(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByPlant?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingByWeek(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByWeek?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetShippingByVia(int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetShippingByVia?campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetDateUpdate(string opt, int id, string data1, string data2, string data3, string data4, string data5, string data6, string data7)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/ShippingStatus/GetDateUpdate?opt=" + opt + "&id=" + id + "&data1=" + data1 + "&data2=" + data2 + "&data3=" + data3 + "&data4=" + data4 + "&data5=" + data5 + "&data6=" + data6 + "&data7=" + data7);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}