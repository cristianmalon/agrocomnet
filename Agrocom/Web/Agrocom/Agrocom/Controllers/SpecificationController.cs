﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using System.Web.Http.Cors;
using Agrocom.Filters;
using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;


namespace Agrocom.Controllers
{
    public class SpecificationController : Controller
    {
        // GET: Specification
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult BigBold()
        {
            return View();
        }

        public ActionResult Blu()
        {
            return View();
        }
        public ActionResult Ozblu()
        {
            return View();
        }
        public ActionResult SpecsClient()
        {
            loadImage();
            return View();
        }
        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string GetSpecsClientList(string opt, int campaignID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Specification/GetSpecsClientList?opt=" + opt + "&campaignID=" + campaignID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListCustomerWithDestination(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetClientSaveUpdate(int campaignID, string cropID, int userID, string customers)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Specification/GetClientSaveUpdate/?campaignID=" + campaignID + "&cropID=" + cropID + "&userID=" + userID);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", customers, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string SaveProductUpdate(int campaignID, int userID, string file, MassLoadProduct objMassiveUpload)
        {            
            var client = new RestClient(GlobalVars.UriApi + "/api/Specification/SaveProductUpdate?campaignID=" + campaignID + "&userID=" + userID + "&file=" + file);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(objMassiveUpload);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}