﻿using Agrocom.Models;
using Common.Entities;
using Newtonsoft.Json;
using RestSharp;
using System.Web.Mvc;

namespace Agrocom.Controllers
{
    public class WowController : Controller
    {
        // GET: Wow
        public ActionResult Index()
        {
            loadImage();
            return View();
        }

        public void loadImage()
        {
            ViewBag.contentwrapper = Session["contentwrapper"];
            ViewBag.imgfondo = Session["imgfondo"];
            ViewBag.logo = Session["logo"];
            ViewBag.linealogo = Session["linealogo"];
            ViewBag.colorFooter = Session["colorFooter"];
            ViewBag.infofoot = Session["infofoot"];
        }

        public string ListWow(string opt, string id, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetById(string opt, string id, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/ListByFilter?opt=" + opt + "&id=" + id + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListAllWithMarket(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListCustomer(string marketID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetAll");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string ListDestinationAll(string opt, string id, string mar, string via)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Destination/ListByFilter?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string ListMarket(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Market/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetCustomerById(int customerID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Customer/GetById?idCustomer=" + customerID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public string GetForSpecs(int idPO, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/GetForSpecs?idPO=" + idPO + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetForBilling(int idPO, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/GetForBilling?idPO=" + idPO + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string SaveWow(int wowID, int customerID, int destinationID, int directInvoice, string certificationID
        , int documents, int documentsMoreInfo, string documentsInfo, int packingListID, int insuranceCertificate
        , int phytosanitary, int phytosanitaryMoreInfo, string phytosanitaryInfo, int certificateOrigin, int certificateOriginMoreInfo
        , string certificateOriginInfo, string moreInfo, int consigneeID, string consigneeAddress, string consigneeContact
        , string consigneePhone, int notifyID, string notifyIDAddress, string notifyIDContact, string notifyIDPhone
        , int notifyID2, string notifyID2Address, string notifyID2Contact, string notifyID2Phone, int notifyID3
        , string notifyID3Address, string notifyID3Contact, string notifyID3Phone, string docsCopyEmailInfo, string noted
        , string price, string accountSale, string finalPaymentConditions, string advanceCondition, int userID
        , string documentsExportSea, string documentsExportAir)
        {

            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/Create?wowID=" +
                wowID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&directInvoice=" + directInvoice + "&certificationID=" + certificationID +
                "&documents=" + documents + "&documentsMoreInfo=" + documentsMoreInfo + "&documentsInfo=" + documentsInfo + "&packingListID=" + packingListID + "&insuranceCertificate=" + insuranceCertificate +
                "&phytosanitary=" + phytosanitary + "&phytosanitaryMoreInfo=" + phytosanitaryMoreInfo + "&phytosanitaryInfo=" + phytosanitaryInfo + "&certificateOrigin=" + certificateOrigin + "&certificateOriginMoreInfo=" + certificateOriginMoreInfo +
                "&certificateOriginInfo=" + certificateOriginInfo + "&moreInfo=" + moreInfo + "&consigneeID=" + consigneeID + "&consigneeAddress=" + consigneeAddress + "&consigneeContact=" + consigneeContact +
                "&consigneePhone=" + consigneePhone + "&notifyID=" + notifyID + "&notifyIDAddress=" + notifyIDAddress + "&notifyIDContact=" + notifyIDContact + "&notifyIDPhone=" + notifyIDPhone +
                "&notifyID2=" + notifyID2 + "&notifyID2Address=" + notifyID2Address + "&notifyID2Contact=" + notifyID2Contact + "&notifyID2Phone=" + notifyID2Phone + "&notifyID3=" + notifyID3 +
                "&notifyID3Address=" + notifyID3Address + "&notifyID3Contact=" + notifyID3Contact + "&notifyID3Phone=" + notifyID3Phone + "&docsCopyEmailInfo=" + docsCopyEmailInfo + "&noted=" + noted +
                "&price=" + price + "&accountSale=" + accountSale + "&finalPaymentConditions=" + finalPaymentConditions + "&advanceCondition=" + advanceCondition + "&userID=" + userID +
                "&documentsExportSea=" + documentsExportSea + "&documentsExportAir=" + documentsExportAir);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAllCetrifications(string opt, string id)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Certification/ListByFilter?opt=" + opt + "&id=" + id);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        [HttpPost]
        public string Save(ENWow o)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/Save");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            var sz = JsonConvert.SerializeObject(o);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", sz, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetForBillingMC(string opt, string id, string cropID)
        {
            var client = new RestClient(GlobalVars.UriApi + "/api/Wow/GetWowMC?opt=" + opt + "&id=" + id + "&cropID=" + cropID);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Session["Token"].ToString());
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}