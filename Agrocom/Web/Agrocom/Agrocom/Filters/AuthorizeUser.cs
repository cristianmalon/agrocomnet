﻿using Agrocom.Models;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class AuthorizeUser : AuthorizeAttribute
    {
        private Users oUser;
        private int idMenu;
        public AuthorizeUser(int idMenu = 0)
        {
            this.idMenu = idMenu;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            String nombreOperacion = "";
            String nombreModulo = "";


            oUser = (Users)HttpContext.Current.Session["User"];
            var lMenu = (List<Menu>)HttpContext.Current.Session["Menu"];

            //lMenu.ForEach(x => x.menusChildren.Any(y => y.menuID == idMenu));
            var find = false;
            if (lMenu!= null)
            {
                foreach (var item in lMenu)
                {
                    if (item.menusChildren.Any(x => x.menuID == idMenu))
                    {
                        find = true;
                    }
                }
            }

            if (!find)
            {
                filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?operacion=" + nombreOperacion + "&modulo=" + nombreModulo + "&msjeErrorExcepcion=");
            }

        }

        //public string getNombreDeOperacion(int idOperacion)
        //{
        //    var ope = from op in db.operaciones
        //              where op.id == idOperacion
        //              select op.nombre;
        //    String nombreOperacion;
        //    try
        //    {
        //        nombreOperacion = ope.First();
        //    }
        //    catch (Exception)
        //    {
        //        nombreOperacion = "";
        //    }
        //    return nombreOperacion;
        //}

    }
}