﻿using Agrocom.Controllers;
using Agrocom.Models;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrocom.Filters
{
    public class VerifySession : ActionFilterAttribute
    {
        private Users oUser;
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //HttpContext.Current.Session.Timeout = 11;
            //session
            try
            {
                base.OnActionExecuting(filterContext);

                oUser = (Users)HttpContext.Current.Session["User"];
                if (oUser == null)
                {

                    if (filterContext.Controller is LoginController == false)
                    {
                        filterContext.HttpContext.Response.Redirect("/Login/Index");
                    }



                }

            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?operacion=''&modulo=''&msjeErrorExcepcion= ''");
            }

        }
    }
}