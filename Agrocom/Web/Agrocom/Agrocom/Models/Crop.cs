﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrocom.Models
{
    public class Crop
    {
        public string cropID { get; set; }
        public string crop { get; set; }
        public string campaign { get; set; }
        public int campaignID { get; set; }
    }
}