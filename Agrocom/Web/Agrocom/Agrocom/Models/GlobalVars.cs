﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Agrocom.Models
{
    public class GlobalVars
    {
        public static string UriApi
        {
            get { return ConfigurationManager.AppSettings["uriApiAgrocom"]; }
        }
    }
}