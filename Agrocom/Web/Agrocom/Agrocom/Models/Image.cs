﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrocom.Models
{
    public class Image
    {
        public string settingDetailKey { get; set; }
        public int settingOrder { get; set; }
        public string settingValue { get; set; }
    }
}