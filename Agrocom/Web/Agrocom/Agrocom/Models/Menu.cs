﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrocom.Models
{
    public class Menus
    {
        public int menuID { get; set; }
        public string description { get; set; }
        public int father { get; set; }
        public string page { get; set; }
        public int status { get; set; }
        public int order { get; set; }
        public string icon { get; set; }
    }
}