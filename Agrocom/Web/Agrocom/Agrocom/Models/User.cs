﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrocom.Models
{
    public class User
    {
        public int userId { get; set; }
        public string login { get; set; }
        public string DNI { get; set; }
        public string paternalLastName { get; set; }
        public string maternalLastName { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public Nullable<System.DateTime> originalDate { get; set; }
        public Nullable<System.DateTime> lastEntryDate { get; set; }
        public string email { get; set; }
        public string passwdSalt { get; set; }
        public string passwdHash { get; set; }
        public int statusID { get; set; }
        public string phone { get; set; }
    }
}