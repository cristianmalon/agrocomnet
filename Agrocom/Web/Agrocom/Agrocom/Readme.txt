﻿Solution for getting: site under construction message after azure webapp deployment
===================================================================================
1)Open your app service in Azure. On left hand panel, scroll and find Advanced Tools. Click Go. This will take you to Kudu service for your app service.
2)On the nav bar, click Tools > Zip push deploy.
3)There will be a file app_offline.htm in the list.
4)Click on delete button left to it and delete it. That's it! Your app should be up now.

Note: To set in maintance mode, follow each instrucction:
1st.Option: Rename file: SiteMaintenance.html to app_offline.htm and then upload it to root

2nd.Option: In advance tools > zip push deploy, clic "+" to create a new file and rename it with: app_offline.htm