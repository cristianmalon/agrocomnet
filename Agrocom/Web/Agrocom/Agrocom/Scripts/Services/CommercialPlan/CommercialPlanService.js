﻿//var fnListWeekByCampaign = function(campaignID, success) {

//    fnGet("/Forecast/ListWeekByCampaign?idCampaign=" + campaignID, success);
//}

var fnCategoryGetByCrop = function (cropID, success) {
    var opt = 'cro';
    var id = cropID;
    var gro = '';
    fnGet("/Sales/CategoryGetByCrop?opt=" + opt + "&id=" + id + "&gro=" + gro, success);
}

var fnGetFormatByCrop = function (cropID, success) {
    var opt = 'fci';
    var id = cropID;
    fnGet("/Sales/GetFormatByCropIDMC?opt=" + opt + "&id=" + id, success);
}

var fnGetPackageProductAll = function (success) {
    var opt = '';
    var id = '';
    fnGet("/Sales/GetPackageProductAll?opt=" + opt + "&id=" + id, success);
}

var fnGetForecastForPlan = function (campaignID, success) {
    var opt = '';
    fnGet("/Forecast/GetForecastForPlan?opt=" + opt + "&campaignID=" + campaignID, success);
}

var fnGetCurrentWeek = function (cropID, campaignID, success) {
    var opt = 'act';
    var id = '';
    fnGet("/Forecast/ListWeeksMC?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID, success);
}

var fnGetVarCatByCrop = function (cropID, success) {
    var opt = 'vcc';
    var id = cropID;
    var log = '';
    fnGet("/CommercialPlan/GetVarCatByCrop?opt=" + opt + "&id=" + id + "&log=" + log, success);
}

var fnCommercialPlan = function (cropID, success) {
    var opt = 'id';
    var id = cropID;
    fnGet("/CommercialPlan/ListCrop?opt=" + opt + "&id=" + id, success);
}

var fnListSeason = function (success) {
    var opt = 'all';
    var id = '';
    fnGet("/CommercialPlan/ListSeason?opt=" + opt + "&id=" + id, success);
}

var fnListOriginWithGrower = function (success) {
    var opt = 'gro';
    var id = '';
    fnGet("/CommercialPlan/ListOriginWithGrower?opt=" + opt + "&id=" + id, success);
}

var fnListGrower = function (id, cropID, success) {
    var opt = 'cro';
    fnGet("/CommercialPlan/ListGrower?opt=" + opt + "&id=" + id + "&cropID=" + cropID, success);
}

var fnListPlanById = function (id, success) {
    var opt = 'id';
    var cropId = '';
    var seasonId = '';
    var originId = '';
    var growerId = '';
    fnGet("/CommercialPlan/ListPlanById?opt=" + opt + "&id=" + id + "&cropId=" + cropId +
        "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId, success);
}

var fnListPlan = function (success) {
    var opt = 'all';
    var id = '';
    var cropId = '';
    var seasonId = '';
    var originId = '';
    var growerId = '';
    fnGet("/CommercialPlan/ListPlan?opt=" + opt + "&id=" + id + "&cropId=" + cropId +
        "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId, success);
}

var fnListWeekBySeason = function (idSeason, success) {
    fnGet2("/CommercialPlan/ListWeekBySeason?idSeason=" + idSeason, success);
}

var fnListForecast = function (idGrower, idSeason, success) {
    fnGet("/CommercialPlan/ListForecast?idGrower=" + idGrower + "&idSeason=" + idSeason, success);
}

var fnListMarket = function (success) {
    var opt = '';
    var id = '';
    fnGet("/CommercialPlan/ListMarket?opt=" + opt + "&id=" + id, success);
}

var fnListCodepack = function (success) {
    fnGet("/CommercialPlan/ListCodepack", success);
}

var fnListCodepackWithVia = function (cropID, success) {
    var opt = '';
    var id = cropID;
    fnGet("/CommercialPlan/ListCodepackWithVia?opt=" + opt + "&id=" + id, success);
}

var fnListDestinationAll = function (success) {
    var opt = 'all';
    var id = '';
    var mar = '';
    var via = '';
    fnGet("/CommercialPlan/ListDestinationAll?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via, success);
}

var fnListDestinationAllWithCustomer = function (success) {
    var opt = 'all';
    var id = '';
    var mar = '';
    var via = '';
    fnGet("/CommercialPlan/ListDestinationAllWithCustomer?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via, success);
}

var fnListAllWithMarket = function (success) {
    var opt = 'wmr';
    var id = '';
    fnGet("/CommercialPlan/ListAllWithMarket?opt=" + opt + "&id=" + id, success);
}

var fnListOrigin = function (success) {
    var opt = 'all';
    var id = '';
    fnGet("/CommercialPlan/ListOrigin?opt=" + opt + "&id=" + id, success);
}

var fnListCustomerRequestByPlanId = function (idPlan, success) {
    var searchOpt = 'plan';
    var planID = idPlan;
    var marketID = '';
    fnGet("/CommercialPlan/ListCustomerRequestByPlan?searchOpt=" + searchOpt + "&planID=" + idPlan + "&marketID=" + marketID, success);
}

var fnListPlanAlternative = function (idPlanOrigin, success) {
    var searchOpt = 'oid';
    fnGet("/CommercialPlan/ListPlanAlternative?searchOpt=" + searchOpt + "&planOriginID=" + idPlanOrigin, success);
}

var fnListPlanCustomerByPlanAlternative = function (planAlternativeID, success) {
    var searchOpt = 'pla';
    fnGet("/CommercialPlan/ListPlanCustomerByPlanAlternative?searchOpt=" + searchOpt + "&planAlternativeID=" + planAlternativeID, success);
}

var fnListCustomerRequestWeekByPlanCustomerId = function (planCustomerRequestID, success) {
    var searchOpt = 'cid';
    fnGet("/CommercialPlan/ListCustomerRequestWeekByPlanCustomerId?searchOpt=" + searchOpt + "&planCustomerRequestID=" + planCustomerRequestID, success);
}

var fnGet = function (url, success) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
        } else {
            //console.log('Error call url: ' + url + ', Response:' + xhr.responseText);
        }
    }
    xhr.send();
}

var fnGet2 = function (url, successmethod) {
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        success: successmethod
    });
}

var fnPost = function (url, params, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
            //console.log("POST SERVICES : " + xhr.responseText);
        } else {
            error(xhr.responseText);
        }
    }
    xhr.send(params);
}   