﻿var fnGetLocalCurrency = function (success) {
    var opt = "cur";
    var campaignID = 0;
    var nroPackinglist = '';
    var container = '';
    var dayini = '';
    var dayfin = '';
    fnGet2("/Finance/GetLocalCurrency?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin, success);
}

var fnGetDocumentType = function (success) {
    var opt = "doc";
    var campaignID = 0;
    var nroPackinglist = '';
    var container = '';
    var dayini = '';
    var dayfin = '';
    var periodo = '';
    var statusid = '';
        fnGet2("/Finance/GetDocumentType?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin + "&periodo=" + periodo + "&statusid=" + statusid, success);
}

var fnGetPaymentsType = function (success) {
    var opt = "pay";
    var campaignID = localStorage.campaignID;
    var nroPackinglist = '';
    var container = '';
    var dayini = '';
    var dayfin = '';
    fnGet2("/Finance/GetPaymentsType?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin, success);
}

var fnGetPacking = function (campaignID, success) {
    var opt = "pak";
    var nroPackinglist = '';
    var container = '';
    var dayini = '';
    var dayfin = '';
    fnGet2("/Finance/GetPacking?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin, success);
}

var fnGetContainer = function (campaignID, success) {
    var opt = "con";
    var nroPackinglist = '';
    var container = '';
    var dayini = '';
    var dayfin = '';
    fnGet2("/Finance/GetContainer?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin, success);
}

var fnGetDocumentsDetailByID = function (orderProductionID, customerInvoiceID, success, error) {
    fnGet3("/Finance/GetDocumentsDetailByID?orderProductionID=" + orderProductionID + "&customerInvoiceID=" + customerInvoiceID, success, error);
}

var fnGetPaymentsDetailByID = function (orderProductionID, customerPaymentID, success, error) {
    fnGet3("/Finance/GetPaymentsDetailByID?orderProductionID=" + orderProductionID + "&customerPaymentID=" + customerPaymentID, success, error);
}

var fnGetDocumentsByPOID = function (orderProductionID, success, error) {
    fnGet3("/Finance/GetDocumentsByPOID?orderProductionID=" + orderProductionID, success, error);
}

var fnGetPaymentByPOID = function (orderProductionID, success, error) {
    fnGet3("/Finance/GetPaymentByPOID?orderProductionID=" + orderProductionID, success, error);
}

var fnGetPOForFinance = function (orderProductionID, campaignID, userID , success, error) {
    fnGet3("/Finance/GetPOForFinance?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID, success, error);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODOS GET Y POST >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
var fnGet = function (url, success) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
        } else {
            console.log('Error call url: ' + url + ', Response:' + xhr.responseText);
        }
    }
    xhr.send();
}

var fnGet2 = function (url, successmethod) {
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        success: successmethod 
    });
}
 
var fnGet3 = function (url, successmethod, errormethod) {
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        success: successmethod,
        error: errormethod
    });
}

var fnPost = function (url, params, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
            //console.log("POST SERVICES : " + xhr.responseText);
        } else {
            error(xhr.responseText);
        }
    }
    xhr.send(params);
}   