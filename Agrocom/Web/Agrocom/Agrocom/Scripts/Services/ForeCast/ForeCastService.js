﻿var fnloadCombosWeek = function (userID, cropID, success) {
    var optID = "cro";
    fnGet2("ListGrowerByUserCrop?opt=" + optID + "&id=" + userID + "&cropID=" + cropID, success);
}

var fnloadForecastUnified = function (growerID, farmID, cropID, originID, success) {
    fnGet2("ListForecastUnified?growerID=" + growerID + "&farmID=" + farmID + "&cropID=" + cropID + "&originID=" + originID, success);
}


var fnloadNextWeek = function (cropID, campaignID, success) {
    var optID = "nex";
    var filter01 = '';
    fnGet2("ListWeeksMC?opt=" + optID + "&id=" + filter01 + "&cropID=" + cropID + "&campaignID=" + campaignID, success);
}

var fnloadCurrentWeek = function (cropID, campaignID, success) {
    var optID = "act";
    var filter01 = '';
    fnGet2("ListWeeksMC?opt=" + optID + "&id=" + filter01 + "&cropID=" + cropID + "&campaignID=" + campaignID, success);
}

var fnTotalForecast = function (projectedWeekID, loteID, success) {
    fnGet2("ListForecastGeneralValuesMC?projectedWeekID=" + projectedWeekID + "&loteID=" + loteID, success);
}

var fnloadStage = function (cropID, growerID, farmID, success) {
    fnGet2("ListStageByCrop?cropID=" + cropID + "&growerID=" + growerID + "&farmID=" + farmID, success);
}

var fnloadFarm = function (growerID, cropID, success) {
    fnGet2("ListFarmByCrop?growerID=" + growerID + "&cropID=" + cropID, success);
}

var fnloadLots = function (stageID, success) {
    fnGet2("ListLotByStage?stageID=" + stageID, success);
}

var fnListWeekByCampaign = function (campaignID, success) {
    fnGet2("ListWeekByCampaign?campaignID=" + campaignID, success);
}

var fnloadSize = function (cropID, growerID, lotID, success) {
    fnGet2("ListSizeByCrop?cropID=" + cropID + "&growerID=" + growerID + "&lotID=" + lotID, success);
}

var fnloadCategory = function (cropID, growerID, success) {
    var optID = "gri";
    fnGet2("ListCategoryByGrowerIDMC?opt=" + optID + "&id=" + cropID + "&growerID=" + growerID, success);
}

var fnloadTitleForm = function (cropID, success) {
    fnGet2("ListTitleFormByCropIDMC?cropID=" + cropID, success);
}

var fnloadGrowerByID = function (growerID, success) {
    var optID = "id";
    var filter01 = '';
    fnGet2("ListGrowerByUserCropMC?opt=" + optID + "&id=" + growerID + "&cropID=" + filter01, success);
}

var fnloadStageByGrower = function (cropID, growerID, farmID, success) {
    fnGet2("ListStageByCrop?cropID=" + cropID + "&growerID=" + growerID + "&farmID=" + farmID, success);
}

var fnloadBrandsByGrower = function (originID, growerID, success) {
    var optID = "gro";
    fnGet2("ListCategoryByGrowerIDMC?opt=" + optID + "&id=" + originID + "&growerID=" + growerID, success);
}

var fnloadBrandsByGrowerCrop = function (cropID, growerID, success) {
    var optID = "cro";
    fnGet2("ListCategoryByGrowerIDMC?opt=" + optID + "&id=" + cropID + "&growerID=" + growerID, success);
}

var fnloadVarietiesByGrower = function (growerID, success) {
    var optID = "gro";
    var filter01 = '';
    fnGet2("ListVarietiesByGrowerMC?opt=" + optID + "&id=" + growerID + "&log=" + filter01, success);
}

var fnloadSizesByGrower = function (cropID, growerID, success) {
    var lotID = 0;
    fnGet2("ListSizeByCrop?cropID=" + cropID + "&growerID=" + growerID + "&lotID=" + lotID, success);
}

var fnListMasiveForecast = function (growerID, cropID ,success) {
    fnGet2("GetMasiveForecastMC?growerID=" + growerID + "&cropID=" + cropID, success);
}

var fnListProjectedProductionByVariety = function (userID, dateIni, dateFin, campaignID, farmID, success) {
    fnGet2("GetProjectedProductionByVarietyMC?userID=" + userID + "&dateIni=" + dateIni + "&dateFin=" + dateFin + "&campaignID=" + campaignID + "&farmID=" + farmID, success);
}

var fnListProjectedProduction = function (projectedWeekID, loteID, success) {
    var optID = "erny";
    fnGet2("ListProjectedProductionMC?login=" + optID + "&projectedWeekID=" + projectedWeekID + "&loteID=" + loteID, success);
}

var fnSaveMasiveUploadForecast = function (params, campaignID, success, error) {
    fnPost2("SaveMasiveUploadForecastMC?campaignID=" + campaignID, params, success, error);
}

var fnSaveUploadForecast = function (params, success, error) {
    fnPost2("SaveUploadForecast", params, success, error);
}

var fnGet = function (url, success) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
        } else {
            //console.log('Error call url: ' + url + ', Response:' + xhr.responseText);
        }
    }
    xhr.send();
}

var fnGet2 = function (url, successmethod) {
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        success: successmethod,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        }
    });
}

var fnPost = function (url, params, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
            //console.log("POST SERVICES : " + xhr.responseText);
        } else {
            error(xhr.responseText);
        }
    }
    xhr.send(params);
}

var fnPost2 = function (url, params, success, error) {
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: url,
        data: params,
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        //async: false,
        error: error,
        success: success
    });
} 
