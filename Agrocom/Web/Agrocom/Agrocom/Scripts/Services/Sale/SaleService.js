﻿var fnloadWeekByCampaign = function (campaignID, success) {
    fnGet2("ListWeekByCampaignMC?campaignID=" + campaignID, success);
}

var fnloadNextWeek = function (weekIni, weekEnd, userID,success) {
    var optID = "nwk";
    var filter01 = '';
    var campaignID = 0;
    fnGet2("GetNextWeekMC?opt=" + optID + "&val=" + filter01 + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID, success);
}

var fnloadActualyWeek = function (weekIni, weekEnd, userID, success) {
    var optID = "act";
    var filter01 = '';
    var campaignID = 0;
    fnGet2("GetNextWeekMC?opt=" + optID + "&val=" + filter01 + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID, success);
}

var fnloadAllWeek = function (weekIni, weekEnd, userID, campaignID, success) {
    var optID = "lwi";
    var filter01 = '';
    fnGet2("GetNextWeekMC?opt=" + optID + "&val=" + filter01 + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID, success);
}

var fnloadDataStatus = function (campaignID, weekIni, weekEnd, userID, success) {
    var optID = "stc";
    var campaignID = 0;
    fnGet2("GetNextWeekMC?opt=" + optID + "&val=" + campaignID + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID, success);
}

var fnloadDataMarket = function (success) {
    var optID = "all";
    var filter01 = "";
    fnGet2("ListMarketMC?opt=" + optID + "&id=" + filter01, success);
}

var fnloadDataCustomerMarket = function (success) {
    var optID = "wmr";
    var filter01 = "";
    fnGet2("ListAllWithMarketMC?opt=" + optID + "&id=" + filter01, success);
}

var fnloadDataDestinationCustomer = function (success) {
    var optID = "wcu";
    var filter01 = "";
    var filter02 = "";
    var filter03 = "";
    fnGet2("/CommercialPlan/ListDestinationAllWithCustomerMC?opt=" + optID + "&id=" + filter01 + "&mar=" + filter02 + "&via=" + filter03, success);
}

var fnloadDataVia = function (success) {
    var optID = "all";
    var filter01 = "";
    fnGet2("ListViaMC?opt=" + optID + "&id=" + filter01, success);
}

var fnloadDataConditionPayment = function (success) {
    var optID = "all";
    var filter01 = "";
    fnGet2("ListConditionPaymentMC?opt=" + optID + "&id=" + filter01, success);
}

var fnloadDataVariety = function (cropID, success) {
    var optID = "cro";
    var filter01 = '';
    fnGet2("/Forecast/ListVarietiesByGrowerMC?opt=" + optID + "&id=" + cropID + "&log=" + filter01, success);
}

var fnloadDataFormat = function (cropID, success) {
    var optID = "fci";
    fnGet2("GetFormatByCropIDMC?opt=" + optID + "&id=" + cropID, success);
}

var fnloadDataTypeBox = function (success) {
    var optID = "pkc";
    var filter01 = '';
    fnGet2("GetPackageProductAllMC?opt=" + optID + "&id=" + filter01, success);
}

var fnloadDataCategory = function (cropID, success) {
    var optID = "cro";
    var filter01 = '';
    fnGet2("CategoryGetByCropMC?opt=" + optID + "&id=" + cropID + "&gro=" + filter01, success);
}

var fnloadDataPresentation = function (cropID, success) {
     var opt ="pci"
    fnGet2("GetPresentByCrop?opt=" + opt + "&id=" + cropID, success);
}

var fnloadDataSize = function (cropID, success) {
    var optID = "cro";
    fnGet2("ListSizeGetByCropMC?opt=" + optID + "&id=" + cropID, success);
}

var fnGetMarketByUserID = function (userID, success) {
    var opt = 'lim';
    var val = '';
    var weekIni = 0;
    var weekEnd = 0;
    var campaignID = 0;
    fnGet2("GetMarketByUserID?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID, success);
}

var fnloadDataMarketUser = function (userID, commentary, success) {
    var optID = "lin";
    var filter01 = '';
    var filter02 = 0;
    var filter03 = 0;
    fnGet2("GetStatusByFilterMC?opt=" + optID + "&val=" + filter01 + "&weekIni=" + filter02 + "&weekEnd=" + filter03 + "&userID=" + userID, success);
}

var fnGetStatusRejectPO = function (saleID, commentary, success) {
    var optID = "rej";
    var filter01 = commentary;
    var filter02 = 0;
    var filter03 = 0;
    var campaignID = 0;
    fnGet2("GetStatusByFilterMC?opt=" + optID + "&val=" + filter01 + "&weekIni=" + filter02 + "&weekEnd=" + filter03 + "&userID=" + saleID + "&campaignID=" + campaignID, success);
}

var fnGetStatusByMod = function (saleID, success) {
    var optID = "upd";
    var filter01 = '';
    var filter02 = 0;
    var filter03 = 0;
    var campaignID = 0;
    fnGet2("GetStatusByFilterMC?opt=" + optID + "&val=" + filter01 + "&weekIni=" + filter02 + "&weekEnd=" + filter03 + "&userID=" + saleID + "&campaignID=" + campaignID, success);
}

var fnSaveSalePC = function (params, success, error, complete) {
    fnPost2("SaveSalePC", params, success, error, complete);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODOS GET Y POST >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
var fnGet = function (url, success) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
        } else {
            console.log('Error call url: ' + url + ', Response:' + xhr.responseText);
        }
    }
    xhr.send();
}

var fnGet2 = function (url, successmethod) {
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        success: successmethod
    });
}

var fnPost2 = function (url, params, successmethod, errormethod, completemethod) {
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: url,
        async: false,
        contentType: "application/json",
        dataType: 'json',
        data: params,
        success: successmethod,
        error: errormethod,
        complete: completemethod
    })
}

var fnPost = function (url, params, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
            //console.log("POST SERVICES : " + xhr.responseText);
        } else {
            error(xhr.responseText);
        }
    }
    xhr.send(params);
}   