﻿var fnListDestinationAll = function (opt, id, mar, via, success) {
    fnGet2("/Wow/ListDestinationAll?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via, success);
}

var fnListMarket = function (opt, id, success) {
    fnGet2("/Wow/ListMarket?opt=" + opt + "&id=" + id, success);
}

var fnListAllWithMarket = function (opt, id, success) {
    fnGet2("/Wow/ListAllWithMarket?opt=" + opt + "&id=" + id, success);
}

var fnListAllConsignee = function (opt, id, success) {
    fnGet2("/Wow/ListAllWithMarket?opt=" + opt + "&id=" + id, success);
}

var fnListAllNotify = function (opt, id, success) {
    fnGet2("/Wow/ListAllWithMarket?opt=" + opt + "&id=" + id, success);
}

var fnGetAllCetrifications = function (opt, id, success) {
    fnGet2("/Wow/GetAllCetrifications?opt=" + opt + "&id=" + id, success);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<METODOS GET Y POST >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
var fnGet = function (url, success) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
        } else {
            console.log('Error call url: ' + url + ', Response:' + xhr.responseText);
        }
    }
    xhr.send();
}

var fnGet2 = function (url, successmethod) {
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        success: successmethod
    });
}

var fnPost = function (url, params, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            success(xhr.responseText);
            //console.log("POST SERVICES : " + xhr.responseText);
        } else {
            error(xhr.responseText);
        }
    }
    xhr.send(params);
}   