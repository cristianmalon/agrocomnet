﻿var PO = []

function generateFilePdfIE(PorderProductionID) {
    var docPDF = generatePdfIE(PorderProductionID);
    docPDF.save(PO.orderProduction + 'IE'+ '.pdf');
}

function generatePdfIE(PorderProductionID) {
    var doc = new jsPDF()

    //AJAX
    {
        PO.orderProduction = "AGA-0001/2020"
    }

    doc.line(5, 5, 205, 5) // horizontal 
    doc.line(5, 5, 5, 290) // Vert 
    doc.line(5, 290, 205, 290) // horizontal 
    doc.line(205, 5, 205, 290) // Vert 

    doc.text(20, 275, 'Approved by')
    doc.text(20, 281, 'Date')
    doc.text(20, 287, 'State')

    return doc;
}