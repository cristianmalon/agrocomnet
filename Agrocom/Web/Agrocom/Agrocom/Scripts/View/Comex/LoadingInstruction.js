﻿var sessionUserID = $('#lblSessionUserID').text();
var ActualyWeek = [];
var dataCharge = [];
var dataOperator = [];
var dataTerminal = [];
var dataShipping = [];
var dataInvoice = [];
var dataConsignee = [];
var dataNotify = [];
var dataShippingP = [];
var dataDestination = [];
var dataTariff = [];
var dataPlant = [];
var dataShipper = [];
var dataPackingList = [];
var table;
var dataVariety = [];
var dataCategory = [];
var dataNetWeight = [];
var dataTypeBox = [];
var dataPresentation = [];
var dataBrand = [];
var dataLabel = [];
var dataCodePack2 = [];
var trEdit = "";
var dataAllWeek = [];
var dataParameter = [];
var dataStatus = [];
var dataSizes = [];
var dataVia = [];
var dataBL = [];
var _dataCategory = [];
var _dataVariety = [];
var _dataCodepack = [];
var _dataTypeBox = [];
var _dataPresentation = [];
var _dataSize = [];
var _campaignID = localStorage.campaignID;
var _cropID = localStorage.cropID;
var _viaID = 0;
var _marketID = '';
var _initial = '';
var _final = '';

$(function () {
	$('#divList thead tr').clone(true).appendTo('#divList thead');
	$('#divList thead tr:eq(1) th').each(function (i) {
		if (i > 2) {
			var title = $(this).text();
			$(this).html('<input type="text" class="form-control form-control-sm" style="font-size:12px!important" placeholder="Search ' + title + '" />');
		} else {
			$(this).html(' ');
		}
		$('input', this).on('keyup change', function () {
			if (table.column(i).search() !== this.value) {
				table
					.column(i)
					.search(this.value)
					.draw();
			}
		});
	});
	var opt = "act";
	var val = "";
	var weekIni = 0;
	var weekEnd = 0;
	var userID = 0;
	var campaignID = 0;
	$.ajax({
		type: "GET",
		url: "/PO/GetCurrentWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				ActualyWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description
						}
					}
				);
			}
		}
	});
	$.ajax({
		type: "GET",
		url: "/forecast/ListWeekByCampaign?campaignID=" + _campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataAllWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingStatus?opt=" + "sta" + "&orderPoductionID=" + _campaignID + "&cropID=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataStatus = dataJson.map(
					obj => {
						return {
							"id": obj.statusID,
							"name": obj.status
						}
					}
				);
			}
		}
	});

	loadComboFilter(dataAllWeek, 'selectWeek', false, '[ALL]');
	$('#selectWeek').val(ActualyWeek[0].id).selectpicker('refresh');

	loadComboFilter(dataStatus, 'selectStatus', true, '[ALL]');
	$('#selectStatus').selectpicker('refresh');

	$(btnSearch).click(function () {
		sweet_alert_progressbar();
		showDataSearched();
	})

	//$(btnSync).click(function () {
	//	SyncInfoToNetsuite();
	//})

	loadData();
	$('#modalShipperInvoiceConsigneeNotify').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var recipient = button.data('whatever') // Extract info from data-* attributes
		if (recipient == 'Invoice') {
			var customerID = $("#selectInvoice option:selected").val();
			var customer = $("#selectInvoice option:selected").text();
			customerList(customerID);
		}
		else {
			if (recipient == 'Consignee') {
				var customerID = $("#selectConsignee option:selected").val();
				var customer = $("#selectConsignee option:selected").text();
				customerList(customerID);
			}
			else {
				if (recipient == 'Notify') {

					var customerID = $("#selectNotify option:selected").val();
					var customer = $("#selectNotify option:selected").text();
					customerList(customerID);

				} else {
					if (recipient == 'Shipper') {
						var customerID = $("#selectShipper option:selected").val();
						var customer = $("#selectShipper option:selected").text();
						loadShipper(_cropID);
					}
				}
			}
		}

		var modal = $(this)
		modal.find('.modal-title').text('Details ' + recipient)
		modal.find('.modal-body input').val(recipient)
	})

	loadDefaultValues();
	loadDefaultValuesCodePack();
	$(document).ready(function () {

		$('#dateETD2').datetimepicker({
			format: 'd-m-Y',//'d-m-Y H:i:s',
			timepicker: false,
			//inline: true        
			closeOnDateSelect: true,
			scrollInput: false
		});

		$('#dateETA2').datetimepicker({
			format: 'd-m-Y',
			timepicker: false,
			//inline: true        
			closeOnDateSelect: true,
			scrollInput: false
		});

		$('#dateArrivel2').datetimepicker({
			format: 'd-m-Y',
			timepicker: false,
			//inline: true        
			closeOnDateSelect: true,
			scrollInput: false
		});

		$('#dateDeparture2').datetimepicker({
			format: 'd-m-Y',
			timepicker: false,
			//inline: true        
			closeOnDateSelect: true,
			scrollInput: false
		});

		$('#arrivalTime2').datetimepicker({
			format: 'H:i',
			datepicker: false,
			//inline: true        
			closeOnDateSelect: true,
			scrollInput: false
		});

		$('#dateTerminal2').datetimepicker({
			format: 'd-m-Y',
			timepicker: false,
			//inline: true        
			closeOnDateSelect: true,
		});

	});	

	$('#selectCategory').change(function () {

		loadCombo([], 'selectBrand', true);

		loadCombo([], 'selectLabel', true);

		if (this.value == "") {
			return;
		}

		if ($("#selectCategory option:selected").val().trim().length > 0) {

			loadBrandLabel($("#selectCategory option:selected").val().trim())
		}

		if (dataBrand.length > 0) {

			loadCombo(dataBrand, 'selectBrand', false);
			loadCombo(dataLabel, 'selectLabel', false);
			$('#selectBrand').selectpicker('refresh');
			$('#selectLabel').selectpicker('refresh');

		} else {
			loadCombo([], 'selectBrand', true);
			loadCombo([], 'selectLabel', true);
		}
	})

	$('#selectNetWeight').change(function () {
		loadCombo([], 'SelectSizeBox', true);

		if (this.value == "") {
			return;
		}
		if ($("#selectNetWeight option:selected").val().trim().length > 0) {

			if ($("#selectTypeBox option:selected").val().trim().length > 0) {
				var netWeight = ($("#selectNetWeight option:selected").val().trim());
				var typeBox = ($("#selectTypeBox option:selected").val().trim());

				loadCodePack(netWeight, typeBox);
			}
		}

		if (dataCodePack2.length > 0) {
			loadCombo(dataCodePack2, 'SelectSizeBox', true);
			$('#SelectSizeBox').selectpicker('refresh');
			$('#SelectSizeBox').change();
			$('#txtBoxes').val(0)
			$('#txtNet').val(0)
			$(txtPallets).val(0)

		} else {
			loadComboSizeBox([], 'SelectSizeBox', true, "");
			$('#SelectSizeBox').selectpicker('refresh');
			$('#txtBoxPallet').val(0)
			$('#txtBoxes').val(0)
			$('#txtNet').val(0)
			$(txtPallets).val(0)

		}
		calculatePallets();

	})

	$('#selectTypeBox').change(function () {

		loadCombo([], 'SelectSizeBox', true);

		if (this.value == "") {
			return;
		}
		if ($("#selectTypeBox option:selected").val().trim().length > 0) {

			if ($("#selectNetWeight option:selected").val().trim().length > 0) {
				var netWeight = ($("#selectNetWeight option:selected").val().trim());
				var typeBox = ($("#selectTypeBox option:selected").val().trim());

				loadCodePack(netWeight, typeBox);
			}
		}

		if (dataCodePack2.length > 0) {

			createCodePack();
			loadCombo(dataCodePack2, 'SelectSizeBox', false);
			$('#SelectSizeBox').selectpicker('refresh');
			$('#SelectSizeBox').change();
			$('#txtBoxes').val(0)
			$('#txtNet').val(0)
			$(txtPallets).val(0)
		} else {
			loadComboSizeBox([], 'SelectSizeBox', false, "")
			$('#SelectSizeBox').selectpicker('refresh');
			$('#txtBoxPallet').val(0)
			$('#txtNet').val(0)
			$('#txtNet').val(0)
			$(txtPallets).val(0)
		}
	})

	$('#SelectSizeBox').change(function () {
		$('#txtBoxPallet').val("");
		if (this.value == "") {
			return;
		}
		if ($("#SelectSizeBox option:selected").val().trim().length > 0) {
			$('#txtBoxPallet').val(dataCodePack2[0].boxesPerPallet);
			var boxPallet = $('#txtBoxPallet').val();
			$(txtBoxPallet).val(boxPallet);

			//switch ($('.chkpadlock').prop('checked').toLowerCase()) {
			//	case 'false':
			//		$(txtBoxes).attr("step", boxPallet);
			//		break;
			//	default:
			//}

			//$(txtBoxes).attr("step", boxPallet);
			$(txtBoxes).val(boxPallet)
			var pallet = ($(txtBoxes) / $(txtBoxPallet));
			$(txtPallets).val(pallet);
		}
		else {
			$('#txtBoxPallet').val(0)
			$(txtPallets).val(0);
		}
	})

	$('#txtBoxes').change(function () {
		var currentBoxes = $(this).val();
		var boxPallet = parseInt($('#txtBoxPallet').val());
		var pallets = parseInt(currentBoxes) / boxPallet;

		if ($('#chkpadlock').is(":checked")) {
			
			pallets = pallets + 0.2;
		} else {
			if (pallets % 1 == 0) {
				pallets = pallets;
			} else {
				pallets = pallets;
				$(txtBoxes).val(0);
			}
		}

		$('#txtPallets').val(pallets.toFixed(2))
		var currentPrice = parseFloat($('#txtPrice').val());
		var totalPrice = (currentPrice * currentBoxes).toFixed(2);
		$('#txtTotalPrice').val(totalPrice);

		//obteniendo el peso segun el formato seleccionado
		var netWeightSelected = dataNetWeight.filter(item => item.id == $(selectNetWeight).val())
		//si encuentra un formato:
		if (netWeightSelected.length > 0) {
			//peso dle formato
			var weightPerBox = parseFloat(netWeightSelected[0].weight)
			var boxes = parseInt($(txtBoxes).val())
			var totalNet = (weightPerBox * boxes).toFixed(2);
			$(txtNet).val(totalNet)
		}
	})

	$('#txtPrice').change(function () {
		$('#txtBoxes').change()
	})

	$('#txtPrice').val('0')
	$('#txtBoxes').val('0')
	$('#txtNet').val('0')
	$('#txtPaymentTerm').val('')
	$('#txtBoxPallet').val('0')
	$('#txtPallets').val('0')

	$(selectVariety).change(function () {
		createCodePack();
	})

	$(selectCategory).change(function () {
		createCodePack();
	})

	$(selectNetWeight).change(function () {
		createCodePack();
	})

	$(selectTypeBox).change(function () {
		createCodePack();
	})

	$(selectBrand).change(function () {
		createCodePack();
	})

	$(selectPresentation).change(function () {
		createCodePack();
	})

	$(selectLabel).change(function () {
		createCodePack();
	})

	$('#chkplu').change(function () {
		createCodePack();
	}) 

	$('#addCodePack').click(function (e) {
		addtblCodePack();
	});

	$('#btnAddBluCodePack').click(function (e) {
		addtblCodePack();
	});

	$('#btnUpdateBluCodepack').click(function (e) {
		//updatetblCodePack();
		addtblCodePack();
	});

	$(document).on("click", ".row-remove", function () {
		$(this).parents('tr').detach();
		calculateTotal();
	});

	showDataSearched();

	$('#btnSave').click(function (e) {
		SaveLoadingInstruction();
	});

	$(coldTret).click(function () {
		showParameter();
	})

	$(document).on("change", "#_selectCategory", function () {
		calculeCodepack();
	});

	$(document).on("change", "#_selectVariety", function () {
		calculeCodepack();
	});

	$(document).on("change", "#_selectCodePack", function () {
		GetFillSelectPresentation($('#_selectCodePack option:selected').val());
		CalculateBoxPerPalletAndContainer();
		calculeCodepack();
	});

	$(document).on("change", "#_selectTypeBox", function () {
		calculeCodepack();
	});

	$(document).on("change", "#_selectPresentation", function () {
		calculeCodepack();
	});

	$(document).on("change", "#_selectSize", function () {
		calculeCodepack();
	});

	$('#selectVia').change(function () {
		switch (_cropID.toLowerCase()) {
			case 'blu':
				break;
			default:
				//let _dataOperator = [];
				//let _dataTerminal = [];
				//let _dataShipping = [];

				//_dataOperator = dataOperator.filter(x => x.viaID == $(this).val()).length > 0 ?
				//	dataOperator.filter(x => x.viaID == $(this).val()) : dataOperator.filter(x => x.viaID != $(this).val());
				//_dataTerminal = dataTerminal.filter(x => x.viaID == $(this).val()).length > 0 ?
				//	dataTerminal.filter(x => x.viaID == $(this).val()) : dataTerminal.filter(x => x.viaID != $(this).val());
				//_dataShipping = dataShipping.filter(x => x.viaID == $(this).val()).length > 0 ?
				//	dataShipping.filter(x => x.viaID == $(this).val()) : dataShipping.filter(x => x.viaID != $(this).val());

				//loadCombo(_dataOperator, 'selectOperator', false);
				//$('#selectOperator').selectpicker('refresh');

				//loadCombo(_dataTerminal, 'selectEntrance', false);
				//$('#selectEntrance').selectpicker('refresh');

				//loadCombo(_dataShipping, 'selectShippingLine', false);
				//$('#selectShippingLine').selectpicker('refresh');
				//$(txtVessel).val('');
				break;
		}
	});

	if (_cropID == 'UVA') {
		$("#chkpadlock").attr("disabled", "disabled");
		$("#chkplu").attr("disabled", "disabled");
	} else {
		$("#chkpadlock").removeAttr("disabled");
		$("#chkplu").removeAttr("disabled");
	}

});

function showDataSearched() {
	var statusID = $("#selectStatus").val();
	if (statusID == 'All') {
		statusID = '';
	}

	var weekSelected = $("#selectWeek").val();
	var color = '';
	var api = "/Comex/GetSalesOrdersRequestLoading?campaignID=" + _campaignID;

	$.ajax({
		type: "GET",
		url: api,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
				return;
			}
			var dataList = JSON.parse(data);
			if (dataList == null || dataList.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
				return;
			}
			var content = "";
			var _dataList = [];
			if (statusID != '') _dataList = dataList.filter(item => item.statusID == statusID && item.projectedweekID == parseInt(weekSelected));
			else _dataList = dataList.filter(item => item.projectedweekID == parseInt(weekSelected));
			if (_dataList.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
			}
			let bExistPdf = false;
			$.each(_dataList, function (index, row) {
				if (row.statusConfirmID == 5 && row.statusID == 1) {
					content += '<tr  class="table-warning">';
					color = 'badge-warning';
					bExistPdf = false;
				} else if (row.statusConfirmID == 5 && row.statusID == 2) {
					content += '<tr  class="table-success">';
					color = 'badge-success';
					bExistPdf = true;
				}
				content += "<td style='text-align:center;  min-width: 65px !important;'>";
				content += "<button class='btn btn-sm' style='background-color:none; border:none; padding: 0px 0px 0px 5px;'  onclick='openModalLoadingInstruction(" + row.orderProductionID + ")' data-toggle='modal' data-target='#modalCreateUpdateIE'><span class='icon text-primary'><i class='fas fa-edit fa-sm'></span></i></button> ";
				content += "</td>";
				content += "<td style='text-align:center;  min-width: 65px !important;'>";
				if (bExistPdf) content += "<button class='btn btn-sm' style='background-color:none; border:none; padding: 0px 0px 0px 0px;'  onclick='generatePdf(" + row.shippingLoadingID + ")'><span class='icon text-danger'><i class='fas fa-file-pdf'></i></span></button>";
				content += "</td>";
				content += "<td style='text-align:center;  min-width: 65px !important;'>";
				if (bExistPdf) content += "<button class='btn btn-sm' style='background-color:none; border:none; padding: 0px 0px 0px 0px;'  onclick='generatePdfComex(" + row.shippingLoadingID + ")'><span class='icon text-danger'><i class='fas fa-file-pdf'></i></span></button>";
				content += "</td>";
				content += "<td style='text-align:center;display:none;' class='orderProductionID'>" + row.orderProductionID + " </td>";
				content += "<td style='text-align:center;' >" + row.orderProduction + " </td>";
				content += "<td style='text-align:center;' >" + row.nroPackingList + " </td>";
				content += "<td style='font-size:14px;text-align:center;' ><div class='badge " + color + " badge - pill'>" + row.status + "</td>";
				content += "<td style='text-align:center;' >" + row.weekDeparture + " </td>";
				content += "<td style='text-align:center;' ><textarea id='customer' class='form-control form-control-sm' rows='1' readonly>" + row.customer + "</textarea></td>";
				content += "<td style='text-align:center;' ><textarea id='consignee' class='form-control form-control-sm' rows='1' readonly>" + row.consignee + "</textarea></td>";
				content += "<td style='text-align:center;display:none;' >" + row.notify + " </td>";
				content += "<td style='text-align:center;' >" + row.destination + " </td>";
				content += "<td style='text-align:center;' >" + row.boxes + " </td>";
				content += "</tr>";
			});
			$("table#divList").DataTable().clear();
			$("table#divList").DataTable().destroy();
			$("table#divList tbody").empty().append(content);
			table = $('table#divList').DataTable({
				"paging": true,
				"ordering": false,
				"info": true,
				"responsive": true,
				//"destroy": true,
				dom: 'Bfrtip',
				//select: true,
				lengthMenu: [
					[10, 25, 50, -1],
					['10 rows', '25 rows', '50 rows', 'All']
				],
				buttons: [
					{ extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
				      exportOptions: {
					  columns: [4, 5, 6, 7, 8, 9, 10, 11, 12]
						}
					},
					'pageLength'
				],
			});

		}

	});
	sweet_alert_progressbar_cerrar();
}

table = $('#divList').DataTable({
	orderCellsTop: true,
	fixedHeader: true,
	ordering: false
});

function cleanControls() {

	$('#dateETD2').datetimepicker({
		format: 'd-m-Y',
		timepicker: false,
		//inline: true        
		closeOnDateSelect: true,
		scrollInput: false,
	});

	$('#dateETA2').datetimepicker({
		format: 'd-m-Y',
		timepicker: false,
		//inline: true        
		closeOnDateSelect: true,
		scrollInput: false
	});

	$('#dateArrivel2').datetimepicker({
		format: 'd-m-Y',
		timepicker: false,
		//inline: true        
		closeOnDateSelect: true,
		scrollInput: false
	});

	$('#dateDeparture2').datetimepicker({
		format: 'd-m-Y',
		timepicker: false,
		//inline: true        
		closeOnDateSelect: true,
		scrollInput: false
	});

	$('#arrivalTime2').datetimepicker({
		format: 'H:i',
		datepicker: false,
		//inline: true        
		closeOnDateSelect: true,
		scrollInput: false
	});

	$('#dateTerminal2').datetimepicker({
		format: 'd-m-Y',
		timepicker: false,
		//inline: true        
		closeOnDateSelect: true,
		scrollInput: false
	});

	$("#txtShippingLoading").val("");
	$("#txtOrderProduction").val("");
	$("#selectCharge").val("").selectpicker('refresh');
	$("#selectOperator").val("").selectpicker('refresh');
	$("#selectPackingList").val("").selectpicker('refresh');
	$("#txtNPackingList").val("");

	$("#txtContact").val("");
	$("#selectEntrance").val("").selectpicker('refresh');
	$("#selectShippingLine").val("").selectpicker('refresh');
	$("#txtReference").val("")
	$("#txtBooking").val("")

	$("#txtTravelingS").val("")
	$("#selectFreight").val("").selectpicker('refresh');
	$("#selectRegimen").val("DRAWBACK").selectpicker('refresh');
	$("#selectTariff").val("").selectpicker('refresh');

	$("#selectPort").val("").selectpicker('refresh');
	$("#selectDestinationPort").val("").selectpicker('refresh');
	$("#dateETD").val("");

	$("#selectShipper").val("").selectpicker('refresh');
	$("#selectInvoice").val("").selectpicker('refresh');
	$("#selectConsignee").val("").selectpicker('refresh');
	$("#selectNotify").val("").selectpicker('refresh');

	$("#selectPlant").val("").selectpicker('refresh');
	$("#txtVGM").val("");
	$(coldTret).prop('checked', false);
	$(controlAtmosphere).prop('checked', false);

	$("#txtTemperature").val("");
	$("#txtVentilation").val("");
	$("#txtHumedity").val("");
	$("#txtQuest").val("");

	$("#noteShipping").val("");

	$('table#tblCatVar>tbody').empty();
	$('table#tblCodePack>tbody').empty();

	$('#btnEdit').addClass('display-none')
	$('#btnAdd').removeClass('display-none')

}

function openModalLoadingInstruction(orderProductionID) {
	//$(modalCreateUpdateIE).draggable();
	cleanControls();
	$.ajax({
		type: "GET",
		url: "/Comex/GetDetailRequestLoading?orderPoductionID=" + orderProductionID + "&cropID=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length == 0) {
				$(modalCreateUpdateIE).modal('hide');
			}
			else {
				var dataJson = JSON.parse(data);
				
				$('#txtShippingLoading').val(dataJson.shippingLoadingID).change();
				$('#txtOrderProduction').val(dataJson.orderProductionID).change();
				$("#selectCharge").val(dataJson.managerID).selectpicker('refresh').change();
				$("#selectOperator").val(dataJson.logisticOperatorID).change();
				$("#selectPackingList").val(dataJson.nroPackingListID).selectpicker('refresh').change()
				$("#txtNPackingList").val(dataJson.nroPackingList).change();
				$('#createDate').val(dataJson.dateCreated);
				$("#txtContact").val(dataJson.contact).change();
				$("#selectEntrance").val(dataJson.terminalID).selectpicker('refresh').change();
				$("#selectShippingLine").val(dataJson.shippingCompanyID).change();
				$("#txtReference").val(dataJson.orderProduction).change();
				$("#txtBooking").val(dataJson.booking).change();
				$("#txtVessel").val(dataJson.veseel).change();
				$('#selectFreight').val(dataJson.freightCondition).change();
				$("#selectRegimen").val("DRAWBACK").change();	//Se deja en duro la información ya que es el único valor que se presenta en el combo
				var _Tariff = [];
				_Tariff = dataJson.tariffHeadingID;
				$(selectTariff).selectpicker('val', _Tariff.split(','))
				//$("#selectTariff").val(dataJson.tariffHeadingID).change();								
				$("#selectBL").val(dataJson.shippingLoadingBLId).change();
				$("#selectPort").val(dataJson.originDestinationID).change()
				$("#selectDestinationPort").val(dataJson.arriveDestinationID).change()
				if (dataJson.etd != null)
					if (dataJson.etd.length > 0) {
						//var FechaHoraETD = (dataJson.etd);
						//var FechaETD = FechaHoraETD.split("-");
						//var FechaETDForm = FechaETD[2] + "-" + FechaETD[1] + "-" + FechaETD[0];
						//var dateETD = FechaETDForm;
						//$("#dateETD2").val(dateETD).change();
						$('#dateETD2').val(dataJson.etd).change();
					}

				if (dataJson.etd != null)
					if (dataJson.eta.length > 0) {
						//var FechaHoraETA = (dataJson.eta);
						//var FechaETA = FechaHoraETA.split("-");
						//var FechaETAForm = FechaETA[2] + "-" + FechaETA[1] + "-" + FechaETA[0];
						//var dateETA = FechaETAForm;
						//$("#dateETA2").val(dateETA).change();
						$('#dateETA2').val(dataJson.eta).change();
					}

				$(checkFitosanitario).prop('checked', (dataJson.certificates == 1) ? true : false);;
				$("#selectShipper").val(dataJson.shipperID).change();
				$("#selectInvoice").val(dataJson.customerID).change();
				$("#selectConsignee").val(dataJson.consigneeID).change();
				$("#selectNotify").val(dataJson.notifyID).change();

				$("#selectPlant").val(dataJson.processPlantID).change();
				$("#txtVGM").val(dataJson.vgm).change();
				$(coldTret).prop('checked', (dataJson.coldTreatment == 1) ? true : false);
				$(controlAtmosphere).prop('checked', (dataJson.controlledAtmosphere == 1) ? true : false);
				$("#txtTemperature").val(dataJson.temperature).val();
				$("#txtVentilation").val(dataJson.ventilation).val();
				$(checkHumedity).prop('checked', (dataJson.humedity == 1) ? true : false);
				$(checkQuest).prop('checked', (dataJson.quest == 1) ? true : false);

				//3.DATE TO PLANT
				if (dataJson.datetoPlantEntry != null)
					if (dataJson.datetoPlantEntry.length > 0) {
						//var FechaHoraArrivel = (dataJson.datetoPlantEntry);
						//var FechaArrivel = FechaHoraArrivel.split("-");
						//var FechaArrivelForm = FechaArrivel[2] + "-" + FechaArrivel[1] + "-" + FechaArrivel[0];
						//var dateArrivel = FechaArrivelForm;
						//$("#dateArrivel2").val(dateArrivel).change();
						$('#dateArrivel2').val(dataJson.datetoPlantEntry).change();
					}

				//4.DEPARTURE DATE
				if (dataJson.departureDate != null)
					if (dataJson.departureDate.length > 0) {
						//var FechaHoraDeparture = (dataJson.departureDate);
						//var FechaDeparture = FechaHoraDeparture.split("-");
						//var FechaDepartureForm = FechaDeparture[2] + "-" + FechaDeparture[1] + "-" + FechaDeparture[0];
						//var dateDeparture = FechaDepartureForm;
						//$("#dateDeparture2").val(dateDeparture).change();
						$('#dateDeparture2').val(dataJson.departureDate).change();
					} else {
						//$("#dateDeparture").val(fi);
					}

				//5.ENTRY DATE
				if (dataJson.terminalEntryDate != null)
					if (dataJson.terminalEntryDate.length > 0) {
						//var FechaHoradateTerminal = (dataJson.terminalEntryDate);
						//var FechadateTerminal = FechaHoradateTerminal.split("-");
						//var FechadateTerminalForm = FechadateTerminal[2] + "-" + FechadateTerminal[1] + "-" + FechadateTerminal[0];
						//var datedateTerminal = FechadateTerminalForm;
						//$("#dateTerminal2").val(datedateTerminal).change();
						$('#dateTerminal2').val(dataJson.terminalEntryDate).change();
					}
				if (dataJson.arrivalTime != null)
					if (dataJson.arrivalTime.length > 0) {
						$("#arrivalTime2").val(dataJson.arrivalTime);//**Buscarr TIMEPICKER
					}

				$("#noteShipping").val(dataJson.comments).change();

				_initial = dataJson.initial;
				_final = dataJson.final;

				var content = '';
				$.each(dataJson.details, function (i, e) {
					content = '<tr>';
					content += '<td style="max-width:50px; min-width:50px;">';
					content += '<button class="btn btn-sm" style="background-color:none; border:none; padding: 0px 5px 8px 10px;" onclick="openModalTabla(this)" >';
					content += '<span class="icon text-primary"><i class="fas fa-edit fa-sm"></i></span>';
					content += '</button>';
					content += '<i style="color: darkred; padding: 0px 0px 0px 0px;" class="fas fa-trash fa-sm row-remove " ></i>';
					content += '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="shippingLoadingDetailID" style="display:none">' + e.shippingLoadingDetailID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="shippingLoadingID" style="display:none">' + e.shippingLoadingID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="categoryID" style="display:none">' + e.categoryID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="category" style="display:none">' + e.category + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="varietyID" style="display:none">' + e.varietyID + '</td>';
					content += '<td style="max-width:150px; min-width:80px;" class="variety">' + e.variety + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="brandID" style="display:none">' + e.brandID + '</td>';
					content += '<td style="max-width:100px; min-width:70px;" class="brand">' + e.brand + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="formatID" style="display:none">' + e.formatID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="format" style="display:none">' + e.format + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="packageProductID" style="display:none">' + e.packageProductID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="packageProduct" style="display:none">' + e.packageProduct + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="presentationID" style="display:none">' + e.presentationID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="presentation" style="display:none">' + e.presentation + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="labelID" style="display:none">' + e.labelID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="label" style="display:none">' + e.label + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="codePackID" style="display:none">' + e.codePackID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="codePack" style="display:none">' + e.codePack + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="sizeID" style="display:none">' + e.sizeID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="size" style="display:none">' + e.size + '</td>';
					content += '<td style="max-width:180px; min-width:155px;" class="codePack2">' + e.codePack2 + '</td>';
					content += '<td style="max-width:60px; min-width:50px;text-align:right;" class="quantity">' + e.quantity + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="statusID" style="display:none">' + e.statusID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="pallets" style="display:none">' + e.pallets + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="marketID" style="display:none">' + e.marketID + '</td>';
					content += '<td style="max-width:200px; min-width:200px;display:none" class="viaID" style="display:none">' + e.viaID + '</td>';
					content += '</tr>';
					$("table#tblCodePack tbody").append(content);
					//table = $('table#tblCodePack').DataTable({
					//	ordering: false,
					//	paging: false,
					//	autoWidth: false,
					//	"fnInitComplete": function () {
					//		$('.dataTables_scrollBody thead tr').css({ visibility: 'collapse' });
					//		$('.dataTables_scrollBody tfoot tr').css({ visibility: 'collapse' });
					//	},
					//	"scrollY": "50px",
					//	"scrollX": true,
					//	scrollCollapse: true,
					//	"dom": 'frtp',
					//	"responsive": true,
					//	searching: false,
					//});

				});
				let objTr = $('#tblCodePack>tbody>tr:first');
				_viaID = objTr.find("td.viaID").text();
				_marketID = objTr.find("td.marketID").text();

				loadTitle(_viaID);

				switch (_cropID.toLowerCase()) {
					case 'blu':
						$('#selectVia').prop("disabled", true);
						$('#selectVia').val(_viaID).selectpicker('refresh').change();
						break;
					default:
						$('#selectVia').prop("disabled", true);
						$('#selectVia').val(_viaID).selectpicker('refresh').change();
						break;
				}

			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			sweet_alert_error("Error", "Data was not saved");

		},
	});
}

function customerList(customerID) {
	sweet_alert_progressbar();
	$.ajax({
		type: "GET",
		url: "/Comex/CustomerGetById?opt=" + "id" + "&id=" + customerID,
		async: true,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				$('#txtCustomer').val(dataJson[0].name)
				$('#txtAddress').val(dataJson[0].address)
				$('#txtContact1').val(dataJson[0].contact)
				$('#txtPhone').val(dataJson[0].phone)
				$('#txtEmail').val(dataJson[0].email)

			}
		}
	});
	sweet_alert_progressbar_cerrar();
}

function openModalTabla(xthis) {

	switch (_cropID.toLowerCase()) {
		case 'blu':
			$(modalBluCodepack).modal('show');
			//$('table#tblCatVar>tbody').empty();
			//$(txtShippingLoadingDetailID).val(0)
			if (xthis == 0) $(btnAddBluCodePack).show(); else $(btnAddBluCodePack).hide();
			if (xthis == 0) $(btnUpdateBluCodepack).hide(); else $(btnUpdateBluCodepack).show();
			GetFillSelectBrand();
			GetFillSelectVariety();
			GetFillSelectFormat(_viaID, _marketID);
			GetFillSelectTypeBox();
			GetFillSelectPresentation();
			GetFillSelectSize();
			$('#_selectVariety').multiselect('deselectAll', false);
			$('#_selectVariety').multiselect('refresh');
			if (xthis == 0) {
				$(_txtBoxPerPallet).val(0);
				$(_txtBoxPerPerContainer).val(0);
				trEdit = ''
			}
			else {
				trEdit = $(xthis).closest('tr');
				$(_selectCategory).val($(xthis).closest('tr').find("td.categoryID").text()).selectpicker('refresh');
				$(_selectVariety).multiselect('select', $(xthis).closest('tr').find("td.varietyID").text().split(','));
				$(_selectCodePack).val($(xthis).closest('tr').find("td.codePackID").text()).selectpicker('refresh').change();
				$(_selectTypeBox).val($(xthis).closest('tr').find("td.packageProductID").text()).selectpicker('refresh');
				$(_selectPresentation).val($(xthis).closest('tr').find("td.presentationID").text()).selectpicker('refresh').change();
				$(_selectSize).val($(xthis).closest('tr').find("td.sizeID").text()).selectpicker('refresh');
				$(_txtCodePack).val($(xthis).closest('tr').find("td.codePack2").text());
				$(_txtBoxPerPerContainer).val($(xthis).closest('tr').find("td.quantity").text());
				CalculateBoxPerPalletAndContainer();
				//$(_txtBoxPerPallet).val();
			}
			break;
		default:
			$(modalCodepack).modal('show');
			$('table#tblCatVar>tbody').empty();
			$(txtShippingLoadingDetailID).val(0)
			$('#btnEdit').addClass('display-none')
			$('#btnAdd').removeClass('display-none')
			$('#chkpadlock').prop('checked', false);
			if (xthis == 0) {
				loadDefaultValuesCodePack();
				trEdit = ''
			}
			else {
				trEdit = $(xthis).closest('tr');				
				$(selectVariety).val($(xthis).closest('tr').find("td.varietyID").text()).selectpicker('refresh')
				$(selectCategory).val($(xthis).closest('tr').find("td.categoryID").text()).selectpicker('refresh').change();
				$(selectNetWeight).val($(xthis).closest('tr').find("td.formatID").text()).selectpicker('refresh').change();
				$(selectTypeBox).val($(xthis).closest('tr').find("td.packageProductID").text()).selectpicker('refresh');
				$(selectBrand).val($(xthis).closest('tr').find("td.brandID").text()).selectpicker('refresh');
				$(selectPresentation).val($(xthis).closest('tr').find("td.presentationID").text()).selectpicker('refresh');
				$(selectLabel).val($(xthis).closest('tr').find("td.labelID").text()).selectpicker('refresh');
				$(SelectSizeBox).val($(xthis).closest('tr').find("td.codePackID").text()).selectpicker('refresh').change();
				$(txtShippingLoadingDetailID).val($(xthis).closest('tr').find("td.shippingLoadingDetailID").text());
				$(txtShippingLoadingID).val($(xthis).closest('tr').find("td.shippingLoadingID").text());
				$(txtBoxes).val($(xthis).closest('tr').find("td.quantity").text());
				$(txtCodePack).val($(xthis).closest('tr').find("td.codePack2").text());
				$(txtStatus).val($(xthis).closest('tr').find("td.statusID").text());
			}
	}
}

//>>>Crop:Blu Información
function GetFillSelectBrand() {

	var opt = "cro";
	var gro = "";
	$.ajax({
		type: "GET",
		url: "/Sales/CategoryGetByCrop?opt=" + opt + "&id=" + _cropID + "&gro=" + gro,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		success: function (data) {
			if (data.length > 0) {

				var dataJson = JSON.parse(data);
				_dataCategory = dataJson.map(
					obj => {
						return {
							"categoryID": obj.categoryID,
							"description": obj.description
						}
					}
				);

			}
		}
	});

	$(selectVariety).empty();

	let data = _dataCategory.map(
		obj => {
			return {
				"id": obj.categoryID,
				"name": obj.description
			}
		}
	);
	loadCombo(data, '_selectCategory', true);
	$('#_selectCategory').selectpicker('refresh');
}

function GetFillSelectVariety() {

	var opt = 'cro';
	var log = '';
	var sUrlApi = "/CommercialPlan/VarietyByCrop?opt=" + opt + "&id=" + _cropID + "&log=" + log
	$.ajax({
		type: "GET",
		url: sUrlApi,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
			} else {
				var dataJson = JSON.parse(data);
				if (dataJson == null || dataJson.length == 0) {
					sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
				} else {
					_dataVariety = dataJson.map(
						obj => {
							return {
								abbreviation: obj.abbreviation,
								id: obj.varietyID,
								description: obj.name
							}
						}
					);


				}
			}
		},
		error: function (datoEr) {
			sweet_alert_info('Information', "There was an issue trying to list data.");
		},

	});
	$(_selectVariety).empty();

	let data = _dataVariety.map(
		obj => {
			return {
				"id": obj.id,
				"name": obj.description
			}
		}
	);

	loadCombo(data, '_selectVariety', false);

	SettingCustomizedMultiselect($('#_selectVariety'));
}

function GetFillSelectFormat(_viaID, _marketID) {

	var opt = 'cro';
	var id = _cropID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			_dataCodepack = JSON.parse(data);
		}
	});

	let dataCodepackSelect = _dataCodepack.filter(item => item.viaID == _viaID && item.marketID.trim() == _marketID.trim()).map(
		obj => {
			return {
				"id": obj.codePackID,
				"name": obj.description

			}
		}
	);

	$(_selectCodePack).empty();

	loadCombo(dataCodepackSelect, '_selectCodePack', true);
	$('#_selectCodePack').selectpicker('refresh').change();

}

function GetFillSelectTypeBox() {

	var optID = "pkc";
	var filter01 = '';
	$.ajax({
		type: "GET",
		url: "/Sales/GetPackageProductAllMC?opt=" + optID + "&id=" + filter01,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				_dataTypeBox = dataJson.map(
					obj => {
						return {
							"id": obj.packageProductID,
							"description": obj.packageProduct,
							"abbreviation": obj.abbreviation
						}
					}
				);
			}
		}
	});

	$(_selectTypeBox).empty();

	let data = _dataTypeBox.map(
		obj => {
			return {
				"id": obj.id,
				"name": obj.description
			}
		}
	);
	loadCombo(data, '_selectTypeBox', true);
	$('#_selectTypeBox').selectpicker('refresh');
}

function GetFillSelectPresentation(formatID) {

	var opt = 'pre';
	id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/GetCodepackWithPresentation?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				_dataPresentation = dataJson
			}
		}
	});

	$(_selectPresentation).empty();

	let data = _dataPresentation.filter(item => item.codePackID == formatID).map(
		obj => {
			return {
				"id": obj.presentationID,
				"name": obj.presentation + obj.moreInfo
			}
		});

	loadCombo(data, '_selectPresentation', false);
	$('#_selectPresentation').selectpicker('refresh').change();
}

function GetFillSelectSize() {

	var opt = "cro";
	var id = _cropID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				_dataSize = dataJson.map(
					obj => {
						return {
							"sizeID": obj.sizeID,
							"detail": obj.detail
						}
					}
				);
			}
		}
	});

	$(_selectSize).empty();

	let data = _dataSize.map(
		obj => {
			return {
				"id": obj.sizeID,
				"name": obj.detail
			}
		}
	);
	loadCombo(data, '_selectSize', true);
	$('#_selectSize').selectpicker('refresh');
}

function CalculateBoxPerPalletAndContainer() {

	if ($(_selectCodePack).val() > 0) {
		var boxPallet = _dataCodepack.filter(element => element.codePackID == $(_selectCodePack).val())[0].boxesPerPallet
		var boxContainer = _dataCodepack.filter(element => element.codePackID == $(_selectCodePack).val())[0].boxPerContainer
		$(_txtBoxPerPallet).val(boxPallet);
		//$(_txtBoxPerPerContainer).val(boxContainer);
	}

}

function calculeCodepack() {
	let varietyID = '';
	let arrayVarietyID = $(_selectVariety).val();
	$.each(arrayVarietyID, function (index, value) {
		varietyID += value + ',';
	});
	if (varietyID.length == 0) {
		$(_txtCodePack).val('');
		return false;
	}

	if ($("#_selectCodePack option:selected").val() == 0) {
		return false;
	}

	if ($("#_selectTypeBox option:selected").val() == 0) {
		return false;
	}

	if ($("#_selectCategory option:selected").val() == 0) {
		return false;
	}

	if ($("#_selectSize option:selected").val() == 0) {
		return false;
	}

	let formatID;
	let typeBoxID;
	let brandID;
	let presentationID;
	var varietyCode;
	var formatCode;
	var typeBoxCode;
	var brandCode;
	var presentationCode;
	var sizeCode;
	var codepack = '';
	$.each(arrayVarietyID, function (index, value) {
		varietyID = value;
		formatID = $(_selectCodePack).val();
		typeBoxID = $(_selectTypeBox).val();
		brandID = $(_selectCategory).val();
		sizeID = $(_selectSize).val();
		viaID = _viaID;
		presentationID = $(_selectPresentation).val();

		varietyCode = _dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
		formatCode = _dataCodepack.filter(item => item.codePackID == formatID && item.viaID == viaID)[0].weight;
		typeBoxCode = _dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation;
		brandCode = _dataCategory.filter(item => item.categoryID == brandID)[0].description;
		presentationCode = _dataPresentation.filter(item => item.presentationID == presentationID && item.codePackID == formatID)[0].presentation;
		sizeCode = _dataSize.filter(item => item.sizeID == sizeID)[0].detail;

		codepack = varietyCode.trim() + '_' +
			formatCode +
			typeBoxCode.slice(0, 1) +
			brandCode.slice(0, 1) + '_' +
			presentationCode + '_' +
			sizeCode + '_1'// + ',' + codepack

	});
	$(_txtCodePack).val(codepack);

}
//<<<Crop:Blu Información

function loadShipper(cropID) {

	$.ajax({
		type: "GET",
		url: "/Comex/GetExporterType?cropID=" + cropID,
		async: true,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				$('#txtCustomer').val(dataJson[0].customer)
				$('#txtAddress').val(dataJson[0].address)
				$('#txtContact1').val(dataJson[0].contact)
				$('#txtPhone').val(dataJson[0].phone)
				$('#txtEmail').val(dataJson[0].email)

			}
		}
	});

}

function loadComboSizeBox(data, control, firtElement, textFirstElement) {
	var content = "";
	if (firtElement == true) {
		content += "<option value='0'>" + textFirstElement + "</option>";
	}
	$('#' + control).empty().append(content);
}

function loadDefaultValuesCodePack() {
	loadCombo(dataVariety, 'selectVariety', false);
	$('#selectVariety').prop("disabled", false);
	$('#selectVariety').selectpicker('refresh');

	loadCombo(dataCategory, 'selectCategory', false);
	$('#selectCategory').prop("disabled", false);
	$('#selectCategory').selectpicker('refresh');
	$('#selectCategory').change();

	loadCombo(dataNetWeight, 'selectNetWeight', false);
	$('#selectNetWeight').prop("disabled", false);
	$('#selectNetWeight').selectpicker('refresh');
	$('#selectNetWeight').change();

	loadCombo(dataTypeBox, 'selectTypeBox', false);
	$('#selectTypeBox').prop("disabled", false);
	$('#selectTypeBox').selectpicker('refresh');
	$('#selectTypeBox').change();

	loadCombo(dataPresentation, 'selectPresentation', false);
	$('#selectPresentation').prop("disabled", false);
	$('#selectPresentation').selectpicker('refresh');

}

function loadDefaultValues() {

	loadCombo(dataVia, 'selectVia', true);
	$('#selectVia').selectpicker('refresh');

	loadCombo(dataCharge, 'selectCharge', false);
	$('#selectCharge').prop("disabled", false);
	$('#selectCharge').selectpicker('refresh');

	loadCombo(dataOperator, 'selectOperator', false);
	$('#selectOperator').prop("disabled", false);
	$('#selectOperator').selectpicker('refresh');

	loadCombo(dataPackingList, 'selectPackingList', false);
	$('#selectPackingList').prop("disabled", false);
	$('#selectPackingList').selectpicker('refresh');

	loadCombo(dataTerminal, 'selectEntrance', false);
	$('#selectEntrance').prop("disabled", false);
	$('#selectEntrance').selectpicker('refresh');

	loadCombo(dataShipping, 'selectShippingLine', false);
	$('#selectShippingLine').prop("disabled", false);
	$('#selectShippingLine').selectpicker('refresh');

	loadCombo(dataInvoice, 'selectInvoice', false);
	$('#selectInvoice').prop("disabled", false);
	$('#selectInvoice').selectpicker('refresh');

	loadCombo(dataConsignee, 'selectConsignee', false);
	$('#selectConsignee').prop("disabled", false);
	$('#selectConsignee').selectpicker('refresh');

	loadCombo(dataNotify, 'selectNotify', false);
	$('#selectNotify').prop("disabled", false);
	$('#selectNotify').selectpicker('refresh');

	loadCombo(dataShippingP, 'selectPort', false);
	$('#selectPort').prop("disabled", false);
	$('#selectPort').selectpicker('refresh');

	loadCombo(dataDestination, 'selectDestinationPort', false);
	$('#selectDestinationPort').prop("disabled", false);
	$('#selectDestinationPort').selectpicker('refresh');

	loadCombo(dataTariff, 'selectTariff', false);
	$('#selectTariff').prop("disabled", false);
	$('#selectTariff').selectpicker('refresh');

	loadCombo(dataPlant, 'selectPlant', false);
	$('#selectPlant').prop("disabled", false);
	$('#selectPlant').selectpicker('refresh');

	loadCombo(dataShipper, 'selectShipper', false);
	$('#selectShipper').prop("disabled", false);
	$('#selectShipper').selectpicker('refresh');

	loadCombo(dataBL, 'selectBL', false);
	$('#selectBL').prop("disabled", false);
	$('#selectBL').selectpicker('refresh');

}

function calculatePallets() {

	var boxPallet = $("#txtBoxPallet").val();
	var boxes = $("#txtBoxes").val();

	var pallets = (boxes / boxPallet);

	$("#txtPallets").val(pallets);
}

function showParameter() {
	var destinationID = $("#selectDestinationPort option:selected").val();
	var coldTreatment = $('#coldTret').prop('checked') ? 1 : 0;

	loadParameter(_cropID, destinationID, coldTreatment);
}

function loadBrandLabel(categoryID) {
	$.ajax({
		type: "GET",
		url: "/Comex/BrandGetByCateID?opt=" + "cat" + "&cropID=" + categoryID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataBrand = dataJson.map(
					obj => {
						return {
							"id": obj.brandID,
							"name": obj.brand,
							"abbreviation": obj.abbreviation
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetLabelByCropID?opt=" + "lbl" + "&cropID=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataLabel = dataJson.map(
					obj => {
						return {
							"id": obj.labelID,
							"name": obj.label,
							"abbreviation": obj.abbreviation
						}
					}
				);
			}
		}
	});

}

function loadCodePack(netWeight, typeBox) {

	$.ajax({
		type: 'POST',
		url: "/Comex/CodepackByFormatIDPackageProductID?opt=" + "pcr" + "&viaid=" + netWeight + "&cropID=" + typeBox,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataCodePack2 = dataJson.map(
					obj => {
						return {
							"id": obj.codePackID,
							"name": obj.codePack,
							"boxPerContainer": obj.boxPerContainer,
							"boxesPerPallet": obj.boxesPerPallet
						}
					}
				);
			}
		}
	});

	//<<<Obtener los calibres
	var opt = 'cro';
	var id = _cropID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataSizes = dataJson.map(
					obj => {
						return {
							"id": obj.sizeID,
							"name": obj.sizeID
						}
					}
				);
			}
		}
	});

}

function loadParameter(cropID, destinationID, coldTreatment) {

	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingParameter?cropID=" + cropID + "&destinationID=" + destinationID + "&coldTreatment=" + coldTreatment,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				$("#txtTemperature").val(dataJson[0].temperature)
				$("#txtVentilation").val(dataJson[0].ventilation)
				$(checkHumedity).prop('checked', (dataJson[0].humedity == 1) ? true : false);
				$(checkQuest).prop('checked', (dataJson[0].quest == 1) ? true : false);

			}
		}
	});

}

function loadData() {

	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingStatus?opt=" + "man" + "&orderPoductionID=" + 0 + "&cropID=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataCharge = dataJson.map(
					obj => {
						return {
							"id": obj.managerID,
							"name": obj.manager
						}
					}
				);
			}
		}
	});

	//>>>Obtener la Vía
	let opt = 'all';
	let id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/ListVia?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataVia = dataJson.map(
					obj => {
						return {
							"id": obj.viaID,
							"name": obj.name
						}
					}
				);
			}
		}
	});
	//<<<Obtener la Vía

	//>>>Obtener Operador Logístico
	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingStatus?opt=" + "log" + "&orderPoductionID=" + 0 + "&cropID=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataOperator = dataJson.map(
					obj => {
						return {
							"id": obj.logisticOperatorID,
							"name": obj.logisticOperator,
							"viaID": obj.viaID
						}
					}
				);
			}
		}
	});
	//<<<Obtener Operador Logístico

	//>>>Obtener Almacén de Ingreso
	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingStatus?opt=" + "ter" + "&orderPoductionID=" + 0 + "&cropID=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataTerminal = dataJson.map(
					obj => {
						return {
							"id": obj.terminalID,
							"name": obj.terminal,
							"viaID": obj.viaID
						}
					}
				);
			}
		}
	});
	//<<<Obtener Almacén de Ingreso

	//>>>Obtener Línea de Embarque
	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingStatus?opt=" + "com" + "&orderPoductionID=" + 0 + "&cropID=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataShipping = dataJson.map(
					obj => {
						return {
							"id": obj.shippingCompanyID,
							"name": obj.shippingCompany,
							"viaID": obj.viaID
						}
					}
				);
			}
		}
	});
	//<<<Obtener Línea de Embarque

	$.ajax({
		type: "GET",
		url: "/Comex/GetCustomerType",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataInvoice = dataJson.map(
					obj => {
						return {
							"id": obj.customerID,
							"name": obj.customer
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetConsigneeType",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataConsignee = dataJson.map(
					obj => {
						return {
							"id": obj.customerID,
							"name": obj.customer
						}
					}
				);
			}
		}
	});


	$.ajax({
		type: "GET",
		url: "/Comex/GetNotifyType",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataNotify = dataJson.map(
					obj => {
						return {
							"id": obj.customerID,
							"name": obj.customer
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetDestinationType",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataShippingP = dataJson.map(
					obj => {
						return {
							"id": obj.destinationID,
							"name": obj.destination
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetDestinationArrive",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataDestination = dataJson.map(
					obj => {
						return {
							"id": obj.destinationID,
							"name": obj.destination
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetTariffList?cropID=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataTariff = dataJson.map(
					obj => {
						return {
							"id": obj.tariffHeadingID,
							"name": obj.tariffHeading
						}
					}
				);
			}
		}
		
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingStatus?opt=" + "pla" + "&orderPoductionID=" + 0 + "&cropID=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataPlant = dataJson.map(
					obj => {
						return {
							"id": obj.processPlantID,
							"name": obj.processPlant
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetExporterType?cropID=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataShipper = dataJson.map(
					obj => {
						return {
							"id": obj.customerID,
							"name": obj.customer
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetNroPacking?campaignID=" + _campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataPackingList = dataJson.map(
					obj => {
						return {
							"id": obj.nroPackingListID,
							"name": obj.abbreviation
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/VarietyGetByCrop?opt=" + "cro" + "&id=" + _cropID + "&log=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataVariety = dataJson.map(
					obj => {
						return {
							"id": obj.varietyID,
							"name": obj.name,
							"abbreviation": obj.abbreviation

						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/CategoryGetByCrop?opt=" + "cro" + "&id=" + _cropID + "&gro=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataCategory = dataJson.map(
					obj => {
						return {
							"id": obj.categoryID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetFormatByCrop?opt=" + "fci" + "&id=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataNetWeight = dataJson.map(
					obj => {
						return {
							"id": obj.formatID,
							"abreviation": obj.format,
							"name": obj.format,
							"weight": obj.weight
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetPackageProductAll?opt=" + "pkc" + "&id=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {

			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataTypeBox = dataJson.map(
					obj => {
						return {
							"id": obj.packageProductID,
							"name": obj.packageProduct,
							"abbreviation": obj.abbreviation
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetPresentByCrop?opt=" + "pci" + "&id=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataPresentation = dataJson.map(
					obj => {
						return {
							"id": obj.presentationID,
							"name": obj.description,
							"presentation": obj.presentation
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Comex/GetShippingLoadingBL",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataBL = dataJson.map(
					obj => {
						return {
							"id": obj.shippingLoadingBLId,
							"name": obj.description,
						}
					}
				);
			}
		}
	});
	sweet_alert_progressbar_cerrar();
}

function loadTitle(_viaID) {

	$.ajax({
		type: "GET",
		url: "/Comex/TittleByVia?viaID=" + _viaID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				var data = dataJson.map(
					obj => {
						return {
							"value": obj.settingValue,
						}
					}
				);
				$(txtOperator).text(data[0].value);
				$(txtTerminal).text(data[1].value);
				$(txtLine).text(data[2].value);
				$(txtName).text(data[3].value);
			}
		}
	});
}

function createCodePack() {
	if ($("#selectVariety").val() == null) {
		return;
	} if ($("#selectNetWeight").val() == null) {
		return;
	} if ($("#selectTypeBox").val() == null) {
		return;
	}
	if ($("#selectBrand").val() == null) {
		return;
	} if ($("#selectLabel").val() == null) {
		return;
	} if ($("#selectPresentation").val() == null) {
		return;
	}

	varietyID = $("#selectVariety option:selected").val();
	netWeightID = $("#selectNetWeight option:selected").val();
	typeBoxID = $("#selectTypeBox option:selected").val();
	brandID = $("#selectBrand option:selected").val();
	presentationID = $("#selectPresentation option:selected").val();
	labelID = $("#selectLabel option:selected").val();

	var varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
	var netWeightCode = dataNetWeight.filter(item => item.id == netWeightID)[0].abreviation
	var typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation
	var brandCode = dataBrand.filter(item => item.id == brandID)[0].abbreviation
	var presentationCode = dataPresentation.filter(item => item.id == presentationID)[0].presentation
	var labelCode = dataLabel.filter(item => item.id == labelID)[0].abbreviation

	if ($('#chkplu').is(":checked")) {
		var labelCodeP = 'P';
	}
	else {
		var labelCodeP = 'G';
	}

	switch (_cropID.toLowerCase()) {
		case 'cit':
			var codePack = varietyCode.trim() + '_' + netWeightCode + typeBoxCode.slice(0, 1) + brandCode.trim() + '_' + (typeBoxCode.slice(0, 1) + brandCode.trim() + labelCodeP).slice(0, 3);
			break;
		default:
			var codePack = varietyCode.trim() + '_' + netWeightCode + typeBoxCode.slice(0, 1) + brandCode.trim() + '_ ' + (presentationCode + labelCode).slice(0, 3);
	} 

	$(txtCodePack).val(codePack);
}

function addtblCodePack() {
	let shippingLoadingDetailID;
	let shippingLoadingID;
	let varietyID;
	let variety;
	let categoryID;
	let category;
	let netWeightID;
	let netWeight;
	let packageProductID;
	let packageProduct;
	let brandID;
	let brand;
	let presentationID;
	let presentation;
	let sizeID;
	let labelID;
	let label;
	let codePackID;
	let codePack;
	let boxPallet;
	let quantity;
	let txtNet;
	let pallets;
	let codePack2;
	let statusID;
	let bRsl = false;

	switch (_cropID.toLowerCase()) {

		case 'blu':
			categoryID = $('#_selectCategory option:selected').val();
			let _dataBrand = [];
			$.ajax({
				type: "GET",
				url: "/Comex/BrandGetByCateID?opt=" + "cat" + "&cropID=" + categoryID,
				async: false,
				headers: {
					'Cache-Control': 'no-cache, no-store, must-revalidate',
					'Pragma': 'no-cache',
					'Expires': '0'
				},
				success: function (data) {
					if (data.length > 0) {
						var dataJson = JSON.parse(data);
						_dataBrand = dataJson.map(
							obj => {
								return {
									"id": obj.brandID,
									"name": obj.brand,
									"abbreviation": obj.abbreviation
								}
							}
						);
					}
				}
			});
			shippingLoadingDetailID = $('#txtShippingLoadingDetailID').val();
			shippingLoadingID = $('#txtShippingLoadingID').val();			
			varietyID = $('#_selectVariety').val();
			variety = $('#_selectVariety option:selected').text();
			categoryID = $('#_selectCategory option:selected').val();
			category = $('#_selectCategory option:selected').text();
			netWeightID = _dataCodepack.filter(element => element.codePackID == $(_selectCodePack).val())[0].formatID;
			netWeight = '';
			packageProductID = $('#_selectTypeBox option:selected').val();
			packageProduct = $('#_selectTypeBox option:selected').text();
			brandID = _dataBrand[0].id;
			brand = _dataBrand[0].name;
			presentationID = $('#_selectPresentation option:selected').val();
			presentation = $('#_selectPresentation option:selected').text();
			sizeID = $('#_selectSize option:selected').val();
			labelID = 0;
			label = '';
			codePackID = $('#_selectCodePack option:selected').val();
			codePack = $('#_selectCodePack option:selected').text();
			boxPallet = $('#_txtBoxPerPallet').val();
			quantity = $('#_txtBoxPerPerContainer').val();
			txtNet = '';
			pallets = $('#_txtPallets').val();
			codePack2 = $('#_txtCodePack').val();
			statusID = 1;

			bRsl = true;
			$("#modalBluCodepack").modal('hide');
			break;
		default:
			shippingLoadingDetailID = $('#txtShippingLoadingDetailID').val();
			shippingLoadingID = $('#txtShippingLoadingID').val();
			varietyID = $('#selectVariety').val();
			varietyID = $('#selectVariety option:selected').val();
			variety = $('#selectVariety option:selected').text();
			categoryID = $('#selectCategory option:selected').val();
			category = $('#selectCategory option:selected').text();
			netWeightID = $('#selectNetWeight option:selected').val();
			netWeight = $('#selectNetWeight option:selected').text();
			packageProductID = $('#selectTypeBox option:selected').val();
			packageProduct = $('#selectTypeBox option:selected').text();
			brandID = $('#selectBrand option:selected').val();
			brand = $('#selectBrand option:selected').text();
			presentationID = $('#selectPresentation option:selected').val();
			presentation = $('#selectPresentation option:selected').text();
			sizeID = '';
			labelID = $('#selectLabel option:selected').val();
			label = $('#selectLabel option:selected').text();
			codePackID = $('#SelectSizeBox option:selected').val();
			codePack = $('#SelectSizeBox option:selected').text();
			boxPallet = $('#txtBoxPallet').val();
			quantity = $('#txtBoxes').val();
			txtNet = $('#txtNet').val();
			pallets = $('#txtPallets').val();
			codePack2 = $('#txtCodePack').val();
			statusID = $('#txtStatus').val();

			bRsl = true;
			$("#modalCodepack").modal('hide');
			break;
	}

	if (!bRsl) return;
	if (trEdit.length > 0) {
		$(trEdit).find('td.shippingLoadingDetailID').text(shippingLoadingDetailID);
		$(trEdit).find('td.shippingLoadingID').text(shippingLoadingID);
		$(trEdit).find('td.varietyID').text(varietyID);
		$(trEdit).find('td.variety').text(variety);
		$(trEdit).find('td.categoryID').text(categoryID);
		$(trEdit).find('td.category').text(category);
		$(trEdit).find('td.brandID').text(brandID);
		$(trEdit).find('td.brand').text(brand);
		$(trEdit).find('td.formatID').text(netWeightID);
		$(trEdit).find('td.format').text(netWeight);
		$(trEdit).find('td.packageProductID').text(packageProductID);
		$(trEdit).find('td.packageProduct').text(packageProduct);
		$(trEdit).find('td.presentationID').text(presentationID);
		$(trEdit).find('td.presentation').text(presentation);
		$(trEdit).find('td.sizeID').text(sizeID);
		$(trEdit).find('td.labelID').text(labelID);
		$(trEdit).find('td.label').text(label);
		$(trEdit).find('td.codePackID').text(codePackID);
		$(trEdit).find('td.codePack2').text(codePack2);
		$(trEdit).find('td.quantity').text(quantity);
		$(trEdit).find('td.statusID').text(statusID);
		$(trEdit).find('td.pallets').text(pallets);
		$("table#tblCodePack tbody").append(content);
	} else {
		var content = '<tr>';
		content += '<td style="max-width:30px; min-width:50px;">';
		content += '<button class="btn btn-sm" style="background-color:none; border:none; padding: 0px 5px 8px 10px;" onclick="openModalTabla(this)" >';
		content += '<span class="icon text-primary"><i class="fas fa-edit fa-sm"></i></span>';
		content += '</button>';
		content += '<i style="color: darkred; padding: 0px 0px 0px 0px;" class="fas fa-trash fa-sm row-remove"></i>';
		content += '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class="shippingLoadingDetailID">' + shippingLoadingDetailID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" shippingLoadingID" >' + shippingLoadingID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" categoryID" >' + categoryID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" category" >' + category + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" varietyID ">' + varietyID + '</td>';
		content += '<td style="max-width:150px; min-width:80px;" class=" variety">' + variety + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" brandID">' + brandID + '</td>';
		content += '<td style="max-width:100px; min-width:70px;" class=" brand">' + brand + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" formatID">' + netWeightID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" format ">' + netWeight + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" packageProductID">' + packageProductID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" packageProduct">' + packageProduct + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" presentationID">' + presentationID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" presentation">' + presentation + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" labelID">' + labelID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" label">' + label + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" codePackID">' + codePackID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" codePack">' + codePack + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" sizeID">' + sizeID + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" size">' + sizeID + '</td>';
		content += '<td style="max-width:180px; min-width:155px;" class="codePack2">' + codePack2 + '</td>';		
		content += '<td style="max-width:60px; min-width:50px; text-align:right;" class=" quantity">' + quantity + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class=" statusID">' + '1' + '</td>';
		content += '<td style="max-width:200px; min-width:200px;display:none" class="pallets">' + pallets + '</td>';
		content += '</tr>';
		$("table#tblCodePack tbody").append(content);
	}
}

function updatetblCodePack() {
	let shippingLoadingDetailID;
	let shippingLoadingID;
	let varietyID;
	let variety;
	let categoryID;
	let category;
	let netWeightID;
	let netWeight;
	let packageProductID;
	let packageProduct;
	let brandID;
	let brand;
	let presentationID;
	let presentation;
	let sizeID;
	let labelID;
	let label;
	let codePackID;
	let codePack;
	let boxPallet;
	let quantity;
	let txtNet;
	let pallets;
	let codePack2;
	let statusID;
	switch (_cropID.toLowerCase()) {

		case 'blu':
			categoryID = $('#_selectCategory option:selected').val();
			let _dataBrand = [];
			$.ajax({
				type: "GET",
				url: "/Comex/BrandGetByCateID?opt=" + "cat" + "&cropID=" + categoryID,
				async: false,
				headers: {
					'Cache-Control': 'no-cache, no-store, must-revalidate',
					'Pragma': 'no-cache',
					'Expires': '0'
				},
				success: function (data) {

					if (data.length > 0) {
						var dataJson = JSON.parse(data);
						_dataBrand = dataJson.map(
							obj => {
								return {
									"id": obj.brandID,
									"name": obj.brand,
									"abbreviation": obj.abbreviation
								}
							}
						);
					}
				}
			});
			shippingLoadingDetailID = $('#txtShippingLoadingDetailID').val();
			shippingLoadingID = $('#txtShippingLoadingID').val();
			varietyID = $('#_selectVariety').val();
			variety = $('#_selectVariety option:selected').text();
			categoryID = $('#_selectCategory option:selected').val();
			category = $('#_selectCategory option:selected').text();
			netWeightID = _dataCodepack.filter(element => element.codePackID == $(_selectCodePack).val())[0].formatID;	//$('#selectNetWeight option:selected').val();
			netWeight = '';
			packageProductID = $('#_selectTypeBox option:selected').val();
			packageProduct = $('#_selectTypeBox option:selected').text();
			brandID = _dataBrand[0].id;
			brand = _dataBrand[0].name;
			presentationID = $('#_selectPresentation option:selected').val();
			presentation = $('#_selectPresentation option:selected').text();
			sizeID = $('#_selectSize option:selected').val();
			labelID = 0;
			label = '';
			codePackID = $('#_selectCodePack option:selected').val();
			codePack = $('#_selectCodePack option:selected').text();
			boxPallet = '';
			quantity = $('#_txtBoxPerPerContainer').val();
			txtNet = '';
			pallets = '';
			codePack2 = $('#_txtCodePack').val();
			statusID = 1;
			$("#modalBluCodepack").modal('hide');
			break;
		default:
			shippingLoadingDetailID = $('#txtShippingLoadingDetailID').val();
			shippingLoadingID = $('#txtShippingLoadingID').val();
			varietyID = $('#selectVariety').val();
			varietyID = $('#selectVariety option:selected').val();
			variety = $('#selectVariety option:selected').text();
			categoryID = $('#selectCategory option:selected').val();
			category = $('#selectCategory option:selected').text();
			netWeightID = $('#selectNetWeight option:selected').val();
			netWeight = $('#selectNetWeight option:selected').text();
			packageProductID = $('#selectTypeBox option:selected').val();
			packageProduct = $('#selectTypeBox option:selected').text();
			brandID = $('#selectBrand option:selected').val();
			brand = $('#selectBrand option:selected').text();
			presentationID = $('#selectPresentation option:selected').val();
			presentation = $('#selectPresentation option:selected').text();
			sizeID = '';
			labelID = $('#selectLabel option:selected').val();
			label = $('#selectLabel option:selected').text();
			codePackID = $('#SelectSizeBox option:selected').val();
			codePack = $('#SelectSizeBox option:selected').text();
			boxPallet = $('#txtBoxPallet').val();
			quantity = $('#txtBoxes').val();
			txtNet = $('#txtNet').val();
			pallets = $('#txtPallets').val();
			codePack2 = $('#txtCodePack').val();
			statusID = $('#txtStatus').val();
			$("#modalCodepack").modal('hide');
			break;
	}

	$(trEdit).find('td.shippingLoadingDetailID').text(shippingLoadingDetailID);
	$(trEdit).find('td.shippingLoadingID').text(shippingLoadingID);
	$(trEdit).find('td.varietyID').text(varietyID);
	$(trEdit).find('td.variety').text(variety);
	$(trEdit).find('td.categoryID').text(categoryID);
	$(trEdit).find('td.category').text(category);
	$(trEdit).find('td.brandID').text(brandID);
	$(trEdit).find('td.brand').text(brand);
	$(trEdit).find('td.formatID').text(netWeightID);
	$(trEdit).find('td.format').text(netWeight);
	$(trEdit).find('td.packageProductID').text(packageProductID);
	$(trEdit).find('td.packageProduct').text(packageProduct);
	$(trEdit).find('td.presentationID').text(presentationID);
	$(trEdit).find('td.presentation').text(presentation);
	$(trEdit).find('td.sizeID').text(sizeID);
	$(trEdit).find('td.labelID').text(labelID);
	$(trEdit).find('td.label').text(label);
	$(trEdit).find('td.codePackID').text(codePackID);
	$(trEdit).find('td.codePack2').text(codePack2);
	$(trEdit).find('td.quantity').text(quantity);
	$(trEdit).find('td.statusID').text(statusID);
	$("table#tblCodePack tbody").append(content);
	
}

function calculateTotal() {
	var txtPallets = 0
	$("#tblCatVar tbody tr td.txtPallets").each(function () {
		txtPallets += parseFloat($(this).text());

	})

	$('#tblCatVar .totalPallets').html(txtPallets.toFixed(2))

}

function dateDiff(d1, d2) {
	var diff = Math.abs(d1 - d2);
	if (Math.floor(diff / 86400000)) {
		return Math.floor(diff / 86400000) + " days";
	} else if (Math.floor(diff / 3600000)) {
		return Math.floor(diff / 3600000) + " hours";
	} else if (Math.floor(diff / 60000)) {
		return Math.floor(diff / 60000) + " minutes";
	} else {
		return "< 1 minute";
	}
}

function SaveLoadingInstruction() {

	let TotPallet = parseFloat(0);
	$('table#tblCodePack>tbody>tr').each(function (iRow, objRow) {
		let objPallets = $(objRow).find('.pallets');
		TotPallet += parseFloat((objPallets.text() == "") ? 0 : objPallets.text());
	});

	switch (_cropID.toLowerCase()) {
		case 'blu':
			TotPallet = parseFloat(TotPallet.toFixed(2));
			break;
		default:
			TotPallet = parseFloat(TotPallet.toFixed(2));
	}

	//switch (_cropID.toLowerCase()) {
	//	case 'blu':
	//		//if (TotPallet > parseFloat(20) && _viaID == '1') {
	//		//	sweet_alert_error('Error', 'Total pallets must be 20. Cannot enter more pallets');
	//		//	return;
	//		//} else if (TotPallet < parseFloat(20) && _viaID == '1') {
	//		//	sweet_alert_error('Error', 'Total pallets must be 20. Cannot enter smaller pallets');
	//		//	return;
	//		//}
	//		//else if (TotPallet % parseFloat(1) != parseFloat(0) && _viaID == '2') {
	//		//	sweet_alert_error('Error', 'Total pallets must be a whole number');
	//		//	return;
 //           //}
	//		break;
	//	case 'cit':
	//		//if (TotPallet > parseFloat(21)) {
	//		//	sweet_alert_error('Error', 'Total pallets must be 20 or 21. Cannot enter more pallets');
	//		//	return;
	//		//} else if (TotPallet < parseFloat(20)) {
	//		//	sweet_alert_error('Error', 'Total pallets must be 20. Cannot enter smaller pallets');
	//		//	return;
	//		//}
	//		//break;
	//	default:
	//		if (TotPallet > parseFloat(20)) {
	//			sweet_alert_error('Error', 'Total pallets must be 20. Cannot enter more pallets');
	//			return;
	//		} else if (TotPallet < parseFloat(20)) {
	//			sweet_alert_error('Error', 'Total pallets must be 20. Cannot enter smaller pallets');
	//			return;
 //           }
	//}	
	
	if (!obligatorySave()) {
		return
	}

	if (!obligatory2()) {
		return
	}

	if (!obligatory3()) {
		return
	}

	if (!obligatory4()) {
		return
	}

	if (!obligatory5()) {
		return
	}

	//if (!obligatory6()) {
	//	return
	//}

	var o = {}
	o.dateCreated = "";
	o.shippingLoadingID = $('#txtShippingLoading').val();
	o.orderProductionID = $('#txtOrderProduction').val();
	o.managerID = $('#selectCharge option:selected').val();
	o.manager = $('#selectCharge option:selected').text();
	o.logisticOperatorID = $('#selectOperator option:selected').val();
	o.logisticOperator = $('#selectOperator option:selected').text();
	o.nroPackingListID = $('#selectPackingList option:selected').val();
	o.abbreviation = $('#selectPackingList option:selected').text();
	o.abbreviationPacking = $('#selectPackingList option:selected').text();
	o.nroPackingList = $('#txtNPackingList').val();
	o.contact = $('#txtContact').val();
	o.terminalID = $('#selectEntrance option:selected').val();
	o.terminal = $('#selectEntrance option:selected').text();
	o.shippingCompanyID = $('#selectShippingLine option:selected').val();
	o.shippingCompany = $('#selectShippingLine option:selected').text();
	o.orderProduction = $('#txtReference').val();
	o.booking = $('#txtBooking').val();
	o.veseel = $('#txtVessel').val();
	o.freightCondition = $('#selectFreight option:selected').val();
	o.regime = $('#selectRegimen option:selected').val();
	var HTS = $('#selectTariff').val();
	var HTS2 = '';
	$.each(HTS, function (index, value) {
		HTS2 += value + ',';
	});
	o.tariffHeadingID = HTS2;
	o.tariffHeading = $('#selectTariff option:selected').text();
	o.originDestinationID = $('#selectPort option:selected').val();
	o.originDestination = $('#selectPort option:selected').text();
	o.arriveDestinationID = $('#selectDestinationPort option:selected').val();
	o.arriveDestination = $('#selectDestinationPort option:selected').text();
	o.shippingLoadingBLId = $('#selectBL option:selected').val();
	o.bl = $('#selectBL option:selected').text();

	var FechaHoraETD = ($('#dateETD2').val());
	var FechaETD = FechaHoraETD.split("-");
	var FechaETDForm = FechaETD[2] + "-" + FechaETD[1] + "-" + FechaETD[0];
	var dateETD = FechaETDForm + " 00:00:00";
	o.etd = dateETD;

	var FechaHoraETA = ($('#dateETA2').val());
	var FechaETA = FechaHoraETA.split("-");
	var FechaETAForm = FechaETA[2] + "-" + FechaETA[1] + "-" + FechaETA[0];
	var dateETA = FechaETAForm + " 00:00:00";
	o.eta = dateETA;

	o.certificates = $('#checkFitosanitario').prop('checked') ? 1 : 0
	o.shipperID = $('#selectShipper option:selected').val()
	o.shipper = $('#selectShipper option:selected').text()
	o.customerID = $('#selectInvoice option:selected').val()
	o.customer = $('#selectInvoice option:selected').text()
	o.consigneeID = $('#selectConsignee option:selected').val()
	o.consignee = $('#selectConsignee option:selected').text()
	o.notifyID = $('#selectNotify option:selected').val()
	o.notify = $('#selectNotify option:selected').text()
	o.processPlantID = $('#selectPlant option:selected').val()
	o.processPlant = $('#selectPlant option:selected').text()
	o.vgm = $('#txtVGM').val()
	o.coldTreatment = $('#coldTret').prop('checked') ? 1 : 0
	o.controlledAtmosphere = $('#controlAtmosphere').prop('checked') ? 1 : 0
	o.temperature = $('#txtTemperature').val()
	o.ventilation = $('#txtVentilation').val()
	o.humedity = $('#checkHumedity').prop('checked') ? 1 : 0
	o.quest = $('#checkQuest').prop('checked') ? 1 : 0

	var FechaHoraArrivel = ($('#dateArrivel2').val());
	var FechaArrivel = FechaHoraArrivel.split("-");
	var FechaArrivelForm = FechaArrivel[2] + "-" + FechaArrivel[1] + "-" + FechaArrivel[0];
	var dateArrivel = FechaArrivelForm + " 00:00:00";
	o.datetoPlantEntry = dateArrivel;

	var FechaHoraDeparture = ($('#dateDeparture2').val());
	var FechaDeparture = FechaHoraDeparture.split("-");
	var FechaDepartureForm = FechaDeparture[2] + "-" + FechaDeparture[1] + "-" + FechaDeparture[0];
	var dateDeparture = FechaDepartureForm + " 00:00:00";
	o.departureDate = dateDeparture;
	o.arrivalTime = $('#arrivalTime2').val() + ":00";

	var FechaHoradateTerminal = ($('#dateTerminal2').val());
	var FechadateTerminal = FechaHoradateTerminal.split("-");
	var FechadateTerminalForm = FechadateTerminal[2] + "-" + FechadateTerminal[1] + "-" + FechadateTerminal[0];
	var datedateTerminal = FechadateTerminalForm + " 00:00:00";
	o.terminalEntryDate = datedateTerminal;

	o.comments = $('#noteShipping').val();
	o.statusID = 0;
	o.campaignID = _campaignID;
	o.details = [];

	let iTotalVarietyWithoutCodepack = 0;
	$('table#tblCodePack>tbody>tr').each(function (i, e) {
		var item = {}

		item.shippingLoadingDetailID = $(e).find('td.shippingLoadingDetailID').text();
		item.shippingLoadingID = $(e).find('td.shippingLoadingID').text();
		item.categoryID = $(e).find('td.categoryID').text();
		item.category = $(e).find('td.category').text();
		item.varietyID = $(e).find('td.varietyID').text();
		item.variety = $(e).find('td.variety').text();
		item.brandID = $(e).find('td.brandID').text();
		item.brand = $(e).find('td.brand').text();
		item.formatID = $(e).find('td.formatID').text();
		item.format = $(e).find('td.format').text();
		item.packageProductID = $(e).find('td.packageProductID').text();
		item.packageProduct = $(e).find('td.packageProduct').text();
		item.presentationID = $(e).find('td.presentationID').text();
		item.presentation = $(e).find('td.presentation').text();
		item.labelID = $(e).find('td.labelID').text();
		item.label = $(e).find('td.label').text();
		item.codePackID = $(e).find('td.codePackID').text();
		item.codePack = $(e).find('td.codePack').text();
		item.sizeID = $(e).find('td.sizeID').text();
		item.size = 0;
		item.codePack2 = $(e).find('td.codePack2').text();
		if ($(e).find('td.codePack2').text().trim().length == 0) {
			iTotalVarietyWithoutCodepack++;
		}
		item.quantity = $(e).find('td.quantity').text();
		item.statusID = $(e).find('td.statusID').text();
		item.pallets = $(e).find('td.pallets').text();

		o.details.push(item)
	});

	if (iTotalVarietyWithoutCodepack > 0) {
		sweet_alert_error('Error!', 'There are at least 1 variety without codepack. Edit information and try again.');
		return;
	}

	var sUrlApi = "/Comex/SaveShippingLoading";
	$.ajax({
		type: 'POST',
		url: sUrlApi,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		contentType: "application/json",
		dataType: 'json',
		data: JSON.stringify(o),
		success: function (datares) {
			if (datares.length > 0) {
				sweet_alert_success('Saved!', 'Data saved successfully.');
				$("#modalCreateUpdateIE").modal('hide');
				showDataSearched();
			}
			else {
				sweet_alert_error('Error!', 'Try Again.');
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {
			sweet_alert_error("Error", "Data was not saved");

		}
	})
}

function generatePdf(shippingLoadingID) {
	var dataPO;
	let bRsl = false;
	$.ajax({
		type: "GET",
		url: "/Comex/GetForPdf?shippingLoadingID=" + shippingLoadingID + "&cropID=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
				return;
			}
			dataPO = JSON.parse(data);
			if (dataPO == null || dataPO.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
				return;
			}
			po = dataPO;
			bRsl = true;
		}
	});
	if (!bRsl) return;
	//var doc = new jsPDF()
	var docTmp = new jsPDF('p', 'mm', [300, 210]);
	
	$height = 305
	if (dataPO.details.length > 5) {
		//$splitCadena = docTmp.splitTextToSize(dataPO.comments, 200);
		$height = $height + (dataPO.details.length * 3.5);
    }

	var doc = new jsPDF('p', 'mm', [$height, 210]);

	var imgData = '';
	switch (_cropID.toLowerCase()) {
		case 'blu':
			imgData = '../../images/logo_Larama.jpg'  
			doc.addImage(imgData, 'JPEG', 18, 3, 30, 30)
			break;
		default:
			imgData = '../../images/logoandrea200x200.jpg'
			doc.addImage(imgData, 'JPEG', 23, 8, 22, 21)
	}
	
	doc.setFontSize(20);
	doc.setFontType('bold')

	doc.setLineWidth(0.5)	//grosor de la linea
	doc.line(55, 20, 163, 20)	//linea
	doc.text(55, 19, " INSTRUCCIÓN DE EMBARQUE ");	//texto
	doc.setFontSize(10);
	doc.text(80, 26, "N° PackingList: " + dataPO.nroPackingList);
	doc.setFontSize(8);	//tamano del font
	doc.setFontType('normal')	//tipo fuente
	doc.setLineWidth(0.1)	// grosor de la linea
	doc.rect(25, 31, 30, 5)	//cuadros
	doc.setFontType('bold')
	doc.text(27, 35, "Persona Encargada:");
	doc.rect(55, 31, 57, 5);
	doc.setFontType('normal');
	doc.text(57, 35, dataPO.manager);

	doc.rect(112, 31, 30, 5)
	doc.setFontType('bold')
	doc.text(114, 35, "N° Orden de Venta: ");
	doc.rect(142, 31, 45, 5)
	doc.setFontType('normal')
	if ((dataPO.orderProduction) != 'NULL') {
		doc.text(144, 35, dataPO.orderProduction);
	} else {
		doc.text(144, 35, "NO TIENE AÚN SO");
	}
	//FILA 2
	doc.rect(25, 36, 30, 5)
	doc.setFontType('bold')
	doc.text(27, 40, "Operador Logístico:");
	doc.rect(55, 36, 57, 5)
	doc.setFontType('normal')
	doc.text(57, 40, dataPO.logisticOperator);

	doc.rect(112, 36, 30, 5)
	doc.setFontType('bold')
	doc.text(114, 40, "Fecha de Contacto");
	doc.rect(142, 36, 45, 5)
	doc.setFontType('normal')
	doc.text(144, 40, dataPO.date);

	//FILA 3
	doc.rect(25, 41, 30, 5)
	doc.setFontType('bold')
	doc.text(27, 45, "Ingreso Terminal :");
	doc.rect(55, 41, 57, 5)
	doc.setFontType('normal')
	doc.text(57, 45, dataPO.terminal);

	doc.rect(112, 41, 30, 5)
	doc.setFontType('bold')
	doc.text(114, 45, "Contacto:");
	doc.rect(142, 41, 45, 5)
	doc.setFontType('normal')
	doc.text(144, 45, dataPO.contact);

	//FILA 4
	doc.rect(25, 46, 30, 5)
	doc.setFontType('bold')
	doc.text(27, 50, "Naviera :");
	doc.rect(55, 46, 57, 5)
	doc.setFontType('normal')
	doc.text(57, 50, dataPO.shippingLine);

	doc.rect(112, 46, 30, 5)
	doc.setFontType('bold')
	doc.text(114, 50, "N° Booking:");
	doc.rect(142, 46, 45, 5)
	doc.setFontType('normal')
	doc.text(144, 50, dataPO.booking);

	doc.setLineWidth(0.5)	//grosor de la linea

	doc.line(25, 53, 187, 53)	//linea

	//texto col1
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(40, 58, "DATOS DEL EMBARCADOR");
	doc.setLineWidth(0.5)	//grosor de la linea col1
	doc.line(25, 59, 103, 59)	//linea

	//texto col2
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(118, 58, "DATOS DE FACTURACIÓN");

	doc.setLineWidth(0.5)	//grosor de la linea col1

	doc.line(112, 59, 187, 59)	//linea
	doc.setLineWidth(0.1)

	//Fila6 COL 1
	doc.rect(25, 60, 22, 7)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 63, "Razón Social:");

	doc.rect(47, 60, 56, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);

	var embarcador = dataPO.shipper
	$splitCadena = doc.splitTextToSize(embarcador, 50);
	doc.text(48, 63, $splitCadena);

	//Fila6 colum2
	doc.rect(112, 60, 22, 7)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(114, 63, "Razón Social:");

	doc.rect(134, 60, 53, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	var customer = dataPO.customer
	$splitCadena = doc.splitTextToSize(customer, 50);
	doc.text(135, 63, $splitCadena);

	//Fila7
	doc.rect(25, 67, 22, 16)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 70, "Dirección:");

	doc.rect(47, 67, 56, 16);
	doc.setFontType('normal');
	doc.setFontSize(7);
	var direccionEmbarcador = dataPO.shipperaddress
	$splitCadena = doc.splitTextToSize(direccionEmbarcador, 50);
	doc.text(48, 70, $splitCadena);

	//Fila7 colum2
	doc.rect(112, 67, 22, 16)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(114, 70, "Dirección:");

	doc.rect(134, 67, 53, 16)
	doc.setFontType('normal')
	doc.setFontSize(7);
	var direccionCustomer = dataPO.customeraddress
	$splitCadena = doc.splitTextToSize(direccionCustomer, 50);
	doc.text(135, 70, $splitCadena);

	//Fila9 col1
	doc.rect(25, 83, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 86, "Contacto:");

	doc.rect(47, 83, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 86, dataPO.shippercontact);

	//Fila9 colum2
	doc.rect(112, 83, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 86, "Contacto:");

	doc.rect(134, 83, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 86, dataPO.customercontact);

	//Fila10 col1
	doc.rect(25, 88, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 91, "Teléfono:");

	doc.rect(47, 88, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 91, dataPO.shipperphone);
	//Fila10 colum2
	doc.rect(112, 88, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 91, "Teléfono:");

	doc.rect(134, 88, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 91, dataPO.customerphone);

	//Fila11 col1
	doc.rect(25, 93, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 96, "E-mail:");

	doc.rect(47, 93, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 96, dataPO.shipperemail);

	//Fila11 colum2
	doc.rect(112, 93, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 96, "E-mail:");

	doc.rect(134, 93, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 96, dataPO.customeremail);

	//linea4
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(40, 103, "DATOS DEL CONSIGNATARIO");
	//grosor de la linea col1
	doc.setLineWidth(0.5)
	//linea
	doc.line(25, 104, 103, 104)

	//texto col2
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(125, 103, "DATOS DEL NOTIFICANTE");
	doc.setLineWidth(0.5)	//grosor de la linea col1
	doc.line(112, 104, 187, 104)	//linea
	doc.setLineWidth(0.1)

	//Fila12 col1
	doc.rect(25, 105, 22, 7)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 108, "Razón Social");

	doc.rect(47, 105, 56, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	var datosConsignee = dataPO.consignee
	$splitCadena = doc.splitTextToSize(datosConsignee, 50);
	doc.text(48, 108, $splitCadena);

	//Fila12 colum2
	doc.rect(112, 105, 22, 7)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 108, "Razón Social:");

	doc.rect(134, 105, 53, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	var datosNotify = dataPO.notify
	$splitCadena = doc.splitTextToSize(datosNotify, 50);
	doc.text(135, 108, $splitCadena);

	//Fila13 col1
	doc.rect(25, 112, 22, 16)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 115, "Dirección:");

	doc.rect(47, 112, 56, 16)
	doc.setFontType('normal')
	doc.setFontSize(7);

	var direccionConsignee = dataPO.consigneeaddress
	$splitCadena = doc.splitTextToSize(direccionConsignee, 50);
	doc.text(48, 115, $splitCadena);

	//Fila13 colum2
	doc.rect(112, 112, 22, 16)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 115, "Dirección:");

	doc.rect(134, 112, 53, 16)
	doc.setFontType('normal')
	doc.setFontSize(7);

	var direccionNotify = dataPO.notifyaddress
	$splitCadena = doc.splitTextToSize(direccionNotify, 50);
	doc.text(135, 115, $splitCadena);

	//Fila14 col1
	doc.rect(25, 128, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 131, "Contacto:");

	doc.rect(47, 128, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 131, dataPO.consigneecontact);

	//Fila14 colum2
	doc.rect(112, 128, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 131, "Contacto:");

	doc.rect(134, 128, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 131, dataPO.notifycontact);

	//Fila15 col1
	doc.rect(25, 133, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 136, "Teléfono:");

	doc.rect(47, 133, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 136, dataPO.consigneephone);

	//Fila15 colum2
	doc.rect(112, 133, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 136, "Teléfono:");

	doc.rect(134, 133, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 136, dataPO.notifyphone);

	//Fila16 col1
	doc.rect(25, 138, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 141, "E-mail:");

	doc.rect(47, 138, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 141, dataPO.consigneeemail);

	//Fila16 colum2
	doc.rect(112, 138, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 141, "E-mail:");

	doc.rect(134, 138, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 141, dataPO.notifyemail);

	//new
	doc.rect(25, 143, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(27, 146, "TAX/USCI:");

	doc.rect(47, 143, 56, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(48, 146, dataPO.consigneeUSCI);

	//Fila16 colum2
	doc.rect(112, 143, 22, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(114, 146, "TAX/USCI");

	doc.rect(134, 143, 53, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(135, 146, dataPO.notifyUSCI);
	//end new

	//TERCERA FUENTES EN MAYUSC
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(95, 153, "ADUANA");
	doc.setLineWidth(0.5)	//grosor de la linea col1
	doc.line(25, 154, 187, 154)	//linea
	doc.setLineWidth(0.1)

	//Fila18 col1
	doc.rect(25, 155, 48, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(36, 158, "Nave Viaje");

	doc.rect(73, 155, 38, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(75, 158, "Condición Flete");

	//Fila18 col2
	doc.rect(112, 155, 38, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(118, 158, "Puerto Embarque");

	doc.rect(150, 155, 37, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 158, "Puerto Destino");

	//Fila19 col1
	doc.rect(25, 160, 48, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	if (dataPO.veseel.length>27) {
		doc.setFontSize(6);
    }
	doc.text(26, 163, dataPO.veseel);

	doc.rect(73, 160, 38, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(74, 163, dataPO.freightCondition);

	//Fila19 col2
	doc.rect(112, 160, 38, 8)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(113, 163, dataPO.portShipping);

	doc.rect(150, 160, 37, 8)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(151, 163, dataPO.portDestination);

	//Fila20 col1
	doc.rect(25, 165, 48, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(36, 168, "Regimen");

	doc.rect(73, 165, 38, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(75, 168, "Partida Arancelaria");

	//bl
	doc.rect(25, 175, 48, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(36, 178, "BL or AWB");

	doc.rect(73, 175, 38, 5)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(74, 178, dataPO.bl);

	//Fila20 col2
	doc.rect(112, 168, 38, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(126, 171, "E.T.D");

	doc.rect(150, 168, 37, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(165, 171, "E.T.A ");

	//Fila21 col1
	doc.rect(25, 170, 48, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(26, 173, dataPO.regime);

	doc.rect(73, 170, 38, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(74, 173, dataPO.tariffHeading);

	//Fila21 col2
	doc.rect(112, 173, 38, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(113, 176, dataPO.etd);

	doc.rect(150, 173, 37, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(151, 176, dataPO.eta);

	//Fila22 col2
	doc.rect(112, 178, 38, 5)
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(113, 181, "Certificado FitoSanitario");

	doc.rect(150, 178, 37, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	if (dataPO.certificates == '1') {
		doc.text(151, 181, "SI");
	} else {
		doc.text(151, 181, "NO");
	}

	//TERCERA FUENTES EN MAYUSC
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(25, 185, "DESCRIPCIÓN DE LA CARGA");
	doc.setLineWidth(0.5)	//grosor de la linea col1
	doc.line(25, 186, 187, 186)	//linea
	doc.setLineWidth(0.1)
	//Fila23 col1
	doc.rect(25, 187, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(39, 190, "Variedad");
	//Fila23 col2
	doc.rect(65, 187, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(80, 190, "Envase");
	//Fila23 col3
	doc.rect(105, 187, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(121, 190, "Marca");
	//Fila23 col4 De aquí
	doc.rect(145, 187, 42, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(159, 190, "Cantidad");

	base = 195
	doc.setFontType('normal')
	$.each(dataPO.details, function (i, e) {
		doc.rect(25, base - 3, 40, 5)
		doc.rect(65, base - 3, 40, 5)
		doc.rect(105, base - 3, 40, 5)
		doc.rect(145, base - 3, 42, 5)
		doc.text(45, base, e.variety, 'center');
		doc.text(85, base, e.codePack, 'center');
		doc.text(125, base, e.brand, 'center');
		doc.text(165, base, e.quantity.toFixed(2).toString(), 'center');
		base = base + 5
	})	

	//CUARTA FILA FUENTES EN MAYUSC
	//doc.line(149, 112, 103,112)
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(25, base + 2, "FECHA DESPACHO"); //212
	//grosor de la linea col1
	doc.setLineWidth(0.5)
	//linea
	doc.line(25, base + 3, 187, base + 3)//214
	doc.setLineWidth(0.1)

	//Fila25 col1
	doc.rect(25, base + 4, 40, 5)//216
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(35, base + 7, "Ingreso Planta ");//219
	//Fila25 col2
	doc.rect(65, base + 4, 40, 5)//216
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(76, base + 7, "Hora Llenado");//219
	//Fila25 col3
	doc.rect(105, base + 4, 40, 5)//216
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(117, base + 7, "Fecha Salida");//219
	//Fila25 col4
	doc.rect(145, base + 4, 42, 5)//216
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(150, base + 7, "Fecha Ingreso Terminal");//219

	//Fila26 col1
	doc.rect(25, base + 9, 40, 5)//221
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(38, base + 12, dataPO.datetoPlantEntry);//224
	//Fila26 col2
	doc.rect(65, base + 9, 40, 5)//221
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(80, base + 12, dataPO.arrivalTime);//224
	//Fila26 col3
	doc.rect(105, base + 9, 40, 5) //221
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(119, base + 12, dataPO.departureDate);//224
	//Fila25 col4
	doc.rect(145, base + 9, 42, 5) //221
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(158, base + 12, dataPO.terminalEntryDate);//224

	////QUINTA FILA FUENTES EN MAYUSC
	//doc.line(149, 112, 103,112)
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(26, base + 19, "MERCANCIA DEL CUERPO B/L"); //225
	//grosor de la linea col1
	doc.setLineWidth(0.5)
	//linea
	doc.line(25, base + 20, 108, base + 20) //227

	///COMMENTS
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(26, base + 25, dataPO.comments);

	//texto col2
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(112, base + 19, "PARÁMETROS"); //225
	//grosor de la linea col1
	doc.setLineWidth(0.5)
	//linea
	doc.line(110, base + 20, 187, base + 20)//227
	doc.setLineWidth(0.1)

	//Fila27 col1
	if (dataPO.details.length > 5) {
		$height = 49
		$height = $height + (dataPO.details.length * 1.5); 
		doc.rect(25, base + 21, 83, $height)
	} else {
		doc.rect(25, base + 21, 83, 49)
    }
	doc.setFontType('normal')
	doc.setFontSize(7);

	//Fila 28c ol1
	doc.rect(110, base + 22, 20, 5)//230
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(112, base + 25, "Temperatura");//233
	//Fila 28c col2
	doc.rect(130, base + 22, 20, 5)//230
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(132, base + 25, "Ventilación"); //233

	//Fila 28c col3
	doc.rect(150, base + 22, 20, 5)//230
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(152, base + 25, "Humedad");//233

	//Fila 28c col4
	doc.rect(170, base + 22, 17, 5)//230
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(172, base + 25, "Quest");//233

	//Fila 29c ol1
	doc.rect(110, base + 27, 20, 5)//235
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(112, base + 30, (dataPO.temperature).toString());//238 must be string or array

	//Fila 29c col2
	doc.rect(130, base + 27, 20, 5)//235
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(132, base + 30, dataPO.ventilation);//238

	//Fila 29c col3
	doc.rect(150, base + 27, 20, 5)//235
	doc.setFontType('normal')
	doc.setFontSize(7);
	if (dataPO.humedity == '1') {
		doc.text(152, base + 30, 'ON');//238
	} else {
		doc.text(152, base + 30, 'OFF');//238
	}
	//doc.text(152, base + 36, dataPO.humedity);//238 Revisarrrr

	//Fila 29c col4
	doc.rect(170, base + 27, 17, 5)//235
	doc.setFontType('normal')
	doc.setFontSize(7);
	if (dataPO.quest == '1') {
		doc.text(172, base + 30, 'ON');//238
	} else {
		doc.text(172, base + 30, 'OFF');//238
	}
	//doc.text(172, base + 36, dataPO.quest);//238 revisar

	//Fila 30c ol1
	doc.rect(110, base + 32, 20, 5)//240
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(111, base + 35, "Cold Treatment");//243
	//Fila 30c col2
	doc.rect(130, base + 32, 57, 5)//240
	doc.setFontType('normal')
	doc.setFontSize(7);
	if (dataPO.coldTreatment == '1') {
		doc.text(132, base + 35, " SI");//243
	} else {
		doc.text(132, base + 35, " NO");//243
	}

	//texto col2
	doc.setFontType('bold')
	doc.setFontSize(10);
	doc.text(112, base + 44, "PLANTA Y DIRECCIÓN DE LLENADO"); //252
	//grosor de la linea col1
	doc.setLineWidth(0.5)
	//linea
	doc.line(110, base + 45, 187, base + 45)//254
	doc.setLineWidth(0.1)

	//Fila 31c col1
	doc.rect(110, base + 47, 22, 5)//257
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(112, base + 50, "Sucursal");//260
	//Fila 31c col2	
	doc.rect(132, base + 47, 55, 5)//257
	doc.setFontType('normal')
	doc.setFontSize(7);
	var sucursal = dataPO.processPlant
	$splitCadena = doc.splitTextToSize(sucursal, 50);
	doc.text(133, base + 50, $splitCadena);//260

	//Fila 32c col1
	doc.rect(110, base + 52, 22, 7)//262
	doc.setFontType('bold')
	doc.setFontSize(7);
	doc.text(112, base + 55, "Dirección:");//265
	//Fila 32c col2	
	doc.rect(132, base + 52, 55, 7)//262
	doc.setFontType('normal')
	doc.setFontSize(7);
	var direccionPlanta = dataPO.processPlantaddress
	$splitCadena = doc.splitTextToSize(direccionPlanta, 50);
	doc.text(133, base + 55, $splitCadena);//265

	doc.save('IE_' + dataPO.nroPackingList + '.pdf');

}

function generatePdfComex(shippingLoadingID) {
	let bRsl = false;
	$.ajax({
		type: "GET",
		url: "/Comex/GetForPdf?shippingLoadingID=" + shippingLoadingID + "&cropID=" + _cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
				return;
			}
			dataPO = JSON.parse(data);
			if (dataPO == null || dataPO.length == 0) {
				sweet_alert_error('Error!', 'There has been a problem when consulting the information. Try again.');
				return;
			}
			bRsl = true;
		}
	});

	if (!bRsl) return;
	var doc = new jsPDF()

	var imgData = '';
	switch (_cropID.toLowerCase()) {
		case 'blu':
			imgData = '../../images/logo_Larama.jpg'
			doc.addImage(imgData, 'JPEG', 18, 8, 30, 30)
			break;
		default:
			imgData = '../../images/logoandrea200x200.jpg'
			doc.addImage(imgData, 'JPEG', 23, 12, 22, 21)
	}		

	doc.setFontSize(20);
	doc.setFontType('bold')
	//doc.line(60, 30, 60, 20) 
	//grosor de la linea
	doc.setLineWidth(0.5)
	//linea
	doc.line(72, 28, 155, 28)
	//texto
	doc.text(73, 25, " COMERCIO EXTERIOR ");	

	//tamano del font
	doc.setFontSize(8);
	//tipo fuente
	doc.setFontType('normal')
	// grosor de la linea
	//Fila 1	
	doc.line(25, 38, 200, 38)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(28, 43, "Persona Encargada: " + dataPO.manager);
	doc.text(89, 43, "N° Packing List: " + dataPO.nroPackingList);
	doc.text(149, 43, "N° Orden de Venta: " + dataPO.orderProduction);
	doc.line(25, 48, 200, 48)
	doc.setFontSize(11);
	doc.text(99, 55, " PRE- EMBARQUE");
	//LINEA 3
	doc.line(25, 58, 200, 58)
	doc.setLineWidth(0.1)
	//Fila col1
	doc.rect(25, 60, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 63, "Broker/Cliente");
	//Fila col2
	doc.rect(70, 60, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(71, 63, "Consignatario");
	//Fila col3
	doc.rect(115, 60, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(116, 63, "Naviera");
	//Fila 1 col4
	doc.rect(155, 60, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 63, "Operador Logistico");

	//Fila2  col1
	doc.rect(25, 65, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(26, 68, dataPO.consignee);

	//Fila 2 col2
	doc.rect(70, 65, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(71, 68, dataPO.consignee);
	//Fila2 col3
	doc.rect(115, 65, 40, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(116, 68, dataPO.shippingLine);

	//Fila 2 col4
	doc.rect(155, 65, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(156, 68, dataPO.logisticOperator);

	//Fila3  col1
	doc.rect(25, 72, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 75, "Nave Viaje");

	//Fila 3 col2
	doc.rect(70, 72, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(71, 75, "N° Booking");

	//Fila3 col3
	doc.rect(115, 72, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(116, 75, "Puerto Embarque");

	//Fila 3 col4
	doc.rect(155, 72, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 75, "Puerto Destino");

	//Fila4  col1
	doc.rect(25, 77, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(27, 80, dataPO.veseel);

	//Fila 4 col2
	doc.rect(70, 77, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(71, 80, dataPO.booking);

	//Fila4 col3
	doc.rect(115, 77, 40, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(116, 80, dataPO.portShipping);

	//Fila 4 col4
	doc.rect(155, 77, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(7);
	doc.text(156, 80, dataPO.portDestination);

	//Fila5  col1
	doc.rect(25, 84, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 87, "Fecha Llenado");

	//Fila 5 col2
	doc.rect(70, 84, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(71, 87, "Ingreso a Terminal");

	//Fila5 col3
	doc.rect(115, 84, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(116, 87, "E.T.D. Origen");

	//Fila 5 col4
	doc.rect(155, 84, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 87, "E.T.A. Destino");

	//Fila6  col1
	doc.rect(25, 89, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(27, 92, dataPO.date);

	//Fila 6 col2
	doc.rect(70, 89, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(71, 92, dataPO.terminalEntryDate);

	//Fila6 col3
	doc.rect(115, 89, 40, 7)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(116, 92, dataPO.etd);

	//Fila 6 col4
	doc.rect(155, 89, 45, 7)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(156, 92, dataPO.eta);
	//linea 5
	doc.setLineWidth(0.5)
	doc.line(25, 100, 200, 100)
	doc.setLineWidth(0.1)
	//Fila7  col1
	doc.rect(25, 102, 45, 7)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 105, "N° Contenedor");

	//Fila7  col2
	doc.rect(70, 102, 130, 7)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(72, 105, "");

	//Fila8  col1
	doc.rect(25, 109, 45, 7)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 113, "N° Factura");

	//Fila8  col2
	doc.rect(70, 109, 130, 7)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(72, 113, "");
	//linea6
	doc.setLineWidth(0.5)
	doc.line(25, 119, 200, 119)
	doc.setLineWidth(0.1)

	doc.setFontType('bold')
	doc.setFontSize(11);
	doc.text(100, 124, "DOCUMENTACION");

	//linea7
	doc.setLineWidth(0.5)
	doc.line(25, 127, 200, 127)
	doc.setLineWidth(0.1)

	//Fila8  col1
	doc.rect(25, 130, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 133, "ISF 10+2 (USA)");

	//Fila 8 col2
	doc.rect(70, 130, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(71, 133, "FDA (USA)");

	//Fila8 col3
	doc.rect(115, 130, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(116, 133, "Packing List");

	//Fila 8 col4
	doc.rect(155, 130, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 133, "Certificado FitoSanatorio");

	//Fila9  col1
	doc.rect(25, 135, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(27, 138, "");

	//Fila 9 col2
	doc.rect(70, 135, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(71, 133, "");

	//Fila9 col3
	doc.rect(115, 135, 40, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(116, 138, "");

	//Fila 9 col4
	doc.rect(155, 135, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(156, 138, "");

	//Fila10  col1
	doc.rect(25, 140, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 143, "Emisión de Factura");

	//Fila 10 col2
	doc.rect(70, 140, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(71, 143, "Certificado de Origen");

	//Fila10 col3
	doc.rect(115, 140, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(116, 143, "Correción del B.L");

	//Fila 10 col4
	doc.rect(155, 140, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 143, "Entrega del B.L.");
	//Fila11  col1
	doc.rect(25, 145, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(27, 148, "");
	//Fila 11 col2
	doc.rect(70, 145, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(71, 148, "");
	//Fila11 col3
	doc.rect(115, 145, 40, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(116, 148, "");
	//Fila 11 col4
	doc.rect(155, 145, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(156, 148, "");

	//Fila12  col1
	doc.rect(25, 150, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(27, 153, "Otros Documentos ");
	//Fila 12 col2
	doc.rect(70, 150, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(71, 153, "Fecha Envio - Doc. Por");
	//Fila12 col3
	doc.rect(115, 150, 40, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(116, 153, "Fecha Envio - Courier");
	//Fila 12 col4
	doc.rect(155, 150, 45, 5)
	doc.setFontType('bold')
	doc.setFontSize(8);
	doc.text(156, 153, "N° Guía Área");

	//Fila13  col1
	doc.rect(25, 155, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(27, 158, "");
	//Fila 13 col2
	doc.rect(70, 155, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(71, 158, "");
	//Fila13 col3
	doc.rect(115, 155, 40, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(116, 158, "");
	//Fila 13 col4
	doc.rect(155, 155, 45, 5)
	doc.setFontType('normal')
	doc.setFontSize(8);
	doc.text(156, 158, "");

	//Fila 14 col1
	doc.rect(25, 163, 175, 8)
	doc.setFontType('bold')
	doc.setFontSize(11);
	doc.text(100, 167, "Observaciones");

	//Fila 14 col1
	doc.rect(25, 171, 175, 38)
	doc.setFontType('normal')
	doc.setFontSize(11);
	doc.text(100, 174, "");

	doc.save('IEcover_' + dataPO.nroPackingList + '.pdf');

}

function obligatorySave() {

	var response = false;

	var charge = $("#selectCharge").val();
	if (charge == 0 || charge == null) {
		Swal.fire('Warning', 'Chosse a Person in charge', 'warning')
		return response;
	}

	var operator = $("#selectOperator").val();
	if (operator == 0 || operator == null) {
		Swal.fire('Warning', 'Choose a Logistic Operator', 'warning')
		return response;
	}

	var packingList = $("#selectPackingList").val();
	if (packingList == 0 || packingList == null) {
		Swal.fire('Warning', 'Choose a PackingList', 'warning')
		return response;
	}

	var txtContact = $("#txtContact").val();
	if (txtContact == '' || txtContact == null) {
		Swal.fire('Warning', 'Enter a contact details', 'warning')
		return response;
	}

	var selectEntrance = $("#selectEntrance").val();
	if (selectEntrance == 0 || selectEntrance == null) {
		Swal.fire('Warning', 'Choose an entrance to terminal', 'warning')
		return response;
	}

	var selectShippingLine = $("#selectShippingLine").val();
	if (selectShippingLine == 0 || selectShippingLine == null) {
		Swal.fire('Warning', 'Choose a Shipping Line', 'warning')
		return response;
	}

	var txtBooking = $("#txtBooking").val();
	if (txtBooking == '' || txtBooking == null) {
		Swal.fire('Warning', 'Enter a Booking', 'warning')
		return response;
	}

	var txtVessel = $("#txtVessel").val();
	if (txtVessel == '' || txtVessel == null) {
		Swal.fire('Warning', 'Enter a Vessel', 'warning')
		return response;
	}

	var selectFreight = $("#selectFreight").val();
	if (selectFreight == 0 || selectFreight == null) {
		Swal.fire('Warning', 'Choose a Freight Condition', 'warning')
		return response;
	}

	var selectRegimen = $("#selectRegimen").val();
	if (selectRegimen == '' || selectRegimen == null) {
		Swal.fire('Warning', 'Choose a Regimen', 'warning')
		return response;
	}

	var selectTariff = $("#selectTariff").val();
	if (selectTariff == '' || selectTariff == null) {
		Swal.fire('Warning', 'Choose a Tariff Heading', 'warning')
		return response;
	}

	var selectPort = $("#selectPort").val();
	if (selectPort == 0 || selectPort == null) {
		Swal.fire('Warning', 'Choose a Port of Origin', 'warning')
		return response;
	}

	var selectDestinationPort = $("#selectDestinationPort").val();
	if (selectDestinationPort == 0 || selectDestinationPort == null) {
		Swal.fire('Warning', 'Choose a Destination Port', 'warning')
		return response;
	}

	var dateEtd = $(dateETD2).selectpicker('val')
	if ($(dateEtd).val() == 0 || dateEtd == null) {
		Swal.fire('Warning', 'Enter an ETD', 'warning')
		return response;
	}

	var dateEta = $(dateETA2).selectpicker('val')
	if ($(dateEta).val() == 0 || dateEta == null) {
		Swal.fire('Warning', 'Enter an ETA', 'warning')
		return response;
	}

	var selectShipper = $("#selectShipper").val();
	if (selectShipper == 0 || selectShipper == null) {
		Swal.fire('Warning', 'Choose a Shipper', 'warning')
		return response;
	}

	var selectInvoice = $("#selectInvoice").val();
	if (selectInvoice == 0 || selectInvoice == null) {
		Swal.fire('Warning', 'Choose a Customer', 'warning')
		return response;
	}

	var selectConsignee = $("#selectConsignee").val();
	if (selectConsignee == 0 || selectConsignee == null) {
		Swal.fire('Warning', 'Choose a Consignee', 'warning')
		return response;
	}

	var selectNotify = $("#selectNotify").val();
	if (selectNotify == 0 || selectNotify == null) {
		Swal.fire('Warning', 'Choose a Notifier', 'warning')
		return response;
	}

	var selectPlant = $("#selectPlant").val();
	if (selectPlant == 0 || selectPlant == null) {
		Swal.fire('Warning', 'Choose a Plant', 'warning')
		return response;
	}

	var selectBL = $("#selectBL").val();
	if (selectBL == 0 || selectBL == null) {
		Swal.fire('Warning', 'Choose a BL', 'warning')
		return response;
	}

	var txtVGM = $("#txtVGM").val();
	if (txtVGM == '' || txtVGM == null) {
		Swal.fire('Warning', 'Enter VGM', 'warning')
		return response;
	}

	var txtTemperature = $("#txtTemperature").val();
	if (txtTemperature == '' || txtTemperature == null) {
		Swal.fire('Warning', 'Enter a Temperature', 'warning')
		return response;
	}

	var txtVentilation = $("#txtVentilation").val();
	if (txtVentilation == '' || txtVentilation == null) {
		Swal.fire('Warning', 'Enter a Ventilation', 'warning')
		return response;
	}

	var dateArrivel = $(dateArrivel2).selectpicker('val')
	if ($(dateArrivel).val() == 0 || dateArrivel == null) {
		Swal.fire('Warning', 'Enter an Arrival Date to Plant', 'warning')
		return response;
	}

	var dateDeparture = $(dateDeparture2).selectpicker('val')
	if ($(dateDeparture).val() == 0 || dateDeparture == null) {
		Swal.fire('Warning', 'Enter a Departure Date', 'warning')
		return response;
	}

	var arrivalTime = $(arrivalTime2).selectpicker('val')
	if ($(arrivalTime).val() == 0 || arrivalTime == null) {
		Swal.fire('Warning', 'Enter an Arrival Time', 'warning')
		return response;
	}

	var dateTerminal = $(dateTerminal2).selectpicker('val')
	if ($(dateTerminal).val() == 0 || dateTerminal == null) {
		Swal.fire('Warning', 'Enter Arrival Date to Terminal', 'warning')
		return response;
	}

	return true;
}


function obligatory2() {

	var response = false;

	var fecha1 = ($("#dateETA2").val()).split("-");
	var year1 = fecha1[2];
	var mes1 = fecha1[1];
	var dia1 = fecha1[0];
	var g1 = new Date(year1, mes1, dia1);

	var fecha2 = ($("#dateETD2").val()).split("-");

	var year2 = fecha2[2];
	var mes2 = fecha2[1];
	var dia2 = fecha2[0];
	var g2 = new Date(year2, mes2, dia2);

	if (g1.getTime() === g2.getTime()) {
		Swal.fire('Warning', 'Iqual - Must choose E.T.A Date greater than Date E.T.D', 'warning')
		return response;
	}
	else if (g1.getTime() < g2.getTime()) {
		Swal.fire('Warning', 'Less - Date E.T.A must be greater than Date E.T.D', 'warning')
		return response;
	}

	response = true;
	return response;

}

function obligatory3() {

	var response = false;

	var fecha1 = ($("#dateArrivel2").val()).split("-");
	var year1 = fecha1[2];
	var mes1 = fecha1[1];
	var dia1 = fecha1[0];
	var g1 = new Date(year1, mes1, dia1);

	var fecha2 = ($("#dateETD2").val()).split("-");

	var year2 = fecha2[2];
	var mes2 = fecha2[1];
	var dia2 = fecha2[0];
	var g2 = new Date(year2, mes2, dia2);

	if (g1.getTime() === g2.getTime()) {
		Swal.fire('Warning', 'Iqual - Must choose E.T.D Date greater than Arrival Date', 'warning')
		return response;
	}
	else if (g2.getTime() < g1.getTime()) {
		Swal.fire('Warning', 'Less - Date E.T.D must be greater than Arrival Date', 'warning')
		return response;
	}

	response = true;
	return response;

}

function obligatory4() {

	var response = false;

	var fecha1 = ($("#dateDeparture2").val()).split("-");
	var year1 = fecha1[2];
	var mes1 = fecha1[1];
	var dia1 = fecha1[0];
	var g1 = new Date(year1, mes1, dia1);

	var fecha2 = ($("#dateETD2").val()).split("-");

	var year2 = fecha2[2];
	var mes2 = fecha2[1];
	var dia2 = fecha2[0];
	var g2 = new Date(year2, mes2, dia2);

	if (g1.getTime() === g2.getTime()) {
		Swal.fire('Warning', 'Iqual - Must choose E.T.D Date greater than Departure Date', 'warning')
		return response;
	}
	else if (g2.getTime() < g1.getTime()) {
		Swal.fire('Warning', 'Less - Date E.T.D must be greater than Departure Date', 'warning')
		return response;
	}

	response = true;
	return response;

}

function obligatory5() {

	var response = false;

	var fecha1 = ($("#dateTerminal2").val()).split("-");
	var year1 = fecha1[2];
	var mes1 = fecha1[1];
	var dia1 = fecha1[0];
	var g1 = new Date(year1, mes1, dia1);

	var fecha2 = ($("#dateETD2").val()).split("-");

	var year2 = fecha2[2];
	var mes2 = fecha2[1];
	var dia2 = fecha2[0];
	var g2 = new Date(year2, mes2, dia2);

	if (g1.getTime() === g2.getTime()) {
		Swal.fire('Warning', 'Iqual - Must choose E.T.D Date greater than Terminal Date', 'warning')
		return response;
	}
	else if (g2.getTime() < g1.getTime()) {
		Swal.fire('Warning', 'Less - Date E.T.D must be greater than Terminal Date', 'warning')
		return response;
	}

	response = true;
	return response;

}

function obligatory6() {

	var response = false;

	var initial1 = _initial.split("-");
	var year1 = initial1[2];
	var mes1 = initial1[1];
	var dia1 = initial1[0];
	var g1 = new Date(year1, mes1, dia1);

	var final2 = _final.split("-");
	var year2 = final2[2];
	var mes2 = final2[1];
	var dia2 = final2[0];
	var g2 = new Date(year2, mes2, dia2);

	var etd3 = ($("#dateETD2").val()).split("-");
	var year3 = etd3[2];
	var mes3 = etd3[1];
	var dia3 = etd3[0];
	var g3 = new Date(year3, mes3, dia3);

	if (g3.getTime() > g2.getTime()) {
		Swal.fire('Warning', 'ETD date must be within Week of Departure', 'warning')
		return response;
	}
	else if (g3.getTime() < g1.getTime()) {
		Swal.fire('Warning', 'ETD date must be within Week of Departure', 'warning')
		return response;
	}

	response = true;
	return response;

}

function SettingCustomizedMultiselect(obj) {
	var orderCount = 0;
	$(obj).multiselect({
		buttonWidth: '180px',
		includeSelectAllOption: false,
		nonSelectedText: 'Select expertise!',
		maxHeight: '1000px',
		dropUp: true,
		enableFiltering: true,
		onChange: function (option, checked) {
			if (checked) {
				orderCount++;
				$(option).data('order', orderCount);
			}
			else {
				$(option).data('order', '');
			}
		},
		buttonText: function (options) {
			if (options.length === 0) {
				return 'None selected';
			}
			else if (options.length > 3) {
				return options.length + ' selected';
			}
			else {
				var selected = [];
				options.each(function () {
					selected.push([$(this).text(), $(this).data('order')]);
				});

				selected.sort(function (a, b) {
					return a[1] - b[1];
				});

				var text = '';
				for (var i = 0; i < selected.length; i++) {
					text += selected[i][0] + ', ';
				}

				return text.substr(0, text.length - 2);
			}
		},
	});
}