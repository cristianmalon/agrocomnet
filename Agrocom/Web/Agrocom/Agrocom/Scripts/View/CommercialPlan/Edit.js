﻿var sessionUserID = $('#lblSessionUserID').text()
var idPlan = 0;
var idPlanOrigin = 0;
var dataWeeks = [];
var dataForecast = [];
var dataMartet = [];
var dataDestinationAll = [];
var dataOringinMarket = [];
var dataCustomerRequest = [];
var dataPlanAlternative = [];
var _dataPlanAlternative = [];
var dataPlan = [];
var idSeason = 0;
var idGrower = "";
var dataPlanAlternativeAprove = [];
var dataCodepackVia = [];
var dataCustomerMarket = [];
var dataDestinationCustomer = [];
var dataPriority = [];
var dataResumen =
{
    "A": [],
    "B": [],
    "C": [],
    "D": [],
    "E": [],
    "F": [],//FORECAST
    "O": [],//OFERTED
}

var dataFormatMarket = []

$(function () {

    var $contextMenu = $("#contextMenu");

    $("body").on("contextmenu", "table tr [contenteditable='true']", function (e) {
        $contextMenu.css({
            display: "block",
            left: e.pageX,
            top: e.pageY
        });
        debugger;
        return false;
    });

    $('html').click(function () {
        $contextMenu.hide();
    });

    $("#contextMenu li a").click(function (e) {
        var f = $(this);
        debugger;
    });

    load();

    /************** Eventos **************/
    //Btn Add Plan
    $("#btnAddPlanMarket").click(
        function () {
            addNewPlan();
        }
    )

    //Btn Aprove Plan
    $("#btnAprovePlan").click(
        function () {
            openAprove();
        }
    )

    $(document).on("change", "select.selectVia", function () {
        var control = $(this).parent().parent().find('select.selectFormat').attr('id');
        var marketID = $(this).parent().parent().parent().parent().find('label.lblIdMarket').text();

        $('#' + control).empty();
        //var data_filter = dataFormat.filter(element => element.viaID == $(this).val())
        //console.log(JSON.stringify(dataFormatMarket));
        var data_filter = dataFormatMarket.filter(element => element.viaID == $(this).val() && element.marketID.trim() == marketID)
        let data = data_filter.map(
            obj => {
                return {
                    "id": obj.codePackID,
                    "name": obj.description
                }
            }
        );
        //console.log(JSON.stringify(data));
        //console.log('#' + control);
        loadControlSelect('#' + control, data);
    });

    $(document).on("change", "select.selectCustomer", function () {
        var control = $(this).parent().parent().find('select.selectDestination').attr('id');
        var marketID = $(this).parent().parent().parent().parent().find('label.lblIdMarket').text();

        $('#' + control).empty();
        //console.log(marketID);
        var data_filter = dataDestinationCustomer.filter(element => element.customerID == $(this).val() && element.marketID.trim() == marketID.trim())

        let data = data_filter.map(
            obj => {
                return {
                    "id": obj.destinationID,
                    "name": obj.destination
                }
            }
        );
        //console.log(JSON.stringify(data));
        //console.log('#' + control);
        loadControlSelect('#' + control, data);
    });

    $('div.divReturn').click(function (e) {
        window.location.href = '/CommercialPlan/Index';
    });

    $(document).on("keyup", "#tablePlanMarket [contenteditable='true']", function (e) {
        var mount = $(e.target).text();
        if (isNormalInteger(mount)) {

        } else {
            $(e.target).text('0');
        }
        subTotalPercentCountry();
    });

    $(document).on("keyup", ".tblResumen  [contenteditable='true']", function (e) {
        var mount = $(e.target).text();
        if (isNormalInteger(mount)) {

        } else {
            $(e.target).text('0');
        }
        subTotalPercentCountry2();
    });

    $(document).on("keyup", ".tblClient [contenteditable='true']", function (e) {
        $currenTblId = $(e.target).parents('table').attr('id');

        var mount = parseFloat($(e.target).text());
        var boxContainer = parseInt($(e.target).parents('tr').children().eq(8).text());
        if (!Number.isInteger(mount / boxContainer) && mount != 0) {
            $(e.target).css('color', 'red');
        } else {
            $(e.target).removeAttr('style');
        }
        addSubTotalClient($currenTblId);
    });

    $(document).on("keyup", ".tblClient-promises [contenteditable='true']", function (e) {
        $currenTblId = $(e.target).parents('table').attr('id');

        var mount = parseFloat($(e.target).text());
        var boxContainer = parseInt($(e.target).parents('tr').children().eq(8).text());
        if (!Number.isInteger(mount / boxContainer) && mount != 0) {
            $(e.target).css('color', 'red');
        } else {
            $(e.target).removeAttr('style');
        }
        addSubTotalClient($currenTblId);
    });

    $(document).on("click", ".row-remove", function () {
        //console.log($(this).parents('table').attr('id'));
        $idClient = $(this).parents('tr').children().eq(1).text();
        //console.log($idClient);
        $idtable = $(this).parents('table').attr('id')
        if ($idtable.includes("tblClient-promise")) {
            //console.log('promise')
            deleteClientRequest($idClient);
        }
        else {
            //console.log('tblClient')
            deletePlanClient($idClient);
        }

        $(this).parents('tr').detach();
        addSubTotalClient($idtable);

    });

    $(document).on("click", ".row-down", function () {
        var $row = $(this).parents('tr');
        var verify = $row.next().attr('class');
        //console.log('down');
        //console.log('down');
        if (verify == 'text-align-right') {
            //if (typeof verify === 'undefined') {
            $row.next().after($row.get(0));
        }
    });

    $(document).on("click", ".row-up", function () {
        var $row = $(this).parents('tr');
        if ($row.index() === 0) return; // Don't go above the header
        $row.prev().before($row.get(0));
    });

    $(document).on("click", ".btnAddClient", function () {
        //alert(123);
        $marketId = $(this).parent().parent().parent().parent().find("label.lblIdMarket").text();
        $idPlanAlternative = $(this).parent().parent().parent().parent().parent().parent().find("label#lblPlanAlternativeID").text();
        $divId = $(this).parent().parent().parent().parent().parent().parent().attr("id");
        //console.log('------');
        //console.log($idPlanAlternative);
        //console.log($marketId);


        $idAlternative = $(this).attr('id').replace('btnAddClient-', '');
        $currenTblId = "tblClient-" + $idAlternative;

        $currentSelClientID = '#selectClient-' + $idAlternative;
        $clientText = $($currentSelClientID + " option:selected").text();
        $clientId = $($currentSelClientID).val();

        $currentPriorityID = '#selectPriority-' + $idAlternative;
        $priorityText = $($currentPriorityID + " option:selected").text();
        $priorityID = $($currentPriorityID).val();

        $currentFormatID = '#selectFormat-' + $idAlternative;
        $formatText = $($currentFormatID + " option:selected").text();
        $formatID = $($currentFormatID).val();

        $currentDestinationID = '#selectDestination-' + $idAlternative;
        $destinationText = $($currentDestinationID + " option:selected").text();
        $destinationID = $($currentDestinationID).val();

        $currentViaID = '#selectVia-' + $idAlternative;
        $viaText = $($currentViaID + " option:selected").text();
        $viaID = $($currentViaID).val();

        var planCustomerRequestID = 0
        var cropID = localStorage.cropID;
        if ($divId == 'divPromises') {
            planCustomerRequestID = saveplanCustomerRequestCreate(idPlan.trim(), $clientId, $destinationID, $formatID, $marketId, sessionUserID, cropID);
        } else {
            planCustomerRequestID = SaveCustomerFormat($formatID, $idPlanAlternative, $marketId, $destinationID, $clientId, $priorityID, 1, 1, sessionUserID);
        }


        if (planCustomerRequestID == 0) {
            return;
        }

        var row = '<tr class="text-align-right trCustomerFormat">';
        row += '<td "><i class="fas fa-trash row-remove"></i>	&nbsp;<i class="fas fa-arrow-up row-up"></i>	&nbsp;<i class="fas fa-arrow-down row-down"></i></td>';
        row += '<td style="display:none">' + planCustomerRequestID + '</td><td width="20%">' + $priorityText + '</td>';
        row += '<td style="display:none">' + $clientId + '</td><td >' + $clientText + '</td>';

        var codepackFilter = dataFormatMarket.filter(element => element.codePackID == $formatID && element.marketID.trim() == $marketId)[0];
        var countryFilter = dataDestinationAll.filter(element => element.destinationID == $destinationID)[0];

        row += '<td style="display:none">' + $formatID + '</td><td >' + $formatText + '</td><td >' + codepackFilter.weight + '</td>';

        if ($viaID == '1') {
            row += '<td >' + codepackFilter.boxPerContainer + '</td>';
        } else {
            row += '<td contenteditable="true">' + codepackFilter.boxesPerPallet + '</td>';
        }

        row += '<td style="display:none">' + $destinationID + '</td><td >' + $destinationText + '</td><td class="tdCountryId" style="display:none">' + countryFilter.originID + '</td>';
        row += '<td style="display:none">' + $viaID + '</td><td >' + $viaText + '</td>';
        for (var x = 0; x < dataWeeks.length; x++) {
            row += '<td  contenteditable="true">0';
            row += "</td>";
        }
        row += '</tr>';
        $("#" + $currenTblId + " tbody").append(row);

        addSubTotalClient($currenTblId);
    });

    $(document).on("focusout", ".tblClient-promises td", function () {
        $mount = $(this).text();
        $idCustomerWeek = $(this).parent().find('td:eq(1)').text();
        $index = $(this).index()
        $idMarket = $(this).parent().parent().parent().parent().find(".lblIdMarket").text();
        $idDiv = $(this).parent().parent().parent().parent().parent().parent().attr('id');

        $projectedWeekID = dataWeeks[$index - 14].projectedWeekID;
        $id = SavePlanCustomerRequestWeek(0, $idCustomerWeek, $projectedWeekID, $mount, 0);

    })

    $(document).on("focusout", ".tblClient td", function () {
        $mount = $(this).text();
        $idCustomerWeek = $(this).parent().find('td:eq(1)').text();
        $index = $(this).index()
        $idDiv = $(this).parent().parent().parent().parent().parent().parent().attr('id');

        $projectedWeekID = dataWeeks[$index - 14].projectedWeekID;
        $id = SavePlanCustomerWeek(0, $idCustomerWeek, $projectedWeekID, $mount, 0);

    })

    $(document).on("mouseover", ".tblClient [contenteditable='true']", function (e) {
        var weight = parseFloat($(this).parents('tr').children().eq(7).text());
        var box = parseFloat($(this).text());
        $(this).attr('title', (weight * box) + ' kg');
    });

    $(document).on("mouseover", ".tblClient-promises [contenteditable='true']", function (e) {
        var weight = parseFloat($(this).parents('tr').children().eq(7).text());
        var box = parseFloat($(this).text());
        $(this).attr('title', (weight * box) + ' kg');
    });

    $("#lblPlanId").click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#accordionMarket2").offset().top
        }, 2000);
    });

    $(".spanAlert").click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".trOferted").offset().top
        }, 2000);
    });

    $(".spanSalesAllocation").click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".navSalesAllocation").offset().top
        }, 2000);
    });

    $(".spanPreliminaryBID").click(function () {
        var el = $('div#divPreliminaryBID>div.card-header>a');
        el.removeClass('collapsed');
        el.addClass('collapsed');
        $(el).attr('aria-expanded', 'true')

        var el2 = $('div#divPreliminaryBID>div#accordionPromise');
        el2.removeClass('show');
        el2.addClass('show');

        $([document.documentElement, document.body]).animate({
            scrollTop: $("div#divPreliminaryBID").offset().top
        }, 2000);
    });

});


function isNormalInteger(str) {
    return /^\+?(0|[1-9]\d*)$/.test(str);
}
function deletePlanClient(id) {
    var planCustomerFormatID = 0
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/CommercialPlan/PlanCustomerDelete?planCustomerFormatID=" + id + "&userID=" + sessionUserID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planCustomerFormatID = dataJson[0].planCustomerFormatID

        }
    });
    return planCustomerFormatID;
}
function deleteClientRequest(id) {
    var planCustomerRequestID = 0
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/CommercialPlan/CustomerRequestDelete?planCustomerRequestID=" + id + "&userID=" + sessionUserID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planCustomerRequestID = dataJson[0].planCustomerRequestID

        }
    });
    return planCustomerRequestID;
}

function addSubTotalClientIni() {
    $("table.tblClient").each(function (i, table) {
        var idTable = $(table).attr('id');

        $('#' + idTable + ' tbody>tr').each(function (itr, tr) {
            var boxContainer = parseInt($(tr).children().eq(8).text());
            $(tr).find('td').each(function (itd, td) {
                if (itd >= 14) {
                    var mount = parseFloat($(td).text());
                    if (!Number.isInteger(mount / boxContainer) && mount != 0) {
                        $(td).css('color', 'red');
                    } else {
                        $(td).removeAttr('style');
                    }
                }
            })
        });


        addSubTotalClient(idTable);
    })
}
function addSubTotalClient(id) {
    $("#" + id + " tr.trSubtotal").remove();

    var sum = 0.0;
    var dataSubtTotal = [];

    $.each(dataWeeks, function (iWeeks, valWeek) {
        dataSubtTotal.push({ "id": iWeeks, "mount": 0 });
    });

    $('#' + id + ' > tbody > tr').each(function (index, element) {

        var iSpanCols = parseInt(14)

        $.each(dataWeeks, function (index2, value) {

            $valCell = $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text();
            $valWeight = parseFloat($(element).find('td:eq(7)').text());

            if ($.isNumeric($valCell)) {

            } else {
                $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text('0');
                $valCell = 0;
            }

            $.each(dataSubtTotal, function (i, v) {
                if (v.id == index2) {
                    v.mount += parseFloat($valCell) * $valWeight;

                }
            });
        });
    });

    var rowSubTotal = '<tr class="trSubtotal table-primary text-align-right"><td colspan=8  >Sub Total Kg (Box * Weight)&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    $.each(dataSubtTotal, function (i, v) {
        rowSubTotal += '<td>' + Math.round(v.mount) + '</td>';
    });
    rowSubTotal += '</tr>';
    $("#" + id + " tbody").append(rowSubTotal);

    var plan = ($("#" + id + "").parent().parent().parent().find('label.lblPlanAlternative').text());
    loadResumenByPlan(plan);
}

function saveplanCustomerRequestCreate(planID, customerID, destinationID, codepackID, marketID, userID, cropID) {
    var planCustomerRequestID = 0
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/CommercialPlan/SavePlanCustomerRequest?planCustomerRequestID=" + planCustomerRequestID
            + "&planCustomerRequest=" + "x" + "&planID=" + planID + "&customerID=" + customerID + "&destinationID=" + destinationID
            + "&codepackID=" + codepackID + "&marketID=" + marketID + "&userID=" + userID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planCustomerRequestID = dataJson[0].planCustomerRequestID

        }
    });
    return planCustomerRequestID;
}

function SaveCustomerFormat(codepackID, alternativeID, marketID, destinationID, customerID, priorityID, wildcard, order, userID) {
    var planCustomerFormatID = 0
    var uriSave = "/CommercialPlan/SaveCustomerFormat?codepackID=" + codepackID + "&alternativeID=" + alternativeID + "&marketID=" + marketID +
        "&destinationID=" + destinationID + "&customerID=" + customerID + "&priorityID=" + priorityID +
        "&wildcard=" + wildcard + "&order=" + order + "&userID=" + userID;
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: uriSave,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planCustomerFormatID = dataJson[0].planCustomerFormatID

        }
    });
    return planCustomerFormatID;
}

function SavePlanAlternative(planAlternativeID, planAlternative, planOriginID, userID) {
    var planAlternativeID = 0
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/CommercialPlan/SavePlanAlternative?planAlternativeID=" + planAlternativeID + "&planAlternative=" +
            planAlternative + "&planOriginID=" + planOriginID + "&userID=" + userID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planAlternativeID = dataJson[0].planAlternativeID

        }
    });
    return planAlternativeID;
}


function SavePlanCustomerRequestWeek(planCustomerRequestWeekID, planCustomerRequestID, projectedWeekID, amount, userID) {
    var planCustomerRequestWeekID = 0
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/CommercialPlan/SavePlanCustomerRequestWeek?planCustomerRequestWeekID=" + planCustomerRequestWeekID +
            "&planCustomerRequestID=" + planCustomerRequestID + "&projectedWeekID=" + projectedWeekID +
            "&amount=" + amount + "&userID=" + userID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planCustomerRequestWeekID = dataJson[0].planCustomerRequestWeekID

        }
    });
    return planCustomerRequestWeekID;
}

function SavePlanCustomerWeek(planCustomerWeekID, planCustomerFormatID, projectedWeekID, amount, userID) {
    var planCustomerWeekID = 0
    var uriSave = "/CommercialPlan/SavePlanCustomerWeek?planCustomerWeekID=" + planCustomerWeekID +
        "&planCustomerFormatID=" + planCustomerFormatID + "&projectedWeekID=" + projectedWeekID +
        "&amount=" + amount + "&userID=" + userID;
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: uriSave,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            planCustomerWeekID = dataJson[0].planCustomerWeekID

        }
    });
    return planCustomerWeekID;
}



function load() {
    idPlan = $("#lblPlanId").text();

    loadPlanData();
    idPlanOrigin = $("#lblOriginId").text();

    fnListWeekBySeason(idSeason, successListWeekBySeason);

    var val_priority = "";
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPriority?opt=all&value=" + val_priority,
        async: false,
        success: function (data) {
            dataPriority = JSON.parse(data);
        }
    });

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListForecast?idGrower=" + idGrower + "&idSeason=" + idSeason,
        async: false,
        success: function (data) {
            dataForecast = JSON.parse(data);
        }
    });

    var opt = 'all';
    var id = '';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListMarket?opt=" + opt + "&id=" + id,
        async: false,
        success: function (data) {
            dataMartet = JSON.parse(data);

        }
    });

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListCodepack?opt=" + opt + "&id=" + id,
        async: false,
        success: function (data) {
            dataFormat = JSON.parse(data);

        }
    });

    opt = 'cro';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListCodepackWithVia?opt=" + opt + "&cropID=" + localStorage.cropID,
        async: false,
        success: function (data) {
            dataFormatMarket = JSON.parse(data);

        }
    });

    opt = 'all';
    id = '';
    var mar = '';
    var via = '';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListDestinationAll?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
        async: false,
        success: function (data) {
            dataDestinationAll = JSON.parse(data);
        }
    });

    opt = 'wcu';
    id = '';
    mar = '';
    via = '';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListDestinationAllWithCustomer?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
        async: false,
        success: function (data) {
            dataDestinationCustomer = JSON.parse(data);
        }
    });

    opt = 'wmr';
    id = '';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListAllWithMarket?opt=" + opt + "&id=" + id,
        async: false,
        success: function (data) {
            dataCustomerMarket = JSON.parse(data);
        }
    });
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListOrigin",
        async: false,
        success: function (data) {
            dataOringinMarket = JSON.parse(data);

        }
    });

    opt = 'plan';
    id = idPlan;
    mar = '';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListCustomerRequestByPlanId?searchOpt=" + opt +
            "&planID=" + idPlan + "&marketID=" + mar,
        async: false,
        success: function (data) {
            dataCustomerRequest = JSON.parse(data);

        }
    });

    opt = 'oid';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanAlternative?searchOpt=" + opt + "&planOriginID=" + idPlanOrigin,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                dataPlanAlternative = JSON.parse(data);
            }

        }
    });
    loadTblForecast();
    loadResumenForecast();
    loadTablePromises();
    createListPlanMarketOrigin();

    loadDataPromises();
    $.each(dataPlanAlternative, function (iPlanAlternative, planAlternative) {
        loadDataPlanAlternative(planAlternative.planAlternative, planAlternative.planAlternativeID);
        loadDataClientPlanMarket(planAlternative.planAlternativeID)
    })

    addSubTotalClientIni();
}

var successListWeekBySeason = function (data) {
    dataWeeks = JSON.parse(data);
}

function loadResumenForecast() {
    var resForecast = [];
    var resForecastFlag = [];
    $.each(dataWeeks, function (iWeeks, valWeek) {
        resForecast.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
        resForecastFlag.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
    });

    dataResumen.A = resForecastFlag;
    dataResumen.B = resForecastFlag;
    dataResumen.C = resForecastFlag;
    dataResumen.D = resForecastFlag;
    dataResumen.E = resForecastFlag;

    $('table#tblForecast tbody tr').each(function (itr, tr) {
        var colIni = 5;
        $(tr).find('td').each(function (itd, td) {
            if (itd > colIni - 1) {
                resForecast[itd - colIni].mount += parseFloat($(td).text());
            }
        });
    });
    dataResumen.F = resForecast
}

function loadPlanData() {
    var opt = 'id';
    var id = idPlan;
    var cropId = '';
    var seasonId = '';
    var originId = '';
    var growerId = '';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanById?opt=" + opt + "&id=" + id + "&cropId=" + cropId +
            "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId,
        async: false,
        success: function (data) {
            if (data.length > 0) {

                var dataJson = JSON.parse(data);
                $('#txtCrop').val(dataJson[0].crop);
                $('#txtSeason').val(dataJson[0].season);
                $('#txtNonExport').val(dataJson[0].nonExport);
                $('#txtOrigin').val(dataJson[0].origin);
                $('#txtGrower').val(dataJson[0].grower);
                $('#txtPlan').val(dataJson[0].plan);
                $("#lblOriginId").text(dataJson[0].planOriginID)
                $('#txtUserName').val(dataJson[0].userName);
                $('#txtUserID').val(dataJson[0].userID);
                $('#txtLastUpdate').val(dataJson[0].dateLastModified);


                idSeason = dataJson[0].campaignID;
                idGrower = dataJson[0].growerID;
            }
        }
    });
}

function openAprove() {
    var dataweekaprov = [];

    dataweekaprov = dataWeeks.map(
        obj => {
            return {
                "id": obj.projectedWeekID,
                "name": obj.description
            }
        }
    );


    if (dataweekaprov.length == 0) { return; }

    var content = "";
    content += "<table id='tablaAprove' class='table table-bordered table-striped' table-responsive >";
    content += "<thead  bgcolor='#13213d' >";
    content += "<tr>";
    content += "<td style='max-width: 50px; min-width:50px;'><font color='#fff'>Plan</font></td>";
    content += "<td><font color='#fff'>WEEKINI</font></td>";
    content += "<td><font color='#fff'>WEEKEND</font></td>";
    content += "<td><font color='#fff'>STATUS</font></td>";
    content += "</tr>";
    content += "</thead>";
    content += "<tbody>";

    var opt = "apr";
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanAlternativeAprove?opt=" + opt + "&planOriginID=" + idPlanOrigin,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                _dataPlanAlternative = JSON.parse(data);
                var color = '';
                for (var i = 0; i < _dataPlanAlternative.length; i++) {
                    if (dataPlanAlternative[i]["statusID"] == 1) { color = 'badge-success'; }
                    else color = 'badge-warning';

                    content += "<tr>";
                    content += '<td style="text-align: center; font-size:13px;display:none" id="tdAlternativeID">' + dataPlanAlternative[i]["planAlternativeID"] + '</td>';
                    content += '<td style="max-width: 50px; min-width:50px;">';
                    content += dataPlanAlternative[i]["planAlternative"];
                    content += "</td>";
                    content += "<td style='text-align:center;'><select id='cboSemanaIni' class='form-control form-control-sm'>";
                    console.log(dataweekaprov);
                    $.each(dataweekaprov, function (indice, row) {
                        content += "<option value='" + row.id + "'>";
                        content += row.name;
                        content += "</option>";
                    });
                    content += "</select></td>"
                    content += "<td style='text-align:center;'><select id='cboSemanaFin' class='form-control form-control-sm'>";
                    $.each(dataweekaprov, function (indice, row) {
                        content += "<option value='" + row.id + "'>";
                        content += row.name;
                        content += "</option>";
                    });
                    content += "</select></td>"
                    content += '<td style="text-align: center; font-size:13px;display:none" id="tdStatusId">' + dataPlanAlternative[i]["statusID"] + '</td>';
                    content += "<td style='max-width: 100px; min-width:100px'><div class='badge " + color + " badge-pill'>" + _dataPlanAlternative[i]["status"] + "</div></td>"
                    content += "<td style='text-align:center;'>";
                    content += "<button class='btn btn-info btn-sm btn-sm' onclick='saveAProve(this)'>Aprove Plan <span class='icon text-white'><i class='fas fa-edit'></i></span></button> "
                    content += "</td>"
                    content += "</tr>";
                }
            }
        },
        complete: function () {
            $('#tablaAprove>tbody>tr').each(function (indice, row) {

                var id = parseInt($(row).closest('tr').find('#tdAlternativeID').text());

                if (_dataPlanAlternative.length > 0) {

                    $.each(_dataPlanAlternative, function (i, e) {
                        if (e.weekIni != -1) {
                            if (id == e.planAlternativeID) {
                                $(row).find('#cboSemanaIni').value = e.weekIni;
                            }
                        }
                    });
                }
            });
        }
    });

    content += "</tbody>";
    content += "</table>";
    document.getElementById("divAprove").innerHTML = content;
}

function saveAProve(xthis) {

    var weekinicio = $(xthis).closest('tr').find('#cboSemanaIni').val();
    var weekfin = $(xthis).closest('tr').find('#cboSemanaFin').val();
    var AlternativeID = $(xthis).closest('tr').find('#tdAlternativeID').text();
    var tdStatusId = $(xthis).closest('tr').find('#tdStatusId').val();

    var jsonObAprove = {}
    jsonObAprove = JSON.stringify({
        weekinicio: weekinicio,
        weekfin: weekfin,
        AlternativeID: AlternativeID,
        tdStatusId: tdStatusId
    });

    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/CommercialPlan/SavePlanAlternativeAprove",
        data: jsonObAprove,
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        //async: false,
        error: function (datoError) {
            sweet_alert_warning('Error!', 'Fail PO Updated.');
        },
        success: function (data) {
            sweet_alert_success('Good Job!', 'PO Saved.');

        },
        complete: function () {

        }
    });
}

function loadForecast() {

}

function loadTblForecast() {
    var table = '';
    table += "<table  id='tblForecast'  class='table-cebra'  >";
    table += '<thead >';
    table += "<tr>";
    table += "<th class='sticky' >Grower</th>";
    table += "<th class='sticky2'>Farm</th>";
    table += "<th style='display:none'>Brand</th>";
    table += "<th class='sticky3'>Variety</th>";
    table += "<th class='sticky4 '>Kg Total</th>";

    $.each(dataWeeks, function (iWeeks, valWeek) {
        table += '<th> Week' + valWeek.number + '</th>';
    });

    table += '</tr>'
    table += "</thead>";
    table += '<tbody>';
    $.each(dataForecast, function (i, forecast) {
        table += '<tr >';
        table += '<td class="sticky text-white"  style="background: #6C97A8; ">' + "OZBLU PERU" + '</td>';
        table += '<td class="sticky2 text-white"  style="background: #6C97A8">' + forecast.farm + '</td>';
        table += '<td style="display:none">' + forecast.Brand + '</strong></td>';
        table += '<td class="sticky3 text-white"  style="background: #6C97A8">' + forecast.variety + '</td>';
        table += '<td class="sticky4 text-align-right text-white" style="background: #6C97A8">' + Math.round(forecast.total) + '</td>';

        var rowBandera = '';
        var rowBandera2 = '';
        var mountTot = 0;
        var arrColumns = Object.keys(forecast);
        $.each(dataWeeks, function (iWeeks, valWeek) {
            var mount = 0;
            $.each(arrColumns, function (icol, column) {
                if (column == valWeek.projectedWeekID) {
                    mount = forecast[column];
                    if (mount == null) mount = 0;
                }
            })
            rowBandera += '<td class=" text-align-right">' + Math.round(parseFloat(mount)) + '</td>';
            rowBandera2 += '<td class=" text-align-right">' + (mount) + '</td>';
            mountTot += mount;
        });
        table += rowBandera;
        table += '</tr>';
    });

    table += '</tbody>';
    table += "</table>";
    $('#divForecast').html(table);
}

function loadTablePromises() {

    var conntentPromises = ''
    var id = 'promises';

    conntentPromises += '<ul class="nav nav-pills mb-3 nav" id="pills-promises" role="tablist">';
    for (var i = 0; i < dataMartet.length; i++) {
        var active = 'false';
        var active2 = '';
        if (i == 0) { active = 'true'; active2 = 'active' };
        conntentPromises += '<li class="nav-item">';
        conntentPromises += '<a class="nav-link ' + active2 + '" id="pills-tab' + i + '" data-toggle="pill" href="#pills-' + id + '-' + dataMartet[i].marketID + '" role="tab" aria-controls="pills-' + dataMartet[i].name + '" aria-selected="' + active + '">' + dataMartet[i].name + '</a>';
        conntentPromises += '</li>';
    }
    conntentPromises += '</ul>';

    conntentPromises += '<div class="tab-content" id="pills-tabContent-' + id + '">';

    for (var i = 0; i < dataMartet.length; i++) {

        var promisesId = id + '-' + dataMartet[i].marketID;

        var active = '';
        if (i == 0) active = 'show active';
        conntentPromises += '<div class="tab-pane fade ' + active + '" id="pills-' + promisesId + '" role="tabpanel" aria-labelledby="pills-home-tab" style="border-color: #666666; border: groove;">';

        conntentPromises += '<label for="lblIdMarket-' + promisesId + '" ><h6 class=" font-weight-bold" bgcolor="#13213d">Market ID:&nbsp;&nbsp;</h6></label>'
        conntentPromises += '<label class="lblIdMarket" id="lblIdMarket-' + promisesId + '"><h6 bgcolor="#13213d">' + dataMartet[i].marketID + '</h6></label>';

        conntentPromises += '<form><div class="form-row">'

        conntentPromises += '<div class="form-group-sm col-2">'
        conntentPromises += '<label for="selectClient-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6></label> <a href="/Maintenance/Customer" target="_blank"><i class="fas fa-user-plus"></i></a>'
        conntentPromises += '<select class="custom-select  mr-sm-2 selectCustomer" id="selectClient-' + promisesId + '"> </select>'
        conntentPromises += '</div>';
        conntentPromises += '<div class="form-group-sm col-2">'
        conntentPromises += '<label for="selectPriority-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Priority</h6></label>'
        conntentPromises += '<select class="custom-select  mr-sm-2" id="selectPriority-' + promisesId + '"></select>'
        conntentPromises += '</div>';

        conntentPromises += '<div class="form-group-sm col-2">'
        conntentPromises += '<label for="selectDestination-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Destination</h6></label>'
        conntentPromises += '<select class="custom-select  mr-sm-2 selectDestination" id="selectDestination-' + promisesId + '"> </select>'
        conntentPromises += '</div>';
        conntentPromises += '<div class="form-group-sm col-2">'
        conntentPromises += '<label for="selectVia-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Via</h6></label>'
        conntentPromises += '<select class="custom-select  mr-sm-2 selectVia" id="selectVia-' + promisesId + '"> <option value="1" selected>Sea</option><option value="2" >Air</option> </select>'

        conntentPromises += '</div>';
        conntentPromises += '<div class="form-group-sm col-2">'
        conntentPromises += '<label for="selectFormat-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Format</h6></label>'
        conntentPromises += '<select class="custom-select  mr-sm-2 selectFormat" id="selectFormat-' + promisesId + '"> </select>'
        conntentPromises += '</div>';
        conntentPromises += '<div class="form-group-sm col-2">'
        conntentPromises += '<br><br><button type="button" class="btn btn-info mb-1 btnAddClient btn-sm" id="btnAddClient-' + promisesId + '">Add <span class="icon text-white"><i class="fas fa-plus"></i></span></button>';
        conntentPromises += '</div>';

        conntentPromises += '</div></form>';


        var tableCliente = '';
        tableCliente += '<br><table style="width:100%" class ="table table-bordered table-responsive tblClient-promises table-striped" id="tblClient-' + promisesId + '"> ';
        tableCliente += '<thead class="table-thead-agrocom">';
        tableCliente += '<tr >';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdPrioridad</th>';
        tableCliente += '<th width="70%">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPrioridad&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdClient</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdCodepack</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFormat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th >Weight&nbspBox</th>';
        tableCliente += '<th >Box&nbspContainer/Pallet</th>';
        tableCliente += '<th style="display:none">IdDestino</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountryIdId&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdVia</th>';
        tableCliente += '<th >&nbsp&nbsp&nbspVia&nbsp&nbsp&nbsp</th>';
        for (var x = 0; x < dataWeeks.length; x++) {
            tableCliente += "<th> Week " + dataWeeks[x].number.toString();
            tableCliente += "</th>";
        }

        tableCliente += '</tr>';
        tableCliente += '</thead>';
        tableCliente += '<tbody>'
        tableCliente += '</tbody>'
        tableCliente += '</table>';


        conntentPromises += tableCliente;
        conntentPromises += '<br/>';


        conntentPromises += '' + promisesId + ' -' + dataMartet[i].marketID + '</div>';
    }

    conntentPromises += '</div>';

    $("#divPromises").append(conntentPromises);


    $.each(dataMartet, function (index, element) {
        let data = dataFormatMarket.filter(item => item.viaID == "1" && item.marketID.trim() == element.marketID.trim()).map(
            obj => {
                return {
                    "id": obj.codePackID,
                    "name": obj.description
                }
            }
        );


        loadControlSelect($('#selectFormat-promises-' + element.marketID), data);

        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == element.marketID.trim());
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );

        loadControlSelect($('#selectClient-promises-' + element.marketID), data);

        if ($dataCustomerFilter.length > 0) {
            $dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == $dataCustomerFilter[0].customerID && item.marketID.trim() == element.marketID.trim());
            data = $dataDestinationFilter.map(
                obj => {
                    return {
                        "id": obj.destinationID,
                        "name": obj.destination
                    }
                }
            );
            loadControlSelect($('#selectDestination-promises-' + element.marketID), data);
        }

        data = dataPriority.map(
            obj => {
                return {
                    "id": obj.priorityid,
                    "name": obj.description
                }
            }
        );

        loadControlSelect($('#selectPriority-promises-' + element.marketID), data);

    })
}

function loadDataPromises() {
    $.each(dataMartet, function (e, market) {
        var dataFilter = dataCustomerRequest.filter(row => row.marketID.trim() == market.marketID.trim())
        $.each(dataFilter, function (i, element) {
            var row = '<tr class="text-align-right trCustomerFormat">';
            row += '<td "><i class="fas fa-trash row-remove"></i>	&nbsp;<i class="fas fa-arrow-up row-up"></i>	&nbsp;<i class="fas fa-arrow-down row-down"></i></td>';
            row += '<td style="display:none">' + element.planCustomerRequestID + '</td><td width="20%">' + element.priority + '</td>';
            row += '<td style="display:none">' + element.customerID + '</td><td >' + element.customer + '</td>';

            row += '<td style="display:none">' + element.codePackID + '</td><td >' + element.codepack + '</td><td >' + element.weight + '</td>';

            if (element.viaID == '1') {
                row += '<td >' + element.boxPerContainer + '</td>';
            } else {
                row += '<td contenteditable="true">' + element.boxesPerPallet + '</td>';
            }
            row += '<td style="display:none">' + element.destinationID + '</td><td >' + element.destination + '</td><td class="tdCountryId" style="display:none">' + element.originID + '</td>';
            row += '<td style="display:none">' + element.viaID + '</td><td >' + element.via + '</td>';
            dataAmountByWeek = []
            dataAmountByWeek = ListCustomerRequestWeekByPlanCustomerId(element.planCustomerRequestID)
            $.each(dataWeeks, function (iweek, week) {
                row += '<td  contenteditable="true">';
                var amountFilter = dataAmountByWeek.filter(amount => amount.projectedWeekID == week.projectedWeekID)
                if (amountFilter.length > 0) {
                    row += amountFilter[0].amount;

                } else row += '0';

                row += "</td>";
            })
            row += '</tr>';
            $('#tblClient-promises-' + market.marketID.trim() + ' tbody').append(row);
        })
    })

}

function loadDataClientPlanMarket(planAlternativeID) {
    var searchOpt = 'pla';
    var dataCustomerByPlanAlternative = [];
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanCustomerByPlanAlternative?searchOpt=" + searchOpt + "&planAlternativeID=" + planAlternativeID,
        async: false,
        success: function (data) {
            dataCustomerByPlanAlternative = JSON.parse(data);

        }
    });

    $.each(dataMartet, function (e, market) {
        var dataFilter = dataCustomerByPlanAlternative.filter(row => row.marketID.trim() == market.marketID.trim())
        $.each(dataFilter, function (i, element) {
            var row = '<tr class="text-align-right trCustomerFormat">';
            row += '<td "><i class="fas fa-trash row-remove"></i>	&nbsp;<i class="fas fa-arrow-up row-up"></i>	&nbsp;<i class="fas fa-arrow-down row-down"></i></td>';
            row += '<td style="display:none">' + element.planCustomerFormatID + '</td><td width="20%">' + element.priority + '</td>';
            row += '<td style="display:none">' + element.customerID + '</td><td >' + element.customer + '</td>';

            row += '<td style="display:none">' + element.codePackID + '</td><td >' + element.codepack + '</td><td >' + element.weight + '</td>';

            if (element.viaID == '1') {
                row += '<td >' + element.boxPerContainer + '</td>';
            } else {
                row += '<td contenteditable="true">' + element.boxesPerPallet + '</td>';
            }
            row += '<td style="display:none">' + element.destinationID + '</td><td >' + element.destination + '</td><td class="tdCountryId" style="display:none">' + element.originID + '</td>';
            row += '<td style="display:none">' + element.viaID + '</td><td >' + element.via + '</td>';
            dataAmountByWeek = []
            dataAmountByWeek = ListCustomerWeekByPlanCustomerId(element.planCustomerFormatID)
            $.each(dataWeeks, function (iweek, week) {
                row += '<td  contenteditable="true">';
                var amountFilter = dataAmountByWeek.filter(amount => amount.projectedWeekID == week.projectedWeekID)
                if (amountFilter.length > 0) {
                    row += amountFilter[0].amount;

                } else row += '0';

                row += "</td>";
            })
            row += '</tr>';
            $('table#tblClient-' + planAlternativeID + '-' + market.marketID + ' tbody').append(row);
        })
    })

}

var dataAmountByWeek = []
function ListCustomerRequestWeekByPlanCustomerId(planCustomerRequestID) {
    var searchOpt = 'cid';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListCustomerRequestWeekByPlanCustomerId?searchOpt=" +
            searchOpt + "&planCustomerRequestID=" + planCustomerRequestID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                dataAmountByWeek = JSON.parse(data);
            } else dataAmountByWeek = []

        }
    });
    return dataAmountByWeek;
}

function ListCustomerWeekByPlanCustomerId(planCustomerFormatID) {
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanCustomerWeekByCustomer?planCustomerFormatID=" + planCustomerFormatID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                dataAmountByWeek = JSON.parse(data);
            } else dataAmountByWeek = []

        }
    });
    return dataAmountByWeek;
}

function loadControlSelect(control, data) {
    var contenido = "";
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].id + "'>";
        contenido += data[i].name;
        contenido += "</option>";
    }

    $(control).append(contenido);
}

function createListPlanMarketOrigin() {
    var tabletext = '';
    tabletext += '<table class="table table-bordered table-hover table-striped " ';
    tabletext += '  id="tablePlanMarket"';
    tabletext += ' > <thead class="table-thead-agrocom">';
    tabletext += '<trstyle="background-color: #13213d;" style="color:#fff" ><th rowspan="2">Market</th><th rowspan="2" style="display:none">Market Id</th><th rowspan="2">Country</th><th rowspan="2" style="display:none">Country ID</th><th colspan="5">Enter Percent By Country In The Plan</th></tr>';
    tabletext += '<tr class="trPlan" style="background: #6C97A8"></tr>'
    tabletext += '<tbody>';

    $.each(dataMartet, function (indexM, market) {
        var datafilter = dataOringinMarket.filter(element => element.marketID.trim() == market.marketID.trim());
        var background = '#f2f2f2';
        if (indexM % 2 == 0) {
            background = '#fff';
        }
        tabletext += '<tr><td style="background-color:' + background + ';vertical-align: middle;" rowspan=' + datafilter.length + '>' + market.name + '</td><td style="background-color:' + background + ' ;display:none" rowspan=' + datafilter.length + ' >' + market.marketID + '</td>';
        $.each(datafilter, function (index, origin) {
            if (index == 0) {
                tabletext += '<td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td style="display:none" class="tdCountryId">' + origin.originID + '</td>';
            }
            else {
                tabletext += '<tr><td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td style="display:none" class="tdCountryId">' + origin.originID + '</td>';
            }
        })
    });

    tabletext += '</tbody>'
    tabletext += '<tFoot><tr style="color:#fff; background-color: #5c7a99!important;font-weight: bold;" class="trSubTotalPercentCountry"><td colspan="2" class="text-align-right" >Summary</td></tr></tFoot>'

    document.getElementById('divTableMarket').innerHTML = tabletext;

}
function subTotalPercentCountry2() {

    $('table.tblResumen>tbody').each(function (iPlan, tbody) {

        var percentByContry = []
        $(tbody).find('tr').each(function (iTr, tr) {

            $(tr).find('td.tdCountryId').each(function (iTd, td) {
                var indexIni = 0
                if ($(tr).attr('class') == 'trMarket') {
                    indexIni = 8;
                } else {
                    indexIni = 5;
                }
                var mount = 0
                percentByContry.push({ "countryId": $(td).text(), "mount": 0 });
                for (var x = 0; x < dataWeeks.length; x++) {
                    percentByContry[iTr].mount = parseInt(percentByContry[iTr].mount) + parseInt($(td).parent().find('td:eq(' + (parseInt(x) + parseInt(indexIni)) + ')').text());
                }
            })

        })

        var total = 0;
        $.each(percentByContry, function (iEle, element) {
            total = total + element.mount
        })

        $(tbody).find('tr').each(function (iTr, tr) {
            $(tr).find('td.tdCountryId').each(function (iTd, td) {
                var percent = 0
                if (total > 0) {
                    percent = Math.round((parseInt(percentByContry[iTr].mount) * parseInt(100)) / total);
                }
                $(td).next().next().next().text(parseInt(percent));
            })
        })


        var SubTotalPreliminar = 0;
        var SubTotalAllocation = 0;
        $(tbody).find('tr').each(function (iTr, tr) {
            $(tr).find('td.tdCountryId').each(function (iTd, td) {
                SubTotalPreliminar = SubTotalPreliminar + parseInt($(td).next().next().text());
                SubTotalAllocation = SubTotalAllocation + parseInt($(td).next().next().next().text());
            })
        })

        $(tbody).find('td.SubTotalPreliminar').removeAttr('style');
        $(tbody).find('td.SubTotalPreliminar').text(SubTotalPreliminar);
        if (SubTotalPreliminar > 100) { $(tbody).find('td.SubTotalPreliminar').css('background-color', 'red'); }
        $(tbody).find('td.SubTotalAllocation').text(SubTotalAllocation);
    })
}
function subTotalPercentCountry() {
    var summary = [0, 0, 0, 0, 0]

    $('table#tablePlanMarket tbody tr').each(function (iTr, tr) {
        $(tr).find('td.tdNumber').each(function (iTd, td) {
            summary[iTd] = parseInt(summary[iTd]) + parseInt($(td).text());
        });
    })
    $('table#tablePlanMarket tfoot tr').each(function (iTr, tr) {
        $(tr).find('td.tdNumber').each(function (iTd, td) {
            $(td).removeAttr('style');
            $(td).text(summary[iTd]);
            if (summary[iTd] > 100) {
                $(td).css('background-color', 'red');
            }
        });
    })

    var countryPercentAll = [{ "data": [] }, { "data": [] }, { "data": [] }, { "data": [] }, { "data": [] }]
    $('table#tablePlanMarket tbody tr').each(function (iTr, tr) {

        $(tr).find('td.tdNumber').each(function (iTd, td) {
            var countryId = $(td).parent().find('td.tdCountryId').text();
            countryPercentAll[iTd].data.push({ "countryID": countryId, "percent": $(td).text() })

        })
    })

    $('table.tblResumen>tbody').each(function (iPlan, tbody) {
        var dataOnly = countryPercentAll[iPlan].data;
        $(tbody).find('tr').each(function (iTr, tr) {
            $(tr).find('td.tdCountryId').each(function (iTd, td) {
                var countryId = $(td).parent().find('td.tdCountryId').text();
                var mount = dataOnly.filter(element => element.countryID == countryId);
                if (mount.length > 0) {
                    $(td).next().next().text(mount[0].percent);
                }

            })

        })
        var percentByContry = []
        $(tbody).find('tr').each(function (iTr, tr) {

            $(tr).find('td.tdCountryId').each(function (iTd, td) {

                var indexIni = 0
                if ($(tr).attr('class') == 'trMarket') {
                    indexIni = 8;

                } else {
                    indexIni = 5;
                }
                var mount = 0
                percentByContry.push({ "countryId": $(td).text(), "mount": 0 });
                for (var x = 0; x < dataWeeks.length; x++) {
                    percentByContry[iTr].mount = parseInt(percentByContry[iTr].mount) + parseInt($(td).parent().find('td:eq(' + (parseInt(x) + parseInt(indexIni)) + ')').text());

                }

            })

        })
        var total = 0;
        $.each(percentByContry, function (iEle, element) {

            total = total + element.mount
        })

        $(tbody).find('tr').each(function (iTr, tr) {
            $(tr).find('td.tdCountryId').each(function (iTd, td) {
                var percent = 0
                if (total > 0) {
                    percent = (parseInt(percentByContry[iTr].mount) * parseInt(100)) / total;
                }

                $(td).next().next().next().text(parseInt(percent));
            })
        })
    })

}

function addNewPlan() {

    var rowsCount = $("#tablePlanMarket tbody tr:first td").length;
    if (rowsCount > 8) {
        alert("Only 5 plans allowed");
        return;
    }
    var plan = "";
    switch (rowsCount) {
        case 4:
            plan = "A";
            break;
        case 5:
            plan = "B";
            break;
        case 6:
            plan = "C";
            break;
        case 7:
            plan = "D";
            break;
        case 8:
            plan = "E";
            break;
        default:

    }
    var description = $("#txtNewPlanName").val();
    var planAlternativeID = SavePlanAlternative(0, plan, idPlanOrigin, 0);
    var arrPlanAlternativeIDMarketID = [];
    dataPlanAlternative.push({ "planAlternativeId": planAlternativeID, "planAlternative": plan.trim() })

    $.each(dataWeeks, function (i, week) {
        dataPlanAlternativeAprove.push({ "projectedWeekID": week.projectedWeekID, "description": week.description, "planAlternativeId": planAlternativeID, "planAlternative": plan.trim() })
    })

    $("#txtNewPlanName").val("");
    $("#tablePlanMarket tbody tr").append("<td class='tdNumber' contenteditable='true'>0</td>");
    $("#tablePlanMarket thead tr.trPlan").append("<td>" + plan + "</td>");
    $("#tablePlanMarket tFoot tr").append("<td class='tdNumber'>0</td>");

    $("#nav-tab a").length;
    var pill = "<a class='nav-item nav-link'";
    pill += "id='nav-tab-plan" + plan + "'";
    pill += "data-toggle='tab'";
    pill += "href='#nav-plan" + plan + "'";
    pill += "role='tab'";
    pill += "aria-controls='nav-contact'";

    if ($("#nav-tab a").length == 0) {
        pill += "aria-selected='true'>plan " + plan + "</a>";
    } else {
        pill += "aria-selected='false'>plan " + plan + "</a>";
    }

    var tableResumen = '<table class="table table-bordered table-responsive tblResumen table-striped table-hover" data-show-footer="true" data-show-export="true" data-sortable="true">';
    tableResumen += '<thead><tr class="text-white" style="background-color: #13213d;"><th style="display:none">planAlternativeID</th><th style="display:none" >IdMarket</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMarket&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountry&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th style="display:none">IdCountry</th><th style="display:none">planAlternativeID</th>';
    tableResumen += '<th style="background-color: #5c7a99;">% Preliminar</th><th style="background-color: #5c7a99;">%Projected Sales Allocation</th>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<th class='text-white' style='background-color: #13213d;'> Week&nbsp " + dataWeeks[x].number.toString();
        tableResumen += "</th>";
    }

    tableResumen += '</tr><thead>';
    tableResumen += '<tbody class="text-align-right">';

    $.each(dataMartet, function (indexM, market) {
        var datafilter = dataOringinMarket.filter(element => element.marketID.trim() == market.marketID.trim());
        var background = '#fff';
        if (indexM % 2 == 0) {
            background = '#f2f2f2';
        }
        tableResumen += '<tr class="trMarket" ><td style="background-color:' + background + '; display:none" rowspan=' + datafilter.length + '>' + planAlternativeID + '-' + market.marketID + '</td><td style="background-color:' + background + '; display:none" rowspan=' + datafilter.length + '>' + market.marketID + '</td><td style="background-color:' + background + ' ;vertical-align: middle" rowspan=' + datafilter.length + '>' + market.name + '</td>';


        $.each(datafilter, function (index, origin) {
            if (index == 0) {
                tableResumen += '<td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td class="tdCountryId" style="display:none">' + origin.originID + '</td><td class="tdPlanAlternativeID" style="display:none">' + planAlternativeID + '-' + market.marketID + '</td>';
            }
            else {
                tableResumen += '<tr><td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td class="tdCountryId" style="display:none">' + origin.originID + '</td><td class="tdPlanAlternativeID" style="display:none">' + planAlternativeID + '-' + market.marketID + '</td>';
            }
            for (var x = 0; x < 2; x++) {
                if (x == 0) tableResumen += "<td style='background-color:#13213d;color:aliceblue' contenteditable='true'>0";
                else tableResumen += "<td class='tdResumen' style='background-color:#13213d;color:aliceblue'>0";
                tableResumen += "</td>";
            }

            for (var x = 0; x < dataWeeks.length + 2; x++) {
                tableResumen += "<td class='tdResumen'>0";
                tableResumen += "</td>";
            }
            tableResumen += "</tr>";
        })
    });
    tableResumen += '<tr> <td colspan=2 style="background-color: #13213d;" >Total</td ><td class="SubTotalPreliminar">0</td><td class="SubTotalAllocation">0</td> </tr>';
    tableResumen += '<tr class="trOferted text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Projected Sales Allocation</td>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<td>0";
        tableResumen += "</td>";
    }
    tableResumen += "</tr>";
    tableResumen += '<tr class="bg-primary trForecast text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Harvest Forecast</td>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<td>0";
        tableResumen += "</td>";
    }
    tableResumen += "</tr>";
    tableResumen += '<tr class="bg-primary trBalance text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Diference (Kg)</td>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<td>0";
        tableResumen += "</td>";
    }
    tableResumen += "</tr>";
    tableResumen += '</tbody></table>';

    var contentByPlan = "<br/>";
    contentByPlan += tableResumen + "<br/>";
    contentByPlan += '<label style="display:none">Plan Alternative ID:</label>';
    contentByPlan += '<label id="lblPlanAlternativeID" style="display:none">' + planAlternativeID + '</label> <br/>';
    contentByPlan += '<label style="display:none">Plan Alternative:</label>';
    contentByPlan += '<label class="lblPlanAlternative" id="lblPlanAlternative" style="display:none"> ' + plan + '</label>';
    contentByPlan += '<ul class="nav nav-pills mb-3 navSalesAllocation" id="pills-tab" role="tablist">';
    for (var i = 0; i < dataMartet.length; i++) {
        var active = 'false';
        var active2 = '';
        if (i == 0) { active = 'true'; active2 = 'active' };
        contentByPlan += '<li class="nav-item">';
        contentByPlan += '<a class="nav-link ' + active2 + '" id="pills-tab' + i + '" data-toggle="pill" href="#pills-' + planAlternativeID + '-' + dataMartet[i].marketID + '" role="tab" aria-controls="pills-' + dataMartet[i].name + '" aria-selected="' + active + '">' + dataMartet[i].name + '</a>';
        contentByPlan += '</li>';
    }
    contentByPlan += '</ul>';

    contentByPlan += '<div class="tab-content" id="pills-tabContent-lblPlanAlternativeID' + planAlternativeID + '">';

    for (var i = 0; i < dataMartet.length; i++) {

        var planAlternativeIDMarketID = planAlternativeID + '-' + dataMartet[i].marketID;
        var itemPlanAlternativeIDMarketID = {
            "planAlternativeIDMarketID": planAlternativeIDMarketID, "planAlternativeID": planAlternativeID, "marketID": dataMartet[i].marketID
        };
        arrPlanAlternativeIDMarketID.push(itemPlanAlternativeIDMarketID);
        var active = '';
        if (i == 0) active = 'show active';
        contentByPlan += '<div class="tab-pane fade ' + active + '" id="pills-' + planAlternativeIDMarketID + '" role="tabpanel" aria-labelledby="pills-home-tab" style="border-color: #666666; border: groove;">';

        contentByPlan += '<label style="display:none" for="lblIdMarket-' + planAlternativeIDMarketID + '" >Market ID:</label>'
        contentByPlan += '<label style="display:none" class="lblIdMarket" id="lblIdMarket-' + planAlternativeIDMarketID + '">' + dataMartet[i].marketID + '</label>';

        contentByPlan += '<form><div class="form-row">'

        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectClient-' + planAlternativeIDMarketID + '">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <a href="/Maintenance/Customer" target="_blank"><i class="fas fa-user-plus"></i></a>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectCustomer" id="selectClient-' + planAlternativeIDMarketID + '"> </select>'
        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectPriority-' + planAlternativeIDMarketID + '">Priority</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2" id="selectPriority-' + planAlternativeIDMarketID + '"></select>'
        contentByPlan += '</div>';

        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectDestination-' + planAlternativeIDMarketID + '">Destination</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectDestination" id="selectDestination-' + planAlternativeIDMarketID + '"> </select>'
        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectVia-' + planAlternativeIDMarketID + '">Via</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectVia" id="selectVia-' + planAlternativeIDMarketID + '"> <option value="1" selected>Sea</option><option value="2" >Air</option> </select>'

        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectFormat-' + planAlternativeIDMarketID + '">Format</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectFormat" id="selectFormat-' + planAlternativeIDMarketID + '"> </select>'
        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<br /><button type="button" class="btn btn-primary btn-sm  mb-1 btnAddClient" id="btnAddClient-' + planAlternativeIDMarketID + '">Add <span class="text-white"><i class="fas fa-plus-circle"></i></span></button>';
        contentByPlan += '</div>';

        contentByPlan += '</div></form>';
        contentByPlan += '<hr> <div class="d-sm-flex align-items-center justify-content-between mb-4"><h1 class="h3 mb-0 text-gray-800" >Projected Sales Allocation</h1></div><hr>'


        var tableCliente = '';
        tableCliente += '<table style="width:100%" class ="table table-bordered table-responsive tblClient" id="tblClient-' + planAlternativeIDMarketID + '"> ';
        tableCliente += '<thead class="text-white" style ="background-color: #13213d;">';
        tableCliente += '<tr >';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdPrioridad</th>';
        tableCliente += '<th width="70%">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPrioridad&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdClient</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdCodepack</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFormat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th >Weight&nbspBox</th>';
        tableCliente += '<th >Box&nbspContainer/Pallet</th>';
        tableCliente += '<th style="display:none">IdDestino</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountryIdId&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdVia</th>';
        tableCliente += '<th >&nbsp&nbsp&nbspVia&nbsp&nbsp&nbsp</th>';
        for (var x = 0; x < dataWeeks.length; x++) {
            tableCliente += "<th> Week " + dataWeeks[x].number.toString();
            tableCliente += "</th>";
        }

        tableCliente += '</tr>';
        tableCliente += '</thead>';
        tableCliente += '<tbody>'
        tableCliente += '</tbody>'
        tableCliente += '</table>';

        contentByPlan += tableCliente;

        contentByPlan += '<div>';
        contentByPlan += '<h4><span class="badge badge-info spanPreliminaryBID" style="cursor:pointer">Preliminary BID vs </span><span class="badge badge-info spanSalesAllocation" style="cursor:pointer">Projected Sales Allocation</span></h4>'
        contentByPlan += '</div>';

        var tableComparative = '';
        tableComparative += '<table style="width:100%" class ="table table-bordered table-responsive" id="tblComparative-' + planAlternativeIDMarketID + '"> ';
        tableComparative += '<thead class="table-thead-agrocom">';
        tableComparative += '<tr >';
        tableComparative += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableComparative += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableComparative += '<th >&nbsp&nbsp&nbsp&nbspType&nbsp&nbsp&nbsp&nbsp</th>';

        for (var x = 0; x < dataWeeks.length; x++) {
            tableComparative += "<th> Week " + dataWeeks[x].number.toString();
            tableComparative += "</th>";
        }

        tableComparative += '</tr>';
        tableComparative += '</thead>';
        tableComparative += '<tbody>'
        tableComparative += '</tbody>'
        tableComparative += '</table>';

        contentByPlan += '<div style="border:dashed">';
        contentByPlan += tableComparative;
        contentByPlan += '</div>';

        contentByPlan += '</div>';

    }

    contentByPlan += '</div>';
    contentByPlan += '<span class="badge badge-danger spanAlert" style="cursor:pointer" id="span-' + plan + '"></span><br />'

    var pillContent = "";
    if ($("#nav-tab a").length == 0) {
        pillContent += "<div class='tab-pane fade show active'";
    } else {
        pillContent += "<div class='tab-pane fade'";
    }

    pillContent += "id='nav-plan" + plan + "'";
    pillContent += " role='tabpanel' ";
    pillContent += "aria-labelledby='nav-contact-tab' style='border-style: outset;'>";
    pillContent += contentByPlan;
    pillContent += "</div>";

    $("#nav-tab").append(pill);
    $("#nav-tabContent").append(pillContent);


    $.each(arrPlanAlternativeIDMarketID, function (index, element) {
        let data = dataFormatMarket.filter(item => item.viaID == "1" && item.marketID.trim() == element.marketID.trim()).map(
            obj => {
                return {
                    "id": obj.codePackID,
                    "name": obj.description
                }
            }
        );

        loadControlSelect($('#selectFormat-' + element.planAlternativeIDMarketID), data);

        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == element.marketID.trim());
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );

        loadControlSelect($('#selectClient-' + element.planAlternativeIDMarketID), data);

        if ($dataCustomerFilter.length > 0) {
            $dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == $dataCustomerFilter[0].customerID && item.marketID.trim() == element.marketID.trim());
            data = $dataDestinationFilter.map(
                obj => {
                    return {
                        "id": obj.destinationID,
                        "name": obj.destination
                    }
                }
            );
            loadControlSelect($('#selectDestination-' + element.planAlternativeIDMarketID), data);
        }

        data = dataPriority.map(
            obj => {
                return {
                    "id": obj.priorityid,
                    "name": obj.description
                }
            }
        );

        loadControlSelect($('#selectPriority-' + element.planAlternativeIDMarketID), data);
    });

    loadResumenByPlan(plan);
}

function loadDataPlanAlternative(plan, planAlternativeID) {

    var rowsCount = $("#tablePlanMarket tbody tr:first td").length;
    if (rowsCount > 8) {
        alert("Only 5 plans allowed");
        return;
    }
    var plan = "";
    switch (rowsCount) {
        case 4:
            plan = "A";
            break;
        case 5:
            plan = "B";
            break;
        case 6:
            plan = "C";
            break;
        case 7:
            plan = "D";
            break;
        case 8:
            plan = "E";
            break;
        default:

    }

    var description = $("#txtNewPlanName").val();

    var arrPlanAlternativeIDMarketID = [];
    dataPlanAlternative.push({ "planAlternativeId": planAlternativeID, "planAlternative": plan.trim() })

    $.each(dataWeeks, function (i, week) {
        dataPlanAlternativeAprove.push({ "projectedWeekID": week.projectedWeekID, "description": week.description, "planAlternativeId": planAlternativeID, "planAlternative": plan.trim() })
    })

    $("#txtNewPlanName").val("");
    $("#tablePlanMarket tbody tr").append("<td class='tdNumber' contenteditable='true'>0</td>");
    $("#tablePlanMarket thead tr.trPlan").append("<td>" + plan + "</td>");
    $("#tablePlanMarket tFoot tr").append("<td class='tdNumber'>0</td>");

    $("#nav-tab a").length;
    var pill = "<a class='nav-item nav-link'";
    pill += "id='nav-tab-plan" + plan + "'";
    pill += "data-toggle='tab'";
    pill += "href='#nav-plan" + plan + "'";
    pill += "role='tab'";
    pill += "aria-controls='nav-contact'";

    if ($("#nav-tab a").length == 0) {
        pill += "aria-selected='true'>plan " + plan + "</a>";
    } else {
        pill += "aria-selected='false'>plan " + plan + "</a>";
    }

    var tableResumen = '<table class="table table-bordered table-hovered table-responsive tblResumen table-striped" data-show-footer="true" data-show-export="true" data-sortable="true">';
    tableResumen += '<thead class="table-thead-agrocom"><tr  style="color:#fff"><th style="display:none">planAlternativeID</th><th style="display:none" >IdMarket</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMarket&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountry&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th style="display:none">IdCountry</th><th style="display:none">planAlternativeID</th>';
    tableResumen += '<th style="background-color: #5c7a99;">% Preliminar</th><th style="background-color: #5c7a99;">%Projected Sales Allocation</th>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<th> Week&nbsp " + dataWeeks[x].number.toString();
        tableResumen += "</th>";
    }

    tableResumen += '</tr><thead>';
    tableResumen += '<tbody class="text-align-right">';

    $.each(dataMartet, function (indexM, market) {
        var datafilter = dataOringinMarket.filter(element => element.marketID.trim() == market.marketID.trim());
        var background = '#fff';
        if (indexM % 2 == 0) {
            background = '#f2f2f2';
        }
        tableResumen += '<tr class="trMarket" ><td style="background-color:' + background + '; display:none" rowspan=' + datafilter.length + '>' + planAlternativeID + '-' + market.marketID + '</td><td style="background-color:' + background + '; display:none" rowspan=' + datafilter.length + ' class="tdCountryID">' + market.marketID + '</td><td style="background-color:' + background + ' ;vertical-align: middle" rowspan=' + datafilter.length + '>' + market.name + '</td>';


        $.each(datafilter, function (index, origin) {
            if (index == 0) {
                tableResumen += '<td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td class="tdCountryId" style="display:none">' + origin.originID + '</td><td class="tdPlanAlternativeID" style="display:none">' + planAlternativeID + '-' + market.marketID + '</td>';
            }
            else {
                tableResumen += '<tr><td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td class="tdCountryId" style="display:none">' + origin.originID + '</td><td class="tdPlanAlternativeID" style="display:none">' + planAlternativeID + '-' + market.marketID + '</td>';
            }
            for (var x = 0; x < 2; x++) {
                if (x == 0) tableResumen += "<td style='background-color:#13213d;color:aliceblue' contenteditable='true'>0";
                else tableResumen += "<td class='tdResumen' style='background-color:#13213d;color:aliceblue'>0";
                tableResumen += "</td>";
            }
            for (var x = 0; x < dataWeeks.length; x++) {
                tableResumen += "<td class='tdResumen'>0";
                tableResumen += "</td>";
            }
            tableResumen += "</tr>";
        })
    });
    tableResumen += '<tr> <td colspan=2 style="background-color: #13213d;" >Total</td><td class="SubTotalPreliminar">0</td><td class="SubTotalAllocation">0</td> </tr>';
    tableResumen += '<tr class="trOferted text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Projected Sales Allocation</td>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<td>0";
        tableResumen += "</td>";
    }
    tableResumen += "</tr>";
    tableResumen += '<tr class="bg-primary trForecast text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Harvest Forecast</td>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<td>0";
        tableResumen += "</td>";
    }
    tableResumen += "</tr>";
    tableResumen += '<tr class="bg-primary trBalance text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Diference (Kg)</td>';
    for (var x = 0; x < dataWeeks.length; x++) {
        tableResumen += "<td>0";
        tableResumen += "</td>";
    }
    tableResumen += "</tr>";
    tableResumen += '</tbody></table>';

    var contentByPlan = "<br/>";
    contentByPlan += tableResumen + "<br/>";
    contentByPlan += '<label style="display:none">Plan Alternative ID:</label>';
    contentByPlan += '<label id="lblPlanAlternativeID" style="display:none">' + planAlternativeID + '</label> <br/>';
    contentByPlan += '<label style="display:none">Plan Alternative:</label>';
    contentByPlan += '<label class="lblPlanAlternative" id="lblPlanAlternative" style="display:none"> ' + plan + '</label>';

    contentByPlan += '<ul class="nav nav-pills mb-3 navSalesAllocation" id="pills-tab" role="tablist">';
    for (var i = 0; i < dataMartet.length; i++) {
        var active = 'false';
        var active2 = '';
        if (i == 0) { active = 'true'; active2 = 'active' };
        contentByPlan += '<li class="nav-item">';
        contentByPlan += '<a class="nav-link ' + active2 + '" id="pills-tab' + i + '" data-toggle="pill" href="#pills-' + planAlternativeID + '-' + dataMartet[i].marketID + '" role="tab" aria-controls="pills-' + dataMartet[i].name + '" aria-selected="' + active + '">' + dataMartet[i].name + '</a>';
        contentByPlan += '</li>';
    }
    contentByPlan += '</ul>';

    contentByPlan += '<div class="tab-content" id="pills-tabContent-lblPlanAlternativeID' + planAlternativeID + '">';

    for (var i = 0; i < dataMartet.length; i++) {

        var planAlternativeIDMarketID = planAlternativeID + '-' + dataMartet[i].marketID;
        var itemPlanAlternativeIDMarketID = {
            "planAlternativeIDMarketID": planAlternativeIDMarketID, "planAlternativeID": planAlternativeID, "marketID": dataMartet[i].marketID
        };
        arrPlanAlternativeIDMarketID.push(itemPlanAlternativeIDMarketID);
        var active = '';
        if (i == 0) active = 'show active';
        contentByPlan += '<div class="tab-pane fade ' + active + '" id="pills-' + planAlternativeIDMarketID + '" role="tabpanel" aria-labelledby="pills-home-tab" style="border-color: #666666; border: groove;">';

        contentByPlan += '<label style="display:none" for="lblIdMarket-' + planAlternativeIDMarketID + '" >Market ID:</label>'
        contentByPlan += '<label style="display:none" class="lblIdMarket" id="lblIdMarket-' + planAlternativeIDMarketID + '">' + dataMartet[i].marketID + '</label>';

        contentByPlan += '<form><div class="form-row">'

        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectClient-' + planAlternativeIDMarketID + '">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <a href="/Maintenance/Customer" target="_blank"><i class="fas fa-user-plus"></i></a>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectCustomer" id="selectClient-' + planAlternativeIDMarketID + '"> </select>'
        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectPriority-' + planAlternativeIDMarketID + '">Priority</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2" id="selectPriority-' + planAlternativeIDMarketID + '"></select>'
        contentByPlan += '</div>';

        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectDestination-' + planAlternativeIDMarketID + '">Destination</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectDestination" id="selectDestination-' + planAlternativeIDMarketID + '"> </select>'
        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectVia-' + planAlternativeIDMarketID + '">Via</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectVia" id="selectVia-' + planAlternativeIDMarketID + '"> <option value="1" selected>Sea</option><option value="2" >Air</option> </select>'

        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<label for="selectFormat-' + planAlternativeIDMarketID + '">Format</label>'
        contentByPlan += '<select class="custom-select  mr-sm-2 selectFormat" id="selectFormat-' + planAlternativeIDMarketID + '"> </select>'
        contentByPlan += '</div>';
        contentByPlan += '<div class="form-group-sm col-2">'
        contentByPlan += '<br /><button type="button" class="btn btn-primary btn-sm  mb-1 btnAddClient" id="btnAddClient-' + planAlternativeIDMarketID + '">Add <span class="text-white"><i class="fas fa-plus-circle"></i></span></button>';
        contentByPlan += '</div>';

        contentByPlan += '</div></form>';
        contentByPlan += '<h4><span class="badge badge-info">Projected Sales Allocation</span></h4>';

        var tableCliente = '';
        tableCliente += '<table style="width:100%" class ="table table-bordered table-responsive tblClient" id="tblClient-' + planAlternativeIDMarketID + '"> ';
        tableCliente += '<thead class="table-thead-agrocom">';
        tableCliente += '<tr >';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdPrioridad</th>';
        tableCliente += '<th width="70%">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPrioridad&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdClient</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdCodepack</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFormat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th >Weight&nbspBox</th>';
        tableCliente += '<th >Box&nbspContainer/Pallet</th>';
        tableCliente += '<th style="display:none">IdDestino</th>';
        tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountryIdId&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableCliente += '<th style="display:none">IdVia</th>';
        tableCliente += '<th >&nbsp&nbsp&nbspVia&nbsp&nbsp&nbsp</th>';
        for (var x = 0; x < dataWeeks.length; x++) {
            tableCliente += "<th> Week " + dataWeeks[x].number.toString();
            tableCliente += "</th>";
        }

        tableCliente += '</tr>';
        tableCliente += '</thead>';
        tableCliente += '<tbody>'
        tableCliente += '</tbody>'
        tableCliente += '</table>';

        contentByPlan += tableCliente;

        contentByPlan += '<div>';
        contentByPlan += '<h4><span class="badge badge-info spanPreliminaryBID" style="cursor:pointer">Preliminary BID vs </span><span class="badge badge-info spanSalesAllocation" style="cursor:pointer">Projected Sales Allocation</span></h4>'

        contentByPlan += '</div>';

        var tableComparative = '';
        tableComparative += '<table style="width:100%" class ="table table-bordered table-responsive" id="tblComparative-' + planAlternativeIDMarketID + '"> ';
        tableComparative += '<thead class="table-thead-agrocom">';
        tableComparative += '<tr >';
        tableComparative += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableComparative += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
        tableComparative += '<th >&nbsp&nbsp&nbsp&nbspType&nbsp&nbsp&nbsp&nbsp</th>';

        for (var x = 0; x < dataWeeks.length; x++) {
            tableComparative += "<th> Week " + dataWeeks[x].number.toString();
            tableComparative += "</th>";
        }

        tableComparative += '</tr>';
        tableComparative += '</thead>';
        tableComparative += '<tbody>'
        tableComparative += '</tbody>'
        tableComparative += '</table>';

        contentByPlan += '<div style="border:dashed">';
        contentByPlan += tableComparative;
        contentByPlan += '</div>';

        contentByPlan += '</div>';

    }

    contentByPlan += '</div>';
    contentByPlan += '<span class="badge badge-danger spanAlert" style="cursor:pointer" id="span-' + plan + '"></span><br />'

    var pillContent = "";
    if ($("#nav-tab a").length == 0) {
        pillContent += "<div class='tab-pane fade show active'";
    } else {
        pillContent += "<div class='tab-pane fade'";
    }

    pillContent += "id='nav-plan" + plan + "'";
    pillContent += " role='tabpanel' ";
    pillContent += "aria-labelledby='nav-contact-tab' style='border-style: outset;'>";
    pillContent += contentByPlan;
    pillContent += "</div>";

    $("#nav-tab").append(pill);
    $("#nav-tabContent").append(pillContent);


    $.each(arrPlanAlternativeIDMarketID, function (index, element) {
        let data = dataFormatMarket.filter(item => item.viaID == "1" && item.marketID.trim() == element.marketID.trim()).map(
            obj => {
                return {
                    "id": obj.codePackID,
                    "name": obj.description
                }
            }
        );
        loadControlSelect($('#selectFormat-' + element.planAlternativeIDMarketID), data);

        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == element.marketID.trim());
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );

        loadControlSelect($('#selectClient-' + element.planAlternativeIDMarketID), data);

        if ($dataCustomerFilter.length > 0) {
            $dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == $dataCustomerFilter[0].customerID && item.marketID.trim() == element.marketID.trim());
            data = $dataDestinationFilter.map(
                obj => {
                    return {
                        "id": obj.destinationID,
                        "name": obj.destination
                    }
                }
            );
            loadControlSelect($('#selectDestination-' + element.planAlternativeIDMarketID), data);
        }


        data = dataPriority.map(
            obj => {
                return {
                    "id": obj.priorityid,
                    "name": obj.description
                }
            }
        );

        loadControlSelect($('#selectPriority-' + element.planAlternativeIDMarketID), data);

    });

    loadResumenByPlan(plan);
}

function loadResumenByPlan(idPlan) {
    idPlan = idPlan.trim();
    $("#nav-plan" + idPlan + " table.tblResumen td.tdResumen").each(function (itd, td) {
        $(td).text("0");
    });
    $("#nav-plan" + idPlan.trim() + " table.tblResumen tbody td.tdPlanAlternativeID").each(function (itd, td) {
        var planAlternativeId = $(td).text();
        var countryId = $(td).parent().find('td.tdCountryId').text();
        var tr = $(td).parent();

        $("#tblClient-" + planAlternativeId + " tbody>tr>td.tdCountryId").each(function (itdchild, tdchild) {
            if ($(tdchild).text().trim() == countryId.trim()) {
                $.each(dataWeeks, function (iweek, week) {
                    var valOrigin = parseFloat($(tdchild).parent().find('td:eq(' + (parseInt(iweek) + parseInt(14)) + ')').text()) * parseFloat($(tdchild).parent().find('td:eq(7)').text());
                    if ($(tr).attr('class') == 'trMarket') {
                        var varOld = $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(8)) + ')').text();
                        var valNew = parseFloat(valOrigin) + parseInt(varOld);
                        $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(8)) + ')').text(Math.round(valNew));
                    } else {
                        var varOld = $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(5)) + ')').text();
                        var valNew = parseFloat(valOrigin) + parseInt(varOld);
                        $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(5)) + ')').text(Math.round(valNew));
                    }
                })
            }
        })

    });
    $.each(dataWeeks, function (iweek, week) {
        var sum = 0
        $("#nav-plan" + idPlan + " tbody tr.trSubtotal").each(function (itr, tr) {
            sum += parseFloat($(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text());


        });
        $("#nav-plan" + idPlan + " tbody tr.trOferted ").each(function (itr, tr) {
            $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(Math.round(sum));
        });

    });

    $("#nav-plan" + idPlan + " table.tblResumen tbody tr.trForecast").each(function (itr, tr) {
        $.each(dataResumen.F, function (iweek, week) {
            $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(Math.round(week.mount));
        })
    });

    var resOferted = [];
    var resBalance = [];
    $.each(dataWeeks, function (iWeeks, valWeek) {
        resOferted.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
        resBalance.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
    });

    $("#nav-plan" + idPlan + " table.tblResumen tbody tr.trBalance").each(function (itr, tr) {
        $.each(dataResumen.F, function (iweek, week) {
            $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(Math.round(week.mount));
        })
    });

    var sum = 0;
    $('#span-' + idPlan).text('');
    $.each(dataWeeks, function (iweek, week) {
        $("#nav-plan" + idPlan + " tbody tr.trForecast").each(function (itr, tr) {
            sum += parseFloat($(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text());
        });
        $("#nav-plan" + idPlan + " tbody tr.trOferted").each(function (itr, tr) {
            sum -= parseFloat($(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text());
        });


        $("#nav-plan" + idPlan + " tbody tr.trBalance").each(function (itr, tr) {
            $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(sum);
            if (sum < 0) {
                $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').css('background-color', 'red');
                $('#span-' + idPlan).text('Error: check the Diference');

            }
            else {
                $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').removeAttr('style');

            }
        });
    });

    loadComparative();
}

function loadComparative() {
    var dataCustomerPromises = [];
    var dataCustomerPromisesAll = []

    $.each(dataMartet, function (iMarket, market) {
        $('#divPromises table#tblClient-promises-' + market.marketID + ' tbody tr.trCustomerFormat').each(function (itr, td) {
            var customerID = $(td).find('td:eq(3)').text();
            var customer = $(td).find('td:eq(4)').text();
            var destinationID = $(td).find('td:eq(9)').text();
            var destination = $(td).find('td:eq(10)').text();

            var customerFormat = { "marketID": market.marketID, "customerID": customerID, "customer": customer, "destinationID": destinationID, "destination": destination, "mountPromises": [], "mountOferted": [], "mountBalance": [] }
            $.each(dataWeeks, function (iWeeks, valWeek) {
                customerFormat.mountPromises.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
                customerFormat.mountOferted.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
                customerFormat.mountBalance.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
            });

            dataCustomerPromisesFilter = dataCustomerPromisesAll.filter(element => element.customerID == customerFormat.customerID && element.destinationID == customerFormat.destinationID)
            if (dataCustomerPromisesFilter.length > 0) {

            } else {
                dataCustomerPromisesAll.push(customerFormat);
            }
        });
        $('#divPromises table#tblClient-promises-' + market.marketID + ' tbody tr.trCustomerFormat').each(function (itr, td) {
            var customerID = $(td).find('td:eq(3)').text();
            var customer = $(td).find('td:eq(4)').text();
            var destinationID = $(td).find('td:eq(9)').text();
            var destination = $(td).find('td:eq(10)').text();
            var weightPerBox = $(td).find('td:eq(7)').text();

            var customerFormat = { "marketID": market.marketID, "customerID": customerID, "customer": customer, "destinationID": destinationID, "destination": destination, "mountPromises": [], "mountOferted": [], "mountBalance": [] }

            $.each(dataCustomerPromisesAll, function (i, element) {
                if (element.customerID == customerFormat.customerID && element.destinationID == customerFormat.destinationID) {
                    $.each(dataWeeks, function (iWeeks, valWeek) {
                        $.each(element.mountPromises, function (iChild, child) {
                            if (child.projectedWeekID == valWeek.projectedWeekID) {
                                var box = $(td).find('td:eq(' + (parseInt(14) + parseInt(iChild)) + ')').text();
                                child.mount += parseFloat(box) * parseFloat(weightPerBox);
                            }
                        });

                    });
                }
            });
        });
    });
    $.each(dataPlanAlternative, function (idataPlanAlternative, dataPlanAlternative) {


        $.each(dataMartet, function (iMarket, market) {
            var dataCustomerOferted = dataCustomerPromisesAll.filter(element => element.marketID == market.marketID);
            $('table#tblClient-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody>tr.trCustomerFormat').each(function (itr, td) {
                var customerID = $(td).find('td:eq(3)').text();
                var customer = $(td).find('td:eq(4)').text();
                var destinationID = $(td).find('td:eq(9)').text();
                var destination = $(td).find('td:eq(10)').text();
                var weightPerBox = $(td).find('td:eq(7)').text();
                var customerFormat = { "marketID": market.marketID, "customerID": customerID, "customer": customer, "destinationID": destinationID, "destination": destination, "mountPromises": [], "mountOferted": [], "mountBalance": [] }

                $.each(dataCustomerOferted, function (i, element) {
                    if (element.customerID == customerFormat.customerID && element.destinationID == customerFormat.destinationID) {
                        $.each(dataWeeks, function (iWeeks, valWeek) {
                            $.each(element.mountOferted, function (iChild, child) {
                                if (child.projectedWeekID == valWeek.projectedWeekID) {
                                    var box = $(td).find('td:eq(' + (parseInt(14) + parseInt(iChild)) + ')').text();
                                    child.mount += parseFloat(box) * parseFloat(weightPerBox);

                                }
                            });

                        });
                    }
                });
            });

            $.each(dataWeeks, function (iWeeks, valWeek) {

                $.each(dataCustomerOferted, function (i, element) {
                    var acum = 0;
                    $.each(element.mountBalance, function (iChild, child) {
                        if (child.projectedWeekID == valWeek.projectedWeekID) {
                            var promises = element.mountPromises.filter(e => e.projectedWeekID == valWeek.projectedWeekID)[0].mount;
                            var oferted = element.mountOferted.filter(e => e.projectedWeekID == valWeek.projectedWeekID)[0].mount;

                            acum += parseFloat(promises) - parseFloat(oferted);
                            child.mount = acum

                        }

                    });

                });
            });

            $('table#tblComparative-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody').html("");
            $.each(dataCustomerOferted, function (i, element) {
                var tr = '<tr>';
                tr += '<td rowspan=3>' + element.customer + '</td>';
                tr += '<td rowspan=3>' + element.destination + '</td>';
                tr += '<td>Preliminary BID</td>'
                $.each(element.mountPromises, function (iChild, child) {

                    $.each(dataWeeks, function (iWeeks, valWeek) {
                        if (child.projectedWeekID == valWeek.projectedWeekID) {
                            tr += '<td>' + child.mount + '</td>';
                        }
                    });
                });
                tr += '</tr>';
                tr += '<tr>';
                tr += '<td>Projected Sales Allocation</td>'
                $.each(element.mountOferted, function (iChild, child) {

                    $.each(dataWeeks, function (iWeeks, valWeek) {
                        if (child.projectedWeekID == valWeek.projectedWeekID) {
                            tr += '<td>' + child.mount + '</td>';
                        }
                    });
                });
                tr += '</tr>';
                tr += '<tr>';
                tr += '<td>Diference</td>'

                var acum = 0;
                $.each(element.mountBalance, function (iChild, child) {

                    $.each(dataWeeks, function (iWeeks, valWeek) {
                        if (child.projectedWeekID == valWeek.projectedWeekID) {
                            acum = child.mount
                            tr += '<td>' + acum + '</td>';
                        }
                    });
                });
                tr += '</tr>';

                $('table#tblComparative-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody').append(tr);
            });

        });
    });
    subTotalPercentCountry2();
}
