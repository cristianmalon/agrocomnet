﻿var dataCrop = [];
var dataSeason = [];
var dataOrigin = [];
var dataGrower = [];
var sessionUserID = $('#lblSessionUserID').text()

$(function () {

    fnCommercialPlan(localStorage.cropID, successCommercialPlan);

    fnListSeason(successListSeason);

    fnListOriginWithGrower(successListOriginWithGrower);

    fnListGrower($(lblSessionUserID).text(), localStorage.cropID, successListGrower);

    $('#selectCrop').change(function () {
        var data = dataSeason.filter(element => element.cropId == this.value)
        loadCombo(data, 'selectSeason', true);
    });

    $('#selectOrigin').change(function () {
        var data = dataGrower.filter(element => element.originId == this.value)
        loadCombo(data, 'selectGrower', true);
    });

    cleanControls();
    list();

    $('#btnSave').click(function (e) {
        save();
    });
});

var successCommercialPlan = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataCrop = dataJson.map(
            obj => {
                return {
                    "id": obj.cropid,
                    "name": obj.description
                }
            }
        );
    }
}

var successListSeason = function (data) {

    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataSeason = dataJson.map(
            obj => {
                return {
                    "id": obj.campaignID,
                    "name": obj.description,
                    "cropId": obj.crop
                }
            }
        );
    }
}

var successListOriginWithGrower = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataOrigin = dataJson.map(
            obj => {
                return {
                    "id": obj.originID,
                    "name": obj.description,
                    "cropId": obj.crop
                }
            }
        );
    }
}

var successListGrower = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataGrower = dataJson.map(
            obj => {
                return {
                    "id": obj.growerID,
                    "name": obj.businessName,
                    "originId": obj.originID
                }
            }
        );
    }
}

function save() {
    var cropId = $("#selectCrop option:selected").val()
    var seasonId = $("#selectSeason option:selected").val()
    var originId = $("#selectOrigin option:selected").val()
    var growerId = $("#selectGrower option:selected").val()
    var plan = $("#txtPlan").val()
    var planId = $("#lblPlanId").text()
    var nonExport = $("#inputNonExport").val()

    //identificamos que sea el tercer caracter
    var v_porcentaje = nonExport.indexOf(".");
    if (!obligatory()) {
        return
    }

    if (v_porcentaje == "-1") {
        if (nonExport.length > 2) {
            Swal.fire('Warning', 'input a valid percentage', 'warning')
            return;
        }
    } else if (v_porcentaje == "3") {
        Swal.fire('Warning', 'input a valid percentage', 'warning')
        return;
    }

    var opt = 'val';
    var id = '';
    var api = "/CommercialPlan/GetIfExists?opt=" + opt + "&id=" + id + "&cropId=" + cropId + "&seasonId=" + seasonId + "&originId=" + originId + "&growerId=" + growerId
    var count = 0;
    if (parseInt(planId) == 0) {
        count = 1
        $.ajax({
            type: "GET",
            url: api,
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    count = dataJson[0].count;

                }
            }
        });
    }

    var savePlan = false;
    if (count == 0) {
        api = "/CommercialPlan/SavePlanOriginGrower?planID=" + planId + "&plan=" + plan + "&campaignID=" + seasonId + "&originID=" + originId + "&growerID=" + growerId + "&nonExport=" + nonExport + "&userID=" + sessionUserID
        $.ajax({
            type: "GET",
            url: api,
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    savePlan = true;
                }
            }
        });
    }
    else {
        sweet_alert_error("an equal record already exists");
        return;
    }

    if (savePlan) {
        list();
        $('#myModal').modal('hide');
        cleanControls();
    }

}

function cleanControls() {
    $('.modal-body select').empty().append("<option value=''>--Seleccione--</option>");

    loadCombo(dataCrop, 'selectCrop', true);
    loadCombo(dataOrigin, 'selectOrigin', true);
    $('#txtPlan').val("");
    $('#inputNonExport').val("");
    $('#lblPlanId').text("0");
}

function openModal(id) {
    if (id == 0) {
        cleanControls();
        $('#selectOrigin').prop("disabled", false);
        $('#selectGrower').prop("disabled", false);
        $('#selectCrop').prop("disabled", false);
        $('#selectSeason').prop("disabled", false);
        loadCombo(dataGrower, 'selectGrower', true);
    } else {
        fnListPlanById(id, successListPlanById);
    }
}

var successListPlanById = function (data) {
    if (data.length > 0) {

        var dataJson = JSON.parse(data);
        loadCombo(dataGrower, 'selectGrower', true);
        loadCombo(dataSeason, 'selectSeason', true);
        loadCombo(dataOrigin, 'selectOrigin', true);
        loadCombo(dataCrop, 'selectCrop', true);


        $('#selectOrigin').val(dataJson[0].originID).change();
        $('#selectGrower').val(dataJson[0].growerID).change();
        $('#selectCrop').val(dataJson[0].cropID).change();
        $('#selectSeason').val(dataJson[0].campaignID);
        $('#txtPlan').val(dataJson[0].plan);
        $('#inputNonExport').val(dataJson[0].nonExport);

        $('#selectOrigin').prop("disabled", true);
        $('#selectGrower').prop("disabled", true);
        $('#selectCrop').prop("disabled", true);
        $('#selectSeason').prop("disabled", true);

        $('#lblPlanId').text(dataJson[0].planID);

    }
};

function list() {
    fnListPlan(successListPlan);
}

var successListPlan = function (data) {
    var dataList = JSON.parse(data);

    if (data.length > 0) {
        createList(["Id", "Plan", "Season", "Crop",
            "Origin", "Grower", "Creator User", "Last Modified"], dataList);
    }
}

function createList(arrayColumnas, data) {
    var content = "";
    content += "<table id='tablas' class='table table-bordered table-striped ' table-responsive>";
    content += "<thead  bgcolor='#13213d' style='font-size:12px!important' >";
    content += "<tr>";
    content += "<td style='max-width:180px!important;min-width:180px!important'><font color='#fff' >Edit</font></td>";
    for (var i = 0; i < arrayColumnas.length; i++) {
        if (i == 0) { content += "<td style='display:none'>"; }
        else content += "<td><font color='#fff'>";
        content += arrayColumnas[i];
        content += "</font></td>";

    }

    content += "</tr>";
    content += "</thead>";
    var llaves = Object.keys(data[0]);
    content += "<tbody  style='font-size:12px!important'>";
    for (var i = 0; i < data.length; i++) {
        var llaveId = llaves[0];
        content += "<tr>";
        content += "<td style='text-align:center;max-width:180px!important;min-width:180px!important'>";
        content += "<a href='/CommercialPlan/Edit?PlanId=" + data[i][llaveId] + "' class='btn btn-primary btn-sm ' style='padding:0 0 0 0!important'>";
        content += "<span class='icon text-white-50'>";
        content += "<i class='fas fa-edit fa-sm'></i>";
        content += "</span>";
        content += "<span class=' text' style='font-size:12px!important'> Main Data</span>";
        content += "</a>";
        content += "<a href='#' class='btn btn-info btn-sm ' style='padding:0 0 0 0!important' onclick='openModal(" + data[i][llaveId] + ")' data-toggle='modal' data-target='#myModal' data-backdrop='static' data-keyboard='false'>";
        content += "<span class='icon text-white-50'>";
        content += "<i class='fas fa-edit fa-sm'></i>";
        content += "</span>";
        content += "<span class='text' style='font-size:12px!important'> Go To Plan</span>";
        content += "</a>";

        content += "</td>"
        for (var j = 0; j < llaves.length; j++) {
            var valorLLaves = llaves[j];
            if (j == 0) { content += "<td style='display:none'>"; }
            else content += "<td>";
            content += data[i][valorLLaves];
            content += "</td>";

        }
        
        content += "</tr>";
    }
    content += "</tbody>";
    content += "</table>";
    document.getElementById("divList").innerHTML = content;
    $("table#tablas").dataTable();
}

function obligatory() {
    var success = true;
    var controlObligatory = $(".obligatory");
    $(".obligatory").each(function (i, element) {
        if ($(element).val() == "") {
            $(element).focus();
            sweet_alert_error("You Should Complete all fields");
            success = false;
        }

    })

    return success;
}