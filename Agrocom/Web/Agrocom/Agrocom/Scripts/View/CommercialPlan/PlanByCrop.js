﻿
var dataWeek = [];
var dataPlan = [];
var dataVariety = [];
var dataCategory = [];
var dataFormat = [];
var dataTypeBox = [];
var dataBrand = [];
var dataPresentation = [];
var dataLabel = [];
var dataSize = [];
var dataColor = [];
var dataForecast = [];
var trEdit = [];
var dataSizeBox = [];
var dataPlanCommercial = [];
var __dataPlanCommercial = [];
var dataVarCatByCrop = [];

var table = $('#tblCommercialPlan').DataTable({
	orderCellsTop: false,
	fixedHeader: true,
	order: false
});

var iTotVarietyXCategory = 0;

!function ($) {

	$(function () {

		$("#lblPlanId").text(localStorage.cropID);
		$("#lblCamp").text(localStorage.campaign);

		//>>>Obtener semanas por campaña
		//fnListWeekByCampaign(localStorage.campaignID, successListWeekByCampaign);

		$.ajax({
			async: false,
			type: 'GET',
			url: "/Forecast/ListWeekByCampaign?campaignID=" + localStorage.campaignID,
			headers: {
				'Cache-Control': 'no-cache, no-store, must-revalidate',
				'Pragma': 'no-cache',
				'Expires': '0'
			},
			success: function (data) {
				if (data.length > 0) {
					dataWeek = JSON.parse(data);

				}
			}
		});
		//<<<Obtener semanas por campaña

		//>>>Obtener categorías por cultivo
		var opt = "cro";
		var gro = "";
		$.ajax({
			type: "GET",
			url: "/Sales/CategoryGetByCrop?opt=" + opt + "&id=" + localStorage.cropID + "&gro=" + gro,
			async: false,
			headers: {
				'Cache-Control': 'no-cache, no-store, must-revalidate',
				'Pragma': 'no-cache',
				'Expires': '0'
			},
			success: function (data) {
				if (data.length > 0) {
					var dataJson = JSON.parse(data);
					dataCategory = dataJson.map(
						obj => {
							return {
								"categoryID": obj.categoryID,
								"description": obj.description
							}
						}
					);
				}
			}
		});
		//<<<Obtener categorías por cultivo

		//>>>Obtener formato por cultivo
		var optID = "fci";
		$.ajax({
			type: "GET",
			url: "/Sales/GetFormatByCropIDMC?opt=" + optID + "&id=" + localStorage.cropID,
			async: false,
			headers: {
				'Cache-Control': 'no-cache, no-store, must-revalidate',
				'Pragma': 'no-cache',
				'Expires': '0'
			},
			success: function (data) {
				if (data.length > 0) {
					var dataJson = JSON.parse(data);
					dataFormat = dataJson.map(
						obj => {
							return {
								"id": obj.formatID,
								"abbreviation": obj.format,
								"description": obj.format,
								"weight": obj.weight
							}
						}
					);
				}
			}
		});
		//<<<Obtener formato por cultivo

		//>>>Obtener todos los empaques del producto
		optID = "pkc";
		var filter01 = '';
		$.ajax({
			type: "GET",
			url: "/Sales/GetPackageProductAllMC?opt=" + optID + "&id=" + filter01,
			async: false,
			headers: {
				'Cache-Control': 'no-cache, no-store, must-revalidate',
				'Pragma': 'no-cache',
				'Expires': '0'
			},
			success: function (data) {
				if (data.length > 0) {
					var dataJson = JSON.parse(data);
					dataTypeBox = dataJson.map(
						obj => {
							return {
								"id": obj.packageProductID,
								"description": obj.packageProduct,
								"abbreviation": obj.abbreviation
							}
						}
					);
				}
			}
		});
		//<<<Obtener todos los empaques del producto
		var optplan = "pla";
		//>>>Obtener el plan de proyección de campo según campaña
		$.ajax({
			type: "GET",
			url: "/Forecast/GetForecastForPlan?opt=" + optplan + "&campaignID=" + localStorage.campaignID,
			async: false,
			headers: {
				'Cache-Control': 'no-cache, no-store, must-revalidate',
				'Pragma': 'no-cache',
				'Expires': '0'
			},
			success: function (data) {
				if (data.length > 0) {
					var dataJson = JSON.parse(data);
					dataForecast = dataJson.map(
						obj => {
							return {
								"varietyID": obj.varietyID,
								"name": obj.name,
								"categoryID": obj.categoryID,
								"description": obj.description,
								"projectedWeekID": obj.projectedWeekID,
								"number": obj.number,
								"BOXES": obj.boxes
							}
						}
					);
				}
			}
		});
		//<<<Obtener el plan de proyección de campo según campaña

		loadData();

		//>>>Asignación de métodos según evento a objetos
		$(document).on("click", ".btnDeleteRowToProcess", function (e) {
			var trDetele = $(this).closest('tr')
			deleteRowProcess(trDetele)
		})
		$(document).on("change", "#selectVariety", function () {
			calculeCodepack();
		});

		$(document).on("click", "#btnSave", function () {
			GenerateSaleRequestAndPO();
		});

		$(document).on("change", "#selectFormat", function () {
			calculeCodepack();
		});

		$(document).on("change", "#selectTypeBox", function () {
			calculeCodepack();
		});

		$(document).on("change", "#selectBrand", function () {
			calculeCodepack();
		});

		$(document).on("change", "#selectPresentation", function () {
			calculeCodepack();
		});

		$(document).on("change", "#selectLabel", function () {
			calculeCodepack();
		});

		$(document).on("change", "#selectSize", function () {
			calculeCodepack();
		});

		$(document).on("click", "#btnAddClient", function () {
			addClientCodepack();
			sweet_alert_success('Good Job!', 'Datas Saved.');
		});

		$(document).on("click", ".btnRowEdit", function () {
			var tr = $(this).closest('tr');
			rowEdit(tr);
		});

		$(document).on("click", ".btnRowSave", function () {
			var tr = $(this).closest('tr');
			rowSave(tr, 1);
		});

		$(document).on("click", ".btnRowDelete", function () {
			var tr = $(this).closest('tr');

			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.isConfirmed) {
					rowSave(tr, 0);
				} else {
					if (
						/* Read more about handling dismissals below */
						result.dismiss === Swal.DismissReason.cancel
					) {
						swalWithBootstrapButtons.fire(
							'Cancelled',
							'Your action was cancelled',
							'error'
						);
					}
				}
			});
		});

		$(document).on("click", "#btnEditClient", function () {
			updateRow()
		});

		$(document).on("change", "#selectClient", function () {
			loadDestination($(selectClient).val())
		});

		$(document).on("change", "#selectFormat", function () {
			loadSizeBox()
		});

		$(document).on("change", "#selectTypeBox", function () {
			loadSizeBox()
		});

		$(document).on("change", "#selectCodepack", function () {
			$(txtBoxPerPallet).val(0);
			$(txtBoxPerPerContainer).val(0);

			if (dataSizeBox.length > 0) {
				var dataFilter = dataSizeBox.filter(item => item.codePackID == $(selectCodepack).val())[0];
				$(txtBoxPerPallet).val(dataFilter.boxesPerPallet);
				$(txtBoxPerPerContainer).val(dataFilter.boxPerContainer);
			}
		});

		$(document).on("click", "#btnAddClientModal", function () {
			loadSizeBox();
			calculeCodepack();
			trEdit = '';
			clearDetails();
			$(btnAddClient).show();
			$(btnEditClient).hide();
			$(modalCodepack).modal('show');
			openModal();
		});

		$(document).on("click", "#btnProcessSR", function () {
			AvoidCloseModalByAnyKey("modalProcess");
			$(modalProcess).draggable();
			$(modalProcess).modal('show');
			drawVarietysToProcess();
		});

		$(document).on("click", "#selectWeekProcess", function () {
			loadPlanComercial();
		});

		$(document).on("keyup", "input.txtBoxNumber", function (e) {
			if (e.which >= 37 && e.which <= 40) {
				e.preventDefault();
			}
			var _value = '';
			$(this).val(function (index, value) {
				_value += value
					.replace(/\D/g, "")
					.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");	//miles
				return _value;
			});
			if (_value.length == 0) return;
			$(this).closest('tr').find('.tdBtn').attr("style", "background-color:#F9E79F");
			$(this).closest('tr').find(".btnRowSave").show();
			var table = $(this).closest('table')
			calculateProjectedSales(table);

		});

		$(document).on("click", ".btnAddProcess", function (e) {
			var trProcess = $(this).closest('tr');
			addRowToProcess(trProcess)
		})
		//<<<Asignación de métodos según evento a objetos

		$(document).on("click", "#btnExport", function () {
			ExportCommercialPlan();

		});
	});

}(window.jQuery);

var successListWeekByCampaign = function (data) {
	if (data.length > 0) {
		dataWeek = JSON.parse(data);
	}
}

function clearDetails() {
	$(selectClient).selectpicker('val', '');
	$(selectProgram).selectpicker('val', '');
	$(selectDestination).selectpicker('val', '');
	$(selectKAM).selectpicker('val', '');
	$(selectIncoterm).selectpicker('val', '');
	$(selectPaymentTerms).selectpicker('val', '');
	$(selectPriceConditions).selectpicker('val', '');
	$(txtCodepack).val('');
	$(txtBoxPerPallet).val(0);
	$(txtBoxPerPerContainer).val(0);
	$('#selectSize').multiselect('deselectAll', false);
	$('#selectSize').multiselect('refresh');
	$('#selectColor').multiselect('deselectAll', false);
	$('#selectColor').multiselect('refresh');
	trEdit = null;
	bError = true;
}

function ExportCommercialPlan() {
	//$('#divExportCommercialPlan').attr('hidden', 'false');
	$('#divExportCommercialPlan').empty();

	//>>>Clonar tablas
	$("#tblPlan-VR-21-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblExportCommercialPlan');
	$("#tblPlan-VR-21-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-21-222');
	$("#tblPlan-VR-18-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-18-111');
	$("#tblPlan-VR-18-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-18-222');
	$("#tblPlan-VR-14-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-14-111');
	$("#tblPlan-VR-14-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-14-222');
	$("#tblPlan-VR-17-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-17-111');
	$("#tblPlan-VR-17-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-17-222');
	$("#tblPlan-VR-19-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-19-111');
	$("#tblPlan-VR-19-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-19-222');
	$("#tblPlan-VR-13-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-13-111');
	$("#tblPlan-VR-13-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-13-222');
	$("#tblPlan-VR-20-1").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-20-111');
	$("#tblPlan-VR-20-2").clone().appendTo("#divExportCommercialPlan").attr('id', 'tblPlan-VR-20-222');
	//<<<Clonar tablas

	//>>>Cabecera
	var html_harvest_week_01 = '<th>[]</th><th style="display:none">tdPlanCustomerVarietyID</th><th>Variety</th><th>Category</th><th>Customer</th><th>Program</th><th>KAM</th><th>Priority</th><th>Status</th><th>Destination</th><th>Box Net Weight</th><th>Box Brand</th>';	//13 columns
	var html_harvest_week_02 = '<th style="display:none">customerID</th><th style="display:none">consigneeID</th><th style="display:none">destinationID</th><th style="display:none">KAMID</th><th style="display:none">priorityID</th><th style="display:none">statusID</th><th style="display:none">IncotermID</th><th style="display:none">PaymentTerms</th><th style="display:none">PriceConditions</th>';
	html_harvest_week_02 = html_harvest_week_02 + '<th style="display:none">varietyID</th><th style="display:none">categoryID</th><th style="display:none">formatID</th><th style="display:none">packageProductID</th><th style="display:none">brandID</th><th style="display:none">presentationID</th><th style="display:none">labelID</th><th style="display:none">boxPerPallet</th><th style="display:none">codepackID</th><th style="display:none">sizeID</th>';	//19 columns	

	//Quitamos el colspan en la 1era. fila
	//Completamos con nuevas columnas el colspan removido para que funcione el datatable
	$("#tblExportCommercialPlan > thead tr:first th:first").removeAttr('colspan');
	$("#tblExportCommercialPlan > thead tr:first th:first").before(html_harvest_week_01);
	$("#tblExportCommercialPlan > thead tr:first th:eq(13)").before(html_harvest_week_02);

	//Modificamos la 2da. fila de la semana proyectada
	$("#tblExportCommercialPlan > thead tr.trForecast th:eq(13)").before('<td style="display:none"></td>');
	var html_projected_week = $("#tblExportCommercialPlan > thead tr.trForecast").html();
    html_projected_week = "<tr>" + html_projected_week.replace(/th/g, 'td') + "</tr>";
	$('#tblExportCommercialPlan > tbody tr:first').before(html_projected_week);
	$("#tblExportCommercialPlan tr.trForecast").remove();

	//Modificamos la 3era. fila de la semana de despacho
	var html_departure_week = $("#tblExportCommercialPlan > thead tr:eq(1)").html();
	html_departure_week = "<tr>" + html_departure_week.replace(/th/g, 'td') + "</tr>";
	$('#tblExportCommercialPlan > tbody tr:first').before(html_departure_week);
	$("#tblExportCommercialPlan > thead tr:eq(1)").remove(); 
	//<<<Cabecera

	//>>>Totales
	var html01 = '<td></td><td style="display:none"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>';	//12 columns
	var html02 = '<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>';
	html02 = html02 + '<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>';	//19 columns

	//En cada tabla copiamos el footer a su body
	//Quitamos el colspan en fila con tr: trProjectedSales
	//Completamos con nuevas columnas el colspan removido para que funcione el datatable
	//Quitamos el colspan en fila con tr: trBalance
	//Completamos con nuevas columnas el colspan removido para que funcione el datatable
	$('#tblExportCommercialPlan > tbody').append($('#tblExportCommercialPlan > tfoot').html());
	$("#tblExportCommercialPlan > tbody tr.trProjectedSales td:first").removeAttr('colspan');	
	$("#tblExportCommercialPlan > tbody tr.trProjectedSales td:first").before(html01);	
	$("#tblExportCommercialPlan > tbody tr.trProjectedSales td:eq(13)").before(html02);	
	$("#tblExportCommercialPlan > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblExportCommercialPlan > tbody tr.trBalance td:first").before(html01);
	$("#tblExportCommercialPlan > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-21-222 > tbody').append($('#tblPlan-VR-21-222 > tfoot').html());
	$("#tblPlan-VR-21-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-21-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-21-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-21-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-21-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-21-222 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-18-111 > tbody').append($('#tblPlan-VR-18-111 > tfoot').html());
	$("#tblPlan-VR-18-111 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-18-111 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-18-111 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-18-111 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-18-111 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-18-111 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-18-222 > tbody').append($('#tblPlan-VR-18-222 > tfoot').html());
	$("#tblPlan-VR-18-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-18-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-18-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-18-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-18-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-18-222 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-14-111 > tbody').append($('#tblPlan-VR-14-111 > tfoot').html());
	$("#tblPlan-VR-14-111 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-14-111 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-14-111 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-14-111 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-14-111 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-14-111 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-14-222 > tbody').append($('#tblPlan-VR-14-222 > tfoot').html());
	$("#tblPlan-VR-14-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-14-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-14-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-14-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-14-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-14-222 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-17-111 > tbody').append($('#tblPlan-VR-17-111 > tfoot').html());
	$("#tblPlan-VR-17-111 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-17-111 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-17-111 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-17-111 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-17-111 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-17-111 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-17-222 > tbody').append($('#tblPlan-VR-17-222 > tfoot').html());
	$("#tblPlan-VR-17-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-17-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-17-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-17-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-17-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-17-222 > tbody tr.trBalance td:eq(13)").before(html02);
	
	$('#tblPlan-VR-19-111 > tbody').append($('#tblPlan-VR-19-111 > tfoot').html());
	$("#tblPlan-VR-19-111 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-19-111 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-19-111 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-19-111 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-19-111 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-19-111 > tbody tr.trBalance td:eq(13)").before(html02);
	
	$('#tblPlan-VR-19-222 > tbody').append($('#tblPlan-VR-19-222 > tfoot').html());
	$("#tblPlan-VR-19-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-19-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-19-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-19-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-19-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-19-222 > tbody tr.trBalance td:eq(13)").before(html02);
	
	$('#tblPlan-VR-13-111 > tbody').append($('#tblPlan-VR-13-111 > tfoot').html());
	$("#tblPlan-VR-13-111 > tbody tr.trProjectedSales td:first").removeAttr('colspan');
	$("#tblPlan-VR-13-111 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-13-111 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-13-111 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-13-111 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-13-111 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-13-222 > tbody').append($('#tblPlan-VR-13-222 > tfoot').html());
	$("#tblPlan-VR-13-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');	
	$("#tblPlan-VR-13-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-13-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-13-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-13-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-13-222 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-20-111 > tbody').append($('#tblPlan-VR-20-111 > tfoot').html());
	$("#tblPlan-VR-20-111 > tbody tr.trProjectedSales td:first").removeAttr('colspan');	
	$("#tblPlan-VR-20-111 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-20-111 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-20-111 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-20-111 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-20-111 > tbody tr.trBalance td:eq(13)").before(html02);

	$('#tblPlan-VR-20-222 > tbody').append($('#tblPlan-VR-20-222 > tfoot').html());
	$("#tblPlan-VR-20-222 > tbody tr.trProjectedSales td:first").removeAttr('colspan');	
	$("#tblPlan-VR-20-222 > tbody tr.trProjectedSales td:first").before(html01);
	$("#tblPlan-VR-20-222 > tbody tr.trProjectedSales td:eq(13)").before(html02);
	$("#tblPlan-VR-20-222 > tbody tr.trBalance td:first").removeAttr('colspan');
	$("#tblPlan-VR-20-222 > tbody tr.trBalance td:first").before(html01);
	$("#tblPlan-VR-20-222 > tbody tr.trBalance td:eq(13)").before(html02);
	//<<<Totales

	$('#tblExportCommercialPlan > tbody > tr').each(function () {
		let obj = $(this).find('td.tdCodepack');
		if (obj.length > 0) {
			obj.contents().unwrap()
			/*console.log();*/
		}
	})

	//>>>Merge de tablas
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-21-222 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-18-111 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-18-222 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-14-111 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-14-222 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-17-111 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-17-222 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-19-111 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-19-222 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-13-111 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-13-222 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-20-111 > tbody').html());
	$('#tblExportCommercialPlan > tbody:last').append($('#tblPlan-VR-20-222 > tbody').html());
	//<<<Merge de tablas

	//>>>Eliminar restantes
	$('#tblPlan-VR-21-222').remove();
	$('#tblPlan-VR-18-111').remove();
	$('#tblPlan-VR-18-222').remove();
	$('#tblPlan-VR-14-111').remove();
	$('#tblPlan-VR-14-222').remove();
	$('#tblPlan-VR-17-111').remove();
	$('#tblPlan-VR-17-222').remove();
	$('#tblPlan-VR-19-111').remove();
	$('#tblPlan-VR-19-222').remove();
	$('#tblPlan-VR-13-111').remove();
	$('#tblPlan-VR-13-222').remove();
	$('#tblPlan-VR-20-111').remove();
	$('#tblPlan-VR-20-222').remove();
	$("#tblExportCommercialPlan tfoot").remove();
	//<<<Eliminar tablas restantes

    let arrayColumnsToShow = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 48];
	let sheetName = 'data';
	let fileName = "Commercial_Program_" + localStorage.campaign + "_" + GetCurrentDateTimeYYYYMMDDHHMM() + ".xls";
	CuztomiseHtmlDatatableExport('tblExportCommercialPlan', true, true, arrayColumnsToShow, sheetName, fileName);
	
	//$('#divExportCommercialPlan').attr('hidden', 'true');
}

function deleteRowProcess(trDetele) {
	var tdplanCustomerVarietyID = $(trDetele).find('.tdplanCustomerVarietyID');
	var tdboxesProcess = $(trDetele).find('.tdtoProcess');
	var dataPlanCommercialX = dataPlanCommercial;
	var itemFind = dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text());
	dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text()).boxes = parseInt(itemFind.boxes) + parseInt(tdboxesProcess.text());
	dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text()).toProcess = "<input style='max-width:100px' min=0 step='" + itemFind.boxesPerPallet + "'  class='form-control form-control-sm' type='number' max='" + itemFind.boxes + "' value='0'/>";
	dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text()).inProcess = parseInt(itemFind.inProcess) - parseInt(tdboxesProcess.text());

	DataFiltered();
	$(trDetele).remove();

}

function DataFiltered() {
	table = $('#tblCommercialPlan').DataTable({
		order: false,
		fixedHeader: true,
		destroy: true,
		"data": dataPlanCommercial,
		"columns": [
			{ "max-width": "100px", "data": "planCustomerVarietyID", visible: false, className: 'tdplanCustomerVarietyID' },
			{ "max-width": "20px", "data": "varietyID", visible: false, className: 'tdvarietyID' },
			{ "max-width": "20px", "data": "categoryID", visible: false, className: 'tdcategoryID' },
			{ "max-width": "20px", "data": "customerID", visible: false, className: 'tdcustomerID' },
			{ "max-width": "20px", "data": "destinationID", visible: false, className: 'tddestinationID' },
			{ "max-width": "100px", "data": "variety", className: 'tdvariety' },
			{ "max-width": "70px", "data": "category", className: 'tdcategory' },
			{ "max-width": "150px", "data": "customer", className: 'tdcustomer' },
			{ "max-width": "100px", "data": "program", className: 'tdprogram' },
			{ "max-width": "50px", "data": "priority", className: 'tdpriority' },
			{ "max-width": "80px", "data": "destination", className: 'tddestination' },
			{ "max-width": "80px", "data": "codePack", className: 'tdcodePack' },
			{ "max-width": "50px", "data": "boxesPerPallet", className: 'tdboxesPerPallet' },
			{ "max-width": "100px", "data": "boxes", className: 'tdboxes' },
			{ "max-width": "50px", "min-width": "50px", "data": "toProcess", className: 'tdtoProcess' },
			{ "max-width": "30px", "data": "add", className: 'tdadd' },
			{ "max-width": "60px", "data": "price", visible: false, className: 'tdprice' },
			{ "max-width": "80px", "min-width": "80px", "data": "inProcess", className: 'tdinProcess' },
			{ "max-width": "100px", "min-width": "100px", "data": "proceced", visible: false, className: 'tdproceced' }
		],
		rowReorder: { dataSrc: 'planCustomerVarietyID' }

	});
}

function addRowToProcess(trProcess) {
	$(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4]).visible(true);
	var planCustomerVarietyID = parseInt(trProcess.find('td.tdplanCustomerVarietyID').text())
	var totalBoxes = trProcess.find('td.tdboxes').text();
	var boxesToProcess = parseInt(trProcess.find('td.tdtoProcess>input').val());
	var boxesInProcess = parseInt(trProcess.find('td.tdinProcess').text());
	var diference = totalBoxes - boxesToProcess
	var totalInProcess = boxesInProcess + boxesToProcess
	var palletxBox = parseInt(trProcess.find('td.tdboxesPerPallet').text());
	var totalPallets = (boxesToProcess / palletxBox)

	if (diference < 0) {
		sweet_alert_error("Error", "review requested boxes")
		$(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4]).visible(false);
		return;
	}
	if (!Number.isInteger(totalPallets)) {
		sweet_alert_error("Error", "boxes must be exact pallets")
		$(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4]).visible(false);
		return;
	}

	trProcess.find('td.tdboxes').text(diference);
	trProcess.find('td.tdinProcess').text(totalInProcess);

	dataPlanCommercial.find(x => x.planCustomerVarietyID == planCustomerVarietyID).boxes = diference;
	dataPlanCommercial.find(x => x.planCustomerVarietyID == planCustomerVarietyID).inProcess = totalInProcess;

	//>>>Forzamos a que se muestre oculta la columna del plan comercial
	$("#tblToProcess>tbody>tr").each(function (i, tr) {
		if ($(tr).find('td.tdplanCustomerVarietyID').text() == planCustomerVarietyID) {
			$(tr).find('td.tdplanCustomerVarietyID').text()
		}
	})
	//<<<Forzamos a que se muestre oculta la columna del plan comercial

	var qPO = parseFloat(totalPallets) / 20;
	if (!Number.isInteger(qPO)) {
		var residuo = totalPallets % 20
		var residuoBoxes = boxesToProcess % (palletxBox * 20)
		drawRowToProcess(trProcess, residuo, residuoBoxes)
		totalPallets = totalPallets - residuo

	}

	qPO = parseInt(totalPallets) / 20;
	var i;
	for (i = 0; i < qPO; i++) {
		drawRowToProcess(trProcess, 20, palletxBox * 20)
	}

	$(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4]).visible(false);

}

function drawVarietysToProcess() {
	let iprojectedWeekID = 0;
	var lstWeekByCampaign;

	//>>>Obtener la semana actual y semanas por campaña a mostrarse en el modal que contendrá los planes comercial (de la semana que se seleccione) a procesar
	var opt = "act";
	var id = "";
	var cropID = localStorage.cropID;
	var campaignID = localStorage.campaignID;

	$.ajax({
		async: false,
		type: 'GET',
		url: "/Forecast/ListWeeksMC?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			let objCurrentWeek = JSON.parse(data);
			iprojectedWeekID = objCurrentWeek[0].projectedWeekID;

		}
	});

	$.ajax({
		async: false,
		type: 'GET',
		url: "/Forecast/ListWeekByCampaign?campaignID=" + localStorage.campaignID,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				let dataJson = JSON.parse(data);
				lstWeekByCampaign = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description,
						}
					}
				);
			}
		}
	});
	loadCombo(lstWeekByCampaign, 'selectWeekProcess', false);
	//$('#selectWeekProcess').val(iprojectedWeekID);
	$('#selectWeekProcess').prop('selectedIndex', 0);
	$('#selectFilterVariety').prop('selectedIndex', 0);
	//<<<Obtener la semana actual y semanas por campaña a mostrarse en el modal que contendrá los planes comercial (de la semana que se seleccione) a procesar
	loadPlanComercial();
}

function loadPlanComercial() {
	GetCommercialPlanByCampaignAndWeekToShowInTableCommercialPlan();
	//>>>Dibujar las variedades que contendrá los planes comerciales a procesar por variedad
	cleartblToProcess();
	//<<<Dibujar las variedades que contendrá los planes comerciales a procesar por variedad
}

//function loadCombo(data, control, firtElement) {
//	var content = "";
//	if (firtElement == true) {
//		content += "<option value=''>--Choose--</option>";
//	} else {
//		content += "<option value=''>--All--</option>";
//	}
//	for (var i = 0; i < data.length; i++) {
//		content += "<option value='" + data[i].id + "'>";
//		content += data[i].name;
//		content += "</option>";
//	}
//	$('#' + control).empty().append(content);
//}

function drawRowToProcess(trProcess, totalPallets, boxes) {
	var tr = ''
	var flag = 0
	var iNroVarietyXCategory = 0;
	$('#tblToProcess>tbody>tr').each(function (i, e) {
		if ($(e).find('td.tdStatus').text() == '2' && trProcess.find('td.tdvarietyID').text() == $(e).find('td.tdvarietyID').text() &&
			trProcess.find('td.tdcategoryID').text().trim() == $(e).find('td.tdcategoryID').text().trim()) {
			flag = 1;
			tr = $(e);
			iNroVarietyXCategory = $(e).find('td.tdVarietyXCategory').text()
		}
		if (flag == 1) {
			if ($(e).find('td.tdStatus').text() == '2') {
				flag = 0;
				return
			}

			tr = $(e)

		}

	})

	var trAdd = '<tr>'
	trAdd += '<td style="display:none" class="tdVarietyXCategory">' + iNroVarietyXCategory + '</td>'
	trAdd += '<td style="display:none" class="tdplanCustomerVarietyID">' + trProcess.find('td.tdplanCustomerVarietyID').text() + '</td>'
	trAdd += '<td style="display:none" class="tdvarietyID">' + trProcess.find('td.tdvarietyID').text() + '</td>'
	trAdd += '<td style="display:none" class="tdcategoryID">' + trProcess.find('td.tdcategoryID').text() + '</td>'
	trAdd += '<td style="display:none" class="tdcustomerID">' + trProcess.find('td.tdcustomerID').text() + '</td>'
	trAdd += '<td style="display:none" class="tddestinationID">' + trProcess.find('td.tddestinationID').text() + '</td>'
	trAdd += '<td class="tdvariety var' + trProcess.find('td.tdvarietyID').text() + '">' + trProcess.find('td.tdvariety').text() + '</td>'
	trAdd += '<td class="tdcategory cat' + trProcess.find('td.tdcategoryID').text().trim() + '">' + trProcess.find('td.tdcategory').text() + '</td>'
	trAdd += '<td class="tdcustomer">' + trProcess.find('td.tdcustomer').text() + '</td>'
	trAdd += '<td class="tdprogram">' + trProcess.find('td.tdprogram').text() + '</td>'
	trAdd += '<td class="tdpriority">' + trProcess.find('td.tdpriority').text() + '</td>'
	trAdd += '<td class="tddestination">' + trProcess.find('td.tddestination').text() + '</td>'
	trAdd += '<td class="tdcodePack">' + trProcess.find('td.tdcodePack').text() + '</td>'
	trAdd += '<td class="tdboxesPerPallet">' + trProcess.find('td.tdboxesPerPallet').text() + '</td>'
	trAdd += '<td class="tdtoProcess">' + boxes + '</td>'
	if (totalPallets == 20) {
		trAdd += '<td class="tdPallets palletComplete" >' + totalPallets + '</td>'
	} else {
		trAdd += '<td class="tdPallets residuesinComplete">' + totalPallets + '</td>'
	}
	trAdd += '<td class="tdDelete"><button class="btnDeleteRowToProcess">Delete</button></td>'
	trAdd += '<td class="tdSort" rowspan="1">' + 0 + '</td>'
	trAdd += '<td style="display:none;" class="tdStatus">0</td>'
	if (totalPallets == 20) {
		trAdd += '<td style="display:none;" class="tdMixed" >0</td>'
	} else {
		trAdd += '<td style="display:none;" class="tdMixed">1</td>'
	}
	trAdd += '</tr>'
	$(tr).after(trAdd)
	$("#tblToProcess").tableDnD({
		onDragStop: function (table, row) {
			verifyRowsResidues()
		}
	});
}

function verifyRowsResidues() {
	var iBoxes = 0;
	var trAux = [];
	var iIndex = 0;
	var customer = 0;
	var destination = 0;
	var contador = 0;
	var inicial = 0;
	var posc1 = 1;
	var posición = 0;

	$("#tblToProcess>tbody>").each(function (i, tr) {
		var pallets = parseInt($(tr).find('td.tdPallets').text())

		if ($(tr).find('td.tdStatus').text() == '2') {
			iBoxes = 0;
			trAux = [];
			iIndex = 0;
			return;
		}

		iIndex = iIndex + 1;
		$(tr).find('td.tdSort').text(iIndex)

		if (pallets < 20) {
			$(tr).find('td.tdPallets').removeClass("residuesComplete").addClass("residuesinComplete")
			$(tr).find('td.tdSort').attr('rowspan', 1);
			$(tr).find('td.tdSort').show()
			$(tr).find('td.tdStatus').text('0')

			contador = 0;

			iBoxes = pallets + iBoxes;
			trAux.push(tr)

			//NEW
			inicial = inicial + 1;
			posc1 = iIndex;

			if (iBoxes <= 20 && customer == parseInt($(tr).find('td.tdcustomerID').text()) && destination == parseInt($(tr).find('td.tddestinationID').text())) {

				iTotVarietyXCategory++;
				contador = contador + 1;
				posc1 = posc1 + 1;

				$.each(trAux, function (ii, ttr) {

					$(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
					$(ttr).find('td.tdStatus').text('1')

					if (ii == 0) {

						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(ttr).find('td.tdSort').attr('rowspan', trAux.length);
						posición = parseInt($(ttr).find('td.tdSort').text())
					} else {

						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(ttr).find('td.tdSort').hide()
						$(ttr).find('td.tdSort').text(posición)
					}

					$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);

				});
				if ((posc1 - trAux.length) < 0) {
					posc1 = 1;
				} else {
					posc1 = posc1 - trAux.length + 1;
				}
				iIndex = posición;
			} else {
				customer = parseInt($(tr).find('td.tdcustomerID').text());
				destination = parseInt($(tr).find('td.tddestinationID').text());
				var rowCount = $('#tblToProcess tr').length;

				if (inicial > 1 && contador == 0) {
					// cuando el segundo registro  es distinto al primero

					iTotVarietyXCategory++;
					contador = contador + 1;
					$.each(trAux, function (ii, ttr) {
						$(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
						$(ttr).find('td.tdStatus').text('1')

						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(tr).find('td.tdSort').text(posc1)
					});

					iBoxes = 0
					trAux = []
					trAux.push(tr)
					contador = 0;
				} else if (contador > 0) {
					//Cuando ingreso anteriormente a agrupar registros
					iBoxes = 0
					trAux = [];
					trAux.push(tr);
					contador = 0;

					iTotVarietyXCategory++;
					contador = contador + 1;
					if (posición > 0) {
						posc1 = posición + 1;
						iIndex = posc1;
					}

					$.each(trAux, function (ii, ttr) {
						if (ii == 0) {
							$(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
							$(ttr).find('td.tdStatus').text('1')

							$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
							$(ttr).find('td.tdSort').attr('rowspan', trAux.length);

							$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
							$(tr).find('td.tdSort').text(posc1)

						}
					});
				} else if (rowCount - 3 == i || rowCount - 4 == i) {
					// cuando es el ultimo registro

					iTotVarietyXCategory++;
					contador = contador + 1;
					if (posición > 0) { posc1 = posición + 1; }
					$.each(trAux, function (ii, ttr) {
						$(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
						$(ttr).find('td.tdStatus').text('1')

						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(tr).find('td.tdSort').text(posc1)
					});

					iBoxes = 0
					trAux = []
					trAux.push(tr)
					contador = 0;
				}
				else if ((i == 1 || i == 2) && rowCount - 3 > 1) {

					iTotVarietyXCategory++;
					contador = contador + 1;
					if (posición > 0) { posc1 = posición + 1; iIndex = posc1; }
					$.each(trAux, function (ii, ttr) {
						$(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
						$(ttr).find('td.tdStatus').text('1')

						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(tr).find('td.tdSort').text(posc1)
					});

					iBoxes = 0
					trAux = []
					trAux.push(tr)
					contador = 0;
				}
			}

			//END NEW

			if (iBoxes == 20) {
				iIndex = iIndex - trAux.length + 1;
				iTotVarietyXCategory++;
				$.each(trAux, function (ii, ttr) {
					//alert(iTotVarietyXCategory);
					$(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
					$(ttr).find('td.tdStatus').text('1')
					if (ii == 0) {
						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(ttr).find('td.tdSort').attr('rowspan', trAux.length);
					} else {
						$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
						$(ttr).find('td.tdSort').hide()
					}

					$(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
					$(tr).find('td.tdSort').text(iIndex)
				})
				iBoxes = 0
				trAux = []
			} else if (iBoxes > 20) {
				iBoxes = pallets
				trAux = []
				trAux.push(tr)
			}
		} else {
			$(tr).find('td.tdStatus').text('1')
			iBoxes = 0
			trAux = []
		}
	})

	$("#tblToProcess>tbody>").each(function (i, tr) {
		//iIndex
		//if()
	})
}

function GetCommercialPlanByCampaignAndWeekToShowInTableCommercialPlan() {
	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/PlanCustomerVarietyListByWeekID?campaignID="
			+ localStorage.campaignID + "&projectedWeekID="
			+ $("#selectWeekProcess option:selected").val(),
		//async: false,
		success: function (data) {
			dataPlanCommercial = JSON.parse(data);
			dataPlanCommercial = dataPlanCommercial.map(
				obj => {
					return {
						"planCustomerVarietyID": obj.planCustomerVarietyID,
						"varietyID": obj.varietyID,
						"categoryID": obj.categoryID,
						"customerID": obj.customerID,
						"destinationID": obj.destinationID,
						"variety": obj.variety,
						"category": obj.category,
						"customer": '<textarea rows="1" style="padding:0 0 5px 0px; font-size:13px" class="form-control form-control-sm" readonly>' + obj.customer + '</textarea>',
						"program": obj.program,
						"programID": obj.programID,
						"priority": obj.priority,
						"destination": obj.destination,
						"codePack": obj.codePack,
						"boxesPerPallet": obj.boxesPerPallet,
						"boxes": obj.boxes,
						"toProcess": "<input style='max-width:100px' min=0 step='" + obj.boxesPerPallet + "' class='form-control form-control-sm' type='number' max='" + obj.boxes + "' value='0'/>",
						"add": '<button class=" btnAddProcess">Add</button>',
						"price": 0,
						"inProcess": 0,
						"proceced": obj.boxes,
						"customerDescription": obj.customer,
					}
				}
			);
			table = $('#tblCommercialPlan').DataTable({
				fixedHeader: true,
				destroy: true,
				"data": dataPlanCommercial,
				"columns": [
					{ "max-width": "100px", "data": "planCustomerVarietyID", visible: false, className: 'tdplanCustomerVarietyID' },
					{ "max-width": "20px", "data": "varietyID", visible: false, className: 'tdvarietyID' },
					{ "max-width": "20px", "data": "categoryID", visible: false, className: 'tdcategoryID' },
					{ "max-width": "20px", "data": "customerID", visible: false, className: 'tdcustomerID' },
					{ "max-width": "20px", "data": "destinationID", visible: false, className: 'tddestinationID' },
					{ "max-width": "100px", "data": "variety", className: 'tdvariety' },
					{ "max-width": "70px", "data": "category", className: 'tdcategory' },
					{ "max-width": "150px", "data": "customer", className: 'tdcustomer' },
					{ "max-width": "100px", "data": "program", className: 'tdprogram' },
					{ "max-width": "70px", "data": "priority", className: 'tdpriority' },
					{ "max-width": "80px", "data": "destination", className: 'tddestination' },
					{ "max-width": "80px", "data": "codePack", className: 'tdcodePack' },

					{ "max-width": "50px", "data": "boxesPerPallet", className: 'tdboxesPerPallet' },
					{ "max-width": "100px", "data": "boxes", className: 'tdboxes' },
					{ "max-width": "50px", "min-width": "50px", "data": "toProcess", className: 'tdtoProcess' },

					{ "max-width": "30px", "data": "add", className: 'tdadd' },
					{ "max-width": "60px", "data": "price", visible: false, className: 'tdprice' },
					{ "max-width": "80px", "min-width": "80px", "data": "inProcess", className: 'tdinProcess' },
					{ "max-width": "100px", "min-width": "100px", "data": "proceced", visible: false, className: 'tdproceced' }
				],
				rowReorder: { dataSrc: 'planCustomerVarietyID' }
			});
		},
		complete: function () {

		}
	});
}

let UniqueData = (arr) => {
	//To store the unique sub arrays
	let uniques = [];

	//To keep track of the sub arrays
	let itemsFound = {};

	for (let val of arr) {
		//convert the sub array to the string
		let stringified = JSON.stringify(val);

		//If it is already added then skip to next element
		if (itemsFound[stringified]) {
			continue;
		}

		//Else add the value to the unique list
		uniques.push(val);

		//Mark it as true so that it can tracked
		itemsFound[stringified] = true;
	}

	//Return the unique list
	return uniques;
}

function ShowCommercialPlanForEachVarietyAndCategory() {
	let dataCatByVar = [];
	$.each(dataVariety, function (iVar, variety) {
		dataCatByVar = dataVarCatByCrop.filter(item => item.varietyID == variety.id);
		$.each(dataCatByVar, function (iCat, objCat) {
			var dataPlanByVarCat = dataPlan.filter(item => item.varietyID == variety.id && item.categoryID == objCat.categoryID);
			if (dataPlanByVarCat.length > 0) {
				var dataCustomer1 = [];
				$.each(dataPlanByVarCat, function (ii, ee) {
					var flagFind = false;
					$.each(dataCustomer1, function (yy, xx) {
						if (xx == ee.planCustomerVarietyID) {
							flagFind = true;
						}
					})
					if (flagFind == false) {
						dataCustomer1.push(ee.planCustomerVarietyID)
					}
				})

				var tr = ''
				$.each(dataCustomer1, function (iplanCustomerVarietyID, planCustomerVarietyID) {
					var dataBD = dataPlanByVarCat.filter(item => item.planCustomerVarietyID == planCustomerVarietyID)
					tr = prinRowByTrBd(dataBD) + tr
				});
				var id = 'tblPlan-VR-' + variety.id + '-' + objCat.categoryID;
				$("#" + id + ">tbody").empty().append(tr);
				calculateProjectedSales("table#" + id)
			}
		});
	});

}

function prinRowByTrBd(dataBD) {

	var table = ''
	if (dataBD[0].statusConfirmID == "1") {
		table = '<tr style="text-align:center ">'
	} else {
		table = '<tr style="text-align:center" class="table-secondary">'
	}
	table += '<td style="min-width:120px;" class="tdBtn sticky" ><button style="font-size:14px" class=" btn btn-sm btn-danger btnRowDelete"><i class="fas fa-trash fa-sm"></i></button><button class="btn btn-primary btnRowEdit btn-sm " style="font-size:14px"><i class="fas fa-edit fa-sm"></i></button><button class="btn btn-success btn-sm btnRowSave" style="font-size:14px;display:none"><i class="fas fa-save fa-sm"></i></button></td>'
	table += '<td style="display:none" class="tdPlanCustomerVarietyID">' + dataBD[0].planCustomerVarietyID + '</td>'
	table += '<td style="min-width:120px; max-width:120px;" class="tdVariety sticky2">' + dataBD[0].variety + '</td>'
	table += '<td style="min-width:60px; max-width:60px;" class="tdCategory sticky3">' + dataBD[0].category + '</td>'
	table += '<td style="min-width:150px; max-width:150px;" class="tdClient sticky4">' + dataBD[0].customer + '</td>'
	table += '<td style="min-width:150px; max-width:150px;" class="tdProgram sticky5">' + dataBD[0].program + '</td>'
	table += '<td style="min-width:80px; max-width:80px;" class="tdKAM sticky6">' + dataBD[0].kam + '</td>'

	table += '<td class="tdPiority sticky7" style="min-width:90px; max-width:90px;">' + dataBD[0].priority + '</td>'
	table += '<td class="tdStatus sticky8" style="min-width:60px; max-width:60px;">' + dataBD[0].statusConfirm + '</td>'
	table += '<td style="min-width:100px; max-width:100px;" class="tdDestination sticky9">' + dataBD[0].destination + '</td>'

	table += '<td style="min-width:50px; max-width:50px;" class="tdBoxWeight sticky10">' + dataBD[0].boxWeight + '</td>'
	table += '<td style="min-width:100px; max-width:100px;" class="tdBrand sticky11">' + dataBD[0].brand + '</td>'
	table += '<td style="min-width:177px; max-width:177px;" class="tdCodepack sticky12"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + dataBD[0].codePack + '</textarea></td>'

	table += '<td style="display:none" class="tdClientID">' + dataBD[0].customerID + '</td>'
	table += '<td style="display:none" class="tdProgramID">' + dataBD[0].programID + '</td>'
	table += '<td style="display:none" class="tdDestinationID">' + dataBD[0].destinationID + '</td>'
	table += '<td style="display:none" class="tdKAMID">' + dataBD[0].kamID + '</td>'
	table += '<td style="display:none" class="tdPiorityID">' + dataBD[0].priorityID + '</td>'
	table += '<td style="display:none" class="tdstatusConfirmID">' + dataBD[0].statusConfirmID + '</td>'

	table += '<td style="display:none" class="tdIncotermID">' + dataBD[0].incotermID + '</td>'
	table += '<td style="display:none" class="tdPaymentTerms">' + dataBD[0].paymentTermID + '</td>'
	table += '<td style="display:none" class="tdPriceConditions">' + dataBD[0].conditionPaymentID + '</td>'

	table += '<td style="display:none" class="tdVarietyID">' + dataBD[0].varietyID + '</td>'
	table += '<td style="display:none" class="tdCategoryID">' + dataBD[0].categoryID + '</td>'
	table += '<td style="display:none" class="tdFormatID">' + dataBD[0].formatID + '</td>'
	table += '<td style="display:none" class="tdTypeBoxID">' + dataBD[0].packageProductID + '</td>'
	table += '<td style="display:none" class="tdBrandID">' + dataBD[0].brandID + '</td>'
	table += '<td style="display:none" class="tdPresentationID">' + dataBD[0].presentationID + '</td>'
	table += '<td style="display:none" class="tdLabelID">' + dataBD[0].labelID + '</td>'
	table += '<td style="display:none" class="tdBoxPerPallet">' + dataBD[0].boxesPerPallet + '</td>'
	table += '<td style="display:none" class="tdCodepackID">' + dataBD[0].codePackID + '</td>'
	table += '<td style="display:none" class="tdSizeID">' + dataBD[0].sizeID + '</td>'

	$.each(dataWeek, function (iWeek, week) {
		var flagFind = false
		$.each(dataBD, function (iWWeek, wweek) {
			if (week.projectedWeekID == wweek.projectedWeekID - 1 && flagFind == false) {
				flagFind = true;
				table += '<td style="text-align:right;" class="tdSalesProjected" contenteditable="true" >';
				table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="' + wweek.boxes + '" style="text-align:right;max-width:100px;background-color:transparent;"/>';
				table += '</td>';
			}
		})
		if (flagFind == false) {
			table += '<td style="text-align:right;" class="tdSalesProjected" contenteditable="true" >';
			table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/>';
			table += '</td>';
		}
	});

	table += '<td class="tdTotalSalesProjected" contenteditable="true">0</td>'
	table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/>';
	table += '</tr>'
	return table;

}

function openModal() {
	$(selectClient).attr('disabled', false);
	$(selectVariety).attr('disabled', false);
	$(selectCategory).attr('disabled', false);
}

function updateRow() {
	if ($(txtCodepack).val() == '') {
		bError = true;
		sweet_alert_warning('error', 'choose codepack')
		return
	}

	$selectProgram = $("#selectProgram option:selected")
	$selectDestination = $("#selectDestination option:selected")
	$selectKAM = $("#selectKAM option:selected")
	$selectPriority = $("#selectPriority option:selected")
	$selectStatusID = $("#selectStatus option:selected")

	$typeBoxID = $("#selectTypeBox option:selected")
	$formatID = $("#selectFormat option:selected")
	$codepackID = $("#selectCodepack option:selected")
	$brandID = $("#selectBrand option:selected")
	$presentationID = $("#selectPresentation option:selected")
	$labelID = $("#selectLabel option:selected")
	$sizeID = $("#selectSize")
	$colorID = $("#selectColor")

	$incotemID = $("#selectIncoterm option:selected")
	$paymentTermID = $("#selectPaymentTerms")
	$priceConditionID = $("#selectPriceConditions option:selected")

	$codepack = $("#txtCodepack").val()
	let strSize = GetTextMultiselect('selectSize', 1);
	let strColor = GetTextMultiselect('selectColor', 1);

	$(trEdit).find('td.tdProgram').text($($selectProgram).text())
	$(trEdit).find('td.tdProgramID').text($($selectProgram).val())
	$(trEdit).find('td.tdDestination').text($($selectDestination).text())
	$(trEdit).find('td.tdDestinationID').text($($selectDestination).val())
	$(trEdit).find('td.tdKAM').text($($selectKAM).text())
	$(trEdit).find('td.tdKAMID').text($($selectKAM).val())
	$(trEdit).find('td.tdPiority').text($($selectPriority).text())
	$(trEdit).find('td.tdPiorityID').text($($selectPriority).val())

	$(trEdit).find('td.tdTypeBoxID').text($($typeBoxID).val())
	$(trEdit).find('td.tdBoxWeight').text($($formatID).text())
	$(trEdit).find('td.tdFormatID').text($($formatID).val())	
	$(trEdit).find('td.tdCodepackID').text($($codepackID).val())
	$(trEdit).find('td.tdBrand').text($($brandID).text())
	$(trEdit).find('td.tdBrandID').text($($brandID).val())
	$(trEdit).find('td.tdPresentationID').text($($presentationID).val())
	$(trEdit).find('td.tdLabelID').text($($labelID).val())
	$(trEdit).find('td.tdSizeID').text(strSize).text();
	$(trEdit).find('td.tdColorID').text(strColor).text();

	$(trEdit).find('td.tdCodepack').text($codepack)
	$(trEdit).find('td.tdBoxPerPallet').text($(txtBoxPerPallet).val())

	$(trEdit).find('td.tdIncotermID').text($($incotemID).val())
	$(trEdit).find('td.tdPaymentTerms').text($($paymentTermID).val())
	$(trEdit).find('td.tdPriceConditions').text($($priceConditionID).val())

	$(trEdit).find('td.tdStatus').text($($selectStatusID).text())
	$(trEdit).find('td.tdstatusConfirmID').text($($selectStatusID).val())

	if ($($selectStatusID).val() == "1") {
		$(trEdit).removeClass("table-secondary");
	} else {
		$(trEdit).addClass("table-secondary");
	}

	$(trEdit).find(".btnRowSave").show()
	$(trEdit).find(".tdBtn").attr("style", "background-color:#F9E79F")
	$(modalCodepack).modal('hide')

}

function rowSave(tr, statusID) {
	$(tr).find(".btnRowSave").hide()
	$(tr).find(".tdBtn").removeAttr("style")

	var o = {
		"planCustomerVarietyID": $(tr).find(".tdPlanCustomerVarietyID").text(),
		"campaignID": localStorage.campaignID,
		"customerID": $(tr).find(".tdClientID").text(),
		"programID": $(tr).find(".tdProgramID").text(),
		"destinationID": $(tr).find(".tdDestinationID").text(),
		"kamID": $(tr).find(".tdKAMID").text(),
		"priorityID": $(tr).find(".tdPiorityID").text(),
		"statusConfirmID": $(tr).find(".tdstatusConfirmID").text(),
		"sort": 0,
		"varietyID": $(tr).find(".tdVarietyID").text(),
		"categoryID": $(tr).find(".tdCategoryID").text(),
		"category": $(tr).find(".tdCategory").text(),
		"formatID": $(tr).find(".tdFormatID").text(),
		"packageProductID": $(tr).find(".tdTypeBoxID").text(),
		"brandID": $(tr).find(".tdBrandID").text(),
		"presentationID": $(tr).find(".tdPresentationID").text(),
		"labelID": $(tr).find(".tdLabelID").text(),
		"codepack": $(tr).find(".tdCodepack").text(),
		"codepackID": $(tr).find(".tdCodepackID").text(),
		"incotermID": $(tr).find(".tdIncotermID").text(),
		"conditionPaymentID": $(tr).find(".tdPriceConditions").text(),
		"paymentTermID": $(tr).find(".tdPaymentTerms").text(),
		"userID": $(lblSessionUserID).text(),
		"statusID": statusID,
		"sizeID": $(tr).find(".tdSizeID").text(),
		"colorID": $(tr).find(".tdColorID").text(),
		"planBoxWeek": []
	}
	$.each(dataWeek, function (iWeek, week) {
		var planBoxWeek = {
			"planBoxWeekID": 0,
			"planCustomerVarietyID": $(tr).find(".tdPlanCustomerVarietyID").text(),
			"projectedWeekID": week.projectedWeekID,
			"boxes": $(tr).find(".tdSalesProjected:eq(" + iWeek + ")").find("input.txtBoxNumber").val().replace(",", ""),
			"price": 0,
			"userID": $(lblSessionUserID).text()
		}
		o.planBoxWeek.push(planBoxWeek)

	})

	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/PlanCustomerVariety",
		//async: true,
		contentType: "application/json",
		dataType: 'json',
		data: JSON.stringify(o),
		success: function (datares) {
			if (datares[0].planCustomerVarietyID > 0) {
				if (statusID == 1) {
					$(tr).find(".tdPlanCustomerVarietyID").text(datares[0].planCustomerVarietyID)
					sweet_alert_success('Good Joob!', 'successfully saved');
				} else {
					sweet_alert_success('Deleted!', 'Your item has been deleted')
					var table = $(tr).closest('table')
					$(tr).remove()

					calculateProjectedSales(table);
				}

			} else {

			}
		},
		error: function (datoEr) {
			sweet_alert_error("Error", "There has been a problem!");
		}
	})
}

function rowEdit(tr) {
	$(modalCodepack).modal('show');
	$(btnAddClient).hide()
	$(btnEditClient).show()

	$(selectClient).attr('disabled', 'disabled');
	$(selectVariety).attr('disabled', 'disabled');
	$(selectCategory).attr('disabled', 'disabled');

	trEdit = tr;

	$selectClient = $(tr).find(".tdClientID").text()
	$selectProgram = $(tr).find(".tdProgramID").text()
	$selectDestination = $(tr).find(".tdDestinationID").text()
	$selectKAM = $(tr).find(".tdKAMID").text()
	$selectPriority = $(tr).find(".tdPiorityID").text()
	$selectStatusID = $(tr).find(".tdstatusConfirmID").text()

	$varietyID = $(tr).find(".tdVarietyID").text()
	$categoryID = $(tr).find(".tdCategoryID").text()
	$typeBoxID = $(tr).find(".tdTypeBoxID").text()
	$formatID = $(tr).find(".tdFormatID").text()
	$codepackID = $(tr).find(".tdCodepackID").text()	
	$brandID = $(tr).find(".tdBrandID").text()
	$presentationID = $(tr).find(".tdPresentationID").text()
	$labelID = $(tr).find(".tdLabelID").text()
	$sizeID = $(tr).find(".tdSizeID").text().split(',');
	$colorID = $(tr).find(".tdColorID").text().split(',');

	$codepack = $(tr).find(".tdCodepack").text()
	$boxPerPallet = $(tr).find(".tdBoxPerPallet").text()

	$incotemID = $(tr).find(".tdIncotermID").text()
	$paymentTerm = $(tr).find(".tdPaymentTerms").text()
	$priceCondition = $(tr).find(".tdPriceConditions").text()

	$("#selectClient").selectpicker('val', $selectClient).selectpicker('refresh').change();
	$("#selectProgram").selectpicker('val', $selectProgram).selectpicker('refresh');
	$("#selectDestination").val($selectDestination).selectpicker('refresh');
	$("#selectKAM").val($selectKAM).selectpicker('refresh');
	$("#selectPriority").val($selectPriority).selectpicker('refresh');
	$("#selectStatus").val($selectStatusID).selectpicker('refresh');

	$("#selectVariety").val($varietyID).selectpicker('refresh');
	$("#selectCategory").val($categoryID.trim()).selectpicker('refresh');
	$("#selectTypeBox").val($typeBoxID).change().selectpicker('refresh');
	$("#selectFormat").val($formatID).selectpicker('refresh').change();
	$("#selectCodepack").val($codepackID).selectpicker('refresh');		
	$("#selectBrand").val($brandID).selectpicker('refresh');
	$("#selectPresentation").val($presentationID).selectpicker('refresh');
	$("#selectLabel").val($labelID).selectpicker('refresh');
	$("#selectSize").multiselect('select', $sizeID).multiselect('refresh');
	$("#selectColor").multiselect('select', $colorID).multiselect('refresh');
	
	$("#txtCodepack").val($codepack)
	$("#txtBoxPerPallet").val($boxPerPallet)

	$(selectIncoterm).val($incotemID).selectpicker('refresh');
	$(selectPaymentTerms).selectpicker('val', $paymentTerm).selectpicker('refresh');
	$(selectPriceConditions).val($priceCondition).selectpicker('refresh');
		
}

function isNormalInteger(str) {
	return /^\+?(0|[1-9]\d*)$/.test(str);
}

function loadSelectVariety() {
	$(selectVariety).empty()
	$.each(dataVariety, function (i, item) {
		$(selectVariety).append($('<option>', {
			value: item.id,
			text: item.description
		}));
	});
}

function FillSelectCategory() {
	$(selectCategory).empty()
	$.each(dataCategory, function (i, item) {
		$(selectCategory).append($('<option>', {
			value: item.categoryID.trim(),
			text: item.description
		}));
	});
}

function FillSelectFormat() {
	$(selectFormat).empty()
	$.each(dataFormat, function (i, item) {
		$(selectFormat).append($('<option>', {
			value: item.id,
			text: item.description
		}));
	});
}

function FillSelectTypeBox() {
	$(selectTypeBox).empty()
	$.each(dataTypeBox, function (i, item) {
		$(selectTypeBox).append($('<option>', {
			value: item.id,
			text: item.description
		}));
	});
}

function loadSelectBrand() {
	$(selectBrand).empty()
	$.each(dataBrand, function (i, item) {
		$(selectBrand).append($('<option>', {
			value: item.id,
			text: item.description
		}));
	});
}

function loadSelectPresentation() {
	$(selectPresentation).empty()
	$.each(dataPresentation, function (i, item) {
		$(selectPresentation).append($('<option>', {
			value: item.id,
			text: item.description
		}));
	});
}

function loadSelectLabel() {
	$(selectLabel).empty()

	$.each(dataLabel, function (i, item) {
		$(selectLabel).append($('<option>', {
			value: item.id,
			text: item.description
		}));
	});
}

function calculeCodepack() {

	let sizeID = '';
	let varietyID;
	let formatID;
	let typeBoxID;
	let brandID;
	let presentationID;
	let labelID;
	var varietyCode;
	var formatCode;
	var typeBoxCode;
	var brandCode;
	var presentationCode;
	var labelCode;
	var sizeCode;
	var codepack = '';
	var pack = '';

	let arraySizeID = $(selectSize).val();
	$.each(arraySizeID, function (index, value) {
		sizeID += value + ',';
	});

	$.each(arraySizeID, function (index, value) {
		sizeID = value;
		varietyID = $(selectVariety).val()
		formatID = $(selectFormat).val()
		typeBoxID = $(selectTypeBox).val()
		brandID = $(selectBrand).val()
		presentationID = $(selectPresentation).val()
		labelID = $(selectLabel).val()

		varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
		formatCode = dataFormat.filter(item => item.id == formatID)[0].abbreviation;
		typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation;
		brandCode = dataBrand.filter(item => item.id == brandID)[0].abbreviation;
		presentationCode = dataPresentation.filter(item => item.id == presentationID)[0].abbreviation;
		labelCode = dataLabel.filter(item => item.id == labelID)[0].abbreviation;
		sizeCode = dataSize.filter(item => item.id == sizeID)[0].size;

		codepack = varietyCode.trim() + '_' +
			formatCode +
			typeBoxCode.slice(0, 1) +
			brandCode.trim() + '_' +
			(presentationCode + labelCode).slice(0, 3) + '1_' +
			sizeCode.trim() + '_NN_1';

		pack = (codepack.trim() == '') ? pack : codepack + ',' + pack;		

	});		

	$(txtCodepack).val(pack);

}

function DrawCommercialPlanStructureByCrop() {
	let bRsl = false;

	$.ajax({
		async: false,
		type: 'GET',
		url: "/CommercialPlan/GetVarCatByCrop?cropID=" + localStorage.cropID,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
			}
			else {
				var dataJson = JSON.parse(data);
				if (dataJson == null || dataJson.length == 0) {
					sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
				} else {
					dataVarCatByCrop = dataJson.map(
						obj => {
							return {
								varietyID: obj.varietyID,
								variety: obj.variety,
								categoryID: obj.categoryID,
								category: obj.category
							}
						}
					);
					bRsl = true;
				}
			}
		}
	});

	if (!bRsl) {
		sweet_alert_warning();
		return;
	}

	let dataCatByVar = [];
	var table = ''
	$.each(dataVariety, function (iVr, vr) {
		table += '<div class="card"><div>'
		table += '<div class="card-header">'
		table += '<button class="btn btn-info btn-sm btn-sm btn-block" style=" background-color: #435E85;" data-toggle="collapse" data-target="#Plan-VAR-' + vr.id + '" aria-expanded="true"> ' + vr.description + ' <span class="collapse-in"><i class="fas fa-arrow-down "></i></span> </button>'
		table += '</div>'
		table += '<div id="Plan-VAR-' + vr.id + '" class="collapse show" style="">'

		dataCatByVar = dataVarCatByCrop.filter(item => item.varietyID == vr.id);
		$.each(dataCatByVar, function (iCat, objCat) {
			table += '<div class="card-body p-3">'
			//>>>Begin table
			table += '<table style="max-width:100%; font-size:12px" class="table table-bordered table-hover table-responsive tblPlan tabla-sm table-cebra" id="tblPlan-VR-' + vr.id + '-' + objCat.categoryID + '">'
			//>>>Begin thead
			table += '<thead class="text-white" style="background-color: #091440; text-align:center;vertical-align:middle; font-size:12px">'

			//Drawing 1st row per variety
			table += '<tr>'
			table += '<th colspan="12" style="text-align:right; background-color:#1d325c" class="sticky">Harvest Week</th>'
			$.each(dataWeek, function (iWeek, week) {
				table += '<th class="thWeekHavest" style="background-color:#1d325c">Week ' + (parseInt(week.number) - parseInt(0)) + '</th>'
				//table += '<th class="thWeekHavest">Week ' + week.number + '</th>'
			})
			table += '<th>Harvest Week</th>'
			table += '</tr>'

			//Drawing 2nd row per variety
			table += '<tr>'
			table += '<th style="min-width:120px; max-width:120px; background-color:#27437b" class="sticky">[]</th>'
			table += '<th style="display:none">tdPlanCustomerVarietyID</th>'
			table += '<th style="min-width:120px; max-width:120px; background-color:#27437b" class="sticky2">Variety</th>'
			table += '<th style="min-width:60px;; max-width:60px; background-color:#27437b" class="sticky3">Category</th>'
			table += '<th style="min-width:150px; max-width:150px; background-color:#27437b" class="sticky4">Customer</th>'
			table += '<th style="min-width:150px; max-width:150px; background-color:#27437b" class="sticky5">Program</th>'
			table += '<th style="min-width:80px; max-width:80px; background-color:#27437b" class="sticky6">KAM</th>'
			table += '<th style="min-width:90px; max-width:90px; background-color:#27437b" class="sticky7">Priority</th>'
			table += '<th style="min-width:60px; max-width:60px; background-color:#27437b" class="sticky8">Status</th>'
			table += '<th style="min-width:100px; max-width:100px; background-color:#27437b" class="sticky9">Destination</th>'
			table += '<th style="min-width:50px; max-width:50px; background-color:#27437b" class="sticky10">Weight</th>'
			table += '<th style="min-width:100px; max-width:100px; background-color:#27437b" class="sticky11">Box Brand</th>'
			table += '<th style="min-width:177px; max-width:177px; background-color:#27437b" class="sticky12">Codepack</th>'
			table += '<th style="display:none">customerID</th>'
			table += '<th style="display:none">consigneeID</th>'
			table += '<th style="display:none">destinationID</th>'
			table += '<th style="display:none">KAMID</th>'
			table += '<th style="display:none">priorityID</th>'
			table += '<th style="display:none">statusID</th>'
			table += '<th style="display:none">IncotermID</th>'
			table += '<th style="display:none">PaymentTerms</th>'
			table += '<th style="display:none">PriceConditions</th>'
			table += '<th style="display:none">varietyID</th>'
			table += '<th style="display:none">categoryID</th>'
			table += '<th style="display:none">formatID</th>'
			table += '<th style="display:none">packageProductID</th>'
			table += '<th style="display:none">brandID</th>'
			table += '<th style="display:none">presentationID</th>'
			table += '<th style="display:none">labelID</th>'
			table += '<th style="display:none">boxPerPallet</th>'
			table += '<th style="display:none">codepackID</th>'
			table += '<th style="display:none">sizeID</th>'
			table += '<th style="display:none">colorID</th>'
			$.each(dataWeek, function (iWeek, week) {
				table += '<th class="thWeek" style="background-color:#27437b"> Week ' + (parseInt(week.number) + parseInt(1)) + '</th>'
			})
			table += '<th class="thWeek" style="background-color:#27437b"> Total</th>'
			table += '</tr>'

			//Drawing 3th row per variety
			table += '<tr class="text-align-right trForecast" style="">'

			table += '<th class="sticky" ></th>'
			table += '<th style="display:none"></th>'
			table += '<th class="sticky2" style="min-width:120px; max-width:120px;"> ' + vr.description + ' </th>'
			table += '<th class="sticky3" style="min-width:60px; max-width:60px;">' + objCat.category + '</th>'
			table += '<th class="sticky4" style="min-width:150px; max-width:150px;"></th>'
			table += '<th class="sticky5" style="min-width:150px; max-width:150px;"></th>'
			table += '<th class="sticky6" style="min-width:80px; max-width:80px;"></th>'
			table += '<th class="sticky7" style="min-width:90px; max-width:90px;"></th>'
			table += '<th class="sticky8" style="min-width:60px; max-width:60px;"></th>'
			table += '<th class="sticky9" style="min-width:100px; max-width:100px;"></th>'
			table += '<th class="sticky10 tdBoxWeight" style="min-width:50px; max-width:50px; text-align:center">8.2</th>'
			table += '<th class="sticky11" style="min-width:100px; max-width:100px;"></th>'
			table += '<th class="sticky12" style="min-width:177px; max-width:177px;"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none">IncotermID</th>'
			table += '<th style="display:none">PaymentTerms</th>'
			table += '<th style="display:none">PriceConditions</th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			table += '<th style="display:none"></th>'
			$.each(dataWeek, function (iWeek, week) {
				table += '<th class="thForecastWeek">0</th>'
			})
			table += '<th class="thTotalForecastWeek">0</th>'
			table += '</thead>'
			//<<<End thead

			//table += '<tbody id="tbodyCat-1" class="table-primary">'
			table += '<tbody id="tbodyCat-1" class="">'
			table += '</tbody>'

			table += '<tfoot>'
			table += '<tr style="background-color:#65939b;text-align:right" class="trProjectedSales">'
			table += '<td colspan="12" style="background-color:#65939b;text-align:right" class="sticky">Projected Sales Allocation (8.2 KG)</td>'
			$.each(dataWeek, function (iWeek, week) {
				table += '<td class="tdProjectedSales">' + '0' + '</td>'
			})
			table += '<td class="tdTotalProjectedSales">0</td>'
			table += '</tr>'
			table += '<tr style="background-color:#4e73df;text-align:right" class="trBalance">'
			table += '<td colspan="12" style="background-color:#4e73df;text-align:right" class="sticky">Balance (8.2 KG)</td>'
			$.each(dataWeek, function (iWeek, week) {
				table += '<td class="tdBalance ">' + '0' + '</td>'
			})
			table += '<td class="tdTotalBalance">0</td>'
			table += '</tr>'
			table += '</tfoot>'
			table += '</table>'
			//<<<End table
			table += '</div>'
		});
		table += '</div></div></div>'
	})
	$(tblVarietys).html(table)

}

function addClientCodepack() {

	if ($("#selectCodepack").val() == null) {
		sweet_alert_warning('error', 'choose box size')
		return
	}

	if ($(txtCodepack).val() == '') {
		sweet_alert_warning('error', 'choose codepack')
		return
	}

	$selectClient = $("#selectClient option:selected")
	$selectProgram = $("#selectProgram option:selected")
	$selectDestination = $("#selectDestination option:selected")
	$selectKAM = $("#selectKAM option:selected")
	$selectPriority = $("#selectPriority option:selected")
	$selectStatusID = $("#selectStatus option:selected")

	$categoryID = $("#selectCategory option:selected")
	$varietyID = $("#selectVariety option:selected")
	$typeBoxID = $("#selectTypeBox option:selected")
	$formatID = $("#selectFormat option:selected")
	$codepackID = $("#selectCodepack option:selected")	
	$brandID = $("#selectBrand option:selected")
	$presentationID = $("#selectPresentation option:selected")
	$labelID = $("#selectLabel option:selected")
	$sizeID = $("#selectSize")
	$colorID = $("#selectColor")

	$selectIncoterm = $("#selectIncoterm option:selected")
	$selectPaymentTerms = $("#selectPaymentTerms option:selected")
	$selectPriceConditions = $("#selectPriceConditions option:selected")
	//$codepack = $(txtCodepack).val()

	var table = ''
	if ($($selectStatusID).val() == "1") {
		table = '<tr style="text-align:center ">'
	} else {
		table = '<tr style="text-align:center" class="">'
	}
	let strSize = GetTextMultiselect('selectSize', 1);
	let strColor = GetTextMultiselect('selectColor', 1);

	table += '<td style="background-color:#F9E79F" class="tdBtn sticky" ><button style="font-size:14px" class="btnRowDelete btn btn-sm btn-danger"><i class="fas fa-trash"></i></button><button class="btnRowEdit btn btn-sm btn-primary" style="font-size:14px"><i class="fas fa-edit"></i></button><button class="btnRowSave btn btn-sm btn-success" style="font-size:14px"><i class="fas fa-save"></i></button></td>'
	table += '<td style="display:none" class="tdPlanCustomerVarietyID">0</td>'
	table += '<td style="min-width:120px" class="tdVariety sticky2">' + $($varietyID).text() + '</td>'
	table += '<td style="min-width:80px" class="tdCategory sticky3">' + $($categoryID).text() + '</td>'
	table += '<td style="min-width:200px" class="tdClient sticky4">' + $($selectClient).text() + '</td>'
	table += '<td style="min-width:120px" class="tdProgram sticky5">' + $($selectProgram).text() + '</td>'
	table += '<td style="min-width:80px" class="tdKAM sticky6">' + $($selectKAM).text() + '</td>'

	table += '<td style="min-width:80px" class="tdPiority sticky7">' + $($selectPriority).text() + '</td>'
	table += '<td style="min-width:80px" class="tdStatus sticky8">' + $($selectStatusID).text() + '</td>'
	table += '<td style="min-width:80px" class="tdDestination sticky9">' + $($selectDestination).text() + '</td>'

	table += '<td style="min-width:120px" class="tdBoxWeight sticky10">' + $($formatID).text() + '</td>'
	table += '<td style="min-width:120px" class="tdBrand sticky11">' + $($brandID).text() + '</td>'
	table += '<td style="min-width:120px" class="tdCodepack sticky12">' + $(txtCodepack).val() + '</td>'

	table += '<td style="display:none" class="tdClientID">' + $($selectClient).val() + '</td>'
	table += '<td style="display:none" class="tdProgramID">' + $($selectProgram).val() + '</td>'
	table += '<td style="display:none" class="tdDestinationID">' + $($selectDestination).val() + '</td>'
	table += '<td style="display:none" class="tdKAMID">' + $($selectKAM).val() + '</td>'
	table += '<td style="display:none" class="tdPiorityID">' + $($selectPriority).val() + '</td>'
	table += '<td style="display:none" class="tdstatusConfirmID">' + $($selectStatusID).val() + '</td>'

	table += '<td style="display:none" class="tdIncotermID">' + $($selectIncoterm).val() + '</td>'
	table += '<td style="display:none" class="tdPaymentTerms">' + $($selectPaymentTerms).val() + '</td>'
	table += '<td style="display:none" class="tdPriceConditions">' + $($selectPriceConditions).val() + '</td>'

	table += '<td style="display:none" class="tdVarietyID">' + $($varietyID).val() + '</td>'
	table += '<td style="display:none" class="tdCategoryID">' + $($categoryID).val() + '</td>'
	table += '<td style="display:none" class="tdFormatID">' + $($formatID).val() + '</td>'
	table += '<td style="display:none" class="tdTypeBoxID">' + $($typeBoxID).val() + '</td>'
	table += '<td style="display:none" class="tdBrandID">' + $($brandID).val() + '</td>'
	table += '<td style="display:none" class="tdPresentationID">' + $($presentationID).val() + '</td>'
	table += '<td style="display:none" class="tdLabelID">' + $($labelID).val() + '</td>'
	table += '<td style="display:none" class="tdBoxPerPallet">' + '100' + '</td>'
	table += '<td style="display:none" class="tdCodepackID">' + $($codepackID).val() + '</td>'
	table += '<td style="display:none" class="tdSizeID">' + strSize + '</td>'
	table += '<td style="display:none" class="tdColorID">' + strColor + '</td>'

	$.each(dataWeek, function (iWeek, week) {
		table += '<td style="text-align:right;" class="tdSalesProjected" contenteditable="true" >';
		table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/>';
		table += '</td>';
	})

	table += '<td class="tdTotalSalesProjected" contenteditable="true">0</td>'
	table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/>';
	table += '</tr>'
	var id = 'tblPlan-VR-' + $varietyID.val() + '-' + $categoryID.val()
	$("#" + id + ">tbody").append(table);

}

function ShowCalculateTotalBoxexByWeekForEachVariety() {
	let dataCatByVar = [];
	$.each(dataVariety, function (iVr, vr) {
		dataCatByVar = dataVarCatByCrop.filter(item => item.varietyID == vr.id);
		$.each(dataCatByVar, function (iCat, objCat) {
			var trCat = $('#tblPlan-VR-' + vr.id + '-' + objCat.categoryID + '>thead>tr.trForecast')
			let dataForecastCat = dataForecast.filter(item => item.categoryID == objCat.categoryID && item.varietyID == vr.id);
			var totalHarvest = 0;
			$.each(dataWeek, function (iWeek, week) {
				var mount = dataForecastCat.filter(item => item.projectedWeekID == week.projectedWeekID + 0)
				if (mount.length >= 1) {
					$(trCat).find('.thForecastWeek:eq(' + iWeek + ')').html(mount[0].BOXES);
					totalHarvest = parseInt((mount[0].BOXES).replace(',', '')) + totalHarvest;
				}
			});
			$(trCat).find('.thTotalForecastWeek').html(Number(totalHarvest).toLocaleString('en'));
		});

	});
}

function calculateProjectedSales(table) {
	var trForecast = $(table).find('thead>tr.trForecast')
	var tbody = $(table).find('tbody')

	var trProjectedSales = $(table).find('tfoot>tr.trProjectedSales')
	var trBalance = $(table).find('tfoot>tr.trBalance')

	var balance = 0
	var totalForecast = 0
	$.each(dataWeek, function (iWeek, week) {
		var mountForecast = $(trForecast).find('th.thForecastWeek:eq(' + iWeek + ')').text()
		totalForecast = totalForecast + parseInt(mountForecast.replace(',', ''));
		var mountTotalSales = 0;
		$(tbody).find('tr').each(function (iTr, tr) {
			var formatKg = parseFloat($(tr).find('td.tdBoxWeight').text())
			mountTotalSales = (parseFloat($(tr).find('td.tdSalesProjected:eq(' + iWeek + ')').find('input.txtBoxNumber').val().replace(',', '')) * formatKg) + mountTotalSales;
		})
		$(trProjectedSales).find('td.tdProjectedSales:eq(' + iWeek + ')').text(Number((mountTotalSales / 8.2).toFixed(0)).toLocaleString('en'))

		balance = parseInt(mountForecast.replace(',', '')) - (mountTotalSales / 8.2).toFixed(0) + balance
		if (balance < 0) {
			$(trBalance).find('td.tdBalance:eq(' + iWeek + ')').text(Number(balance).toLocaleString('en')).css('background-color', 'red').css('color', 'white');
		} else {
			$(trBalance).find('td.tdBalance:eq(' + iWeek + ')').text(Number(balance).toLocaleString('en')).removeAttr('style');
		}

	})

	$(tbody).find('tr').each(function (iTr, tr) {
		var boxesByRow = 0
		$(tr).find('td.tdSalesProjected').each(function (iTd, td) {
			boxesByRow = parseInt(($(td).find('input.txtBoxNumber').val()).replace(',', '')) + (boxesByRow);

		})

		$(tr).find('td.tdTotalSalesProjected').text(Number(boxesByRow).toLocaleString('en'));
	})

	var totalBoxSales = 0
	$(trProjectedSales).find('td.tdProjectedSales').each(function (iTd, td) {
		totalBoxSales = parseInt($(td).text().replace(',', '')) + totalBoxSales
	})
	$(trProjectedSales).find('td.tdTotalProjectedSales').text(Number(totalBoxSales).toLocaleString('en'));

	var totalBalance = totalForecast - totalBoxSales

	$(trBalance).find('td.tdTotalBalance').text(Number(totalBalance).toLocaleString('en'))
}

function loadData() {
	FillSelectCategory();
	FillSelectFormat();
	FillSelectTypeBox();
	GetFillCustomer();
	GetFillProgram();
	GetFillSelectVariety();
	GetFillSelectBrand();	
	GetFillSelectPresentation();
	GetFillSelectLabel();
	GetFillSizes();
	GetFillColor();
	GetFillSelectIncoterm();
	GetFillSelectPriceCondition();
	GetFillSelectPaymentTerm();
	GetFillSelectKam();
	sweet_alert_progressbar_cerrar();
}

function GetFillSelectKam() {
	$(selectKAM).empty();

	$('#selectKAM').append($('<option>', {
		value: "0",
		text: ""
	}));

	dataKam = []
	opt = "all";
	$.ajax({
		type: 'GET',
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/Sales/ListKam?opt=" + opt + "&id=" + '',
		//async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectKAM).append($('<option>', {
						value: item.kamID,
						text: item.kam
					}));
				})

				dataKam = dataJson.map(
					obj => {
						return {
							id: obj.kamID,
							description: obj.kam
						}
					}
				);
			}
			$('#selectKAM').selectpicker('refresh');
		}
	});

}

function GetFillSelectLabel() {
	$(selectLabel).empty();
	dataLabel = []
	opt = "lbl";
	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/LabelByCrop?opt=" + opt + "&cropID=" + localStorage.cropID,
		//async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectLabel).append($('<option>', {
						value: item.labelID,
						text: item.label
					}));
				})

				dataLabel = dataJson.map(
					obj => {
						return {
							abbreviation: obj.abbreviation,
							id: obj.labelID,
							description: obj.label
						}
					}
				);
			}
		}
	});

}

function GetFillSelectPresentation() {
	$(selectPresentation).empty();
	dataPresentation = [];
	var opt = 'pci';
	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/PresentationByCropID?opt=" + opt + "&id=" + localStorage.cropID,
		//async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectPresentation).append($('<option>', {
						value: item.presentationID,
						text: item.presentation
					}));
				})

				dataPresentation = dataJson.map(
					obj => {
						return {
							abbreviation: obj.presentation,
							id: obj.presentationID,
							description: obj.description
						}
					}
				);
			}
		}
	});
}

function GetFillCustomer() {
	$('#selectClient').empty()

	$('#selectClient').append($('<option>', {
		value: "0",
		text: ""
	}));

	var opt = 'use';
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/CustomerByCrop?opt=" + opt + "&id=" + localStorage.cropID,
		headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$('#selectClient').append($('<option>', {
						value: item.customerID,
						text: item.name
					}));

				})

			}
			$('#selectClient').selectpicker('refresh').change();
		}
	});
}

function GetFillProgram() {
	$(selectProgram).empty()

	$('#selectProgram').append($('<option>', {
		value: "0",
		text: ""
	}));

	var opt = 'all';
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/CustomerByCrop?opt=" + opt + "&id=" + localStorage.cropID,
		headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {					
					$('#selectProgram').append($('<option>', {
						value: item.customerID,
						text: item.name
					}));

				})

			}
			$('#selectProgram').selectpicker('refresh');
		}
	});
}

function loadDestination(customerID) {
	$(selectDestination).empty();

	$('#selectDestination').append($('<option>', {
		value: "0",
		text: ""
	}));

	var opt = 'cus';
	var id = customerID;
	var mar = '';
	var via = '';
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/DestinationByCustomerID?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
		async: false,
		headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$('#selectDestination').append($('<option>', {
						value: item.destinationID,
						text: item.destination
					}));
				})
			}
			$('#selectDestination').selectpicker('refresh');
		}
	});
}

function GetFillSelectVariety() {
	$(selectVariety).empty();

	dataVariety = [];
	var opt = 'cro';
	var log = '';
	var sUrlApi = "/CommercialPlan/VarietyByCrop?opt=" + opt + "&id=" + localStorage.cropID + "&log=" + log
	var bRsl = false;
	$.ajax({
		type: "GET",
		url: sUrlApi,
		async: false,
		headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
			} else {
				var dataJson = JSON.parse(data);
				if (dataJson == null || dataJson.length == 0) {
					sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
				} else {
					$.each(dataJson, function (i, item) {
						$(selectVariety).append($('<option>', {
							value: item.varietyID,
							text: item.name
						}));
					});
					dataVariety = dataJson.map(
						obj => {
							return {
								abbreviation: obj.abbreviation,
								id: obj.varietyID,
								description: obj.name
							}
						}
					);
					iTotVarietyXCategory = (dataVariety.length) * 2 + 1;
					bRsl = true;
				}
			}
		},
		error: function (datoEr) {
			sweet_alert_info('Information', "There was an issue trying to list data.");
		},
		complete: function () {
			if (bRsl) {
				//sweet_alert_progressbar();
				DrawCommercialPlanStructureByCrop();
				ShowCalculateTotalBoxexByWeekForEachVariety();
				GetCommercialPlanByCampaign();
			}
		},
	});

}

function GetFillSelectBrand() {
	$(selectBrand).empty()
	dataBrand = []
	var opt = 'cro';
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/BrandByCrop?opt=" + opt + "&cropID=" + localStorage.cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectBrand).append($('<option>', {
						value: item.brandID,
						text: item.description
					}));
				})

				dataBrand = dataJson.map(
					obj => {
						return {
							abbreviation: obj.abbreviation,
							id: obj.brandID,
							description: obj.description
						}
					}
				);
			}
		}
	});
}

function loadSizeBox() {
	var formatID = $(selectFormat).val()
	var packageProductID = $(selectTypeBox).val()
	$(selectCodepack).empty()
	dataSizeBox = [];
	var opt = "pcr";

	$.ajax({
		type: "GET",
		url: "/CommercialPlan/CodepackByFormatIDMC?opt=" + opt + "&viaid=" + formatID + "&cropID=" + packageProductID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectCodepack).append($('<option>', {
						value: item.codePackID,
						text: item.codePack
					}));
				})
				dataSizeBox = dataJson;
			}
			$(selectCodepack).change()
		}
	});
}

function GetFillSizes() {
	$(selectSize).empty()

	var opt = "cro";
	var id = localStorage.cropID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectSize).append($('<option>', {
						value: item.sizeID,
						text: item.sizeID
					}));
				})

				SettingCustomizedMultiselect($('#selectSize'));

				dataSize = dataJson.map(
					obj => {
						return {
							id: obj.sizeID,
							size: obj.sizeID
						}
					}
				);
			}
		}
	});
}

function GetFillColor() {
	$(selectColor).empty()

	var cropID = localStorage.cropID;
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListColorByFilter?cropID=" + cropID,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectColor).append($('<option>', {
						value: item.colorID,
						text: item.color
					}));
				})

				SettingCustomizedMultiselect($('#selectColor'));

				dataColor = dataJson.map(
					obj => {
						return {
							id: obj.colorID,
							color: obj.color
						}
					}
				);
			}
		}
	});
}

function GetFillSelectIncoterm() {
	$(selectIncoterm).empty()

	$('#selectIncoterm').append($('<option>', {
		value: "0",
		text: ""
	}));

	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/IncotermByViaID?cropID=" + localStorage.cropID + "&viaID=" + "1",
		//async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);

			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectIncoterm).append($('<option>', {
						value: item.incotermID,
						text: item.incoterm
					}));
				})
			}
			$(selectIncoterm).selectpicker('refresh')
		}
	});
}

function GetFillSelectPriceCondition() {
	$(selectPriceConditions).empty()

	$('#selectPriceConditions').append($('<option>', {
		value: "0",
		text: ""
	}));

	var opt = 'all';
	var id = '';
	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/PriceCondition?opt=" + opt + "&id=" + id,
		//async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectPriceConditions).append($('<option>', {
						value: item.conditionPaymentID,
						text: item.description
					}));
				})
			}
			$(selectPriceConditions).selectpicker('refresh')
		}
	});
}

function GetFillSelectPaymentTerm() {
	$(selectPaymentTerms).empty()

	$('#selectPaymentTerms').append($('<option>', {
		value: "0",
		text: ""
	}));

	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/CommercialPlan/PaymentTermByCropID?cropID=" + localStorage.cropID,
		//async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (dataJson.length > 0) {
				$.each(dataJson, function (i, item) {
					$(selectPaymentTerms).append($('<option>', {
						value: item.paymentTermID,
						text: item.description
					}));
				})
			}
			$(selectPaymentTerms).selectpicker('refresh')
		}
	});
}

function GetCommercialPlanByCampaign() {
	var sUrlApi = "/CommercialPlan/PlanCustomerVarietyList?campaignID=" + localStorage.campaignID;
	var bRsl = false;
	$.ajax({
		type: "GET",
		url: sUrlApi,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
			} else {
				var dataJson = JSON.parse(data);
				if (dataJson == null || dataJson.length == 0) {
					sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
				} else {
					dataPlan = dataJson;
					bRsl = true;
				}
			}
		},
		error: function (datoEr) {
			sweet_alert_info('Information', "There was an issue trying to list data.");
		},
		complete: function () {
			if (bRsl) {
				//sweet_alert_progressbar();
				ShowCommercialPlanForEachVarietyAndCategory();

			}

		}
	});

}

function validarenvio(data) {

	var grupo = 0;
	var DestinationID = 0;
	var CustomerID = 0;
	var result = true;

	for (var i = 0; i < data.length; i++) {
		if (i == 0) {
			grupo = data[i].Group;
			DestinationID = data[i].DestinationID;
			CustomerID = data[i].CustomerID;
			continue;
		}

		if (grupo == data[i].Group) {
			if (DestinationID != data[i].DestinationID || CustomerID != data[i].CustomerID) {
				result = false;
				return result;
			}
		}
		else {
			grupo == data[i].Group
			DestinationID = data[i].DestinationID;
			CustomerID = data[i].CustomerID;
		}

	}

	return result;
}

function GenerateSaleRequestAndPO() {
	var CPdetail = [];
	var _CPdetail = {};
	let iGroup = 0;
	let iTotaRows = 0;
	var boolPallest = 0;

	$('table#tblToProcess>tbody>tr').each(function (index, row) {
		var iStatus = parseInt(jQuery.trim($(row).find('td.tdStatus').text()));
		if (iStatus == 0) { boolPallest = 1; }
		if (iStatus == 1) {
			iTotaRows++;
			if (iGroup != parseInt(jQuery.trim($(row).find('td.tdSort').text()))) {
				iGroup = parseInt(jQuery.trim($(row).find('td.tdSort').text()));
			}
			_CPdetail = {};
			_CPdetail["Id"] = (parseInt($(row).find('td.tdVarietyXCategory').text()) == 0) ? iTotaRows : parseInt($(row).find('td.tdVarietyXCategory').text());
			_CPdetail["PlanCustomerVarietyID"] = parseInt($(row).find('td.tdplanCustomerVarietyID').text());
			_CPdetail["VarietyID"] = parseInt($(row).find('td.tdvarietyID').text());
			_CPdetail["CategoryID"] = parseInt($(row).find('td.tdcategoryID').text());
			_CPdetail["CustomerID"] = parseInt($(row).find('td.tdcustomerID').text());
			_CPdetail["Program"] = $(row).find('td.tdprogram').text();
			_CPdetail["Priority"] = $(row).find('td.tdpriority').text();
			_CPdetail["DestinationID"] = parseInt($(row).find('td.tddestinationID').text());
			_CPdetail["CodePack"] = $(row).find('td.tdcodePack').text();
			_CPdetail["BoxesPerPallet"] = parseInt($(row).find('td.tdboxesPerPallet').text());
			_CPdetail["ToProcess"] = parseInt($(row).find('td.tdtoProcess').text());
			_CPdetail["Pallets"] = parseInt($(row).find('td.tdPallets').text());
			_CPdetail["Group"] = iGroup;
			CPdetail.push(_CPdetail);
		}
	});
	if (boolPallest > 0) {
		sweet_alert_info('Error!', 'You must enter the total number of pallets to be processed.');
		return;
	}

	//Validamos los contenedores pertenezcan al mismo customer y destino
	if (!validarenvio(CPdetail)) {
		sweet_alert_info('Error!', 'Fail Pallets must belong to the same customer and destination.');
		return;
	}

	var scropId = localStorage.cropID;
	var icampaignID = localStorage.campaignID;
	var iuserCreated = $('#lblSessionUserID').text();
	var sWeekSelected = $('#selectWeekProcess option:selected').val();
	let jsonObjCPdetail = JSON.stringify({
		cropId: scropId,
		campaignID: icampaignID,
		userCreated: iuserCreated,
		projectedWeekID: sWeekSelected,
		planComercialDetail: CPdetail,
	});

	var bRsl = false;
	var sUrlApi = "/CommercialPlan/CreateSaleRequestAndPO";
	var sSaleRequestCodes = '';
	$.ajax({
		type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: sUrlApi,
		data: jsonObjCPdetail,
		contentType: "application/json",
		Accept: "application/json",
		dataType: 'json',
		async: false,
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_info('Information', 'There has been a problem when saving the information. Try again later.');
			} else {
				$.each(data, function (iData, objData) {
					sSaleRequestCodes = sSaleRequestCodes + '; ' + objData.referenceNumber
				});
				sSaleRequestCodes = sSaleRequestCodes.substring(1, sSaleRequestCodes.length - 1);
				bRsl = true;
			}

		},
		error: function (datoEr) {
			sweet_alert_info('Error!', 'Fail Sale Requests and POs Created.');
		},
		complete: function () {
			if (bRsl) {
				//sweet_alert_progressbar();
				sweet_alert_success_prg_copytoclipboard('Good Job!', 'Sale Requests and POs created. S.R.Codes:', sSaleRequestCodes);
				cleartblToProcess();
			}

		}

	});

}

function cleartblToProcess() {
	$('#tblToProcess>tbody').empty();
	var trAdd = '';
	let dataCatByVar = [];
	$.each(dataVariety, function (i, variety) {
		dataCatByVar = dataVarCatByCrop.filter(item => item.varietyID == variety.id);
		$.each(dataCatByVar, function (iCat, objCat) {
			trAdd = '<tr class="tdvariety var' + variety.id + ' nodrag">'
			trAdd += '<td style="display:none" class="tdVarietyXCategory">' + 0 + '</td>'
			trAdd += '<td style="display:none" class="tdplanCustomerVarietyID">' + 0 + '</td>'
			trAdd += '<td style="display:none" class="tdvarietyID">' + variety.id + '</td>'
			trAdd += '<td style="display:none" class="tdcategoryID">' + objCat.categoryID.trim() + '</td>'
			trAdd += '<td style="display:none" class="tdcustomerID">' + 0 + '</td>'
			trAdd += '<td style="display:none" class="tddestinationID">' + 0 + '</td>'
			trAdd += '<td class="tdvariety var' + variety.id + '">' + variety.description + '</td>'
			trAdd += '<td class="tdcategory cat' + objCat.categoryID.trim() + '">' + objCat.category + '</td>'
			trAdd += '<td class="tdcustomer"></td>'
			trAdd += '<td class="tdprogram"></td>'
			trAdd += '<td class="tdpriority"></td>'
			trAdd += '<td class="tddestination"></td>'
			trAdd += '<td class="tdcodePack"></td>'
			trAdd += '<td class="tdboxesPerPallet"></td>'
			trAdd += '<td class="tdtoProcess"></td>'
			trAdd += '<td class="tdPallets" ></td>'
			trAdd += '<td class="tdDelete"></td>'
			trAdd += '<td></td>'
			trAdd += '<td style="display:none" class="tdStatus">2</td>'
			trAdd += '<td style="display:none" class="tdMixed" >0</td>'
			trAdd += '</tr>'
			$('#tblToProcess>tbody').append(trAdd);
		});
	});
	$("#tblToProcess").tableDnD({
		onDragStop: function (table, row) {
			verifyRowsResidues()
		}
	});
}

$(document).ready(function () {
	$('#tblCommercialPlan thead tr').clone(true).appendTo('#tblCommercialPlan thead');
	$('#tblCommercialPlan thead tr:eq(1) td').each(function (i) {
		if (i < 14) {
			var title = $(this).text();
			$(this).html('<input type="text" />');
			$('input', this).on('keyup change', function () {
				if (table.column(i).search() !== this.value) {
					table
						.column(i)
						.search(this.value)
						.draw();
				}
			});
		} else {
			$(this).html('');
		}
	});

});

function SettingCustomizedMultiselect(obj) {
	var orderCount = 0;
	$(obj).multiselect({
		buttonWidth: '75px',
		includeSelectAllOption: true,
		nonSelectedText: 'Select expertise!',
		maxHeight: '1000px',
		dropUp: true,
		enableFiltering: false,
		onChange: function (option, checked) {
			if (checked) {
				orderCount++;
				$(option).data('order', orderCount);
			}
			else {
				$(option).data('order', '');
			}
		},
		buttonText: function (options) {
			if (options.length === 0) {
				return 'Choose';
			}
			else if (options.length > 3) {
				return options.length + ' selected';
			}
			else {
				var selected = [];
				options.each(function () {
					selected.push([$(this).text(), $(this).data('order')]);
				});

				selected.sort(function (a, b) {
					return a[1] - b[1];
				});

				var text = '';
				for (var i = 0; i < selected.length; i++) {
					text += selected[i][0] + ', ';
				}

				return text.substr(0, text.length - 2);
			}
		},
	});
}