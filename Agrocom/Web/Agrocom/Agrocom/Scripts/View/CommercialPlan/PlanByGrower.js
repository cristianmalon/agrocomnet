﻿var sessionUserID = $('#lblSessionUserID').text()
var idSeason = '';
var idGrower = '';
var dataForecast = [];
var dataWeeks = [];
var idPlanOrigin = '';
var dataPlanAlternativeAprove = []
var dataMartet = [];
var dataOringinMarket = [];
var dataFormatMarket = [];
var dataCustomerMarket = []
var dataDestinationCustomer = []
var dataPriority = [
	{ "priorityID": 1, "priority": "Strategic" },
	{ "priorityID": 2, "priority": "Tactical" },
	{ "priorityID": 3, "priority": "Development" }
];
var dataResumen =
{
	"A": [],
	"B": [],
	"C": [],
	"D": [],
	"E": [],
	"F": [],//FORECAST
	"O": [],//OFERTED
}
$(function () {
	$(document).ajaxStart(function () {
		$("#wait").css("display", "block");
	});
	$(document).ajaxComplete(function () {
		$("#wait").css("display", "none");
	});
    list();
    

    
});

function list() {
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanForGrower?userID=" + sessionUserID,
        async: false,
        success: function (data) {
            //console.log(data);
            var dataList = JSON.parse(data);

            if (data.length > 0) {
                var content = ''

                $.each(dataList, function (i, e) {
                    content += '<tr>'
                    content += '<td style="display:none">' + e.planID + '</td><td>' + e.plan + '</td>'
                    content += '<td>' + e.season + '</td>'
                    content += '<td>' + e.crop + '</td>'
                    content += '<td>' + e.origin + '</td>'
                    content += '<td>' + e.businessName + '</td>'
                    content += '<td>' + e.dateLastModified + '</td>'
                    content += "<td><button class='btn btn-primary' onclick='openModal(" + e.planID + ")' data-toggle='modal' data-target='#myModal'><i class='fas fa-play'></i></button></td>"
                    content += '</tr>'
                })
                $("table#table tbody").append(content);
            }


        }
    });
	$('#table').dataTable()({
		//dom: 'Bfrtip',	
		//searching: true,
		//"info": true,
		//"lengthChange": true,
		select: true,
		lengthMenu: [
			[10, 25, 50, -1],
			['10 rows', '25 rows', '50 rows', 'All']
		],
		"language": {
			"search": "Filtrar"
		},
		dom: 'Bfrtip',
		buttons: [
			//'excel'
			{ extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
			'pageLength'
		]
	});
}

function loadPlanData(idPlan) {
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanById?id=" + idPlan,
        async: false,
        success: function (data) {
            if (data.length > 0) {

                var dataJson = JSON.parse(data);
                console.log(dataJson);

                idSeason = dataJson[0].campaignID;
                idGrower = dataJson[0].growerID;
                idPlanOrigin = dataJson[0].planOriginID;
            }
        }
    });
}


function openModal(idPlan) {
	$(document).ajaxStart(function () {
		$("#wait").css("display", "block");
	});

    idSeason = 0;
    idGrower = 0;
    dataForecast = [];
	dataWeeks = []
	idPlanOrigin = '';
	dataPlanAlternativeAprove = []
	dataMartet = [];
	dataOringinMarket = [];
	dataFormatMarket = [];
	dataCustomerMarket = []
	dataDestinationCustomer = []
	dataResumen =
	{
		"A": [],
		"B": [],
		"C": [],
		"D": [],
		"E": [],
		"F": [],//FORECAST
		"O": [],//OFERTED
	}
	divTableMarket

	$("#divTableMarket").html('');
	$("#nav-tab").html('');
	$("#nav-tabContent").html('');

    loadPlanData(idPlan)

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListWeekBySeason?idSeason=" + idSeason,
        async: false,
        success: function (data) {
            dataWeeks = JSON.parse(data);
            //console.log(data);
        }
    });

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListForecast?idGrower=" + idGrower + "&idSeason=" + idSeason,
        async: false,
        success: function (data) {
            dataForecast = JSON.parse(data);
        }
	});
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListMarket",
		async: false,
		success: function (data) {
			dataMartet = JSON.parse(data);
			console.log(data);
		}
	});
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListOrigin",
		async: false,
		success: function (data) {
			dataOringinMarket = JSON.parse(data);
			//console.log(data);
		}
	});

	//$.ajax({
	//	type: "GET",
	//	url: "/CommercialPlan/ListCodepackWithVia/?cropID=BLU",
	//	async: false,
	//	success: function (data) {
	//		dataFormatMarket = JSON.parse(data);
	//		//console.log(data);
	//	}
	//});
	//$.ajax({
	//	type: "GET",
	//	url: "/CommercialPlan/ListAllWithMarket",
	//	async: false,
	//	success: function (data) {
	//		dataCustomerMarket = JSON.parse(data);
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/CommercialPlan/ListDestinationAllWithCustomer",
	//	async: false,
	//	success: function (data) {
	//		dataDestinationCustomer = JSON.parse(data);
	//	}
	//});
    

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListPlanAlternative?planOriginID=" + idPlanOrigin,
        async: false,
        success: function (data) {
            if (data.length > 0) dataPlanAlternative = JSON.parse(data);
            //console.log(data);
        }
	});
	$(document).ajaxComplete(function () {
		$("#wait").css("display", "none");
	});
	loadTblForecast();
	loadResumenForecast();
	createListPlanMarketOrigin();

	$.each(dataPlanAlternative, function (iPlanAlternative, planAlternative) {
		console.log(planAlternative);
        loadDataPlanAlternative(planAlternative.planAlternative, planAlternative.planAlternativeID);
        loadDataClientPlanMarket(planAlternative.planAlternativeID)
    })
    //console.log(JSON.stringify(dataForecast));

	addSubTotalClientIni();
}
function addSubTotalClientIni() {
	$("table.tblClient").each(function (i, table) {
		var idTable = $(table).attr('id');

		$('#' + idTable + ' tbody>tr').each(function (itr, tr) {
			//console.log(itr,tr);
			//console.log('tr')
			//console.log($(tr).attr('class'))
			var boxContainer = parseInt($(tr).children().eq(8).text());
			$(tr).find('td').each(function (itd, td) {
				if (itd >= 14) {
					//console.log($(td).text())
					var mount = parseFloat($(td).text());
					//if (!Number.isInteger(mount / boxContainer) && mount != 0) {
					//	$(td).css('color', 'red');
					//} else {
					//	$(td).removeAttr('style');
					//}
				}
			})
		});


		addSubTotalClient(idTable);
	})
}
function addSubTotalClient(id) {
	console.log('addSubTotalClient');
	$("#" + id + " tr.trSubtotal").remove();

	var sum = 0.0;
	var dataSubtTotal = [];

	$.each(dataWeeks, function (iWeeks, valWeek) {
		dataSubtTotal.push({ "id": iWeeks, "mount": 0 });
	});

	$('#' + id + ' > tbody > tr').each(function (index, element) {

		var iSpanCols = parseInt(14)

		$.each(dataWeeks, function (index2, value) {

			$valCell = $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text();
			$valWeight = parseFloat($(element).find('td:eq(7)').text());

			if ($.isNumeric($valCell)) {

			} else {
				$(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text('0');
				$valCell = 0;
			}

			$.each(dataSubtTotal, function (i, v) {
				if (v.id == index2) {
					v.mount += parseFloat($valCell) * $valWeight;
					//if (parseFloat(v.mount) > 0) console.log(v.mount);
				}
			});
		});
	});

	var rowSubTotal = '<tr class="trSubtotal table-primary text-align-right"><td colspan=6  >Sub Total Kg (Box * Weight)&nbsp;&nbsp;&nbsp;&nbsp;</td>';
	$.each(dataSubtTotal, function (i, v) {
		rowSubTotal += '<td>' + Math.round(v.mount) + '</td>';
	});
	rowSubTotal += '</tr>';
	$("#" + id + " tbody").append(rowSubTotal);

	var plan = ($("#" + id + "").parent().parent().parent().find('label.lblPlanAlternative').text());
	loadResumenByPlan(plan);
}

function loadResumenForecast() {
	var resForecast = [];
	var resForecastFlag = [];
	$.each(dataWeeks, function (iWeeks, valWeek) {
		resForecast.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
		resForecastFlag.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
	});

	dataResumen.A = resForecastFlag;
	dataResumen.B = resForecastFlag;
	dataResumen.C = resForecastFlag;
	dataResumen.D = resForecastFlag;
	dataResumen.E = resForecastFlag;
	//dataResumen.F = resForecastFlag;

	$('table#tblForecast tbody tr').each(function (itr, tr) {
		var colIni = 5;
		$(tr).find('td').each(function (itd, td) {
			if (itd > colIni - 1) {
				resForecast[itd - colIni].mount += parseFloat($(td).text());
			}
		});
	});
	dataResumen.F = resForecast
}
function loadTblForecast() {
    var table = '';
    table += "<table class='table table-bordered table-responsive' id='tblForecast'  class='table table-hover table-sm table-bordered table-responsive' >";
    table += '<thead  class="thead-light">';
    table += "<tr>";
    table += "<th  style='display:none'>Grower&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>";
    table += "<th>Farm&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>";
    table += "<th style='display:none'>Brand</th>";
    table += "<th>Variety&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>";
    table += "<th>Kg&nbspTotal</th>";

    $.each(dataWeeks, function (iWeeks, valWeek) {
        table += '<th> Week ' + valWeek.number + '</th>';
    });

    table += '</tr>'
    table += "</thead>";
    table += '<tbody>';
    $.each(dataForecast, function (i, forecast) {
        table += '<tr c>';
        table += '<td  style="display:none">' + "OZBLU PERU" + '</td>';
        table += '<td >' + forecast.farm + '</td>';
        table += '<td style="display:none">' + forecast.Brand + '</td>';
        table += '<td>' + forecast.variety + '</td>';
        table += '<td class="text-align-right">' + Math.round(forecast.total) + '</td>';
        var rowBandera = '';
        var rowBandera2 = '';
        var mountTot = 0;
        var arrColumns = Object.keys(forecast);
        $.each(dataWeeks, function (iWeeks, valWeek) {
            var mount = 0;
            $.each(arrColumns, function (icol, column) {
                if (column == valWeek.projectedWeekID) {
                    mount = forecast[column];
                    if (mount == null) mount = 0;
                }
            })
            rowBandera += '<td class="text-align-right">' + Math.round(parseFloat(mount)) + '</td>';
            rowBandera2 += '<td class="text-align-right">' + (mount) + '</td>';
            mountTot += mount;
        });
        table += rowBandera;
        table += '</tr>';
    });

    table += '</tbody>';
    table += "</table>";
    $('#divForecast').html(table);
}

function loadDataPlanAlternative(plan, planAlternativeID) {

	var rowsCount = $("#tablePlanMarket tbody tr:first td").length;
	if (rowsCount > 8) {
		alert("Only 5 plans allowed");
		return;
	}
	var plan = "";
	switch (rowsCount) {
		case 4:
			plan = "A";
			break;
		case 5:
			plan = "B";
			break;
		case 6:
			plan = "C";
			break;
		case 7:
			plan = "D";
			break;
		case 8:
			plan = "E";
			break;
		default:
		// code block
	}

	var description = $("#txtNewPlanName").val();

	var arrPlanAlternativeIDMarketID = [];
	dataPlanAlternative.push({ "planAlternativeId": planAlternativeID, "planAlternative": plan.trim() })

	$.each(dataWeeks, function (i, week) {
		dataPlanAlternativeAprove.push({ "projectedWeekID": week.projectedWeekID, "description": week.description, "planAlternativeId": planAlternativeID, "planAlternative": plan.trim() })
	})

	$("#txtNewPlanName").val("");
	$("#tablePlanMarket tbody tr").append("<td class='tdNumber'>0</td>");
	$("#tablePlanMarket thead tr.trPlan").append("<td>" + plan + "</td>");
	$("#tablePlanMarket tFoot tr").append("<td class='tdNumber'>0</td>");
	//Pill

	$("#nav-tab a").length;
	var pill = "<a class='nav-item nav-link'";
	pill += "id='nav-tab-plan" + plan + "'";
	pill += "data-toggle='tab'";
	pill += "href='#nav-plan" + plan + "'";
	pill += "role='tab'";
	pill += "aria-controls='nav-contact'";

	if ($("#nav-tab a").length == 0) {
		pill += "aria-selected='true'>plan " + plan + "</a>";
	} else {
		pill += "aria-selected='false'>plan " + plan + "</a>";
	}

	var tableResumen = '<table class="table table-bordered table-hovered table-responsive tblResumen table-striped" data-show-footer="true" data-show-export="true" data-sortable="true">';
	tableResumen += '<thead><tr class="bg-info" style="color:#fff"><th style="display:none">planAlternativeID</th><th style="display:none" >IdMarket</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMarket&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountry&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th style="display:none">IdCountry</th><th style="display:none">planAlternativeID</th>';
	tableResumen += '<th style="background-color: #5c7a99;">% Preliminar</th><th style="background-color: #5c7a99;">% Sales Allocation</th>';
	//tableResumen += '<th>% Preliminar</th><th>% Sales Allocation</th>'
	for (var x = 0; x < dataWeeks.length; x++) {
		tableResumen += "<th> Week&nbsp " + dataWeeks[x].number.toString();
		tableResumen += "</th>";
	}

	tableResumen += '</tr><thead>';
	tableResumen += '<tbody class="text-align-right">';

	$.each(dataMartet, function (indexM, market) {
		var datafilter = dataOringinMarket.filter(element => element.marketID.trim() == market.marketID.trim());
		var background = '#fff';
		if (indexM % 2 == 0) {
			background = '#f2f2f2';
		}
		tableResumen += '<tr class="trMarket" ><td style="background-color:' + background + '; display:none" rowspan=' + datafilter.length + '>' + planAlternativeID + '-' + market.marketID + '</td><td style="background-color:' + background + '; display:none" rowspan=' + datafilter.length + ' class="tdCountryID">' + market.marketID + '</td><td style="background-color:' + background + ' ;vertical-align: middle" rowspan=' + datafilter.length + '>' + market.name + '</td>';


		$.each(datafilter, function (index, origin) {
			//console.log(index);
			if (index == 0) {
				tableResumen += '<td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td class="tdCountryId" style="display:none">' + origin.originID + '</td><td class="tdPlanAlternativeID" style="display:none">' + planAlternativeID + '-' + market.marketID + '</td>';
			}
			else {
				tableResumen += '<tr><td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td class="tdCountryId" style="display:none">' + origin.originID + '</td><td class="tdPlanAlternativeID" style="display:none">' + planAlternativeID + '-' + market.marketID + '</td>';
			}
			for (var x = 0; x < 2; x++) {
				if (x == 0) tableResumen += "<td style='background-color:#17a2b8;color:aliceblue' contenteditable='false'>0";
				else tableResumen += "<td class='tdResumen' style='background-color:#17a2b8;color:aliceblue'>0";
				tableResumen += "</td>";
			}
			for (var x = 0; x < dataWeeks.length; x++) {
				tableResumen += "<td class='tdResumen'>0";
				tableResumen += "</td>";
			}
			tableResumen += "</tr>";
		})
	});
	tableResumen += '<tr> <td colspan=2 style="background-color: #17a2b8;" >Total</td><td class="SubTotalPreliminar">0</td><td class="SubTotalAllocation">0</td> </tr>';
	tableResumen += '<tr class="trOferted text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Sales Allocation</td>';
	for (var x = 0; x < dataWeeks.length; x++) {
		tableResumen += "<td>0";
		tableResumen += "</td>";
	}
	tableResumen += "</tr>";
	tableResumen += '<tr class="bg-primary trForecast text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Harvest Forecast</td>';
	for (var x = 0; x < dataWeeks.length; x++) {
		tableResumen += "<td>0";
		tableResumen += "</td>";
	}
	tableResumen += "</tr>";
	tableResumen += '<tr class="bg-primary trBalance text-align-right" style="color:#fff; background-color: #5c7a99!important;font-weight: bold;"><td colspan=4 >Diference (Kg)</td>';
	for (var x = 0; x < dataWeeks.length; x++) {
		tableResumen += "<td>0";
		tableResumen += "</td>";
	}
	tableResumen += "</tr>";
	tableResumen += '</tbody></table>';

	var contentByPlan = "<br/>";
	contentByPlan += tableResumen + "<br/>";
	contentByPlan += '<label style="display:none">Plan Alternative ID:</label>';
	contentByPlan += '<label id="lblPlanAlternativeID" style="display:none">' + planAlternativeID + '</label> <br/>';
	contentByPlan += '<label style="display:none">Plan Alternative:</label>';
	contentByPlan += '<label class="lblPlanAlternative" id="lblPlanAlternative" style="display:none"> ' + plan + '</label>';

	contentByPlan += '<ul class="nav nav-pills mb-3 navSalesAllocation" id="pills-tab" role="tablist">';
	for (var i = 0; i < dataMartet.length; i++) {
		var active = 'false';
		var active2 = '';
		if (i == 0) { active = 'true'; active2 = 'active' };
		contentByPlan += '<li class="nav-item">';
		contentByPlan += '<a class="nav-link ' + active2 + '" id="pills-tab' + i + '" data-toggle="pill" href="#pills-' + planAlternativeID + '-' + dataMartet[i].marketID + '" role="tab" aria-controls="pills-' + dataMartet[i].name + '" aria-selected="' + active + '">' + dataMartet[i].name + '</a>';
		contentByPlan += '</li>';
	}
	contentByPlan += '</ul>';

	contentByPlan += '<div class="tab-content" id="pills-tabContent-lblPlanAlternativeID' + planAlternativeID + '">';

	for (var i = 0; i < dataMartet.length; i++) {

		var planAlternativeIDMarketID = planAlternativeID + '-' + dataMartet[i].marketID;
		var itemPlanAlternativeIDMarketID = {
			"planAlternativeIDMarketID": planAlternativeIDMarketID, "planAlternativeID": planAlternativeID, "marketID": dataMartet[i].marketID
		};
		arrPlanAlternativeIDMarketID.push(itemPlanAlternativeIDMarketID);
		var active = '';
		if (i == 0) active = 'show active';
		contentByPlan += '<div class="tab-pane fade ' + active + '" id="pills-' + planAlternativeIDMarketID + '" role="tabpanel" aria-labelledby="pills-home-tab" style="border-color: #666666; border: groove;">';

		contentByPlan += '<label style="display:none" for="lblIdMarket-' + planAlternativeIDMarketID + '" >Market ID:</label>'
		contentByPlan += '<label style="display:none" class="lblIdMarket" id="lblIdMarket-' + planAlternativeIDMarketID + '">' + dataMartet[i].marketID + '</label>';

		//contentByPlan += '<form><div class="form-row">'

		//contentByPlan += '<div class="form-group-sm col-2">'
		//contentByPlan += '<label for="selectClient-' + planAlternativeIDMarketID + '">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <a href="/Maintenance/Customer" target="_blank"><i class="fas fa-user-plus"></i></a>'
		//contentByPlan += '<select class="custom-select  mr-sm-2 selectCustomer" id="selectClient-' + planAlternativeIDMarketID + '"> </select>'
		//contentByPlan += '</div>';
		//contentByPlan += '<div class="form-group-sm col-2">'
		//contentByPlan += '<label for="selectPriority-' + planAlternativeIDMarketID + '">Priority</label>'
		//contentByPlan += '<select class="custom-select  mr-sm-2" id="selectPriority-' + planAlternativeIDMarketID + '"></select>'
		//contentByPlan += '</div>';

		//contentByPlan += '<div class="form-group-sm col-2">'
		//contentByPlan += '<label for="selectDestination-' + planAlternativeIDMarketID + '">Destination</label>'
		//contentByPlan += '<select class="custom-select  mr-sm-2 selectDestination" id="selectDestination-' + planAlternativeIDMarketID + '"> </select>'
		//contentByPlan += '</div>';
		//contentByPlan += '<div class="form-group-sm col-2">'
		//contentByPlan += '<label for="selectVia-' + planAlternativeIDMarketID + '">Via</label>'
		//contentByPlan += '<select class="custom-select  mr-sm-2 selectVia" id="selectVia-' + planAlternativeIDMarketID + '"> <option value="1" selected>Sea</option><option value="2" >Air</option> </select>'

		//contentByPlan += '</div>';
		//contentByPlan += '<div class="form-group-sm col-2">'
		//contentByPlan += '<label for="selectFormat-' + planAlternativeIDMarketID + '">Format</label>'
		//contentByPlan += '<select class="custom-select  mr-sm-2 selectFormat" id="selectFormat-' + planAlternativeIDMarketID + '"> </select>'
		//contentByPlan += '</div>';
		//contentByPlan += '<div class="form-group-sm col-2">'
		//contentByPlan += '<br /><button type="button" class="btn btn-primary btn-xs  mb-1 btnAddClient" id="btnAddClient-' + planAlternativeIDMarketID + '">Add </button>';
		//contentByPlan += '</div>';

		//contentByPlan += '</div></form>';
		contentByPlan += '<h4><span class="badge badge-info">Sales Allocation</span></h4>';

		var tableCliente = '';
		tableCliente += '<table style="width:100%" class ="table table-bordered table-responsive tblClient" id="tblClient-' + planAlternativeIDMarketID + '"> ';
		tableCliente += '<thead class="thead-light">';
		tableCliente += '<tr >';
		tableCliente += '<th style="display:none">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th style="display:none">IdPrioridad</th>';
		tableCliente += '<th width="70%">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPrioridad&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th style="display:none">IdClient</th>';
		tableCliente += '<th style="display:none">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th style="display:none">IdCodepack</th>';
		tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFormat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th >Weight&nbspBox</th>';
		tableCliente += '<th >Box&nbspContainer/Pallet</th>';
		tableCliente += '<th style="display:none">IdDestino</th>';
		tableCliente += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th style="display:none">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCountryIdId&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th style="display:none">IdVia</th>';
		tableCliente += '<th >&nbsp&nbsp&nbspVia&nbsp&nbsp&nbsp</th>';
		for (var x = 0; x < dataWeeks.length; x++) {
			tableCliente += "<th> Week " + dataWeeks[x].number.toString();
			tableCliente += "</th>";
		}

		tableCliente += '</tr>';
		tableCliente += '</thead>';
		tableCliente += '<tbody>'
		tableCliente += '</tbody>'
		tableCliente += '</table>';

		contentByPlan += tableCliente;

		contentByPlan += '<div>';
		//contentByPlan += 'Comparative vs Promises';
		//contentByPlan += '<h4><span class="badge badge-info">Preliminary BID vs Sales Allocation</span></h4>'
		//contentByPlan += '<h4><span class="badge badge-info spanPreliminaryBID" style="cursor:pointer">Preliminary BID vs </span><span class="badge badge-info spanSalesAllocation" style="cursor:pointer"> Sales Allocation</span></h4>'

		contentByPlan += '</div>';

		//var tableComparative = '';
		//tableComparative += '<table style="width:100%" class ="table table-bordered table-responsive" id="tblComparative-' + planAlternativeIDMarketID + '"> ';
		//tableComparative += '<thead class="thead-light">';
		//tableComparative += '<tr >';
		//tableComparative += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspClient&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		//tableComparative += '<th >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDestination&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		//tableComparative += '<th >&nbsp&nbsp&nbsp&nbspType&nbsp&nbsp&nbsp&nbsp</th>';

		//for (var x = 0; x < dataWeeks.length; x++) {
		//	tableComparative += "<th> Week " + dataWeeks[x].number.toString();
		//	tableComparative += "</th>";
		//}

		//tableComparative += '</tr>';
		//tableComparative += '</thead>';
		//tableComparative += '<tbody>'
		//tableComparative += '</tbody>'
		//tableComparative += '</table>';

		//contentByPlan += '<div style="border:dashed">';
		//contentByPlan += tableComparative;
		//contentByPlan += '</div>';

		contentByPlan += '</div>';

	}

	contentByPlan += '</div>';
	contentByPlan += '<span class="badge badge-danger spanAlert" style="cursor:pointer" id="span-' + plan + '"></span><br />'

	var pillContent = "";
	//console.log($("#nav-tab a").length);
	if ($("#nav-tab a").length == 0) {
		pillContent += "<div class='tab-pane fade show active'";
	} else {
		pillContent += "<div class='tab-pane fade'";
	}

	pillContent += "id='nav-plan" + plan + "'";
	pillContent += " role='tabpanel' ";
	pillContent += "aria-labelledby='nav-contact-tab' style='border-style: outset;'>";
	pillContent += contentByPlan;
	pillContent += "</div>";

	$("#nav-tab").append(pill);
	$("#nav-tabContent").append(pillContent);


	$.each(arrPlanAlternativeIDMarketID, function (index, element) {
		//console.log('id:' + value);
		//let data = dataFormat.filter(element => element.viaID == "1").map(
		//let data = dataFormatMarket.filter(item => item.viaID == "1" && item.marketID.trim() == element.marketID.trim()).map(
		//	obj => {
		//		return {
		//			"id": obj.codePackID,
		//			"name": obj.description
		//		}
		//	}
		//);
		////console.log(JSON.stringify(data));
		//loadControlSelect($('#selectFormat-' + element.planAlternativeIDMarketID), data);


		////$.ajax({
		////	type: "GET",
		////	url: "/CommercialPlan/ListCustomer?marketID=" + element.marketID,
		////	async: false,
		////	success: function (data) {
		////		dataCustomer = JSON.parse(data);
		////		//console.log(data);
		////	}
		////});


		////data = dataCustomer.map(
		////	obj => {
		////		return {
		////			"id": obj.customerID,
		////			"name": obj.name
		////		}
		////	}
		////);
		//$dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == element.marketID.trim());
		//data = $dataCustomerFilter.map(
		//	obj => {
		//		return {
		//			"id": obj.customerID,
		//			"name": obj.customer
		//		}
		//	}
		//);

		//loadControlSelect($('#selectClient-' + element.planAlternativeIDMarketID), data);

		//if ($dataCustomerFilter.length > 0) {
		//	$dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == $dataCustomerFilter[0].customerID && item.marketID.trim() == element.marketID.trim());
		//	data = $dataDestinationFilter.map(
		//		obj => {
		//			return {
		//				"id": obj.destinationID,
		//				"name": obj.destination
		//			}
		//		}
		//	);
		//	loadControlSelect($('#selectDestination-' + element.planAlternativeIDMarketID), data);
		//}


		//data = dataPriority.map(
		//	obj => {
		//		return {
		//			"id": obj.priorityID,
		//			"name": obj.priority
		//		}
		//	}
		//);

		//loadControlSelect($('#selectPriority-' + element.planAlternativeIDMarketID), data);

		//var dataDestination = []
		//$.ajax({
		//	type: "GET",
		//	url: "/CommercialPlan/ListDestination?marketID=" + element.marketID,
		//	async: false,
		//	success: function (data) {
		//		dataDestination = JSON.parse(data);
		//	}
		//});

		//data = dataDestination.map(
		//	obj => {
		//		return {
		//			"id": obj.destinationID,
		//			"name": obj.origin.trim() + '-' + obj.description
		//		}
		//	}
		//);
		////console.log(JSON.stringify(dataDestination));
		//loadControlSelect($('#selectDestination-' + element.planAlternativeIDMarketID), data);
	});

	loadResumenByPlan(plan);
}

function loadDataClientPlanMarket(planAlternativeID) {
	//console.log('loadDataClientPlanMarket');
	//console.log(JSON.stringify(dataCustomerRequest));
	//var tabPlanId = $('label#lblPlanAlternativeID').parent().attr('id');
	var dataCustomerByPlanAlternative = [];
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListPlanCustomerByPlanAlternative?planAlternativeID=" + planAlternativeID,
		async: false,
		success: function (data) {
			dataCustomerByPlanAlternative = JSON.parse(data);
			//console.log(data);
		}
	});
	//console.log(JSON.stringify(dataCustomerByPlanAlternative));

	$.each(dataMartet, function (e, market) {
		//$('table#tblClient-' + planAlternativeID + '-' + market.marketID + ' tbody').append('<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td></tr>')

		//console.log(JSON.stringify(market));
		var dataFilter = dataCustomerByPlanAlternative.filter(row => row.marketID.trim() == market.marketID.trim())
		$.each(dataFilter, function (i, element) {
			var row = '<tr class="text-align-right trCustomerFormat">';
			row += '<td style="display:none"></td>';
			row += '<td style="display:none">' + element.planCustomerFormatID + '</td><td width="20%">' + element.priority + '</td>';
			row += '<td style="display:none">' + element.customerID + '</td><td style="display:none">' + element.customer + '</td>';

			row += '<td style="display:none">' + element.codePackID + '</td><td >' + element.codepack + '</td><td >' + element.weight + '</td>';

			if (element.viaID == '1') {
				row += '<td >' + element.boxPerContainer + '</td>';
			} else {
				row += '<td contenteditable="false">' + element.boxesPerPallet + '</td>';
			}
			row += '<td style="display:none">' + element.destinationID + '</td><td >' + element.destination + '</td><td class="tdCountryId" style="display:none">' + element.originID + '</td>';
			row += '<td style="display:none">' + element.viaID + '</td><td >' + element.via + '</td>';
			dataAmountByWeek = []
			dataAmountByWeek = ListCustomerWeekByPlanCustomerId(element.planCustomerFormatID)
			$.each(dataWeeks, function (iweek, week) {
				row += '<td  contenteditable="false">';
				var amountFilter = dataAmountByWeek.filter(amount => amount.projectedWeekID == week.projectedWeekID)
				if (amountFilter.length > 0) {
					row += amountFilter[0].amount;
					console.log(amountFilter);
				} else row += '0';

				row += "</td>";
			})
			row += '</tr>';
			$('table#tblClient-' + planAlternativeID + '-' + market.marketID + ' tbody').append(row);
		})
	})

}

function loadControlSelect(control, data) {
	var contenido = "";
	for (var i = 0; i < data.length; i++) {
		contenido += "<option value='" + data[i].id + "'>";
		contenido += data[i].name;
		contenido += "</option>";
	}

	$(control).append(contenido);
}

function loadResumenByPlan(idPlan) {
	//console.log(idPlan);
	idPlan = idPlan.trim();
	$("#nav-plan" + idPlan + " table.tblResumen td.tdResumen").each(function (itd, td) {
		$(td).text("0");
	});
	//console.log('id plan');
	//console.log(idPlan);
	$("#nav-plan" + idPlan.trim() + " table.tblResumen tbody td.tdPlanAlternativeID").each(function (itd, td) {
		var planAlternativeId = $(td).text();
		var countryId = $(td).parent().find('td.tdCountryId').text();
		var tr = $(td).parent();
		//console.log($(tr).text());
		//console.log(countryId);
		//console.log(planAlternativeId);

		$("#tblClient-" + planAlternativeId + " tbody>tr>td.tdCountryId").each(function (itdchild, tdchild) {
			//console.log($(tdchild).text());

			if ($(tdchild).text().trim() == countryId.trim()) {
				//console.log('eeeeeeeeee');
				$.each(dataWeeks, function (iweek, week) {
					var valOrigin = parseFloat($(tdchild).parent().find('td:eq(' + (parseInt(iweek) + parseInt(14)) + ')').text()) * parseFloat($(tdchild).parent().find('td:eq(7)').text());
					//console.log($(tdchild).parent().find('td:eq(7)').text())
					if ($(tr).attr('class') == 'trMarket') {
						//$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(6)) + ')').text(valOrigin);
						//tdFind = $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(6)) + ')');
						//var valOld = parseFloat($(tdFind).text()) + parseFloat(valOrigin);
						//$(tdFind).text(valOld);
						var varOld = $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(8)) + ')').text();
						var valNew = parseFloat(valOrigin) + parseInt(varOld);
						$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(8)) + ')').text(Math.round(valNew));
					} else {
						//$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(3)) + ')').text(valOrigin);
						//tdFind = $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(3)) + ')');
						//var valOld = parseFloat($(tdFind).text()) + parseFloat(valOrigin);
						//$(tdFind).text(valOld);
						var varOld = $(tr).find('td:eq(' + (parseInt(iweek) + parseInt(5)) + ')').text();
						var valNew = parseFloat(valOrigin) + parseInt(varOld);
						$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(5)) + ')').text(Math.round(valNew));
					}
				})
			}
		})

	});
	$.each(dataWeeks, function (iweek, week) {
		//console.log('oferted');
		//console.log(idPlan);
		var sum = 0
		$("#nav-plan" + idPlan + " tbody tr.trSubtotal").each(function (itr, tr) {
			//console.log($(tr).text());
			sum += parseFloat($(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text());


		});
		//console.log(sum);
		$("#nav-plan" + idPlan + " tbody tr.trOferted ").each(function (itr, tr) {
			$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(Math.round(sum));
		});

	});

	$("#nav-plan" + idPlan + " table.tblResumen tbody tr.trForecast").each(function (itr, tr) {
		$.each(dataResumen.F, function (iweek, week) {
			$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(Math.round(week.mount));
		})
	});

	var resOferted = [];
	var resBalance = [];
	$.each(dataWeeks, function (iWeeks, valWeek) {
		resOferted.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
		resBalance.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
	});

	$("#nav-plan" + idPlan + " table.tblResumen tbody tr.trBalance").each(function (itr, tr) {
		$.each(dataResumen.F, function (iweek, week) {
			$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(Math.round(week.mount));
		})
	});

	var sum = 0;
	$('#span-' + idPlan).text('');
	//$('#span-' + idPlan).text('aaa');
	$.each(dataWeeks, function (iweek, week) {
		$("#nav-plan" + idPlan + " tbody tr.trForecast").each(function (itr, tr) {
			sum += parseFloat($(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text());
		});
		$("#nav-plan" + idPlan + " tbody tr.trOferted").each(function (itr, tr) {
			sum -= parseFloat($(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text());
		});


		$("#nav-plan" + idPlan + " tbody tr.trBalance").each(function (itr, tr) {
			$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').text(sum);
			if (sum < 0) {
				//console.log('aaaaaaaaaa');
				$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').css('background-color', 'red');
				$('#span-' + idPlan).text('Error: check the Diference');
				//} else {
				//	$(e.target).removeAttr('style');
			}
			else {
				$(tr).find('td:eq(' + (parseInt(iweek) + parseInt(1)) + ')').removeAttr('style');

			}
		});
	});

	loadComparative();
}

function loadComparative() {
	var dataCustomerPromises = [];
	var dataCustomerPromisesAll = []

	$.each(dataMartet, function (iMarket, market) {
		//console.log(JSON.stringify(market));
		$('#divPromises table#tblClient-promises-' + market.marketID + ' tbody tr.trCustomerFormat').each(function (itr, td) {
			//console.log("aaa")
			//console.log($(td).find('td:eq(3)').text()); //idcliente
			var customerID = $(td).find('td:eq(3)').text();
			var customer = $(td).find('td:eq(4)').text();
			var destinationID = $(td).find('td:eq(9)').text();
			var destination = $(td).find('td:eq(10)').text();

			var customerFormat = { "marketID": market.marketID, "customerID": customerID, "customer": customer, "destinationID": destinationID, "destination": destination, "mountPromises": [], "mountOferted": [], "mountBalance": [] }
			$.each(dataWeeks, function (iWeeks, valWeek) {
				customerFormat.mountPromises.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
				customerFormat.mountOferted.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
				customerFormat.mountBalance.push({ "projectedWeekID": valWeek.projectedWeekID, "mount": 0 });
			});

			dataCustomerPromisesFilter = dataCustomerPromisesAll.filter(element => element.customerID == customerFormat.customerID && element.destinationID == customerFormat.destinationID)
			if (dataCustomerPromisesFilter.length > 0) {
				console.log('yala')
			} else {
				dataCustomerPromisesAll.push(customerFormat);
			}
		});
		//console.log(JSON.stringify( dataCustomerPromisesAll));
		$('#divPromises table#tblClient-promises-' + market.marketID + ' tbody tr.trCustomerFormat').each(function (itr, td) {
			var customerID = $(td).find('td:eq(3)').text();
			var customer = $(td).find('td:eq(4)').text();
			var destinationID = $(td).find('td:eq(9)').text();
			var destination = $(td).find('td:eq(10)').text();
			var weightPerBox = $(td).find('td:eq(7)').text();

			var customerFormat = { "marketID": market.marketID, "customerID": customerID, "customer": customer, "destinationID": destinationID, "destination": destination, "mountPromises": [], "mountOferted": [], "mountBalance": [] }

			$.each(dataCustomerPromisesAll, function (i, element) {
				if (element.customerID == customerFormat.customerID && element.destinationID == customerFormat.destinationID) {
					$.each(dataWeeks, function (iWeeks, valWeek) {
						$.each(element.mountPromises, function (iChild, child) {
							if (child.projectedWeekID == valWeek.projectedWeekID) {
								var box = $(td).find('td:eq(' + (parseInt(14) + parseInt(iChild)) + ')').text();
								child.mount += parseFloat(box) * parseFloat(weightPerBox);
							}
						});

					});
				}
			});
		});
	});
	$.each(dataPlanAlternative, function (idataPlanAlternative, dataPlanAlternative) {


		$.each(dataMartet, function (iMarket, market) {
			var dataCustomerOferted = dataCustomerPromisesAll.filter(element => element.marketID == market.marketID);
			//console.log(JSON.stringify( dataCustomerOferted));
			$('table#tblClient-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody>tr.trCustomerFormat').each(function (itr, td) {
				var customerID = $(td).find('td:eq(3)').text();
				var customer = $(td).find('td:eq(4)').text();
				var destinationID = $(td).find('td:eq(9)').text();
				var destination = $(td).find('td:eq(10)').text();
				var weightPerBox = $(td).find('td:eq(7)').text();
				var customerFormat = { "marketID": market.marketID, "customerID": customerID, "customer": customer, "destinationID": destinationID, "destination": destination, "mountPromises": [], "mountOferted": [], "mountBalance": [] }

				$.each(dataCustomerOferted, function (i, element) {
					if (element.customerID == customerFormat.customerID && element.destinationID == customerFormat.destinationID) {
						$.each(dataWeeks, function (iWeeks, valWeek) {
							$.each(element.mountOferted, function (iChild, child) {
								if (child.projectedWeekID == valWeek.projectedWeekID) {
									var box = $(td).find('td:eq(' + (parseInt(14) + parseInt(iChild)) + ')').text();
									child.mount += parseFloat(box) * parseFloat(weightPerBox);
									//console.log(box);
								}
							});

						});
					}
				});
			});

			$.each(dataWeeks, function (iWeeks, valWeek) {

				$.each(dataCustomerOferted, function (i, element) {
					var acum = 0;
					$.each(element.mountBalance, function (iChild, child) {
						//console.log(JSON.stringify(child));
						if (child.projectedWeekID == valWeek.projectedWeekID) {
							var promises = element.mountPromises.filter(e => e.projectedWeekID == valWeek.projectedWeekID)[0].mount;
							var oferted = element.mountOferted.filter(e => e.projectedWeekID == valWeek.projectedWeekID)[0].mount;

							//child.mount = parseFloat(promises) - parseFloat(oferted);
							acum += parseFloat(promises) - parseFloat(oferted);
							child.mount = acum
							//console.log((element.mountOferted).filter(e => e.projectedWeekID == valWeek.projectedWeekID));
						}

					});

				});
			});

			//$.each(dataCustomerOferted,function()

			//$('table#tblClient-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody>tr.trCustomerFormat').each(function (itr, td) {
			$('table#tblComparative-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody').html("");
			$.each(dataCustomerOferted, function (i, element) {
				var tr = '<tr>';
				//tr += '<td rowspan=2>' + element.customerID + '</td>';
				tr += '<td rowspan=3>' + element.customer + '</td>';
				//tr += '<td rowspan=2>' + element.destinationID + '</td>';
				tr += '<td rowspan=3>' + element.destination + '</td>';
				tr += '<td>Preliminary BID</td>'
				$.each(element.mountPromises, function (iChild, child) {

					$.each(dataWeeks, function (iWeeks, valWeek) {
						if (child.projectedWeekID == valWeek.projectedWeekID) {
							tr += '<td>' + child.mount + '</td>';
						}
					});
				});
				tr += '</tr>';
				tr += '<tr>';
				tr += '<td>Sales Allocation</td>'
				$.each(element.mountOferted, function (iChild, child) {

					$.each(dataWeeks, function (iWeeks, valWeek) {
						if (child.projectedWeekID == valWeek.projectedWeekID) {
							tr += '<td>' + child.mount + '</td>';
						}
					});
				});
				tr += '</tr>';
				tr += '<tr>';
				tr += '<td>Diference</td>'

				var acum = 0;
				$.each(element.mountBalance, function (iChild, child) {

					$.each(dataWeeks, function (iWeeks, valWeek) {
						if (child.projectedWeekID == valWeek.projectedWeekID) {
							acum = child.mount
							tr += '<td>' + acum + '</td>';
						}
					});
				});
				tr += '</tr>';

				//console.log(tr);
				$('table#tblComparative-' + dataPlanAlternative.planAlternativeId + '-' + market.marketID + ' tbody').append(tr);
			});

			//$('#myTable > tbody:last-child').append('<tr>...</tr><tr>...</tr>');

			//});
			//console.log(JSON.stringify(dataCustomerOferted));
		});
	});
	//console.log(JSON.stringify(dataCustomerPromisesAll));
	subTotalPercentCountry2();
}

function subTotalPercentCountry2() {

	$('table.tblResumen>tbody').each(function (iPlan, tbody) {
		console.log('tbody')
		console.log(tbody)
		console.log(iPlan)


		var percentByContry = []
		$(tbody).find('tr').each(function (iTr, tr) {

			$(tr).find('td.tdCountryId').each(function (iTd, td) {
				//dataWeeks
				var indexIni = 0
				if ($(tr).attr('class') == 'trMarket') {
					indexIni = 8;
				} else {
					indexIni = 5;
				}
				var mount = 0
				percentByContry.push({ "countryId": $(td).text(), "mount": 0 });
				for (var x = 0; x < dataWeeks.length; x++) {
					percentByContry[iTr].mount = parseInt(percentByContry[iTr].mount) + parseInt($(td).parent().find('td:eq(' + (parseInt(x) + parseInt(indexIni)) + ')').text());
				}
			})

		})
		console.log(JSON.stringify(percentByContry))
		var total = 0;
		$.each(percentByContry, function (iEle, element) {
			total = total + element.mount
		})

		$(tbody).find('tr').each(function (iTr, tr) {
			$(tr).find('td.tdCountryId').each(function (iTd, td) {
				var percent = 0
				if (total > 0) {
					percent = Math.round((parseInt(percentByContry[iTr].mount) * parseInt(100)) / total);
				}
				$(td).next().next().next().text(parseInt(percent));
			})
		})


		var SubTotalPreliminar = 0;
		var SubTotalAllocation = 0;
		$(tbody).find('tr').each(function (iTr, tr) {
			$(tr).find('td.tdCountryId').each(function (iTd, td) {
				SubTotalPreliminar = SubTotalPreliminar + parseInt($(td).next().next().text());
				SubTotalAllocation = SubTotalAllocation + parseInt($(td).next().next().next().text());
			})
		})

		$(tbody).find('td.SubTotalPreliminar').removeAttr('style');
		$(tbody).find('td.SubTotalPreliminar').text(SubTotalPreliminar);
		if (SubTotalPreliminar > 100) { $(tbody).find('td.SubTotalPreliminar').css('background-color', 'red'); }
		$(tbody).find('td.SubTotalAllocation').text(SubTotalAllocation);
	})
}
function ListCustomerWeekByPlanCustomerId(planCustomerFormatID) {
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListPlanCustomerWeekByCustomer?planCustomerFormatID=" + planCustomerFormatID,
		async: false,
		success: function (data) {
			if (data.length > 0) {
				dataAmountByWeek = JSON.parse(data);
			} else dataAmountByWeek = []

			//console.log(data);
		}
	});
	return dataAmountByWeek;
}
function createListPlanMarketOrigin() {
	//alert(123);
	//console.log(JSON.stringify(dataOringinMarket));

	//var data_filter = dataFormat.filter(element => element.viaID == dataMartet[i].marketID).length()


	var tabletext = '';
	//tabletext += '<table class="table table-bordered table-responsive" ';
	//tabletext += '  id="tablePlanMarket2"';
	//tabletext += ' > <thead>';
	//tabletext += '    <tr>';
	//tabletext += '      <th data-field="planMarketId">ID</th>';
	//tabletext += '      <th data-field="planMarketId">Plan</th>';
	//tabletext += '      <th data-field="planMarketId">Description</th>';

	//for (var i = 0; i < dataMartet.length; i++) {
	//	tabletext += "<th colspan=" + dataOringinMarket.filter(element => element.marketID.trim() == dataMartet[i].marketID.trim()).length+">";
	//	tabletext += dataMartet[i].name + '&nbsp%';
	//	tabletext += "</th>";
	//}

	//tabletext += '    </tr>';
	//tabletext += '<th colspan="3">';
	//for (var i = 0; i < dataMartet.length; i++) {
	//	var datafilter = dataOringinMarket.filter(element => element.marketID.trim() == dataMartet[i].marketID.trim());
	//	console.log(JSON.stringify(datafilter));
	//	$.each(datafilter, function (index, origin) {
	//		console.log(JSON.stringify(origin));
	//		tabletext += '<td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td>';
	//	})
	//}
	//tabletext += '</th>';
	//tabletext += '  </thead>';
	//tabletext += ' <tbody> </tbody>';
	//tabletext += '</table>';

	tabletext += '<table class="table table-bordered table-hover table-striped " ';
	tabletext += '  id="tablePlanMarket"';
	tabletext += ' > <thead>';
	tabletext += '<tr class="bg-info" style="color:#fff" ><th rowspan="2">Market</th><th rowspan="2" style="display:none">Market Id</th><th rowspan="2">Country</th><th rowspan="2" style="display:none">Country ID</th><th colspan="5">Enter Percent By Country In The Plan</th></tr>';
	tabletext += '<tr class="bg-primary trPlan" style="color:#fff"></tr>'
	tabletext += '<tbody>';

	//for (var i = 0; i < dataMartet.length; i++) {
	$.each(dataMartet, function (indexM, market) {
		var datafilter = dataOringinMarket.filter(element => element.marketID.trim() == market.marketID.trim());
		var background = '#f2f2f2';
		if (indexM % 2 == 0) {
			background = '#fff';
		}
		tabletext += '<tr><td style="background-color:' + background + ';vertical-align: middle;" rowspan=' + datafilter.length + '>' + market.name + '</td><td style="background-color:' + background + ' ;display:none" rowspan=' + datafilter.length + ' >' + market.marketID + '</td>';
		//console.log(JSON.stringify(datafilter));
		$.each(datafilter, function (index, origin) {
			//console.log(index);
			if (index == 0) {
				tabletext += '<td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td style="display:none" class="tdCountryId">' + origin.originID + '</td>';
			}
			else {
				tabletext += '<tr><td >' + origin.description + '&nbsp&nbsp&nbsp&nbsp&nbsp</td><td style="display:none" class="tdCountryId">' + origin.originID + '</td>';
			}
		})
	});

	tabletext += '</tbody>'
	tabletext += '<tFoot><tr style="color:#fff; background-color: #5c7a99!important;font-weight: bold;" class="trSubTotalPercentCountry"><td colspan="2" class="text-align-right" >Summary</td></tr></tFoot>'

	document.getElementById('divTableMarket').innerHTML = tabletext;

}