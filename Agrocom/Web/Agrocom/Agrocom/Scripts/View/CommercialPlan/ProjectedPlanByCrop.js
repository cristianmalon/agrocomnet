﻿var bError = false;
var dataWeek = [];
var dataPlan = [];
var dataVariety = [];
var dataCategory = [];
var dataFormat = [];
var dataTypeBox = [];
var dataBrand = [];
var dataPresentation = [];
var dataLabel = [];
var dataForecast = [];
var dataForecastByBrand = [];
var trEdit = null;
var dataSizeBox = [];
var dataPlanCommercial = [];
var dataVarCatByCrop = [];
var dataSizeByCrop = [];
var dataExport = [];
var dataMartet = [];
var dataWow = [];
var dataVia = [];
var dataKam = [];
var dataCustomerMarket = [];
var dataCodepack = [];
var dataIncoterm = [];
var dataSizes = [];
var dataCodepackSelect = [];
var _$selectDestination;
var _contador = 0;
var _sort = 1;
var tablePC;

$(window).resize(function () {

    return;
});

var dataPriority = [
    { "value": 1, "text": "Strategic" },
    { "value": 2, "text": "Tactical" },
    { "value": 3, "text": "Development" }
]

var dataStatus = [
    { "value": 1, "text": "Confirm" },
    { "value": 2, "text": "Pending" }
]

//var table = $('#tblCommercialPlan').DataTable({
//    orderCellsTop: false,
//    fixedHeader: true,
//    order: false
//});

var iTotVarietyXCategory = 0;

!function ($) {
    $(function () {
        sweet_alert_progressbar();
        $("#lblPlanId").text(localStorage.cropID);
        $("#lblCamp").text(localStorage.campaign);

        //>>>Obtener pesos por cultivo
        $.ajax({
            async: false,
            type: 'GET',
            url: "/CommercialPlan/ListSizeByCrop?cropID=" + localStorage.cropID,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    dataWeek = JSON.parse(data);
                    var dataJson = JSON.parse(data);

                    dataSizeByCrop = dataJson.map(
                        obj => {
                            return {
                                "value": obj.formatID,
                                "text": obj.codePack
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener pesos por cultivo

        //>>>Obtener la incoterm
        $.ajax({
            type: "GET",
            url: "/Sales/GetIncotermMC?opt=sal&cropID=" + localStorage.cropID + "&viaID=0",
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                var dataJson = JSON.parse(data);
                if (data.length > 0) {
                    dataIncoterm = dataJson.map(
                        obj => {
                            return {
                                "id": obj.incotermID,
                                "name": obj.name,
                                "viaID": obj.viaID
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener la incoterm

        //>>>Obtener los calibres
        var opt = 'cro';
        var id = localStorage.cropID;
        $.ajax({
            type: "GET",
            url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {

                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataSizes = dataJson.map(
                        obj => {
                            return {
                                "id": obj.sizeID,
                                "name": obj.sizeID
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener los calibres

        //>>>Obtener la presentación
        var opt = 'pre';
        id = '';
        $.ajax({
            type: "GET",
            url: "/Sales/GetCodepackWithPresentation?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataPresentation = dataJson
                }
            }
        });
        //<<<Obtener la presentación

        //>>>Obtener semanas por campaña
        $.ajax({
            async: false,
            type: 'GET',
            url: "/Forecast/ListWeekByCampaign?campaignID=" + localStorage.campaignID,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    dataWeek = JSON.parse(data);

                }
            }
        });
        //<<<Obtener semanas por campaña

        //>>>Obtener los formatos por campaña
        var opt = 'cro';
        var id = localStorage.cropID;
        $.ajax({
            type: "GET",
            url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                dataCodepack = JSON.parse(data);
            }
        });
        //<<<Obtener los formatos por campaña

        //>>>Obtener mercados
        var opt = 'all';
        var id = '';
        $.ajax({
            type: "GET",
            url: "/CommercialPlan/ListMarket?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                //dataMartet
                var dataJson = JSON.parse(data);
                dataMartet = dataJson.map(
                    obj => {
                        return {
                            "marketID": obj.marketID,
                            "description": obj.name
                        }
                    }
                );

            }
        });
        //<<<Obtener mercados

        //>>>Obtener Clientes por mercado
        opt = 'wmr';
        id = '';
        $.ajax({
            type: "GET",
            url: "/Sales/ListAllWithMarket?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                dataCustomerMarket = JSON.parse(data);
            }
        });
        //<<<Obtener Clientes por mercado

        //>>>Obtener Vias
        opt = 'all';
        id = '';
        $.ajax({
            type: "GET",
            url: "/Sales/ListVia?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                var dataJson = JSON.parse(data);
                if (data.length > 0) {
                    dataVia = dataJson.map(
                        obj => {
                            return {
                                "viaID": obj.viaID,
                                "description": obj.name
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener Vias

        //>>>Obtener Kam
        opt = 'all';
        id = '';
        $.ajax({
            type: "GET",
            url: "/Sales/ListKam?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                var dataJson = JSON.parse(data);
                if (data.length > 0) {
                    dataKam = dataJson.map(
                        obj => {
                            return {
                                "kamID": obj.kamID,
                                "description": obj.kam
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener Kam

        //>>>Obtener categorías por cultivo
        var opt = "cro";
        var gro = "";
        $.ajax({
            type: "GET",
            url: "/Sales/CategoryGetByCrop?opt=" + opt + "&id=" + localStorage.cropID + "&gro=" + gro,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {

                    var dataJson = JSON.parse(data);
                    dataCategory = dataJson.map(
                        obj => {
                            return {
                                "categoryID": obj.categoryID,
                                "description": obj.description
                            }
                        }
                    );

                }
            }
        });
        //<<<Obtener categorías por cultivo

        //>>>Obtener formato por cultivo
        var optID = "fci";
        $.ajax({
            type: "GET",
            url: "/Sales/GetFormatByCropIDMC?opt=" + optID + "&id=" + localStorage.cropID,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataFormat = dataJson.map(
                        obj => {
                            return {
                                "id": obj.formatID,
                                "abbreviation": obj.format,
                                "description": obj.format,
                                "weight": obj.weight
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener formato por cultivo

        //>>>Obtener todos los empaques del producto
        optID = "pkc";
        var filter01 = '';
        $.ajax({
            type: "GET",
            url: "/Sales/GetPackageProductAllMC?opt=" + optID + "&id=" + filter01,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataTypeBox = dataJson.map(
                        obj => {
                            return {
                                "id": obj.packageProductID,
                                "description": obj.packageProduct,
                                "abbreviation": obj.abbreviation
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener todos los empaques del producto

        //>>>Obtener el plan de proyección de campo según campaña
        var optplan = "pla";
        $.ajax({
            type: "GET",
            url: "/Forecast/GetForecastForPlan?opt=" + optplan + "&campaignID=" + localStorage.campaignID,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataForecast = dataJson.map(
                        obj => {
                            return {
                                "varietyID": obj.varietyID,
                                "name": obj.name,
                                "categoryID": obj.categoryID,
                                "description": obj.description,
                                "projectedWeekID": obj.projectedWeekID,
                                "number": obj.number,
                                "BOXES": obj.boxes,
                                "formatID": obj.formatID
                            }
                        }
                    );
                }
            }
        });

        var optplan = "bra";
        $.ajax({
            type: "GET",
            url: "/Forecast/GetForecastForPlan?opt=" + optplan + "&campaignID=" + localStorage.campaignID,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataForecastByBrand = dataJson.map(
                        obj => {
                            return {
                                "brandID": obj.brandID,
                                "brand": obj.brand,
                                "varietyID": obj.varietyID,
                                "name": obj.name,
                                "categoryID": obj.categoryID,
                                "description": obj.description,
                                "projectedWeekID": obj.projectedWeekID,
                                "number": obj.number,
                                "BOXES": obj.boxes,
                                "formatID": obj.formatID
                            }
                        }
                    );
                }
            }
        });
        //<<<Obtener el plan de proyección de campo según campaña

        loadData();

        $('#chkplu').change(function () {
            calculeCodepack();
        });

        //>>>Asignación de métodos según evento a objetos
        $(document).on("click", ".btnDeleteRowToProcess", function (e) {
            var trDetele = $(this).closest('tr')
            deleteRowProcess(trDetele)
            //var table = $(this).closest('table')
            //calculateProjectedSales(table)
        })

        $(document).on("change", "#selectVariety", function () {
            calculeCodepack();
        });
        $(document).on("change", "#selectCodepack", function () {
            calculeCodepack();
        });
        $(document).on("change", "#selectVia", function () {
            changeVia($(selectVia).val());
        });

        $(document).on("change", "#selectMarket", function () {
            changeMarket($(selectMarket).val());
        });

        $(document).on("click", "#btnSave", function () {
            GenerateSaleRequestAndPO();
        });

        $(document).on("change", "#selectTypeBox", function () {
            calculeCodepack();
        });
        $(document).on("change", "#selectBrand", function () {
            calculeCodepack();
            loadLabel($(selectBrand).val());
        });
        $(document).on("change", "#selectPresentation", function () {
            calculeCodepack();
        });
        $(document).on("click", "#btnAddClient", function () {
            addClientCodepack();
            if (!bError) sweet_alert_success('Good Job!', 'Datas Saved.');
        });
        $(document).on("click", "#btnCancelClient", function () {
            clearDetails();
        });
        $(document).on("click", ".btnRowEdit", function () {
            var tr = $(this).closest('tr');
            rowEdit(tr);
        });
        $(document).on("click", ".btnRowSave", function () {
            var tr = $(this).closest('tr');
            rowSave(tr, 1);
        });
        $(document).on("click", ".btnRowDelete", function () {
            var tr = $(this).closest('tr');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    var id = $(tr).find(".tdPlanCustomerVarietyID").text();

                    if (id > 0) {
                        rowSave(tr, 0);
                    }
                    else {
                        tr.remove();
                    }

                } else {
                    if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your action was cancelled',
                            'error'
                        );
                    }
                }
            });


        });
        $(document).on("click", "#btnEditClient", function () {
            updateRow()
        });
        $(document).on("change", "#selectClient", function () {
            loadDestination($(selectClient).val());
            loadDataWow($(selectClient).val());
            loadComboConsignnee([], 'selectConsignee', true);
            if (!GetFillSelectPaymentTerm($(selectClient).val())) return;
        });
        $(document).on("change", "#selectDestination", function () {
            $dataWowFilter = dataWow.filter(item => item.destinationID == this.value);
            data = $dataWowFilter.map(
                obj => {
                    return {
                        "id": obj.wowID,
                        "name": obj.consignee
                    }
                }
            );
            if (data.length > 0) {
                loadComboConsignnee(data, 'selectConsignee', true);
                //if (trEdit != null) {
                //    $('#selectConsignee').selectpicker('val', _$selectConsignee);
                //}
            } else {
                loadComboConsignnee([], 'selectConsignee', true);
            }

            $('#selectConsignee').selectpicker('refresh');
        });

        $(document).on("change", "#cboFCustomer", function () {
            loadFDestination($(cboFCustomer).val())
        });

        $(document).on("change", "#selecFtVia", function () {
            LoadFFormat($(selecFtVia).val())
        });

        $(document).on("change", "#selectFormat", function () {
            loadInternalPackage($('#selectFormat option:selected').val());
            calculeCodepack();
            loadSizeBox();
        });

        $(document).on("change", "#selectTypeBox", function () {
            //loadSizeBox()
        });

        $(document).on("click", "#btnAddClientModal", function () {
            localStorage.codepacksToStore = '';
            clearDetails();
            $(btnAddClient).show();
            $(btnEditClient).hide();
            $(modalCodepack).modal('show');
            openModal();
        });

        $(document).on("click", "#btnProcessSR", function () {
            AvoidCloseModalByAnyKey("modalProcess");
            $(modalProcess).draggable();
            $(modalProcess).modal('show');
            drawVarietysToProcess();
        });

        $(document).on("click", "#btnSearch", function () {
            GetCommercialPlanByFilters();
        });

        $(document).on("click", "#btnExport", function () {
            DownloadTemplate();

        });

        $(document).on("click", "#selectWeekProcess", function () {
            loadPlanComercial();
        });
        $(document).on("keyup", "input.txtBoxNumber", function (e) {
            if (e.which >= 37 && e.which <= 40) {
                e.preventDefault();
            }
            var _value = '';
            $(this).val(function (index, value) {
                _value += value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                return _value;
            });

            var table = $(this).closest('table')
            if (_value.length == 0) {
                $(this).val(0);
                calculateProjectedSales(table);
                return;
            }
            $(this).closest('tr').find('.tdBtn').attr("style", "background-color:#F9E79F");
            $(this).closest('tr').find(".btnRowSave").show();

            calculateProjectedSales(table);

        });
        $(document).on("click", ".btnAddProcess", function (e) {
            var trProcess = $(this).closest('tr');
            addRowToProcess(trProcess);
        });
        $(document).on("change", "#selectConsignee", function () {

            loadDateCreated();
        });
        //<<<Asignación de métodos según evento a objetos

    });


}(window.jQuery);

function loadDateCreated() {

    if ($(selectConsignee).val() > 0) {
        var notify = dataWow.filter(element => element.wowID == $(selectConsignee).val())[0].notify
        $(txtCreationDate).val(notify);
    }

}

function loadDataWow(customerID) {
    var bRsl = false;
    if (customerID == '') {
        return;
    }
    $.ajax({
        type: "GET",
        url: "/Sales/ListWowByCustomer?customerID=" + customerID + "&cropID=" + localStorage.cropID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                dataWow = JSON.parse(data);
            }
        }
    });
    bRsl = true;
}

var successListWeekByCampaign = function (data) {
    if (data.length > 0) {
        dataWeek = JSON.parse(data);
    }
}

function deleteRowProcess(trDetele) {
    var tdplanCustomerVarietyID = $(trDetele).find('.tdplanCustomerVarietyID');
    var tdboxesProcess = $(trDetele).find('.tdtoProcess');
    var dataPlanCommercialX = dataPlanCommercial;
    var itemFind = dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text());
    dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text()).boxes = parseInt(itemFind.boxes) + parseInt(tdboxesProcess.text());
    dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text()).toProcess = "<input style='max-width:100px' min=0 step='" + itemFind.boxesPerPallet + "'  class='form-control form-control-sm' type='number' max='" + itemFind.boxes + "' value='0'/>";
    dataPlanCommercialX.find(someobject => someobject.planCustomerVarietyID == tdplanCustomerVarietyID.text()).inProcess = parseInt(itemFind.inProcess) - parseInt(tdboxesProcess.text());

    DataFiltered();
    $(trDetele).remove();

}

function DownloadTemplate() {

    var createXLSLFormatObj = [];

    var xlsHeader = ["Brand", "Variety", "Size", "Format", "Customer", "Destination", "Program", "Priority", "Status", "Via", "Codepack"]
    $.each(dataWeek, function (iWeek, week) {
        xlsHeader.push("Week" + (parseInt(week.number) + parseInt(1)));
    });

    var xlsRows;
    /* XLS Rows Data */
    if (dataExport.length > 0) {
        xlsRows = dataExport;
    } else {
        xlsRows = dataPlan;
    }

    /* XLS Rows Data */
    createXLSLFormatObj.push(xlsHeader);

    $.each(dataMartet, function (iCat, objCat) {
        var dataPlanByVarCat = xlsRows.filter(item => item.marketID == objCat.marketID);
        if (dataPlanByVarCat.length > 0) {
            var dataCustomer1 = [];
            $.each(dataPlanByVarCat, function (ii, ee) {
                var flagFind = false;
                $.each(dataCustomer1, function (yy, xx) {
                    if (xx == ee.planCustomerVarietyID) {
                        flagFind = true;
                    }
                })
                if (flagFind == false) {
                    dataCustomer1.push(ee.planCustomerVarietyID)
                }
            })

            var tr = ''
            $.each(dataCustomer1, function (iplanCustomerVarietyID, planCustomerVarietyID) {
                var dataBD = dataPlanByVarCat.filter(item => item.planCustomerVarietyID == planCustomerVarietyID)
                var innerRowData = [];
                innerRowData.push(dataBD[0].category);
                innerRowData.push(dataBD[0].variety);
                innerRowData.push(dataBD[0].size);
                innerRowData.push(dataBD[0].codePackDES);
                innerRowData.push(dataBD[0].customer);
                innerRowData.push(dataBD[0].destination);
                innerRowData.push(dataBD[0].program);
                innerRowData.push(dataBD[0].priority);
                innerRowData.push(dataBD[0].statusConfirm);
                innerRowData.push(dataBD[0].via);
                innerRowData.push(dataBD[0].codePack);

                $.each(dataWeek, function (iWeek, week) {
                    var flagFind = false
                    $.each(dataBD, function (iWWeek, wweek) {
                        if (week.projectedWeekID == wweek.projectedWeekID - 1 && flagFind == false) {
                            flagFind = true;
                            innerRowData.push(parseFloat(wweek.boxes.replace(",", "")).toFixed(2))
                        }
                    })
                    if (flagFind == false) {
                        innerRowData.push(0)
                    }
                });
                createXLSLFormatObj.push(innerRowData);
            })

        }
    });

    /* File Name */
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var curHour = today.getHours() > 12 ? today.getHours() : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
    var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    today = mm + '/' + dd + '/' + yyyy;
    var now = yyyy + mm + dd + "-" + curHour + curMinute;

    var filename = "Programa Comercial " + localStorage.campaign + "_" + now + "_XLS.xlsx";
    /* Sheet Name */
    var ws_name = "FreakySheet";

    //if (typeof console !== 'undefined') //console.log(new Date());
    var wb = XLSX.utils.book_new();
    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

    /* Add worksheet to workbook */
    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    /* Write workbook and Download */
    //if (typeof console !== 'undefined') //console.log(new Date());
    XLSX.writeFile(wb, filename);
    //if (typeof console !== 'undefined') //console.log(new Date());
}

function DataFiltered() {
    table = $('#tblCommercialPlan').DataTable({
        order: false,
        fixedHeader: true,
        destroy: true,
        "data": dataPlanCommercial,
        "columns": [
            { "max-width": "100px", "data": "planCustomerVarietyID", visible: false, className: 'tdplanCustomerVarietyID' },

            { "max-width": "100px", "data": "addBoxes", visible: false, className: 'tdaddBoxes' },
            { "max-width": "20px", "data": "customerID", visible: false, className: 'tdcustomerID' },
            { "max-width": "20px", "data": "programID", visible: false, className: 'tdprogramID' },
            { "max-width": "20px", "data": "destinationID", visible: false, className: 'tddestinationID' },
            { "max-width": "20px", "data": "categoryID", visible: false, className: 'tdcategoryID' },
            { "max-width": "20px", "data": "varietyID", visible: false, className: 'tdvarietyID' },
            { "max-width": "20px", "data": "sizeID", visible: false, className: 'tdsizeID' },
            { "max-width": "20px", "data": "codePackID", visible: false, className: 'tdcodepackID' },
            { "max-width": "20px", "data": "viaID", visible: false, className: 'tdviaID' },
            { "max-width": "20px", "data": "marketID", visible: false, className: 'tdmarketID' },
            { "max-width": "20px", "data": "wowID", visible: false, className: 'tdwowID' },
            { "max-width": "20px", "data": "incotermID", visible: false, className: 'tdincotermID' },
            { "max-width": "20px", "data": "incoterm", visible: false, className: 'tdincoterm' },
            { "max-width": "80px", "data": "codePack", visible: false, className: 'tdcodePack' },
            { "max-width": "60px", "data": "price", visible: false, className: 'tdprice' },

            { "max-width": "200px", "data": "customer", className: 'tdcustomer' },
            { "max-width": "100px", "data": "destination", className: 'tddestination' },
            { "max-width": "80px", "data": "priority", className: 'tdpriority' },
            { "max-width": "50px", "data": "via", className: 'tdvia' },
            { "max-width": "50px", "data": "category", className: 'tdcategory' },
            { "max-width": "150px", "data": "variety", className: 'tdvariety' },
            { "max-width": "100px", "data": "format", className: 'tdformat' },
            { "max-width": "50px", "data": "boxesPerPallet", className: 'tdboxesPerPallet' },
            { "max-width": "60px", "data": "boxes", className: 'tdboxes' },
            { "max-width": "70px", "min-width": "70px", "data": "toProcess", className: 'tdtoProcess' },
            { "max-width": "30px", "data": "add", className: 'tdadd' },
            { "max-width": "70px", "min-width": "70px", "data": "inProcess", className: 'tdinProcess' },
            { "max-width": "100px", "min-width": "100px", "data": "proceced", visible: false, className: 'tdproceced' }
        ],
        rowReorder: { dataSrc: 'planCustomerVarietyID' }

    });
}

function addRowToProcess(trProcess) {
    $(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]).visible(true);

    var addbox = parseInt(trProcess.find('td.tdaddBoxes').text());
    var planCustomerVarietyID = parseInt(trProcess.find('td.tdplanCustomerVarietyID').text())
    var totalBoxes = trProcess.find('td.tdboxes').text();
    var boxesToProcess = parseInt(trProcess.find('td.tdtoProcess>input').val());
    var boxesInProcess = parseInt(trProcess.find('td.tdinProcess').text());
    var diference = totalBoxes - boxesToProcess
    var totalInProcess = boxesInProcess + boxesToProcess
    var palletxBox = parseInt(trProcess.find('td.tdboxesPerPallet').text());

    var totalPallets = (boxesToProcess / palletxBox)

    if (diference < 0) {
        sweet_alert_error("Error", "review requested boxes")
        $(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]).visible(false);
        return;
    }
    if (!Number.isInteger(totalPallets)) {
        sweet_alert_error("Error", "boxes must be exact pallets")
        $(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]).visible(false);
        return;
    }

    trProcess.find('td.tdboxes').text(diference);
    trProcess.find('td.tdinProcess').text(totalInProcess);

    dataPlanCommercial.find(x => x.planCustomerVarietyID == planCustomerVarietyID).boxes = diference;
    dataPlanCommercial.find(x => x.planCustomerVarietyID == planCustomerVarietyID).inProcess = totalInProcess;

    //>>>Forzamos a que se muestre oculta la columna del plan comercial
    $("#tblToProcess>tbody>tr").each(function (i, tr) {
        if ($(tr).find('td.tdplanCustomerVarietyID').text() == planCustomerVarietyID) {
            $(tr).find('td.tdplanCustomerVarietyID').text()
        }
    })
    //<<<Forzamos a que se muestre oculta la columna del plan comercial

    var qPO = parseFloat(totalPallets) / 20;
    if (!Number.isInteger(qPO)) {
        var residuo = totalPallets % 20
        var residuoBoxes = boxesToProcess % (palletxBox * 20)
        drawRowToProcess(trProcess, residuo, residuoBoxes)
        totalPallets = totalPallets - residuo

    }

    qPO = parseInt(totalPallets) / 20;
    var i;
    for (i = 0; i < qPO; i++) {
        drawRowToProcess(trProcess, 20, palletxBox * 20)
    }

    $(tblCommercialPlan).DataTable().columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]).visible(false);

}

function drawVarietysToProcess() {
    let iprojectedWeekID = 0;
    var lstWeekByCampaign;

    //>>>Obtener la semana actual y semanas por campaña a mostrarse en el modal que contendrá los planes comercial (de la semana que se seleccione) a procesar
    var opt = "act";
    var id = "";
    var cropID = localStorage.cropID;
    var campaignID = localStorage.campaignID;

    $.ajax({
        async: false,
        type: 'GET',
        url: "/Forecast/ListWeeksMC?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            let objCurrentWeek = JSON.parse(data);
            iprojectedWeekID = objCurrentWeek[0].projectedWeekID;

        }
    });

    $.ajax({
        async: false,
        type: 'GET',
        url: "/Forecast/ListWeekByCampaign?campaignID=" + localStorage.campaignID,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                let dataJson = JSON.parse(data);
                lstWeekByCampaign = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description,
                        }
                    }
                );
            }
        }
    });
    loadCombo(lstWeekByCampaign, 'selectWeekProcess', false);
    $('#selectWeekProcess').prop('selectedIndex', 0);
    $('#selectFilterVariety').prop('selectedIndex', 0);
    //<<<Obtener la semana actual y semanas por campaña a mostrarse en el modal que contendrá los planes comercial (de la semana que se seleccione) a procesar
    loadPlanComercial();
}

function loadPlanComercial() {
    GetCommercialPlanByCampaignAndWeekToShowInTableCommercialPlan();
    //>>>Dibujar las variedades que contendrá los planes comerciales a procesar por variedad
    cleartblToProcess();
    //<<<Dibujar las variedades que contendrá los planes comerciales a procesar por variedad
}

function drawRowToProcess(trProcess, totalPallets, boxes) {
    
    _contador = _contador + 1;
    var tr = ''
    var flag = 0
    var iNroVarietyXCategory = 0;
    $('#tblToProcess>tbody>tr').each(function (i, e) {
        if ($(e).find('td.tdStatus').text() == '2' && trProcess.find('td.tdcategoryID').text().trim() == $(e).find('td.tdcategoryID').text().trim()) {
            flag = 1;
            tr = $(e);
            iNroVarietyXCategory = $(e).find('td.tdVarietyXCategory').text()
        }
        if (flag == 1) {
            if ($(e).find('td.tdStatus').text() == '2') {
                flag = 0;
                return
            }
            tr = $(e)
        }
    })

    var trAdd = '<tr>'
    trAdd += '<td style="display:none" class="tdVarietyXCategory">' + iNroVarietyXCategory + '</td>'
    trAdd += '<td style="display:none" class="tdplanCustomerVarietyID">' + trProcess.find('td.tdplanCustomerVarietyID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdcategoryID">' + trProcess.find('td.tdcategoryID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdvarietyID">' + trProcess.find('td.tdvarietyID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdcustomerID">' + trProcess.find('td.tdcustomerID').text() + '</td>'
    trAdd += '<td style="display:none" class="tddestinationID">' + trProcess.find('td.tddestinationID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdviaID">' + trProcess.find('td.tdviaID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdicotermID">' + trProcess.find('td.tdicotermID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdpaymentID">' + trProcess.find('td.tdpaymentID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdformatID">' + trProcess.find('td.tdcodepackID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdSizeID">' + trProcess.find('td.tdsizeID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdWowID">' + trProcess.find('td.tdwowID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdMarketID">' + trProcess.find('td.tdmarketID').text() + '</td>'
    trAdd += '<td style="display:none" class="tdcodePack">' + trProcess.find('td.tdcodePack').text() + '</td>'
    trAdd += '<td class="tdcategory cat' + trProcess.find('td.tdcategoryID').text() + '">' + trProcess.find('td.tdcategory').text() + '</td>'
    trAdd += '<td class="tdvariety" style="min-width:100px;text-align: center;">' + trProcess.find('td.tdvariety').text() + '</td>'
    trAdd += '<td class="tdsize"  style="text-align: center; font-size:12px;max-width:100px;min-width:100px">';
    trAdd += '<select id ="selectSizePr' + _contador + '" class="form-control form-control-sm" multiple="multiple" style="font-size:12px!important">';
    trAdd += '</select>';
    trAdd += '</td>';
    trAdd += '<td class="tdcustomer">' + trProcess.find('td.tdcustomer').text() + '</td>'
    trAdd += '<td class="tddestination">' + trProcess.find('td.tddestination').text() + '</td>'
    trAdd += '<td class="tdprogram">' + trProcess.find('td.tdprogram').text() + '</td>'
    trAdd += '<td class="tdpriority" style="display:none">' + trProcess.find('td.tdpriority').text() + '</td>'
    trAdd += '<td class="tdvia">' + trProcess.find('td.tdvia').text() + '</td>'
    trAdd += '<td class="tdicoterm">' + trProcess.find('td.tdincoterm').text() + '</td>'
    trAdd += '<td class="tdpriceperbox">' + "<input style='max-width:70px' min=0  class='form-control form-control-sm' type='number'  value='" + trProcess.find('td.tdprice').text() + "'/>" + '</td>'
    trAdd += '<td class="tdformat">' + trProcess.find('td.tdformat').text() + '</td>'

    trAdd += '<td class="tdwork onoffswitch" style="text-align: center; font-size:12px;max-width:80px;min-width:50px">';
    trAdd += '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="checkwork" tabindex="0" checked disabled>';
    trAdd += '<label class="onoffswitch-label" for="checkwork"><span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span></label>';
    trAdd += '</td>';

    //trAdd += '<td id="" class="tdwork onoffswitch" style="text-align: center; font-size:12px;max-width:80px;min-width:50px"> <div class="btn-group btn-group-sm" role="group" aria-label="..."><span style="display:none" id="statusTablaID"></span>';
    //trAdd += '<button id="SI" class="btn btn-sm" type ="button" style = "padding: 0 5px 0 5px;background-color:#4CAF50; color:white" disabled>YES</button> <button id="No" class="btn btn-sm" style="padding: 0 5px 0 5px; color:white; background-color: #bdb9b9" type="button" disabled>NO</button>'
    //trAdd += '</div ></td > '

    var palletxBox = parseInt(trProcess.find('td.tdboxesPerPallet').text());

    trAdd += '<td class="tdboxesPerPallet">' + palletxBox + '</td>'
    trAdd += '<td class="tdtoProcess">' + boxes + '</td>'

    if (totalPallets == 20) {
        trAdd += '<td class="tdPallets palletComplete" >' + totalPallets + '</td>'
    } else {
        trAdd += '<td class="tdPallets residuesinComplete">' + totalPallets + '</td>'
    }
    trAdd += '<td class="tdDelete"><button class="btnDeleteRowToProcess">Delete</button></td>'
    trAdd += '<td class="tdSort" rowspan="1">' + 0 + '</td>'
    trAdd += '<td style="display:none;" class="tdStatus">0</td>'
    if (totalPallets == 20) {
        trAdd += '<td style="display:none;" class="tdMixed" >0</td>'
    } else {
        trAdd += '<td style="display:none;" class="tdMixed">1</td>'
    }
    trAdd += '</tr>'
    $(tr).after(trAdd)
    $(tr).hide();
    $("#selectSizePr" + _contador).empty();

    $.each(dataSizes, function (i, item) {
        $("#selectSizePr" + _contador).append($('<option>', {
            value: item.id,
            text: item.name
        }));
    });
    SettingCustomizedMultiselectST($('#selectSizePr' + _contador));

    var sizes = trProcess.find('td.tdsizeID').text().split(',');
    if (sizes.length > 0) {
        $("#selectSizePr" + _contador).multiselect('select', sizes).multiselect('refresh');
    }

    $("#tblToProcess").tableDnD({
        onDragStop: function (table, row) {

            verifyRowsResidues();
        }
    });
        
}

function verifyRowsResidues() {

    var trAux = [];
    var iBoxes = 0;
    var iIndex = 0;
    var customer = 0;
    var destination = 0;
    var contador = 0;
    var inicial = 0; //variable que indica si ya ingreso a aereos
    var posc1 = 1; //acumulador para las posiciones de aereos	
    var posición = 0;//variable para el sort del agrupamiento
    var flag_maritimo = 0;

    $("#tblToProcess>tbody>").each(function (i, tr) {

        var pallets = parseInt($(tr).find('td.tdPallets').text())
        var via = parseInt($(tr).find('td.tdviaID').text())

        if ($(tr).find('td.tdStatus').text() == '2') {
            iBoxes = 0;
            trAux = [];
            iIndex = 0;
            return;
        }

        iIndex = iIndex + 1;
        $(tr).find('td.tdSort').text(iIndex)

        if (pallets < 20 && via == 1) {
            $(tr).find('td.tdPallets').removeClass("residuesComplete").addClass("residuesinComplete")
            $(tr).find('td.tdSort').attr('rowspan', 1);
            $(tr).find('td.tdSort').show()
            $(tr).find('td.tdStatus').text('0')

            flag_maritimo = 1;
            contador = 0;

            iBoxes = pallets + iBoxes;
            trAux.push(tr)
            inicial = inicial + 1;
            posc1 = iIndex;

            if (iBoxes <= 20 && customer == parseInt($(tr).find('td.tdcustomerID').text()) && destination == parseInt($(tr).find('td.tddestinationID').text())) {

                iTotVarietyXCategory++;
                contador = contador + 1;
                posc1 = posc1 + 1;

                $.each(trAux, function (ii, ttr) {

                    $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                    $(ttr).find('td.tdStatus').text('1')

                    if (ii == 0) {

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(ttr).find('td.tdSort').attr('rowspan', trAux.length);
                        posición = parseInt($(ttr).find('td.tdSort').text())
                    } else {

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(ttr).find('td.tdSort').hide()
                        $(ttr).find('td.tdSort').text(posición)
                    }

                    $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);

                });
                if ((posc1 - trAux.length) < 0) {
                    posc1 = 1;
                } else {
                    posc1 = posc1 - trAux.length + 1;
                }
                iIndex = posición;
            } else {
                customer = parseInt($(tr).find('td.tdcustomerID').text());
                destination = parseInt($(tr).find('td.tddestinationID').text());
                var rowCount = $('#tblToProcess tr').length;

                if (inicial > 1 && contador == 0) {
                    // cuando el segundo registro  es distinto al primero
                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    $.each(trAux, function (ii, ttr) {
                        $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                        $(ttr).find('td.tdStatus').text('1')

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(tr).find('td.tdSort').text(posc1)
                    });

                    iBoxes = 0
                    trAux = []
                    trAux.push(tr)
                    contador = 0;
                } else if (contador > 0) {
                    //Cuando ingreso anteriormente a agrupar registros
                    iBoxes = 0
                    trAux = [];
                    trAux.push(tr);
                    contador = 0;

                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    if (posición > 0) {
                        posc1 = posición + 1;
                        iIndex = posc1;
                    }

                    $.each(trAux, function (ii, ttr) {
                        if (ii == 0) {
                            $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                            $(ttr).find('td.tdStatus').text('1')

                            $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                            $(ttr).find('td.tdSort').attr('rowspan', trAux.length);

                            $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                            $(tr).find('td.tdSort').text(posc1)

                        }
                    });
                } else if (rowCount - 3 == i || rowCount - 4 == i) {
                    // cuando es el ultimo registro
                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    if (posición > 0) { posc1 = posición + 1; }
                    $.each(trAux, function (ii, ttr) {
                        $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                        $(ttr).find('td.tdStatus').text('1')

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(tr).find('td.tdSort').text(posc1)
                    });

                    iBoxes = 0
                    trAux = []
                    trAux.push(tr)
                    contador = 0;
                }
                else if ((i == 1 || i == 2) && rowCount - 3 > 1) {

                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    if (posición > 0) { posc1 = posición + 1; iIndex = posc1; }
                    $.each(trAux, function (ii, ttr) {
                        $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                        $(ttr).find('td.tdStatus').text('1')

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(tr).find('td.tdSort').text(posc1)
                    });

                    iBoxes = 0
                    trAux = []
                    trAux.push(tr)
                    contador = 0;
                }
            }

            if (iBoxes == 20) {
                if ((iIndex - trAux.length) < 0) {
                    iIndex = 1;
                } else {
                    iIndex = iIndex - trAux.length + 1;
                }
                iTotVarietyXCategory++;
                $.each(trAux, function (ii, ttr) {
                    $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                    $(ttr).find('td.tdStatus').text('1')
                    if (ii == 0) {
                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(ttr).find('td.tdSort').attr('rowspan', trAux.length);
                    } else {
                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(ttr).find('td.tdSort').hide()
                    }

                    $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                    $(tr).find('td.tdSort').text(iIndex)
                })
                iBoxes = 0
                trAux = []
            }
            else if (iBoxes > 20) {
                iBoxes = pallets
                trAux = []
                trAux.push(tr)
            }

        } else if (pallets < 20 && via == 2) {

            if (flag_maritimo > 0) {
                trAux = [];
                flag_maritimo = 0;
            }

            posc1 = iIndex;
            $(tr).find('td.tdPallets').removeClass("residuesComplete").addClass("residuesinComplete")
            $(tr).find('td.tdSort').attr('rowspan', 1);
            $(tr).find('td.tdSort').show()
            $(tr).find('td.tdStatus').text('0')
            inicial = inicial + 1;

            iBoxes = pallets + iBoxes;
            trAux.push(tr)

            if (iBoxes <= 20 && customer == parseInt($(tr).find('td.tdcustomerID').text()) && destination == parseInt($(tr).find('td.tddestinationID').text())) {

                iTotVarietyXCategory++;
                contador = contador + 1;
                posc1 = posc1 + 1;

                $.each(trAux, function (ii, ttr) {

                    $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                    $(ttr).find('td.tdStatus').text('1')

                    if (ii == 0) {

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(ttr).find('td.tdSort').attr('rowspan', trAux.length);
                        posición = parseInt($(ttr).find('td.tdSort').text())
                    } else {

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(ttr).find('td.tdSort').hide()
                        $(ttr).find('td.tdSort').text(posición)
                    }

                    $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);

                });
                if ((posc1 - trAux.length) < 0) {
                    posc1 = 1;
                } else {
                    posc1 = posc1 - trAux.length + 1;
                }
                iIndex = posición;
            }
            else if (iBoxes > 20) {
                iBoxes = pallets
                trAux = []
                trAux.push(tr)
                customer = 0;
                destination = 0;
            }
            else {
                customer = parseInt($(tr).find('td.tdcustomerID').text());
                destination = parseInt($(tr).find('td.tddestinationID').text());
                var rowCount = $('#tblToProcess tr').length;

                if (inicial > 1 && contador == 0) {
                    // cuando el segundo registro  es distinto al primero
                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    $.each(trAux, function (ii, ttr) {
                        $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                        $(ttr).find('td.tdStatus').text('1')

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(tr).find('td.tdSort').text(posc1)
                    });

                    iBoxes = 0
                    trAux = []
                    trAux.push(tr)
                    contador = 0;
                } else if (contador > 0) {
                    //Cuando ingreso anteriormente a agrupar registros
                    iBoxes = 0
                    trAux = [];
                    trAux.push(tr);
                    contador = 0;

                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    if (posición > 0) {
                        posc1 = posición + 1;
                        iIndex = posc1;
                    }

                    $.each(trAux, function (ii, ttr) {
                        if (ii == 0) {
                            $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                            $(ttr).find('td.tdStatus').text('1')

                            $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                            $(ttr).find('td.tdSort').attr('rowspan', trAux.length);

                            $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                            $(tr).find('td.tdSort').text(posc1)

                        }
                    });
                } else if (rowCount - 3 == i || rowCount - 4 == i) {
                    // cuando es el ultimo registro
                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    if (posición > 0) { posc1 = posición + 1; }
                    $.each(trAux, function (ii, ttr) {
                        $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                        $(ttr).find('td.tdStatus').text('1')

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(tr).find('td.tdSort').text(posc1)
                    });

                    iBoxes = 0
                    trAux = []
                    trAux.push(tr)
                    contador = 0;
                }
                else if ((i == 1 || i == 2) && rowCount - 3 > 1) {

                    iTotVarietyXCategory++;
                    contador = contador + 1;
                    if (posición > 0) { posc1 = posición + 1; iIndex = posc1; }
                    $.each(trAux, function (ii, ttr) {
                        $(ttr).find('td.tdPallets').removeClass("residuesinComplete").addClass("residuesComplete")
                        $(ttr).find('td.tdStatus').text('1')

                        $(ttr).find('td.tdVarietyXCategory').text(iTotVarietyXCategory);
                        $(tr).find('td.tdSort').text(posc1)
                    });

                    iBoxes = 0
                    trAux = []
                    trAux.push(tr)
                    contador = 0;
                }
            }
        }
        else {
            $(tr).find('td.tdStatus').text('1')
            iBoxes = 0
            trAux = []
            customer = 0
            destination = 0;
            flag_maritimo = 1;
            contador = 0;
        }
    })
}

function GetCommercialPlanByCampaignAndWeekToShowInTableCommercialPlan() {
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/CommercialPlan/PlanCustomerVarietyListByWeekID?campaignID="
            + localStorage.campaignID + "&projectedWeekID="
            + $("#selectWeekProcess option:selected").val(),
        //async: false,
        success: function (data) {
            dataPlanCommercial = JSON.parse(data);
            dataPlanCommercial = dataPlanCommercial.map(
                obj => {
                    return {
                        "planCustomerVarietyID": obj.planCustomerVarietyID,
                        "varietyID": obj.varietyID,
                        "categoryID": obj.categoryID,
                        "customerID": obj.customerID,
                        "destinationID": obj.destinationID,
                        "variety": obj.variety,
                        "category": obj.category,
                        "customer": '<textarea rows="1" style="padding:0 0 5px 0px; font-size:13px" class="form-control form-control-sm" readonly>' + obj.customer + '</textarea>',
                        "program": obj.program,
                        "programID": obj.programID,
                        "priority": obj.priority,
                        "destination": obj.destination,
                        "codePack": obj.codePack,
                        "codePackID": obj.codePackID,
                        "boxesPerPallet": obj.boxesPerPallet,
                        "boxes": obj.boxes,
                        "toProcess": "<input style='max-width:100px' min=0 step='" + obj.boxesPerPallet + "' class='form-control form-control-sm' type='number' max='" + obj.boxes + "' value='0'/>",
                        "add": '<button class="btnAddProcess">Add</button>',
                        "price": obj.price,
                        "inProcess": 0,
                        "proceced": obj.boxes,
                        "padlock": obj.padlock,
                        "addBoxes": obj.addBoxes,
                        "customerDescription": obj.customer,
                        "via": obj.via,
                        "viaID": obj.viaID,
                        "format": obj.codePackDES,
                        "sizeID": obj.sizeID,
                        "marketID": obj.marketID,
                        "wowID": obj.consigneeID,
                        "incoterm": obj.incoterm,
                        "incotermID": obj.incotermID,
                        "locked": (obj.padlock == 1) ? '<span style="font-size: 200%;color: tomato;"><i class="fa fa-lock"></i></span>' : '<span style="font-size: 200%;color: forestgreen;"><i class="fa fa-unlock"></i></span>',

                    }
                }
            );
            table = $('#tblCommercialPlan').DataTable({
                fixedHeader: true,
                destroy: true,
                "data": dataPlanCommercial,
                "columns": [
                    { "max-width": "100px", "data": "planCustomerVarietyID", visible: false, className: 'tdplanCustomerVarietyID' },

                    { "max-width": "100px", "data": "addBoxes", visible: false, className: 'tdaddBoxes' },
                    { "max-width": "20px", "data": "customerID", visible: false, className: 'tdcustomerID' },
                    { "max-width": "20px", "data": "programID", visible: false, className: 'tdprogramID' },
                    { "max-width": "20px", "data": "destinationID", visible: false, className: 'tddestinationID' },
                    { "max-width": "20px", "data": "categoryID", visible: false, className: 'tdcategoryID' },
                    { "max-width": "20px", "data": "varietyID", visible: false, className: 'tdvarietyID' },
                    { "max-width": "20px", "data": "sizeID", visible: false, className: 'tdsizeID' },
                    { "max-width": "20px", "data": "codePackID", visible: false, className: 'tdcodepackID' },
                    { "max-width": "20px", "data": "viaID", visible: false, className: 'tdviaID' },
                    { "max-width": "20px", "data": "marketID", visible: false, className: 'tdmarketID' },
                    { "max-width": "20px", "data": "wowID", visible: false, className: 'tdwowID' },
                    { "max-width": "20px", "data": "incotermID", visible: false, className: 'tdincotermID' },
                    { "max-width": "20px", "data": "incoterm", visible: false, className: 'tdincoterm' },
                    { "max-width": "80px", "data": "codePack", visible: false, className: 'tdcodePack' },
                    { "max-width": "60px", "data": "price", visible: false, className: 'tdprice' },

                    { "max-width": "200px", "data": "customer", className: 'tdcustomer' },
                    { "max-width": "100px", "data": "destination", className: 'tddestination' },
                    { "max-width": "80px", "data": "priority", className: 'tdpriority' },
                    { "max-width": "50px", "data": "via", className: 'tdvia' },
                    { "max-width": "50px", "data": "category", className: 'tdcategory' },
                    { "max-width": "150px", "data": "variety", className: 'tdvariety' },
                    { "max-width": "100px", "data": "format", className: 'tdformat' },
                    { "max-width": "50px", "data": "boxesPerPallet", className: 'tdboxesPerPallet' },
                    { "max-width": "60px", "data": "boxes", className: 'tdboxes' },
                    { "max-width": "70px", "min-width": "70px", "data": "toProcess", className: 'tdtoProcess' },
                    { "max-width": "30px", "data": "add", className: 'tdadd' },
                    { "max-width": "70px", "min-width": "70px", "data": "inProcess", className: 'tdinProcess' },
                    { "max-width": "100px", "min-width": "100px", "data": "proceced", visible: false, className: 'tdproceced' }
                ],
                rowReorder: { dataSrc: 'planCustomerVarietyID' }
            });
        },
        complete: function () {

        }
    });
}

let UniqueData = (arr) => {
    //To store the unique sub arrays
    let uniques = [];

    //To keep track of the sub arrays
    let itemsFound = {};

    for (let val of arr) {
        //convert the sub array to the string
        let stringified = JSON.stringify(val);

        //If it is already added then skip to next element
        if (itemsFound[stringified]) {
            continue;
        }

        //Else add the value to the unique list
        uniques.push(val);

        //Mark it as true so that it can tracked
        itemsFound[stringified] = true;
    }

    //Return the unique list
    return uniques;
}

function ShowCommercialPlanForEachVarietyAndCategory() {
    $.each(dataMartet, function (iCat, objMk) {
        var dataPlanByVarCat = dataPlan.filter(item => item.marketID == objMk.marketID);
        if (dataPlanByVarCat.length > 0) {
            var dataCustomer1 = [];
            $.each(dataPlanByVarCat, function (ii, ee) {
                var flagFind = false;
                $.each(dataCustomer1, function (yy, xx) {
                    if (xx == ee.planCustomerVarietyID) {
                        flagFind = true;
                    }
                })
                if (flagFind == false) {
                    dataCustomer1.push(ee.planCustomerVarietyID)
                }
            })

            var tr = ''
            $.each(dataCustomer1, function (iplanCustomerVarietyID, planCustomerVarietyID) {
                var dataBD = dataPlanByVarCat.filter(item => item.planCustomerVarietyID == planCustomerVarietyID)
                tr = prinRowByTrBd(dataBD) + tr
            })
            var id = 'tblPlan-VR-' + objMk.marketID;
            $("#" + id + ">tbody").empty().append(tr);

            calculateProjectedSales("table#" + id)
            tablePC = $('#tblPlan-VR-' + objMk.marketID + '');
            tablePC.removeClass("table-responsive");

            //tablePC.DataTable({
            //    ordering: false,

            //    paging: false,
            //    autoWidth: false,
            //    "fnInitComplete": function () {
            //        $('.dataTables_scrollBody thead tr').css({ visibility: 'collapse' });
            //        $('.dataTables_scrollBody tfoot tr').css({ visibility: 'collapse' });

            //    },
            //    "scrollY": "400px",
            //    "scrollX": true,
            //    scrollCollapse: true,
            //    "dom": 'frtp',
            //    "responsive": true,
            //    searching: false,

            //});
            
        }
    });
}
$(document).ready(function () {
    $('a[data-toggle="pill"]').on('shown.bs.pill', function (e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });

});

function prinRowByTrBd(dataBD) {

    var table = ''
    if (dataBD[0].statusConfirmID == "1") {
        table = '<tr style="text-align:center ">'
    } else {
        table = '<tr style="text-align:center" class="table-secondary">'
    }

    table += '<td style="min-width:120px;max-width:120px" class="tdBtn sticky" ><button style="font-size:14px" class=" btn btn-sm btn-danger btnRowDelete"><i class="fas fa-trash fa-sm"></i></button><button class="btn btn-primary btnRowEdit btn-sm " style="font-size:14px"><i class="fas fa-edit fa-sm"></i></button><button class="btn btn-success btn-sm btnRowSave" style="font-size:14px;display:none"><i class="fas fa-save fa-sm"></i></button></td>'
    table += '<td style="display:none" class="tdPlanCustomerVarietyID">' + dataBD[0].planCustomerVarietyID + '</td>'
    table += '<td style="min-width:50px;max-width:50px" class="tdBrand sticky2">' + dataBD[0].category + '</td>'
    table += '<td style="min-width:130px;max-width:130px" class="tdVariety sticky3"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + dataBD[0].variety + '</textarea></td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdSize sticky4">' + dataBD[0].sizeID + '</td>'
    table += '<td style="min-width:100px;max-width:100px" class="tdFormat sticky5">' + dataBD[0].codePackDES + '</td>'
    table += '<td style="min-width:200px;max-width:200px" class="tdClient sticky6"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + dataBD[0].customer + '</textarea></td>'
    table += '<td style="min-width:120px;max-width:120px" class="tdProgram sticky7"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + dataBD[0].program + '</textarea></td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdDestination sticky8">' + dataBD[0].destination + '</td>'
    table += '<td style="min-width:30px;max-width:30px" class="tdVia sticky9">' + dataBD[0].via + '</td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdPiority sticky10">' + dataBD[0].priority + '</td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdStatus sticky11">' + dataBD[0].statusConfirm + '</td>'
    table += '<td style="display:none;" class="tdCodepack"><textarea class="form - control form - control - sm" rows="1">' + dataBD[0].codePack + '</textarea></td>'
    table += '<td style="display:none" class="tdMarketID">' + dataBD[0].marketID + '</td>'
    table += '<td style="display:none" class="tdClientID">' + dataBD[0].customerID + '</td>'
    table += '<td style="display:none" class="tdProgramID">' + dataBD[0].programID + '</td>'
    table += '<td style="display:none" class="tdDestinationID">' + dataBD[0].destinationID + '</td>'
    table += '<td style="display:none" class="tdConsigneeID">' + dataBD[0].consigneeID + '</td>'
    table += '<td style="display:none" class="tdViaID">' + dataBD[0].viaID + '</td>'
    table += '<td style="display:none" class="tdBrandID">' + dataBD[0].categoryID + '</td>'
    table += '<td style="display:none" class="tdVarietyID">' + dataBD[0].varietyID + '</td>'
    table += '<td style="display:none" class="tdPiorityID">' + dataBD[0].priorityID + '</td>'
    table += '<td style="display:none" class="tdFormatID">' + dataBD[0].codePackID + '</td>'
    table += '<td style="display:none" class="tdTypeBoxID">' + dataBD[0].packageProductID + '</td>'
    table += '<td style="display:none" class="tdPresentationID">' + dataBD[0].presentationID + '</td>'
    table += '<td style="display:none" class="tdCodepackID">' + dataBD[0].codePackID + '</td>'
    table += '<td style="display:none" class="tdstatusConfirmID">' + dataBD[0].statusConfirmID + '</td>'
    table += '<td style="display:none" class="tdLabelID">' + dataBD[0].categoryID + '</td>'
    table += '<td style="display:none" class="tdBoxPerPallet">' + dataBD[0].boxesPerPallet + '</td>'
    table += '<td style="display:none" class="tdBoxPerContainer">' + dataBD[0].boxPerContainer + '</td>'
    table += '<td style="display:none" class="tdIncotermID">' + dataBD[0].incotermID + '</td>'
    table += '<td style="display:none" class="tdPaymentTerms">' + dataBD[0].paymentTermID + '</td>'
    table += '<td style="display:none" class="tdPriceConditions">' + dataBD[0].conditionPaymentID + '</td>'
    table += '<td style="display:none" class="tdKAMID">' + dataBD[0].kamID + '</td>'
    table += '<td style="display:none" class="tdCategoryID">' + dataBD[0].categoryID + '</td>'
    table += '<td style="display:none" class="tdsizeID">' + dataBD[0].sizeID + '</td>'
    table += '<td style="display:none" class="tdprice">' + dataBD[0].price + '</td>'

    $.each(dataWeek, function (iWeek, week) {
        var flagFind = false
        $.each(dataBD, function (iWWeek, wweek) {
            if (week.projectedWeekID == wweek.projectedWeekID - 1 && flagFind == false) {
                flagFind = true;
                table += '<td style="text-align:right;" class="tdSalesProjected" contenteditable="true" >';
                table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="' + wweek.boxes + '" style="text-align:right;max-width:100px;background-color:transparent;"/>';
                table += '</td>';
            }
        })
        if (flagFind == false) {
            table += '<td style="text-align:right;" class="tdSalesProjected" contenteditable="true" >';
            table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/>';
            table += '</td>';
        }
    });

    table += '<td class="tdTotalSalesProjected" contenteditable="true">'
    table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/></td>';
    table += '</tr>'
    return table;

}

function openModal() {
    $('#chkplu').prop('checked', false);
    $('#chkpadlock').prop('checked', false);
    $(selectClient).attr('disabled', false);
    $(selectVariety).attr('disabled', false);
    $(selectCategory).attr('disabled', false);
}

function updateRow() {
    if ($(txtCodepack).val() == '') {
        bError = true;
        sweet_alert_warning('error', 'choose codepack')
        return
    }
    if ($("#selectCodepack").val().length == 0) {
        bError = true;
        sweet_alert_warning('error', 'choose size box')
        return
    }
    $selectMarket = $("#selectMarket option:selected")
    $selectClient = $("#selectClient option:selected")
    $selectProgram = $("#selectProgram option:selected")
    $selectDestination = $("#selectDestination option:selected")
    $selectConsignee = $("#selectConsignee option:selected")
    $selectVia = $("#selectVia option:selected")
    $brandID = $("#selectBrand option:selected")
    $varietyID = $("#selectVariety")
    $selectPriority = $("#selectPriority option:selected")
    $formatID = $("#selectFormat option:selected")
    $typeBoxID = $("#selectTypeBox option:selected")
    $presentationID = $("#selectPresentation option:selected")
    $sizeID = $("#selectCodepack")
    $selectStatusID = $("#selectStatus option:selected")
    $labelID = $brandID;
    $selectIncoterm = $("#selectIncoterm option:selected")
    $selectPaymentTerms = $("#selectPaymentTerms option:selected")
    $selectPriceConditions = $("#selectPriceConditions option:selected")
    $selectKAM = $("#selectKAM option:selected")
    $categoryID = $("#selectCategory option:selected")    
    $price = $(txtPrice).val();
    

    //var padlock = 0;
    //var plu = 0;

    //if ($('#chkpadlock').is(":checked")) {
    //    padlock = 1;
    //}

    //if ($('#chkplu').is(":checked")) {
    //    plu = 1;
    //}

    $(trEdit).find('td.tdBrand').text($($brandID).text());
    $(trEdit).find('td.tdVariety').text($('#selectVariety option:selected').text());
    $(trEdit).find('td.tdSize').text($('#selectCodepack option:selected').text());
    $(trEdit).find('td.tdFormat').text($($formatID).text());
    $(trEdit).find('td.tdClient').text($($selectClient).text());
    $(trEdit).find('td.tdProgram').text($($selectProgram).text());
    $(trEdit).find('td.tdDestination').text($($selectDestination).text());
    $(trEdit).find('td.tdVia').text($($selectVia).text());
    $(trEdit).find('td.tdPiority').text($($selectPriority).text());
    $(trEdit).find('td.tdStatus').text($($selectStatusID).text());
    $(trEdit).find('td.tdCodepack').text(localStorage.codepacksToStore);
    $(trEdit).find('td.tdMarketID').text($($selectMarket).val());
    $(trEdit).find('td.tdClientID').text($($selectClient).val());
    $(trEdit).find('td.tdProgramID').text($($selectProgram).val());
    $(trEdit).find('td.tdDestinationID').text($($selectDestination).val());
    $(trEdit).find('td.tdConsigneeID').text($($selectConsignee).val());
    $(trEdit).find('td.tdViaID').text($($selectVia).val());
    $(trEdit).find('td.tdBrandID').text($($brandID).val());
    $(trEdit).find('td.tdVarietyID').text($($varietyID).val());
    $(trEdit).find('td.tdCategoryID').text($($brandID).val());
    $(trEdit).find('td.tdPiorityID').text($($selectPriority).val());
    $(trEdit).find('td.tdFormatID').text($($formatID).val());
    $(trEdit).find('td.tdTypeBoxID').text($($typeBoxID).val());
    $(trEdit).find('td.tdPresentationID').text($($presentationID).val());
    $(trEdit).find('td.tdsizeID').text($($sizeID).val())	//Esto es el Size	;
    $(trEdit).find('td.tdCodepackID').text($($formatID).val())
    $(trEdit).find('td.tdstatusConfirmID').text($($selectStatusID).val());
    $(trEdit).find('td.tdLabelID').text($($labelID).val());
    $(trEdit).find('td.tdBoxPerPallet').text($(txtBoxPerPallet).val());
    $(trEdit).find('td.tdBoxPerContainer').text($(txtBoxPerPerContainer).val());
    $(trEdit).find('td.tdIncotermID').text($($selectIncoterm).val());
    $(trEdit).find('td.tdPaymentTerms').text($($selectPaymentTerms).val());
    $(trEdit).find('td.tdPriceConditions').text($($selectPriceConditions).val());
    $(trEdit).find('td.tdKAMID').text($($selectKAM).val());
    $(trEdit).find('td.tdstatusConfirmID').text($($selectStatusID).val());
    $(trEdit).find('td.tdprice').text($price)
    if ($($selectStatusID).val() == "1") {
        $(trEdit).removeClass("table-secondary");
    } else {
        $(trEdit).addClass("table-secondary");
    }

    $(trEdit).find(".btnRowSave").show();
    $(trEdit).find(".tdBtn").attr("style", "background-color:#F9E79F");
    $(modalCodepack).modal('hide')
    bError = false;
}

function rowSave(tr, statusID) {
    $(tr).find(".btnRowSave").hide()
    $(tr).find(".tdBtn").removeAttr("style")

    var o = {
        "planCustomerVarietyID": $(tr).find(".tdPlanCustomerVarietyID").text(),
        "marketID": $(tr).find(".tdMarketID").text(),
        "customerID": $(tr).find(".tdClientID").text(),
        "programID": $(tr).find(".tdProgramID").text(),
        "destinationID": $(tr).find(".tdDestinationID").text(),
        "consigneeID": $(tr).find(".tdConsigneeID").text(),
        "viaID": $(tr).find(".tdViaID").text(),
        "brandID": $(tr).find(".tdCategoryID").text(),
        "varietyID": $(tr).find(".tdVarietyID").text(),
        "priorityID": $(tr).find(".tdPiorityID").text(),
        "formatID": 0,
        "packageProductID": $(tr).find(".tdTypeBoxID").text(),
        "presentationID": $(tr).find(".tdPresentationID").text(),
        "codepack": $(tr).find(".tdCodepack").text(),
        "statusConfirmID": $(tr).find(".tdstatusConfirmID").text(),
        "incotermID": $(tr).find(".tdIncotermID").text(),
        "paymentTermID": $(tr).find(".tdPaymentTerms").text(),
        "conditionPaymentID": $(tr).find(".tdPriceConditions").text(),
        "kamID": $(tr).find(".tdKAMID").text(),
        "labelID": $(tr).find(".tdBrandID").text(),
        "codepackID": $(tr).find(".tdFormatID").text(),
        "campaignID": localStorage.campaignID,
        "categoryID": $(tr).find(".tdCategoryID").text(),
        "category": $(tr).find(".tdCategory").text(),
        "userID": $(lblSessionUserID).text(),
        "statusID": statusID,
        "sort": 0,
        "sizeID": $(tr).find(".tdsizeID").text(),
        "price": $(tr).find(".tdprice").text(),
        "planBoxWeek": []
    }
    localStorage.codepacksToStore = '';
    $.each(dataWeek, function (iWeek, week) {
        var planBoxWeek = {
            "planBoxWeekID": 0,
            "planCustomerVarietyID": $(tr).find(".tdPlanCustomerVarietyID").text(),
            "projectedWeekID": week.projectedWeekID,
            "boxes": $(tr).find(".tdSalesProjected:eq(" + iWeek + ")").find("input.txtBoxNumber").val().replace(",", ""),
            "price": 0,
            "userID": $(lblSessionUserID).text()
        }
        o.planBoxWeek.push(planBoxWeek)

    })

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/CommercialPlan/CreateCommercialPlanMultiVariertySize",
        //async: true,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares[0].planCustomerVarietyID > 0) {
                if (statusID == 1) {
                    $(tr).find(".tdPlanCustomerVarietyID").text(datares[0].planCustomerVarietyID)
                    sweet_alert_success('Good Job!', 'successfully saved');
                } else {
                    sweet_alert_success('Deleted!', 'Your item has been deleted')
                    var table = $(tr).closest('table')
                    $(tr).remove()
                    calculateProjectedSales(table);
                }
            } else {
                sweet_alert_error("Error", "There was a problem while registering the commercial plan.!");
            }
        },
        error: function (datoEr) {
            sweet_alert_error("Error", "There has been a problem!");
        }
    });
}

function rowEdit(tr) {
    $(modalCodepack).modal('show');
    $(btnAddClient).hide()
    $(btnEditClient).show()

    $(selectClient).attr('disabled', 'disabled');
    $(selectVariety).attr('disabled', 'disabled');
    $(selectCategory).attr('disabled', 'disabled');

    trEdit = tr;

    $selectMarket = $(tr).find(".tdMarketID").text();
    $selectClient = $(tr).find(".tdClientID").text();
    $selectProgram = $(tr).find(".tdProgramID").text();
    _$selectDestination = $(tr).find(".tdDestinationID").text();
    _$selectConsignee = $(tr).find(".tdConsigneeID").text();
    $selectVia = $(tr).find(".tdViaID").text();
    $selectKAM = $(tr).find(".tdKAMID").text();
    $selectPriority = $(tr).find(".tdPiorityID").text();
    $selectStatusID = $(tr).find(".tdstatusConfirmID").text();
    $varietyID = $(tr).find(".tdVarietyID").text().split(',');
    $categoryID = $(tr).find(".tdCategoryID").text();
    $formatID = $(tr).find(".tdFormatID").text();
    $typeBoxID = $(tr).find(".tdTypeBoxID").text();
    $brandID = $(tr).find(".tdBrandID").text();
    $presentationID = $(tr).find(".tdPresentationID").text();
    $labelID = $(tr).find(".tdLabelID").text();
    $codepack = $(tr).find(".tdCodepack").text();
    $codepackID = $(tr).find(".tdCodepackID").text();
    $sizeID = $(tr).find(".tdsizeID").text().split(',');
    $boxPerPallet = $(tr).find(".tdBoxPerPallet").text();
    $incotemID = $(tr).find(".tdIncotermID").text();
    _$paymentTerm = $(tr).find(".tdPaymentTerms").text();
    $priceCondition = $(tr).find(".tdPriceConditions").text();
    $price = $(tr).find(".tdprice").text();

    //let arrayCodePack = $codepack.split(',');
    //let codepacksToShow = '';
    //$.each(arrayCodePack, function (iRowCode, oCode) {
    //    codepacksToShow = oCode + '\n' + codepacksToShow;
    //});
    //localStorage.codepacksToStore = $codepack;

    $('#selectMarket').selectpicker('val', $selectMarket).selectpicker('refresh').change();
    $("#selectClient").selectpicker('val', $selectClient).selectpicker('refresh').change();
    $("#selectDestination").selectpicker('val', _$selectDestination).selectpicker('refresh').change();
    $("#selectProgram").selectpicker('val', $selectProgram);
    $("#selectVia").selectpicker('val', $selectVia);
    $("#selectBrand").selectpicker('val', $brandID).selectpicker('refresh');
    $("#selectVariety").multiselect('select', $varietyID).multiselect('refresh');
    $("#selectPriority").selectpicker('val', $selectPriority).selectpicker('refresh');
    $("#selectFormat").selectpicker('val', $formatID).selectpicker('refresh');
    $("#selectTypeBox").val($typeBoxID).change()
    $("#selectPresentation").val($presentationID).selectpicker('refresh')
    $("#selectCodepack").multiselect('select', $sizeID).multiselect('refresh');
    $("#selectStatus").selectpicker('val', $selectStatusID).selectpicker('refresh');
    $("#selectLabel").val($labelID);
    $("#txtCodepack").val($codepack);
    $("#txtBoxPerPallet").val($boxPerPallet)
    $("#selectCategory").val($categoryID.trim())
    $(selectIncoterm).selectpicker('val', $incotemID).selectpicker('refresh');
    $(selectPriceConditions).selectpicker('val', $priceCondition).selectpicker('refresh');
    $("#selectKAM").selectpicker('val', $selectKAM).selectpicker('refresh');
    $("#txtPrice").val($price);
    $("#selectConsignee").selectpicker('val', _$selectConsignee).selectpicker('refresh');;
}

function isNormalInteger(str) {
    return /^\+?(0|[1-9]\d*)$/.test(str);
}

function loadSelectVariety() {
    $(selectVariety).empty()
    $(selectFVariety).empty()

    $('#selectFVariety').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    $.each(dataVariety, function (i, item) {
        $(selectVariety).append($('<option>', {
            value: item.id,
            text: item.description
        }));

        $(selectFVariety).append($('<option>', {
            value: item.id,
            text: item.description
        }));
    });

    $('#selectFVariety').selectpicker('refresh');
}

function FillStatus() {
    loadCombo([], 'selectStatus', true);

    data = dataStatus.map(
        obj => {
            return {
                "id": obj.value,
                "name": obj.text
            }
        }
    );
    loadCombo(data, 'selectStatus', true);
    $('#selectStatus').selectpicker('refresh');
}

function FillKam() {

    loadCombo([], 'selectKAM', true);

    data = dataKam.map(
        obj => {
            return {
                "id": obj.kamID,
                "name": obj.description
            }
        }
    );
    loadCombo(data, 'selectKAM', true);
    $('#selectKAM').selectpicker('refresh');
}

function FillVia() {
    loadCombo([], 'selectVia', true);

    data = dataVia.map(
        obj => {
            return {
                "id": obj.viaID,
                "name": obj.description
            }
        }
    );
    loadCombo(data, 'selectVia', true);
    $('#selectVia').selectpicker('refresh');

}

function FillMarket() {

    loadCombo([], 'selectMarket', true);

    data = dataMartet.map(
        obj => {
            return {
                "id": obj.marketID.trim(),
                "name": obj.description
            }
        }
    );

    loadCombo(data, 'selectMarket', true);
    $('#selectMarket').selectpicker('refresh');
}

function FillSelectCategory() {
    $(selectCategory).empty();
    $.each(dataCategory, function (i, item) {
        $(selectCategory).append($('<option>', {
            value: item.categoryID.trim(),
            text: item.description
        }));
    });
}

function FillSelectPriority() {
    loadCombo([], 'selectPriority', true);

    data = dataPriority.map(
        obj => {
            return {
                "id": obj.value,
                "name": obj.text
            }
        }
    );

    loadCombo(data, 'selectPriority', true);
    $('#selectPriority').selectpicker('refresh');
}

function FillSelectCategoryFilter() {
    $(selectFCategory).empty()

    $('#selectFCategory').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    $.each(dataCategory, function (i, item) {


        $('#selectFCategory').append($('<option>', {
            value: item.categoryID.trim(),
            text: item.description
        }));

    });
    $('.selectpicker').selectpicker('refresh');
}

function FillSelectTypeBox() {
    loadCombo([], 'selectTypeBox', true);

    data = dataTypeBox.map(
        obj => {
            return {
                "id": obj.id,
                "name": obj.description
            }
        }
    );
    loadCombo(data, 'selectTypeBox', true);
    $('#selectTypeBox').selectpicker('refresh');
}

function loadSelectLabel() {
    $(selectLabel).empty()
    $.each(dataLabel, function (i, item) {
        $(selectLabel).append($('<option>', {
            value: item.id,
            text: item.description
        }));
    });

}

function changeVia(viaID) {
    if (viaID == "") {
        loadCombo([], 'selectFormat', true);
        loadCombo([], 'selectIncoterm', true);
        $('#selectFormat').change().selectpicker('refresh');
        return;
    }

    dataCodepackSelect = dataCodepack.filter(item => item.viaID == viaID && item.marketID.trim() == $("#selectMarket option:selected").val()).map(
        obj => {
            return {
                "id": obj.codePackID,
                "name": obj.description

            }
        }
    );

    loadCombo(dataCodepackSelect, 'selectFormat', true);
    $('#selectFormat').change().selectpicker('refresh');

    let data3 = dataIncoterm.filter(item => item.viaID == viaID)
    if (data3.length > 0) {
        loadCombo(data3, 'selectIncoterm', true);
    } else {
        loadCombo([], 'selectIncoterm', true);
    }
    $('#selectIncoterm').selectpicker('refresh');
}

function changeMarket(marketID) {
    loadCombo([], 'selectClient', true);
    //loadCombo([], 'selectProgram', true);
    loadCombo([], 'selectDestination', true);
    loadComboConsignnee([], 'selectConsignee', true);
    loadCombo([], 'selectFormat', true);
    loadCombo([], 'selectPresentation', true);

    $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == marketID.trim());

    data = $dataCustomerFilter.map(
        obj => {
            return {
                "id": obj.customerID,
                "name": obj.customer
            }
        }
    );

    loadCombo(data, 'selectClient', true);
    $('#selectClient').selectpicker('refresh');
    $('#selectDestination').selectpicker('refresh');
    $('#selectConsignee').selectpicker('refresh');
    $('#selectFormat').selectpicker('refresh');
    $('#selectPresentation').selectpicker('refresh');
    $(txtCodepack).val('');
}

function calculeCodepack() {
    if ($("#selectVariety option:selected").val() == 0) {
        return false;
    }
    if ($("#selectTypeBox option:selected").val() == 0) {
        return false;
    }
    if ($("#selectBrand option:selected").val() == 0) {
        return false;
    }
    if ($("#selectFormat option:selected").val() == 0) {
        return false;
    }
    if ($("#selectCodepack").val().length == 0) {
        return false;
    }

    let formatID;
    let typeBoxID;
    let brandID;
    let presentationID;
    let labelID;
    let varietyCode;
    let formatCode;
    let typeBoxCode;
    let brandCode;
    let presentationCode;
    let codepackToShow = '';
    let codepackAux = '';
    let varietyID = '';

    let arrayVarietyID = $(selectVariety).val();
    $.each(arrayVarietyID, function (index, value) {
        varietyID += value + ',';
    });
    let arraySizeID = $(selectCodepack).val();

    $.each(arrayVarietyID, function (index, value) {
        varietyID = value;
        formatID = $(selectFormat).val();
        typeBoxID = $(selectTypeBox).val();
        brandID = $(selectBrand).val();
        viaID = $(selectVia).val();
        presentationID = $(selectPresentation).val();
        labelID = $(selectLabel).val();

        varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
        formatCode = dataCodepack.filter(item => item.codePackID == formatID && item.viaID == viaID)[0].weight;
        typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation;
        brandCode = dataCategory.filter(item => item.categoryID == brandID)[0].description;
        presentationCode = dataPresentation.filter(item => item.presentationID == presentationID && item.codePackID == formatID)[0].presentation;

        codepackAux = varietyCode.trim() + '_' +
            formatCode +
            typeBoxCode.slice(0, 1) +
            brandCode.slice(0, 1) + '_' +
            presentationCode;

        codepackToShow = (codepackAux.trim() == '') ? codepackToShow : codepackAux + ',' + codepackToShow;

    });
    codepackToShow = codepackToShow.substring(0, codepackToShow.length - 1);
    let arrayCodePack = codepackToShow.split(',');
    let codepacksToStore = '';
    codepackToShow = '';
    $.each(arraySizeID, function (iRowSize, oSize) {
        $.each(arrayCodePack, function (iRowCode, oCode) {
            codepackToShow = (oCode + '_+' + oSize.substring(1, oSize.length) + '_' + 1) + '\n' + codepackToShow;
            codepacksToStore = (oCode + '_+' + oSize.substring(1, oSize.length) + '_' + 1) + ',' + codepacksToStore;
        });
    });
    localStorage.codepacksToStore = codepacksToStore;
    $(txtCodepack).val(codepackToShow);

}

function DrawCommercialPlanStructureByCrop() {
    let bRsl = false;

    $.ajax({
        async: false,
        type: 'GET',
        url: "/CommercialPlan/GetVarCatByCrop?cropID=" + localStorage.cropID,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            }
            else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    dataVarCatByCrop = dataJson.map(
                        obj => {
                            return {
                                varietyID: obj.varietyID,
                                variety: obj.variety,
                                categoryID: obj.categoryID,
                                category: obj.category
                            }
                        }
                    );
                    bRsl = true;
                }
            }
        }
    });

    var conntentPromises = ''
    var id = 'promises';

    for (var i = 0; i < dataMartet.length; i++) {
        var promisesId = id + '-' + dataMartet[i].marketID;

        var tableCliente = '';
        tableCliente += '<div class="card">'
        tableCliente += '<div class="card-header">'
        tableCliente += '<button class="btn btn-info btn-sm btn-sm btn-block" style=" background-color: #435E85;" data-toggle="collapse" data-target="#Plan-VAR-' + dataMartet[i].marketID + '" aria-expanded="true"> ' + dataMartet[i].description + ' <span class="collapse-in"><i class="fas fa-arrow-down "></i></span> </button>'
        tableCliente += '</div>'
        tableCliente += '<div id="Plan-VAR-' + dataMartet[i].marketID + '" class="collapse show" style="">'
        tableCliente += '<div class="card-body p-3">'
        //>>>Begin table
        tableCliente += '<table style="width:100%; font-size:12px;" class="table table-bordered table-hover tblPlan table-cebra table-responsive " id="tblPlan-VR-' + dataMartet[i].marketID + '">'
        //>>>Begin thead
        tableCliente += '<thead class="text-white" style="background-color: #091440; text-align:center;vertical-align:middle; font-size:12px;">'
        //Drawing 1st row per variety
        tableCliente += '<tr>'
        tableCliente += '<th colspan="11" style=" text-align:right; background-color:#1d325c;min-width:1070px;" class="sticky">Harvest Week</th>'
        $.each(dataWeek, function (iWeek, week) {
            tableCliente += '<th class="thWeekHavest" style="background-color:#1d325c">Week ' + (parseInt(week.number) - parseInt(0)) + '</th>'

        })
        tableCliente += '<th>Harvest Week</th>'
        tableCliente += '</tr>'
        //Drawing 2nd row per variety
        tableCliente += '<tr>'
        tableCliente += '<th style="min-width:120px;max-width:120px; background-color:#27437b" class="sticky">[]</th>'
        tableCliente += '<th style="display:none">tdPlanCustomerVarietyID</th>'
        tableCliente += '<th style="min-width:50px;max-width:50px; background-color:#27437b" class="sticky2">Brand</th>'
        tableCliente += '<th style="min-width:130px;max-width:130px; background-color:#27437b" class="sticky3">Variety</th>'
        tableCliente += '<th style="min-width:80px;max-width:80px; background-color:#27437b" class="sticky4">Size</th>'
        tableCliente += '<th style="min-width:100px;max-width:100px; background-color:#27437b" class="sticky5">Format</th>'
        tableCliente += '<th style="min-width:200px;max-width:200px; background-color:#27437b" class="sticky6">Customer</th>'
        tableCliente += '<th style="min-width:120px ;max-width:120px; background-color:#27437b" class="sticky7">Program</th>'
        tableCliente += '<th style="min-width:80px ;max-width:80px; background-color:#27437b" class="sticky8">Destination</th>'
        tableCliente += '<th style="min-width:30px;max-width:30px; background-color:#27437b" class="sticky9">Via</th>'
        tableCliente += '<th style="min-width:80px;max-width:80px; background-color:#27437b" class="sticky10">Priority</th>'
        tableCliente += '<th style="min-width:80px;max-width:80px; background-color:#27437b" class="sticky11">Status</th>'
        tableCliente += '<th style="display:none;">Codepack</th>'
        tableCliente += '<th style="display:none">marketID</th>'
        tableCliente += '<th style="display:none">customerID</th>'
        tableCliente += '<th style="display:none">programID</th>'
        tableCliente += '<th style="display:none">destinationID</th>'
        tableCliente += '<th style="display:none">consigneeID</th>'
        tableCliente += '<th style="display:none">ViaID</th>'
        tableCliente += '<th style="display:none">brandID</th>'
        tableCliente += '<th style="display:none">varietyID</th>'
        tableCliente += '<th style="display:none">priorityID</th>'
        tableCliente += '<th style="display:none">formatID</th>'
        tableCliente += '<th style="display:none">typeBoxID</th>'
        tableCliente += '<th style="display:none">presentationID</th>'
        tableCliente += '<th style="display:none">codepackID</th>'
        tableCliente += '<th style="display:none">statusID</th>'
        tableCliente += '<th style="display:none">labelID</th>' //solo texto es el label
        tableCliente += '<th style="display:none">boxPerPallet</th>'
        tableCliente += '<th style="display:none">boxPerContainer</th>'
        tableCliente += '<th style="display:none">IncotermID</th>'
        tableCliente += '<th style="display:none">PaymentTerms</th>'
        tableCliente += '<th style="display:none">PriceConditions</th>'
        tableCliente += '<th style="display:none">KamID</th>'
        tableCliente += '<th style="display:none">categoryID</th>'
        tableCliente += '<th style="display:none">tdsizeID</th>'
        tableCliente += '<th style="display:none">tdprice</th>'
        $.each(dataWeek, function (iWeek, week) {
            tableCliente += '<th class="thWeek" style="background-color:#27437b"> Week ' + (parseInt(week.number) + parseInt(1)) + '</th>'
        })
        tableCliente += '<th class="thWeek" style="background-color:#27437b;min-width:100px"> Total</th>'
        tableCliente += '</tr>'

        //Drawing 4th row per variety
        tableCliente += '<tr class="text-align-right trForecast" style="background-color:#223890">'
        tableCliente += '<th style="min-width:120px;max-width:120px"  class="sticky"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="min-width:50px;max-width:50px" class="sticky2"></th>'
        tableCliente += '<th style="min-width:130px;max-width:130px" class="sticky3"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky4"></th>'
        tableCliente += '<th style="min-width:100px;max-width:100px" class="sticky5"></th>'
        tableCliente += '<th style="min-width:200px;max-width:200px" class="sticky6"></th>'
        tableCliente += '<th style="min-width:120px;max-width:120px" class="sticky7"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky8"></th>'
        tableCliente += '<th style="min-width:30px;max-width:30px; text-align:center" class="tdBoxWeight sticky9"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky10"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky11">OzBlu(KG)</th>'
        tableCliente += '<th style="display:none;"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        $.each(dataWeek, function (iWeek, week) {
            tableCliente += '<th class="thForecastWeekOZB">0</th>'
        })
        tableCliente += '<th class="thTotalForecastWeekOZB" style="min-width:100px;max-width:100px">0</th>'
        tableCliente += '</tr>'

        //Drawing 3th row per variety
        tableCliente += '<tr class="text-align-right trForecast" style="background-color:#223890">'
        tableCliente += '<th style="min-width:120px;max-width:120px" class="sticky"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="min-width:50px;max-width:50px" class="sticky2"></th>'
        tableCliente += '<th style="min-width:130px;max-width:130px" class="sticky3"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky4"></th>'
        tableCliente += '<th style="min-width:100px;max-width:100px" class="sticky5"></th>'
        tableCliente += '<th style="min-width:200px;max-width:200px" class="sticky6"></th>'
        tableCliente += '<th style="min-width:120px;max-width:120px" class="sticky7"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky8"></th>'
        tableCliente += '<th style="min-width:30px;max-width:30px; text-align:center" class="tdBoxWeight sticky9"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky10"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky11">BigBold (KG)</th>'
        tableCliente += '<th style="display:none; "></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        $.each(dataWeek, function (iWeek, week) {
            tableCliente += '<th class="thForecastWeekBIB">0</th>'
        })
        tableCliente += '<th class="thTotalForecastWeekBIB" style="min-width:100px;max-width:100px">0</th>'
        tableCliente += '</tr>'

        //Drawing 5th row per variety
        tableCliente += '<tr class="text-align-right trForecast" style="background-color:#223890">'
        tableCliente += '<th style="min-width:120px;max-width:120px" class="sticky"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="min-width:50px;max-width:50px" class="sticky2"></th>'
        tableCliente += '<th style="min-width:130px;max-width:130px" class="sticky3"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky4"></th>'
        tableCliente += '<th style="min-width:100px;max-width:100px" class="sticky5"></th>'
        tableCliente += '<th style="min-width:200px;max-width:200px" class="sticky6"></th>'
        tableCliente += '<th style="min-width:120px;max-width:120px" class="sticky7"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky8"></th>'
        tableCliente += '<th style="min-width:30px;max-width:30px; text-align:center" class="tdBoxWeight sticky9"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky10"></th>'
        tableCliente += '<th style="min-width:80px;max-width:80px" class="sticky11">Total (KG)</th>'
        tableCliente += '<th style="display:none;"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        tableCliente += '<th style="display:none"></th>'
        $.each(dataWeek, function (iWeek, week) {
            tableCliente += '<th class="thForecastWeek">0</th>'
        })
        tableCliente += '<th class="thTotalForecastWeek" style="min-width:100px;max-width:100px">0</th>'
        tableCliente += '</tr>'
        tableCliente += '</thead>'
        //<<<End thead
        tableCliente += '<tbody id="tbodyCat-1" class="" >'
        tableCliente += '</tbody>'

        tableCliente += '<tfoot>'
        var datafooter = dataCodepack.filter(item => item.marketID.trim() == dataMartet[i].marketID && item.viaID == 1).sort((a, b) => (a.weight > b.weight) ? 1 : ((b.weight > a.weight) ? -1 : 0));

        $.each(datafooter, function (ifooter, objfooter) {
            tableCliente += '<tr style="background-color:#65939b;text-align:right" class="trProjectedSales' + objfooter.codePackID + '">'
            tableCliente += '<td style="background-color:#65939b;min-width:1070px;" colspan="11" class="sticky">Projected Sales Allocation ( ' + objfooter.description + ' )</td>'
            $.each(dataWeek, function (iWeek, week) {
                tableCliente += '<td class="tdProjectedSales' + objfooter.codePackID + '">' + '0' + '</td>'
            })
            tableCliente += '<td class="tdTotalProjectedSales' + objfooter.codePackID + '">0</td>'
            tableCliente += '</tr>'
        })

        $.each(dataCategory, function (ibrand, objbrand) {
            tableCliente += '<tr style="background-color:#4e73df;text-align:right" class="trBalance' + objbrand.categoryID + '">'
            tableCliente += '<td style="background-color:#4e73df;min-width:1070px;" colspan="11" class="sticky">Balance (' + objbrand.description + ' KG)</td>'
            $.each(dataWeek, function (iWeek, week) {
                tableCliente += '<td class="tdBalance' + objbrand.categoryID + '">' + '0' + '</td>'
            })
            tableCliente += '<td class="tdTotalBalance' + objbrand.categoryID + '">0</td>'

            tableCliente += '</tr>'
        });

        tableCliente += '<tr style="background-color:#4e73df;text-align:right" class="trBalance">'
        tableCliente += '<td style="background-color:#4e73df;min-width:1070px;" colspan="11" class="sticky">Total Balance (KG)</td>'
        $.each(dataWeek, function (iWeek, week) {
            tableCliente += '<td class="tdBalance">' + '0' + '</td>'
        })
        tableCliente += '<td class="tdTotalBalance">0</td>'

        tableCliente += '</tr>'
        tableCliente += '</tfoot>'
        tableCliente += '</table>'
        //<<<End table
        tableCliente += '</div>'
        tableCliente += '</div></div>'
        conntentPromises += tableCliente;

    }

    $("#divPromises").append(conntentPromises);

}

function addClientCodepack() {
    if ($(txtCodepack).val() == '') {
        bError = true;
        sweet_alert_warning('error', 'choose codepack');
        return;
    }
    $selectMarket = $("#selectMarket option:selected");
    $selectClient = $("#selectClient option:selected");
    $selectProgram = $("#selectProgram option:selected");
    $selectDestination = $("#selectDestination option:selected");
    $selectConsignee = $("#selectConsignee option:selected");
    $selectVia = $("#selectVia option:selected");
    $brandID = $("#selectBrand option:selected");
    $varietyID = $("#selectVariety");
    $selectPriority = $("#selectPriority option:selected");
    $formatID = $("#selectFormat option:selected");
    $typeBoxID = $("#selectTypeBox option:selected");
    $presentationID = $("#selectPresentation option:selected");
    $sizeID = $("#selectCodepack");	//Size seleccionado
    $selectStatusID = $("#selectStatus option:selected");
    $labelID = $brandID;
    $selectIncoterm = $("#selectIncoterm option:selected");
    $selectPaymentTerms = $("#selectPaymentTerms option:selected");
    $selectPriceConditions = $("#selectPriceConditions option:selected");
    $selectKAM = $("#selectKAM option:selected");
    $categoryID = $("#selectCategory option:selected");     

    var table = ''
    if ($($selectStatusID).val() == "1") {
        table = '<tr style="text-align:center ">'
    } else {
        table = '<tr style="text-align:center" class="table-secondary">'
    }
    let strVariety = GetTextMultiselect('selectVariety', 1);
    let strSize = GetTextMultiselect('selectCodepack', 1);

    table += '<td style="background-color:#F9E79F;min-width:120px" class="tdBtn sticky" ><button style="font-size:14px" class="btnRowDelete btn btn-sm btn-danger"><i class="fas fa-trash"></i></button><button class="btnRowEdit btn btn-sm btn-primary" style="font-size:14px"><i class="fas fa-edit"></i></button><button class="btnRowSave btn btn-sm btn-success" style="font-size:14px"><i class="fas fa-save"></i></button></td>'
    table += '<td style="display:none" class="tdPlanCustomerVarietyID">0</td>'
    table += '<td style="min-width:50px;max-width:50px" class="tdBrand sticky2">' + $($brandID).text() + '</td>'
    table += '<td style="min-width:130px;max-width:130px"; class="tdVariety sticky3"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + strVariety + '</textarea></td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdSize sticky4">' + strSize + '</td>'
    table += '<td style="min-width:100px;max-width:100px" class="tdFormat sticky5">' + $($formatID).text() + '</td>'
    table += '<td style="min-width:200px;max-width:200px" class="tdClient sticky6"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + $($selectClient).text() + '</textarea></td>'
    table += '<td style="min-width:120px;max-width:120px" class="tdProgram sticky7"><textarea style="font-size:11px" class="form-control form-control-sm" rows="1">' + $($selectProgram).text() + '</textarea></td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdDestination sticky8">' + $($selectDestination).text() + '</td>'
    table += '<td style="min-width:30px;max-width:30px" class="tdVia sticky9">' + $($selectVia).text() + '</td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdPiority sticky10">' + $($selectPriority).text() + '</td>'
    table += '<td style="min-width:80px;max-width:80px" class="tdStatus sticky11">' + $($selectStatusID).text() + '</td>'
    table += '<td style="display:none; min-width:120px" class="tdCodepack">' + localStorage.codepacksToStore + '</td>'
    table += '<td style="display:none" class="tdMarketID">' + $($selectMarket).val() + '</td>'
    table += '<td style="display:none" class="tdClientID">' + $($selectClient).val() + '</td>'
    table += '<td style="display:none" class="tdProgramID">' + $($selectProgram).val() + '</td>'
    table += '<td style="display:none" class="tdDestinationID">' + $($selectDestination).val() + '</td>'
    table += '<td style="display:none" class="tdConsigneeID">' + $($selectConsignee).val() + '</td>'
    table += '<td style="display:none" class="tdViaID">' + $($selectVia).val() + '</td>'
    table += '<td style="display:none" class="tdBrandID">' + $($brandID).val() + '</td>'
    table += '<td style="display:none" class="tdVarietyID">' + $($varietyID).val() + '</td>'
    table += '<td style="display:none" class="tdCategoryID">' + $($brandID).val() + '</td>'
    table += '<td style="display:none" class="tdPiorityID">' + $($selectPriority).val() + '</td>'
    table += '<td style="display:none" class="tdFormatID">' + $($formatID).val() + '</td>'
    table += '<td style="display:none" class="tdCodepackID">' + $($formatID).val() + '</td>'
    table += '<td style="display:none" class="tdTypeBoxID">' + $($typeBoxID).val() + '</td>'
    table += '<td style="display:none" class="tdPresentationID">' + $($presentationID).val() + '</td>'
    table += '<td style="display:none" class="tdsizeID">' + $($sizeID).val() + '</td>'	//Size seleccionado
    table += '<td style="display:none" class="tdstatusConfirmID">' + $($selectStatusID).val() + '</td>'
    table += '<td style="display:none" class="tdLabelID">' + $($labelID).val() + '</td>'
    table += '<td style="display:none" class="tdBoxPerPallet">' + $(txtBoxPerPallet).val() + '</td>'
    table += '<td style="display:none" class="tdBoxPerContainer">' + $(txtBoxPerPerContainer).val() + '</td>'
    table += '<td style="display:none" class="tdIncotermID">' + $($selectIncoterm).val() + '</td>'
    table += '<td style="display:none" class="tdPaymentTerms">' + $($selectPaymentTerms).val() + '</td>'
    table += '<td style="display:none" class="tdPriceConditions">' + $($selectPriceConditions).val() + '</td>'
    table += '<td style="display:none" class="tdKAMID">' + $($selectKAM).val() + '</td>'
    table += '<td style="display:none" class=""></td>'
    table += '<td style="display:none" class=""></td>'
    table += '<td style="display:none" class="tdprice">' + $(txtPrice).val() + '</td>'

    $.each(dataWeek, function (iWeek, week) {
        table += '<td style="text-align:right;" class="tdSalesProjected" contenteditable="true" >';
        table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/>';
        table += '</td>';
    })
    table += '<td class="tdTotalSalesProjected" contenteditable="true">'
    table += '<input type="text" class="txtBoxNumber form-control form-control-sm" value="0" style="text-align:right;max-width:100px;background-color:transparent;"/></td>';
    table += '</tr>'
    var id = 'tblPlan-VR-' + $selectMarket.val()
    $("#" + id + ">tbody").append(table);
    bError = false;
}

function clearDetails() {
    $(selectMarket).selectpicker('val', '');
    $(selectClient).selectpicker('val', '');
    $(selectProgram).selectpicker('val', '');
    $(selectDestination).selectpicker('val', '');
    $(selectConsignee).selectpicker('val', '');
    $(selectVia).selectpicker('val', '');
    $(selectBrand).selectpicker('val', '');
    $(selectPriority).selectpicker('val', '');
    $(selectFormat).selectpicker('val', '');
    $(selectTypeBox).selectpicker('val', '');
    $(selectPresentation).selectedIndex = 0;
    $(selectStatus).selectpicker('val', '');
    $(selectIncoterm).selectpicker('val', '');
    $(selectPaymentTerms).selectpicker('val', '');
    $(selectPriceConditions).selectpicker('val', '0');
    $(selectKAM).selectpicker('val', '');
    $(selectLabel).val('');
    $(txtCodepack).val('');
    $(txtCreationDate).val('');
    $(txtBoxPerPallet).val(0);
    $(txtBoxPerPerContainer).val(0);
    $('#txtPrice').val('0');
    $('#selectVariety').multiselect('deselectAll', false);
    $('#selectVariety').multiselect('refresh');
    $('#selectCodepack').multiselect('deselectAll', false);
    $('#selectCodepack').multiselect('refresh');    
    trEdit = null;
    bError = true;
}

function SettingCustomizedMultiselect(obj) {
    var orderCount = 0;
    $(obj).multiselect({
        buttonWidth: '180px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select expertise!',
        maxHeight: '1000px',
        dropUp: true,
        enableFiltering: false,
        onChange: function (option, checked) {
            if (checked) {
                orderCount++;
                $(option).data('order', orderCount);
            }
            else {
                $(option).data('order', '');
            }
        },
        buttonText: function (options) {
            if (options.length === 0) {
                return 'None selected';
            }
            else if (options.length > 3) {
                return options.length + ' selected';
            }
            else {
                var selected = [];
                options.each(function () {
                    selected.push([$(this).text(), $(this).data('order')]);
                });

                selected.sort(function (a, b) {
                    return a[1] - b[1];
                });

                var text = '';
                for (var i = 0; i < selected.length; i++) {
                    text += selected[i][0] + ', ';
                }

                return text.substr(0, text.length - 2);
            }
        },
    });
}

function SettingCustomizedMultiselectS(obj) {
    var orderCount = 0;
    $(obj).multiselect({
        buttonWidth: '150px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select expertise!',
        maxHeight: '1000px',
        dropUp: true,
        enableFiltering: false,
        onChange: function (option, checked) {
            if (checked) {
                orderCount++;
                $(option).data('order', orderCount);
            }
            else {
                $(option).data('order', '');
            }
        },
        buttonText: function (options) {
            if (options.length === 0) {
                return 'None selected';
            }
            else if (options.length > 3) {
                return options.length + ' selected';
            }
            else {
                var selected = [];
                options.each(function () {
                    selected.push([$(this).text(), $(this).data('order')]);
                });

                selected.sort(function (a, b) {
                    return a[1] - b[1];
                });

                var text = '';
                for (var i = 0; i < selected.length; i++) {
                    text += selected[i][0] + ', ';
                }

                return text.substr(0, text.length - 2);
            }
        },
    });
}

function SettingCustomizedMultiselectST(obj) {
    var orderCount = 0;
    $(obj).multiselect({
        buttonWidth: '100px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select expertise!',
        maxHeight: '1000px',
        dropUp: true,
        enableFiltering: false,
        onChange: function (option, checked) {
            if (checked) {
                orderCount++;
                $(option).data('order', orderCount);
            }
            else {
                $(option).data('order', '');
            }
        },
        buttonText: function (options) {
            if (options.length === 0) {
                return 'None selected';
            }
            else if (options.length > 3) {
                return options.length + ' selected';
            }
            else {
                var selected = [];
                options.each(function () {
                    selected.push([$(this).text(), $(this).data('order')]);
                });

                selected.sort(function (a, b) {
                    return a[1] - b[1];
                });

                var text = '';
                for (var i = 0; i < selected.length; i++) {
                    text += selected[i][0] + ', ';
                }

                return text.substr(0, text.length - 2);
            }
        },
    });
}

function ShowCalculateTotalBoxexByWeekForEachVariety() {
    let dataCatByVar = [];

    for (var i = 0; i < dataMartet.length; i++) {
        var trCat = $('#tblPlan-VR-' + dataMartet[i].marketID + '>thead>tr.trForecast')
        var totalHarvest = 0;
        var totalHarvestBO = 0;

        $.each(dataWeek, function (iWeek, week) {
            var mount = dataForecast.filter(item => item.projectedWeekID == week.projectedWeekID + 0)
            if (mount.length >= 1) {
                //recorrer el mount y totalizarlo
                $(trCat).find('.thForecastWeek:eq(' + iWeek + ')').html(mount[0].BOXES);
                totalHarvest = parseFloat((mount[0].BOXES).replace(',', '')) + totalHarvest;
            }
        });

        $.each(dataCategory, function (iBrand, objBrand) {
            totalHarvestBO = 0;
            $.each(dataWeek, function (iWeek, week) {
                var mount = dataForecastByBrand.filter(item => item.projectedWeekID == week.projectedWeekID + 0 && item.brandID == objBrand.categoryID)
                if (mount.length >= 1) {
                    //recorrer el mount y totalizarlo
                    $(trCat).find('.thForecastWeek' + objBrand.categoryID + ':eq(' + iWeek + ')').html(mount[0].BOXES);
                    totalHarvestBO = parseFloat((mount[0].BOXES).replace(',', '')) + totalHarvestBO;
                }
            });
            $(trCat).find('.thTotalForecastWeek' + objBrand.categoryID).html(Number(totalHarvestBO).toLocaleString('en'));
        });

        $(trCat).find('.thTotalForecastWeek').html(Number(totalHarvest).toLocaleString('en'));
    }

}

function calculateProjectedSales(table) {
    var str_market = table[0].id;

    if (str_market !== undefined) {
        str_market = str_market.slice(-2);
    }
    else {
        str_market = table.slice(-2);
    }

    var dataformat = dataCodepack.filter(item => item.marketID.trim() == str_market && item.viaID == 1);

    var trForecast = $(table).find('thead>tr.trForecast')
    var tbody = $(table).find('tbody')

    //bucle por el brand
    $.each(dataCategory, function (ibrand, objbrand) {
        //bucle para por el formato
        var mountTotalBrand = 0;
        $.each(dataformat, function (iformat, objformat) {

            var trProjectedSales = $(table).find('tfoot>tr.trProjectedSales' + objformat.codePackID)
            var trBalance = $(table).find('tfoot>tr.trBalance' + objbrand.categoryID)
            var balance = 0
            var totalForecast = 0

            $.each(dataWeek, function (iWeek, week) {
                var mountForecast = $(trForecast).find('th.thForecastWeek' + objbrand.categoryID + ':eq(' + iWeek + ')').text()
                totalForecast = totalForecast + parseInt(mountForecast.replace(',', ''));
                var mountTotalSales = 0;
                mountTotalBrand = 0;
                $(tbody).find('tr').each(function (iTr, tr) {
                    var formatKg = $(tr).find('td.tdFormat').text()
                    var tdBrandID = $(tr).find('td.tdBrandID').text()
                    if (formatKg == objformat.description) {
                        mountTotalSales = (parseFloat($(tr).find('td.tdSalesProjected:eq(' + iWeek + ')').find('input.txtBoxNumber').val().replace(',', ''))) + mountTotalSales;
                    }

                    if (formatKg == objformat.description && tdBrandID == objbrand.categoryID) {
                        mountTotalBrand = (parseFloat($(tr).find('td.tdSalesProjected:eq(' + iWeek + ')').find('input.txtBoxNumber').val().replace(',', '')) * parseFloat(objformat.weight)) + mountTotalBrand;
                    }

                })

                $(trProjectedSales).find('td.tdProjectedSales' + objformat.codePackID + ':eq(' + iWeek + ')').text(Number((mountTotalSales).toFixed(0)).toLocaleString('en'))

            })

            $(tbody).find('tr').each(function (iTr, tr) {
                var boxesByRow = 0
                $(tr).find('td.tdSalesProjected').each(function (iTd, td) {
                    boxesByRow = parseInt(($(td).find('input.txtBoxNumber').val()).replace(',', '')) + (boxesByRow);

                })

                $(tr).find('td.tdTotalSalesProjected').text(Number(boxesByRow).toLocaleString('en'));
            })

            var totalBoxSales = 0
            $(trProjectedSales).find('td.tdProjectedSales' + objformat.codePackID).each(function (iTd, td) {
                totalBoxSales = parseInt($(td).text().replace(',', '')) + totalBoxSales
            })
            $(trProjectedSales).find('td.tdTotalProjectedSales' + objformat.codePackID).text(Number(totalBoxSales).toLocaleString('en'));

        });

    });

    //totalizamos los balances 
    $.each(dataCategory, function (ibrand, objbrand) {
        //bucle para por el formato
        var mountTotalBrand = 0;

        var trBalance = $(table).find('tfoot>tr.trBalance' + objbrand.categoryID)
        var balance = 0
        var totalForecast = 0

        $.each(dataWeek, function (iWeek, week) {
            var mountForecast = $(trForecast).find('th.thForecastWeek' + objbrand.categoryID + ':eq(' + iWeek + ')').text()
            totalForecast = totalForecast + parseInt(mountForecast.replace(',', ''));
            mountTotalBrand = 0;

            $(tbody).find('tr').each(function (iTr, tr) {
                var tdBrandID = $(tr).find('td.tdBrandID').text()

                var objformat = dataCodepack.filter(item => item.marketID.trim() == str_market && item.viaID == $(tr).find('td.tdViaID').text() && item.codePackID == $(tr).find('td.tdCodepackID').text());

                if (tdBrandID == objbrand.categoryID) {
                    mountTotalBrand = (parseFloat($(tr).find('td.tdSalesProjected:eq(' + iWeek + ')').find('input.txtBoxNumber').val().replace(',', '')) * parseFloat(objformat[0].weight)) + mountTotalBrand;
                }

            })

            balance = parseInt(mountForecast.replace(',', '')) - (mountTotalBrand).toFixed(0) + balance;
            if (balance < 0) {
                $(trBalance).find('td.tdBalance' + objbrand.categoryID + ':eq(' + iWeek + ')').text(Number(balance).toLocaleString('en')).css('background-color', 'red').css('color', 'white');
            } else {
                $(trBalance).find('td.tdBalance' + objbrand.categoryID + ':eq(' + iWeek + ')').text(Number(balance).toLocaleString('en')).removeAttr('style');
            }
        })
        $(trBalance).find('td.tdTotalBalance' + objbrand.categoryID).text(Number(balance).toLocaleString('en'))

    });

    //totalizamos los balances 
    var trBalance = $(table).find('tfoot>tr.trBalance');
    var trBalanceBIB = $(table).find('tfoot>tr.trBalanceBIB');
    var trBalanceOZB = $(table).find('tfoot>tr.trBalanceOZB');
    var balance = 0;
    var totalBalance = 0;

    $.each(dataWeek, function (iWeek, week) {
        var balanceOZB = $(trBalanceOZB).find('td.tdBalanceOZB:eq(' + iWeek + ')').text().replace(',', '');
        var balanceBIB = $(trBalanceBIB).find('td.tdBalanceBIB:eq(' + iWeek + ')').text().replace(',', '');

        balance = (parseInt(balanceOZB.replace(',', '')) + parseInt(balanceBIB.replace(',', '')));
        if (balance < 0) {
            $(trBalance).find('td.tdBalance:eq(' + iWeek + ')').text(Number(balance).toLocaleString('en')).css('background-color', 'red').css('color', 'white');
        } else {
            $(trBalance).find('td.tdBalance:eq(' + iWeek + ')').text(Number(balance).toLocaleString('en')).removeAttr('style');
        }
    })

    var balanceTotalOZB = $(trBalanceOZB).find('td.tdTotalBalanceOZB').text().replace(',', '');
    var balanceTotalBIB = $(trBalanceBIB).find('td.tdTotalBalanceBIB').text().replace(',', '');
    totalBalance = (parseInt(balanceTotalOZB.replace(',', '')) + parseInt(balanceTotalBIB.replace(',', '')));
    $(trBalance).find('td.tdTotalBalance').text(Number(totalBalance).toLocaleString('en'))

}

function loadData() {
    FillSelectCategory();
    FillSelectPriority();
    FillMarket();
    FillVia();
    FillKam();
    FillSelectCategoryFilter();
    FillStatus();
    FillSelectTypeBox();
    GetFillSizes();
    GetFillCustomer();
    GetFillCustomerTray();
    GetFillProgramTray();
    GetFillViaTray();
    GetFillSelectBrand();
    GetFillSelectVariety();
    GetFillSelectPriceCondition();
    sweet_alert_progressbar_cerrar();
}

function GetFillSizes() {
    $(selectCodepack).empty();

    $.each(dataSizes, function (i, item) {
        $(selectCodepack).append($('<option>', {
            value: item.id,
            text: item.name
        }));
    });
    SettingCustomizedMultiselectS($('#selectCodepack'));

}

function GetFillCustomer() {
    loadCombo([], 'selectProgram', true);

    var opt = 'all';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/CustomerByCrop?opt=" + opt + "&id=" + localStorage.cropID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (dataJson.length > 0) {

                data = dataJson.map(
                    obj => {
                        return {
                            "id": obj.customerID,
                            "name": obj.name
                        }
                    }
                );

            }
            loadCombo(data, 'selectProgram', true);
            $('#selectProgram').selectpicker('refresh');
        }
    });
}

function GetFillCustomerTray() {
    $('#cboFCustomer').empty()

    $('#cboFCustomer').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    var opt = 'cuspla';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/CustomerByCrop?opt=" + opt + "&id=" + localStorage.campaignID,
        async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (dataJson.length > 0) {
                $.each(dataJson, function (i, item) {

                    $('#cboFCustomer').append($('<option>', {
                        value: item.customerID,
                        text: item.name
                    }));

                })
            }
            $('#cboFCustomer').selectpicker('refresh').change();
        }
    });
}

function GetFillProgramTray() {
    $(selectFProgram).empty()

    $('#selectFProgram').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    var opt = 'propla';
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/CustomerByCrop?opt=" + opt + "&id=" + localStorage.campaignID,
        async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (dataJson.length > 0) {
                $.each(dataJson, function (i, item) {

                    $('#selectFProgram').append($('<option>', {
                        value: item.customerID,
                        text: item.name
                    }));
                })
            }
            $('#selectFProgram').selectpicker('refresh');
        }
    });
}

function loadFDestination(customerID) {
    $("#selectFDestination").empty();
    if (customerID == null) customerID = '';
    $(selectFDestination).empty();
    
    var opt = 'cus';
    var id = customerID;
    var mar = '';
    var via = '';    

    $('#selectFDestination').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/DestinationByCustomerID?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
        //async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);

            if (dataJson.length > 0) {
                $.each(dataJson, function (i, item) {

                    $("#selectFDestination").append($('<option>', {
                        value: item.destinationID,
                        text: item.destination
                    }));
                })
            }
        }
    });
}

function GetFillViaTray() {
    $('#selecFtVia').empty()

    $('#selecFtVia').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    var opt = 'all';
    $.ajax({
        type: "GET",
        url: "/Sales/ListVia?opt=" + opt + "&id=" + '',
        async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (dataJson.length > 0) {
                $.each(dataJson, function (i, item) {

                    $('#selecFtVia').append($('<option>', {
                        value: item.viaID,
                        text: item.name
                    }));

                })
            }
            $('#selecFtVia').selectpicker('refresh').change();
        }
    });
}

function LoadFFormat(viaID) {
    $("#selectFFormat").empty();

    $('#selectFFormat').append($('<option>', {
        value: "-1",
        text: "ALL"
    }));

    var opt = 'vic';
    var viaid = viaID;
    var cropID = localStorage.cropID;   

    $.ajax({
        type: "GET",
        url: "/Sales/CodepackByFormatIDPackageProductIDMC?opt=" + opt + "&viaid=" + viaid + "&cropID=" + cropID,
        async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);

            if (dataJson.length > 0) {
                $.each(dataJson, function (i, item) {

                    $("#selectFFormat").append($('<option>', {
                        value: item.codePackID,
                        text: item.description
                    }));
                })
            }
            $('#selectFFormat').selectpicker('refresh');
        }
    });
}

function loadLabel(brandtext) {
    if (brandtext == '') {
        return;
    }
    var dataFilter = dataCategory.filter(item => item.categoryID == brandtext)[0];
    $(selectLabel).val(dataFilter.description);

}

function loadDestination(customerID) {
    loadCombo([], 'selectDestination', true);
    var opt = 'cus';
    var id = customerID;
    var mar = '';
    var via = '';

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/DestinationByCustomerID?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            var dataJson = JSON.parse(data);

            if (dataJson.length > 0) {

                data = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationID,
                            "name": obj.destination
                        }
                    }
                );
                loadCombo(data, 'selectDestination', true);
                if (trEdit != null) {
                    $('#selectDestination').selectpicker('val', _$selectDestination);
                }
                $('#selectDestination').selectpicker('refresh');

            }

        }
    });

}

function GetFillSelectVariety() {
    $(selectVariety).empty();

    dataVariety = [];
    var opt = 'cro';
    var log = '';
    var sUrlApi = "/CommercialPlan/VarietyByCrop?opt=" + opt + "&id=" + localStorage.cropID + "&log=" + log
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    $.each(dataJson, function (i, item) {
                        $(selectVariety).append($('<option>', {
                            value: item.varietyID,
                            text: item.name
                        }));
                    });

                    SettingCustomizedMultiselect($('#selectVariety'));

                    dataVariety = dataJson.map(
                        obj => {
                            return {
                                abbreviation: obj.abbreviation,
                                id: obj.varietyID,
                                description: obj.name
                            }
                        }
                    );
                    iTotVarietyXCategory = (dataVariety.length) * 2 + 1;
                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {

                DrawCommercialPlanStructureByCrop();
                ShowCalculateTotalBoxexByWeekForEachVariety();
                GetCommercialPlanByCampaign();

            }
        },
    });

}

function GetFillSelectBrand() {
    loadCombo([], 'selectBrand', true);

    data = dataCategory.map(
        obj => {
            return {
                "id": obj.categoryID,
                "name": obj.description
            }
        }
    );

    loadCombo(data, 'selectBrand', true);
    $('#selectBrand').selectpicker('refresh').change();
}

function loadInternalPackage(formatID) {
    loadCombo([], 'selectPresentation', true);
    let data = dataPresentation.filter(item => item.codePackID == formatID).map(
        obj => {
            return {
                "id": obj.presentationID,
                "name": obj.presentation + obj.moreInfo
            }
        });
    loadCombo(data, 'selectPresentation', false);
    $('#selectPresentation').selectpicker('refresh').change();
}

function loadSizeBox() {

    if ($(selectFormat).val() > 0) {
        var boxPallet = dataCodepack.filter(element => element.codePackID == $(selectFormat).val())[0].boxesPerPallet
        var boxContainer = dataCodepack.filter(element => element.codePackID == $(selectFormat).val())[0].boxPerContainer
        $(txtBoxPerPallet).val(boxPallet);
        $(txtBoxPerPerContainer).val(boxContainer);
    }

}

function GetFillSelectPriceCondition() {

    loadCombo([], 'selectPriceConditions', true);
    var opt = 'all';
    var id = '';
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/CommercialPlan/PriceCondition?opt=" + opt + "&id=" + id,
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (dataJson.length > 0) {
                var dataList = JSON.parse(data);

                dataJson = dataList.map(
                    obj => {
                        return {
                            "id": obj.conditionPaymentID,
                            "name": obj.description
                        }
                    }
                );
            }
            loadCombo(dataJson, 'selectPriceConditions', false)
            $(selectPriceConditions).selectpicker('refresh');
        }
    });
}

function GetFillSelectPaymentTerm(customerID) {
    var bRsl = false;
    if (customerID == '') {
        return;
    }
    loadCombo([], 'selectPaymentTerms', true);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/ListPaymentTermByCustomer?customerID=" + customerID + "&cropiD=" + localStorage.cropID,
        async: true,
        success: function (data) {
            if (data.length > 0) {
                var dataList = JSON.parse(data);

                dataJson = dataList.map(
                    obj => {
                        return {
                            "id": obj.paymentTermID,
                            "name": obj.description
                        }
                    }
                );
            }

            loadCombo(dataJson, 'selectPaymentTerms', true);
            if (trEdit != null) {
                $('#selectPaymentTerms').selectpicker('val', _$paymentTerm);
            }
            $(selectPaymentTerms).selectpicker('refresh');
        }
    });
    bRsl = true;
}

function GetCommercialPlanByCampaign() {
    var sUrlApi = "/CommercialPlan/PlanCustomerVarietyList?campaignID=" + localStorage.campaignID;
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    dataPlan = dataJson;
                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {

                ShowCommercialPlanForEachVarietyAndCategory();
            }

        }
    });

}

function GetCommercialPlanByFilters() {
    var customerID = $(cboFCustomer).val();
    var destinationID = $(selectFDestination).val();
    if (destinationID == null) destinationID = -1;
    var programID = $(selectFProgram).val();
    var brandID = $(selectFCategory).val();
    var viaID = $(selecFtVia).val();
    var formatID = $(selectFFormat).val();
    if (formatID == null) formatID = -1;
    var sUrlApi = "/CommercialPlan/GetPlanBlueberryByFilters?campaignID=" + localStorage.campaignID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&programID=" + programID + "&brandID=" + brandID + "&viaID=" + viaID + "&formatID=" + formatID;
    var bRsl = false;

    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'No records were found for the filters entered. Try again.');

            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'No records were found for the filters entered. Try again.');
                } else {
                    CleanPlanComercial();
                    dataPlan = dataJson;
                    dataExport = dataJson;
                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {
                //DrawCommercialPlanStructureByCrop();
                ShowCommercialPlanForEachVarietyAndCategory();
            }
        },
    });

}

function CleanPlanComercial() {
    let dataCatByVar = [];
    $.each(dataVariety, function (iVar, variety) {
        dataCatByVar = dataVarCatByCrop.filter(item => item.varietyID == variety.id);
        $.each(dataSizeByCrop, function (iSize, objSize) {
            $.each(dataCatByVar, function (iCat, objCat) {
                var dataPlanByVarCat = dataPlan.filter(item => item.varietyID == variety.id && item.categoryID == objCat.categoryID && item.formatID == objSize.value);
                if (dataPlanByVarCat.length > 0) {
                    var id = 'tblPlan-VR-' + variety.id + '-' + objCat.categoryID + '-' + objSize.value;
                    $("#" + id + ">tbody").empty();
                }
            });
        });

    });

}

function validarenvio(data) {

    var grupo = 0;
    var DestinationID = 0;
    var CustomerID = 0;
    var result = true;

    for (var i = 0; i < data.length; i++) {
        if (i == 0) {
            grupo = data[i].Group;
            DestinationID = data[i].DestinationID;
            CustomerID = data[i].CustomerID;
            continue;
        }

        if (grupo == data[i].Group) {
            if (DestinationID != data[i].DestinationID || CustomerID != data[i].CustomerID) {
                result = false;
                return result;
            }
        }
        else {
            grupo == data[i].Group
            DestinationID = data[i].DestinationID;
            CustomerID = data[i].CustomerID;
        }

    }

    return result;
}

function validSizes(data) {
    var size = "";
    var result = true;

    for (var i = 0; i < data.length; i++) {
        size = data[i].SizeID;

        if (size.length == 0) {
            result = false;
            return result;
        }
    }

    return result;

}

function validBoxes(data) {

    var grupo = 0;
    var flaglocked = 0;
    var lockedId = 0;
    var result = true;

    for (var i = 0; i < data.length; i++) {
        if (i == 0) {
            grupo = data[i].Group;
            lockedId = data[i].LockedID;
            //marco el flag del candado
            if (lockedId == 1) { flaglocked = 1; }
            continue;
        }

        if (grupo == data[i].Group) {
            if (lockedId == data[i].LockedID) {
                if (lockedId == 1) { flaglocked = flaglocked + 1; }

            }
            else {
                if (data[i].LockedID == 1) { flaglocked = flaglocked + 1; }
            }
        }
        else {
            grupo == data[i].Group
            lockedId = data[i].LockedID;
        }

    }

    if (flaglocked > 1) {
        result = false;
        return result;
    }

    return result;
}

function GenerateSaleRequestAndPO() {
    var CPdetail = [];
    var _CPdetail = {};
    let iGroup = 0;
    let iTotaRows = 0;
    var boolPallest = 0;

    $('table#tblToProcess>tbody>tr').each(function (index, row) {
        var iStatus = parseInt(jQuery.trim($(row).find('td.tdStatus').text()));
        if (iStatus == 0) { boolPallest = 1; }
        if (iStatus == 1) {
            iTotaRows++;
            if (iGroup != parseInt(jQuery.trim($(row).find('td.tdSort').text()))) {
                iGroup = parseInt(jQuery.trim($(row).find('td.tdSort').text()));
            }
            //Capturamos los calibres
            var array_sizes = $(row).find('td.tdsize>select').val();
            var str_sizes = "";
            $.each(array_sizes, function (isize, objSize) {
                str_sizes = objSize + "," + str_sizes;
            });
            str_sizes = str_sizes.substring(0, str_sizes.length - 1);

            //valor del codePack
            var str_codePack = $(row).find('td.tdcodePack').text().split(',');
            var codePackS = '';
            $.each(str_codePack, function (index, value) {
                let codePack = value.split('_');

                $.each(array_sizes, function (index, value) {
                    codePackS = codePack[0] + '_' + codePack[1] + '_' + codePack[2] + '_' + value.replace('T', '+') + '_1' + ',' + codePackS;

                });
            });
            console.log(codePackS);
            _CPdetail = {};
            _CPdetail["Id"] = (parseInt($(row).find('td.tdVarietyXCategory').text()) == 0) ? iTotaRows : parseInt($(row).find('td.tdVarietyXCategory').text());
            _CPdetail["PlanCustomerVarietyID"] = parseInt($(row).find('td.tdplanCustomerVarietyID').text());
            _CPdetail["CustomerID"] = parseInt($(row).find('td.tdcustomerID').text());
            _CPdetail["Program"] = $(row).find('td.tdprogram').text();
            _CPdetail["DestinationID"] = parseInt($(row).find('td.tddestinationID').text());
            _CPdetail["BrandID"] = $(row).find('td.tdcategoryID').text().trim();
            _CPdetail["VarietyID"] = $(row).find('td.tdvarietyID').text();
            _CPdetail["SizeID"] = str_sizes;
            _CPdetail["CodePack"] = codePackS;//$(row).find('td.tdcodePack').text();
            _CPdetail["CodePackID"] = parseInt($(row).find('td.tdformatID').text());
            _CPdetail["ViaID"] = parseInt($(row).find('td.tdviaID').text());
            _CPdetail["MarketID"] = $(row).find('td.tdMarketID').text();
            _CPdetail["WowID"] = parseInt($(row).find('td.tdWowID').text());
            _CPdetail["Price"] = parseFloat($(row).find('td.tdpriceperbox>input').val());
            _CPdetail["Priority"] = $(row).find('td.tdpriority').text();
            _CPdetail["BoxesPerPallet"] = parseInt($(row).find('td.tdboxesPerPallet').text());
            _CPdetail["ToProcess"] = parseInt($(row).find('td.tdtoProcess').text());
            _CPdetail["Pallets"] = parseInt($(row).find('td.tdPallets').text());
            _CPdetail["Group"] = iGroup;
            _CPdetail["workOrder"] = $(row).find('#checkwork').is(':checked') ? 1 : 0;//$('#checkwork').is(':checked') ? 1 : 0;
            CPdetail.push(_CPdetail);

        }
    });

    if (boolPallest > 0) {
        sweet_alert_info('Error!', 'You must enter the total number of pallets to be processed.');
        return;
    }

    //Validamos los contenedores pertenezcan al mismo customer y destino
    if (!validarenvio(CPdetail)) {
        sweet_alert_info('Error!', 'The pallets must belong to the same customer and destination.');
        return;
    }

    //Validamos los contenedores pertenezcan al mismo customer y destino
    if (!validBoxes(CPdetail)) {
        sweet_alert_info('Error!', 'The pallets exceed the number of boxes allowed.');
        return;
    }

    //Validamos que se haya seleccionado un calibre
    if (!validSizes(CPdetail)) {
        sweet_alert_info('Error!', 'Choose a size.');
        return;
    }

    var scropId = localStorage.cropID;
    var icampaignID = localStorage.campaignID;
    var iuserCreated = $('#lblSessionUserID').text();
    var sWeekSelected = $('#selectWeekProcess option:selected').val();

    let jsonObjCPdetail = JSON.stringify({
        cropId: scropId,
        campaignID: icampaignID,
        userCreated: iuserCreated,
        projectedWeekID: sWeekSelected,
        planComercialDetail: CPdetail,
    });

    var bRsl = false;
    var sUrlApi = "/CommercialPlan/CreateSaleRequestAndPOBLU";
    var sSaleRequestCodes = '';
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: sUrlApi,
        data: jsonObjCPdetail,
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again later.');
            } else {
                $.each(data, function (iData, objData) {
                    sSaleRequestCodes = sSaleRequestCodes + '; ' + objData.referenceNumber
                });
                sSaleRequestCodes = sSaleRequestCodes.substring(1, sSaleRequestCodes.length);
                bRsl = true;
            }

        },
        error: function (datoEr) {
            sweet_alert_info('Error!', 'Fail Sale Requests and SOs Created.');
        },
        complete: function () {
            if (bRsl) {

                sweet_alert_success_prg_copytoclipboard('Good Job!', 'Sale Requests and SOs created. S.R.Codes:', sSaleRequestCodes);
                cleartblToProcess();
                //sweet_alert_progressbar_cerrar();
            }

        }

    });

}

function cleartblToProcess() {
    $('#tblToProcess>tbody').empty();
    var trAdd = '';
    let dataCatByVar = [];
    
    $.each(dataCategory, function (icategory, objCategory) {
        trAdd = '<tr class="tdcategory var' + objCategory.categoryID + '  nodrag">'
        trAdd += '<td style="display:none" class="tdVarietyXCategory">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdplanCustomerVarietyID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdcategoryID">' + objCategory.categoryID + '</td>'
        trAdd += '<td style="display:none" class="tdvarietyID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdcustomerID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tddestinationID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdviaID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdicotermID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdpaymentID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdformatID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdSizeID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdWowID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdMarketID">' + 0 + '</td>'
        trAdd += '<td style="display:none" class="tdcodePack">' + 0 + '</td>'
        trAdd += '<td style="min-width:60px;" class="tdcategory cat' + objCategory.categoryID + '">' + objCategory.description + '</td>'
        trAdd += '<td class="tdvariety" style="min-width:100px;text-align: center;"></td>'
        trAdd += '<td style="min-width:100px;" class="tdsize"></td>'
        trAdd += '<td style="min-width:100px;" class="tdcustomer"></td>'
        trAdd += '<td style="min-width:100px;" class="tddestination"></td>'
        trAdd += '<td style="min-width:100px;" class="tdprogram"></td>'
        trAdd += '<td style="min-width:100px;display:none;" class="tdpriority"></td>'
        trAdd += '<td style="min-width:30px;" class="tdvia"></td>'
        trAdd += '<td style="min-width:60px;" class="tdicoterm"></td>'
        trAdd += '<td style="min-width:70px;" class="tdpriceperbox"></td>'
        trAdd += '<td style="min-width:100px;" class="tdformat"></td>'
        trAdd += '<td style="min-width:50px;" class="tdwork"></td>'
        trAdd += '<td class="tdboxesPerPallet"></td>'
        trAdd += '<td class="tdtoProcess"></td>'
        trAdd += '<td class="tdPallets" ></td>'
        trAdd += '<td class="tdDelete"></td>'
        trAdd += '<td></td>'
        trAdd += '<td style="display:none" class="tdStatus">2</td>'
        trAdd += '<td style="display:none" class="tdMixed" >0</td>'
        trAdd += '</tr>'
        $('#tblToProcess>tbody').append(trAdd)
    });

    $("#tblToProcess").tableDnD({
        onDragStop: function (table, row) {

            verifyRowsResidues()
        }
    });
}

function loadComboConsignnee(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--To be Confirmed--</option>";
    }

    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
    $('#' + control).selectpicker('refresh');

}

$(document).ready(function () {
    $('#tblCommercialPlan thead tr').clone(true).appendTo('#tblCommercialPlan thead');
    $('#tblCommercialPlan thead tr:eq(1) td').each(function (i) {
        if (i < 14) {
            var title = $(this).text();
            $(this).html('<input type="text" />');
            $('input', this).on('keyup change', function () {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        } else {
            $(this).html('');
        }
    });

});
