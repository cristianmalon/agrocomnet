﻿var campaignID = localStorage.campaignID;
var userID = $("#lblSessionUserID").text();
var dataPackingList = [];
var dataContainer = [];
var dataCurrency = [];
var dataDocumentType = [];
var dataPaymentsType = [];
var _orderProductionID = 0;
var _customerSettlementID = 0;
var _customerInvoiceID = 0;
var _customerPaymentID = 0;
var _growerSettlementID = 0;
var PrimeraCarga = false;
var _newgrowerSettlementID = 0;
var _indexgrowerSettlementID = 0;
var _txtExchangeRate = 0;
var _DatePayment = [];
var _Variable_Fixed = 0;
var _Variable_Expends = 0;
var _Variable_ExpendsDolar = 0;
var _vexpenseFixed = 0;
var _vexpenseVariable = 0;

var _Ocean_Freight = 0;
var _Cold_Tratment = 0;
var _Import_Duty = 0;
var _Forwarding_Charge = 0;
var _Market_Charge = 0;
var _Transport_Cost = 0;
var _Detention_Charge = 0;
var _Cold_Storage = 0;
var _Other_Charge = 0;
var _Repacking_Charges = 0;
var _costos;
var _flagcurrency = 0;

var _documents = [];

const EstadosF = {
    Pending_Settlement: 1,
    Canceled: 2,
    Liquidated: 3,
    Amortized_Liquidate: 4,
    Liquidated_Paid: 5
}


function cleanControlsD() {
    $("#selectTypeDocumentD").val('')
    $("#FechaD").val('');
    $("#selectCurrencyD").val('')
    $("#txtNumberD").val('');
    $("#txtAmountD").val('');
    $("#txtExchangeRateD").val('');
    $("#txtObservationD").val('');
    $("#txtUserFreightD").val('');;;
    $("#txtInsuraceD").val('');;;

    $("#txtUserCreatedD").val('');
    $("#txtDateCreatedD").val('');
    $("#txtUserModifyD").val('');
    $("#txtLastModifyD").val('');

    $("#tblDocuments1>tbody").empty();
    $("#tblDocuments1>tfoot>tr>td").find("#inputTotalFooter").text();

    var objFecha = new Date();
    var dia = objFecha.getDate();
    var mes = objFecha.getMonth() + 1;
    var anio = objFecha.getFullYear();

    var fecha = anio + "-" + mes + "-" + dia;
    $("#FechaD").val(fecha);
}

function cleanControlsP() {
    $("#selectTypePaymentsP2").val('')
    $("#selectCurrencyP").val('')
    $("#txtNumberP").val('');
    $("#txtAmountP").val('');
    $("#txtExchangeRateP").val('');
    $("#txtObservationP").val('');
    $("#txtUserCreatedP").val('');
    $("#txtDateCreatedP").val('');
    $("#txtUserModifyP").val('');
    $("#txtLastModifyP").val('');

    $("#tblPayments1>tbody").empty();
    $("#tblPayments1>tfoot>tr>td").find("#totalCurrencyPtable").text();
    $("#tblPayments1>tfoot>tr>td").find("#totalUSDCurrencyPtable").text();

    var objFecha = new Date();
    var dia = objFecha.getDate();
    var mes = objFecha.getMonth() + 1;
    var anio = objFecha.getFullYear();

    var fecha = anio + "-" + mes + "-" + dia;
    $("#DateP").val(fecha);
}

function cleanControls() {

    $("#txtPO").val('');
    $("#txtContainer").val('');
    $("#txtCustomer").val('');
    $("#txtIncoterm").val('');
    $("#selectCurrency").val('');
    $("#txtExchangeRate").val('');
    $("#txtObservation").val('');
    var objFecha = new Date();
    var dia = objFecha.getDate();
    var mes = objFecha.getMonth() + 1;
    var anio = objFecha.getFullYear();
    var fecha = anio + "-" + mes + "-" + dia;
    $("#Date").val(fecha);

}

function openModalDocuments(orderProductionID, customerInvoiceID) {

    _orderProductionID = orderProductionID;
    _customerInvoiceID = customerInvoiceID;

    cleanControlsD();

    if (parseInt(orderProductionID) > 0) {

        fnGetDocumentsDetailByID(orderProductionID, customerInvoiceID, successGetDocumentsDetailByID, errorGetDocumentsDetailByID);
    }
}

var successGetDocumentsDetailByID = function (data) {
    if (data.length > 0) {
        var dataDocumentsDetails = JSON.parse(data);

        $(selectTypeDocumentD).selectpicker('val', dataDocumentsDetails.documentTypeID)
        var date2 = dataDocumentsDetails.date;
        if (date2 == null) {
            date2 = "";
        }

        if (date2 != "") {
            var from = date2.split("/");
            date = from[2] + "-" + from[1] + "-" + from[0];
        } else {
            date = dataDocumentsDetails.date;
        }

        $("#FechaD").val(date);

        $("#selectCurrencyD").selectpicker('val', dataDocumentsDetails.localCurrencyID);
        $("#txtNumberD").val(dataDocumentsDetails.number);
        $("#txtAmountD").val(dataDocumentsDetails.totalAmount);
        $("#txtExchangeRateD").val(dataDocumentsDetails.exchangeRate);
        $("#txtIncotermD").val(dataDocumentsDetails.incoterm);
        $("#txtObservationD").val(dataDocumentsDetails.comments);
        $("#txtUserFreightD").val(dataDocumentsDetails.freightBL);
        $("#txtInsuraceD").val(dataDocumentsDetails.insuranceBL);

        $("#txtUserCreatedD").val(dataDocumentsDetails.userCreated);
        $("#txtDateCreatedD").val(dataDocumentsDetails.dateCreated);
        $("#txtUserModifyD").val(dataDocumentsDetails.userModify);
        $("#txtLastModifyD").val(dataDocumentsDetails.dateModified);

        $('#selectTypeDocumentD').prop("disabled", true);
        $('#selectCurrencyD').prop("disabled", true);

        let _incoterm = dataDocumentsDetails.incoterm;
        let _typeD = dataDocumentsDetails.documentTypeID;
        if (_incoterm == 'FOB' && _typeD == 1) {
            $("#txtUserFreightD").attr("disabled", "disabled");
            $("#txtInsuraceD").attr("disabled", "disabled");
        } else if (_incoterm != 'FOB' && _typeD == 1) {
            $("#txtUserFreightD").removeAttr("disabled");
            $("#txtInsuraceD").removeAttr("disabled");
        } else {
            $("#txtUserFreightD").attr("disabled", "disabled");
            $("#txtInsuraceD").attr("disabled", "disabled");
        }

        var content = "";        

        $.each(dataDocumentsDetails.details, function (index, row) {
            content += "    <tr style='font-size: 14px'>";
            content += "      <td id='customerInvoiceDetailID'  style='display:none'>" + row.customerInvoiceDetailID + "</td>";
            content += "      <td id='customerInvoiceID'  style='display:none'>" + row.customerInvoiceID + "</td>";
            content += "      <td id='customerSettlementDetailID'  style='display:none'>" + row.customerSettlementDetailID + "</td>";
            content += "      <td id='brand'>" + row.brand + " </td>";
            content += "      <td id='variety'>" + row.variety + " </td>";
            content += "      <td id='description'>" + row.description + "</td>";
            content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
            content += "      <td id='quantityBoxes'>" + row.quantityBoxes + "</td>";
            content += "      <td id='weight' hidden>" + row.weight + "</td>";
            content += "      <td id='pricePerBox'>" + row.pricePerBox + "</td>";
            //content += "      <td id='total'><input class='inputTotal' id='inputTotal' onblur='calcularFooterDocuments()' type='number' value='" + row.total + "'></td>";
            content += "      <td id='inputTotal' class='inputTotal' onblur='calcularFooterDocuments()'>" + row.total + "</td>";
            content += "      <td id='priceBoxFOB'>" + row.pricePerBoxFOB + "</td>";
            content += "      <td id='totalFOB' class='totalFOB' onblur='calcularFooterDocuments()'>" + row.totalFOB + "</td>";
            content += "    </tr>";
        });

        $("#tblDocuments1>tbody").empty().append(content);
        calcularFooterDocuments();
    }
}

var errorGetDocumentsDetailByID = function (xhr, ajaxOptions, thrownError) {
    $.notify("Problems loading the list!", "error");
    console.log(api)
    console.log(xhr);
    console.log(ajaxOptions);
    console.log(thrownError);
}

function calcularFooterDocuments() {
    var total = 0;
    var totalFOB = 0;
    $('#tblDocuments1>tbody>tr').each(function (i, e) {
        total = parseFloat($(e).find('td.inputTotal').text()) + total;
        totalFOB = parseFloat($(e).find('td.totalFOB').text()) + totalFOB;
    })
    $('#inputTotalFooter').text(total.toFixed(2))
    $('#totalFooterFOB').text(totalFOB.toFixed(2))
}

function calcularFooterPayments() {
    var total = 0;
    var totalUSD = 0;
    $('#tblPayments1>tbody>tr').each(function (i, e) {
        total = parseFloat($(e).find('input.inpTotal').val()) + total;
        totalUSD = parseFloat($(e).find('input.inpTotalUSD').val()) + totalUSD;
    })
    $('#totalCurrencyPtable').text(total.toFixed(2))
    $('#totalUSDCurrencyPtable').text(totalUSD.toFixed(2))
    //$('#txtAmountP').val(total.toFixed(2))
}

function openModalPayment(orderProductionID, customerPaymentID, DatePayment) {

    _orderProductionID = orderProductionID;
    _customerPaymentID = customerPaymentID;
    _DatePayment = DatePayment;

    cleanControlsP();
    if (parseInt(orderProductionID) > 0) {

        fnGetPaymentsDetailByID(orderProductionID, customerPaymentID, successGetPaymentsDetailByID, errorGetPaymentsDetailByID);
    }
}

var successGetPaymentsDetailByID = function (data) {
    if (data.length > 0) {
        var dataPaymentDetails = JSON.parse(data);
        var content = "";
        $("#selectTypePaymentsP2").selectpicker('val', dataPaymentDetails.paymentTypeID)

        var date = "";
        var date2 = _DatePayment;
        if (date2 == null) {
            date2 = "";
        }
        if (date2 != "") {
            var from = date2.split("/");
            date = from[2] + "-" + from[1] + "-" + from[0];
        } else {
            date = _DatePayment;
        }

        $("#DateP").val(date);
        $("#selectCurrencyP").selectpicker('val', dataPaymentDetails.localCurrencyID);
        $("#txtNumberP").val(dataPaymentDetails.numberOperation);
        $("#txtAmountP").val(dataPaymentDetails.amount);
        $("#txtExchangeRateP").val(dataPaymentDetails.exchangeRate);
        $("#txtObservationP").val(dataPaymentDetails.comments);
        $("#txtUserCreatedP").val(dataPaymentDetails.userCreated);
        $("#txtDateCreatedP").val(dataPaymentDetails.dateCreated);
        $("#txtUserModifyP").val(dataPaymentDetails.userModify);
        $("#txtLastModifyP").val(dataPaymentDetails.dateModified);
        $("#customerSettlementID").val(dataPaymentDetails.customerSettlementID);
        $('#selectTypePaymentsP2').prop("disabled", true);
        $('#selectCurrencyP').prop("disabled", true);
        $.each(dataPaymentDetails.details, function (index, row) {
            content += "    <tr style='font-size: 14px'>";
            content += "      <td id='customerPaymentsDetailID'  style='display:none'>" + row.customerPaymentsDetailID + "</td>";
            content += "      <td id='customerPaymentID'  style='display:none'>" + row.customerPaymentID + "</td>";
            content += "      <td id='customerSettlementDetailID'  style='display:none'>" + row.customerSettlementDetailID + "</td>";
            content += "      <td id='brand' >" + row.brand + " </td>";
            content += "      <td id='variety' >" + row.variety + " </td>";
            content += "      <td id='description'>" + row.description + "</td>";
            content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
            content += "      <td id='quantityBoxes'  >" + row.quantityBoxes + "</td>";
            content += "      <td id='pricePerBox' style='display:none'><input class='inpPricePerBox' type='number' value='" + row.pricePerBox + "'></td>";
            content += "      <td id='total'><input class='inpTotal'  type='number' value='" + row.total + "'></td>";
            content += "      <td id='totalUSD'><input disabled class='inpTotalUSD'  type='number' value='" + row.totalUSD + "'></td>";
            content += "    </tr>";
        });

        $("#tblPayments1>tbody").empty().append(content);
        calcularFooterPayments();
    }
}

var errorGetPaymentsDetailByID = function (xhr, ajaxOptions, thrownError) {
    $.notify("Problems loading the list!", "error");
    console.log(api)
    console.log(xhr);
    console.log(ajaxOptions);
    console.log(thrownError);
}

$(function () {
    var currentdate = new Date();
    var finalDate = currentdate.getDate() + "/"
        + ((currentdate.getMonth() + 1) < 10 ? '0' : '') + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear();

    var future = new Date();
    future.setDate(future.getDate() - 7);
    var begindate = future.getDate() + "/" + ((future.getMonth() + 1) < 10 ? '0' : '') + (future.getMonth() + 1) + "/" + future.getFullYear();

    jQuery('#txtFrom').datetimepicker({
        format: 'd/m/Y',
        closeOnDateSelect: true,
        timepicker: false,
    });

    jQuery('#txtUntil').datetimepicker({
        format: 'd/m/Y',
        closeOnDateSelect: true,
        timepicker: false,
    });

    $('#chkFlagExchange').change(function () {

        if ($('#chkFlagExchange').is(":checked")) {
            _flagcurrency = 1;
        }
        else {
            _flagcurrency = 0;
        }
        calcularCost();
    });

    $('#Date').change(function () {
        var monedaInicialVal = $("#selectCurrency option:selected").val();
        if (monedaInicialVal == 1)//si es dolar
        {
            fnGetTipoCambio();
        }

    });

    $("#txtFrom").val(begindate);
    $("#txtUntil").val(finalDate);

    createList();
    loadData();
    loadDefaultValues();

    //Btn View Sispacking
    $("#btnViewSispacking").click(
        function () {
            openPackingList();
        }
    )

    //Btn View Sispacking
    $("#btnLiquidated").click(
        function () {
            ListOfFinance(EstadosF.Liquidated);
        }
    )

    $("#btnAmortizedLiquidate").click(
        function () {
            ListOfFinance(EstadosF.Amortized_Liquidate);
        }
    )

    //btnCanceled
    $("#btnLiquidatedPaid").click(
        function () {
            ListOfFinance(EstadosF.Liquidated_Paid);
        }
    )

    $("#btnPendingSettlement").click(
        function () {
            ListOfFinance(EstadosF.Pending_Settlement);
        }
    )

    $(document).on("click", ".checkcost", function () {
        alert('changed');
    })

    $(document).on("click", "#btnModalDocuments", function () {
        openModalDocuments(_orderProductionID, 0)
    })

    $(document).on("click", "#btnEditDocuments", function () {
        openModalDocuments(_orderProductionID, $(this).parents('tr').find('#customerInvoiceID').text())
    })

    $(document).on("click", "#btnModalEdit", function () {
        _vexpenseFixed = 0;
        _vexpenseVariable = 0;
        _Variable_Expends = 0;
        _Variable_Fixed = 0;
        _Ocean_Freight = 0;
        _Cold_Tratment = 0;
        _Import_Duty = 0;
        _Forwarding_Charge = 0;
        _Market_Charge = 0;
        _Transport_Cost = 0;
        _Detention_Charge = 0;
        _Cold_Storage = 0;
        _Other_Charge = 0;
        _Repacking_Charges = 0;
        openModal(this, $(this).parents('tr').find('#orderProductionID').text())
    })

    $(document).on("click", "#btnModal", function () {
        openModal(this, 0)
    })

    $(document).on("click", "#btnModalPayments", function () {

        openModalPayment(_orderProductionID, 0, null)
    })

    $(document).on("click", "#btnModalSettlement", function () {

        AddNewSettlement(_orderProductionID)

    })

    $(document).on("click", "#btnSearch", function () {
        createList();
    })

    $(document).on("click", "#btnDeleteSettlement", function () {
        var growerSettlementID = $(this).closest("tr").find('#growerSettlementID').val();
        _indexgrowerSettlementID = $(this).closest("tr").index();
        _indexgrowerSettlementID = _indexgrowerSettlementID - 1;


        if (_indexgrowerSettlementID > 0) {
            $('#tblSettlement>tbody>tr').each(function (i, e) {
                if (i == _indexgrowerSettlementID) {
                    _newgrowerSettlementID = $(e).find('#growerSettlementID').val();

                    return;
                }
            });
        }
        PreeliminarSettelement(growerSettlementID);


    })

    $(document).on("click", "#btnEditPayment", function () {
        openModalPayment(_orderProductionID, $(this).parents('tr').find('#customerPaymentID').text(), $(this).parents('tr').find('#date').text())
    })

    $(document).on("click", "#btnSaveDocuments", function () {
        PreGuardarDocuments();
    })

    $(document).on("click", "#btnSavePayments", function () {
        PreGuardarPayments();
    })

    $(document).on("click", "#btnSaveDetailsCost", function () {
        PreGuardarDetailsCost();
    })

    $(document).on("click", "#btnSaveFinanceGrower", function () {
        PreGuardarFinanceGrower();
    })

    $(document).on("change", "input.inputTotal", function () {

        var qBoxes = parseInt($(this).closest('tr').find('td#quantityBoxes').text())

        var qTotalFob = parseFloat($(this).val())

        var qPerBox = qTotalFob / qBoxes;
        $(this).closest('tr').find('input.inputPricePerBox').val(qPerBox.toFixed(2))
    })

    $(document).on("change", "input.inpTotal", function () {
        if ($("#txtExchangeRateP").val() == '' || $("#txtExchangeRateP").val() <= 0) {
            $(this).val('0');
            sweet_alert_error('Error', 'Enter Exchange Rate.');
            return;
        }

        var qBoxes = parseInt($(this).closest('tr').find('td#quantityBoxes').text())

        var qTotal = parseFloat($(this).val())

        var qRate = $("#txtExchangeRateP").val();

        var qtotalUSD = qTotal / qRate;

        var qPerBox = qTotal / qBoxes;

        $(this).closest('tr').find('input.inpPricePerBox').val(qPerBox.toFixed(2))
        $(this).closest('tr').find('input.inpTotalUSD').val(qtotalUSD.toFixed(2))

        calcularFooterPayments();
    })

    $(document).on("change", "input#txtExchangeRateP", function () {
        var totalUSD = 0;
        $('#tblPayments1>tbody>tr').each(function (i, e) {
            var qTotal = parseFloat($(this).closest('tr').find('input.inpTotal').val())
            var qRate = $("#txtExchangeRateP").val();

            var qtotalUSD = qTotal / qRate;
            $(this).closest('tr').find('input.inpTotalUSD').val(qtotalUSD.toFixed(2))
        })
        calcularFooterPayments();
    })

})

function loadData() {
    var campaignID = localStorage.campaignID;

    fnGetPacking(campaignID, successGetPackingList);

    fnGetContainer(campaignID, successGetContainer);

    fnGetLocalCurrency(successGetLocalCurrency2);

    fnGetDocumentType(successGetDocumentType);

    fnGetPaymentsType(successGetPaymentsType);

}

function fnGetTipoCambio() {

    var date = $("#Date").val();

    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Finance/GetTypeChange?fecha=" + date,
        async: false,
        success: function (data) {
            if (data.length > 0) {

                var dataJson = JSON.parse(data);

                $("#txtExchangeRate").val(dataJson.venta);
                $("#txtExchangeRate").change();
            }
        }
    });

}

var successGetPackingList = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataPackingList = dataJson.map(
            obj => {
                return {
                    "id": obj.orderProductionID,
                    "name": obj.packingList
                }
            }
        );
    }
}

var successGetContainer = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataContainer = dataJson.map(
            obj => {
                return {
                    "id": obj.orderProductionID,
                    "name": obj.container
                }
            }
        );
    }
}

var successGetLocalCurrency2 = function (data) {
    var dataJson = JSON.parse(data);

    if (data.length > 0) {
        dataCurrency = dataJson.map(
            obj => {
                return {
                    "id": obj.localCurrencyID,
                    "name": obj.localCurrency + " (" + obj.localCurrencySymbol + ")",
                    "symbol": obj.localCurrencySymbol
                }
            }
        );
    }
}

var successGetDocumentType = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        console.log(data);
        dataDocumentType = dataJson.map(
            obj => {
                return {
                    "id": obj.documentTypeID,
                    "name": obj.documentType
                }
            }
        );
    }
}

var successGetPaymentsType = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataPaymentsType = dataJson.map(
            obj => {
                return {
                    "id": obj.paymentTypeID,
                    "name": obj.paymentType
                }
            }
        );
    }
}

function loadDefaultValues() {

    loadCombo(dataPackingList, 'cboPackingList', true, '[ALL]');
    $('#cboPackingList').selectpicker('refresh');

    loadCombo(dataContainer, 'cboContainer', true, '[ALL]');
    $('#cboContainer').selectpicker('refresh');

    loadCombo(dataCurrency, 'selectCurrency', false);
    $('#selectCurrency').selectpicker('refresh');
    loadCombo(dataCurrency, 'selectCurrencyD', false);
    $('#selectCurrencyD').selectpicker('refresh');
    loadCombo(dataCurrency, 'selectCurrencyP', false);
    $('#selectCurrencyP').selectpicker('refresh');

    loadCombo(dataDocumentType, 'selectTypeDocumentD', false);
    $('#selectTypeDocumentD').selectpicker('refresh');

    loadCombo(dataPaymentsType, 'selectTypePaymentsP2', false);
    $('#selectTypePaymentsP2').selectpicker('refresh');

}

$("#selectCurrency").change(function () {
    var monedaInicial = $("#selectCurrency option:selected").text();
    var monedaInicialVal = $("#selectCurrency option:selected").val();
    $('#tblGastos>thead>tr').each(function (i, e) {
        $(e).find('#CostDistributionCurrency').text(monedaInicial)
    })

    $('#tblSettlement1>thead>tr').each(function (i, e) {
        $(e).find('#CurrencyLocalBox').text(monedaInicial);
        $(e).find('#CurrencyLocalSales').text(monedaInicial)
    })

    $(selectCurrencyD).selectpicker('val', monedaInicialVal)
    $('#selectCurrencyD').selectpicker('refresh');
    $('#selectCurrencyP').val(monedaInicialVal);
    $('#selectCurrencyP').selectpicker('refresh');

    if (monedaInicialVal == 1)//Si es dolar
    {
        fnGetTipoCambio();
    }
})

$("#selectCurrencyP").change(function () {
    var monedaInicial = $("#selectCurrencyP option:selected").text();
    var monedaInicialVal = $("#selectCurrencyP option:selected").val();
    if (monedaInicialVal > 0) {
        $('#tblPayments1>thead>tr').each(function (i, e) {
            $(e).find('#CurrencyPtable').text(monedaInicial)

        })
    }
})

function loadCombo(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function openModal(obj, orderProductionID) {
    if (orderProductionID == 0) {
        $('table#tblDocuments>tbody').empty();
        $('table#tblPayments>tbody').empty();

        $('table#tblSettlement>tbody').empty();
        $('table#tblGastos>tbody').empty();

        $('table#tblSettlement1>tbody').empty();
        $('table#tblSettlement1>tfoot').empty();

        $('table#tblSettlement2>tbody').empty();
        $('table#tblSettlement2>tfoot').empty();

        $('table#tblSettlement3>tbody').empty();
        $('table#tblSettlement3>tfoot').empty();

        $("#tblDetails>tbody").empty();
        $("#tblDetails2>tbody").empty();
        $("#tblDetails3>tbody").empty();
        cleanControls();
    }
    else {
        PrimeraCarga = true;
        cleanControls();
        _orderProductionID = orderProductionID;
        $('table#tblDocuments>tbody').empty();
        $('table#tblPayments>tbody').empty();

        $('table#tblSettlement>tbody').empty();
        $('table#tblGastos>tbody').empty();

        $('table#tblSettlement1>tbody').empty();
        $('table#tblSettlement1>tfoot').empty();

        $('table#tblSettlement2>tbody').empty();
        $('table#tblSettlement2>tfoot').empty();

        $('table#tblSettlement3>tbody').empty();
        $('table#tblSettlement3>tfoot').empty();

        $("#tblDetails>tbody").empty();
        $("#tblDetails2>tbody").empty();
        $("#tblDetails3>tbody").empty();

        loadPOForFinanceCabecera(orderProductionID);
        loadPOForFinanceSettlement(orderProductionID);
        loadDocumentsByPOID(orderProductionID);
        loadPaymentByPOID(orderProductionID);
        loadCostDistribution(orderProductionID);
        CalcularCostVariable();
        loadPOForFinanceAdjutsInvoiceOZM(orderProductionID);
        loadPOForFinancePayments(orderProductionID);
        loadPOSettlementByPOID(orderProductionID);
        //fnGetTipoCambio();
    }
}

function AddNewSettlement(orderProductionID) {

    var growerSettlementID = 0;
    var customerSettlementID = _customerSettlementID;
    var userID = parseInt($("#lblSessionUserID").text());
    var statusConfirm = "";
    var date = $("#Date").val();
    var reference = '';
    var number = 0;
    var qc = 0;
    var insurance = 0;
    var others = 0;
    var comissionPercent = 0;
    var comission = 0;
    var promotionFeePercent = 0;
    var promotion = 0;

    var o = {}

    o = {
        "growerSettlementID": growerSettlementID,
        "customerSettlementID": customerSettlementID,
        "number": number,
        "reference": reference,
        "date": date,
        "qc": qc,
        "insurance": insurance,
        "others": others,
        "comissionPercent": comissionPercent,
        "comission": comission,
        "promotionFeePercent": promotionFeePercent,
        "promotion": promotion,
        "statusConfirm": statusConfirm,
        "userID": userID,
        "details": []
    }

    $('table#tblSettlement1>tbody>tr').each(function (i, e) {

        var det = {
            "growerSettlementDetailID": $(e).find("#growerSettlementDetailID").val(),
            "growerSettlementID": $(e).find("#growerSettlementID").val(),
            "customerSettlementDetailID": $(e).find("#customerSettlementDetailID").val(),
            "pricePerBox": $(e).find("#pricePerBox").val(),
            "total": $(e).find("#totalSettlement").val()
        }

        o.details.push(det);

    });

    var api = "/Finance/SaveFinanceGrowerWithoutDetail"

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {

            if (parseInt(datares) > 0) {
                sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
                loadPOSettlementByPOID(orderProductionID)
            } else {
                if (parseInt(datares) == -1) {
                    sweet_alert_error("Cancelado!", "No se ha registrado el costo de distribución!");
                    return;
                }
                sweet_alert_error("Error", "NO se guardaron los datos!");

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            sweet_alert_error("Error", "NO se guardaron los datos!");
        }
    });

}

function loadDocumentsByPOID(orderProductionID) {
    var content = "";
    //Recorriendo la lista para llenar el detalle de la tabla documentos 
    fnGetDocumentsByPOID(orderProductionID, successGetDocumentsByPOID, errorGetDocumentsByPOID);
}

var successGetDocumentsByPOID = function (data) {
    if (data.length > 0) {
        var dataDocuments = JSON.parse(data);
        //Creando el formato de la tabla
        var content = "";
        //Recorriendo la lista para llenar el detalle de la tabla
        $.each(dataDocuments, function (index, row) {
            content += "    <tr style='font-size: 14px'>";
            content += "      <td id='customerInvoiceID'  style='display:none'>" + row.customerInvoiceID + "</td>";
            content += "      <td id='documentTypeID'  style='display:none'>" + row.documentTypeID + "</td>";
            content += "      <td id='localCurrencyID' style='display:none'>" + row.localCurrencyID + "</td>";
            content += '      <td id="Operations" td style="max-width:20px;min-width:20px; text-align:center"><button class="btn btn-sm btn-primary" id="btnEditDocuments" data-toggle="modal" data-target="#myModalDocuments" data-backdrop="static" data-keyboard="false"><i style="color: white" class="fas fa-edit fa-sm"></i></button></td>';
            content += "      <td id='documentType'>" + row.documentType + "</td>";
            content += "      <td id='invoice' >" + row.number + " </td>";
            content += "      <td id='localCurrencySymbol'>" + row.localCurrencySymbol + " " + row.totalAmount + "</td>";
            content += "      <td id='localCurrency'>" + row.localCurrency + "</td>";
            content += "      <td id='date'>" + row.date + "</td>";
            content += "      <td id='comments'>" + row.comments + "</td>";
            content += "    </tr>";
        });
        $("#tblDocuments>tbody").empty().append(content);
    }
}

var errorGetDocumentsByPOID = function (xhr, ajaxOptions, thrownError) {
    $.notify("Problemas con el cargar el listado!", "error");
    console.log(api)
    console.log(xhr);
    console.log(ajaxOptions);
    console.log(thrownError);
}

function loadPaymentByPOID(orderProductionID) {
    var content = "";
    //Recorriendo la lista para llenar el detalle de la tabla payment
    fnGetPaymentByPOID(orderProductionID, successGetPaymentByPOID, errorGetPaymentByPOID);
}

var successGetPaymentByPOID = function (data) {
    if (data.length > 0) {
        var dataPayment = JSON.parse(data);
        var content = "";
        $.each(dataPayment, function (index, row) {
            content += "    <tr style='font-size: 14px'>";
            content += "      <td id='customerSettlementID' style='display:none'>" + row.customerSettlementID + "</td>";
            content += "      <td id='customerPaymentID' style='display:none'>" + row.customerPaymentID + "</td>";
            content += "      <td id='localCurrencyID' style='display:none'>" + row.localCurrencyID + "</td>";
            content += '      <td id="operations" style="max-width:20px;min-width:20px; text-align:center"><button class="btn btn-sm btn-primary" id="btnEditPayment" data-toggle="modal" data-target="#myModalPayments" data-backdrop="static" data-keyboard="false"><i style="color: white" class="fas fa-edit fa-sm"></i></button></td>';
            content += "      <td id='date' >" + row.date + "</td>";
            content += "      <td id='amount' >" + row.localCurrencySymbol + " " + row.amountUSD + "</td>";
            content += "      <td id='localCurrency' >" + row.localCurrency + "</td>";
            content += "      <td id='amountUSD' >" + row.numberOperation + "</td>";
            content += "      <td id='description' >" + row.description + "</td>";
            content += "      <td id='comments' >" + row.comments + "</td>";
            content += "    </tr>";
        });

        $("#tblPayments>tbody").empty().append(content);
    }
}

var errorGetPaymentByPOID = function (xhr, ajaxOptions, thrownError) {
    $.notify("Problemas con el cargar el listado!", "error");
    console.log(api)
    console.log(xhr);
    console.log(ajaxOptions);
    console.log(thrownError);
}

function loadPOForFinanceCabecera(orderProductionID, campaignID, userID) {
    var campaignID = localStorage.campaignID;
    var userID = $("#lblSessionUserID").text();
    //Recorriendo el arreglo para llenar la cabecera
    fnGetPOForFinance(orderProductionID, campaignID, userID, successGetPOForFinance, errorGetPOForFinance);
}

var successGetPOForFinance = function (data) {
    if (data.length > 0) {
        var dataFinanceHeader = JSON.parse(data);
        _customerSettlementID = dataFinanceHeader.customerSettlementID;
        $("#txtPO").val(dataFinanceHeader.orderProduction);
        $("#txtContainer").val(dataFinanceHeader.container);
        $("#txtCustomer").val(dataFinanceHeader.customer);
        $("#txtIncoterm").val(dataFinanceHeader.incoterm);

        var date2 = dataFinanceHeader.date;
        if (date2 == null) {
            date2 = "";
        }
        if (date2 != "") {
            var from = date2.split("/");
            var date = from[2] + "-" + from[1] + "-" + from[0];
        } else {
            date = dataFinanceHeader.date;
        }

        $("#Date").val(date);
        $("#selectCurrency").val(dataFinanceHeader.localCurrencyID);
        $("#selectCurrency").selectpicker('refresh');
        $("#selectCurrency").change();
        _txtExchangeRate = dataFinanceHeader.exchangeRate;

        if (_txtExchangeRate > 0) {
            $("#txtExchangeRate").val(dataFinanceHeader.exchangeRate);
        } else {
            fnGetTipoCambio();
        }
        
        $("#txtObservation").val(dataFinanceHeader.comments);
        if (_txtExchangeRate > 0) {
            loadCostDistribution(_orderProductionID);
        }

        let flagExchange = dataFinanceHeader.flagExchange;
        if (flagExchange == 1) {
            $('#chkFlagExchange').prop('checked', true);

        } else {
            $('#chkFlagExchange').prop('checked', false);
        } 
    }
}

var errorGetPOForFinance = function (xhr, ajaxOptions, thrownError) {
    $.notify("Problemas con el cargar el listado!", "error");
    console.log(api)
    console.log(xhr);
    console.log(ajaxOptions);
    console.log(thrownError);
}

function loadPOForFinanceSettlement(orderProductionID) {

    var content = "";
    var campaignID = localStorage.campaignID;
    var userID = $("#lblSessionUserID").text();

    var api = "/Finance/GetPOForFinance?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataFinance = JSON.parse(data);
                var content = "";
                $.each(dataFinance.details, function (index, row) {
                    content += "    <tr style='font-size: 14px'>";
                    content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                    content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementID" value ="' + (row.customerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                    content += '      <td> <input style = "" onblur = "" id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" onblur = "" id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" onblur = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "" id = "kgCaja" value ="' + (row.kgCaja) + '" class=" form-control form-control-sm"  min="0" pattern="[0 - 9]{ 10 }"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                    content += '      <td> <input style = "" onblur = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "" id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm"  min="0" pattern="[0 - 9]{ 10 }"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "" id = "pricePerBox" value ="' + (row.pricePerBox) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "calcular(this)" id = "totalSettlement" value ="' + (row.totalSettlement) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "" id = "expenseFixed" value ="' + (row.expenseFixed) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "" id = "expenseVariable" value ="' + (row.expenseVariable) + '" class=" form-control form-control-sm"    type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" onblur = "" id = "netSettlemnt" value ="' + (row.netSettlement) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                    content += "    </tr>";
                });
                $("#tblDetails>tbody").empty().append(content);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);

        }
    });

    calcularTotalSettlement();
}

function loadPOForFinanceAdjutsInvoiceOZM(orderProductionID) {

    var content = "";
    var campaignID = localStorage.campaignID;
    var userID = $("#lblSessionUserID").text();

    var api = "/Finance/GetPOForFinance?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataFinance = JSON.parse(data);
                var content = "";
                $.each(dataFinance.details, function (index, row) {
                    content += "    <tr style='font-size: 14px'>";
                    content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                    content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementID" value ="' + (row.customerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                    content += '      <td> <input style = "" id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;"  id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm"  min="0" pattern="[0 - 9]{ 10 }"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "totalBill" value ="' + (row.totalInvoiceUSD) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "saleValueSettlement" value ="' + (row.totalSettlementUSD) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "settlementExpenseValue" value ="' + (row.totalExpensesUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "settlementFinalBalance" value ="' + (row.netSettlement) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "DN_CN" value ="' + (row.totalDiferenceUSD) + '" class=" form-control form-control-sm"    type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "setfinalPrice" value ="' + parseFloat(row.finalPriceUSD).toFixed(2) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "setfinalPriceXKG" value ="' + parseFloat(row.finalPriceCaja).toFixed(2) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                    content += "    </tr>";
                });
                $("#tblDetails2>tbody").empty().append(content);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);

        }
    });

    calcularTotalAdjustInvoice();
}


function loadPOForFinancePayments(orderProductionID) {

    var content = "";
    var campaignID = localStorage.campaignID;
    var userID = $("#lblSessionUserID").text();

    var api = "/Finance/GetPOForFinance?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataFinance = JSON.parse(data);
                var content = "";
                $.each(dataFinance.details, function (index, row) {
                    content += "    <tr style='font-size: 14px'>";
                    content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                    content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementID" value ="' + (row.customerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                    content += '      <td> <input style = "" id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;"  id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm"  min="0" pattern="[0 - 9]{ 10 }"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "totalToColletUSD" value ="' + (row.totalToColletUSD) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "advancePaymentsUSD" value ="' + (row.advancePaymentsUSD).toFixed(2) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                    content += '      <td> <input style = "text-align: right;" id = "balancePayemntesUSD" value ="' + (row.balancePayemntesUSD).toFixed(2) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';

                    content += "    </tr>";
                });
                $("#tblDetails3>tbody").empty().append(content);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");


        }
    });

    calcularTotalPayments();
}

function loadPOSettlementByPOID(orderProductionID) {

    var content = "";
    var api = "/Finance/GetGrowerSettlementByPOID?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataGrowerSettlement = JSON.parse(data);
                var content = "";
                var growerSettlementID = 0;

                $.each(dataGrowerSettlement, function (index, row) {
                    content += "    <tr style='font-size: 14px'>";
                    content += '      <td style="max-width:20px;min-width:20px; display:none"> <input  id = "growerSettlementID" value ="' + row.growerSettlementID + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += '      <td style="max-width:20px;min-width:20px; display:none"> <input  id = "number" value ="' + row.number + '" class=" form-control form-control-sm" type="text" readonly></input></td>';

                    if (dataGrowerSettlement.length - 1 == index) {
                        growerSettlementID = row.growerSettlementID;
                        content += '      <td id="operations" style="max-width:10px;min-width:10px; text-align:center"><input type="radio" id="checkSeleccionado" name="checkSeleccionado"  value="" checked><button style="border:none" id="btnDeleteSettlement"><i style="color: darkred" class="fas fa-trash fa-sm"></i></button></td>';
                    }
                    else {
                        content += '      <td id="operations" style="max-width:10px;min-width:10px; text-align:center"><input type="radio" id="checkSeleccionado" name="checkSeleccionado"  value=""><button style="border:none" id="btnDeleteSettlement"><i style="color: darkred" class="fas fa-trash fa-sm"></i></button></td>';
                    }

                    content += '      <td style="max-width:30px;min-width:30px;"> <input  id = "reference" value ="' + row.reference + '" class=" form-control form-control-sm" type="text"></input></td>';
                    var date = "";
                    var date2 = row.date;
                    if (date2 == null) {
                        date2 = "";
                    }
                    if (date2 != "") {
                        var from = date2.split("/");
                        date = from[2] + "-" + from[1] + "-" + from[0];
                    } else {
                        date = row.date;
                    }


                    content += "      <td style='max-width:20px;min-width:20px;'><input type='date' value='" + date + "' class='form-control form-control-sm' id= 'Date'  value='2011-08-19'> </td> ";
                    content += '      <td style="max-width:10px;min-width:10px;"> <input style = "" id = "status" value ="' + row.status + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                    content += "    </tr>";
                });
                $("#tblSettlement>tbody").empty().append(content);


                if (dataGrowerSettlement.length == 0) {
                    $("#tblSettlement1>tbody").empty();
                    $("#tblSettlement1>tfoot").empty();
                    $("#tblSettlement2>tbody").empty();
                    $("#tblSettlement2>tfoot").empty();
                    $("#tblSettlement3>tbody").empty();

                    return;
                }

                content = "";

                //Recorriendo la lista para llenar el detalle de la tabla tblSettlement1
                var vacio = 0;
                var api = "/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID;
                $.ajax({
                    type: "GET",
                    url: api,
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {

                            var dataGrowerSettlement = JSON.parse(data);
                            var content = "";
                            $.each(dataGrowerSettlement.details, function (index, row) {
                                content += "    <tr style='font-size: 14px'>";
                                content += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementDetailID" value ="' + (row.growerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                                content += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementID" value ="' + (row.growerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                                content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                                content += '      <td> <input style = "text-align: right;"  id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;"  id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                                content += '      <td> <input style = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;" id = "weight" value ="' + (row.weight) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                                content += '      <td> <input style = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;" id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;" id = "pricePerBoxSettlement" value ="' + (row.pricePerBoxSettlement) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;" id = "pricePerBoxSettlementUSD" value ="' + (row.pricePerBoxSettlementUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;" id = "totalSettlement" value ="' + (row.totalSettlement) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                                content += '      <td> <input style = "text-align: right;" id = "totalSettlementUSD" value ="' + (row.totalSettlementUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';

                                content += "    </tr>";
                            });
                            $("#tblSettlement1>tbody").empty().append(content);
                            vacio = 1;

                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $.notify("Problemas con el cargar el listado!", "error");
                        console.log(api)
                        console.log(xhr);
                        console.log(ajaxOptions);
                        console.log(thrownError);
                    }
                });


                if (vacio == 0) {

                    $("#tblSettlement1>tbody").empty();
                    $("#tblSettlement1>tfoot").empty();
                    $("#tblSettlement2>tbody").empty();
                    $("#tblSettlement2>tfoot").empty();
                    $("#tblSettlement3>tbody").empty();
                    return;
                }

                contentfoot = "";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=8  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
                contentfoot += '<td id="totalSettlement" style="text-align: right">0</td>';
                contentfoot += '<td id="totalSettlementUSD" style="text-align: right"> 0</td>';
                contentfoot += "</tr>";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=8  style='text-align:right; background-color: #A3A6AD; color:white'>Expenses";
                contentfoot += '<td id="expenses" style="text-align: right">0</td>';
                contentfoot += '<td id="expensesUSD" style="text-align: right"> 0</td>';
                contentfoot += "</tr>";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=8  style='text-align:right; background-color: #A3A6AD; color:white'>Net";
                contentfoot += '<td id="net" style="text-align: right">0</td>';
                contentfoot += '<td id="netUSD" style="text-align: right">0</td>';
                contentfoot += "</tr>";
                $("#tblSettlement1>tfoot").empty().append(contentfoot);

                //Calculos de la liquidación tblSettlement1
                var totalSettlement = 0;
                var ExchangeRate = $('#txtExchangeRate').val();

                $('#tblSettlement1>tbody>tr').each(function (j, row) {
                    $('#tblDetails>tbody>tr').each(function (i, e) {
                        if (i == j) {

                            totalSettlement = $(e).closest("tr").find('#totalSettlement').val();
                            $(row).closest("tr").find('#quantityBoxes').val(parseFloat(0).toFixed(2));
                            $(row).closest("tr").find('#totalSettlement').val(parseFloat(totalSettlement).toFixed(2));
                            if (_flagcurrency == 1) { $(row).closest("tr").find('#totalSettlementUSD').val((totalSettlement * ExchangeRate).toFixed(2)); }
                            else { $(row).closest("tr").find('#totalSettlementUSD').val((totalSettlement / ExchangeRate).toFixed(2)); }

                            pricePerBoxSettlementUSD = ($(row).closest("tr").find('#quantityBoxes').val()) / ($(row).closest("tr").find('#totalSettlementUSD').val());
                            $(row).closest("tr").find('#pricePerBoxSettlementUSD').val(pricePerBoxSettlementUSD.toFixed(2));
                            pricePerBoxSettlement = ($(row).closest("tr").find('#quantityBoxes').val()) / ($(row).closest("tr").find('#totalSettlement').val());
                            $(row).closest("tr").find('#pricePerBoxSettlement').val(pricePerBoxSettlement.toFixed(2));
                        }
                    })
                })
                //calculat el tfoot tblSettlement1
                calcularTotalSettlementDetail();

                content2 = ""

                var api = "/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID;
                $.ajax({
                    type: "GET",
                    url: api,
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {
                            var dataGrowerSettlement = JSON.parse(data);
                            var content2 = "";
                            $.each(dataGrowerSettlement.details, function (index, row) {
                                content2 += "    <tr style='font-size: 14px'>";
                                content2 += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementDetailID" value ="' + (row.growerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                                content2 += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementID" value ="' + (row.growerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                                content2 += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                                content2 += '      <td> <input style = "text-align: right;"  id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                                content2 += '      <td> <input style = "text-align: right;"  id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                                content2 += '      <td> <input style = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                                content2 += '      <td> <input style = "text-align: right;" id = "weight" value ="' + (row.weight) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                                content2 += '      <td> <input style = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                                content2 += '      <td> <input style = "text-align: right;" id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                                content2 += '      <td> <input style = "text-align: right;" id = "pricePerBoxSettlementNetUSD" value ="' + (row.pricePerBoxSettlementNetUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                                content2 += '      <td> <input style = "text-align: right;" id = "total" value ="' + (row.total) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';

                            });
                            $("#tblSettlement2>tbody").empty().append(content2);

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $.notify("Problemas con el cargar el listado!", "error");
                        console.log(api)
                        console.log(xhr);
                        console.log(ajaxOptions);
                        console.log(thrownError);
                    }
                });

                //tfoot
                contentfoot = "";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=5  style='text-align:right; background-color: #A3A6AD; color:white'>Total Boxes	";
                contentfoot += '<td id="totalBoxes" style="text-align: right">0</td>';
                contentfoot += "<td style='text-align:right; background-color: #A3A6AD; color:white'>Total Sales	";
                contentfoot += '<td id="totalSales" style="text-align: right"> 0</td>';
                contentfoot += "</tr>";
                $("#tblSettlement2>tfoot").empty().append(contentfoot);

                //Calculos de la liquidación tblSettlement2
                var totalSettlement = 0;
                var ExchangeRate = $('#txtExchangeRate').val();

                $('#tblSettlement2>tbody>tr').each(function (j, row) {
                    $('#tblDetails2>tbody>tr').each(function (i, e) {
                        if (i == j) {

                            var saleValueSettlement = $(e).closest("tr").find('#saleValueSettlement').val();
                            var settlementExpenseValue = $(e).closest("tr").find('#settlementExpenseValue').val();
                            var total = saleValueSettlement - settlementExpenseValue;
                            $(row).closest("tr").find('#total').val(total.toFixed(2));
                            var quantityBoxes = $(row).closest("tr").find('#quantityBoxes').val();
                            var pricePerBoxSettlementNetUSD = total / quantityBoxes;
                            $(row).closest("tr").find('#pricePerBoxSettlementNetUSD').val(pricePerBoxSettlementNetUSD.toFixed(2));

                        }
                    })
                })

                //calculat el tfoot tblSettlement2
                calcularTotalSettlementDetail2();

                //Recorriendo la lista para llenar el detalle de la tabla tblSettlement3
                var api = "/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID;
                $.ajax({
                    type: "GET",
                    url: api,
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {
                            var GetGrowerSettlementDetailByID = JSON.parse(data);
                            var content3 = ""
                            content3 += "<tr style='font-size: 13px'>";
                            content3 += "<td>QC report/inspections</td>";
                            content3 += '<td ></td>';
                            content3 += '<td> <input style = "text-align: right;" onblur="CalcularTotalSettlementDetail3()" id = "qc" value ="' + (GetGrowerSettlementDetailByID.qc) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                            content3 += "</tr>";

                            content3 += "<tr style='font-size: 13px'>";
                            content3 += "<td>Insurance</td>";
                            content3 += '<td  ></td>';
                            content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "insurance" value ="' + (GetGrowerSettlementDetailByID.insurance) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                            content3 += "</tr>";

                            content3 += "<tr style='font-size: 13px'>";
                            content3 += "<td>Others</td>";
                            content3 += '<td  ></td>';
                            content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "others" value ="' + (GetGrowerSettlementDetailByID.others) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                            content3 += "</tr>";

                            content3 += "<tr style='font-size: 13px'>";
                            content3 += "<td>Commission (%)</td>";
                            content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "comissionPercent" value ="' + (GetGrowerSettlementDetailByID.comissionPercent) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                            content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "comission" value ="' + (GetGrowerSettlementDetailByID.comission) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                            content3 += "</tr>";

                            content3 += "<tr style='font-size: 13px'>";
                            content3 += "<td>Promotion fee (%)</td>";
                            content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "promotionFeePercent" value ="' + (GetGrowerSettlementDetailByID.promotionFeePercent) + '" class=" form-control form-control-sm" type="number"></input></td>';
                            content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "promotionFee" value ="' + (GetGrowerSettlementDetailByID.promotionFee) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                            content3 += "</tr>";

                            content3 += "<tr style='font-size: 13px'>";
                            content3 += "<td colspan=2  style='text-align:right; background-color: #A3A6AD; color:white'>Total Expenses</td>";
                            content3 += '<td style = "text-align: right;" id="totalExpenses">0</td>';
                            content3 += "</tr>";

                            $("#tblSettlement3>tbody").empty().append(content3);

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $.notify("Problemas con el cargar el listado!", "error");
                        console.log(api)
                        console.log(xhr);
                        console.log(ajaxOptions);
                        console.log(thrownError);
                    }
                });

                //   

                CalcularTotalSettlementDetail3();


            } else {
                $('table#tblSettlement>tbody').empty();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });
}

$(document).on("click", "#btnEditSettlement", function () {
    $(this).closest("tr").find('#reference').prop('readonly', false);
})

$("#tblSettlement").on('change', "input[type='radio'][name='checkSeleccionado']", function () {
    var index = $(this).closest("tr").index();
    var growerSettlementID = $(this).closest("tr").find('#growerSettlementID').val();
    var checkSeleccionado = ($(this).closest("tr").find('#checkSeleccionado').is(":checked") == true) ? 1 : 0;

    if (checkSeleccionado == 1) {
        var content = "";

        //Recorriendo la lista para llenar el detalle de la tabla tblSettlement1
        var api = "/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID;
        $.ajax({
            type: "GET",
            url: api,
            async: false,
            success: function (data) {
                if (data.length > 2) {
                    var dataGrowerSettlement = JSON.parse(data);
                    var content = "";
                    $.each(dataGrowerSettlement.details, function (index, row) {
                        content += "    <tr style='font-size: 14px'>";
                        content += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementDetailID" value ="' + (row.growerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                        content += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementID" value ="' + (row.growerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                        content += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                        content += '      <td> <input style = "text-align: right;"  id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;"  id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                        content += '      <td> <input style = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;" id = "pricePerBox" value ="' + (row.pricePerBox) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                        content += '      <td> <input style = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;" id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;" id = "pricePerBoxSettlement" value ="' + (row.pricePerBoxSettlement) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;" id = "pricePerBoxSettlementUSD" value ="' + (row.pricePerBoxSettlementUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;" id = "totalSettlement" value ="' + (row.totalSettlement) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
                        content += '      <td> <input style = "text-align: right;" id = "totalSettlementUSD" value ="' + (row.totalSettlementUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';

                        content += "    </tr>";
                    });
                    $("#tblSettlement1>tbody").empty().append(content);

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.notify("Problemas con el cargar el listado!", "error");
                console.log(api)
                console.log(xhr);
                console.log(ajaxOptions);
                console.log(thrownError);
            }
        });

        contentfoot = "";
        contentfoot += "<tr style='font-size: 14px'>";
        contentfoot += "<td colspan=8  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
        contentfoot += '<td id="totalSettlement" style="text-align: right">0</td>';
        contentfoot += '<td id="totalSettlementUSD" style="text-align: right"> 0</td>';
        contentfoot += "</tr>";
        contentfoot += "<tr style='font-size: 14px'>";
        contentfoot += "<td colspan=8  style='text-align:right; background-color: #A3A6AD; color:white'>Expenses";
        contentfoot += '<td id="expenses" style="text-align: right">0</td>';
        contentfoot += '<td id="expensesUSD" style="text-align: right"> 0</td>';
        contentfoot += "</tr>";
        contentfoot += "<tr style='font-size: 14px'>";
        contentfoot += "<td colspan=8  style='text-align:right; background-color: #A3A6AD; color:white'>Net";
        contentfoot += '<td id="net" style="text-align: right">0</td>';
        contentfoot += '<td id="netUSD" style="text-align: right">0</td>';
        contentfoot += "</tr>";
        $("#tblSettlement1>tfoot").empty().append(contentfoot);

        //Calculos de la liquidación tblSettlement1
        var totalSettlement = 0;
        var ExchangeRate = $('#txtExchangeRate').val();

        $('#tblSettlement1>tbody>tr').each(function (j, row) {
            $('#tblDetails>tbody>tr').each(function (i, e) {
                if (i == j) {

                    totalSettlement = $(e).closest("tr").find('#totalSettlement').val();
                    $(row).closest("tr").find('#totalSettlement').val(parseFloat(totalSettlement).toFixed(2));
                    if (_flagcurrency == 1) { $(row).closest("tr").find('#totalSettlementUSD').val((totalSettlement * ExchangeRate).toFixed(2)); }
                    else { $(row).closest("tr").find('#totalSettlementUSD').val((totalSettlement / ExchangeRate).toFixed(2)); }


                    pricePerBoxSettlementUSD = ($(row).closest("tr").find('#quantityBoxes').val()) / ($(row).closest("tr").find('#totalSettlementUSD').val());
                    $(row).closest("tr").find('#pricePerBoxSettlementUSD').val(pricePerBoxSettlementUSD.toFixed(2));
                    pricePerBoxSettlement = ($(row).closest("tr").find('#quantityBoxes').val()) / ($(row).closest("tr").find('#totalSettlement').val());
                    $(row).closest("tr").find('#pricePerBoxSettlement').val(pricePerBoxSettlement.toFixed(2));
                }
            })
        })
        //calculat el tfoot tblSettlement1
        calcularTotalSettlementDetail();

        //Recorriendo la lista para llenar el detalle de la tabla tblSettlement2
        content2 = ""

        var api = "/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID;
        $.ajax({
            type: "GET",
            url: api,
            async: false,
            success: function (data) {
                if (data.length > 2) {
                    var dataGrowerSettlement = JSON.parse(data);
                    var content2 = "";
                    $.each(dataGrowerSettlement.details, function (index, row) {
                        content2 += "    <tr style='font-size: 14px'>";
                        content2 += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementDetailID" value ="' + (row.growerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                        content2 += '      <td  style = "display:none"> <input onblur = " " id = "growerSettlementID" value ="' + (row.growerSettlementID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                        content2 += '      <td  style = "display:none"> <input onblur = " " id = "customerSettlementDetailID" value ="' + (row.customerSettlementDetailID) + '" class=" form-control form-control-sm" type="text"></input></td>';
                        content2 += '      <td> <input style = "text-align: right;"  id = "brand" value ="' + (row.brand) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                        content2 += '      <td> <input style = "text-align: right;"  id = "variety" value ="' + (row.variety) + '" class=" form-control form-control-sm"  type="text" readonly></input></td>';
                        content2 += '      <td> <input style = "" id = "description" value ="' + (row.description) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                        content2 += '      <td> <input style = "text-align: right;" id = "pricePerBox" value ="' + (row.pricePerBox) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                        content2 += '      <td> <input style = "" id = "sizeInfo" value ="' + (row.sizeInfo) + '" class=" form-control form-control-sm" type="text" readonly></input></td>';
                        content2 += '      <td> <input style = "text-align: right;" id = "quantityBoxes" value ="' + (row.quantityBoxes) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                        content2 += '      <td> <input style = "text-align: right;" id = "pricePerBoxSettlementNetUSD" value ="' + (row.pricePerBoxSettlementNetUSD) + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                        content2 += '      <td> <input style = "text-align: right;" id = "total" value ="' + (row.total) + '" class=" form-control form-control-sm"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';

                    });
                    $("#tblSettlement2>tbody").empty().append(content2);

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.notify("Problemas con el cargar el listado!", "error");
                console.log(api)
                console.log(xhr);
                console.log(ajaxOptions);
                console.log(thrownError);
            }
        });

        //tfoot
        contentfoot = "";
        contentfoot += "<tr style='font-size: 14px'>";
        contentfoot += "<td colspan=5  style='text-align:right; background-color: #A3A6AD; color:white'>Total Boxes	";
        contentfoot += '<td id="totalBoxes" style="text-align: right">0</td>';
        contentfoot += "<td style='text-align:right; background-color: #A3A6AD; color:white'>Total Sales	";
        contentfoot += '<td id="totalSales" style="text-align: right"> 0</td>';
        contentfoot += "</tr>";
        $("#tblSettlement2>tfoot").empty().append(contentfoot);

        //Calculos de la liquidación tblSettlement2
        var totalSettlement = 0;
        var ExchangeRate = $('#txtExchangeRate').val();

        $('#tblSettlement2>tbody>tr').each(function (j, row) {
            $('#tblDetails2>tbody>tr').each(function (i, e) {
                if (i == j) {

                    var saleValueSettlement = $(e).closest("tr").find('#saleValueSettlement').val();
                    var settlementExpenseValue = $(e).closest("tr").find('#settlementExpenseValue').val();
                    var total = saleValueSettlement - settlementExpenseValue;
                    $(row).closest("tr").find('#total').val(total.toFixed(2));
                    var quantityBoxes = $(row).closest("tr").find('#quantityBoxes').val();
                    var pricePerBoxSettlementNetUSD = total / quantityBoxes;
                    $(row).closest("tr").find('#pricePerBoxSettlementNetUSD').val(pricePerBoxSettlementNetUSD.toFixed(2));

                }
            })
        })

        //calculat el tfoot tblSettlement2
        calcularTotalSettlementDetail2();

        //Recorriendo la lista para llenar el detalle de la tabla tblSettlement3
        var api = "/Finance/GetGrowerSettlementDetailByID?growerSettlementID=" + growerSettlementID;
        $.ajax({
            type: "GET",
            url: api,
            async: false,
            success: function (data) {
                if (data.length > 2) {
                    var GetGrowerSettlementDetailByID = JSON.parse(data);
                    var content3 = ""
                    content3 += "<tr style='font-size: 13px'>";
                    content3 += "<td>QC report/inspections</td>";
                    content3 += '<td ></td>';
                    content3 += '<td> <input style = "text-align: right;" onblur="CalcularTotalSettlementDetail3()" id = "qc" value ="' + (GetGrowerSettlementDetailByID.qc) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                    content3 += "</tr>";

                    content3 += "<tr style='font-size: 13px'>";
                    content3 += "<td>Insurance</td>";
                    content3 += '<td  ></td>';
                    content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "insurance" value ="' + (GetGrowerSettlementDetailByID.insurance) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                    content3 += "</tr>";

                    content3 += "<tr style='font-size: 13px'>";
                    content3 += "<td>Others</td>";
                    content3 += '<td  ></td>';
                    content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "others" value ="' + (GetGrowerSettlementDetailByID.others) + '" class=" form-control form-control-sm" type="number" ></input></td>';
                    content3 += "</tr>";

                    content3 += "<tr style='font-size: 13px'>";
                    content3 += "<td>Commission</td>";
                    content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "comissionPercent" value ="' + (GetGrowerSettlementDetailByID.comissionPercent) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "comission" value ="' + (GetGrowerSettlementDetailByID.comission) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content3 += "</tr>";

                    content3 += "<tr style='font-size: 13px'>";
                    content3 += "<td>Promotion fee</td>";
                    content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "promotionFeePercent" value ="' + (GetGrowerSettlementDetailByID.promotionFeePercent) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content3 += '<td> <input style = "text-align: right;"  onblur="CalcularTotalSettlementDetail3()" id = "promotionFee" value ="' + (GetGrowerSettlementDetailByID.promotionFee) + '" class=" form-control form-control-sm" type="number" readonly></input></td>';
                    content3 += "</tr>";

                    content3 += "<tr style='font-size: 13px'>";
                    content3 += "<td colspan=2  style='text-align:right; background-color: #A3A6AD; color:white'>Total Expenses</td>";
                    content3 += '<td style = "text-align: right;" id="totalExpenses">0</td>';
                    content3 += "</tr>";

                    $("#tblSettlement3>tbody").empty().append(content3);

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.notify("Problemas con el cargar el listado!", "error");
                console.log(api)
                console.log(xhr);
                console.log(ajaxOptions);
                console.log(thrownError);
            }
        });

        CalcularTotalSettlementDetail3();
    }

});

function CalcularTotalSettlementDetail3() {
    var totalBoxes = 0;
    var totalSales = 0;
    var promotionFeePercent = 0;
    var promotionFee = 0;
    var comission = 0;
    var qc = 0;
    var insurance = 0;
    var others = 0;
    var comission = 0;
    var promotionFee = 0;
    var totalExpenses = 0;
    $('#tblSettlement3>tbody>tr').each(function (j, row) {
        if (j == 4) {
            promotionFeePercent = ($(row).find('#promotionFeePercent').val()) / 100;
        }
        if (j == 3) {
            comissionPercent = ($(row).find('#comissionPercent').val()) / 100;
        }
    })


    $('table#tblSettlement2>tbody>tr').each(function (j, row) {
        totalSales = parseFloat($(row).find('#total').val()) + totalSales;
    })

    comission = comissionPercent * totalSales;
    promotionFee = promotionFeePercent * totalSales;
    //Calcular el Total Expense
    $('table#tblSettlement3>tbody>tr').each(function (j, row) {


        if (j == 0) {
            qc = parseFloat($(row).find('#qc').val());
        }
        if (j == 1) {
            insurance = parseFloat($(row).find('#insurance').val());
        }
        if (j == 2) {
            others = parseFloat($(row).find('#others').val());
        }
        if (j == 3) {
            $(row).find('#comission').val(comission.toFixed(2));
            comission = parseFloat($(row).find('#comission').val());
        }
        if (j == 4) {
            $(row).find('#promotionFee').val(promotionFee.toFixed(2));
            promotionFee = parseFloat($(row).find('#promotionFee').val());
        }
        if (j == 5) {
            totalExpenses = (qc + insurance + others + comission + promotionFee);
            $(row).find('#totalExpenses').text(totalExpenses.toFixed(2));
        }
    })

}


function calcularTotalSettlementDetail() {
    var totalSettlement = 0;
    var totalSettlementUSD = 0;
    var expenses = 0;
    var expensesUSD = 0;
    var net = 0;
    var netUSD = 0;
    $('#tblSettlement1>tbody>tr').each(function (j, row) {

        //TotalMonedaLocal
        totalSettlement = parseFloat($(row).closest("tr").find('#totalSettlement').val()) + totalSettlement;
        //totalMonedaUSD
        totalSettlementUSD = parseFloat($(row).closest("tr").find('#totalSettlementUSD').val()) + parseFloat(totalSettlementUSD);
    })
    $('#tblGastos>tbody').each(function (j, row) {

        //Total Comission
        var CM = parseFloat($(row).find('tr>td').find('#CM').val());
        //Total Fijo
        var TM = parseFloat($(row).find('tr>td').find('#TM').val());
        expenses = CM + TM;

    })

    $('#tblSettlement1>tfoot>tr').each(function (j, row) {
        //Ingresando valores calculados
        $(row).closest("tr").find('#totalSettlement').text(totalSettlement);
        $(row).closest("tr").find('#totalSettlementUSD').text(totalSettlementUSD);
        $(row).closest("tr").find('#expenses').text(expenses);
        //tasa cambio
        var ExchangeRate = $('#txtExchangeRate').val();
        if (_flagcurrency == 1) { expensesUSD = expenses * ExchangeRate; }
        else { expensesUSD = expenses / ExchangeRate; }
        $(row).closest("tr").find('#expensesUSD').text(expensesUSD);
        //NetoMonedalocal
        net = parseFloat(totalSettlement - expenses);
        netUSD = parseFloat(totalSettlementUSD - expensesUSD);

        $(row).closest("tr").find('#net').text(net);
        $(row).closest("tr").find('#netUSD').text(netUSD);
    })

    //Calcular el tfoot tblSettlement2
    calcularTotalSettlementDetail2()

}


function calcularTotalSettlementDetail2() {
    var totalBoxes = 0;
    var totalSales = 0;

    $('table#tblSettlement2>tbody>tr').each(function (j, row) {
        totalBoxes = parseInt($(row).find('#quantityBoxes').val()) + totalBoxes;
        totalSales = parseFloat($(row).find('#total').val()) + totalSales;
    })

    $('table#tblSettlement2>tfoot>tr').each(function (j, row) {
        //Ingresando valores calculados
        $(row).find('#totalBoxes').text(totalBoxes);
        $(row).find('#totalSales').text(totalSales);
    })

}

function RecalcularPorcentaje(xthis) {
    var porcentaje = parseFloat($(xthis).closest("tr").find('#CM').val());
    var ExchangeRate = 1
    var monedaInicialVal = $("#selectCurrency option:selected").val();
    if (monedaInicialVal != 1) //si no es dolar seteamos el tipo de caambio con el valor
    {
        ExchangeRate = parseFloat($('#txtExchangeRate').val());
    }
    //Seteamos el valor del procentaje en dolares
    if (_flagcurrency == 1) { $(xthis).closest("tr").find('#CD').val((porcentaje * ExchangeRate).toFixed(2)); }
    else { $(xthis).closest("tr").find('#CD').val((porcentaje / ExchangeRate).toFixed(2)); }


    var totalboxes = 0;
    var totalSettlement = 0;
    var expenseFixed = 0;
    var expenseVariable = 0;
    var netSettlemnt = 0;
    var content = '';

    var Fixed_Expends = 0;
    var Fixed_Dolar = 0;
    var Variable_Expends = 0;
    var Variable_ExpendsDolar = 0;
    var TotalMonedaActualresta = 0;
    var TotalMonedaDolarresta = 0;
    var totalkgbox = 0;

    $('#tblGastos>tbody>tr').each(function (i, e) {
        if (i == 0) {
            Fixed_Expends = parseFloat($(e).closest('tr').find("#CM").val())
            Fixed_Dolar = parseFloat($(e).closest('tr').find("#CD").val())
        }

        if ($(e).closest('tr').find("input:checkbox[name='checkcost']").is(":checked")) {

            if (i == 1) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#PFM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#PFD").val());

            }
            if (i == 2) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#RTM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#RTD").val());

            }
            if (i == 3) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#OFM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#OFD").val());

            }
            if (i == 4) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#CSM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#CSD").val());

            }
            if (i == 5) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#CTM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#CTD").val());

            }
            if (i == 6) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#IDM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#IDD").val());

            }
            if (i == 7) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#EDM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#EDD").val());

            }
            if (i == 8) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#TCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#TCD").val());

            }
            if (i == 9) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#HNM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#HND").val());

            }
            if (i == 10) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#GIM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#GID").val());
            }
            if (i == 11) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#QCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#QCD").val());
            }
            if (i == 12) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#ISM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#ISD").val());
            }
            if (i == 13) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#RCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#RCD").val());
            }
            if (i == 14) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#OCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#OCD").val());
            }

        }

    })

    $('#tblGastos>tbody').each(function (i, e) {
        TotalMonedaActualresta =
            parseFloat($(e).find('tr>td').find('#PFM').val()) + parseFloat($(e).find('tr>td').find('#RTM').val()) + parseFloat($(e).find('tr>td').find('#OFM').val()) + parseFloat($(e).find('tr>td').find('#CSM').val()) + parseFloat($(e).find('tr>td').find('#CTM').val()) +
            parseFloat($(e).find('tr>td').find('#IDM').val()) + parseFloat($(e).find('tr>td').find('#EDM').val()) + parseFloat($(e).find('tr>td').find('#TCM').val()) + parseFloat($(e).find('tr>td').find('#HNM').val()) + parseFloat($(e).find('tr>td').find('#GIM').val()) +
            parseFloat($(e).find('tr>td').find('#QCM').val()) + parseFloat($(e).find('tr>td').find('#ISM').val()) + parseFloat($(e).find('tr>td').find('#RCM').val()) + parseFloat($(e).find('tr>td').find('#OCM').val());
        TotalMonedaDolarresta =
            parseFloat($(e).find('tr>td').find('#PFD').val()) + parseFloat($(e).find('tr>td').find('#RTD').val()) + parseFloat($(e).find('tr>td').find('#OFD').val()) + parseFloat($(e).find('tr>td').find('#CSD').val()) + parseFloat($(e).find('tr>td').find('#CTD').val()) +
            parseFloat($(e).find('tr>td').find('#IDD').val()) + parseFloat($(e).find('tr>td').find('#EDD').val()) + parseFloat($(e).find('tr>td').find('#TCD').val()) + parseFloat($(e).find('tr>td').find('#HND').val()) + parseFloat($(e).find('tr>td').find('#GID').val()) +
            parseFloat($(e).find('tr>td').find('#QCD').val()) + parseFloat($(e).find('tr>td').find('#ISD').val()) + parseFloat($(e).find('tr>td').find('#RCD').val()) + parseFloat($(e).find('tr>td').find('#OCD').val());
        //Seteamos el footer
        TotalMonedaActualresta = TotalMonedaActualresta - Variable_Expends;
        TotalMonedaDolarresta = TotalMonedaDolarresta - Variable_ExpendsDolar;

        $(e).find('tr>td').find('#TMTOTAL').val((TotalMonedaActualresta + Fixed_Expends + Variable_Expends).toFixed(2));
        $(e).find('tr>td').find('#TM').val((TotalMonedaActualresta).toFixed(2));
        $(e).find('tr>td').find('#TD').val((TotalMonedaDolarresta + Fixed_Dolar + Variable_ExpendsDolar).toFixed(2));
    });

    $('#tblDetails>tbody>tr').each(function (i, e) {
        totalkgbox = parseFloat($(e).find('#kgCaja').val()) + totalkgbox;
        totalboxes = parseFloat($(e).find('#quantityBoxes').val()) + totalboxes;
        totalSettlement = parseFloat($(e).find('#totalSettlement').val()) + totalSettlement;
        expenseFixed = parseFloat($(e).find('#expenseFixed').val()) + expenseFixed;
        expenseVariable = parseFloat($(e).find('#expenseVariable').val()) + expenseVariable;
        netSettlemnt = parseFloat($(e).find('#netSettlemnt').val()) + netSettlemnt;
    })
    content += "<tr>";
    content += '<td id="totalkgbox" style="text-align: right"></td>';
    content += '<td id="totalQuantityB" style="text-align: right">' + (totalboxes).toFixed(2) + '</td>';
    content += "<td colspan=5  style = 'text-align: right;'>";

    content += '<td id="totalSettlement" style="text-align: right">' + (totalSettlement).toFixed(2) + '</td>';

    if (TotalMonedaActualresta > 0) {
        expenseFixed = TotalMonedaActualresta;
    }

    content += '<td id="totalExpenseFixed" style="text-align: right"> ' + (expenseFixed).toFixed(2) + '</td>';
    content += '<td id="totalExpenseVariable" style="text-align: right"> ' + (Variable_Expends + porcentaje).toFixed(2) + '</td>';
    content += '<td id="totalNetSettlemnt" style="text-align: right"> ' + (netSettlemnt).toFixed(2) + '</td>';
    content += "</tr>";
    $("#tblDetails>tfoot").empty().append(content);

}

function calcularInvoiceFOB(xthis) {

    var freight = parseInt($('#txtUserFreightD').val());
    var insurance = parseInt($('#txtInsuraceD').val());
    var tot = freight + insurance;

    var qboxes = 0;
    var quantity = 0;
    var weight = 0;
    var peso = 0;
    var amount = 0;
    var kgtot = 0;
    var totalfs = 0;
    var pricefob = 0;
    var amountfob = 0;
    let content = '';
    $('#tblDocuments1>tbody>tr').each(function (i, e) {
        quantity = parseFloat($(e).find('#quantityBoxes').text());
        peso = parseFloat($(e).find('#weight').text());
        kgtot = (quantity * peso) + kgtot;
    })

    $('#tblDocuments1>tbody>tr').each(function (i, e) {
        qboxes = parseFloat($(e).find('#quantityBoxes').text());
        weight = parseFloat($(e).find('#weight').text());
        amount = parseFloat($(e).find('#inputTotal').text());
        totalfs = (tot * qboxes * weight) / kgtot;
        amountfob = amount - totalfs;
        pricefob = amountfob / qboxes;
        $(e).closest("tr").find('#priceBoxFOB').text(pricefob.toFixed(2));
        $(e).closest("tr").find('#totalFOB').text(amountfob.toFixed(2));
    })

    var ftotal = 0;
    var ftotalfob = 0;
    $('#tblDocuments1>tbody>tr').each(function (i, e) {
        ftotal = parseFloat($(e).find('#inputTotal').text()) + ftotal;
        ftotalfob = parseFloat($(e).find('#totalFOB').text()) + ftotalfob;
    })
    content += "<tr>";
    content += "<td bgcolor='#1c0b2a' style='color: white; font - size: 13px; text - align: right' colspan='6'>" + 'Total'+ "</td>";
    content += '<td id="inputTotalFooter" style="text-align: left"> ' + (ftotal).toFixed(2) + '</td>';
    content += '<td ></td>';
    content += '<td id="totalFooterFOB" style="text-align: left"> ' + (ftotalfob).toFixed(2) + '</td>';
    content += "</tr>";
    $("#tblDocuments1>tfoot").empty().append(content);
}

function calcular(xthis) {
    var monedaInicialVal = $("#selectCurrency option:selected").val();
    var tipocambio = 1;
    if (monedaInicialVal != 1) //si no es dolar seteamos el tipo de caambio con el valor
    {
        tipocambio = parseFloat($('#txtExchangeRate').val());
    }

    //validamos que al cargar si encuentra el porcentaje igua a cero no muestra la validación de lo contrario pasa al flujo completo
    var Porcentajeval = 0;
    $('#tblGastos>tbody').each(function (i, e) {

        Porcentajeval = parseFloat($(e).find('tr>td').find('#Porcentaje').val() == "" ? 0 : $(e).find('tr>td').find('#Porcentaje').val());
    })

    if (Porcentajeval == 0) { return false; }


    if (!PrimeraCarga) {
        if (parseInt($('#selectCurrency option:selected').val()) == 0) {
            if (xthis != null || xthis != undefined) $(xthis).closest("tr").find('#totalSettlement').val('0');
            sweet_alert_error('Error', 'Choose Currency.');

            return false;
        }
        if ($('#txtExchangeRate').val() == '' || parseFloat($('#txtExchangeRate').val()) <= 0) {
            if (xthis != null || xthis != undefined) $(xthis).closest("tr").find('#totalSettlement').val('0');
            sweet_alert_error('Error', 'Enter Exchange Rate.');
            return false;
        }
        if ($("#Porcentaje").val() == '' || parseFloat($("#Porcentaje").val()) <= 0) {
            $("#Porcentaje").val('');
            if (xthis != null || xthis != undefined) $(xthis).closest("tr").find('#totalSettlement').val('0');
            sweet_alert_error('Error', 'Enter Comission Rate.');
            return false;
        }
    }

    //>>>Cálculo Pestaña 1
    var Total = 0;
    var quantityBoxes = 0;
    var PorcentajeComision = 0;
    var TotalGastos = 0;
    var Neto = 0;
    var totalquantityBoxes = 0;
    var vexpenseFixed = 0;
    var vexpenseVariable = 0;
    _vexpenseFixed = 0;
    _vexpenseVariable = 0;
    var contador = 0;
    var totalvariableporLine = 0;
    var totalSetelement = 0;
    var totalSetelementVariable = 0;


    $('#tblDetails>tbody>tr').each(function (i, e) {
        totalquantityBoxes = parseFloat($(e).find('#quantityBoxes').val()) + totalquantityBoxes;
        if (parseFloat($(e).find('#totalSettlement').val()) > 0) { contador = contador + 1; }
    });

    //Obtenemos el total de la liquidadción y el gasto variable en base al porcentaje
    $('#tblDetails>tfoot>tr').each(function (i, e) {
        totalSetelement = parseFloat($(e).find('#totalSettlement').text());
    })

    if (xthis != null || xthis != undefined) {
        //>>>Cálculo Precio x Caja
        Total = parseFloat($(xthis).closest("tr").find('#totalSettlement').val());
        quantityBoxes = parseInt($(xthis).closest("tr").find('#quantityBoxes').val());
        $(xthis).closest("tr").find('#pricePerBox').val((Total / quantityBoxes).toFixed(2));
        //<<<Cálculo Precio x Caja

        //>>>Cálculo Costo Fijo y Gasto Variable
        PorcentajeComision = 0;
        TotalGastos = 0;
        $('#tblGastos>tbody').each(function (i, e) {
            if (i == 0) {
                PorcentajeComision = parseFloat($(e).find('tr').find('#Porcentaje').val());
            }
            TotalGastos = parseFloat($(e).find('tr>td').find('#TM').val() == "" ? 0 : $(e).find('tr>td').find('#TM').val());
        })

        vexpenseFixed = vexpenseFixed + (TotalGastos * (quantityBoxes / totalquantityBoxes));
        vexpenseVariable = vexpenseVariable + (Total * (PorcentajeComision / 100));
        //Calculamos el total de la comisión(Este seria el gasto variable en base al porcentaje)
        totalSetelementVariable = parseFloat(totalSetelement * (PorcentajeComision / 100))

        if (Total >= 0) {
            totalvariableporLine = parseFloat(Total * (PorcentajeComision / 100))
            totalvariableporLine = totalvariableporLine + parseFloat(_Variable_Expends / contador)
            $('#tblDetails>tbody>tr').each(function (i, e) {
                //>>>Cálculo Precio x Caja
                var totalvariableporLine2 = 0;
                Total2 = parseFloat($(e).closest("tr").find('#totalSettlement').val());

                if (Total2 > 0) {
                    totalvariableporLine2 = parseFloat((parseFloat(totalSetelementVariable + _Variable_Expends) / totalSetelement) * Total2);
                }
                $(e).closest("tr").find('#expenseVariable').val(totalvariableporLine2.toFixed(2));
                //<<<Cálculo Costo Fijo y Gasto Variable

            });
        }

        $(xthis).closest("tr").find('#expenseFixed').val((TotalGastos * (quantityBoxes / totalquantityBoxes)).toFixed(2));
        //<<<Cálculo Costo Fijo y Gasto Variable

        //>>>Cálculo Neto
        Neto = parseFloat($(xthis).closest("tr").find('#totalSettlement').val()) - parseFloat($(xthis).closest("tr").find('#expenseFixed').val()) - parseFloat($(xthis).closest("tr").find('#expenseVariable').val());
        $(xthis).closest("tr").find('#netSettlemnt').val((Neto > 0) ? Neto.toFixed(2) : 0.00);
        //<<<Cálculo Neto

    } else {
        $('#tblDetails>tbody>tr').each(function (i, e) {
            //>>>Cálculo Precio x Caja
            totalvariableporLine = 0;
            Total = parseFloat($(e).closest("tr").find('#totalSettlement').val());
            quantityBoxes = parseInt($(e).closest("tr").find('#quantityBoxes').val());
            $(e).closest("tr").find('#pricePerBox').val((Total / quantityBoxes).toFixed(2));
            //<<<Cálculo Precio x Caja

            //>>>Cálculo Costo Fijo y Gasto Variable
            PorcentajeComision = 0;
            TotalGastos = 0;
            $('#tblGastos>tbody').each(function (i, e) {
                if (i == 0) {
                    PorcentajeComision = parseFloat($(e).find('tr').find('#Porcentaje').val());
                }
                TotalGastos = parseFloat($(e).find('tr>td').find('#TM').val() == "" ? 0 : $(e).find('tr>td').find('#TM').val());
            })

            //Calculamos el total de la comisión(Este seria el gasto variable en base al porcentaje)
            totalSetelementVariable = parseFloat(totalSetelement * (PorcentajeComision / 100))

            vexpenseFixed = vexpenseFixed + (TotalGastos * (quantityBoxes / totalquantityBoxes));
            vexpenseVariable = vexpenseVariable + (Total * (PorcentajeComision / 100))

            if (Total > 0) {
                totalvariableporLine = parseFloat((parseFloat(totalSetelementVariable + _Variable_Expends) / totalSetelement) * Total);
            }

            $(e).closest("tr").find('#expenseFixed').val((TotalGastos * (quantityBoxes / totalquantityBoxes)).toFixed(2));
            $(e).closest("tr").find('#expenseVariable').val(totalvariableporLine.toFixed(2));
            //<<<Cálculo Costo Fijo y Gasto Variable

            //>>>Cálculo Neto
            Neto = parseFloat($(e).closest("tr").find('#totalSettlement').val()) - parseFloat($(e).closest("tr").find('#expenseFixed').val()) - parseFloat($(e).closest("tr").find('#expenseVariable').val());
            $(e).closest("tr").find('#netSettlemnt').val((Neto > 0) ? Neto.toFixed(2) : 0.00);
            //<<<Cálculo Neto
        });
    }
    _vexpenseFixed = vexpenseFixed;
    _vexpenseVariable = vexpenseVariable;

    calcularTotalSettlement();//calculo del footer    

    //>>>Mostrar total gastos variables en total de comisión
    var TotalexpenseVariable = 0;
    $('#tblDetails>tfoot>tr').each(function (i, e) {
        TotalexpenseVariable = parseFloat($(e).find('#totalExpenseVariable').text());
    })
    $('#tblGastos>tbody').each(function (i, e) {
        $(e).find('tr>td').find('#CM').val(TotalexpenseVariable - _Variable_Expends);
        if (_flagcurrency == 1) {
            $(e).find('tr>td').find('#CD').val((parseFloat($(e).find('tr>td').find('#CM').val()) * parseFloat(tipocambio)).toFixed(2));
        } else {
            $(e).find('tr>td').find('#CD').val((parseFloat($(e).find('tr>td').find('#CM').val()) / parseFloat(tipocambio)).toFixed(2));
        }

    });

    //<<<Mostrar total gastos variables en total de comisión
    //<<<Cálculo Pestaña 1

    var totalBill = 0;
    var saleValueSettlement = 0;
    var settlementExpenseValue = 0;
    var DN_CN = 0;
    var quantityBoxes = 0;
    var kgxbox = 0;
    var KgBoxes = 0;
    if (xthis != null || xthis != undefined) {
        //Calcular valor venta Liquidación
        if (_flagcurrency == 1) {
            saleValueSettlement = parseFloat($(xthis).closest("tr").find('#totalSettlement').val()) * parseFloat(tipocambio);
            settlementExpenseValue = (parseFloat($(xthis).closest("tr").find('#expenseFixed').val()) + parseFloat($(xthis).closest("tr").find('#expenseVariable').val())) * parseFloat(tipocambio);
        } else {
            saleValueSettlement = parseFloat($(xthis).closest("tr").find('#totalSettlement').val()) / parseFloat(tipocambio);
            settlementExpenseValue = (parseFloat($(xthis).closest("tr").find('#expenseFixed').val()) + parseFloat($(xthis).closest("tr").find('#expenseVariable').val())) / parseFloat(tipocambio);
        }


        //Calcular NC/ND
        $('#tblDetails2>tbody>tr').each(function (i, e) {
            var index = $(xthis).closest("tr").index();
            quantityBoxes = parseFloat($(e).closest("tr").find('#quantityBoxes').val());
            if (i == index) {
                $(e).closest("tr").find('#saleValueSettlement').val(saleValueSettlement.toFixed(2));
                $(e).closest("tr").find('#settlementExpenseValue').val(settlementExpenseValue.toFixed(2));
                $(e).closest("tr").find('#settlementFinalBalance').val((saleValueSettlement - settlementExpenseValue).toFixed(2));
                kgxbox = ((saleValueSettlement - settlementExpenseValue) / quantityBoxes);
                $(e).closest("tr").find('#setfinalPrice').val(kgxbox.toFixed(2));
                $(e).closest("tr").find('#setfinalPriceXKG').val(parseFloat(0).toFixed(2));
                //Calcular NotaCredito/NotaDebito
                DN_CN = parseFloat($(e).closest("tr").find('#saleValueSettlement').val()) - parseFloat($(e).closest("tr").find('#settlementExpenseValue').val()) - parseFloat($(e).closest("tr").find('#totalBill').val());
                $(e).closest("tr").find('#DN_CN').val(DN_CN.toFixed(2));
            }
        })
    } else {
        $('#tblDetails>tbody>tr').each(function (i, e) {
            if (_flagcurrency == 1) {
                saleValueSettlement = parseFloat($(e).closest("tr").find('#totalSettlement').val()) * parseFloat(tipocambio);
                settlementExpenseValue = (parseFloat($(e).closest("tr").find('#expenseFixed').val()) + parseFloat($(e).closest("tr").find('#expenseVariable').val())) * parseFloat(tipocambio);
            }
            else {
                saleValueSettlement = parseFloat($(e).closest("tr").find('#totalSettlement').val()) / parseFloat(tipocambio);
                settlementExpenseValue = (parseFloat($(e).closest("tr").find('#expenseFixed').val()) + parseFloat($(e).closest("tr").find('#expenseVariable').val())) / parseFloat(tipocambio);
            }

            //Calcular NC/ND
            $('#tblDetails2>tbody>tr').each(function (i, ee) {
                var index = $(e).closest("tr").index();
                quantityBoxes = parseFloat($(ee).closest("tr").find('#quantityBoxes').val());
                KgBoxes = parseFloat($(e).closest("tr").find('#kgCaja').val());

                if (i == index) {
                    $(ee).closest("tr").find('#saleValueSettlement').val(saleValueSettlement.toFixed(2));
                    $(ee).closest("tr").find('#settlementExpenseValue').val(settlementExpenseValue.toFixed(2));
                    $(ee).closest("tr").find('#settlementFinalBalance').val((saleValueSettlement - settlementExpenseValue).toFixed(2));
                    kgxbox = (saleValueSettlement - settlementExpenseValue) / quantityBoxes
                    $(ee).closest("tr").find('#setfinalPrice').val(kgxbox.toFixed(2));
                    $(ee).closest("tr").find('#setfinalPriceXKG').val(parseFloat(kgxbox / KgBoxes).toFixed(2));
                    //Calcular NotaCredito/NotaDebito
                    DN_CN = parseFloat($(ee).closest("tr").find('#saleValueSettlement').val()) - parseFloat($(ee).closest("tr").find('#settlementExpenseValue').val()) - parseFloat($(ee).closest("tr").find('#totalBill').val());
                    $(ee).closest("tr").find('#DN_CN').val(DN_CN.toFixed(2));
                }
            })
        })
    }
    calcularTotalAdjustInvoice();
    //>>>Cálculo Pestaña 2

    //>>>Cálculo Pestaña 3
    var totalToColletUSD = 0;
    var balancePayemntesUSD = 0;
    var advancePaymentsUSD = 0;
    var totalSettlement = 0;
    if (xthis != null || xthis != undefined) {
        if (_flagcurrency == 1) { totalToColletUSD = parseFloat($(xthis).closest("tr").find('#netSettlemnt').val()) * parseFloat(tipocambio); }
        else { totalToColletUSD = parseFloat($(xthis).closest("tr").find('#netSettlemnt').val()) / parseFloat(tipocambio); }


        $('#tblDetails3>tbody>tr').each(function (i, e) {
            var index = $(xthis).closest("tr").index();
            if (i == index) {
                $(e).closest("tr").find('#totalToColletUSD').val(totalToColletUSD.toFixed(2));
                //Calcular balance
                balancePayemntesUSD = parseFloat($(e).closest("tr").find('#totalToColletUSD').val()) - parseFloat($(e).closest("tr").find('#advancePaymentsUSD').val());
                $(e).closest("tr").find('#balancePayemntesUSD').val(balancePayemntesUSD.toFixed(2));
            }
        })
        //Calculos de la liquidación    
        totalSettlement = parseFloat($(xthis).closest("tr").find('#totalSettlement').val());
        if ($('#tblSettlement1>tbody>tr').length > 0) {
            $('#tblSettlement1>tbody>tr').each(function (i, e) {
                var index = $(xthis).closest("tr").index();
                if (i == index) {
                    $(e).closest("tr").find('#totalSettlement').val(totalSettlement.toFixed(2));
                    if (_flagcurrency == 1) { $(e).closest("tr").find('#totalSettlementUSD').val((totalSettlement * parseFloat(tipocambio)).toFixed(2)); }
                    else { $(e).closest("tr").find('#totalSettlementUSD').val((totalSettlement / parseFloat(tipocambio)).toFixed(2)); }


                    pricePerBoxSettlementUSD = parseFloat($(e).closest("tr").find('#quantityBoxes').val()) / parseFloat($(e).closest("tr").find('#totalSettlementUSD').val());
                    $(e).closest("tr").find('#pricePerBoxSettlementUSD').val(pricePerBoxSettlementUSD.toFixed(2));
                    pricePerBoxSettlement = parseFloat($(e).closest("tr").find('#quantityBoxes').val()) / parseFloat($(e).closest("tr").find('#totalSettlement').val());
                    $(e).closest("tr").find('#pricePerBoxSettlement').val(pricePerBoxSettlement.toFixed(2));
                }
            })
        }
    } else {
        $('#tblDetails>tbody>tr').each(function (i, e) {
            if (_flagcurrency == 1) { totalToColletUSD = parseFloat($(e).closest("tr").find('#netSettlemnt').val()) * parseFloat(tipocambio); }
            else { totalToColletUSD = parseFloat($(e).closest("tr").find('#netSettlemnt').val()) / parseFloat(tipocambio); }


            $('#tblDetails3>tbody>tr').each(function (i, ee) {
                var index = $(e).closest("tr").index();
                if (i == index) {
                    $(ee).closest("tr").find('#totalToColletUSD').val(totalToColletUSD.toFixed(2));
                    //Calcular balance
                    balancePayemntesUSD = parseFloat($(ee).closest("tr").find('#totalToColletUSD').val()) - parseFloat($(ee).closest("tr").find('#advancePaymentsUSD').val());
                    $(ee).closest("tr").find('#balancePayemntesUSD').val(balancePayemntesUSD.toFixed(2));
                }
            })
            //Calculos de la liquidación    
            totalSettlement = parseFloat($(e).closest("tr").find('#totalSettlement').val());
            if ($('#tblSettlement1>tbody>tr').length > 0) {
                $('#tblSettlement1>tbody>tr').each(function (i, ee) {
                    var index = $(e).closest("tr").index();
                    if (i == index) {
                        $(ee).closest("tr").find('#totalSettlement').val(totalSettlement.toFixed(2));
                        if (_flagcurrency == 1) { $(ee).closest("tr").find('#totalSettlementUSD').val((totalSettlement * parseFloat(tipocambio)).toFixed(2)); }
                        else { $(ee).closest("tr").find('#totalSettlementUSD').val((totalSettlement / parseFloat(tipocambio)).toFixed(2)); }


                        pricePerBoxSettlementUSD = parseFloat($(ee).closest("tr").find('#quantityBoxes').val()) / parseFloat($(ee).closest("tr").find('#totalSettlementUSD').val());
                        $(ee).closest("tr").find('#pricePerBoxSettlementUSD').val(pricePerBoxSettlementUSD.toFixed(2));
                        pricePerBoxSettlement = parseFloat($(ee).closest("tr").find('#quantityBoxes').val()) / parseFloat($(ee).closest("tr").find('#totalSettlement').val());
                        $(ee).closest("tr").find('#pricePerBoxSettlement').val(pricePerBoxSettlement.toFixed(2));
                    }
                })
            }
        });
    }

    calcularTotalPayments();
    // tfoot tblSettlement1
    calcularTotalSettlementDetail();
    //Calculos de la liquidación tblSettlement2
    var totalSettlement = 0;
    $('#tblSettlement2>tbody>tr').each(function (j, row) {
        $('#tblDetails2>tbody>tr').each(function (i, e) {
            if (i == j) {

                var saleValueSettlement = $(e).closest("tr").find('#saleValueSettlement').val();
                var settlementExpenseValue = $(e).closest("tr").find('#settlementExpenseValue').val();
                var total = saleValueSettlement - settlementExpenseValue;
                $(row).closest("tr").find('#total').val(total.toFixed(2));

                var quantityBoxes = $(row).closest("tr").find('#quantityBoxes').val();
                var pricePerBoxSettlementNetUSD = total / quantityBoxes;
                $(row).closest("tr").find('#pricePerBoxSettlementNetUSD').val(pricePerBoxSettlementNetUSD.toFixed(2));

            }
        })
    })

    return true;
}

function calcularTotalPayments() {
    var totalToColletUSD = 0;
    var advancePaymentsUSD = 0;
    var balancePayemntesUSD = 0;
    var content = '';
    $('#tblDetails3>tbody>tr').each(function (i, e) {
        totalToColletUSD = parseFloat($(e).find('#totalToColletUSD').val()) + totalToColletUSD;
        advancePaymentsUSD = parseFloat($(e).find('#advancePaymentsUSD').val()) + advancePaymentsUSD;
        balancePayemntesUSD = parseFloat($(e).find('#balancePayemntesUSD').val()) + balancePayemntesUSD;
    })
    content += "<tr>";
    content += "<td colspan=5  style = 'text-align: right;'>Total ($)";
    content += '<td id="totalToColletUSD" style="text-align: right">' + (totalToColletUSD).toFixed(2) + '</td>';
    content += '<td id="advancePaymentsUSD" style="text-align: right"> ' + (advancePaymentsUSD).toFixed(2) + '</td>';
    content += '<td id="balancePayemntesUSD" style="text-align: right"> ' + (balancePayemntesUSD).toFixed(2) + '</td>';
    content += "</tr>";
    $("#tblDetails3>tfoot").empty().append(content);
}


function calcularTotalSettlement() {
    var totalboxes = 0;
    var totalSettlement = 0;
    var expenseFixed = 0;
    var expenseVariable = 0;
    var netSettlemnt = 0;
    var content = '';
    var totalkgbox = 0;
    $('#tblDetails>tbody>tr').each(function (i, e) {
        totalboxes = parseFloat($(e).find('#quantityBoxes').val()) + totalboxes;
        totalSettlement = parseFloat($(e).find('#totalSettlement').val()) + totalSettlement;
        expenseFixed = parseFloat($(e).find('#expenseFixed').val()) + expenseFixed;
        expenseVariable = parseFloat($(e).find('#expenseVariable').val()) + expenseVariable;
        netSettlemnt = parseFloat($(e).find('#netSettlemnt').val()) + netSettlemnt;
        totalkgbox = parseFloat($(e).find('#kgCaja').val()) + totalkgbox;
    })
    content += "<tr>";
    content += '<td id="totalkgbox" style="text-align: right"></td>';
    content += "<td colspan=4  style = 'text-align: right;'>";
    content += '<td id="totalQuantityB" style="text-align: right">' + (totalboxes).toFixed(2) + '</td>';
    content += "<td colspan=1  style = 'text-align: right;'>";
    content += '<td id="totalSettlement" style="text-align: right">' + (totalSettlement).toFixed(2) + '</td>';
    //Descontamos el costo fijo

    if (_Variable_Fixed > 0) {
        expenseFixed = _Variable_Fixed;
    }

    content += '<td id="totalExpenseFixed" style="text-align: right"> ' + (expenseFixed).toFixed(2) + '</td>';
    content += '<td id="totalExpenseVariable" style="text-align: right"> ' + (expenseVariable).toFixed(2) + '</td>';
    content += '<td id="totalNetSettlemnt" style="text-align: right"> ' + (netSettlemnt).toFixed(2) + '</td>';
    content += "</tr>";
    $("#tblDetails>tfoot").empty().append(content);

}

function calcularTotalAdjustInvoice() {
    var totalBill = 0;
    var saleValueSettlement = 0;
    var settlementExpenseValue = 0;
    var settlementFinalBalance = 0;
    var DN_CN = 0;
    var content = '';
    var finalPrice = 0;
    var PriceXKG = 0;

    $('#tblDetails2>tbody>tr').each(function (i, e) {
        totalBill = parseFloat($(e).find('#totalBill').val()) + totalBill;
        saleValueSettlement = parseFloat($(e).find('#saleValueSettlement').val()) + saleValueSettlement;
        settlementExpenseValue = parseFloat($(e).find('#settlementExpenseValue').val()) + settlementExpenseValue;
        settlementFinalBalance = parseFloat($(e).find('#settlementFinalBalance').val()) + settlementFinalBalance;
        DN_CN = parseFloat($(e).find('#DN_CN').val()) + DN_CN;
        finalPrice = parseFloat($(e).find('#setfinalPrice').val()) + finalPrice;
        PriceXKG = parseFloat($(e).find('#setfinalPriceXKG').val()) + PriceXKG;
    })
    content += "<tr>";
    content += "<td colspan=5  style = 'text-align: right;'>Total ($)";
    content += '<td id="totalBill" style="text-align: right">' + (totalBill).toFixed(2) + '</td>';
    content += '<td id="saleValueSettlement" style="text-align: right"> ' + (saleValueSettlement).toFixed(2) + '</td>';
    content += '<td id="settlementExpenseValue" style="text-align: right"> ' + (settlementExpenseValue).toFixed(2) + '</td>';
    content += '<td id="settlementFinalBalance" style="text-align: right"> ' + (settlementFinalBalance).toFixed(2) + '</td>';
    content += '<td id="DN_CN" style="text-align: right"> ' + (DN_CN).toFixed(2) + '</td>';
    content += '<td id="setfinalPrice" style="text-align: right"> ' + (finalPrice).toFixed(3) + '</td>';
    content += '<td id="setfinalPriceXKG" style="text-align: right"> ' + (PriceXKG).toFixed(3) + '</td>';
    content += "</tr>";
    $("#tblDetails2>tfoot").empty().append(content);
}

function loadCostDistribution(orderProductionID) {
    var content = '';

    var monedaInicial = $("#selectCurrency option:selected").text();
    var ExchangeRate = $('#txtExchangeRate').val();


    $('#tblGastos>thead>tr').each(function (i, e) {
        $(e).find('#CostDistributionCurrency').text(monedaInicial)
    })
    $('#tblSettlement1>thead>tr').each(function (i, e) {
        $(e).find('#CurrencyLocalBox').text(monedaInicial);
        $(e).find('#CurrencyLocalSales').text(monedaInicial)
    })


    var campaignID = localStorage.campaignID;
    var userID = $("#lblSessionUserID").text();
    var api = "/Finance/GetCostDist?orderProductionID=" + orderProductionID + "&campaignID=" + campaignID + "&userID=" + userID;

    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            var content = "";

            if (data.length > 0) {
                var datacosts = JSON.parse(data);
                _costos = datacosts;

                if ($('#chkFlagExchange').is(":checked")) {
                    _flagcurrency = 1;
                }
                else {
                    _flagcurrency = 0;
                }

                content = "";
                for (var i = 0; i < datacosts.length; i++) {
                    if (i == 0) {
                        content += "<tr style='font-size: 13px'>";
                        content += "<td>" + datacosts[i].description + "</td>";
                        content += '<td><input id="Porcentaje" onBlur="calcularCost(this)" value ="' + (datacosts[i].comissionPercent) + '"  type="number"></input></td>';
                        content += '<td><input style = "text-align: right;" name="localamount" onBlur="RecalcularPorcentaje(this)"  id="' + datacosts[i].abrlocal + '" value ="' + (datacosts[i].amount) + '" class=" form-control form-control-sm"   type="number" ></input></td>';
                        if (_flagcurrency == 1) {
                            content += '<td><input style = "text-align: right;"   id="' + datacosts[i].abrusd + '" value ="' + (datacosts[i].amount) * ExchangeRate + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                        } else {
                            content += '<td><input style = "text-align: right;"   id="' + datacosts[i].abrusd + '" value ="' + (datacosts[i].amount) / ExchangeRate + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                        }

                        content += " <td name='settlementExpensesID'  style='display:none'>" + datacosts[i].settlementExpensesID + "</td>";
                        content += "</tr>";

                    }
                    else {
                        content += "<tr style='font-size: 13px'>";
                        content += "<td>" + datacosts[i].description + "</td>";
                        content += '<td></td>';
                        content += '<td><input style = "text-align: right;" name="localamount" onBlur = "calcularCost(this)" id="' + datacosts[i].abrlocal + '" value ="' + (datacosts[i].amount) + '" class=" form-control form-control-sm"   type="number" ></input></td>';
                        if (_flagcurrency == 1) {
                            content += '<td><input style = "text-align: right;" onBlur = "calcularCost(this)" id="' + datacosts[i].abrusd + '" value ="' + (datacosts[i].amount) * ExchangeRate + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                        } else {
                            content += '<td><input style = "text-align: right;" onBlur = "calcularCost(this)" id="' + datacosts[i].abrusd + '" value ="' + (datacosts[i].amount) / ExchangeRate + '" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                        }

                        if (parseFloat(datacosts[i].typeOfExpenses) == 0) {
                            content += '<td><input type="checkbox" name="checkcost" onChange="CalcularCostVariable()"/></td>'
                        }
                        else {
                            content += '<td><input type="checkbox" name="checkcost"  checked onChange="CalcularCostVariable()"/></td>'
                        }
                        content += " <td name='settlementExpensesID'  style='display:none'>" + datacosts[i].settlementExpensesID + "</td>";

                        content += "</tr>";
                    }
                }

                //agregamos el footer
                content += "<tr style='font-size: 13px'>";
                content += "<td>Total</td>";
                content += '<td></td>';
                content += '<td><input style = "text-align: right;" onBlur = "calcularCost(this)" id="TMTOTAL" value ="0" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                content += '<td><input style = "text-align: right;" onBlur = "calcularCost(this)" id="TD" value ="0" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                content += '<td style="display:none"><input style = "text-align: right;" onBlur = "calcularCost(this)" id="TM" value ="0" class=" form-control form-control-sm"   type="number" readonly></input></td>';
                content += "</tr>";
                $("#tblGastos>tbody").empty().append(content);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");

            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });

    calcularCost(null);
    PrimeraCarga = false;
}

function CalcularCostVariable() {
    var Fixed_Expends = 0;
    var Fixed_Dolar = 0;
    var Variable_Expends = 0;
    var Variable_ExpendsDolar = 0;
    var TotalMonedaActualresta = 0;
    var TotalMonedaDolarresta = 0;

    $('#tblGastos>tbody>tr').each(function (i, e) {
        if (i == 0) {
            Fixed_Expends = parseFloat($(e).closest('tr').find("#CM").val())
            Fixed_Dolar = parseFloat($(e).closest('tr').find("#CD").val())
        }

        if ($(e).closest('tr').find("input:checkbox[name='checkcost']").is(":checked")) {

            if (i == 1) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#PFM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#PFD").val());

            }
            if (i == 2) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#RTM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#RTD").val());

            }
            if (i == 3) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#OFM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#OFD").val());

            }
            if (i == 4) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#CSM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#CSD").val());

            }
            if (i == 5) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#CTM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#CTD").val());

            }
            if (i == 6) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#IDM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#IDD").val());

            }
            if (i == 7) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#EDM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#EDD").val());

            }
            if (i == 8) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#TCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#TCD").val());

            }
            if (i == 9) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#HNM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#HND").val());

            }
            if (i == 10) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#GIM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#GID").val());

            }
            if (i == 11) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#QCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#QCD").val());
            }
            if (i == 12) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#ISM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#ISD").val());
            }
            if (i == 13) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#RCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#RCD").val());
            }
            if (i == 14) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#OCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#OCD").val());
            }

        }

    })



    $('#tblGastos>tbody').each(function (i, e) {
        TotalMonedaActualresta =
            parseFloat($(e).find('tr>td').find('#PFM').val()) + parseFloat($(e).find('tr>td').find('#RTM').val()) + parseFloat($(e).find('tr>td').find('#OFM').val()) + parseFloat($(e).find('tr>td').find('#CSM').val()) + parseFloat($(e).find('tr>td').find('#CTM').val()) +
            parseFloat($(e).find('tr>td').find('#IDM').val()) + parseFloat($(e).find('tr>td').find('#EDM').val()) + parseFloat($(e).find('tr>td').find('#TCM').val()) + parseFloat($(e).find('tr>td').find('#HNM').val()) + parseFloat($(e).find('tr>td').find('#GIM').val()) +
            parseFloat($(e).find('tr>td').find('#QCM').val()) + parseFloat($(e).find('tr>td').find('#ISM').val()) + parseFloat($(e).find('tr>td').find('#RCM').val()) + parseFloat($(e).find('tr>td').find('#OCM').val());

        TotalMonedaDolarresta =
            parseFloat($(e).find('tr>td').find('#PFD').val()) + parseFloat($(e).find('tr>td').find('#RTD').val()) + parseFloat($(e).find('tr>td').find('#OFD').val()) + parseFloat($(e).find('tr>td').find('#CSD').val()) + parseFloat($(e).find('tr>td').find('#CTD').val()) +
            parseFloat($(e).find('tr>td').find('#IDD').val()) + parseFloat($(e).find('tr>td').find('#EDD').val()) + parseFloat($(e).find('tr>td').find('#TCD').val()) + parseFloat($(e).find('tr>td').find('#HND').val()) + parseFloat($(e).find('tr>td').find('#GID').val()) +
            parseFloat($(e).find('tr>td').find('#QCD').val()) + parseFloat($(e).find('tr>td').find('#ISD').val()) + parseFloat($(e).find('tr>td').find('#RCD').val()) + parseFloat($(e).find('tr>td').find('#OCD').val());

        //Seteamos el footer
        TotalMonedaActualresta = TotalMonedaActualresta - Variable_Expends;
        TotalMonedaDolarresta = TotalMonedaDolarresta - Variable_ExpendsDolar;

        $(e).find('tr>td').find('#TMTOTAL').val((TotalMonedaActualresta + Fixed_Expends + Variable_Expends).toFixed(2));
        $(e).find('tr>td').find('#TM').val((TotalMonedaActualresta).toFixed(2));
        $(e).find('tr>td').find('#TD').val((TotalMonedaDolarresta + Fixed_Dolar + Variable_ExpendsDolar).toFixed(2));
    });

    _Variable_Fixed = TotalMonedaActualresta;
    _Variable_Expends = Variable_Expends;
    calcular(null);

}

function calcularCost(obj) {

    if (!calcular()) {
        $('#tblGastos>tbody').each(function (i, e) {
            $(e).find('tr>td').find('#PFM').val('0');
            $(e).find('tr>td').find('#RTM').val('0');
            $(e).find('tr>td').find('#OFM').val('0');
            $(e).find('tr>td').find('#CSM').val('0');
            $(e).find('tr>td').find('#CTM').val('0');
            $(e).find('tr>td').find('#IDM').val('0');
            $(e).find('tr>td').find('#EDM').val('0');
            $(e).find('tr>td').find('#TCM').val('0');
            $(e).find('tr>td').find('#HNM').val('0');
            $(e).find('tr>td').find('#GIM').val('0');
            $(e).find('tr>td').find('#QCM').val('0');
            $(e).find('tr>td').find('#ISM').val('0');
            $(e).find('tr>td').find('#RCM').val('0');
            $(e).find('tr>td').find('#OCM').val('0');
            $(e).find('tr>td').find('#TM').val('0');
        });
        return;
    }

    var TotalexpenseVariable = 0;
    var TotalMonedaActual = 0;
    var TotalMonedaDolar = 0;
    var TotaltotalSettlement = 0;
    $('#tblDetails>tfoot>tr').each(function (i, e) {
        TotalexpenseVariable = parseFloat($(e).find('#totalExpenseVariable').text());
        TotaltotalSettlement = parseFloat($(e).find('#totalSettlement').text());
    });

    //Calculo CostDistribution
    Calculo_CostDistribution(TotalexpenseVariable);
    calcularTotalSettlement();
    // tfoot tblSettlement1
    calcularTotalSettlementDetail();
}

function Calculo_CostDistribution(TotalexpenseVariable) {
    //Calculo CostDistribution
    var Variable_Expends = 0;
    var Variable_ExpendsDolar = 0;
    var Fixed_Expends = 0;
    var Fixed_Dolar = 0;

    var monedaInicialVal = $("#selectCurrency option:selected").val();
    var tipocambio = 1;
    if (monedaInicialVal != 1) //si no es dolar seteamos el tipo de caambio con el valor
    {
        tipocambio = parseFloat($('#txtExchangeRate').val());
    }

    $('#tblGastos>tbody>tr').each(function (i, e) {
        if (i == 0) {
            Fixed_Expends = parseFloat($(e).closest('tr').find("#CM").val())
            Fixed_Dolar = parseFloat($(e).closest('tr').find("#CD").val())
        }
        if ($(e).closest('tr').find("input:checkbox[name='checkcost']").is(":checked")) {
            if (i == 1) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#PFM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#PFD").val());

            }
            if (i == 2) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#RTM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#RTD").val());

            }
            if (i == 3) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#OFM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#OFD").val());

            }
            if (i == 4) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#CSM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#CSD").val());

            }
            if (i == 5) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#CTM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#CTD").val());

            }
            if (i == 6) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#IDM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#IDD").val());

            }
            if (i == 7) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#EDM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#EDD").val());

            }
            if (i == 8) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#TCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#TCD").val());

            }
            if (i == 9) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#HNM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#HND").val());

            }
            if (i == 10) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#GIM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#GID").val());

            }
            if (i == 11) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#QCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#QCD").val());
            }
            if (i == 12) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#ISM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#ISD").val());
            }
            if (i == 13) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#RCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#RCD").val());
            }
            if (i == 14) {
                Variable_Expends = Variable_Expends + parseFloat($(e).closest('tr').find("#OCM").val());
                Variable_ExpendsDolar = Variable_ExpendsDolar + parseFloat($(e).closest('tr').find("#OCD").val());
            }

        }
    })

    $('#tblGastos>tbody').each(function (i, e) {
        //Calculo CostDistribution
        if (_flagcurrency == 1) {
            $(e).find('tr>td').find('#PFD').val((($(e).find('tr>td').find('#PFM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#RTD').val((($(e).find('tr>td').find('#RTM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#OFD').val((($(e).find('tr>td').find('#OFM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#CSD').val((($(e).find('tr>td').find('#CSM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#CTD').val((($(e).find('tr>td').find('#CTM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#IDD').val((($(e).find('tr>td').find('#IDM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#EDD').val((($(e).find('tr>td').find('#EDM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#TCD').val((($(e).find('tr>td').find('#TCM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#HND').val((($(e).find('tr>td').find('#HNM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#GID').val((($(e).find('tr>td').find('#GIM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#QCD').val((($(e).find('tr>td').find('#QCM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#ISD').val((($(e).find('tr>td').find('#ISM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#RCD').val((($(e).find('tr>td').find('#RCM').val()) * tipocambio).toFixed(2));
            $(e).find('tr>td').find('#OCD').val((($(e).find('tr>td').find('#OCM').val()) * tipocambio).toFixed(2));

        }
        else {
            $(e).find('tr>td').find('#PFD').val((($(e).find('tr>td').find('#PFM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#RTD').val((($(e).find('tr>td').find('#RTM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#OFD').val((($(e).find('tr>td').find('#OFM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#CSD').val((($(e).find('tr>td').find('#CSM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#CTD').val((($(e).find('tr>td').find('#CTM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#IDD').val((($(e).find('tr>td').find('#IDM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#EDD').val((($(e).find('tr>td').find('#EDM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#TCD').val((($(e).find('tr>td').find('#TCM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#HND').val((($(e).find('tr>td').find('#HNM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#GID').val((($(e).find('tr>td').find('#GIM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#QCD').val((($(e).find('tr>td').find('#QCM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#ISD').val((($(e).find('tr>td').find('#ISM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#RCD').val((($(e).find('tr>td').find('#RCM').val()) / tipocambio).toFixed(2));
            $(e).find('tr>td').find('#OCD').val((($(e).find('tr>td').find('#OCM').val()) / tipocambio).toFixed(2));
        }

        $(e).find('tr>td').find('#CM').val(TotalexpenseVariable - _Variable_Expends);
        TotalMonedaActual =
            parseFloat($(e).find('tr>td').find('#PFM').val()) + parseFloat($(e).find('tr>td').find('#RTM').val()) + parseFloat($(e).find('tr>td').find('#OFM').val()) + parseFloat($(e).find('tr>td').find('#CSM').val()) + parseFloat($(e).find('tr>td').find('#CTM').val()) +
            parseFloat($(e).find('tr>td').find('#IDM').val()) + parseFloat($(e).find('tr>td').find('#EDM').val()) + parseFloat($(e).find('tr>td').find('#TCM').val()) + parseFloat($(e).find('tr>td').find('#HNM').val()) + parseFloat($(e).find('tr>td').find('#GIM').val()) +
            parseFloat($(e).find('tr>td').find('#QCM').val()) + parseFloat($(e).find('tr>td').find('#ISM').val()) + parseFloat($(e).find('tr>td').find('#RCM').val()) + parseFloat($(e).find('tr>td').find('#OCM').val());

        TotalMonedaDolar =
            parseFloat($(e).find('tr>td').find('#PFD').val()) + parseFloat($(e).find('tr>td').find('#RTD').val()) + parseFloat($(e).find('tr>td').find('#OFD').val()) + parseFloat($(e).find('tr>td').find('#CSD').val()) + parseFloat($(e).find('tr>td').find('#CTD').val()) +
            parseFloat($(e).find('tr>td').find('#IDD').val()) + parseFloat($(e).find('tr>td').find('#EDD').val()) + parseFloat($(e).find('tr>td').find('#TCD').val()) + parseFloat($(e).find('tr>td').find('#HND').val()) + parseFloat($(e).find('tr>td').find('#GID').val()) +
            parseFloat($(e).find('tr>td').find('#QCD').val()) + parseFloat($(e).find('tr>td').find('#ISD').val()) + parseFloat($(e).find('tr>td').find('#RCD').val()) + parseFloat($(e).find('tr>td').find('#OCD').val());

        TotalMonedaActual = TotalMonedaActual - Variable_Expends;
        TotalMonedaDolar = TotalMonedaDolar - Variable_ExpendsDolar;
        $(e).find('tr>td').find('#TMTOTAL').val((TotalMonedaActual + Fixed_Expends + Variable_Expends).toFixed(2));
        $(e).find('tr>td').find('#TM').val((TotalMonedaActual).toFixed(2));
        $(e).find('tr>td').find('#TD').val((TotalMonedaDolar + Fixed_Dolar + Variable_ExpendsDolar).toFixed(2));
    });

    _Variable_Fixed = TotalMonedaActual;
    _Variable_Expends = Variable_Expends;
}

function PreGuardarDocuments() {
    Swal.fire({
        title: '¿Want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveDocuments();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancelled!', 'The save process was canceled.')
        }
    });
}

function PreeliminarSettelement(growerSettlementID) {
    Swal.fire({
        title: '¿Desea eliminar?',
        text: "Los datos serán eliminados permanentemente en el sistema.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, eliminar!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            deleteSettelement(growerSettlementID);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancelado!', 'Se canceló el proceso de eliminación.')
        }
    });
}


function PreGuardarPayments() {
    Swal.fire({
        title: '¿Want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Save!',
        cancelButtonText: 'No, Cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            savePayments();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancelled!', 'The save process was canceled.')
        }
    });
}


function PreGuardarDetailsCost() {
    Swal.fire({
        title: '¿Want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Save!',
        cancelButtonText: 'No, Cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveDetailsCost();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancelled!', 'The save process was canceled.')
        }
    });
}

function PreGuardarFinanceGrower() {
    Swal.fire({
        title: '¿Desea guardar?',
        text: "Los datos serán almacenados permanentemente en el sistema.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, guardar!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveFinanceGrower();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancelado!', 'Se canceló el proceso de guardado.')
        }
    });
}

function deleteSettelement(growerSettlementID) {
    var orderProductionID = _orderProductionID;

    var api = "/Finance/GetFinanceGrowerDelete?growerSetlementID=" + growerSettlementID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (datares) {

            if (parseInt(datares) > 0) {
                sweet_alert_success('Guardado!', 'Los datos han sido eliminados.');
                loadPOSettlementByPOID(orderProductionID)

                $('#tblSettlement>tbody>tr').each(function (i, e) {
                    if (i == _indexgrowerSettlementID) {
                        $(e).find('#checkSeleccionado').prop("checked", true);
                        return;
                    }
                });

            } else {
                sweet_alert_error("Error", "NO se guardaron los datos!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "NO se guardaron los datos!");
        }
    });
}

function savePayments() {

    if (!obligatoryP()) {
        return
    }
    //Recorriendo la lista para llenar el detalle de la tabla
    var details = '';
    var detail = '';
    var detailsDocuments = '';

    var paymentType = $(selectTypePaymentsP2).val();
    var date = "";
    var date2 = $("#DateP").val();
    if (date2 == null) {
        date2 = ""
    }
    if (date2 != "") {
        var from = date2.split("-");
        date = from[1] + "/" + from[2] + "/" + from[0];

    } else {
        date = "";

    }
    var localCurrencyIP = $(selectCurrencyP).val();
    var numberOperation = $("#txtNumberP").val();
    var amount = $("#txtAmountP").val();
    var exchangeRate = $("#txtExchangeRateP").val();
    var comments = $("#txtObservationP").val();
    var userCreated = $("#txtUserCreatedP").val();
    var userModify = $("#txtUserModifyP").val();
    var userID = parseInt($("#lblSessionUserID").text());
    var orderProductionID = _orderProductionID;
    var customerPaymentID = _customerPaymentID;
    var customerSettlementID = _customerSettlementID;
    var amountUSD = 0;
    if (_flagcurrency == 1) { amountUSD = ($("#txtAmountP").val() * exchangeRate); }
    else { amountUSD = ($("#txtAmountP").val() / exchangeRate); }

    var o = {}
    o = {
        "customerPaymentID": customerPaymentID,
        "customerSettlementID": customerSettlementID,
        "paymentTypeID": paymentType,
        "numberOperation": numberOperation,
        "date": date,
        "localCurrencyID": localCurrencyIP,
        "exchangeRate": exchangeRate,
        "amount": amount,
        "amountUSD": amountUSD,
        "comments": comments,
        "statusID": 1,
        "userID": userID,
        "details": []
    }

    $('table#tblPayments1>tbody>tr').each(function (i, e) {
        var det = {
            "customerPaymentsDetailID": $(e).find("#customerPaymentsDetailID").text(),
            "customerPaymentID": $(e).find("#customerPaymentID").text(),
            "customerSettlementDetailID": $(e).find("#customerSettlementDetailID").text(),
            "pricePerBox": $(e).find("input.inpPricePerBox").val(),
            "total": $(e).find("input.inpTotal").val(),
            "statusID": 1,
            "userID": userID
        }

        o.details.push(det);

    });

    var api = "/Finance/SaveFinancePayment"

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {

            if (parseInt(datares) > 0) {
                sweet_alert_success('Saved!', 'The data has been saved.');
                $('#myModalPayments').modal('hide');
                loadPaymentByPOID(orderProductionID);
                loadPOForFinancePayments(orderProductionID);
            } else {
                sweet_alert_error("Error", "The data was NOT saved!");

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "The data was NOT saved!");
        }
    });
}

function saveDocuments() {

    if (!obligatoryD()) {
        return
    }
    var documentTypeID = $(selectTypeDocumentD).val();

    var date2 = $("#FechaD").val();

    if (date2 == null) {
        date2 = "";
    }

    if (date2 != "") {
        var from = date2.split("-");
        date = from[1] + "/" + from[2] + "/" + from[0];
    } else {
        date = $("#FechaD").val();
    }

    var localCurrencyID = $(selectCurrencyD).val();
    var number = $("#txtNumberD").val();
    var totalAmount = $("#txtAmountD").val();
    var exchangeRate = $("#txtExchangeRateD").val();
    var comments = $("#txtObservationD").val();
    var freightBL = $("#txtUserFreightD").val();
    var insuranceBL = $("#txtInsuraceD").val();

    var userCreated = $("#txtUserCreatedD").val();
    var userModify = $("#txtUserModifyD").val();
    var orderProductionID = _orderProductionID;
    var customerSettlementID = _customerSettlementID;
    var customerInvoiceID = _customerInvoiceID;
    var userID = parseInt($("#lblSessionUserID").text());
    var totalAmountUSD = 0;
    if (_flagcurrency == 1) { totalAmountUSD = ($("#txtAmountD").val() * exchangeRate); }
    else { totalAmountUSD = ($("#txtAmountD").val() / exchangeRate); }

    var o = {}
    o = {
        "customerInvoiceID": customerInvoiceID,
        "customerSettlementID": customerSettlementID,
        "documentTypeID": documentTypeID,
        "date": date,
        "localCurrencyID": localCurrencyID,
        "number": number,
        "totalAmount": totalAmount,
        "exchangeRate": exchangeRate,
        "totalAmountUSD": totalAmountUSD,
        "comments": comments,
        "insuranceBL": insuranceBL,
        "freightBL": freightBL,
        "statusID": 1,
        "userID": userID,
        "details": []
    }

    $('table#tblDocuments1>tbody>tr').each(function (i, e) {

        var det = {
            "customerInvoiceDetailID": $(e).find("#customerInvoiceDetailID").text(),
            "customerInvoiceID": $(e).find("#customerInvoiceID").text(),
            "customerSettlementDetailID": $(e).find("#customerSettlementDetailID").text(),
            "quantityBoxes": $(e).find("#quantityBoxes").text(),
            "pricePerBox": $(e).find("#pricePerBox").text(),
            "total": $(e).find("#inputTotal").text(),
            "statusID": 1,
            "userID": userID,
            "pricePerBoxFOB": $(e).find("#priceBoxFOB").text(),
            "totalFOB": $(e).find("#totalFOB").text()
        }

        o.details.push(det);

    });

    JSON.stringify(o);
    var api = "/Finance/SaveFinanceInvoice"

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (parseInt(datares) > 0) {
                sweet_alert_success('Saved!', 'The data has been saved.');
                $('#myModalDocuments').modal('hide');
                loadDocumentsByPOID(orderProductionID);
                loadPOForFinanceAdjutsInvoiceOZM(orderProductionID);
            } else {
                sweet_alert_error("Error", "The data was NOT saved!");

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "The data was NOT saved!");
        }
    });

}

function saveDetailsCost() {
    var details = '';
    var detail = '';
    var detailsDetailsCost = '';

    var totalLocalCurrency = 0;
    var totalUSD = 0;
    var orderProductionID = _orderProductionID;
    var customerSettlementID = _customerSettlementID;
    var date = $("#Date").val();
    var localCurrencyID = $("#selectCurrency").val();
    var exchangeRate = $("#txtExchangeRate").val();
    var incotermID = $("#txtIncoterm").val();
    var comments = $("#txtObservation").val();
    var comissionPercent = 0;
    var comission = 0;
    var oceanFreight = 0;
    var coldTratment = 0;
    var importDuty = 0;
    var forwardingCharge = 0;
    var marketCharge = 0;
    var transportCost = 0;
    var detentionCharge = 0;
    var coldStorage = 0;
    var otherCharge = 0;
    var repacking = 0;
    var expenseFixed = 0;
    var expenseVariable = 0;

    var userID = parseInt($("#lblSessionUserID").text());
    $('#tblGastos>tbody').each(function (j, row) {

        totalLocalCurrency = parseFloat($(row).find('tr>td').find('#TM').val());
        totalUSD = parseFloat($(row).find('tr>td').find('#TD').val());
        comissionPercent = parseFloat($(row).find('tr').find('#Porcentaje').val());
        comission = parseFloat($(row).find('tr>td').find('#CM').val());
        oceanFreight = parseFloat($(row).find('tr>td').find('#OFM').val());
        coldTratment = parseFloat($(row).find('tr>td').find('#CTM').val());
        importDuty = parseFloat($(row).find('tr>td').find('#IDM').val());
        forwardingCharge = parseFloat($(row).find('tr>td').find('#FCM').val());
        marketCharge = parseFloat($(row).find('tr>td').find('#MCM').val());
        transportCost = parseFloat($(row).find('tr>td').find('#TCM').val());
        detentionCharge = parseFloat($(row).find('tr>td').find('#DCM').val());
        coldStorage = parseFloat($(row).find('tr>td').find('#CSM').val());
        otherCharge = parseFloat($(row).find('tr>td').find('#OCM').val());
        repacking = parseFloat($(row).find('tr>td').find('#RCM').val());

    })

    $('table#tblDetails>tfoot').each(function (i, row) {
        expenseFixed = parseFloat($(row).find('tr').find('#totalExpenseFixed').text());
        expenseVariable = parseFloat($(row).find('tr').find('#totalExpenseVariable').text());

    });

    var o = {}
    o = {
        "customerSettlementID": customerSettlementID,
        "orderProductionID": orderProductionID,
        "date": date,
        "localCurrencyID": localCurrencyID,
        "exchangeRate": exchangeRate,
        "totalLocalCurrency": totalLocalCurrency,
        "totalUSD": totalUSD,
        "incotermID": incotermID,
        "comments": comments,
        "comissionPercent": comissionPercent,
        "comission": comission,
        "oceanFreight": oceanFreight,
        "coldTratment": coldTratment,
        "importDuty": importDuty,
        "forwardingCharge": forwardingCharge,
        "marketCharge": marketCharge,
        "transportCost": transportCost,
        "detentionCharge": detentionCharge,
        "coldStorage": coldStorage,
        "otherCharge": otherCharge,
        "repacking": repacking,
        "expenseFixed": expenseFixed,
        "expenseVariable": expenseVariable,
        "statusID": 1,
        "userID": userID,
        "Ocean_Freight": _Ocean_Freight,
        "Cold_Tratment": _Cold_Tratment,
        "Import_Duty": _Import_Duty,
        "Forwarding_Charge": _Forwarding_Charge,
        "Market_Charge": _Market_Charge,
        "Transport_Cost": _Transport_Cost,
        "Detention_Charge": _Detention_Charge,
        "Cold_Storage": _Cold_Storage,
        "Other_Charge": _Other_Charge,
        "Repacking_Charges": _Repacking_Charges,
        "flagExchange": _flagcurrency,
        "detailcosts": [],
        "details": []
    }

    var rowCount = $('#tblGastos tr').length;
    $('#tblGastos>tbody>tr').each(function (i, e) {
        if (i == rowCount - 2) { return false; }

        var localamount = 0;
        var typeOfExpenses = 0;
        var checked = $(e).closest('tr').find("input:checkbox[name='checkcost']");
        var amount = $(e).closest("tr").find("input[name='localamount']");
        var settlementExpensesID = $(e).closest("tr").find("td[name='settlementExpensesID']");

        if (settlementExpensesID.length > 0) {
            settlementExpensesID = settlementExpensesID.text();
        }

        if (checked.length > 0) {
            if (checked.is(":checked")) {
                typeOfExpenses = 1;

            }
        }

        if (amount.length > 0) {
            localamount = amount.val()

        }
        var detcost = {
            "settlementExpensesID": settlementExpensesID,
            "amount": localamount,
            "typeOfExpenses": typeOfExpenses
        }

        o.detailcosts.push(detcost);
    });


    $('table#tblDetails>tbody>tr').each(function (i, e) {

        var totalSettl = 0;
        if (_flagcurrency == 1) { totalSettl = (($(e).find("#totalSettlement").val()) * exchangeRate); }
        else { totalSettl = (($(e).find("#totalSettlement").val()) / exchangeRate); }

        var det = {
            "customerSettlementDetailID": $(e).find("#customerSettlementDetailID").val(),
            "customerSettlementID": $(e).find("#customerSettlementID").text(),
            "quantityBoxes": $(e).find("#quantityBoxes").val(),
            "pricePerBox": $(e).find("#pricePerBox").val(),
            "totalSettlement": $(e).find("#totalSettlement").val(),
            "expenseFixed": $(e).find("#expenseFixed").val(),
            "expenseVariable": $(e).find("#expenseVariable").val(),
            "totalSettlementUSD": totalSettl,
            "netSettlement": $(e).find("#netSettlemnt").val(),
            "statusID": 1,
            "userID": userID
        }

        o.details.push(det);

    });

    var api = "/Finance/SaveFinanceCustomerSettlement"

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (parseInt(datares) > 0) {
                sweet_alert_success('Saved!', 'The data has been saved.');
                createList();
            } else {
                sweet_alert_error("Error", "NO se guardaron los datos!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "The data was NOT saved!");
        }
    });

}

function saveFinanceGrower() {
    var details = '';
    var detail = '';
    var detailsDetailsCost = '';
    var growerSettlementID = 0;
    $('#tblSettlement>tbody>tr').each(function (i, e) {

        var checkSeleccionado = ($(this).closest("tr").find('#checkSeleccionado').is(":checked") == true) ? 1 : 0;
        if (checkSeleccionado == 1) {
            growerSettlementID = $(this).closest("tr").find('#growerSettlementID').val();
            return;
        }
    });

    if (growerSettlementID == 0) { return; }

    var customerSettlementID = _customerSettlementID;
    var userID = parseInt($("#lblSessionUserID").text());
    var statusConfirm = "";
    var date = "";
    var reference = '';
    var number = 0;
    var qc = 0;
    var insurance = 0;
    var others = 0;
    var comissionPercent = 0;
    var comission = 0;
    var promotionFeePercent = 0;
    var promotion = 0;

    $('#tblSettlement>tbody>tr').each(function (j, row) {
        if ($(row).find('#checkSeleccionado').is(":checked") == true) {

            statusConfirm = $(row).closest("tr").find('#status').val();
            var date2 = $(row).closest("tr").find('#Date').val();
            if (date2 == null) {
                date2 = "";
            }
            if (date2 != "") {
                var from = date2.split("-");
                date = from[1] + "/" + from[2] + "/" + from[0];
            } else {
                date = $(row).closest("tr").find('#Date').val();
            }

            reference = $(row).closest("tr").find('#reference').val();

            number = $(row).closest("tr").find('#number').val();
        }

    })

    $('#tblSettlement3>tbody').each(function (j, row) {

        qc = parseFloat($(row).find('tr').find('#qc').val());
        insurance = parseFloat($(row).find('tr').find('#insurance').val());
        others = parseFloat($(row).find('tr').find('#others').val());
        comissionPercent = parseFloat($(row).find('tr').find('#comissionPercent').val());
        comission = parseFloat($(row).find('tr').find('#comission').val());
        promotionFeePercent = parseFloat($(row).find('tr').find('#promotionFeePercent').val());
        promotionFee = parseFloat($(row).find('tr').find('#promotionFee').val());
    })

    var o = {}
    o = {
        "growerSettlementID": growerSettlementID,
        "customerSettlementID": customerSettlementID,
        "number": number,
        "reference": reference,
        "date": date,
        "qc": qc,
        "insurance": insurance,
        "others": others,
        "comissionPercent": comissionPercent,
        "comission": comission,
        "promotionFeePercent": promotionFeePercent,
        "promotion": promotion,
        "statusConfirm": statusConfirm,
        "userID": userID,
        "details": []
    }

    $('table#tblSettlement1>tbody>tr').each(function (i, e) {

        var det = {
            "growerSettlementDetailID": $(e).find("#growerSettlementDetailID").val(),
            "growerSettlementID": $(e).find("#growerSettlementID").val(),
            "customerSettlementDetailID": $(e).find("#customerSettlementDetailID").val(),
            "pricePerBox": $(e).find("#pricePerBox").val(),
            "total": $(e).find("#totalSettlement").val(),
            "userID": userID
        }

        o.details.push(det);

    });

    var api = "/Finance/SaveFinanceGrower"

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (parseInt(datares) > 0) {
                sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
            } else {
                sweet_alert_error("Error", "NO se guardaron los datos!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "NO se guardaron los datos!");
        }
    });

}

function pad2(n) {
    return (n < 10 ? '0' : '') + n;
}

function openPackingList() {
    var api = "/Finance/GetPackingList?orderProductionID=" + _orderProductionID;

    $.ajax({
        type: "GET",
        url: api,
        success: function (data) {
            if (data.length > 0) {
                var dataDocuments = JSON.parse(data);
                var content = "";
                $.each(dataDocuments, function (index, row) {
                    content += "  <tr style='font-size: 14px'>";
                    content += "      <td id='orderProductionID'  style='display:none'>" + row.orderProductionID + "</td>";
                    content += "      <td id='grower' style='max-width:160px; min-width:160px;'>" + row.nroPackingList + "</td>";
                    content += "      <td id='grower' style='max-width:120px; min-width:120px;'hidden>" + row.container + "</td>";
                    content += "      <td id='po' style='max-width:100px; min-width:100px;' >" + row.numberPallet + " </td>";
                    content += "      <td id='packingList' style='max-width:120px; min-width:120px;'>" + row.variety + " </td>";
                    content += "      <td id='packingList' style='max-width:110px; min-width:110px;'>" + row.codepack + "</td>";
                    content += "      <td id='container' style='max-width:100px; min-width:100px;'>" + row.brand + "</td>";
                    content += "      <td id='invoice' style='max-width:150px; min-width:150px;'>" + row.pack + "</td>";
                    content += "      <td id='etd'style='max-width:70px; min-width:70px;'>" + row.category + " </td>";
                    content += "      <td id='eta' style='max-width:80px; min-width:80px;'>" + row.sicoca + "</td>";
                    content += "      <td id='boxes' style='max-width:50px; min-width:50px;'>" + row.quantityBoxes + "</td>";
                    content += "    </tr>";

                });
                $("table#tablePackingList").DataTable().destroy();
                $("#tablePackingList>tbody").empty().append(content);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },

        complete: function () {

            $("#tablePackingList").dataTable({
                "pageLength": 20,
                "paging": true,
                "ordering": true,
                "info": true,
                "responsive": true,
                destroy: true,
                dom: 'Bfrtip',
                //buttons: ['copy', 'excel', 'pdf', 'csv'],
                buttons: [
                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'

                ],
            });
        }
    });
}

function ListOfFinance(statusid) {

    //Recorriendo la lista para llenar las PO con PL
    var opt = "lis";
    var campaignID = localStorage.campaignID;
    var nroPackinglist = $('#cboPackingList option:selected').text();
    var container = $('#cboContainer option:selected').text();
    var dayini = '';
    var dayfin = '';
    var periodo = '1';

    var date = new Date();
    var month = pad2(date.getMonth() + 1);
    var day = pad2(date.getDate());
    var year = date.getFullYear();
    dayini = year + "-" + month + "-" + day;
    dayfin = year + "-" + month + "-" + day;
    periodo = -1;


    if (nroPackinglist == null || nroPackinglist == '[ALL]') {
        nroPackinglist = '';
    }
    if (container == null || container == '[ALL]') {
        container = '';
    }
    var api = "/Finance/GetPOList?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin + "&periodo=" + periodo + "&statusid=" + statusid;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataDocuments = JSON.parse(data);
                _documents = dataDocuments;
                var content = "";
                var color = '';
                $.each(dataDocuments, function (index, row) {
                    if (row.statusID == 1) {
                        content += '<tr  class="table-warning">';
                        color = 'badge-warning';
                    }
                    if (row.statusID == 2) {
                        content += '<tr  class="table-danger">';
                        color = 'badge-danger';
                    }
                    if (row.statusID == 3) {
                        content += '<tr  class="table-secondary">';
                        color = 'badge-secondary';
                    }
                    if (row.statusID == 4) {
                        content += '<tr  class="table-primary">';
                        color = 'badge-primary';
                    }
                    if (row.statusID == 5) {
                        content += '<tr  class="table-success">';
                        color = 'badge-success';
                    }
                    //content += "  <tr style='font-size: 14px'>";
                    content += " <td id='orderProductionID'  style='display:none'>" + row.orderProductionID + "</td>";
                    content += ' <td id="Operations" td style="max-width:40px;min-width:40px; text-align:center">';
                    content += ' <button class="btn btn-sm btn-primary" id ="btnModalEdit" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" > <i style="color: white" class="fas fa-edit fa-sm"></i></button ></td > ';
                    content += "      <td id='grower' style='max-width:120px; min-width:120px;font-size:12px;'>" + row.grower + "</td>";
                    content += "      <td id='po' style='max-width:100px; min-width:100px;font-size:12px;' >" + row.po + " </td>";
                    content += "      <td id='packingList' style='max-width:150px; min-width:150px;font-size:12px;'>" + row.packingList + "</td>";
                    content += "      <td ><textarea id='consignee' class='form-control form-control-sm' rows='1'>" + row.consignee + "</textarea></td>";
                    content += "      <td id='container' style='max-width:110px; min-width:110px;font-size:12px;'>" + row.container + "</td>";
                    content += "      <td id='invoice' style='max-width:110px; min-width:110px;font-size:12px;'>" + row.invoice + "</td>";
                    content += "      <td id='etd'style='max-width:80px; min-width:80px;font-size:12px;'>" + row.etd + " </td>";
                    content += "      <td id='eta' style='max-width:80px; min-width:80px;font-size:12px;'>" + row.eta + "</td>";
                    content += "      <td id='boxes' style='max-width:50px; min-width:50px;font-size:12px;'>" + row.boxes + "</td>";
                    content += "      <td id='status' style='max-width:125px; min-width:125px;font-size:14px;'><div class='badge " + color + " badge-pill'>" + row.status + "</div></td>";
                    content += "    </tr>";

                });
                sweet_alert_progressbar();
                $("table#tableList").DataTable().destroy();
                $("#tableList>tbody").empty().append(content);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },

        complete: function () {
            $("#tableList").dataTable({
                "pageLength": 10,
                "paging": true,
                "ordering": true,
                "info": true,
                "responsive": true,
                destroy: true,
                dom: 'Bfrtip',
                buttons: [
                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'

                ],
            });
        }
    });

}

function createList() {

    //Recorriendo la lista para llenar las PO con PL
    var opt = "lis";
    var campaignID = localStorage.campaignID;
    var nroPackinglist = $('#cboPackingList option:selected').text();
    var container = $('#cboContainer option:selected').text();
    var dayini = '';
    var dayfin = '';
    var periodo = '1';
    var statusid = '';

    if ($("#txtFrom").val() == '' || $("#txtUntil").val() == '') {
        var date = new Date();
        var month = pad2(date.getMonth() + 1);//months (0-11)
        var day = pad2(date.getDate());//day (1-31)
        var year = date.getFullYear();
        dayini = year + "-" + month + "-" + day;
        dayfin = year + "-" + month + "-" + day;
        periodo = -1;
    } else {
        var datefrom = $("#txtFrom").val().split('/');
        var dateto = $("#txtUntil").val().split('/');
        dayini = datefrom[2] + '-' + datefrom[1] + '-' + datefrom[0];
        dayfin = dateto[2] + '-' + dateto[1] + '-' + dateto[0];
    }

    if (nroPackinglist == null || nroPackinglist == '[ALL]') {
        nroPackinglist = '';
    }
    if (container == null || container == '[ALL]') {
        container = '';
    }
    var api = "/Finance/GetPOList?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin + "&periodo=" + periodo + "&statusid=" + statusid;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataDocuments = JSON.parse(data);
                _documents = dataDocuments;
                var content = "";
                var color = '';
                $.each(dataDocuments, function (index, row) {
                if (row.statusID == 1) {
                    content += '<tr  class="table-warning">';
                    color = 'badge-warning';
                }
                if (row.statusID == 2) {
                    content += '<tr  class="table-danger">';
                    color = 'badge-danger';
                }
                if (row.statusID == 3) {
                    content += '<tr  class="table-secondary">';
                    color = 'badge-secondary';
                }
                if (row.statusID == 4) {
                    content += '<tr  class="table-primary">';
                    color = 'badge-primary';
                }
                if (row.statusID == 5) {
                    content += '<tr  class="table-success">';
                    color = 'badge-success';
                }                
                    //content += "  <tr style='font-size: 14px'>";
                    content += " <td id='orderProductionID'  style='display:none'>" + row.orderProductionID + "</td>";
                    content += ' <td id="Operations" td style="max-width:40px;min-width:40px; text-align:center">';
                    content += ' <button class="btn btn-sm btn-primary" id ="btnModalEdit" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" > <i style="color: white" class="fas fa-edit fa-sm"></i></button ></td > ';
                    content += "      <td id='grower' style='max-width:120px; min-width:120px;font-size:12px;'>" + row.grower + "</td>";
                    content += "      <td id='po' style='max-width:100px; min-width:100px;font-size:12px;' >" + row.po + " </td>";
                    content += "      <td id='packingList' style='max-width:150px; min-width:150px;font-size:12px;'>" + row.packingList + "</td>";
                    content += "      <td ><textarea id='consignee' class='form-control form-control-sm' rows='1'>" + row.consignee + "</textarea></td>";
                    content += "      <td id='container' style='max-width:110px; min-width:110px;font-size:12px;'>" + row.container + "</td>";
                    content += "      <td id='invoice' style='max-width:110px; min-width:110px;font-size:12px;'>" + row.invoice + "</td>";
                    content += "      <td id='etd'style='max-width:80px; min-width:80px;font-size:12px;'>" + row.etd + " </td>";
                    content += "      <td id='eta' style='max-width:80px; min-width:80px;font-size:12px;'>" + row.eta + "</td>";
                    content += "      <td id='boxes' style='max-width:50px; min-width:50px;font-size:12px;'>" + row.boxes + "</td>";
                    content += "      <td id='status' style='max-width:125px; min-width:125px;font-size:14px;'><div class='badge " + color + " badge-pill'>" + row.status + "</div></td>";
                    content += "    </tr>";

                });
                sweet_alert_progressbar();
                $("table#tableList").DataTable().destroy();
                $("#tableList>tbody").empty().append(content);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },

        complete: function () {

            $("#tableList").dataTable({
                "pageLength": 10,
                "paging": true,
                "ordering": true,
                "info": true,
                "responsive": true,
                destroy: true,
                dom: 'Bfrtip',
                buttons: [
                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'

                ],
            });
        }
    });


    opt = "stf";
    api = "/Finance/GetPOList?opt=" + opt + "&campaignID=" + campaignID + "&nroPackinglist=" + nroPackinglist + "&container=" + container + "&dayini=" + dayini + "&dayfin=" + dayfin + "&periodo=" + periodo + "&status=" + statusid;

    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataDocuments = JSON.parse(data);
                for (var i = 0; i < dataDocuments.length; i++) {

                    if (EstadosF.Pending_Settlement == dataDocuments[i].statusID) {
                        $("#Pending_Settlement").text(dataDocuments[i].quantity)
                    }
                    else if (EstadosF.Liquidated == dataDocuments[i].statusID) {
                        $("#Liquidated").text(dataDocuments[i].quantity)
                    }
                    else if (EstadosF.Amortized_Liquidate == dataDocuments[i].statusID) {
                        $("#Amortized_Liquidate").text(dataDocuments[i].quantity)
                    }
                    else if (EstadosF.Liquidated_Paid == dataDocuments[i].statusID) {
                        $("#Liquidated_Paid").text(dataDocuments[i].quantity)
                    }
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");

        },
    });
}

function sweet_alert_progressbar() {
    let timerInterval
    Swal.fire({
        imageUrl: '../images/AgricolaAndrea71x64.png',
        title: 'AGROCOM',
        html: 'Loading information...',
        timer: 2000,
        timerProgressBar: true,
        showConfirmButton: false,
        didOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                        b.textContent = Swal.getTimerLeft()
                    }
                }
            }, 100)
        },
        willClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {

        }
    })
}



function obligatoryD() {
    var response = false;

    var TypeDocumentD = $(selectTypeDocumentD).selectpicker('val')
    if ($(TypeDocumentD).val() == 0 || TypeDocumentD == null) {
        sweet_alert_error('Error', 'choose Type Document');
        return response;
    }

    var CurrencyD = $(selectCurrencyD).selectpicker('val')
    if ($(CurrencyD).val() == 0 || CurrencyD == null) {
        sweet_alert_error('Error', 'choose Currency.');

        return response;
    }

    var number = $("#txtNumberD").val();
    if (number == "") {
        sweet_alert_error('Error', 'Enter number;');
        return response;
    }

    var amount = $("#txtAmountD").val();
    if (amount <= 0) {
        sweet_alert_error('Error', 'Enter amount');
        return response;
    }

    var exchangeRate = $("#txtExchangeRateD").val();
    if (exchangeRate <= 0) {
        sweet_alert_error('Error', 'Enter Exchange Rate.');
        return response;
    }

    var fecha = $(FechaD).selectpicker('val')
    if ($(fecha).val() == 0 || fecha == null) {
        sweet_alert_error('Error', 'choose fecha.');
        return response;
    }

    var amount2 = parseFloat($("#txtAmountD").val());
    var amountTotal = parseFloat($('#inputTotalFooter').text());
    //if (amount2 > amountTotal || amount2 < amountTotal) {
    //    sweet_alert_error('Error', 'different total amounts.');
    //    return response;
    //}

    response = true;
    return response;
}

function obligatoryP() {
    var response = false;

    var TypePaymentD = $(selectTypePaymentsP2).selectpicker('val')
    if ($(TypePaymentD).val() == 0 || TypePaymentD == null) {
        sweet_alert_error('Error', 'choose Type Payment.');
        return response;
    }

    var CurrencyP = $(selectCurrencyP).selectpicker('val')
    if ($(CurrencyP).val() == 0 || CurrencyP == null) {
        sweet_alert_error('Error', 'choose Currency.');

        return response;
    }

    var number = $("#txtNumberP").val();
    if (number == "") {
        sweet_alert_error('Error', 'Enter number.');
        return response;
    }

    var amount = $("#txtAmountP").val();
    if (amount <= 0) {
        sweet_alert_error('Error', 'Enter amount.');
        return response;
    }

    var exchangeRate = $("#txtExchangeRateP").val();
    if (exchangeRate <= 0) {
        sweet_alert_error('Error', 'Enter Exchange Rate.');
        return response;
    }

    var fecha = $(DateP).selectpicker('val')
    if ($(fecha).val() == 0 || fecha == null) {
        sweet_alert_error('Error', 'choose fecha.');
        return response;
    }

    var amount2 = parseFloat($("#txtAmountP").val());
    var amountTotal = parseFloat($('#totalCurrencyPtable').text());
    if (amount2 > amountTotal || amount2 < amountTotal) {
        sweet_alert_error('Error', 'different total amounts.');
        return response;
    }
    //<<<validación para los pagos no sean mayores
    //>>>Capturamos el monto de los pagos
    var amountP = 0;
    var amountD = 0;
    $("#tblPayments tr").find('td:eq(0)').each(function () {
        var vpayment = $(this).parent().find("#amount").html();
        if (vpayment != undefined) {

            amountP = parseFloat(vpayment.substring(1, vpayment.length)) + amountP
        }
    })

    //>>>Capturamos el monto de los documentos
    $("#tblDocuments tr").find('td:eq(0)').each(function () {
        var vdocument = $(this).parent().find("#localCurrencySymbol").html();
        if (vdocument != undefined) {

            amountD = parseFloat(vdocument.substring(1, vdocument.length)) + amountD
        }
    })

    amountP = amountP + parseFloat($("#txtAmountP").val());

    //if (amountP > amountD) {
    //    sweet_alert_error('Error', 'The amount billed is less than the payment.');
    //    return response;
    //}

    response = true;
    return response;
}
