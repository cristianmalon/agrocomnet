﻿var _campaignID = localStorage.campaignID;
var userID = 0
var dataSize = []
var dataCategory = []
var bSetLastDataSaved = false;
var measure = localStorage.measure;
var _dataFarm = [];

$(function () {
  $(document).ajaxComplete(function () {
    sweet_alert_progressbar_cerrar();
  });

    userID = $("#lblSessionUserID").text();

    loadCombosWeek();
    loadNextWeek();
    updateDate();

    $(document).on("click", "#btnUploadForeCast", function () {
        showModalUploadForeCast();
    });

    $(document).on("change", "#cboGROWER", function () {
        //loadSize();
        loadFarm();
    });

    $(document).on("change", "#cboFarm", function () {
        loadStage();
    });

    $(document).on("change", "#cboStage", function () {
        loadLots();
        loadSize();
    });

    $(document).on("change", "#cboLot", function () {
        loadSize();
        loadDataTable();
    });

    $(document).on("change", "#CboNext", function () {
        loadDataTable();
    });

    $(document).on("click", "#btnSaveForecast", function () {
        saveForecast();
    });

    $.ajax({
        type: "GET",
        url: "/Forecast/ListFarmByCampaign?opt=" + 'for' + "&id=" + localStorage.campaignID,
        //async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var _dataFarm = dataJson.map(
                    obj => {
                        return {
                            "id": obj.farmID,
                            "name": obj.description
                        }
                    }
                );
                loadComboFilter(_dataFarm, 'farmF', true, 'ALL');
                $('#farmF').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
})

function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    if (data != null)
        for (var i = 0; i < data.length; i++) {
            content += "<option value='" + data[i].id + "'>";
            content += data[i].name;
            content += "</option>";
        }
    $('#' + control).empty().append(content);
}

function ListData() {
    let farmID = $("#farmF").val();
    if (farmID == 'All') {
        farmID = '';
    }
    fnListProjectedProductionByVariety(userID, $("#cboSemanaIni").find('option:selected').val(), $("#cboSemanaFin").find('option:selected').val(), _campaignID, farmID, successProjectedProductionByVariety);
}

var successProjectedProductionByVariety = function (data) {
    let html = '';
    let html2 = '';
    var bRsl = false;
    let TotalQuantity = 0;
    if (data == null || data.length == 0) {
        sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again later.');
    } else {
        var lstData = JSON.parse(data);
        if (lstData == null || lstData.length == 0) {
            sweet_alert_info('Information', 'Nothing to show according to filters. Try again.');
            return;
        } else {
            $.each(lstData, function (i, e) {
                //$('#updateDate').val(e.dateModified);
                html += '<tr role="row" class="odd">';
                html += '<td style="text-align: center;" class="sorting_1">' + e.grower + '</td>';
                html += '<td style="text-align: center;" class="sorting_1">' + e.farm + '</td>';
                html += '<td style="text-align: center;" class="sorting_1">' + e.week + '</td>';
                html += '<td style="text-align: center;" class="sorting_1">' + e.variety + '</td>';
                html += '<td style="text-align: center;" class="sorting_1">' + e.category + '</td>';
                html += '<td style="text-align: center;" class="sorting_1">' + e.size + '</td>';
                TotalQuantity += e.quantity;
                html += '<td style="text-align: center;" class="sorting_1">' + e.quantity + '</td>';
                html += '</tr>';
            });
            html2 += '<tr>'
            html2 += '<td colspan="6" style="text-align: right;" rowspan="1">TOTAL ' + measure + '</td>';
            html2 += '<td style="text-align: right;" rowspan="1" colspan="1">' + parseFloat(TotalQuantity).toFixed(2) + '</td>';
            html2 += '</tr>'
            $("table#TblForecastRecords").DataTable().destroy();

            //sweet_alert_progressbar();
            $("table#TblForecastRecords>tbody").empty().append(html);
            $("table#TblForecastRecords>tfoot").empty().append(html2);
            $("table#TblForecastRecords").DataTable({
                //dom: 'Bfrtip',	
                //searching: true,
                //"info": true,
                //"lengthChange": true,
                select: true,
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'All']
                ],
                "language": {
                    "search": "Filtrar"
                },
                dom: 'Bfrtip',
                buttons: [
                    //'excel'
                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'
                ]
            });
        }
    }
}

table = $('#tblSales').DataTable({

    dom: 'Bfrtip',
  buttons: [
    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
    'pageLength'
  ],
    orderCellsTop: true,
    fixedHeader: true,
    ordering: false,
    searching: true
});

function updateDate() {

    $.ajax({
        type: "GET",
        url: "/Forecast/GetForecastForPlan?opt=" + "rep" + "&campaignID=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                $('#updateDate').val(dataJson[0].dateModified);
            }
        }
    });
}

$(document).ready(function () {
    $('#tblSales thead tr').clone(true).appendTo('#tblSales thead');
    $('#tblSales thead tr:eq(1) th').each(function (i) {
        if (i > 0) {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');
        } else {
            $(this).html(' ');
        }
        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table.column(i).search(this.value).draw();
            }
        });
    });

});

function getTotalForecast() {
    var projectedWeekID = $("#CboNext").val();
    var loteID = $("#cboLot").val();
    fnTotalForecast(projectedWeekID, loteID, successTotalForecast);
}

var successTotalForecast = function (data) {
    if (data.length < 0) {
        $("#inputKG").val(0);
        $("#inputCmnt").val("");
    } else {
        var ObjData = JSON.parse(data);
        $('#cboGROWER').val(ObjData[0].growerID);
        $("#inputKG").val(ObjData[0].quantity);
        $("#inputCmnt").val(ObjData[0].commentary);
        $(cboGROWER).change();
    }
    bSetLastDataSaved = true;
}

function saveForecast() {
    if ($(inputKG).val() == '' || parseFloat($(inputKG).val()) == 0) {
        sweet_alert_error('Error!', 'Total KG must be inserted');
        return;
    }
    var TotRow = 0;
    var TotCol = 0;
    var bRsl = false;
    $('table#TblForecastUpdate>tbody>tr').each(function (i, e) {
        if (i != 0) return;
        TotRow = parseFloat($(e).closest('tr').find('input[name=inputTotalAll]').val());
        if (TotRow != 100) {
            sweet_alert_error('Error!', 'Total for all category must be 100%');
            $(e).closest('tr').find('input[name=inputTotalAll]').css('color', 'red');
            bRsl = false;
            return false;
        }
        bRsl = true;
    });
    if (!bRsl) return;

    var totaltd = 0;
    $('table#TblForecastUpdate>tfoot>tr>td').each(function (i, e) {
        if (i == 0) return;
        TotCol = parseFloat($(e).closest('td').find('input[name=inputTotalSize]').val());
        totaltd = totaltd + TotCol;

        if (TotCol != 0 && (TotCol < 100 || TotCol > 100)) {
            sweet_alert_error('Error!', 'Total per each zise must be 100%');
            $(e).closest('td').find('input[name=inputTotalSize]').css('color', 'red');
            bRsl = false;
            return false;
        }
        bRsl = true;
    });
    if (!bRsl) return;

    var dataForecast = [];
    $("table#TblForecastUpdate>tbody>tr").each(function (rowTR, objTR) {
        $.each(dataSize, function (iSize, size) {
            var o = {}
            if (rowTR == 0) {
                o.description1 = "ALL";

            } else {
                $.each(dataCategory, function (iCat, cat) {
                    if (rowTR == iCat + 1) {
                        o.description1 = cat.id;
                    }
                });
            }
            o.description2 = size.id;
            o.description3 = $(objTR).find('input:eq(' + iSize + ')').val()
            dataForecast.push(o);
        });
    })
    dataForecast.percentBySize_table = dataForecast;
    
    var frmDet = new FormData();
    frmDet.append("userID", $(lblSessionUserID).text());
    frmDet.append("lotID", $(cboLot).val());
    frmDet.append("projectedWeekID", $(CboNext).val());
    frmDet.append("quantityTotal", $(inputKG).val());
    frmDet.append("commentary", $(inputCmnt).val());
    frmDet.append("percentBySize_table", JSON.stringify(dataForecast));

    fnSaveUploadForecast(JSON.stringify(Object.fromEntries(frmDet)), successSaveForeCast2, errorSaveForeCast);
}

var successSaveForeCast2 = function (data) {
    sweet_alert_success('Good Job!', 'Data Saved.');
}

var errorSaveForeCast = function (data) {
    sweet_alert_warning('Error!', 'Fail to save data.');
}

function showModalUploadForeCast() {
    loadDefaultValues();
    fnloadTitleForm(localStorage.cropID, successloadTitleFor);
    loadDataTable();
}

var successloadTitleFor = function (data) {

    var dataJson = JSON.parse(data);
    var data = dataJson.map(
        obj => {
            return {
                "value": obj.settingValue,
            }
        }
    );
    $(txtTitleModal).text(data[0].value);
    $(textTotalKg).text(data[1].value);
}

function loadStage() {
    var grower = $("#cboGROWER").val();
    var farmID = $("#cboFarm").val();
    var cropID = localStorage.cropID;
    fnloadStage(cropID, grower, farmID, successloadStage);
}

var successloadStage = function (data) {
    var dataJson = JSON.parse(data);
    var data = dataJson.map(
        obj => {
            return {
                "id": obj.stageID,
                "name": obj.description
            }
        }
    )
    loadCombo(data, "cboStage", false)
    $(cboStage).change()
}

function loadFarm() {
    var growerID = $("#cboGROWER").val();
    var cropID = localStorage.cropID;
    fnloadFarm(growerID, cropID, successloadFarm);
}

var successloadFarm = function (data) {
    var dataJson = JSON.parse(data);
    var dataFarm = dataJson.map(
        obj => {
            return {
                "id": obj.farmID,
                "name": obj.description
            }
        }
    )    
    loadCombo(dataFarm, "cboFarm", false);
    $(cboFarm).change();
}

function loadLots() {
    var stageID = $("#cboStage").val();

    fnloadLots(stageID, successloadLots);
}

var successloadLots = function (data) {
    var dataJson = JSON.parse(data);
    var dataLot = dataJson.map(
        obj => {
            return {
                "id": obj.lotID,
                "name": obj.variety
            }
        }
    )
    loadCombo(dataLot, "cboLot", false)
    $(cboLot).selectpicker('refresh')
    $(cboLot).change()
}

function loadDefaultValues() {
    $('#cboGROWER option:eq(0)').prop('selected', true);
    $(cboGROWER).change();
}

function loadCombosWeek() {
    fnloadCombosWeek(userID, localStorage.cropID, fnSuccessValidate);
    fnListWeekByCampaign(localStorage.campaignID, fnsuccessListWeekByCampaign);
}

var fnsuccessListWeekByCampaign = function (data) {
    var dataJson = JSON.parse(data);
    var data = dataJson.map(
        obj => {
            return {
                "id": obj.projectedWeekID,
                "name": obj.description
            }
        }
    )
    loadCombo(data, "cboSemanaIni", false)
    loadCombo(data, "cboSemanaFin", false)
    ListData();
}

var fnSuccessValidate = function (data) {
    var dataJson = JSON.parse(data);
    var data = dataJson.map(
        obj => {
            return {
                "id": obj.growerID,
                "name": obj.businessName
            }
        }
    )
    loadCombo(data, "growerF", false);
    loadCombo(data, "cboGROWER", false);
}

function loadNextWeek() {
    fnloadNextWeek(localStorage.cropID, localStorage.campaignID, successloadNextWeek);
}

var successloadNextWeek = function (data) {
    var dataJson = JSON.parse(data);
    var data = dataJson.map(
        obj => {
            return {
                "id": obj.projectedWeekID,
                "name": obj.description
            }
        }
    )
    loadCombo(data, "CboNext", false)
}

function loadSize() {
    var cropID = localStorage.cropID;
    var growerID = $("#cboGROWER").val();
    var lotID = $('#cboLot').val();
    fnloadSize(cropID, growerID, lotID, successloadSize);
}

var successloadSize = function (data) {
    var dataJson = JSON.parse(data);
    dataSize = dataJson.map(
        obj => {
            return {
                "id": obj.sizeID,
                "name": obj.code
            }
        }
    )
    loadCategory();
}

function loadCategory() {
    var growerID = $("#cboGROWER").val();
    var cropID = localStorage.cropID;
    fnloadCategory(cropID, growerID, successloadCategory);
}

var successloadCategory = function (data) {
    var dataJson = JSON.parse(data);
    dataCategory = dataJson.map(
        obj => {
            return {
                "id": obj.categoryID,
                "name": obj.category
            }
        }
    )
    loadTable()
}



function loadTable() {
    var contenido = '';
    contenido += "<thead class='thead-agrocom'>";
    contenido += "<tr style='text-align:center;'>";
    contenido += "<th>Category</th>";
    $.each(dataSize, function (i, e) {
        contenido += "<th>" + e.name + "</th>";
    })
    contenido += "<th style='width:10%'></th>";
    contenido += "</tr>";
    contenido += '</thead>'
    contenido += "<tbody style='text-align:right'>";
    contenido += "<tr style='background:silver'><td>ALL</td>";
    $.each(dataSize, function (i, e) {
        contenido += "<td >" + "<input style='text-align:right' name='inputCellSize' onblur=CalculateHorizontalValues(this,'TblForecastUpdate','inputCellSize','inputTotalAll') type='number' class='form-control form-control-sm' min=0 max=100 value='0'/>" + "</td>";
    })
    contenido += "<td style='width:10%'>" + "<input disabled name='inputTotalAll'  class='form-control form-control-sm'  style='text-align:right;width:100%' type='number' min=0 max=100 value='0'/>" + "</td>";
    contenido += "</tr>";
    $.each(dataCategory, function (ii, ee) {
        contenido += "<tr>";
        contenido += "<td>" + ee.name + "</td>";
        $.each(dataSize, function (i, e) {
            contenido += "<td>" + "<input style='text-align:right' name='inputCellSize' onblur=CalculateFooterValues(this,'TblForecastUpdate','inputCellSize','inputTotalSize') type='number'  class='form-control form-control-sm'  min=0 max=100 value='0'/>" + "</td>";
        })
        contenido += "</tr>";
    })
    contenido += "</tbody>";
    contenido += "<tfoot style='text-align:right'>";
    contenido += "<tr>";
    contenido += "<td></td>";
    $.each(dataSize, function (i, e) {
        contenido += "<td>" + "<input class='inputTotalSize' name='inputTotalSize' disabled style='text-align:right'  class='form-control form-control-sm'  type='number' min=0 max=100 value='0'/>" + "</td>";
    })
    contenido += "</tr>";
    contenido += "</tfoot>";
    $('table#TblForecastUpdate').empty().html(contenido);
    //calcularFooterSizes();
}

function loadDataTable() {
    fnListProjectedProduction($("#CboNext").val(), $("#cboLot").val(), successProjectedProduction);
}

var successProjectedProduction = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        
        var dataAll = dataJson.filter(item => item.tcategoryID.trim() == '0')
        if (dataAll.length > 0) {

            $.each(dataSize, function (i, e) {
                var sizeCode = '';
                if (isNaN(e.name.substring(1, 2))) { //es letra jj
                    sizeCode = e.name.toLowerCase();
                } else { //es numero T12UP
                    sizeCode = e.name.substring(0, 2).toLowerCase() + e.name.substring(2, 5).toLowerCase()
                }
                var xx = dataAll[0][sizeCode]
                $("table#TblForecastUpdate>tbody>tr:eq(0)").find('input:eq(' + i + ')').val(parseInt(xx))
            })
        }

        $.each(dataCategory, function (ii, ee) {
            var dataCat = dataJson.filter(item => item.tcategoryID.trim() == ee.id.trim())

            if (dataCategory.length > 0) {

                $.each(dataSize, function (i, e) {
                    var sizeCode = '';
                    if (isNaN(e.name.substring(1, 2))) {
                        sizeCode = e.name.toLowerCase();
                    } else {
                        sizeCode = e.name.substring(0, 2).toLowerCase() + e.name.substring(2, 5).toLowerCase()
                    }
                    var xx = dataCat[0][sizeCode];
                    $("table#TblForecastUpdate>tbody>tr:eq(" + (parseInt(ii) + parseInt(1)) + ")").find('input:eq(' + i + ')').val(parseInt(xx))
                })
            }
        })

    }
}
