﻿var _dataWeek = [];
var GWeek;
var GWeekID;
var GGrowerID;
var GGrowerName;
var GFarm;
var GStage;
var GCategory;
var GVariety= [];
var GSize;
var dataMasiveForecast=[];
var flag = 0;
var DataModified = [];
var head;
var aRowsEdited = [];
var idOrigin;
var growerID;
var _campaignID = localStorage.campaignID;

$(document).ready(function () {
    $("#btnShowMassiveUpdate").click(function (e) {
        
        loadComboGrowerE();
    });
    $("#btnSaveMassiveUpdate").click(function (e) {
        Swal.fire({
            title: '¿Want to save?',
            text: "Do you want to execute this action?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, save!',
            cancelButtonText: 'No, cancel!',
            confirmButtonColor: '#4CAA42',
            cancelButtonColor: '#d33'
        }).then((result) => {
            if (result.value) {
                SaveMasiveForecast();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                sweet_alert_error('Canceled!', 'Save process was canceled.');
            }
        });

    });
    $("#btnUpload").click(function (e) {
        var files = document.getElementById('fileExcel').files;
        var reader = new FileReader();
        DataModified = [];
        if (files == null || files == undefined) {
            $('#fileExcel').val(null);
            sweet_alert_error('Error', 'Must choose an excel file.');
            return;
        } else if (files.length == 0) {
            $('#fileExcel').val(null);
            sweet_alert_error('Error', 'Must choose an excel file.');
            return;
        }
        reader.readAsDataURL(files[0]);
        reader.onload = function () {
            ReadExcel(reader.result);
            $(".btnDelete").prop("show", true);
            $(".lblFile").prop("show", true);
            $(".btnUpload").prop("show", false);
            $(".inputFile").hide();
        }
    });

    $("#fileExcel").change(function (e) {
        if ($("#fileExcel").length > 0) {
            $(".btnDelete").prop("hidden", false);
            $(".btnDelete").show();
            $(".lblFile").prop("hidden", false);
            $(".lblFile").show();
            $(".btnUpload").prop("hidden", false);
            $(".btnUpload").show();
            var filePrueba = $("#fileExcel").val();
            var filePrueba1 = filePrueba.split('\\');
            $(".lblFile").text(filePrueba1[2]);
            $(".inputFile").hide();
        }
        checkfile(this);
    });

    $("#btnDeleteFile").click(function (e) {
        dropFile(this);
    });
});

function loadComboGrowerE() {
    var userID = $("#lblSessionUserID").text();
    fnloadCombosWeek(userID, localStorage.cropID, successloadComboGrowerE);
}

var successloadComboGrowerE = function (data) {
    var dataJson = JSON.parse(data);
    if (dataJson.length > 0) {
        var dataWeek = dataJson.map(
            obj => {
                return {
                    "id": obj.growerID,
                    "name": obj.businessName
                }
            }

        );
        $("#inputKG").val("");
        $("#inputCmnt").val("");
        //noData("listWeekLote");
        loadCombo(dataWeek, 'cboGROWER2', false);
        $("#cboGROWER2").val(dataWeek[0].id).change();
    }
}

function ProccessExcelData(data) {
    //console.log(data);
    var DataFromExcel = [];
    var DataRow = {};
    var msg = "";
    var PrintErrors = "";
    var GrowerID;
    var FarmID;
    var LotID;
    var StageID;
    var CategoryID;
    var VarietyID;
    var SizeID;
    var mount1;
    var mount2;
    var mount3;
    var mount4;
    var today = new Date();
    var yyyy = today.getFullYear();

    if (head[0].substr(1) != "GROWER" || head[2].substr(1) != "FARM" || head[3].substr(1) != "STAGE" ||
        head[6].substr(1) != "VARIETY" || head[8].substr(1) != "CATEGORY" || head[9].substr(1) != "SIZE") {
        msg += "Wrong Headers: <br> \n";
        if (head[4] != "GROWER") {
            msg += " Incorrect header in Column 1(Grower) " + "<br> \n";
        }
        if (head[5] != "FARM") {
            msg += " Incorrect header in Column 2(Farm) " + "<br> \n";
        }
        if (head[6] != "STAGE") {
            msg += " Incorrect header in Column 3(Stage) " + "<br> \n";
        }
        if (head[7] != "VARIETY") {
            msg += " Incorrect header in Column 4(Variety) " + "<br> \n";
        }
        if (head[8] != "CATEGORY") {
            msg += " Incorrect header in Column 5(Category) " + "<br> \n";
        }
        if (head[9] != "SIZE") {
            msg += " Incorrect header in Column 6(Size) " + "<br> \n";
        }
        if (parseInt(head[10].substr(1)) != parseInt(GWeek) + 1 || parseInt(head[11].substr(1)) != parseInt(GWeek) + 2 ||
            parseInt(head[12]).substr(1) != parseInt(GWeek) + 3 || parseInt(head[13].substr(1)) != parseInt(GWeek) + 4) {
            msg += " Incorrect Weeks in Column 1,2,3 or 4" + "<br> \n";
        }
        PrintErrors += msg;
        msg = "";
    } else {
        $.each(data, function (i, e) {

            var w1 = head[10].substr(1);
            var w2 = head[11].substr(1);
            var w3 = head[12].substr(1);
            var w4 = head[13].substr(1);

            mount1 = e[w1];
            mount2 = e[w2];
            mount3 = e[w3];
            mount4 = e[w4];

            if (VerifyGrower(e.GROWER, i)) {
                msg += "Error GROWER name ";
            } else {
                GrowerID = GGrowerID;
            }
            FarmID = VerifyFarm(GFarm, e.FARM.toUpperCase());
            if (FarmID == 0) {
                msg += "Error FARM name ";
            }

            StageID = VerifyStage(GStage, e.STAGE);
            if (StageID == 0) {
                msg += "Error STAGE name ";
            }

            CategoryID = VerifyCategory(GCategory, e.CATEGORY)
            if (CategoryID == 0) {
                
                msg += "Error CATEGORY name ";
            }

            VarietyID = VerifyVariety(GVariety, e.VARIETY);
            if (VarietyID == 0) {
                msg += "Error VARIETY name ";
            }

            SizeID = VerifySize(GSize, jQuery.trim(e.SIZE));
            if (SizeID == 0) {
                msg += "Error SIZE name ";
            }

            if (msg == "") {
                DataRow.GrowerID = GrowerID;
                DataRow.Campaign = yyyy;
                DataRow.FarmID = FarmID;
                DataRow.LotID = LotID;
                DataRow.StageID = StageID;
                DataRow.CategoryID = CategoryID;
                DataRow.VarietyID = VarietyID;
                DataRow.SizeID = SizeID;
                DataRow.Weekstart = w1;
                //Validamos que nose seteen valores indefinidos o nulos
                DataRow.mount1 = mount1 == undefined ? 0 : mount1;
                DataRow.mount2 = mount2 == undefined ? 0 : mount2;
                DataRow.mount3 = mount3 == undefined ? 0 : mount3;
                DataRow.mount4 = mount4 == undefined ? 0 : mount4;
                //console.log("W1:" + mount1 + " W2:" + mount2 + " W3:" + mount3 + " W4:" + mount4);
                DataFromExcel.push(DataRow);
                DataRow = {};
            } else {
                msg += " in ROW " + (i + 2) + "<br> \n";
            }

            PrintErrors += msg;
            msg = "";
        });
        //console.log('Errores hasta el momento:' + PrintErrors)
    }
    //console.log("Tamaño DataFromExcel:" + DataFromExcel.length);
    if (GVariety.length == 0) {
        sweet_alert_warning("Warning", "Varieties were not found according to grower");
        return;
    }
    if (PrintErrors == "") {
        DrawInTable(DataFromExcel);
        sweet_alert_info('Information for user!', "Data processed correctly.");
        $("#btnSaveMassiveUpdate").prop('disabled', false);
    } else {
        //console.log('Errores hasta el momento:' + PrintErrors)
        sweet_alert_error('Error', 'WRONG IDENTIFIERS: <br>\n' + PrintErrors);
    }
}

function getGlobalGrower() {
    $("#btnSaveMassiveUpdate").prop('disabled', true);
    //var ObjData1;
    //var ObjData2;
    //var ObjData3;
    //var ObjData4;
    //var ObjData5;
    //var ObjData6;

    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/Forecast/ListWeekByCampaign?campaignID=" + localStorage.campaignID,
        success: function (data) {
            if (data.length > 0) {
                _dataWeek = JSON.parse(data);

            }
        }
    });

    growerID = $("#cboGROWER2").val();
    farmID = $('#SelectFarm').val();

    fnloadCurrentWeek(localStorage.cropID, localStorage.campaignID, successloadCurrentWeek);

    fnloadGrowerByID(growerID, successloadGrowerByID);

    fnloadFarm(growerID, localStorage.cropID, successloadFarmMassiveUpdate);

    fnloadStageByGrower(localStorage.cropID, growerID, farmID, successloadStageMassiveUpdate);

    fnloadBrandsByGrowerCrop(localStorage.cropID, growerID, successloadCategories);

    fnloadVarietiesByGrower(growerID, successloadVarieties);

    fnloadSizesByGrower(localStorage.cropID, growerID, successloadSizes);

    //fnloadForecastUnified(growerID, farmID, localStorage.cropID, idOrigin, successForecastUnified);
}

var successForecastUnified = function (data) {
    ObjData = JSON.parse(data);
        
        //load week
        GWeekID = ObjData.table[0].projectedWeekID;
        GWeek = ObjData.table[0].number;

        //load growers 
        $.each(ObjData.table1, function (i, e) {
            if (i == 0) {
                GGrowerID = growerID;
                GGrowerName = e.businessName;
                idOrigin = e.originID;
            }
        })

        //load Farm
        GFarm = ObjData.table2;

        //Load Stage
        GStage = ObjData.table3;

        //Load Categories
        GCategory = ObjData.table4;

        //Load Varieties
        GVariety = ObjData.table5;

        //Load Sizes
        GSize = ObjData.table6;
        
}

var successloadCurrentWeek = function (data) {
    ObjData0 = JSON.parse(data);
    GWeekID = ObjData0[0].projectedWeekID;
    GWeek = ObjData0[0].number;
}

var successloadGrowerByID = function (data) {
    var ObjData1 = JSON.parse(data);
    $.each(ObjData1, function (i, e) {
        if (i == 0) {
            GGrowerID = growerID;
            GGrowerName = e.businessName;
            idOrigin = e.originID;
        }
    })
}

var successloadFarmMassiveUpdate = function (data) {
    var ObjData2 = JSON.parse(data);
    //console.log("FARMS:" + JSON.stringify(ObjData2));
    GFarm = ObjData2;
}

var successloadStageMassiveUpdate = function (data) {
    var ObjData3 = JSON.parse(data);
    //console.log("STAGES:" + JSON.stringify(ObjData3));
    GStage = ObjData3;
}

var successloadCategories = function (data) {
    var ObjData4 = JSON.parse(data);
    //console.log("BRANDS:" + JSON.stringify(ObjData4));
    GCategory = ObjData4;
}

var successloadVarieties = function (data) {
    var ObjData5 = JSON.parse(data);
    //console.log("VARIETYS:" + JSON.stringify(ObjData5));
    GVariety = ObjData5;
}

var successloadSizes = function (data) {
    var ObjData6 = JSON.parse(data);
    //console.log("SIZES:" + JSON.stringify(ObjData6));
    GSize = ObjData6;
}

$('#cboGROWER2').change(function () {
    dataMasiveForecast = [];
    getGlobalGrower();
    loadComboFarm();
})

$('#SelectFarm').change(function () {
    GetMasiveForeCast();
    getGlobalGrower();
})

function GetMasiveForeCast() {
	/*
	var sUrlApi = "GetMasiveForecast/?growerID=" + GGrowerID + "&campaignID=" + localStorage.campaignID;
	var bRsl = false;
	$.ajax({
		type: "GET",
		url: sUrlApi,
		async: false,
		success: function (data) {
			if (data == null || data.length == 0) {
				sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
			} else {
				var dataJson = JSON.parse(data);
				if (dataJson == null || dataJson.length == 0) {
					sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
				} else {
					var _dataJson = JSON.parse(data);
					dataMasiveForecast = _dataJson.filter(item => item.bFARMID.trim() == $('#SelectFarm option:selected').val().trim());
					if (dataMasiveForecast.length == 0) {
						$("table#tblExcel").empty();
						sweet_alert_error('Error', 'No data found for the next weeks !!');
					} else {
						bRsl = true;

					}
				}
			}
		},
		error: function (datoEr) {
			sweet_alert_info('Information', "There was an issue trying to list data.");
		},
		complete: function () {
			if (bRsl) {
				sweet_alert_progressbar();
				ShowDataInTable(dataMasiveForecast);
			}
		},
	});
	*/
    fnListMasiveForecast(GGrowerID, localStorage.cropID, successListMassiveForeCast);
}

var successListMassiveForeCast = function (data) {
    
    if (data == null || data.length == 0) {
        sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
    } else {
        var dataJson = JSON.parse(data);
        if (dataJson == null || dataJson.length == 0) {
            sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
        } else {
            //console.log(data);
            var _dataJson = JSON.parse(data);
            dataMasiveForecast = _dataJson.filter(item => item.bFARMID.trim() == $('#SelectFarm option:selected').val().trim());
            if (dataMasiveForecast.length == 0) {
                $("table#tblExcel").empty();
                sweet_alert_error('Error', 'No data found for the next weeks !!');
            } else {
                //sweet_alert_progressbar();
                ShowDataInTable(dataMasiveForecast);
            }
        }
    }
}

function ShowDataInTable(_ObjData) {

    if (flag != 0) {
        $('table#tblExcel').DataTable().destroy();
    }

    var th = "";
    var tb = "";
    head = Object.keys(_ObjData[0]);
    var h1 = head[10];
    var h2 = head[11];
    var h3 = head[12];
    var h4 = head[13];

  th += "<thead class='thead-agrocom' >"
    th += "<tr>";
    th += "<th>[Nro.]</th>";
    $.each(head, function (i, e) {
        if (i >= 0 && i <= 9) {
            if (i == 1 || i == 4 || i == 5 || i == 7) return;
            th += "<th>";
            th += e.toUpperCase().substring(1);
            th += "</th>";
        }
    });
    $.each(head, function (i, e) {
        if (i > 9) {
            th += "<th>";
            th += e.substring(1) + "-INI";
            th += "</th>";
            th += "<th>";
            th += e.substring(1) + "-FIN";
            th += "</th>";
            //console.log("e: " + e);
        }

    });
    th += "</tr>";
    th += "</thead >";
    $("table#tblExcel").empty().append(th);
    //console.log('Cabecera:' + th);
  tb += "<tbody class='table-tr-agrocom'>";
    $.each(_ObjData, function (i, e) {
        tb += "<tr>";
        tb += "<td>" + parseInt(i + 1) + "</td>";
        tb += "<td>" + e.aGROWER + "</td>";
        //tb += "<td>" + e.bFARMID + "</td>";
        tb += "<td>" + e.cFARM + "</td>";
        tb += "<td>" + e.dSTAGE + "</td>";
        //tb += "<td>" + e.eLOTID + "</td>";
        //tb += "<td>" + e.fVARIETYID + "</td>";
        tb += "<td>" + e.gVARIETY + "</td>";
        //tb += "<td>" + e.hCATEGORYID + "</td>";
        tb += "<td>" + e.iCATEGORY + "</td>";
        tb += "<td>" + e.jSIZE + "</td>";
        if (e[h1] == null) {
            tb += "<td>" + 0 + "</td>";
        } else {
            tb += "<td>" + e[h1] + "</td>";
        }
        tb += "<td style='background-color:lightgreen' class='td1'>" + 0 + "</td>";

        if (e[h2] == null) {
            tb += "<td>" + 0 + "</td>";
        } else {
            tb += "<td>" + e[h2] + "</td>";
        }
        tb += "<td style='background-color:lightgreen' class='td2'>" + 0 + "</td>";

        if (e[h3] == null) {
            tb += "<td>" + 0 + "</td>";
        } else {
            tb += "<td>" + e[h3] + "</td>";
        }
        tb += "<td style='background-color:lightgreen' class='td3'>" + 0 + "</td>";

        if (e[h4] == null) {
            tb += "<td>" + 0 + "</td>";
        } else {
            tb += "<td>" + e[h4] + "</td>";
        }
        tb += "<td style='background-color:lightgreen' class='td4'>" + 0 + "</td>";

        tb += "</tr>";
    });
    tb += "</tbody>";
    $("table#tblExcel").append(tb);
    //console.log(tb);
    $(document).ready(function () {
        $('#tblExcel').DataTable();
    });
    flag = 1;
}

function VerifyGrower(grower) {
    var cleanGrower = grower.replace(/ /g, "").toUpperCase();
    //console.log("GROWERID: " + GGrowerID + "GROWER: " + GGrowerName + "/" + cleanGrower);
    if (jQuery.trim(GGrowerName.replace(/ /g, "").toUpperCase()) != jQuery.trim(cleanGrower)) {
        return true;
    } else {
        return false;
    }
}

function VerifyFarm(data, farm) {
    var isinFarm = false;
    var farmID;
    $.each(data, function (i, e) {
        var farmName = (e.description).replace(/ /g, "").toUpperCase();
        if (farmName == farm.replace(/ /g, "").toUpperCase()) {
            isinFarm = true;
            farmID = e.farmID;
        }
        else {
            if (farmName == farm.replace(/ /g, "").toUpperCase().replace("FUNDO", "")) {
                isinFarm = true;
                farmID = e.farmID;
            }
        }
        //console.log("FARM: " + farm.replace(/ /g, "").toUpperCase() + "///" + farm.replace(/ /g, "").toUpperCase().replace("FUNDO", ""));
    });
    if (isinFarm) {
        return farmID;
    } else {
        return 0;
    }
}

function VerifyStage(dataStages, ExcelStage) {
    var isOK = false;
    var StageID;
    $.each(dataStages, function (i, s) {
        if (s.description == ExcelStage) {
            isOK = true;
            StageID = s.description;
        }
    });
    if (isOK) {
        return StageID;
    } else {
        return 0;
    }
}

function VerifyCategory(dataCategories, ExcelCategory) {
    var categoryID;
    var isOK = false;
    $.each(dataCategories, function (i, b) {
        if (b.description.replace(/ /g, "").toUpperCase() == ExcelCategory.replace(/ /g, "").toUpperCase()) {
            isOK = true;
            categoryID = b.categoryID;
        }
    });
    if (isOK) {
        return categoryID;
    } else {
        return 0;
    }
}

function VerifyVariety(dataVariety, ExcelVariety) {
    console.log(dataVariety);
    console.log(ExcelVariety);
    var varietyID;
    var isOK = false;
    if (dataVariety.length == 0) return -1;
    $.each(dataVariety, function (i, v) {

        if (v.name.replace(/ /g, "").toUpperCase() == ExcelVariety.replace(/ /g, "").toUpperCase()) {
            isOK = true;
            varietyID = v.varietyID;
        }
    });

    if (isOK) {
        return varietyID;
    } else {
        return 0;
    }
}

function VerifySize(dataSize, ExcelSize) {

    var splitSize = ExcelSize.split(' ');
    var sizeID;
    var isOK = false;
    //console.log("SIZELENGHT: " + splitSize.length);

    $.each(dataSize, function (i, s) {

        if (splitSize.length > 1) {
            if (s.sizeID.toUpperCase() == ("T" + splitSize[0])) {
                isOK = true;
                sizeID = s.sizeID;
            }
        } else {
            //console.log("SIZE: " + ExcelSize + "//" + s.sizeID.toUpperCase());

            if (jQuery.trim(s.sizeID.toUpperCase()) == ExcelSize.replace(/ /g, "").toUpperCase()) {
                isOK = true;
                sizeID = s.sizeID;
            }
        }

    });
    if (isOK) {
        return sizeID;
    } else {
        return 0;
    }
}

function ReadExcel(url) {
    /* set up XMLHttpRequest */
    var oReq = new XMLHttpRequest();
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function (e) {
        var info = readData();
        function readData() {
            var arraybuffer = oReq.response;
            /* convert data to binary string */
            var data = new Uint8Array(arraybuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");

            /* Call XLSX */
            var workbook = XLSX.read(bstr, { type: "binary" });

            /* DO SOMETHING WITH workbook HERE */
            var first_sheet_name = workbook.SheetNames[0];
            /* Get worksheet */
            var worksheet = workbook.Sheets[first_sheet_name];
            var info = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            //console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
            return info;
        }
        ProccessExcelData(info)
    }
    oReq.send();
}

function SaveMasiveForecast() {
    //1er.Obtener el total de filas
    var nFilas = DataModified.length;
    //2do.Obtener la cantidad de vueltas = total filas / 100, y si >= ###.05, redondear
    var nVueltas = (nFilas / 100).toFixed(2);
    var numero = ((nVueltas + "").split("."));
    if (parseInt(numero[1]) > 0) {
        nVueltas = parseInt(numero[0]) + 1;
    }
    //3er.Hacer un loop(each) x cantidad de vueltas y enviar los datos al ajax
    if (nVueltas == 1) {
        //Parseo de datos a formato json
        let jsonObjExcel = JSON.stringify({
            massiveUploadDetail: DataModified
        });
        //console.log('JsonDataFormat:' + jsonObjExcel);
        //$.ajax({
        //	type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        //	url: "SaveMasiveUploadForecast",
        //	data: jsonObjExcel,
        //	contentType: "application/json",
        //	Accept: "application/json",
        //	dataType: 'json',
        //	async: false,
        //	error: function (datoEr) {
        //		sweet_alert_error('Error', "There was an issue trying to saving.");
        //	},
        //	success: function (data) {
        //		Swal.fire({
        //			title: 'Information for user!',
        //			text: "Masive Data From Excel Saved Correctly.",
        //			icon: 'info',
        //			showCancelButton: false,
        //			confirmButtonText: 'Ok',
        //			confirmButtonColor: '#4CAA42',
        //		});
        //		GetMasiveForeCast();
        //	}
        //});
        fnSaveMasiveUploadForecast(jsonObjExcel, _campaignID, successSaveForeCast, errorSaveForeCast);
        aRowsEdited = [];
    }
    else {
        //console.log(DataModified);
        //console.log("Tot.Filas=" + nFilas);
        var iAux = 0;
        for (i = 1; i <= nVueltas; i++) {
            var JsonPrueba = [];
            //console.log("Vuelta Nro=" + i);
            //console.log("Valor h=" + iAux);
            iNro = 0;
            for (h = iAux + 1; h <= (i * 100); h++) {
                //console.log(h);
                if (h <= nFilas) {
                    JsonPrueba.push(DataModified[h - 1]);
                } else {
                    break;
                }
            }
            iAux = h - 1;
            let jsonObjExcel = JSON.stringify({
                massiveUploadDetail: JsonPrueba
            });
            //console.log(jsonObjExcel);
            //$.ajax({
            //	type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            //	url: "SaveMasiveUploadForecast",
            //	data: jsonObjExcel,
            //	contentType: "application/json",
            //	Accept: "application/json",
            //	dataType: 'json',
            //	async: false,
            //	error: function (datoEr) {

            //	},
            //	success: function (data) {
            //		bRsl = true;
            //	}
            //});
            fnSaveMasiveUploadForecast(jsonObjExcel, _campaignID, successSaveForeCast, errorSaveForeCast);

        }
        aRowsEdited = [];
        GetMasiveForeCast();
    }
}

var successSaveForeCast = function (data) {
    sweet_alert_info('Information for user!', "Masive Data From Excel Saved Correctly.");
    GetMasiveForeCast();
}
var errorSaveForeCast = function (data) {
    sweet_alert_error('Error', "There was an issue trying to saving.");
}

function DrawInTable(_DataFromExcel) {
    var ha = null;
    ha = (ha == null) ? 0 : ha;
    //console.log("HAA: " + ha);
    var h1 = head[10];
    var h2 = head[11];
    var h3 = head[12];
    var h4 = head[13];
    var iNro = 0;
    $('table#tblExcel').DataTable().destroy();
    //volvemos a pintar los estilos
    $.each(dataMasiveForecast, function (i, b) {
        $("#tblExcel>tbody>tr").eq(i).find("td.td1").css('color', 'black');
        $("#tblExcel>tbody>tr").eq(i).find("td.td2").css('color', 'black');
        $("#tblExcel>tbody>tr").eq(i).find("td.td3").css('color', 'black');
        $("#tblExcel>tbody>tr").eq(i).find("td.td4").css('color', 'black');
    });
    
    aRowsEdited = [];
 
    $.each(dataMasiveForecast, function (i, b) {
        $.each(_DataFromExcel, function (j, e) {
 
            if (jQuery.trim(e.FarmID) == jQuery.trim(b.bFARMID) && jQuery.trim(e.StageID) == jQuery.trim(b.dSTAGE) &&
                jQuery.trim(e.CategoryID) == jQuery.trim(b.hCATEGORYID) && jQuery.trim(e.VarietyID) == jQuery.trim(b.fVARIETYID) && jQuery.trim(e.SizeID).toUpperCase() == jQuery.trim(b.jSIZE).toUpperCase())
            {
                
                e.LotID = b.eLOTID;
                //$("#tblExcel>tbody>tr").eq(i).css("background-color", "red");                
                $("#tblExcel>tbody>tr").eq(i).find("td.td1").text(parseFloat(e.mount1).toFixed(0));

                b[h1] = b[h1] == null ? 0 : b[h1];
                b[h1] = b[h1] == undefined ? 0 : b[h1];
                b[h1] = b[h1] == "" ? 0 : b[h1];
                b[h1] = b[h1] == 0 ? 0 : b[h1];

                if (b[h1] != e.mount1) {
                    $("#tblExcel>tbody>tr").eq(i).find("td.td1").css('color', 'red');
                }
                $("#tblExcel>tbody>tr").eq(i).find("td.td2").text(parseFloat(e.mount2).toFixed(0));

                b[h2] = b[h2] == null ? 0 : b[h2];
                b[h2] = b[h2] == undefined ? 0 : b[h2];
                b[h2] = b[h2] == "" ? 0 : b[h2];
                b[h2] = b[h2] == 0 ? 0 : b[h2];

                if (b[h2] != e.mount2) {
                    $("#tblExcel>tbody>tr").eq(i).find("td.td2").css('color', 'red');
                } 

                $("#tblExcel>tbody>tr").eq(i).find("td.td3").text(parseFloat(e.mount3).toFixed(0));

                b[h3] = b[h3] == null ? 0 : b[h3];
                b[h3] = b[h3] == undefined ? 0 : b[h3];
                b[h3] = b[h3] == "" ? 0 : b[h3];
                b[h3] = b[h3] == 0 ? 0 : b[h3];

                if (b[h3] != e.mount3) {
                    $("#tblExcel>tbody>tr").eq(i).find("td.td3").css('color', 'red');
                }

                $("#tblExcel>tbody>tr").eq(i).find("td.td4").text(parseFloat(e.mount4).toFixed(0));

                b[h4] = b[h4] == null ? 0 : b[h4];
                b[h4] = b[h4] == undefined ? 0 : b[h4];
                b[h4] = b[h4] == "" ? 0 : b[h4];
                b[h4] = b[h4] == 0 ? 0 : b[h4];

                if (b[h4] != e.mount4) {
                    $("#tblExcel>tbody>tr").eq(i).find("td.td4").css('color', 'red');
                }

                if (((b[h1] = (b[h1] == null) ? 0 : b[h1]) != e.mount1) || ((b[h2] = (b[h2] == null) ? 0 : b[h2]) != e.mount2) ||
                    ((b[h3] = (b[h3] == null) ? 0 : b[h3]) != e.mount3) || ((b[h4] = (b[h4] == null) ? 0 : b[h4]) != e.mount4)) {
                    aRowsEdited.push(i + 1);

                    iNro += 1;
                    item = {}
                    item["ID"] = iNro;
                    item["LotID"] = e.LotID;
                    item["CategoryID"] = e.CategoryID;
                    item["SizeID"] = e.SizeID;
                    item["Campaign"] = e.Campaign;

                    var day = new Date();
                    var curHour = day.getHours();
                    var curMinute = day.getMinutes();
                    var cursecond = day.getSeconds();
                    if (day.getDay() == 4 && curHour >= 12 && curMinute >= 0 && cursecond > 0) {
                        item["Weekstart"] = parseInt(GWeekID + 2);
                    } else if (day.getDay() > 4) {
                        item["Weekstart"] = parseInt(GWeekID + 2);
                    } else { item["Weekstart"] = parseInt(GWeekID + 1); }

                    item["mount1"] = parseFloat(Math.ceil(e.mount1));
                    item["mount2"] = parseFloat(Math.ceil(e.mount2));
                    item["mount3"] = parseFloat(Math.ceil(e.mount3));
                    item["mount4"] = parseFloat(Math.ceil(e.mount4));
                    DataModified.push(item);
                }
            };
        });
    });

    $(document).ready(function () {
        $('#tblExcel').DataTable();
    });
}

function DownloadTemplate() {
    var createXLSLFormatObj = [];
    var head = Object.keys(dataMasiveForecast[0]);
    /* XLS Head Columns */
    var xlsHeader = ["GROWER", "FARM", "STAGE", "VARIETY", "CATEGORY", "SIZE", head[10].substr(1), head[11].substr(1), head[12].substr(1), head[13].substr(1)];

    /* XLS Rows Data */
    var xlsRows = dataMasiveForecast;

    createXLSLFormatObj.push(xlsHeader);
    $.each(xlsRows, function (index, value) {
        var innerRowData = [];
        innerRowData.push(value.aGROWER);
        innerRowData.push(value.cFARM);
        innerRowData.push(value.dSTAGE);
        innerRowData.push(value.gVARIETY);
        innerRowData.push(value.iCATEGORY);
        innerRowData.push(value.jSIZE);
        innerRowData.push(value[head[10]] = (value[head[10]] == null) ? value[head[10]] : 0);
        innerRowData.push(value[head[11]] = (value[head[11]] == null) ? value[head[11]] : 0);
        innerRowData.push(value[head[12]] = (value[head[12]] == null) ? value[head[12]] : 0);
        innerRowData.push(value[head[13]] = (value[head[13]] == null) ? value[head[13]] : 0);
        createXLSLFormatObj.push(innerRowData);
    });

    /* File Name */
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var curHour = today.getHours() > 12 ? today.getHours() : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
    var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    today = mm + '/' + dd + '/' + yyyy;
    var now = yyyy + mm + dd + "-" + curHour + curMinute;

    var getGrower = $("#cboGROWER2").find('option:selected').text();
    //console.log("GET GROWER: " + getGrower);
    var filename = getGrower + "_" + now + "_XLS.xlsx";

    /* Sheet Name */
    var ws_name = "FreakySheet";

    //if (typeof console !== 'undefined') //console.log(new Date());
    var wb = XLSX.utils.book_new();
    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

    /* Add worksheet to workbook */
    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    /* Write workbook and Download */
    //if (typeof console !== 'undefined') //console.log(new Date());
    XLSX.writeFile(wb, filename);
    //if (typeof console !== 'undefined') //console.log(new Date());
}

function checkfile(sender) {
    var validExts = new Array(".xlsx", ".xls");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
        $(sender).val('');
        sweet_alert_error('Error', "Invalid file selected, valid files are of " + validExts.toString() + " types.");
        return false;
    }
    else return true;

}

function loadComboFarm() {
    var growerID = $("#cboGROWER2 option:selected").val()
    var cropID = localStorage.cropID
    fnloadFarm(growerID, cropID, _successloadFarmMassiveUpdate);
}

var _successloadFarmMassiveUpdate = function (data) {
    
    var dataJson = JSON.parse(data);
    if (dataJson.length > 0) {
        var dataFarm = dataJson.map(
            obj => {
                return {
                    "id": obj.farmID,
                    "name": obj.description
                }
            }

        );
        loadCombo(dataFarm, 'SelectFarm', false);
        $("#SelectFarm").change();
        $("#SelectFarm").selectpicker('refresh');
    }
}

function dropFile(xthis) {
    $(xthis).parent('div').find('.btnDelete').hide();
    $(xthis).parent('div').find('.lblFile').hide();
    $(xthis).parent('div').find('.btnUpload').hide();
    $(xthis).parent('div').find('.inputFile').show();
    $(xthis).parent('div').find('.lblFile').text('');
    $(xthis).parent('div').find('.inputFile').val('');
}