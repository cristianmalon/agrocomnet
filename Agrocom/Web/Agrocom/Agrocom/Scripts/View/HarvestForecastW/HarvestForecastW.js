﻿$(function () {
	loadTitle();
	updateDate(); 	
	tablaForecastWeek();
	sweet_alert_progressbar_cerrar();
})

var measure = localStorage.measure;
var _dataWeek = [];

function updateDate() {

	$.ajax({
		type: "GET",
		url: "/Forecast/GetForecastForPlan?opt=" + "rep" + "&campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				$('#updateDate').val(dataJson[0].dateModified);				
			}
		}
	});
}

function tablaForecastWeek() {

	var cropID = localStorage.cropID;
	var seasonID = parseInt(localStorage.campaignID);

	$.ajax({
		type: "GET",
		url: "/HarvestForecastW/GetForecastByCropID?cropID=" + cropID + "&seasonID=" + seasonID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				var content = dataJson[0].column1;
				$("table#tblHarvestForecastW").append(content);
				calcTotal();
			}
		},
		complete: function () {
			$("table#tblHarvestForecastW").DataTable().destroy();
			var table= $('table#tblHarvestForecastW').DataTable({
				//dom: 'Bfrtip',	
				//searching: true,
				//"info": true,
				//"lengthChange": true,
				select: true,
				lengthMenu: [
					[25, 50, -1],
					['25 rows', '50 rows', 'All']
				],
				dom: 'Bfrtip',
				buttons: [
					
					{ extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true},
					'pageLength'
				],
				
				orderCellsTop: true,
				fixedHeader: true,
				ordering: false,
				
			});

			//$('a.toggle-vis').on('click', function (e) {
			//	e.preventDefault();

			//	// Get the column API object
			//	var column = table.column($(this).attr('data-column'));

			//	// Toggle the visibility
			//	column.visible(!column.visible());
			//});

		}
	});

}

function calcTotal() {

	var txtPrice = 0;
	var nColumnas = $("#tblHarvestForecastW tr:last td").length;
	$("table#tblHarvestForecastW tbody td.tdHD").each(function () {
		txtPrice += parseFloat($(this).text());
	})

	var content = "";
	content += "<tfoot>";
	content += "<tr>";
	for (i = 0; i < (nColumnas - 2); i++) {
		content += "<td colspan='' style='text-align:right; font-size:14px; background-color:#525D75; color:white; border: none'></td>";
	}
	content += "<td colspan='' style='text-align:right; font-size:14px; background-color:#525D75; color:white;border: none'>Total " + measure + "</td>";
	content += "<td class='totalPrecio' style='text-align:right;font-size:14px'></td>";
	content += "</tr>";
	content += "</tfoot>";
	$("table#tblHarvestForecastW").append(content);
	$('table#tblHarvestForecastW tfoot tr td.totalPrecio').html((txtPrice).toFixed(2));

}

function loadTitle() {

	$.ajax({
		type: "GET",
		url: "/Forecast/ListTitleFormByCropIDMC?cropID=" + localStorage.cropID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				var data = dataJson.map(
					obj => {
						return {
							"value": obj.settingValue,
						}
					}
				);
				$(textTitle).text(data[2].value);
			}
		}
	});
}