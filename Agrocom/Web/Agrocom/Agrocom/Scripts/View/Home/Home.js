﻿(function () {
    var dataScheduleBoarding = [];
    var dataKoreanDestination = [];
    var dataShippingCompanies = [];
    var dataJson = [];

    var outputScheduleBoarding = [];
    var outputKoreanDestination = [];
    var outputShippingCompanies = [];

    var myChartScheduleBoarding;
    var optionScheduleBoarding;
    var myChartKoreanDestination;
    var optionKoreanDestination;
    var myChartShippingCompanies;
    var optionShippingCompanies;

    var Report = function () {
        var _that = {
            Init: function () {
                let sUrlApiNotification = "api/home/notificacion/ListNotificationsSplash";

                $.ajax({
                    type: "GET",
                    url: getPath() + sUrlApiNotification,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.Token,
                        'TypeUsr': localStorage.UsrType,
                        'CropId': localStorage.cropID,
                    },
                    async: false,
                    success: function (data) {
                        if (data == undefined) return;
                        if (data.length == 0) return;
                        let sTitule = 'PENDINGS OF YOUR ATTENTION';
                        let shtml = '';
                        $.each(data, function (i, e) {
                            if ((data[i].requestNumber).trim() != '') {
                                shtml = shtml + data[i].requestNumber + '<br/>';
                            } else {
                                shtml = shtml + (data[i].mailDescription).replace('[EJECUTAR_PROCESO]', data[i].nextProcess) + '<br/>';
                            }                            
                        });
                        shtml = '<center>' + shtml + '</center>';
                        sweet_alert_notification(sTitule, shtml);
                    },
                    error: function (xhr, statusText, dataError) {
                        switch (xhr.status) {
                            case 401: break;
                            default: sweet_alert_error('Error!', 'There were errors while loading'); break;
                        }

                    },
                    complete: function () {
                    }
                });

                let sUrlApiKPIs = "api/KPI/ListKPIsSplash";

                $.ajax({
                    type: "GET",
                    url: getPath() + sUrlApiKPIs,
                    headers: {
                        'Authorization': 'Bearer ' + $('#hToken').val(),
                        'cropID': localStorage.cropID,
                    },
                    async: false,
                    success: function (data) {

                        dataJson = data.lstScheduleBoarding;
                        dataScheduleBoarding = dataJson.map(
                            obj => {
                                return {
                                    "id": obj.customerID,
                                    "name": obj.customer,
                                    "kpi": obj.kpi,
                                    "week": obj.number

                                }
                            }
                        );

                        dataJson = data.lstKoreanDestination;
                        dataKoreanDestination = dataJson.map(
                            obj => {
                                return {
                                    "id": obj.customerID,
                                    "name": obj.customer,
                                    "kpi": obj.kpi,
                                    "week": obj.number

                                }
                            }
                        );

                        dataJson = data.lstShippingCompanies;
                        dataShippingCompanies = dataJson.map(
                            obj => {
                                return {
                                    "id": obj.shippingCompanyID,
                                    "name": obj.shippingCompany,
                                    "kpi": obj.kpi,
                                    "week": obj.number

                                }
                            }
                        );
                    },
                    error: function (xhr, statusText, dataError) {
                        switch (xhr.status) {
                            case 204: break;
                            case 401: break;
                            default:
                                break;
                        }

                    },
                    complete: function () {
                    }
                });

            },
            Metodos: {
                Kpi01: function () {

                    optionScheduleBoarding = {
                        legend: {},
                        tooltip: {
                            trigger: 'axis',
                            showContent: false
                        },
                        dataset: {
                            source: outputScheduleBoarding
                        },
                        xAxis: { type: 'category' },
                        yAxis: { gridIndex: 0 },
                        grid: { top: '55%' },
                        series: [
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            {
                                type: 'pie',
                                id: 'pie',
                                radius: '30%',
                                center: ['50%', '25%'],
                                emphasis: { focus: 'data' },
                                label: {
                                    formatter: '{b}: {@26} ({d}%)'
                                },
                                encode: {
                                    itemName: 'Item',
                                    value: '26',
                                    tooltip: '26'
                                }
                            }
                        ]
                    };

                },

                Kpi02: function () {
                    optionKoreanDestination = {
                        legend: {},
                        tooltip: {
                            trigger: 'axis',
                            showContent: false
                        },
                        dataset: {
                            source: outputKoreanDestination
                        },
                        xAxis: { type: 'category' },
                        yAxis: { gridIndex: 0 },
                        grid: { top: '55%' },
                        series: [
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            {
                                type: 'pie',
                                id: 'pie',
                                radius: '30%',
                                center: ['50%', '25%'],
                                emphasis: { focus: 'data' },
                                label: {
                                    formatter: '{b}: {@26} ({d}%)'
                                },
                                encode: {
                                    itemName: 'Item',
                                    value: '26',
                                    tooltip: '26'
                                }
                            }
                        ]
                    };

                },

                Kpi03: function () {
                    optionShippingCompanies = {
                        legend: {},
                        tooltip: {
                            trigger: 'axis',
                            showContent: false
                        },
                        dataset: {
                            source: outputShippingCompanies
                        },
                        xAxis: { type: 'category' },
                        yAxis: { gridIndex: 0 },
                        grid: { top: '55%' },
                        series: [
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                            {
                                type: 'pie',
                                id: 'pie',
                                radius: '30%',
                                center: ['50%', '25%'],
                                emphasis: { focus: 'data' },
                                label: {
                                    formatter: '{b}: {@26} ({d}%)'
                                },
                                encode: {
                                    itemName: 'Item',
                                    value: '26',
                                    tooltip: '26'
                                }
                            }
                        ]
                    };


                }


            },
            Eventos: function () {
                //>>>1er.KPI
                myChartScheduleBoarding.on('updateAxisPointer', function (event) {
                    var xAxisInfo = event.axesInfo[0];
                    if (xAxisInfo) {
                        var dimension = xAxisInfo.value + 1;
                        myChartScheduleBoarding.setOption({
                            series: {
                                id: 'pie',
                                label: {
                                    formatter: '{b}: {@[' + dimension + ']} ({d}%)'
                                },
                                encode: {
                                    value: dimension,
                                    tooltip: dimension
                                }
                            }
                        });
                    }
                });
                myChartScheduleBoarding.setOption(optionScheduleBoarding);
                //>>>1er.KPI

                //>>>2do.KPI
                myChartKoreanDestination.on('updateAxisPointer', function (event) {
                    var xAxisInfo = event.axesInfo[0];
                    if (xAxisInfo) {
                        var dimension = xAxisInfo.value + 1;
                        myChartKoreanDestination.setOption({
                            series: {
                                id: 'pie',
                                label: {
                                    formatter: '{b}: {@[' + dimension + ']} ({d}%)'
                                },
                                encode: {
                                    value: dimension,
                                    tooltip: dimension
                                }
                            }
                        });
                    }
                });
                if (optionKoreanDestination != undefined) myChartKoreanDestination.setOption(optionKoreanDestination);
                //<<<2do.KPI

                //>>>3er.KPI
                myChartShippingCompanies.on('updateAxisPointer', function (event) {
                    var xAxisInfo = event.axesInfo[0];
                    if (xAxisInfo) {
                        var dimension = xAxisInfo.value + 1;
                        myChartShippingCompanies.setOption({
                            series: {
                                id: 'pie',
                                label: {
                                    formatter: '{b}: {@[' + dimension + ']} ({d}%)'
                                },
                                encode: {
                                    value: dimension,
                                    tooltip: dimension
                                }
                            }
                        });
                    }
                });
                myChartShippingCompanies.setOption(optionShippingCompanies);
                //<<<3er.KPI
            }
        }

        return _that;
    }

    $(document).ready(function () {
        sweet_alert_progressbar_cerrar();
        $('#divKPI').removeClass('show');

        Report().Init();

        let _metodos = Report().Metodos;

        //1er.KPI
        var arrScheduleBoarding = getListToArray(dataScheduleBoarding);
        outputScheduleBoarding = getPivotArray(arrScheduleBoarding, 1, 3, 2);

        //2do.KPI
        var arrdataKoreanDestination = getListToArray(dataKoreanDestination);
        outputKoreanDestination = getPivotArray(arrdataKoreanDestination, 1, 3, 2);

        //3er.KPI
        var arrShippingCompanies = getListToArray(dataShippingCompanies);
        outputShippingCompanies = getPivotArray(arrShippingCompanies, 1, 3, 2);

        var chartDomScheduleBoarding = document.getElementById('divScheduleBoarding');
        myChartScheduleBoarding = echarts.init(chartDomScheduleBoarding);

        var chartDomKoreanDestination = document.getElementById('divKoreanDestination');
        myChartKoreanDestination = echarts.init(chartDomKoreanDestination);

        var chartDomShippingCompanies = document.getElementById('divShippingCompanies');
        myChartShippingCompanies = echarts.init(chartDomShippingCompanies);

        _metodos.Kpi01();
        setTimeout(_metodos.Kpi02, 3000);
        _metodos.Kpi03();

        Report().Eventos();

        //Swal.close();
    });
})();
