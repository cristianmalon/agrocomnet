﻿var _consigneeDestinationID = 0; //Sí dataListDestination es igual a 0 (Nuevo) : Sí es 1 (Editar)
var _fruta = 0; //Sí fruta es igual a 0 (Arandanos) : Sí es 1 (Uva)
var dataDestinationCrop = [];
var dataConsignee = [];
var dataVariety = [];
var dataCodePack = [];
var dataCategory = [];
var dataSize = [];
var _idCustomer = [];
var _dataList = [];
var table;
var dataListDestination = []; 
var dataOrigin = [];
var dataFormat = [];
var dataTypeBox = [];
var dataPresentation = [];
var dataBrand = [];
var dataSubsidiary = [];
var _cropID2 = [];
var _consigneeDestinationIDCrop = [];
var _customerConsigneeDestinationSpecsID = 0;
var _userID = $('#lblSessionUserID').text();

$(function () {
	loadListPrincipal();
	loadData();
	loadDefaultValues();
	sweet_alert_progressbar_cerrar();

	//comboConsigneeDestinaton();

	$(document).on("click", ".row-remove", function () {
		$(this).parents('tr').detach();
	});

	$(document).on("click", "#btnAddDestination", function () {
		addDestination();
	});

	$(document).on("click", "#btnAddSpecs", function () {
		AddSpecsComment();
	});

	$(document).on("click", "#btnAddSpecification", function () {
		AddSpecification();
	});

	$('#btnAddSection').on('click', function (e) {

		$('#myModalSection').modal('hide');
	});

	//$(document).on("click", "#btnAddConsigneeDestination", function (e) {
	//	TablaConsigneeDestination();
	//});

	var form = $("#Formulario");
	form.removeClass('was-validated');

});

$('#btnSaveClient').click(function (e) {

	var form = $("#Formulario");
	form.addClass('was-validated');
	if (form[0].checkValidity() === false) {
		event.preventDefault();
		event.stopPropagation();
		sweet_alert_warning('Warning', "Enter the fields highlighted in red.");
	} else {
		PreGuardar();
	}

});

$('#btnSaveCrop').click(function () {
	SaveCrop();
});

$('#btnSaveConsigneeDestination').click(function () {

	SaveConsigneeDestination();

});

//function loadDataBrandByCategory(categoryID) {

//	$.ajax({
//		type: "GET",
//		url: "/Maintenance/BrandGetByCateID?opt=" + "cat" + "&cropID=" + categoryID,
//		async: false,
//		headers: {
//			'Cache-Control': 'no-cache, no-store, must-revalidate',
//			'Pragma': 'no-cache',
//			'Expires': '0'
//		},
//		success: function (data) {

//			if (data.length > 0) {
//				var dataJson = JSON.parse(data);
//				dataBrand = dataJson.map(
//					obj => {
//						return {
//							"id": obj.brandID,
//							"name": obj.brand,
//							"abbreviation": obj.abbreviation
//						}
//					}
//				);
//			}
//		}
//	});
//}

function loadCombo(data, control, firtElement) {
	var content = "";
	if (firtElement == true) {
		content += "<option value=''>--Choose--</option>";
	}
	for (var i = 0; i < data.length; i++) {
		content += "<option value='" + data[i].id + "'>";
		content += data[i].name;
		content += "</option>";
	}
	$('#' + control).empty().append(content);
}

function loadDefaultValues() {

	loadCombo(dataListDestination, 'selectDestination', true);
	$('#selectDestination').prop("disabled", false);
	$('#selectDestination').selectpicker('refresh');
		
	loadCombo(dataOrigin, 'SelectOrigin', true);
	$('#SelectOrigin').prop("disabled", false);
	$('#SelectOrigin').selectpicker('refresh');

	loadCombo(dataSubsidiary, 'SelectSubsidiary', true);
	$('#SelectSubsidiary').prop("disabled", false);
	$('#SelectSubsidiary').selectpicker('refresh');

	loadCombo(dataDestinationCrop, 'selectDestination2', true);
	$('#selectDestination2').prop("disabled", false);
	$('#selectDestination2').selectpicker('refresh');

	
}

function loadData() {

	$.ajax({
		type: "GET",
		url: "/Maintenance/GetOrigin?opt=" + "all" + "&id=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataOrigin = dataJson.map(
					obj => {
						return {
							"id": obj.originID,
							"name": obj.description 
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Maintenance/ListDestination?opt=" + "all" + "&id=" + "" + "&mar=" + "" + "&via=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataListDestination = dataJson.map(
					obj => {
						return {
							"id": obj.destinationID,
							"name": obj.origin + " - " + obj.destination
						}
					}
				);
			}
		}
	});

	//$.ajax({
	//	type: "GET",
	//	url: "/Sales/ListBrand?opt=" + "all" + "&id=" + "" + "&growerID" + "",
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {
	//		var dataJson = JSON.parse(data);
	//		if (data.length > 0) {
	//			dataBrand = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.categoryID,
	//						"name": obj.description
	//					}
	//				}
	//			);
	//		}
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/Maintenance/ListConsignee",
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {
	//		var dataJson = JSON.parse(data);
	//		if (data.length > 0) {
	//			dataConsignee = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.customerID,
	//						"name": obj.consignee
	//					}
	//				}
	//			);
	//		}
	//	}
	//});

	var idCustomer = _idCustomer.toString();
	$.ajax({
		type: "GET",
		url: "/Maintenance/ListDestinationByCustomer?opt=" + "cus" + "&id=" + idCustomer + "&mar=" + "" + "&via=" + "",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataDestinationCrop = dataJson.map(
					obj => {
						return {
							"id": obj.customerID,
							"name": obj.destinationID + "-" + obj.destinationID
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/Maintenance/ListSubsidiary",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataSubsidiary = dataJson.map(
					obj => {
						return {
							"id": obj.id,
							"name": obj.description
						}
					}
				);
			}
		}
	});	
}

function addDestination() {
	var okey = 0;
	var destinationID = $("#selectDestination option:selected").val();
	if (destinationID == "") {
		alert("You must select a Destination");
		return;
	}
		$("table#tblDestination>tbody>tr").each(function (id, row) {
			if ($(row).find("td.destinationID").text() == destinationID){
				return okey = 1;			
			} 
		})

	if (okey == 1) {
		sweet_alert_warning("Alert", "This destination has been added");
		return;
	}
	else {
		var idCustomer = _idCustomer;
		var destinationID = $("#selectDestination option:selected").val();
		var destination = $("#selectDestination option:selected").text();
		var brix = '';
		var acidity = '';
		var bloom = '';
		var traceability = '';
		var comments = '';
		var additional = '';

		var content = "";
		content += '<tr style="font-size:12px">'
		content += '<td class="destinationID" style="min-width:100px; max-width:100px; font-size: 12px" >'+ destinationID + '</td > ';
		content += '<td class="destination" >' + destination + '</td>';
		content += '<td class="brix" hidden>' + brix + '</td>';
		content += '<td class="acidity" hidden>' + acidity + '</td>';
		content += '<td class="bloom" hidden>' + bloom + '</td>';
		content += '<td class="traceability" hidden>' + traceability + '</td>';
		content += '<td class="comments2" hidden>' + comments + '</td>';
		content += '<td class="additional2" hidden>' + additional + '</td>';
		content += '<td style="text-align:center"> <button id="" onclick="AddSpecs(this)" data-toggle="modal" data-target="#myModalSpecs" data-backdrop="static" data-keyboard="false" class="btn btn-sm" type="button" style="background-color:none; border:none; padding: 0px 0px 0px 0px;"> <span class="icon text-primary"><i class="fas fa-edit fas-sm"></i></span></button>';
		content += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
		content += '</tr>'
		$("table#tblDestination tbody").append(content);
	}

}

function addDestinationEdit() {

	var idCustomer = _idCustomer.toString();
	var api = "/Maintenance/ListDestinationByCustomer?opt=" + "cus" + "&id=" + idCustomer + "&mar=" + "" + "&via=" + "";

	$.ajax({
		type: "GET",
		url: api,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var _dataDestination = JSON.parse(data);
				var content = "";
				$.each(_dataDestination, function (index, row) {
				content += "<tr style='font-size:12px'>";
				
					content += '<td class="destinationID" >' + row.destinationID + '</td > ';
					content += "<td class='destination'>" + row.destination + "</td>";
					content += '<td class="brix" hidden>' + row.brix + '</td>';
					content += '<td class="acidity" hidden>' + row.acidity + '</td>';
					content += '<td class="bloom" hidden>' + row.bloom + '</td>';
					content += '<td class="traceability" hidden>' + row.traceability + '</td>';
					content += '<td class="comments2" hidden>' + row.comments + '</td>';
					content += '<td class="additional2" hidden>' + row.additional + '</td>';
					content += '<td style="text-align:center"> <button id="" onclick="AddSpecs(this)" data-toggle="modal" data-target="#myModalSpecs" data-backdrop="static" data-keyboard="false" class="btn btn-sm" type="button" style="background-color:none; border:none; padding: 0px 0px 0px 0px;"> <span class="icon text-primary"><i class="fas fa-edit fas-sm"></i></span></button>';
					content += '<button class="btn btn-sm row-remove" type="button" style="background-color:none; border:none; padding: 0px 0px 0px 0px;"><span class="icon text-danger"><i class="fas fa-trash fa-sm"></i><span></button></td> ';
					content += "</tr>";
				})				

				$("table#tblDestination").DataTable().clear();
				$("table#tblDestination").DataTable().destroy();
				$("table#tblDestination tbody").append(content);

				$('#selectDestination2').empty()
				$('#selectDestination1').empty()
				$.each(_dataDestination, function (i, item) {
					$('#selectDestination1').append($('<option>', {
						value: item.destinationID,
						text: item.description
					}));
				});
				$.each(_dataDestination, function (i, item) {
					$('#selectDestination2').append($('<option>', {
						value: item.destinationID,
						text: item.description
					}));
				});
				$('#selectDestination1').selectpicker('refresh')
				$('#selectDestination2').selectpicker('refresh')
				
			}			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$.notify("Problemas con el cargar el listado!", "error");
			
		}
	});

}

function AddSpecs(xthis) {
	
	trEdit = $(xthis).closest("tr");

	var customerID = _idCustomer;
	var customer = $("#txtBussinesName").val();
	var destinationID = $(xthis).closest("tr").find("td.destinationID").text();
	var destination = $(xthis).closest("tr").find("td.destination").text();
	var brix = $(xthis).closest("tr").find("td.brix").text();
	var acidity = $(xthis).closest("tr").find("td.acidity").text();
	var bloom = $(xthis).closest("tr").find("td.bloom").text();
	var traceability = $(xthis).closest("tr").find("td.traceability").text();
	var comments = $(xthis).closest("tr").find("td.comments2").text();
	var additional = $(xthis).closest("tr").find("td.additional2").text();

	////Asigno los valores a los controles del Modal
	$("#txtCust").val(customer);
	$("#txtcustID").val(customerID);
	$("#txtdestID").val(destinationID);
	$("#txtDest").val(destination);
	$("#txtBrix").val(brix);
	$("#txtAcidity").val(acidity);
	$("#txtBloom").val(bloom);
	$("#txtTraceability").val(traceability);
	$("#txtComments").val(comments);
	$("#txtAdditional").val(additional);

}

function AddSpecsComment() {

	var brix = $("#txtBrix").val();
	var acidity = $("#txtAcidity").val();
	var bloom = $("#txtBloom").val();
	var traceability = $("#txtTraceability").val();
	var comments = $("#txtComments").val();
	var additional = $("#txtAdditional").val();

	$(trEdit).find('td.brix').text(brix);
	$(trEdit).find('td.acidity').text(acidity);
	$(trEdit).find('td.bloom').text(bloom);
	$(trEdit).find('td.traceability').text(traceability);
	$(trEdit).find('td.comments2').text(comments);
	$(trEdit).find('td.additional2').text(additional);

	$(myModalSpecs).modal('hide')
	sweet_alert_success('Good Job!', 'successfully update');
	
}

//Carga Modal para Editar
function CargaModal(xthis) {
	if (xthis == 0) { 
		_idCustomer = xthis;
		cleanControls();
		
	} else {
		cleanControls();
		
		//Obtengo los valores del name de la fila
		$.get("/Maintenance/GetCustomerByID?opt=" + "id" + "&id=" + xthis, function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data)

				_idCustomer = dataJson[0].customerID
				$("#txtBussinesName").val(dataJson[0].name);
				$("#txtAbbreviation").val(dataJson[0].abbreviation);
				$("#txtAddres").val(dataJson[0].address);
				$("#txtContact").val(dataJson[0].contact);
				$("#txtPhone").val(dataJson[0].phone);
				$("#txtemail").val(dataJson[0].email);
				$("#SelectOrigin").selectpicker('val', dataJson.originID);
				$("#SelectOrigin").val(dataJson[0].originID).selectpicker('refresh');
				$("#txtInformation").val(dataJson[0].moreInfo);
				$(checkCustomer).prop('checked', (dataJson[0].customer == 1) ? true : false);
				$(checkConsignee).prop('checked', (dataJson[0].consignee == 1) ? true : false);
				$(checkNotify).prop('checked', (dataJson[0].notify == 1) ? true : false);
				$("#txtRUC").val(dataJson[0].ruc);
				var _Subsi = [];
				_Subsi = dataJson[0].subsidiaryID;
				$(SelectSubsidiary).selectpicker('val', _Subsi.split(','))
				
			}
			addDestinationEdit();
		});
		
	}
	loadConsigneeDestination(xthis)
}

function cleanControls() {

	$("#txtBussinesName").val("");
	$("#txtAbbreviation").val("");
	$("#txtAddres").val("");
	$("#txtInformation").val("");
	$("#SelectOrigin").val("").selectpicker('refresh');
	$("#txtContact").val("");
	$("#txtemail").val("");
	$("#txtPhone").val("");
	$("#txtInformation").val("");
	$("#txtInformation").val("");
	$("#checkCustomer").val("");
	$("#checkConsignee").val("");
	$("#checkNotify").val("");
	$("#txtRUC").val("");
	$("#SelectSubsidiary").val("").selectpicker('refresh');
	$("table#tblDestination tbody").empty();

}

//function cleanControlsCrop() {

//	$("#selectVariety").val("").selectpicker('refresh');
//	$("#selectFormat").val("").selectpicker('refresh');
//	$("#selectFormat").val("").selectpicker('refresh');
//	$(checkGenericBox).prop('checked', false);
//	$("#selectCategory").val("").selectpicker('refresh');
//	$("#selectBrand").val("").selectpicker('refresh');
//	$("#selectPresentation").val("").selectpicker('refresh');
//	$("#selectSize").val("").selectpicker('refresh');
//	$("#selectSize").val("").selectpicker('refresh');
//	$("#txtPrice").val("");
//	$(checkPrincipal).prop('checked', false);
//	$(checkActive).prop('checked', false);
//	$("#txtNotes").val("");
//	$("#txtCodePack").val("");
//	$("table#tblSpecification tbody").empty();

//}

//function CleanTabla() {

//	$('#selectDestination2').val("").selectpicker('refresh');
//	$("#selectConsignee_blu").val("").selectpicker('refresh');

//	$("table#table_ozblu>tbody>tr").find("td.customerID").text("");
//	$("table#table_ozblu>tbody>tr").find("td.cropID").text("");	
//	$("table#table_ozblu>tbody>tr").find("td.consigneeID").text("");
//	$("table#table_ozblu>tbody>tr").find("td.consignee").text("");
//	$("table#table_ozblu>tbody>tr").find("td.destinationID").text("");
//	$("table#table_ozblu>tbody>tr").find("td.destination").text("");
//	$("table#table_ozblu>tbody>tr").find("td.consigneeDestinationID").text("");
//	$("table#table_ozblu tbody").empty();

//	$('#selectDestination1').val("").selectpicker('refresh');
//	$("#selectConsignee_aga").val("").selectpicker('refresh');

//	$("table#table_aga>tbody>tr").find("td.customerID").text("");
//	$("table#table_aga>tbody>tr").find("td.consigneeID").text("");
//	$("table#table_aga>tbody>tr").find("td.consignee").text("");
//	$("table#table_aga>tbody>tr").find("td.destinationID").text("");
//	$("table#table_aga>tbody>tr").find("td.destination").text("");
//	$("table#table_aga>tbody>tr").find("td.consigneeDestinationID").text("");
//	$("table#table_aga>tbody>tr").find("td.cropID").text("");
//	$("table#table_aga tbody").empty();

//	//$("table#table_ozblu tbody").empty();

//}

function loadListPrincipal() {

	var searchName = "";
	var api = "/Maintenance/ListCustomer?opt=" + "all" + "&id=" + "" + "&searchName=" + searchName;

	$.ajax({
		type: "GET",
		url: api,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				_dataList = JSON.parse(data);
				var content = "";
				$.each(_dataList, function (index, row) {

					content += "<tr style='font-size:12px'>";
					content += "<td style='max-width:50px; min-width:50px; text-align:center'> <button class='btn btn-primary btn-sm' onclick ='CargaModal(" + row.customerID+")' data-toggle='modal' data-target='#myModal' > <i class='fa fa-edit fa-sm'></i></button > </td>";
					content += "<td style='max-width:50px; min-width:50px; display:none' id='statusID'>" + row.statusID + "</td>";
					content += "<td style='max-width:50px; min-width:50px; display:none' id='customerID'>" + row.customerID + "</td>";
					content += "<td style='max-width:300px; min-width:300px' id='name'><textarea id='name' style='font-size:12px' class='form-control form-control-sm' rows='1' readonly>" + row.name + "</textarea></td>";
					content += "<td style='max-width:150px; min-width:150px' id='abbreviation'>" + row.abbreviation + "</td>";
					content += "<td style='max-width:300px; min-width:300px' id='address'><textarea id='address' style='font-size:12px' class='form-control form-control-sm' rows='1' readonly>" + row.address + "</textarea></td>";
					content += "<td style='max-width:150px; min-width:150px' id='contact'>" + row.contact + "</td>";
					content += "<td style='max-width:150px; min-width:150px' id='phone'><textarea id='phone' style='font-size:12px' class='form-control form-control-sm' rows='1' readonly>" + row.phone + "</textarea></td>";
					content += "<td style='max-width:270px; min-width:270px' id='email'><textarea id='address' style='font-size:12px' class='form-control form-control-sm' rows='1' readonly> " + row.email + "</textarea></td>";
					content += "<td style='max-width:150px; min-width:150px; id='description'>" + row.description + "</td>";

					content += "</tr>";
				})
				$("table#divList").DataTable().clear();
				$("table#divList").DataTable().destroy();
				$("table#divList tbody").empty().append(content);
				table = $('table#divList').DataTable({
					orderCellsTop: true,
					fixedHeader: true,
					ordering: false,
					searching: true
				
				});
				
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$.notify("Problemas con el cargar el listado!", "error");
			
		}
	});

}

table = $('#divList').DataTable({
	orderCellsTop: true,
	fixedHeader: true,
	ordering:true
});

//Filtros en la cabecera de la tabla principal
$(document).ready(function () {
	// Setup - add a text input to each footer cell
	$('#divList thead tr').clone(true).appendTo('#divList thead');
	$('#divList thead tr:eq(1) th').each(function (i) {
		if (i > 0) {
			var title = $(this).text();
			$(this).html('<input type="text" class="form-control form-control-sm" style="font-size:12px" placeholder="Search ' + title + '" />');
		} else {
			$(this).html(' ');
		}
		$('input', this).on('keyup change', function () {
			if (table.column(i).search() !== this.value) {
				table
					.column(i)
					.search(this.value)
					.draw();
			}
		});
	});
	
});

function PreGuardar() {
	Swal.fire({
		title: '¿Want to save?',
		text: "The data will be permanently stored in the system.",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, save!',
		cancelButtonText: 'No, cancel!',
		confirmButtonColor: '#4CAA42',
		cancelButtonColor: '#d33'
	}).then((result) => {
		if (result.value) {
			SaveClient();
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			sweet_alert_error('Cancelled!', 'The save process was canceled.')
		}
	});
}

function SaveClient() {

	//Capturo el valor actual
	var userID = _userID;
	var name = $("#txtBussinesName").val();
	name = name.replace("&", "@");
	var address = $("#txtAddres").val();
	address = address.replace("&", "@");
	var contact = $("#txtContact").val();
	var phone = $("#txtPhone").val();
	var email = $("#txtemail").val();
	var abbreviation = $("#txtAbbreviation").val();
	abbreviation = abbreviation.replace("&", "@");
	var idOrigin = $('#SelectOrigin option:selected').val()
	var moreinfo = $("#txtInformation").val();
	var customer = $('#checkCustomer').prop('checked') ? 1 : 0
	var consignee = $('#checkConsignee').prop('checked') ? 1 : 0
	var notify = $('#checkNotify').prop('checked') ? 1 : 0
	var ruc = $("#txtRUC").val();
	var subID = '';
	var SUB = $(SelectSubsidiary).val();
	$.each(SUB, function (index, value) {
		subID += value + ',';
	});

	var destinations_table = []

	$("table#tblDestination>tbody>tr").each(function (iTr, tr) {
		var o = {}
		
		var destinationID = $(tr).find('td.destinationID').text();
		var brix = $(tr).find('td.brix').text();
		var acidity = $(tr).find('td.acidity').text();
		var bloom = $(tr).find('td.bloom').text();
		var traceability = $(tr).find('td.traceability').text();
		var comments = $(tr).find('td.comments2').text();
		var additional = $(tr).find('td.additional2').text();

		o.destinationID = parseInt(destinationID);
		o.cropID = '';
		o.brix = brix;
		o.acidity = acidity;
		o.bloom = bloom;
		o.traceability = traceability;
		o.comments = comments;
		o.comments2 = additional;
		o.tolerance = 0;

		destinations_table.push(o)
	})
		
	var customerID = _idCustomer;

	var frmClientN = false;
	
    //Envio de datos a la api
    var frmClientN = new FormData();

    if (customerID == 0) {
        frmClientN.append("userID", userID);
        frmClientN.append("name", name); //las letras rojo es la variable en api y blanca mi variable
        frmClientN.append("address", address);
        frmClientN.append("contact", contact);
        frmClientN.append("phone", phone);
        frmClientN.append("email", email);
        frmClientN.append("abbreviation", abbreviation);
        frmClientN.append("idOrigin", idOrigin);
        frmClientN.append("moreinfo", moreinfo);
        frmClientN.append("customer", customer);
        frmClientN.append("consignee", consignee);
        frmClientN.append("notify", notify);
        frmClientN.append("ruc", ruc);
        frmClientN.append("subsidiaryID", subID);
        frmClientN.append("destinations", JSON.stringify(destinations_table));
        var api = "/Maintenance/GetCreateCustomer"
    }
    else {
        frmClientN.append("userID", userID);
        frmClientN.append("customerID", customerID);
        frmClientN.append("name", name); //las letras rojo es la variable en api y blanca mi variable
        frmClientN.append("address", address);
        frmClientN.append("contact", contact);
        frmClientN.append("phone", phone);
        frmClientN.append("email", email);
        frmClientN.append("abbreviation", abbreviation);
        frmClientN.append("idOrigin", idOrigin);
        frmClientN.append("moreinfo", moreinfo);
        frmClientN.append("customer", customer);
        frmClientN.append("consignee", consignee);
        frmClientN.append("notify", notify);
        frmClientN.append("ruc", ruc);
        frmClientN.append("subsidiaryID", subID);
        frmClientN.append("destinations", JSON.stringify(destinations_table));
        var api = "/Maintenance/GetUpdateCustomer"
    }

    //var api = "/Maintenance/SaveConsigneeDestinationID"
    $.ajax({
		type: 'POST',
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(Object.fromEntries(frmClientN)),
        Accept: "application/json",

        success: function (datares) {
            if (datares.length > 0) {
                sweet_alert_success('Saved!', 'The data has been saved.');
                addDestinationEdit();
                //addDestination();
                $('#selectDestination1').selectpicker("refresh");
                $('#selectDestination2').selectpicker("refresh");
                loadListPrincipal();
				$('#myModal').modal('hide');
				//PostClients();
            } else {
                sweet_alert_error("Error", "The data was NOT saved!");

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "The data was NOT saved!");

        }
    });
	
}

function PostClients() {
	var sUrlApi = getPath() + HOST_NETSUITE + ENDPOINT_PUT_CLIENT;
	$.ajax({
		type: "PUT",
		url: sUrlApi,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		error: function (datoEr) {
			sweet_alert_info('Information', "There was an issue trying to list data.");
		},
		complete: function () {
			
		}
	});
}

function LoadDataCrop(cropID) {

	//var cropID = localStorage.cropID;
	
	//$.ajax({
	//		type: "GET",
	//	url: "/Maintenance/VarietyGetByCrop?opt=" + "cro" + "&id=" + cropID + "&log=" + "",
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {

	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			dataVariety = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.varietyID,
	//						"name": obj.name,
	//						"abbreviation": obj.abbreviation

	//					}
	//				}
	//			);
	//		}
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/Maintenance/GetFormatByCrop?opt=" + "fci" + "&id=" + cropID,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {
	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			dataFormat = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.formatID,
	//						"name": obj.format
	//					}
	//				}
	//			);
	//		}
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/Maintenance/GetPackageProductAll?opt=" + "pkc" + "&id=" + "",
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {

	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			dataTypeBox = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.packageProductID,
	//						"name": obj.packageProduct,
	//						"abbreviation": obj.abbreviation
	//					}
	//				}
	//			);
	//		}
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/Maintenance/CategoryGetByCrop?opt=" + "cro" + "&id=" + cropID + "&gro=" + "",
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {

	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			dataCategory = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.categoryID,
	//						"name": obj.description
	//					}
	//				}
	//			);
	//		}
	//	}
	//});
	
	//$.ajax({
	//	type: "GET",
	//	url: "/Maintenance/GetPresentByCrop?opt=" + "pci" + "&id=" + cropID,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {
	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			dataPresentation = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.presentationID,
	//						"name": obj.presentation,
	//						"presentation": obj.description
	//					}
	//				}
	//			);
	//		}
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/Maintenance/ListSizeGetByCrop?opt=" + "cro" + "&id=" + cropID,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {

	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			dataSize = dataJson.map(
	//				obj => {
	//					return {
	//						"id": obj.sizeID,
	//						"name": obj.sizeID 
	//					}
	//				}
	//			);
	//		}
	//	}
	//});

}

$('#selectCategory').change(function () {

	//loadCombo([], 'selectBrand', true);
	////sí el data de Category está vacío retorna vacío
	//if (this.value == "") {
	//	return;
	//}
	////Cuando tengo seleccion del cultivo
	//if ($("#selectCategory option:selected").val().trim().length > 0) {

	//	loadDataBrandByCategory($("#selectCategory option:selected").val().trim())
	//}

	////List 
	//if (dataBrand.length > 0) {
	//	loadCombo(dataBrand, 'selectBrand', false);
	//	$('#selectBrand').selectpicker('refresh');
	//} else {
	//	loadCombo([], 'selectBrand', true);
	//}
	//loadListPrincipal();

})

function loadConsigneeDestination(xthis) {
	//var customerID = xthis;
	//var cropID = localStorage.cropID;
	//var api = "/Maintenance/GetConsDestByCust?customerID=" + customerID + " &cropID=" + "BLU"
		
	//$.ajax({
	//	type: "GET",
	//	url: api,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {

	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			var content = "";
	//			$.each(dataJson, function (i,item) {
					
	//				content += '<tr style="font-size:12px">';
	//				content += '<td class="customerID" hidden>' + item.customerID + '</td > ';
	//				content += '<td class="customer">' + item.customer + '</td > ';
	//				content += '<td class="cropID" hidden>' + item.cropID + '</td > ';
	//				content += '<td class="consigneeID" hidden>' + item.consigneeID + '</td > ';
	//				content += '<td class="consignee">' + item.consignee + '</td > ';
	//				content += '<td class="destinationID" hidden>' + item.destinationID + '</td > ';
	//				content += '<td class="destination">' + item.destination + '</td > ';
	//				content += '<td class="consigneeDestinationID" hidden>' + item.customerConsigneeDestinationID + '</td>';
	//				content += '<td style="text-align:center"> <button id="" onclick="CargaModalCrop(this)" data-toggle="modal" data-target="#myModalSection" data-backdrop="static" data-keyboard="false" class="btn btn-sm" type="button" style="background-color:none; border:none; padding: 0px 0px 0px 0px;"> <span class="icon text-primary"><i class="fas fa-edit fas-sm"></i></span></button>';
	//				content += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
	//				content += '</tr>';
	//			})
	//			$("table#table_ozblu").DataTable().clear();
	//			$("table#table_ozblu").DataTable().destroy();
	//			$("table#table_ozblu tbody").empty().append(content);
	//		}
	//	}
	//});

	//var api2 = "/Maintenance/GetConsDestByCust?customerID=" + customerID + "&cropID=" + "UVA"

	//$.ajax({
	//	type: "GET",
	//	url: api2,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {

	//		if (data.length > 0) {
	//			var dataJson = JSON.parse(data);
	//			var content1 = "";
	//			$.each(dataJson, function (i, item) {

	//				content1 += '<tr style="font-size:12px">';
	//				content1 += '<td class="customerID" hidden>' + item.customerID + '</td > ';
	//				content1 += '<td class="customer">' + item.customer + '</td > ';
	//				content1 += '<td class="cropID" hidden>' + item.cropID + '</td > ';
	//				content1 += '<td class="consigneeID" hidden>' + item.consigneeID + '</td > ';
	//				content1 += '<td class="consignee">' + item.consignee + '</td > ';
	//				content1 += '<td class="destinationID" hidden>' + item.destinationID + '</td > ';
	//				content1 += '<td class="destination">' + item.destination + '</td > ';
	//				content1 += '<td class="consigneeDestinationID" hidden>' + item.customerConsigneeDestinationID + '</td>';
	//				content1 += '<td style="text-align:center"> <button id="" onclick="CargaModalCrop(this)" data-toggle="modal" data-target="#myModalSection" data-backdrop="static" data-keyboard="false" class="btn btn-sm" type="button" style="background-color:none; border:none; padding: 0px 0px 0px 0px;"> <span class="icon text-primary"><i class="fas fa-edit fas-sm"></i></span></button>';
	//				content1 += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
	//				content1 += '</tr>';
	//			})
	//			$("table#table_aga").DataTable().clear();
	//			$("table#table_aga").DataTable().destroy();
	//			$("table#table_aga tbody").empty().append(content1);
	//		}
	//	}
	//});

}

function AddConsigneeDestination(fruta) {

	//if (fruta == 0) {

	//	var consigneeID_blu = $("#selectConsignee_blu option:selected").val();
	//	if (consigneeID_blu == "") {
	//		alert("Choose a Consignee");
	//		return;
	//	}

	//	var selectDestination2 = $("#selectDestination2 option:selected").val();
	//	if (selectDestination2 == "") {
	//		alert("Choose a Destination");
	//		return;
	//	}

	//	//alert("Arandanos");
	//	var customerID = _idCustomer;
	//	var customer = $("#txtBussinesName").val();
	//	var customerConsigneeDestinationID = _consigneeDestinationID;
	//	var cropID = "BLU";
	//	var consigneeID = $('#selectConsignee_blu option:selected').val()
	//	var consignee = $('#selectConsignee_blu option:selected').text()
	//	var destinationID = $('#selectDestination2 option:selected').val()
	//	var destination = $('#selectDestination2 option:selected').text()
		
	//	var content = "";
	//	content += '<tr>'		
	//	content += '<td class="customerID" hidden>' + customerID + '</td > ';
	//	content += '<td class="customer">' + customer + ' </td > ';
	//	content += '<td class="cropID"hidden>' + cropID + '</td > ';
	//	content += '<td class="consigneeID"hidden>' + consigneeID + '</td > ';
	//	content += '<td class="consignee">' + consignee + '</td > ';
	//	content += '<td class="destinationID"hidden>' + destinationID + '</td > ';
	//	content += '<td class="destination">' + destination + '</td > ';
	//	content += '<td class="consigneeDestinationID"hidden>' + customerConsigneeDestinationID + '</td>';
	//	content += '<td style="text-align:center">';
	//	if (customerConsigneeDestinationID == 0) {
	//		content += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
	//	}
	//	else {
	//		content += '<button id = "" onclick = "CargaModalCrop(this)" data - toggle="modal" data - target="#myModalSection" data - backdrop="static" data - keyboard="false" class="btn btn-sm" type = "button" style = "background-color:none; border:none; padding: 0px 0px 0px 0px;" > <span class="icon text-primary"><i class="fas fa-edit fas-sm"></i></span></button > ';
	//		content += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
	//	}		
	//	content += '</tr>';

	//	//$("table#tblDestination").DataTable().clear();
	//	//$("table#tblDestination").DataTable().destroy();
	//	$("table#table_ozblu tbody").append(content);

	//}
	//else {

	//	var consigneeID_aga = $("#selectConsignee_aga option:selected").val();
	//	if (consigneeID_aga == "") {
	//		alert("Choose a Consignee");
	//		return;
	//	}
	//	var selectDestination1 = $("#selectDestination1 option:selected").val();
	//	if (selectDestination1 == "") {
	//		alert("Choose a Destination");
	//		return;
	//	}

	//	//alert("Grapes");
	//	var customerID = _idCustomer;
	//	var customer = $("#txtBussinesName").val();
	//	var customerConsigneeDestinationID = _consigneeDestinationID;
	//	var cropID = "UVA";
	//	var consigneeID = $('#selectConsignee_aga option:selected').val()
	//	var consignee = $('#selectConsignee_aga option:selected').text()
	//	var destinationID = $('#selectDestination1 option:selected').val()
	//	var destination = $('#selectDestination1 option:selected').text()

	//	var content = "";
	//	content += '<tr>'
	//	content += '<td class="customerID" hidden>' + customerID + '</td > ';
	//	content += '<td class="customer">' + customer + '</td > ';
	//	content += '<td class="cropID"hidden>' + cropID + '</td > ';
	//	content += '<td class="consigneeID"hidden>' + consigneeID + '</td > ';
	//	content += '<td class="consignee">' + consignee + '</td > ';
	//	content += '<td class="destinationID"hidden>' + destinationID + '</td > ';
	//	content += '<td class="destination">' + destination + '</td > ';
	//	content += '<td class="consigneeDestinationID"hidden>' + customerConsigneeDestinationID + '</td>';
	//	content += '<td style="text-align:center">';
	//	if (customerConsigneeDestinationID == 0) {
	//		content += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
	//	}
	//	else {
	//		content += '<button id = "" onclick = "CargaModalCrop(this)" data - toggle="modal" data - target="#myModalSection" data - backdrop="static" data - keyboard="false" class="btn btn-sm" type = "button" style = "background-color:none; border:none; padding: 0px 0px 0px 0px;" > <span class="icon text-primary"><i class="fas fa-edit fas-sm"></i></span></button > ';
	//		content += '<span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td> ';
	//	}
	//	content += '</tr>';

	//	//$("table#tblDestination").DataTable().clear();
	//	//$("table#tblDestination").DataTable().destroy();
	//	$("table#table_aga tbody").append(content);

	//}

}

function SaveConsigneeDestination() {

	//Capturo el valor actual
	//var customerID = _idCustomer;
	//var cropID = $("table#table_ozblu>tbody>tr").find("td.cropID").text();
	
	//if (customerID == 0) {
		
	//	//Parámetros Tabla Ozblu
	//	var customerID = $("table#table_ozblu>tbody>tr").find("td.customerID").text();
	//	var cropID = $("table#table_ozblu>tbody>tr").find("td.cropID").text();
	//	var consigneeID = $("table#table_ozblu>tbody>tr").find("td.consigneeID").text();
	//	var consignee = $("table#table_ozblu>tbody>tr").find("td.consignee").text();
	//	var destinationID = $("table#table_ozblu>tbody>tr").find("td.destinationID").text();
	//	var destination = $("table#table_ozblu>tbody>tr").find("td.destination").text();
	//	var consigneeDestinationID = $("table#table_ozblu>tbody>tr").find("td.consigneeDestinationID").text();

	//} else {

	//	if (cropID == "BLU") {
	//		var customerID = $("table#table_ozblu>tbody>tr").find("td.customerID").text();
	//		var cropID = $("table#table_ozblu>tbody>tr").find("td.cropID").text();
	//		var consigneeID = $("table#table_ozblu>tbody>tr").find("td.consigneeID").text();
	//		var consignee = $("table#table_ozblu>tbody>tr").find("td.consignee").text();
	//		var destinationID = $("table#table_ozblu>tbody>tr").find("td.destinationID").text();
	//		var destination = $("table#table_ozblu>tbody>tr").find("td.destination").text();
	//		var consigneeDestinationID = $("table#table_ozblu>tbody>tr").find("td.consigneeDestinationID").text();
	//	}
	//	else {
	//		var customerID = $("table#table_aga>tbody>tr").find("td.customerID").text();
	//		var cropID = $("table#table_aga>tbody>tr").find("td.cropID").text();
	//		var consigneeID = $("table#table_aga>tbody>tr").find("td.consigneeID").text();
	//		var consignee = $("table#table_aga>tbody>tr").find("td.consignee").text();
	//		var destinationID = $("table#table_aga>tbody>tr").find("td.destinationID").text();
	//		var destination = $("table#table_aga>tbody>tr").find("td.destination").text();
	//		var consigneeDestinationID = $("table#table_aga>tbody>tr").find("td.consigneeDestinationID").text();

	//	}
	//}

	//var frmClientN = false;

	//if (1 == 1) {
	//	//Envpio de datos a la api
	//	var frmConsigneeDestination = new FormData();

	//	if (customerID == 0) {
		
	//		frmConsigneeDestination.append("cropID", cropID); //las letras rojo es la variable en api y blanca mi variable
	//		frmConsigneeDestination.append("consigneeID", consigneeID);
	//		frmConsigneeDestination.append("destinationID", destinationID);
	//		frmConsigneeDestination.append("customerID", customerID);
	//		frmConsigneeDestination.append("consigneeDestinationID", consigneeDestinationID);
						
	//	}
	//	else {
	//		frmConsigneeDestination.append("cropID", cropID); //las letras rojo es la variable en api y blanca mi variable
	//		frmConsigneeDestination.append("consigneeID", consigneeID);
	//		frmConsigneeDestination.append("destinationID", destinationID);
	//		frmConsigneeDestination.append("customerID", customerID);
	//		frmConsigneeDestination.append("consigneeDestinationID", consigneeDestinationID);
	//	}

	//	var api = "/Maintenance/SaveConsigneeDestinationID"
	//	$.ajax({
	//		type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
	//		url: api,
	//		async: false,
	//		contentType: "application/json",
	//		dataType: 'json',
	//		data: JSON.stringify(Object.fromEntries(frmConsigneeDestination)),
	//		Accept: "application/json",

	//		success: function (datares) {
	//			if (datares.length > 0) {
	//				sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
	//				loadListPrincipal();
	//				//$('#myModal2').modal('hide');					
	//			} else {
	//				sweet_alert_error("Error", "NO se guardaron los datos!");
	//			}
	//		},
	//		error: function (xhr, ajaxOptions, thrownError) {
	//			sweet_alert_error("Error", "NO se guardaron los datos!");
	//		}
	//	});
	//}
}

function createCodePack(){

	//var cropID3 = _cropID2;
	
	//var abbVariety = dataVariety.filter(item => item.id == $("#selectVariety option:selected").val())[0].abbreviation
	//var abbFormat = $('#selectFormat option:selected').text()
	//var abbTypeBox = dataTypeBox.filter(item => item.id == $("#selectTypeBox option:selected").val())[0].abbreviation
	//var abbPresentation = dataPresentation.filter(item => item.id == $("#selectPresentation option:selected").val())[0].presentation
	//var abbPresentation2 = $('#selectPresentation option:selected').val()
	//var abbBrand = dataBrand.filter(item => item.id == $("#selectBrand option:selected").val())[0].abbreviation
	////Verifico si es GenericBox
	//var abbGeneric = $('#checkGenericBox').prop('checked') ? 1 : 0

	////Obtengo la primera letra de typeBox
	//var abbTypeBox2 = abbTypeBox.substring(0, 1);
	//var abbBrand2 = abbBrand.substring(0, 1);

	//if (cropID3 == "UVA") {
	//	alert("uva");

	//	////Verifico si es GenericBox
	//	//var abbGeneric = $('#checkGenericBox').prop('checked') ? 1 : 0

	//	////Obtengo la primera letra de typeBox
	//	//var abbTypeBox2 = abbTypeBox.substring(0, 1);
	//	//var abbBrand2 = abbBrand.substring(0, 1);

	
	//	if (abbGeneric == 1) {//Está marcado el check

	//		if (abbPresentation2 == 11) {// Sí el id presentacion es =11 no agrego el abbbrand

	//			var codPacking = abbVariety + '_' + abbFormat + abbTypeBox2.trim() + 'G' + '_' + abbPresentation;//buen
	//		}
	//		else {
	//			var codPacking = abbVariety + '_' + abbFormat + abbTypeBox2.trim() + 'G' + '_' + abbPresentation + abbBrand;
	//		}
			
	//	}
	//	else {

	//		if (abbPresentation2 == 11) {
	//			var abbPresentation2 = abbPresentation.substring(0, 1);

	//			var codPacking = abbVariety + '_' + abbFormat + abbTypeBox2.trim() + abbBrand2  + '_' + abbPresentation;
	//		}
	//		else {

	//			var abbPresentation2 = abbPresentation.substring(0, 1);
	//			var codPacking = abbVariety + '_' + abbFormat + abbTypeBox2.trim() + abbBrand2 + '_' + abbPresentation + abbBrand;
	//		}			
	//	}

	//	$("#txtCodePack").val(codPacking)

	//}
	//else {
	//	alert("Arandanos");

	//	var codPacking = abbVariety + '_' + abbFormat + abbTypeBox2.trim() + abbBrand2 + '_' + abbPresentation;

	//	$("#txtCodePack").val(codPacking)

	//}
}

function AddSpecification() {

	//var consigneeDestinationCrop2 = _consigneeDestinationIDCrop;
	//var customerConsigneeDestinationSpecsID2 = _customerConsigneeDestinationSpecsID;
	////var customerConsigneeDestinationSpecsID = $('#customerConsigneeDestinationSpecsID').val()
	//var consigneeDestinationID = $('#consigneeDestinationID').val()

	//var varietyID = $('#selectVariety option:selected').val()
	//var variety = $('#selectVariety option:selected').text()

	//var formatID = $('#selectFormat option:selected').val()
	//var format = $('#selectFormat option:selected').text()

	//var packageProductID = $('#selectTypeBox option:selected').val()
	//var packageProduct = $('#selectTypeBox option:selected').text()	

	//var generic = $('#checkGenericBox').prop('checked') ? 1 : 0

	//var categoryID = $('#selectCategory option:selected').val()
	//var category = $('#selectCategory option:selected').text()

	//var brandID = $('#selectBrand option:selected').val()
	//var brand = $('#selectBrand option:selected').text()

	//var presentationID = $('#selectPresentation option:selected').val()
	//var presentation = $('#selectPresentation option:selected').text()

	//var sizesID = $('#selectSize option:selected').val()
	//var sizes = $('#selectSize option:selected').text()

	//var price = $('#txtPrice').val()
	
	//var codepackName = $('#txtCodePack').val()

	//var principal = $('#checkPrincipal').prop('checked') ? 1 : 0
	//var statusID = $('#checkActive').prop('checked') ? 1 : 0

	//var notes = $('#txtNotes').val()	

	////var api = "/Maintenance/GetConsDestByCust?customerID =" + customerID + " & cropID=" + cropID
	//var content = "";
	//if (customerConsigneeDestinationSpecsID2 == 0) {

	//	content += '<tr>';
	//	content += '<td style="text-align:center">';
	//	content += '<button id="" onclick="cargamodalcrop(this)" data-toggle="modal" data-target="#mymodalsection" data-backdrop="static" data-keyboard="false" class="btn btn-primary btn-sm" type="button"> <span class="icon text-white"><i class="fas fa-edit fas-sm"></i></span></button>';
	//	content += ' <span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td >';

	//	content += '<td class="customerConsigneeDestinationSpecsID" hidden> ' + customerConsigneeDestinationSpecsID2 + '</td > ';
	//	content += '<td class="consigneeDestinationID" hidden>' + consigneeDestinationCrop2 + '</td > ';

	//	//content += '<td class="varietyID"><select id="varietyID" class="form-control selectpicker form-control-sm obligatory" data-live-search="true" tabindex="-98"> ' + varietyID + '</select></td > ';
	//	content += '<td class="varietyID" hidden>' + varietyID + '</td > ';
	//	content += '<td class="variety">' + variety + '</td > ';

	//	content += '<td class="formatID" hidden>' + formatID + '</td > ';
	//	content += '<td class="format">' + format + '</td > ';

	//	content += '<td class="packageProductID" hidden>' + packageProductID + '</td > ';
	//	content += '<td class="packageProduct">' + packageProduct + '</td > ';
	//	content += '<td class="generic" hidden>' + generic + '</td > ';

	//	content += '<td class="categoryID" hidden>' + categoryID + '</td > ';
	//	content += '<td class="category">' + category + '</td > ';

	//	content += '<td class="brandID" hidden>' + brandID + '</td > ';
	//	content += '<td class="brand">' + brand + '</td > ';

	//	content += '<td class="presentationID" hidden>' + presentationID + '</td > ';
	//	content += '<td class="presentation">' + presentation + '</td > ';

	//	content += '<td class="sizesID" hidden>' + sizesID + '</td > ';
	//	content += '<td class="sizes">' + sizes + '</td > ';

	//	content += '<td class="price1">' + price + '</td > ';
	//	content += '<td class="codepackName">' + codepackName + '</td > ';

	//	content += '<td class="FildeID"></td > ';

	//	content += '<td class="principal" hidden>' + principal + '</td > ';
	//	content += '<td class="statusID" hidden>' + statusID + '</td > ';
	//	content += '<td class="notes">' + notes + '</td > ';

	//	content += '</tr>';

	//} else {

	//	content += '<tr>';
	//	content += '<td style="text-align:center">';
	//	content += '<button id="" onclick="cargamodalcrop(this)" data-toggle="modal" data-target="#mymodalsection" data-backdrop="static" data-keyboard="false" class="btn btn-primary btn-sm" type="button"> <span class="icon text-white"><i class="fas fa-edit fas-sm"></i></span></button>';
	//	content += ' <span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td >';

	//	content += '<td class="customerConsigneeDestinationSpecsID"> ' + customerConsigneeDestinationSpecsID2 + '</td > ';
	//	content += '<td class="consigneeDestinationID">' + consigneeDestinationCrop2 + '</td > ';

	//	//content += '<td class="varietyID"><select id="varietyID" class="form-control selectpicker form-control-sm obligatory" data-live-search="true" tabindex="-98"> ' + varietyID + '</select></td > ';
	//	content += '<td class="varietyID" hidden>' + varietyID + '</td > ';
	//	content += '<td class="variety">' + variety + '</td > ';

	//	content += '<td class="formatID" hidden>' + formatID + '</td > ';
	//	content += '<td class="format">' + format + '</td > ';

	//	content += '<td class="packageProductID" hidden>' + packageProductID + '</td > ';
	//	content += '<td class="packageProduct">' + packageProduct + '</td > ';
	//	content += '<td class="generic" hidden>' + generic + '</td > ';

	//	content += '<td class="categoryID" hidden>' + categoryID + '</td > ';
	//	content += '<td class="category">' + category + '</td > ';

	//	content += '<td class="brandID" hidden>' + brandID + '</td > ';
	//	content += '<td class="brand">' + brand + '</td > ';

	//	content += '<td class="presentationID" hidden>' + presentationID + '</td > ';
	//	content += '<td class="presentation">' + presentation + '</td > ';

	//	content += '<td class="sizesID" hidden>' + sizesID + '</td > ';
	//	content += '<td class="sizes">' + sizes + '</td > ';

	//	content += '<td class="price1">' + price + '</td > ';
	//	content += '<td class="codepackName">' + codepackName + '</td > ';

	//	content += '<td class="FildeID"></td > ';

	//	content += '<td class="principal" hidden>' + principal + '</td > ';
	//	content += '<td class="statusID" hidden>' + statusID + '</td > ';
	//	content += '<td class="notes">' + notes + '</td > ';

	//	content += '</tr>';

	//}
	
	////$("table#tblSpecification").DataTable().clear();
	////$("table#tblSpecification").DataTable().destroy();
	//$("table#tblSpecification tbody").append(content);
}

function SaveCrop() {
	//var customerID = _idCustomer;
	////Capturo el valor actual
	//var customerConsigneeDestinationSpecsID = $("table#tblSpecification>tbody>tr").find("td.customerConsigneeDestinationSpecsID").text();
	//var consigneeDestinationID = $("table#tblSpecification>tbody>tr").find("td.consigneeDestinationID").text();

	//var varietyID = $("table#tblSpecification>tbody>tr").find("td.varietyID").text();
	//var variety = $("table#tblSpecification>tbody>tr").find("td.variety").text();

	//var formatID = $("table#tblSpecification>tbody>tr").find("td.formatID").text();
	//var format = $("table#tblSpecification>tbody>tr").find("td.format").text();

	//var packageProductID = $("table#tblSpecification>tbody>tr").find("td.packageProductID").text();
	//var packageProduct = $("table#tblSpecification>tbody>tr").find("td.packageProduct").text();

	//var generic = $("table#tblSpecification>tbody>tr").find("td.generic").text();

	//var categoryID = $("table#tblSpecification>tbody>tr").find("td.categoryID").text();
	//var category = $("table#tblSpecification>tbody>tr").find("td.category").text();

	//var brandID = $("table#tblSpecification>tbody>tr").find("td.brandID").text();
	//var brand = $("table#tblSpecification>tbody>tr").find("td.brand").text();

	//var presentationID = $("table#tblSpecification>tbody>tr").find("td.presentationID").text();
	//var presentation = $("table#tblSpecification>tbody>tr").find("td.presentation").text();

	//var sizeID = $("table#tblSpecification>tbody>tr").find("td.sizesID").text();
	//var sizes = $("table#tblSpecification>tbody>tr").find("td.sizes").text();

	//var price = $("table#tblSpecification>tbody>tr").find("td.price").text();
	//var codepackName = $("table#tblSpecification>tbody>tr").find("td.codepackName").text();

	//var principal = $("table#tblSpecification>tbody>tr").find("td.principal").text();
	//var statusID = $("table#tblSpecification>tbody>tr").find("td.statusID").text();

	//var notes = $("table#tblSpecification>tbody>tr").find("td.notes").text();
	//var file = $("table#tblSpecification>tbody>tr").find("td.file").text();

	//var name = $(xthis).closest("tr").find("#name").text();
	//var abbreviation = $(xthis).closest("tr").find("#abbreviation").text();
	//var address = $(xthis).closest("tr").find("#address").val();
	//var contact = $(xthis).closest("tr").find("#contact").val();
	//var phone = $(xthis).closest("tr").find("#phone").text();
	//var email = $(xthis).closest("tr").find("#email").val();
	//var originID = $(xthis).closest("tr").find("#originID").text();
	// _idCustomer = $(xthis).closest("tr").find("#customerID").text();//Variable Oculta

	//var frmCrop = false;
	//if (1 == 1) {
	//	//Envpio de datos a la api
	//	var frmCrop = new FormData();

	//	if (customerConsigneeDestinationSpecsID == 0) {
	//		frmCrop.append("customerConsigneeDestinationSpecsID", customerConsigneeDestinationSpecsID);
	//		frmCrop.append("consigneeDestinationID", consigneeDestinationID);
	//		frmCrop.append("varietyID", varietyID);
	//		frmCrop.append("formatID", formatID);
	//		frmCrop.append("packageProductID", packageProductID);
	//		frmCrop.append("generic", generic);
	//		frmCrop.append("categoryID", categoryID); //las letras rojo es la variable en api y blanca mi variable
	//		frmCrop.append("brandID", brandID);
	//		frmCrop.append("presentationID", presentationID);
	//		frmCrop.append("sizesID", sizeID);
	//		frmCrop.append("price", price);
	//		frmCrop.append("codepackName", codepackName);
	//		frmCrop.append("file", file);
			
	//		frmCrop.append("principal", principal);
	//		frmCrop.append("statusID", statusID);
	//		frmCrop.append("comments", notes);
	//		frmCrop.append("string", notes);

	//		//var api = "/Maintenance/GetCreateCustomer"
	//	}
	//	else {
	//		frmClientN.append("customerID", customerID);
	//		frmClientN.append("name", name); //las letras rojo es la variable en api y blanca mi variable
	//		frmClientN.append("address", address);
	//		frmClientN.append("contact", contact);
	//		frmClientN.append("phone", phone);
	//		frmClientN.append("email", email);
	//		frmClientN.append("abbreviation", abbreviation);
	//		frmClientN.append("idOrigin", idOrigin);
	//		frmClientN.append("moreinfo", moreinfo);
	//		frmClientN.append("customer", customer);
	//		frmClientN.append("consignee", consignee);
	//		frmClientN.append("notify", notify);
	//		frmClientN.append("destinations", JSON.stringify(destinations_table));

	//	}

	//	var api = "/Maintenance/saveSpecsConsgineeDestination"
	//	$.ajax({
	//		type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
	//		url: api,
	//		async: false,
	//		contentType: "application/json",
	//		dataType: 'json',
	//		data: JSON.stringify(Object.fromEntries(frmCrop)),
	//		Accept: "application/json",

	//		success: function (datares) {
	//			if (datares.length > 0) {
	//				sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
	//				loadListPrincipal();
	//				//$('#myModal2').modal('hide');					
	//			} else {
	//				sweet_alert_error("Error", "NO se guardaron los datos!");
	//			}
	//		},
	//		error: function (xhr, ajaxOptions, thrownError) {
	//			sweet_alert_error("Error", "NO se guardaron los datos!");
	//		}
	//	});
	//}
}