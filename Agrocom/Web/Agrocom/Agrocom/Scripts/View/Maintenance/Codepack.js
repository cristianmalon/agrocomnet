﻿//var uriWs = 'http://localhost/api/';
var uriWs = 'https://localhost:44306/';
//var uriWs = 'http://200.48.91.28/AGROCOMapi/api/';
//var uriWs = 'https://apiagrocom.azurewebsites.net/api/';


!function ($) {
    $(function () {
        loadComboCrop();
        loadComboVia();
        console.log("CARGAR COMBOS");
        //$("#cboVia").val(1).change();
    });

}(window.jQuery);
function loadComboCrop() {
    console.log("CARGAR COMBOSCROP");
    $.get("ListCrop", function (data) {
        var ObjData = JSON.parse(data);
        console.log("LIST CROPS: " + JSON.stringify(JSON.parse(data)));
        //var dataCrop = [{ "CROPID": "BLU", "DESCRIPTION": "BLUEBERRY" }];
        //fillComboCrop(ObjData, document.getElementById("cboCrop"), false);
        fillComboCrop(ObjData, document.getElementById("CROP"), false);
    });

}
function loadComboVia() {
    $.get("ListVia", function (data) {
        var ObjData = JSON.parse(data);
        //var dataVia = [{ "viaID": "1", "name": "Seafreight", "statusID": 1 }, { "viaID": "2", "name": "Air", "statusID": 1 }];
        console.log("LIST VIAS: " + JSON.stringify(JSON.parse(data)));
        fillComboVia(ObjData, document.getElementById("cboVia"), false);
        fillComboViaModal(ObjData, document.getElementById("viaID"), false);
    });

}
function fillComboCrop(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].cropid + "'>";
        contenido += data[i].description;
        contenido += "</option>";
    }
    control.innerHTML = contenido;
    //$("#cboVia").val(data[0].cropid);
    $("#CROP").val(data[0].cropid).change();
    //Lista();
}
function fillComboVia(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].viaID + "'>";
        contenido += data[i].name;
        contenido += "</option>";
    }
    control.innerHTML = contenido;
    //$("#cboVia").val(data[0].viaID);
    $("#cboVia").val(data[0].viaID).change();
    //Lista();
}
function fillComboViaModal(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].viaID + "'>";
        contenido += data[i].name;
        contenido += "</option>";
    }
    control.innerHTML = contenido;
    //$("#cboVia").val(data[0].viaID);
    $("#viaID").val(data[0].viaID).change();
    //Lista();
}
function Lista() {
    //$.get(uriWs + "PO/GetAllFiltered/?WeekIniid=" + "11" + "&WeekEndId=" + "34", function (data) {

    //var idUser = $('#lblUserId').text();
    var idUser = 14;
    var idCboVia = $("#cboVia").val();
    //var idCboCrop = $("#cboCrop").val();
    var idCboCrop = "BLU";
    //$.get(uriWs + "ShipmentSt/ShipmentStPOByWeekId/?idUser=" + idUser + "&beginDate=" + idCboIni + "&endDate=" + idCboFin, function (data) {
    //    //crearListadoStockGrower(["Id", "GROWER", "AVAILABLE", "WEEK", "KG", "SIZE", "VARIETY", "BRAND", "ORIGIN"], data);
    //    crearListado(data);
    //});
    console.log("URL CROP: " + "ListCode/?idVia=" + idCboVia + "&cropID=" + idCboCrop);
    //var url = uriWs + "Maintenance/ListCode/?idVia=" + idCboVia;
    $.ajax({
        async: false,
        type: 'GET',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "ListCode/?idVia=" + idCboVia + "&cropID=" + idCboCrop,
        //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
        success: function (data) {
            var ObjData;
            //ListModalUpload(data, poID);
            if (data != null && data != undefined && data != "") {
                ObjData = JSON.parse(data);
                console.log("LIST CODEPACKS: DATA LENGTH " + ObjData.length);
            } else {
                ObjData = [];
            }

            if (ObjData.length > 0) {
                crearListado(ObjData);
            } else {
                noData();
            }
        }
    });
}
function ListTableDistrib() {
    //$.get(uriWs + "PO/GetAllFiltered/?WeekIniid=" + "11" + "&WeekEndId=" + "34", function (data) {

    //var idUser = $('#lblUserId').text();
    var idUser = 14;
    var idCboIni = $("#cboSemanaIni").val();
    var idCboFin = $("#cboSemanaFin").val();
    console.log("ID DE USUARIO PRINCIPAL: " + idUser + "IDCBOINI: " + idCboIni + "IDCBOFIN: " + idCboFin);
    //$.get(uriWs + "ShipmentSt/ShipmentStPOByWeekId/?idUser=" + idUser + "&beginDate=" + idCboIni + "&endDate=" + idCboFin, function (data) {
    //    //crearListadoStockGrower(["Id", "GROWER", "AVAILABLE", "WEEK", "KG", "SIZE", "VARIETY", "BRAND", "ORIGIN"], data);
    //    crearListado(data);
    //});
    var url = uriWs + "ShipmentSt/ShipmentStPOByWeekId/?idUser=" + idUser + "&beginDate=" + idCboIni + "&endDate=" + idCboFin;
    $.ajax({
        async: false,
        type: 'GET',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: url,
        success: function (data) {
            //ListModalUpload(data, poID);
            if (data.length > 0) {
                LoadTableDistrib(data);
            } else {
                noData();
            }
        }
    });
}
function LoadTableDistrib(dataDetails) {
    var contenido = "";
    var arryColumnsDetail = Object.keys(dataDetails[0]);
    //console.log("week 1: " + week1 + "/ week2: " + week2 + "/ checkd:" + chekd);
    //contenido += "<tr style='display: none;'><td colspan=12>";

    contenido += "<table  id='TableModal' class='table table-hover table-bordered table-bordered tblSalesRequest' >";
    contenido += "<thead class='thead-agrocom'>";


    contenido += "<tr>";
    for (var z = 0; z < arryColumnsDetail.length - 1; z++) {
        contenido += "<th>";
        contenido += arryColumnsDetail[z];
        contenido += "</th>";
    }
    contenido += "<th>";
    contenido += "USD/Kg";
    contenido += "</th>";
    contenido += "</tr>";
    contenido += "</thead>";
    contenido += "<tbody id='bodyTable'>";
    var llavesDet = Object.keys(dataDetails[0]);
    for (var i = 0; i < dataDetails.length; i++) {
        //contenido += "<tr>";
        contenido += "<tr class='header rowTable'>";
        for (var j = 0; j < llavesDet.length - 1; j++) {
            //if (dataDetails[i][llavesDet[10]] == '1') {
            //var valorLLavesDet = llavesDet[j];
            contenido += "<td >";
            contenido += dataDetails[i][llavesDet[j]];
            contenido += "</td>";
            //}
        }
        contenido += "<td>";
        contenido += "<input style='position: relative; width: 100%; float:left;vertical-align:top;' type='text' placeholder='USD/Kg' />";
        contenido += "</td>";
        contenido += "</tr>";
    }
    contenido += "</tbody>";
    contenido += "</table>";
    document.getElementById("CreateCodePack").innerHTML = contenido;
}
function crearListado(dataDetails) {

    var contenido = "";
    var arryColumnsDetail = Object.keys(dataDetails[0]);
    //console.log("week 1: " + week1 + "/ week2: " + week2 + "/ checkd:" + chekd);
    //contenido += "<tr style='display: none;'><td colspan=12>";

    contenido += "<table  id='TableModal' class='table table-hover table-bordered table-bordered tblSalesRequest' style='font-size: smaller;'>";
    contenido += "<thead class='thead-agrocom'>";


    contenido += "<tr>";
    for (var z = 1; z < arryColumnsDetail.length; z++) {
        contenido += "<th style='text-align: center;'>";
        contenido += arryColumnsDetail[z];
        contenido += "</th>";
    }
    //contenido += "<th>VIEW</th>";
    contenido += "</tr>";
    contenido += "</thead>";
    contenido += "<tbody id='bodyTable'>";
    var llavesDet = Object.keys(dataDetails[0]);
    for (var i = 0; i < dataDetails.length; i++) {
        //contenido += "<tr>";
        contenido += "<tr>";
        for (var j = 1; j < llavesDet.length; j++) {
            //if (dataDetails[i][llavesDet[10]] == '1') {
            //var valorLLavesDet = llavesDet[j];
            if (j > 1 && j < llavesDet.length - 2) {
                contenido += "<td style='text-align: right;'>";
                contenido += dataDetails[i][llavesDet[j]];
                contenido += "</td>";
            } else {
                contenido += "<td style='text-align: center;'>";
                contenido += dataDetails[i][llavesDet[j]];
                contenido += "</td>";
            }

            //}
        }
        contenido += "<td align='center'>";
        contenido += "<button id='btnEdit' type='button' value='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal' data-backdrop='static' data-keyboard='false'>Edit</button>";

        contenido += "</td>";
    }

    contenido += "</tbody>";
    contenido += "</table>";
    contenido += "</tr>";
    document.getElementById("listTable").innerHTML = contenido;

}
function noData() {
    var contenido = "";
    console.log(" NO  HAY DATOS ");
    contenido += "<div>";
    contenido += "    <table class='table table - striped jambo_table bulk_action' cellspacing='0' rules='all' border='1' id='ContentPlaceHolder1_datos' style='border - collapse: collapse; '>";
    contenido += "        <tbody><tr class='headings' align='center' valign='middle' style='color:White;background-color:#405467;font-weight:bold;' >";
    contenido += "            <th scope='col'>MESSAGE</th>";
    contenido += "        </tr><tr class='even pointer' align='center'>";
    contenido += "<td>Without results.</td>";
    contenido += "            </tr>";
    contenido += "        </tbody></table>";
    contenido += "</div>";
    document.getElementById("listTable").innerHTML = contenido;
}
function $N(value, ifnull) {
    if (value === null || value === undefined || value === "")
        return ifnull;
    return value;
}
function checkValues() {
    if ($("#description").val() != "" && $("#weight").val() != "" && $("#clamshellPerBox").val() != "" && $("#boxesPerPallet").val() != "" && $("#layersPerPallet").val() != "" && $("#clamshellPerContainer").val() != "" && $("#boxPerContainer").val() != "" && $("#netWeightContainer").val() != "") {
        return true;
    } else {
        return false;
    }
}
function LoadEditModal(idCodepack) {
    $.ajax({
        async: false,
        type: "GET",
        url: "GetCodePackById/?idCodepack=" + idCodepack,
        error: function (error) {
            console.log("Error Loading Codepack: " + JSON.stringify(error));
        },
        success: function (data) {
            var ObjData = JSON.parse(data);
            console.log("CODEPACKS LOADED: " + JSON.stringify(ObjData) + " // SOME VALUE: " + data[0].codePackID + " //ANOTHERVALUE: " + ObjData[0].description);
            $("#btnSaveModal").val(ObjData[0].codePackID);
            $("#description").val(ObjData[0].description);
            $("#clamshellPerBox").val(ObjData[0].clamshellPerBox);
            $("#weight").val(ObjData[0].weight);
            $("#boxesPerPallet").val(ObjData[0].boxesPerPallet);
            $("#layersPerPallet").val(ObjData[0].layersPerPallet);
            $("#clamshellPerContainer").val(ObjData[0].clamshellPerContainer);
            $("#boxPerContainer").val(ObjData[0].boxPerContainer);
            $("#netWeightContainer").val(ObjData[0].netWeightContainer);
            $("#statusID").val(ObjData[0].statusID).change();
            $("#viaID").val(ObjData[0].viaID).change();
            $("#CROP").val(ObjData[0].cropID).change();
        }
    });
}
function SaveCodePack(idCodepack) {

    var frm = new FormData();
    //frm.append("clamshellPerBox", 0);
    //frm.append("weight", 0.0);
    //frm.append("boxesPerPallet", 0);
    //frm.append("layersPerPallet", $('#layersPerPallet').val());
    //frm.append("clamshellPerContainer", 0);
    //frm.append("boxPerContainer", 0);
    //frm.append("netWeightContainer", 0.0);

    frm.append("description", $("#description").val());
    frm.append("clamshellPerBox", $N($("#clamshellPerBox").val(), 0));
    frm.append("weight", $N($("#weight").val(), 0.0));
    frm.append("boxesPerPallet", $N($("#boxesPerPallet").val(), 0));
    frm.append("layersPerPallet", $('#layersPerPallet').val());
    frm.append("clamshellPerContainer", $N($("#clamshellPerContainer").val(), 0));
    frm.append("boxPerContainer", $N($('#boxPerContainer').val(), 0));
    //frm.append("netWeightContainer", 0.0);
    frm.append("netWeightContainer", $N($('#netWeightContainer').val(), 0.0));

    if (idCodepack == 0) {
        frm.append("viaID", $('#viaID').val());
        frm.append("statusID", $('#statusID').val());
        frm.append("cropID", $('#CROP').val());

        if (confirm("Desea realizar la operacion?") == 1) {
            console.log("Data cabecera");
            console.log(JSON.stringify(Object.fromEntries(frm)));
            $.ajax({
                async: false,
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "GetCreateCodepack",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",
                Accept: "application/json",
                error: function (error) {
                    console.log("Error Saving Codepack: " + JSON.stringify(error));

                },
                dataType: 'json',
                success: function (data) {
                    alert("Succefull Saved !!");
                    console.log("success Codepack: " + JSON.stringify(data));
                    Lista();
                }
            });
        }
    } else {
        frm.append("codePackID", idCodepack);
        if (confirm("Desea realizar la operacion?") == 1) {
            console.log("Data cabecera");
            console.log(JSON.stringify(Object.fromEntries(frm)));
            $.ajax({
                async: false,
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "GetUpdateCodepack",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",
                Accept: "application/json",
                error: function (error) {
                    console.log("Error Updating Codepack: " + JSON.stringify(error));

                },
                dataType: 'json',
                success: function (data) {
                    alert("Codepack Updated Successfully !!");
                    console.log("success Update Codepack: " + JSON.stringify(data));

                    Lista();
                }
            });
        }
    }
}

$(document).ready(function () {
    //$('#cboCrop').change(function (e) {
    //    Lista();
    //});
    $('#cboVia').change(function (e) {
        var viaID = $('#cboVia').val();
        Lista();
    });
    $(document).on("click", "#btnSaveModal", function () {
        console.log("CLICKED SAVE");
        //$('#openModal2').modal('show');
        var current = $(this).val();
        if (checkValues()) {
            SaveCodePack(current);
        } else {
            alert("Must complete all fields!!");
        }

    });
    $(document).on("click", "#btnNew", function () {

        $("#btnSaveModal").val("0");
        $("#description").val("");
        $("#clamshellPerBox").val("");
        $("#weight").val("");
        $("#boxesPerPallet").val("");
        $("#layersPerPallet").val("");
        $("#clamshellPerContainer").val("");
        $("#boxPerContainer").val("");
        $("#netWeightContainer").val("");
        $("#statusID").val(1).change();
        $("#statusID").prop("disabled", true);
        //$("#btnSaveModal").val(0);
        console.log("CLICKED NEW valsave:" + $("#btnSaveModal").val());
    });
    $(document).on("click", "#btnEdit", function () {

        //$('#openModal2').modal('show');
        var current = $(this).val();
        $("#statusID").prop("disabled", false);
        console.log("VAL EDIT CODEPACKID: " + current);
        $("#btnSaveModal").val(current);
        LoadEditModal(current);
        console.log("CLICKED EDIT valsave:" + $("#btnSaveModal").val());
    });
    //$(document).on("click", "tr.header", function () {
    //    //console.log("asd");
    //    ListTableDistrib();
    //    $(this).nextUntil('tr.header').css('display', function (i, v) {
    //        console.log("xxx");
    //        return this.style.display === 'table-row' ? 'none' : 'table-row';
    //    });
    //    //alert("asd");
    //    $("#TableModal tr").removeClass("highlight");
    //    $("#TableModal tr").removeClass("rowSpecificationkSelected");
    //    $(this).closest(".rowTable").addClass("highlight");
    //    $(this).closest(".rowTable").addClass("rowSpecificationkSelected");
    //});
});
function loadModal(POID) {
    console.log("CLICKED UPLOAD1111111111");
    //ListTableModal(POID);
    //POModalShipmentST(POID);
}

