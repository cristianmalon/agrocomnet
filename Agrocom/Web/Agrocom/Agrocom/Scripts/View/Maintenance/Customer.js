﻿//var uriWs = 'http://localhost/api/';
var uriWs = 'https://localhost:44306/';
//var uriWs = 'http://200.48.91.28/AGROCOMapi/api/';
//var uriWs = 'https://apiagrocom.azurewebsites.net/api/';


!function ($) {
    $(function () {
        ListCustomers();
        console.log("CARGAR COMBOS");
        loadComboOrigin();
        //$("#cboVia").val(1).change();
    });

}(window.jQuery);
function loadComboDestination() {
    $.get("ListDestination", function (data) {
        var ObjData = JSON.parse(data);
        //var dataVia = [{ "viaID": "1", "name": "Seafreight", "statusID": 1 }, { "viaID": "2", "name": "Air", "statusID": 1 }];
        console.log("LIST DESTINATION: " + JSON.stringify(ObjData));
        fillComboDestination(ObjData, document.getElementById("cboDest"), true);
    });
}
function fillComboDestination(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].destinationID + "'>";
        contenido += data[i].origin + " - " + data[i].destinaion;
        contenido += "</option>";
    }
    control.innerHTML = contenido;
    $("#cboDest").val(0).change();
    //ListCustomers();
}
function loadComboOrigin() {
    console.log("CARGAR COMBO ORIGIN:");
    $.get("GetOrigin", function (data) {
        var ObjData = JSON.parse(data);
        console.log("LIST ORIGINS: " + JSON.stringify(JSON.parse(data)));
        //var dataCrop = [{ "CROPID": "BLU", "DESCRIPTION": "BLUEBERRY" }];
        fillComboOrigin(ObjData, document.getElementById("cbOrigin"), true);
    });

}


function fillComboOrigin(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].originID + "'>";
        contenido += data[i].description;
        contenido += "</option>";
    }
    control.innerHTML = contenido;
    $("#cbOrigin").val(data[0].originID);
    $("#cbOrigin").val(0).change();
    //ListCustomers();
}
function ListCustomers() {

    var searchName = $("#searchName").val();
    //var url = uriWs + "Maintenance/ListCode/?idVia=" + idCboVia;
    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListCustomer?searchName=" + searchName,
        //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
        success: function (data) {
            //ListModalUpload(data, poID);
            var ObjData = JSON.parse(data);
            console.log("LIST CUSTOMERS: " + JSON.stringify(JSON.parse(data)));
            if (data.length > 0) {
                crearListado(ObjData);
            } else {
                noData();
            }
        }
    });
}

function crearListado(dataDetails) {

    var contenido = "";
    var arryColumnsDetail = Object.keys(dataDetails[0]);
    //console.log("week 1: " + week1 + "/ week2: " + week2 + "/ checkd:" + chekd);
    //contenido += "<tr style='display: none;'><td colspan=12>";

    contenido += "<table  id='TableModal1' class='table table-hover table-bordered table-bordered tblSalesRequest' style='font-size: smaller;'>";
    contenido += "<thead class='thead-agrocom'>";


    contenido += "<tr>";
    for (var z = 1; z < arryColumnsDetail.length; z++) {
        contenido += "<th>";
        contenido += arryColumnsDetail[z];
        contenido += "</th>";
    }
    contenido += "<th> EDIT </th>"
    contenido += "</tr>";
    contenido += "</thead>";
    contenido += "<tbody id='bodyTable1'>";
    var llavesDet = Object.keys(dataDetails[0]);
    for (var i = 0; i < dataDetails.length; i++) {
        //contenido += "<tr>";
        contenido += "<tr>";
        for (var j = 1; j < llavesDet.length; j++) {
            //if (dataDetails[i][llavesDet[10]] == '1') {
            //var valorLLavesDet = llavesDet[j];
            contenido += "<td >";
            contenido += dataDetails[i][llavesDet[j]];
            contenido += "</td>";
            //}
        }


        contenido += "<td>";
        contenido += "<button id='btnEdit' type='button' value='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal' data-backdrop='static' data-keyboard='false'>Edit</button>";

        contenido += "</td>";
    }

    contenido += "</tbody>";
    contenido += "</table>";
    contenido += "</tr>";
    document.getElementById("listTable").innerHTML = contenido;

}
function noDataModal() {
    var dataDetails2 = [{ "destinationID": 20, "customerID": 68, "customer": "Amazon", "originID": "CHI", "description": "Hong Kong", "statusID": "1" }];
    var contenido = "";
    console.log(" NO  HAY DATOS 2");
    console.log("LENGHT NODATA: " + dataDetails2.length);
    var arryColumnsDetail = Object.keys(dataDetails2[0])
    contenido += "<table  id='TableModal2' class='table table-hover table-bordered table-bordered tblSalesRequest' >";
    contenido += "<thead class='thead-agrocom'>";
    console.log("LENGHT NODATA2: " + arryColumnsDetail.length);

    contenido += "<tr>";
    for (var z = 2; z < arryColumnsDetail.length; z++) {
        contenido += "<th>";
        contenido += arryColumnsDetail[z];
        contenido += "</th>";
    }
    //contenido += "<th> EDIT </th>"
    contenido += "</tr>";
    contenido += "</thead>";
    contenido += "<tbody id='bodyTable2'>";

    contenido += "</tbody>";
    contenido += "</table>";
    contenido += "</tr>";
    document.getElementById("tblDestination").innerHTML = contenido;
}
function DeleteCustomerDestination() {
    var ret;
    console.log("Deletions Lenght: " + DeleteCustomersID.length);
    if (DeleteCustomersID.length == 0) {
        ret = true;
    }
    for (var i = 0; i < DeleteCustomersID.length; i++) {
        var frm = new FormData();
        frm.append("destinationID", DeleteDestinationsID[i]);
        frm.append("customerID", DeleteCustomersID[i]);
        console.log("JSON DELETE DESTINATIONS: " + JSON.stringify(Object.fromEntries(frm)));
        $.ajax({
            async: false,
            type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            url: "GetCustDestDelete",
            data: JSON.stringify(Object.fromEntries(frm)),
            contentType: "application/json",
            Accept: "application/json",
            dataType: 'json',
            error: function (error) {
                console.log("ERROR DELETING CUSTOMERDESTINATION:" + Object.keys(error));
                ret = false;
            },
            success: function (data) {
                console.log("SUCCESS CUSTOMERDESTINAITON DELETED: " + Object.keys(data));
                ret = true;
                //alert("CustomerDestination Correctly Saved!");
            }
        });
        //if (ret) {
        //    return ret;
        //} else {
        //    return ret;
        //    break;
        //}
        console.log("return: " + ret);

    }
    DeleteCustomersID = [];
    DeleteDestinationsID = [];
    return ret;
}
function SaveCustomerDestination(customerID) {
    //var retdelval;
    //var retsaveval;
    var CreateDestinationsID = [];
    var CreateCustomersID = [];
    var isDeleted = DeleteCustomerDestination();
    console.log("isDeleted?: " + isDeleted);
    if (isDeleted) {
        $('#TableModal2>tbody>tr').each(function (index) {
            var frm = new FormData();
            frm.append("customerDestinationID", 0);
            frm.append("cropID", "BLU");
            var destID;
            var custID;
            $(this).find('td').each(function (indextd) {
                if (indextd == 0) {
                    console.log("valor index0 DestinationID: " + $(this).text());
                    CreateDestinationsID.push($(this).text());
                    destID = $(this).text();
                    console.log("DeleteDestinations: " + JSON.stringify(DeleteDestinationsID));
                }
                if (indextd == 1) {
                    console.log("valor index1 CustomerID: " + $(this).text());
                    console.log("DeleteCustomers: " + JSON.stringify(DeleteCustomersID));
                    CreateCustomersID.push($(this).text());
                    custID = $(this).text();
                }
            });
            DeleteCustomersID = CreateCustomersID;
            DeleteDestinationsID = CreateDestinationsID;
            frm.append("destinationID", destID);
            frm.append("customerID", customerID);
            console.log("JSON CREATE DESTINAITONS: " + JSON.stringify(Object.fromEntries(frm)))
            console.log("DeleteCustomersF: " + JSON.stringify(DeleteCustomersID));
            console.log("DeleteDestinationsF: " + JSON.stringify(DeleteDestinationsID));
            $.ajax({
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "GetCustDestCreate",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",

                Accept: "application/json",
                dataType: 'json',
                error: function (error) {
                    console.log("ERROR CREATING CUSTOMERDESTINATION:" + Object.keys(error));
                },
                success: function (data) {
                    console.log("SUCCESS CUSTOMERDESTINAITON CREATED: " + Object.keys(data));
                    //alert("CustomerDestination Correctly Saved!");
                }
            });
        });
    }
}
function SaveCustomer(idCustomer) {
    console.log("customerID: " + idCustomer);
    var frm = new FormData();
    frm.append("name", $("#NameBussines").val());
    frm.append("address", $("#Address").val());
    frm.append("contact", $("#Contact").val());
    frm.append("phone", $('#Telephone').val());
    frm.append("email", $('#Email').val());
    frm.append("abbreviation", $('#Abbreviation').val());

    if (idCustomer != "") {
        console.log("IDCUSTOMER NO VACIO: " + idCustomer);
        frm.append("customerID", idCustomer);
        if (confirm("Do you want to do this operation?") == 1) {
            console.log("JSON:" + JSON.stringify(Object.fromEntries(frm)));
            $.ajax({
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "GetUpdateCustomer",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",
                Accept: "application/json",
                dataType: 'json',
                error: function (error) {
                    console.log("ERROR SAVING CUSTOMER:" + Object.keys(error));
                },
                success: function (data) {

                    SaveCustomerDestination(idCustomer);
                    alert("Customer Correctly Saved!");
                    //} else {
                    //    alert("Customer Correctly Saved with no Destinations modified!");
                    //}
                    ListCustomers();
                    $("#openModal").modal('hide');//ocultamos el modal
                    //$('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                    $('.modal-backdrop').remove();//eliminamos el backdrop del modal
                },
                complete: function (data) { }
            });
        }
    } else {
        console.log("IDCUSTOMER VACIO: " + idCustomer);
        frm.append("idOrigin", $('#cbOrigin').val());
        if (confirm("Desea realizar la operacion?") == 1) {
            console.log("JSON:" + JSON.stringify(Object.fromEntries(frm)));
            $.ajax({
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "GetCreateCustomer",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",
                Accept: "application/json",
                dataType: 'json',
                error: function (error) {
                    alert("ERROR:" + Object.keys(error));
                },
                success: function (data) {
                    console.log("DATA CUSTOMER: " + Object.keys(data));
                    console.log("DATA CUSTOMER2: " + JSON.stringify(data[0].customerID));
                    
                    SaveCustomerDestination(data[0].customerID);
                    alert("Customer Correctly Created!");
                    ListCustomers();
                    $("#openModal").modal('hide');//ocultamos el modal
                    //$('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                    $('.modal-backdrop').remove();//eliminamos el backdrop del modal
                }
            });
        }
    }




}
$(document).ready(function () {
    $('#cboCrop').change(function (e) {

    });

    $(document).on("click", "#btnNew", function () {
        var ObjData = [];
        console.log("CLICKED NEW");
        loadModal(ObjData, "");
        var idCustumer = $(this).val();
        $("#btnSaveModal").val("");
        console.log("idCustumer: " + idCustumer);
        loadComboDestination();
        $("#cbOrigin").val(0).change();
        //document.getElementById('cboDest').disabled = true;
        //document.getElementById('btnAddDest').disabled = true;
        document.getElementById('cbOrigin').disabled = false;
    });
    $(document).on("click", "#btnSearch", function () {
        console.log("CLICKED SEARCH");
        ListCustomers();
    });
    $(document).on("click", "#btnSaveModal", function () {
        console.log("CLICKED SAVE");
        //$('#openModal2').modal('show');
        var current = $(this).val();

        SaveCustomer(current);
    });
    $(document).on("click", "#btnRemove", function () {
        console.log("CLICKED Remove");
        if (confirm("You want to remove a destination?") == 1) {
            $(this).closest('tr').remove();
        }
        //$('#openModal2').modal('show');

    });
    $(document).on("click", "#btnAddDest", function () {
        console.log("CLICKED Add");
        //"<td><button id='btnRemove' type='button'>x</button></td>";
        addDestination();
    });
    $(document).on("click", "#btnEdit", function () {
        console.log("CLICKED EDIT");
        var idCustumer = $(this).val();
        $("#btnSaveModal").val(idCustumer);
        console.log("idCustumer: " + idCustumer);
        $.ajax({
            async: false,
            type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            url: "ListDestinationByCustomer/?idCustomer=" + idCustumer,
            //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
            success: function (data) {
                //ListModalUpload(data, poID);
                var ObjData = JSON.parse(data);
                console.log("LIST DESTINATIONS: " + JSON.stringify(ObjData));
                loadModal(ObjData, idCustumer);
            }
        });
        loadComboDestination();
        document.getElementById('cbOrigin').disabled = true;
    });
});
var DeleteCustomersID = [];
var DeleteDestinationsID = [];
function loadModal(dataDetails, idCustomer) {
    console.log("CLICKED LOAD1111111111");
    if (idCustomer != "") {
        $.ajax({
            async: false,
            type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            url: "GetCustomerByID/?idCustomer=" + idCustomer,
            //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
            success: function (data) {
                //ListModalUpload(data, poID);
                var ObjData = JSON.parse(data);
                console.log("CUSTOMER GET: " + ObjData);
                $("#NameBussines").val(ObjData[0].name);
                $("#Address").val(ObjData[0].address);
                $("#Telephone").val(ObjData[0].phone);
                $("#Abbreviation").val(ObjData[0].abbreviation);
                //$("#Country").val(ObjData[0].country);
                $("#cbOrigin").val(ObjData[0].originID).change();
                $("#Contact").val(ObjData[0].contact);
                $("#Email").val(ObjData[0].email);
            }
        });
    } else {
        $("#NameBussines").val("");
        $("#Address").val("");
        $("#Telephone").val("");
        $("#Abbreviation").val("");
        $("#Country").val("");
        $("#Contact").val("");
        $("#Email").val("");
    }

    if (dataDetails.length > 0) {
        var contenido = "";
        var arryColumnsDetail = Object.keys(dataDetails[0]);
        //console.log("week 1: " + week1 + "/ week2: " + week2 + "/ checkd:" + chekd);
        //contenido += "<tr style='display: none;'><td colspan=12>";

        contenido += "<table  id='TableModal2' class='table table-hover table-bordered table-bordered tblSalesRequest' >";
        contenido += "<thead class='thead-agrocom'>";


        contenido += "<tr>";
        for (var z = 0; z < arryColumnsDetail.length; z++) {
            if (z == 0 || z == 1) {
                contenido += "<th style='display:none;'>";
                //contenido += "<th>";
                contenido += arryColumnsDetail[z];
                contenido += "</th>";
            } else {
                contenido += "<th>";
                contenido += arryColumnsDetail[z];
                contenido += "</th>";
            }
        }
        //contenido += "<th> EDIT </th>"
        contenido += "</tr>";
        contenido += "</thead>";
        contenido += "<tbody id='bodyTable2'>";
        var llavesDet = Object.keys(dataDetails[0]);
        for (var i = 0; i < dataDetails.length; i++) {
            //contenido += "<tr>";
            DeleteCustomersID.push(dataDetails[i].customerID);
            DeleteDestinationsID.push(dataDetails[i].destinationID);
            contenido += "<tr>";
            console.log("DDdestinationID: " + dataDetails[i].destinationID);
            console.log("DCustomerID: " + dataDetails[i].customerID);
            for (var j = 0; j < llavesDet.length; j++) {
                if (j == 0 || j == 1) {
                    contenido += "<td style='display:none;'>";
                    //contenido += "<td>";
                    contenido += dataDetails[i][llavesDet[j]];
                    contenido += "</td>";
                } else {
                    //if (dataDetails[i][llavesDet[10]] == '1') {
                    //var valorLLavesDet = llavesDet[j];
                    if (j == llavesDet.length - 1) {
                        if (dataDetails[i].statusID == 1) {
                            contenido += "<td >Enabled</td>";
                        } else {
                            contenido += "<td >Disabled</td>";
                        }

                    } else {
                        contenido += "<td >";
                        contenido += dataDetails[i][llavesDet[j]];
                        contenido += "</td>";
                    }

                }
                //}
            }
            contenido += "<td>";
            contenido += "<button id='btnRemove' type='button'>x</button>";
            contenido += "</td>";
        }

        contenido += "</tbody>";
        contenido += "</table>";
        contenido += "</tr>";
        document.getElementById("tblDestination").innerHTML = contenido;
    } else {
        noDataModal();
    }
    //ListTableModal(POID);
    //POModalShipmentST(POID);

}
function VerifyExistDestination(idDestination) {
    var table = document.getElementById("TableModal2");
    var lastRow = table.rows.length;
    var flag = false;
    console.log("ROWS" + lastRow);
    $('#TableModal2>tbody>tr').each(function (indextr) {
        console.log("indextr" + indextr);
        $(this).find('td').each(function (indextd) {
            if (indextd == 0) {
                console.log("TDS:" + $(this).text() + "index: " + indextr);
                if (idDestination == $(this).text()) {
                    flag = true;
                }
            }
        });
    });
    return flag;
}
function addDestination() {
    var idDestination = $("#cboDest").val();
    console.log("BtnEdit Val: " + $("#btnEdit").val());
    $.get("GetDestinationById/?idDestination=" + idDestination, function (data) {
        var ObjData = JSON.parse(data);
        var isInTable = VerifyExistDestination(idDestination);
        if (!isInTable) {
            console.log("DESTINATION: " + JSON.stringify(ObjData));
            var contenido = "";
            contenido += "<tr>";
            contenido += "<td style='display:none;'>" + ObjData[0].destinationID + "</td>";
            contenido += "<td style='display:none;'>" + $("#btnSaveModal").val() + "</td>";
            //contenido += "<td>" + ObjData[0].destinationID + "</td>";
            //contenido += "<td>" + $("#btnSaveModal").val() + "</td>";
            contenido += "<td>" + $("#NameBussines").val() + "</td>";
            contenido += "<td>" + ObjData[0].originID + "</td>";
            contenido += "<td>" + ObjData[0].destination + "</td>";
            if (ObjData[0].statusID == 1) {
                contenido += "<td>Enabled</td>";
            } else {
                contenido += "<td>Disabled</td>";
            }

            contenido += "<td><button id='btnRemove' type='button'>x</button></td>";
            contenido += "</tr> ";
            $('#TableModal2 tbody').append(contenido);
            console.log("idDestination: " + ObjData[0].destinationID);
        } else {
            alert("Destination is already added!");
        }

    });

}
function noData() {
    var contenido = "";
    console.log(" NO  HAY DATOS ");
    contenido += "<div>";
    contenido += "    <table class='table table - striped jambo_table bulk_action' cellspacing='0' rules='all' border='1' id='ContentPlaceHolder1_datos' style='border - collapse: collapse; '>";
    contenido += "        <tbody><tr class='headings' align='center' valign='middle' style='color:White;background-color:#405467;font-weight:bold;' >";
    contenido += "            <th scope='col'>MESSAGE</th>";
    contenido += "        </tr><tr class='even pointer' align='center'>";
    contenido += "<td>Without results.</td>";
    contenido += "            </tr>";
    contenido += "        </tbody></table>";
    contenido += "</div>";
    document.getElementById("tabla").innerHTML = contenido;
}
