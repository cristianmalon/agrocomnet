﻿//var uriWs = 'http://localhost/api/';
//var uriWs = 'https://localhost:44306/';
//var uriWs = 'http://200.48.91.28/AGROCOMapi/api/';
//var uriWs = 'https://apiagrocom.azurewebsites.net/api/';


!function ($) {
    $(function () {

        ListMarket();
        //$("#cboVia").val(1).change();
    });

}(window.jQuery);
function loadCombo() {
    console.log("CARGAR COMBOSCROP");
    $.get("ListCrop", function (data) {
        var ObjData = JSON.parse(data);
        console.log("LIST CROPS: " + JSON.stringify(JSON.parse(data)));
        //var dataCrop = [{ "CROPID": "BLU", "DESCRIPTION": "BLUEBERRY" }];
        fillCombo(ObjData, document.getElementById("cboCrop"), false);
    });

}
function fillCombo(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].cropid + "'>";
        contenido += data[i].description;
        contenido += "</option>";
    }
    control.innerHTML = contenido;
    $("#cboVia").val(data[0].cropid);
    $("#cboCrop").val(data[0].cropid).change();
    //Lista();
}
function ListMarket() {
    //$.get(uriWs + "PO/GetAllFiltered/?WeekIniid=" + "11" + "&WeekEndId=" + "34", function (data) {

    //var idUser = $('#lblUserId').text();
    var idCboVia = $("#cboVia").val();



    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListMarket",
        //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
        success: function (data) {
            //ListModalUpload(data, poID);
            var ObjData = JSON.parse(data);
            console.log("LIST CODEPACKS: " + JSON.stringify(JSON.parse(data)));
            if (data.length > 0) {
                createList(ObjData);
            } else {
                noData();
            }
        }
    });
}
function createList(dataDetails) {

    var contenido = "";
    var arryColumnsDetail = Object.keys(dataDetails[0]);
    //console.log("week 1: " + week1 + "/ week2: " + week2 + "/ checkd:" + chekd);
    //contenido += "<tr style='display: none;'><td colspan=12>";

    contenido += "<table  id='TableModal' class='table table-hover table-bordered table-bordered tblSalesRequest' >";
    contenido += "<thead class='thead-agrocom'>";


    contenido += "<tr>";

    for (var z = 0; z < arryColumnsDetail.length; z++) {
        contenido += "<th>";
        contenido += arryColumnsDetail[z].toUpperCase();
        contenido += "</th>";
    }
    contenido += "<th> EDIT </th>"
    //contenido += "<th>VIEW</th>";
    contenido += "</tr>";
    contenido += "</thead>";
    contenido += "<tbody id='bodyTable'>";
    var llavesDet = Object.keys(dataDetails[0]);
    for (var i = 0; i < dataDetails.length; i++) {
        //contenido += "<tr>";
        contenido += "<tr>";
        for (var j = 0; j < llavesDet.length; j++) {
            //if (dataDetails[i][llavesDet[10]] == '1') {
            //var valorLLavesDet = llavesDet[j];
            if (j > 2) {
                if (j == 3) {
                    if (dataDetails[i][llavesDet[j]] == '1') {
                        contenido += "<td align ='center'>";
                        contenido += "Enabled";
                        contenido += "</td>";
                    } else {
                        contenido += "<td align ='center'>";
                        contenido += "Disabled";
                        contenido += "</td>";
                    }
                } else {
                    contenido += "<td align ='right'>";
                    contenido += dataDetails[i][llavesDet[j]];
                    contenido += "</td>";
                }

            } else {
                contenido += "<td>";
                contenido += dataDetails[i][llavesDet[j]];
                contenido += "</td>";
            }

            //}
        }


        contenido += "<td align ='center'>";
        contenido += "<button class='btnGo' id='btnEdit' style='width:90%' type='button' value='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal' data-backdrop='static' data-keyboard='false'>Edit</button>";

        contenido += "</td>";
    }

    contenido += "</tbody>";
    contenido += "</table>";
    contenido += "</tr>";
    document.getElementById("listTable").innerHTML = contenido;

}
function noData() {
    var contenido = "";
    console.log(" NO  HAY DATOS ");
    contenido += "<div>";
    contenido += "    <table class='table table - striped jambo_table bulk_action' cellspacing='0' rules='all' border='1' id='ContentPlaceHolder1_datos' style='border - collapse: collapse; '>";
    contenido += "        <tbody><tr class='headings' align='center' valign='middle' style='color:White;background-color:#405467;font-weight:bold;' >";
    contenido += "            <th scope='col'>MESSAGE</th>";
    contenido += "        </tr><tr class='even pointer' align='center'>";
    contenido += "<td>Without results.</td>";
    contenido += "            </tr>";
    contenido += "        </tbody></table>";
    contenido += "</div>";
    document.getElementById("tabla").innerHTML = contenido;
}
$(document).ready(function () {
    $('#cboCrop').change(function (e) {
        //var before_change = $(this).data('pre');//get the pre data
        //var current = $(this).val();
        //console.log("PREV: " + before_change + "CURRENT: " + current);
        ////Do your work here

        //var week2 = $('#cboSemanaFin').val();
        //if (week2 < current) {
        //    alert("Check week ranges1!");
        //    $("#cboSemanaIni").val(before_change).change();
        //} else {
        //Lista();
        //}
        //////////////////////
        //$(this).data('pre', $(this).val());//update the pre data
    });
    $(document).on("click", "#btnSaveModal", function () {
        console.log("CLICKED SAVE: MARKETID: " + $("#marketID").val());

        saveMarketCodepack();
    });
    $(document).on("click", "#btnEdit", function () {
        console.log("CLICKED VIEW");
        var idMarket = $(this).val();
        loadModal(idMarket);
    });
});
function saveMarketCodepack() {

    var ischeked = $('#cb0').is(':checked');
    console.log("CHBX= IS CHECKD?: " + ischeked);
    var DataCP;
    var idMarket = $("#marketID").val();
    $.get("ListCodepackMarket/?idMarket=" + idMarket, function (data) {
        DataCP = JSON.parse(data);
        SaveCodePckmarket(DataCP);
        console.log("LENGTH : " + DataCP.length + "// DATACP: " + $("#cb16").val());
    });
}
function loadCodepacks(idMarket) {

    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListCodepackMarket/?idMarket=" + idMarket,
        //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
        success: function (data) {
            //ListModalUpload(data, poID);
            var ObjData = JSON.parse(data);
            var contenido1 = "";
            var contenido2 = "";
            var contenido3 = "";

            console.log("LISTCODEPACKS: " + JSON.stringify(ObjData) + " // DATALENGHT : " + ObjData.length);
            for (var i = 0; i < ObjData.length; i++) {
                if (ObjData[i].via == 1) {
                    if (ObjData[i].marketID != 0) {
                        contenido1 += "<input id='cb" + i + "' value='" + ObjData[i].codePackID + "' type='checkbox' checked='checked' />";
                        contenido1 += "<label >" + ObjData[i].description + "</label>";
                        contenido1 += "<br/>";
                    } else {
                        contenido1 += "<input id='cb" + i + "' value='" + ObjData[i].codePackID + "' type='checkbox' />";
                        contenido1 += "<label >" + ObjData[i].description + "</label>";
                        contenido1 += "<br/>";
                    }

                }
                if (ObjData[i].via == 2) {
                    if (ObjData[i].marketID != 0) {
                        contenido2 += "<input id='cb" + i + "' value='" + ObjData[i].codePackID + "' type='checkbox' checked='checked'/>";
                        contenido2 += "<label >" + ObjData[i].description + "</label>";
                        contenido2 += "<br/>";
                    } else {
                        contenido2 += "<input id='cb" + i + "' value='" + ObjData[i].codePackID + "' type='checkbox' />";
                        contenido2 += "<label >" + ObjData[i].description + "</label>";
                        contenido2 += "<br/>";
                    }
                }
                if (ObjData[i].via == 3) {
                    if (ObjData[i].marketID != 0) {
                        contenido3 += "<input id='cb" + i + "' value='" + ObjData[i].codePackID + "' type='checkbox' checked='checked'/>";
                        contenido3 += "<label >" + ObjData[i].description + "</label>";
                        contenido3 += "<br/>";
                    } else {
                        contenido3 += "<input id='cb" + i + "' value='" + ObjData[i].codePackID + "' type='checkbox' />";
                        contenido3 += "<label >" + ObjData[i].description + "</label>";
                        contenido3 += "<br/>";
                    }
                }
            }
            document.getElementById("sea").innerHTML = contenido1;
            document.getElementById("air").innerHTML = contenido2;
            document.getElementById("land").innerHTML = contenido3;
        }

    });
}
function loadModal(idMarket) {

    console.log("CLICKED UPLOAD1111111111");
    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListMarketById/?idMarket=" + idMarket,
        //url: "Maintenance/ListCodePack/?idCboVia=" + idCboVia + "&idCboCrop=" + idCboCrop,
        success: function (data) {
            //ListModalUpload(data, poID);
            var ObjData = JSON.parse(data);
            var obj = ObjData[0];
            console.log("LIST MARKET: " + JSON.stringify(ObjData));
            console.log(" // ObjData.name: " + obj.name);
            $("#marketID").val(obj.marketID);
            $("#NameMarket").val(obj.name);
            $("#Abbreviation").val(obj.abbreviation);

            if (JSON.stringify(obj.statusID) == '1') {
                $("#MstatusID").val(obj.statusID);
                $("#MstatusID").val(obj.statusID).change();
            } else {
                $("#MstatusID").val(obj.statusID);
                $("#MstatusID").val(obj.statusID).change();
            }
            //$("#statusID").val(ObjData.name);

            loadCodepacks(idMarket);
        }
    });
    //ListTableModal(POID);
    //POModalShipmentST(POID);
}
function SaveCodePckmarket(data) {
    for (var i = 0; i < data.length; i++) {
        var chb = "#cb" + i;
        var frm = new FormData();
        frm.append("marketID", $("#marketID").val());
        frm.append("codepackID", $(chb).val());
        if ($(chb).is(':checked')) {

            console.log("checkb: " + chb + "is checked");

            $.ajax({
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "CreateMarketCodepack",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",
                Accept: "application/json",
                dataType: 'json',
                success: function (data) {
                    console.log("MARKETCODEPACKS GUARDADOS: ");
                }
            });

        } else {
            console.log("checkb: " + chb + "is not checked");
            $.ajax({
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: "DisableMarketCodepack",
                data: JSON.stringify(Object.fromEntries(frm)),
                contentType: "application/json",
                Accept: "application/json",
                dataType: 'json',
                success: function (data) {
                    console.log("MARKETCODEPACKS DISABLEDS: ");
                }
            });
        }
    }
}

