﻿var cropID = 'UVA'
var _userID = $("#lblSessionUserID").text();
var _orderProductionID = 0;
var campaignID = (localStorage.campaignID);
var dataCustomer = []
var dataDestination = []
var dataStatus = []
var dataAllWeek = []
var statusID = 0
var customerID = 0
var destinationID = 0
var weekID = 0


$(function () {
    //$(document).ajaxStart(function () {
    //    $("#wait").css("display", "block");
    //    $("#loader").css("display", "block")

    //    $(".transact").attr("disabled", true);
    //});
    //$(document).ajaxComplete(function () {
    //    $("#wait").css("display", "none");
    //    $("#loader").css("display", "none")

    //    $(".transact").attr("disabled", false);
    //});

    loadData();
    //loadDefaultValues();
  
    $(document).on("click", "#btnSearch", function () {
        var statusID = [];
        $('#selectStatus :selected').each(function (i, selected) {
            statusID[i] = $(selected).val();
        });
        var customerID = $('#cboCustomer').val();
        var destinationID = $('#cboDestination').val();
        var weekID = $('#cboWeek').val();
        var statusID = $('#selectStatus').val();

        list(customerID, weekID, destinationID, statusID, localStorage.campaignID);
    })

    $(document).on("click", "#btnWith", function () {
        $("#selectStatus").selectpicker('deselectAll');
        $('#selectStatus').selectpicker('refresh');

        var With = dataStatus.filter(item => item.id == 2)
        $('#selectStatus').val(With[0].id).change();

        var customerID = $('#cboCustomer').val();
        var destinationID = $('#cboDestination').val();
        var weekID = $('#cboWeek').val();
        var statusID = $('#selectStatus').val();

        list(customerID, weekID, destinationID, statusID, localStorage.campaignID);

    })

    $(document).on("click", "#btnWithOut", function () {
        $("#selectStatus").selectpicker('deselectAll');
        $('#selectStatus').selectpicker('refresh');

        var WithOut = dataStatus.filter(item => item.id == 1)
        $('#selectStatus').val(WithOut[0].id).change();

        var customerID = $('#cboCustomer').val();
        var destinationID = $('#cboDestination').val();
        var weekID = $('#cboWeek').val();
        var statusID = $('#selectStatus').val();

        list(customerID, weekID, destinationID, statusID, localStorage.campaignID);
    })

    //$(document).on("click", "#btnTotalPO", function () {
    //    $("#selectStatus").selectpicker('deselectAll');
    //    $('#selectStatus').selectpicker('refresh');

    //    var Total = dataStatus.filter(item => item.id == 5 || item.id == 9)
    //    $('#selectStatus').val(Total[0].id).change();

    //    var customerID = $('#cboCustomer').val();
    //    var destinationID = $('#cboDestination').val();
    //    var weekID = $('#cboWeek').val();
    //    var statusID = $('#selectStatus').val();

    //    list(customerID, weekID, destinationID, statusID, localStorage.campaignID);
    //})

    $(document).on("click", "#btnChangeStatus",function(){
        var tr = $(this).closest('tr')
        changeStatus(tr);
    })   
    
})

function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadDefaultValues() {
    //filtros
    loadComboFilter(dataCustomer, 'cboCustomer', true, '[ALL]');
    $('#cboCustomer').selectpicker('refresh');

    loadComboFilter(dataDestination, 'cboDestination', true, '[ALL]');
    $('#cboDestination').selectpicker('refresh');

    loadComboFilter(dataStatus, 'selectStatus', true, '[ALL]');
    //$('#selectStatus').val(dataStatus[0].id).change();
    $('#selectStatus').selectpicker('refresh');

    loadComboFilter(dataAllWeek, 'cboWeek', true, '[ALL]');
    //$('#cboWeek').val(dataAllWeek[0].id).change();
    $('#cboWeek').selectpicker('refresh');    
    
    var customerID = $('#cboCustomer').val();
    var destinationID = $('#cboDestination').val();
    var weekID = $('#cboWeek').val();
    $('#selectStatus').val('All').change();
    $("#selectStatus").selectpicker('refresh');
    var statusID = $('#selectStatus').val();

    list(customerID, weekID, destinationID, statusID, localStorage.campaignID);
    
}

function loadQuantity() {

    var WithOut = dataStatus.filter(item => item.id == 1)
    //document.getElementById("WithOut").innerHTML = WithOut[0].quantity;
    $(WithOut).text(WithOut[0].quantity);

    var With = dataStatus.filter(item => item.id == 2)
    //document.getElementById("With").innerHTML = With[0].quantity;
    $(With).text(With[0].quantity);

}

function loadOtherData() {
    $.ajax({
        type: "GET",
        url: "/PO/GetDestinationByCampaign?campaignID=" + localStorage.campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataDestination = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationID,
                            "name": obj.destination
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/ListWeekBySeason?idSeason=" + localStorage.campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataAllWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/PO/GetBoardingStatus?campaignID=" + localStorage.campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {
                        return {
                            "id": obj.statusID,
                            "name": obj.status,
                            "quantity": obj.quantity
                        }
                    }
                );
            }
        }
    });
}

function loadData() {
    
    $.ajax({
        type: "GET",
        url: "/PO/GetByCampaign/?campaignID=" + localStorage.campaignID,
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataCustomer = dataJson.map(
                    obj => {
                        return {
                            "id": obj.customerID,
                            "name": obj.customer
                        }
                    }
                );
            }
            loadOtherData();
            loadDefaultValues();

        }
    });

}

function changeStatus(tr) {
    var orderProductionID = $(tr).find('td.orderProductionID').text();

    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/PO/GetUpdStatus?orderProductionID=" + orderProductionID,
        //async: false,
        success: function (data) {
            //var datajson = Json.Parse(data);
            var datajson = data;

            $(tr).find('td.status').text('With IE');
            $(tr).find('td.btnActions').text('')
            //if (datajson.lenght > 0) {
            //}
            list()
        }
    });
}

function list() {
    var customerID = $('#cboCustomer').val();
    var destinationID = $('#cboDestination').val();
    var weekID = $('#cboWeek').val();
    var statusID = $('#selectStatus').val();
    var campaignID = localStorage.campaignID;

    if (statusID == 'All') {
        statusID = 0;
    }
    if (customerID == 'All') {
        customerID = 0;
    }
    if (destinationID == 'All') {
        destinationID = 0;
    }
    if (weekID == 'All') {
        weekID = 0;
    }
    $.ajax({
        type: "GET",
        url: "/PO/GetBoardingProgramming/?customerID=" + customerID + "&weekID=" + weekID + "&destinationID=" + destinationID + "&statusID=" + statusID + "&campaignID=" + campaignID,
        //async: false,
        success: function (data) {
            var dataList = JSON.parse(data);
            if (data.length > 0) {
                listaProgramming(dataList);
                loadQuantityByStatus(customerID, weekID, destinationID, campaignID);                
            }

        }
    });

}

function listaProgramming(data) {
    $('#boarding').DataTable().clear().destroy()
    $("#boarding>tbody").empty()
    var color = '';
    $.each(data, function (i, e) {
        var tr = "<tr style='font-size: 14px'>";
        tr += "<td class='orderProductionID' style='display:none'>" + e.orderProductionID + "</td>"
        tr += "<td class='orderProduction' style='max-width: 100px; min-width:100px'>" + e.orderProduction + "</td>"
        tr += "<td class='dateCompleted' style='max-width: 100px; min-width:100px'>" + e.dateCompleted + "</td>"
        tr += "<td class='packing' style='max-width: 100px; min-width:100px'>" + e.packing + "</td>"
        tr += "<td class='vgm' style='max-width: 50px; min-width:50px'>" + e.vgm + "</td>"
        tr += "<td class='loadingDate' style='max-width: 100px; min-width:100px'>" + e.loadingDate + "</td>"
        tr += "<td class='etd' style='max-width: 100px; min-width:100px'>" + e.etd + "</td>"
        tr += "<td class='eta' style='max-width: 100px; min-width:100px'>" + e.eta + "</td>"
        tr += "<td class='departureWeek' style='max-width: 50px; min-width:50px'>" + e.departureWeek + "</td>"
        tr += "<td class='status' style='max-width: 100px; min-width:100px'>" + e.status + "</td>"
        tr += "<td class='customer' style='max-width: 200px; min-width:200px'>" + e.customer + "</td>"
        tr += "<td class='destination' style='max-width: 100px; min-width:100px'>" + e.destination + "</div></td>"
        tr += '<td style="text-align: center;"><button class="btn btn - danger btn - sm" onclick="generateFilePdfGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf text-danger fa-md"></i></button></td>';
        tr += '<td style="text-align: center;"><button class="btn btn - danger btn - sm" onclick="generateFileSIGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf text-danger fa-md"></i></button></td>';
        tr += "<td class='ie' style='max-width: 150px; min-width:150px'>" + e.ie + "</td>"
        if (e.statusID == "2") {
            tr += "<td></td>"
        } else {
            tr += "<td class='btnActions' style='text-align:center;  min-width: 100px !important;'>";
            tr += "<button class='btn btn-primary btn-sm' id='btnChangeStatus' title='Update'>Update<i class='fas fa-arrow-circle' ></i></button>"
            tr += "</td>"
        }
        tr += "</tr>"
        $("#boarding>tbody").append(tr);
    })

    $("#boarding").dataTable({
        dom: 'Bfrtip',
      buttons: [
        { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
        'pageLength'
      ],
        lengthMenu: [10, 25, 50, 75, 100],
        //buttons: [
        //    'copy', 'csv', 'excel', 'pdf', 'print'
        //]
    });;

}

function loadQuantityByStatus(customerID, weekID, destinationID, campaignID) {
    $.ajax({
        type: "GET",
        url: "/PO/GetStatusQuan?customerID=" + customerID + "&weekID=" + weekID + "&destinationID=" + destinationID + "&campaignID=" + campaignID,
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {
                        return {
                            "id": obj.statusID,
                            "name": obj.status,
                            "quantity": obj.quantity
                        }
                    }
                );

                var x1 = dataStatus.filter(item => item.id == 1)[0].quantity;
                var x2 = dataStatus.filter(item => item.id == 2)[0].quantity;
                var total = x1 + x2;

                document.getElementById("WithOut").innerHTML = x1;

                document.getElementById("With").innerHTML = x2;                
                
                document.getElementById("Total").innerHTML = total;

            }
            loadQuantity();
        }
    });
}

