﻿var sessionUserID = $('#lblSessionUserID').text();
var _cropID = localStorage.cropID;
var _dataCodepack = []
var dataAllWeek = []
var ActualyWeek = []
var dataSizes = []
var dataGrower = []
var dataPlant = []
var dataStatus = []
var dataVariety = [];
var dataCategory = [];
var dataFormat = [];
var dataTypeBox = [];
var dataPresentation = [];
var dataBrand = [];
var dataLabel = [];
var dataCodePack2 = [];
var ContBlur = 0;
var EndFirstLoading = 0;
var trRowEdit;
var trRowModify;
var iOrderProdId = 0;
var iNro = 0;
var dMonTotPallet = 0;
var dataSummary = [];
var sDepartureWeekSelected = "0";
var sDepartureWeekEnd = "0";
var _valVariety = '';

(function () {
    var dataCodepack = [];
    var dataTypePackage = [];

    var Handler = function () {
        //>>>Variables Globales
        var _idModalSO = null;
        var _objModalSO = null;
        var dataCustomerMarket = [];
        var dataDestinationCustomer = [];
        var dataWow = [];
        var dataVia = [];
        var dataBrandByCrop = [];
        var dataVarietyByCrop = [];
        var dataCodepackWithPresentation = [];
        var dataIncoterm = [];
        //<<<Variables Globales

        //>>>Definición de controles
        var ctrl = {
            _divSOwithoutProgram: $('#divSOwithoutProgram'),
            _divDetails: $('#divDetails'),
            _btnSOwithoutProgram: $('#btnSOwithoutProgram'),
            _lblTitleModalSOcropBlu: $('#lblTitleModalSOcropBlu'),
            _lblTitleModalSOcropUva_Cit: $('#lblTitleModalSOcropUva-Cit'),
            _selectMarket: $('#selectMarket'),
            _selectCustomer: $('#selectCustomer'),
            _selectDestination: $('#selectDestination'),
            _selectVia: $('#selectVia'),
            _selectConsignee: $('#selectConsignee'),
            _selectCategory_customize: $('#selectCategory_customize'),
            _selectVariety_customize: $('#selectVariety_customize'),
            _selectPackage_customize: $('#selectPackage_customize'),
            _selectPresentation_customize: $('#selectPresentation_customize'),
            _selectSize_customize: $('#selectSize_customize'),
            _selectCodepack_customize: $('#selectCodepack_customize'),
            _selectFinalCustomer_customize: $('#selectFinalCustomer_customize'),
            _txtPrice_customize: $('#txtPrice_customize'),
            _txtBoxes_customize: $('#txtBoxes_customize'),
            _btnAdd_customize: $('#btnAdd_customize'),
            _btnSaveSOwithoutProgram: $('#btnSaveSOwithoutProgram'),
            _btnEdit_customize: $('#btnEdit_customize'),
        };
        //<<<Definición de controles

        var _that = {
            Init: function () {
                if (localStorage.SOwithoutProgram == 'true') ctrl._divSOwithoutProgram.css('display', 'block');
                if (localStorage.SOwithoutProgram == 'false') ctrl._divSOwithoutProgram.css('display', 'none');
                this.Events();
            },
            Methods: {
                Init: function () {

                },
                LoadData: {
                    getMarket: function () {
                        opt = 'all';
                        id = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListMarket?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    let dataMarket = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.marketID,
                                                "name": obj.name
                                            }
                                        }
                                    );
                                    loadCombo(dataMarket, 'selectMarket', true);
                                    $('#selectMarket').prop("disabled", false);
                                    $('#selectMarket').selectpicker('refresh');
                                }
                            }
                        });
                    },
                    getCustomerMarket: function () {
                        opt = 'wmr';
                        id = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListAllWithMarket?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                dataCustomerMarket = JSON.parse(data);
                            }
                        });
                    },
                    getWow: function () {
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListWowByCustomer?customerID=" + ctrl._selectCustomer.val() + "&cropID=" + localStorage.cropID,
                            async: false,
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            success: function (data) {
                                if (data.length > 0) {
                                    dataWow = JSON.parse(data);
                                }
                            }
                        });
                    },
                    getWeek: function () {
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListWeekBySeason?idSeason=" + localStorage.campaignID,
                            async: false,
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    let dataWeek = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.projectedWeekID,
                                                "name": obj.description
                                            }
                                        }
                                    );
                                    loadCombo(dataWeek, 'selectWeek', true);
                                    $('#selectWeek').selectpicker('refresh');
                                }
                            }
                        });
                    },
                    getDestination: function () {
                        var opt = 'wcu';
                        var id = '';
                        var mar = '';
                        var via = '';
                        $.ajax({
                            type: "GET",
                            url: "/CommercialPlan/ListDestinationAllWithCustomer?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
                            async: false,
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            success: function (data) {
                                dataDestinationCustomer = JSON.parse(data);
                            }
                        });
                    },
                    getVia: function () {
                        opt = 'all';
                        id = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListVia?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    dataVia = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.viaID,
                                                "name": obj.name
                                            }
                                        }
                                    );
                                    loadCombo(dataVia, 'selectVia', true);
                                    $('#selectVia').selectpicker('refresh');
                                }
                            }
                        });
                    },
                    getBrand: function () {
                        var opt = 'cro';
                        var id = localStorage.cropID;
                        var growerID = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListBrandMC?opt=" + opt + "&id=" + id + "&growerID=" + growerID,
                            async: false,
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    dataBrandByCrop = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.categoryID,
                                                "name": obj.description
                                            }
                                        }
                                    );
                                    loadCombo(dataBrandByCrop, 'selectCategory_customize', true);
                                    $('#selectCategory_customize').selectpicker('refresh');
                                }
                            }
                        });

                    },
                    getVariety: function () {
                        var opt = 'cro';
                        var id = localStorage.cropID;
                        var log = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/VarietyGetByCropMC?opt=" + opt + "&id=" + id + "&log=" + log,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    dataVarietyByCrop = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.varietyID,
                                                "name": obj.name,
                                                "abbreviation": obj.abbreviation
                                            }
                                        }
                                    );
                                    loadCombo(dataVarietyByCrop, 'selectVariety_customize', false);
                                    $('#selectVariety_customize').selectpicker('refresh');
                                }
                            }
                        });


                    },
                    getCodepack: function () {
                        var opt = 'cro';
                        var id = localStorage.cropID;
                        $.ajax({
                            type: "GET",
                            url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                dataCodepack = JSON.parse(data);
                            }
                        });

                    },
                    getPresentation: function () {
                        opt = 'pre';
                        id = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/GetCodepackWithPresentation?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                if (data.length > 0) {
                                    var dataJson = JSON.parse(data);
                                    dataCodepackWithPresentation = dataJson
                                }
                            }
                        });
                    },
                    getTypBox: function () {
                        opt = "pkc";
                        id = "";
                        $.ajax({
                            type: "GET",
                            url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            success: function (data) {
                                if (data.length > 0) {
                                    var dataJson = JSON.parse(data);
                                    dataTypePackage = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.packageProductID,
                                                "name": obj.packageProduct,
                                                "abbreviation": obj.abbreviation
                                            }
                                        }
                                    );
                                    loadCombo(dataTypePackage, 'selectPackage_customize', true);
                                    $('#selectPackage_customize').selectpicker('refresh');
                                }
                            }
                        });

                    },
                    getSize: function () {
                        var opt = 'cro';
                        var id = localStorage.cropID;
                        $.ajax({
                            type: "GET",
                            url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0'
                            },
                            success: function (data) {
                                if (data.length > 0) {
                                    var dataJson = JSON.parse(data);
                                    let dataSizes = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.sizeID,
                                                "name": obj.code
                                            }
                                        }
                                    );
                                    loadCombo(dataSizes, 'selectSize_customize', false);
                                    $('#selectSize_customize').selectpicker('refresh');
                                }
                            }
                        });

                    },
                    getIncoterm: function () {
                        $.ajax({
                            type: "GET",
                            url: "/Sales/GetIncotermMC?opt=sal&cropID=" + localStorage.cropID + "&viaID=0",
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    dataIncoterm = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.incotermID,
                                                "name": obj.name,
                                                "viaID": obj.viaID
                                            }
                                        }
                                    );
                                }
                            }
                        });
                    },
                    getPriceCondition: function () {
                        opt = 'all';
                        id = '';
                        $.ajax({
                            type: "GET",
                            url: "/Sales/ListConditionPayment?opt=" + opt + "&id=" + id,
                            async: false,
                            headers: {
                                'Cache-Control': 'no-cache, no-store, must-revalidate',
                                'Pragma': 'no-cache',
                                'Expires': '0'
                            },
                            success: function (data) {
                                var dataJson = JSON.parse(data);
                                if (data.length > 0) {
                                    let dataConditionPayment = dataJson.map(
                                        obj => {
                                            return {
                                                "id": obj.conditionPaymentID,
                                                "name": obj.description
                                            }
                                        }
                                    );
                                    loadCombo(dataConditionPayment, 'selectPriceContition_customize', true);
                                    $('#selectPriceContition_customize').selectpicker('refresh');
                                }
                            }
                        });

                    },
                    getPaymentTerm: function () {
                        $.ajax({
                            type: 'POST',
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            url: "/Sales/ListPaymentTermByCustomer?customerID=" + ctrl._selectCustomer.val() + "&cropiD=" + localStorage.cropID,
                            async: true,
                            success: function (data) {
                                if (data.length > 0) {
                                    var dataList = JSON.parse(data);
                                    let dataPayment = dataList.map(
                                        obj => {
                                            return {
                                                "id": obj.paymentTermID,
                                                "name": obj.description
                                            }
                                        }
                                    );
                                    loadCombo(dataPayment, 'selectPayment_customize', true)
                                    $('#selectPayment_customize').selectpicker('refresh');
                                }
                            }
                        });
                    },
                    generarCodepack: function () {
                        let varietyID = '';
                        let arrayVarietyID = $('#selectVariety_customize').val();
                        $.each(arrayVarietyID, function (index, value) {
                            varietyID += value + ',';
                        });
                        if (varietyID.length == 0) {
                            $(txtCodePack).val('');
                            return false;
                        }

                        if ($("#selectCodepack_customize option:selected").val() == 0) {
                            return false;
                        }

                        if ($("#selectPackage_customize option:selected").val() == 0) {
                            return false;
                        }

                        if ($("#selectCategory_customize option:selected").val() == 0) {
                            return false;
                        }

                        if ($("#selectSize_customize option:selected").val() == 0) {
                            return false;
                        }

                        let arraySizeID = $('#selectSize_customize').val();
                        let codePackID;
                        let typeBoxID;
                        let categoryID;
                        let presentationID;
                        let varietyCode;
                        let formatCode;
                        let typeBoxCode;
                        let brandCode;
                        let presentationCode;
                        let codepack = '';
                        let codepackToShow = '';
                        $.each(arrayVarietyID, function (index, value) {
                            varietyID = value;
                            codePackID = $('#selectCodepack_customize').val();
                            typeBoxID = $('#selectPackage_customize').val();
                            categoryID = $('#selectCategory_customize').val();
                            presentationID = $('#selectPresentation_customize').val();

                            varietyCode = dataVarietyByCrop.filter(item => item.id == parseInt(varietyID))[0].abbreviation;
                            formatCode = dataCodepack.filter(item => item.codePackID == parseInt(codePackID))[0].weight;
                            typeBoxCode = dataTypePackage.filter(item => item.id == parseInt(typeBoxID))[0].abbreviation.trim();
                            brandCode = dataBrandByCrop.filter(item => item.id == categoryID)[0].name;
                            console.log(brandCode)
                            presentationCode = dataCodepackWithPresentation.filter(item => item.presentationID == presentationID)[0].presentation;

                            codepack = varietyCode.trim() + '_' +
                                formatCode +
                                typeBoxCode.slice(0, 1) +
                                brandCode.slice(0, 1) + '_' +
                                presentationCode;

                            codepackToShow = (codepack.trim() == '') ? codepackToShow : codepack + ',' + codepackToShow;

                        });
                        codepackToShow = codepackToShow.substring(0, codepackToShow.length - 1);
                        let arrayCodePack = codepackToShow.split(',');
                        let codepacksToStore = '';
                        codepackToShow = '';
                        $.each(arraySizeID, function (iRowSize, oSize) {
                            $.each(arrayCodePack, function (iRowCode, oCode) {
                                codepackToShow = (oCode + '_+' + oSize.substring(1, oSize.length) + '_' + 1) + '\n' + codepackToShow;
                                codepacksToStore = (oCode + '_+' + oSize.substring(1, oSize.length) + '_' + 1) + ',' + codepacksToStore;
                            });
                        });
                        $('#txtCodePack_customize').val(codepacksToStore);

                    },
                    addCatVar: function () {
                        var saleID = 0;
                        var categoryID = $("#selectCategory_customize").val();
                        var arrayCategory = []
                        $.each(dataBrandByCrop, function (i, e) {
                            if (e.id == categoryID) {
                                arrayCategory.push(e.name)
                            }
                        });
                        var arrayVarietyID = $("#selectVariety_customize").val();
                        var arrayVariety = [];
                        $.each(arrayVarietyID, function (ii, ee) {
                            $.each(dataVarietyByCrop, function (i, e) {
                                if (e.id == ee) {
                                    arrayVariety.push(e.name)
                                }
                            })
                        });
                        var codePackID = $("#selectCodepack_customize").val();
                        var arrayCodePack = [];
                        $.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
                            if (e.codePackID == codePackID) {
                                arrayCodePack.push(e.description)
                            }
                        });
                        var arraySizeID = $("#selectSize_customize").val();
                        var quantity = $("#txtBoxes_customize");
                        var quantityMinimun = $("#txtMinBoxes_customize");
                        var net = $("#txtNet_customize");
                        var price = $("#txtPrice_customize");
                        var packageProductID = $("#selectPackage_customize option:selected");
                        var conditionPaymentID = $("#selectPriceContition_customize option:selected");
                        var incotermID = $("#selectIncoterm_customize option:selected");
                        var paymentTerm = $('#selectPayment_customize').find("option:selected");
                        var xchkFlexible = 1;
                        var presentation = $("#selectPresentation_customize option:selected");
                        var finalcustomerID = $("#selectFinalCustomer_customize option:selected").val();
                        var finalcustomer = "";
                        if (finalcustomerID.length > 0) {
                            finalcustomer = $("#selectFinalCustomer_customize option:selected").text();
                        }
                        var pallets = $("#txtPallets_customize").val();
                        var totalPrice = $("#txtTotalPrice_customize").val();
                        var codepack2 = $("#txtCodePack_customize").val();

                        var content = '';
                        content += '<tr class="text-align-right">';
                        content += '<td style="text-align:center"> ';
                        content += '<button class="btn btn-primary btn-sm row-edit"><i class="fas fa-edit"></i></button>';
                        content += '<button class="btn btn-danger btn-sm  row-remove" ><i class="fas fa-trash"></i></button>';
                        content += '</td>';   //00
                        content += '<td style="display:none">' + saleID + '</td>'; //01
                        content += '<td style="display:none" class="tdcategoryID_02">' + categoryID.trim() + '</td>';  //02
                        content += '<td>' + arrayCategory + '</td>';  //03
                        content += '<td style="display:none" class="tdvarietyID_04">' + arrayVarietyID + '</td>';   //04
                        content += '<td>' + arrayVariety + '</td>';   //05
                        content += '<td class="tdsizeID_06">' + arraySizeID + '</td>';   //06
                        content += '<td style="display:none" class="tdcodePackID_07">' + codePackID.trim() + '</td>';  //07
                        content += '<td>' + arrayCodePack + '</td>';  //08
                        content += '<td style="display:none" class="tdpresentationID_09">' + $(presentation).val().trim() + '</td>';   //09
                        content += '<td>' + $(presentation).text().trim() + '</td>';    //10
                        content += '<td class="tdquantity_11">' + $(quantity).val().trim() + '</td>';   //11
                        content += '<td style="display:none;max-width: 60px;min-width: 60px" class="tdBoxesMin_12">' + $(quantityMinimun).val().trim() + '</td>';  //12
                        content += '<td style="display:none" class="tdxchkFlexible_13">' + xchkFlexible + '</td>';   //13
                        content += '<td>' + $(net).val().trim() + '</td>';  //14
                        content += '<td class="tdprice_15">' + $(price).val().trim() + '</td>';    //15
                        content += '<td style="display:none" class="tdpackageProductID_16">' + $(packageProductID).val().trim() + '</td>'; //16
                        content += '<td style="display:none" class="tdincotermID_17">' + $(incotermID).val().trim() + '</td>'; //17
                        content += '<td>' + $(incotermID).text().trim() + '</td > '; //18
                        content += '<td style="display:none" class="tdconditionPaymentID_19">' + $(conditionPaymentID).val().trim() + '</td>'; //19
                        content += '<td>' + $(conditionPaymentID).text().trim() + '</td >';  //20
                        content += '<td class="tdpaymentTerm_21">' + $(paymentTerm).text().trim() + '</td>';   //21
                        content += '<td class="tdFinalCustomer_22">' + finalcustomer.trim() + '</td>';    //22
                        content += '<td class="tdPallet_23">' + pallets.trim() + '</td>'; //23
                        content += '<td class="tdPrice_24">' + totalPrice.trim() + '</td>';   //24
                        content += '<td style="display:none" class="tdFinalCustomerID_25">' + finalcustomerID.trim() + '</td>';   //25
                        content += '<td style="display:none" class="tdPaymentTermID_26">' + $(paymentTerm).val().trim() + '</td>';   //26
                        content += '<td style="display:none" class="tdcodePack2_27">' + codepack2.trim() + '</td>';  //27
                        content += '</tr>';
                        $("table#tblCatVar tbody").append(content);
                        _that.Methods.LoadData.calcTotal();
                        _that.Methods.LoadData.clearDetails();

                        ctrl._divDetails.find('details#objDetails').removeAttr('open');
                    },
                    calcTotal: function () {
                        var pallet = 0
                        var price = 0
                        $("#tblCatVar tbody tr td.tdPallet_23").each(function () {
                            pallet += parseFloat($(this).text());
                        });
                        $("#tblCatVar tbody tr td.tdPrice_24").each(function () {
                            price += parseFloat($(this).text());
                        });
                        $('#tblCatVar .totalPallets').html(pallet.toFixed(2));
                        $('#tblCatVar .totalPrice').html(price.toFixed(2));

                    },
                    clearDetails: function () {
                        $('#selectCategory_customize').selectpicker('val', '');
                        $('#selectVariety_customize').selectpicker('val', '0');
                        $('#selectCodepack_customize').selectpicker('val', '0');
                        $('#selectPresentation_customize').selectpicker('val', '0');
                        $('#selectPackage_customize').selectpicker('val', '0');
                        $('#selectSize_customize').selectpicker('val', '0');
                        $('#selectPayment_customize').selectpicker('val', '0');
                        $('#selectPriceContition_customize').selectpicker('val', '0');
                        $('#selectIncoterm_customize').selectpicker('val', '0');
                        //$('#selectFinalCustomer_customize').selectpicker('val', '0');
                        $('#txtBoxPallet_customize').val(0);
                        $('#txtBoxContainer_customize').val(0);
                        $('#txtBoxes_customize').val(0);
                        $('#txtPrice_customize').val(0);
                        $('#txtMinBoxes_customize').val(0);
                        $('#txtNet_customize').val(0);
                        $('#txtPallets_customize').val(0);
                        $('#txtCodePack_customize').val('');

                    },
                    saveSOwithoutProgram: function () {
                        if (!_that.Methods.LoadData.obligatory()) {
                            return
                        }
                        if ($('#tblCatVar>tbody>tr').length == 0) {
                            return;
                        }

                        var saleID = 0;
                        var destinationID = $("#selectDestination option:selected").val()
                        var customerID = $("#selectCustomer option:selected").val()
                        var projectedWeekID = $("#selectWeek option:selected").val()
                        var wowID = $("#selectConsignee option:selected").val()
                        if (wowID === '') { wowID = '0' }

                        var details = [];
                        $('#tblCatVar>tbody>tr').each(function (i, e) {
                            var saleDetailID = $(e).find('td:eq(1)').text();
                            var categoryID = $(e).find('td:eq(2)').text();
                            var varietyID = $(e).find('td:eq(4)').text();
                            var sizeID = $(e).find('td:eq(6)').text();
                            var codePackID = $(e).find('td:eq(7)').text();
                            var presentationID = $(e).find('td:eq(9)').text();
                            var quantity = $(e).find('td:eq(11)').text();
                            var quantityMinimun = $(e).find('td:eq(12)').text();
                            var flexible = $(e).find('td:eq(13)').text();
                            var price = $(e).find('td:eq(15)').text();
                            var packageProductID = $(e).find('td:eq(16)').text();
                            var incotermID = $(e).find('td:eq(17)').text();
                            var conditionPaymentID = $(e).find('td:eq(19)').text();
                            var paymentTerm = $(e).find('td:eq(21)').text();
                            var finalCustomerID = $(e).find('td.tdFinalCustomerID_25').text();
                            var paymentTermID = $(e).find('td.tdPaymentTermID_26').text();
                            var codePack2 = $(e).find('td.tdcodePack2_27').text();
                            if (finalCustomerID == "") finalCustomerID = 0

                            var detail = {
                                "quantity": quantity, "conditionPaymentID": conditionPaymentID, "incotermID": incotermID, "codePackID": codePackID, "saleDetailID": saleDetailID,
                                "presentationID": presentationID, "categoryID": categoryID, "varieties": varietyID, "paymentTerm": paymentTerm, "sizes": sizeID, "packageProductID": packageProductID,
                                "finalCustomerID": finalCustomerID, "price": price, "paymentTermID": paymentTermID, "quantityMinimun": quantityMinimun, "flexible": flexible, "codePack": codePack2,
                                "workOrder": 1
                            }
                            details.push(detail);

                        })
                        var userID = $("#lblSessionUserID").text();
                        var o = {};
                        o.saleID = saleID;
                        o.destinationID = destinationID;
                        o.projectedWeekID = projectedWeekID;
                        o.customerID = customerID;
                        o.userCreated = userID;
                        o.wowID = wowID;
                        o.commentary = '';
                        o.statusID = $('#txtPriority').val();
                        o.campaignID = localStorage.campaignID;
                        o.details = details;

                        var api = "/Sales/SaveSOwithoutProgram";
                        $.ajax({
                            type: 'POST',
                            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                            url: api,
                            contentType: "application/json",
                            dataType: 'json',
                            data: JSON.stringify(o),
                            success: function (dataResponse) {
                                sweet_alert_success_prg_copytoclipboard('Good Job!', 'Sale order created:', dataResponse[0].requestNumber);
                                _objModalSO.modal('hide');
                            }
                        });
                    },
                    obligatory: function () {
                        $('#spanObligatory').text('');
                        var response = false;

                        var market = $("#selectMarket option:selected").val()
                        if (market == "") {
                            $('#spanObligatory').text('choose Market');
                            return response;
                        }

                        var customer = $("#selectCustomer option:selected").val()
                        if (customer == "") {
                            $('#spanObligatory').text('choose Customer');
                            return response;
                        }

                        var customer = $("#selectWeek option:selected").val()
                        if (customer == "") {
                            $('#spanObligatory').text('choose Week');
                            return response;
                        }

                        var destination = $("#selectDestination option:selected").val()
                        if (destination == "") {
                            $('#spanObligatory').text('choose Destination');
                            return response;
                        }

                        var via = $("#selectVia option:selected").val()
                        if (via == "") {
                            $('#spanObligatory').text('choose Via');
                            return response;
                        }

                        if ($("#selectVia option:selected").val() == 1) {
                            var pallets = $('.totalPallets').text()
                            if (pallets < 20) {
                                $('#spanObligatory').text('you must complete 20 pallets');
                                return response;
                            } else if (pallets > 20) {
                                $('#spanObligatory').text('you must complete only 20 pallets');
                                return response;
                            }
                        }

                        var priority = $("#txtPriority").val()
                        if (priority = 0) {
                            $('#spanObligatory').text('Enter Priority');
                            return response;
                        }

                        $('#spanObligatory').text('');
                        response = true;
                        return response;
                    }
                }
            },
            Events: function () {
                ctrl._btnSOwithoutProgram.click(function () {
                    Swal.fire({
                        title: 'Loading...',
                        onOpen() {
                            switch (localStorage.cropID.toLowerCase()) {
                                case CropSelected.uva:
                                case CropSelected.cit:
                                    ctrl._lblTitleModalSOcropUva_Cit.html(ctrl._btnSOwithoutProgram.text() + ' - ' + localStorage.cropID)
                                    _idModalSO = 'modalSOcropUva-Cit';
                                    break;
                                case CropSelected.blu:
                                    _that.Methods.LoadData.getCodepack();
                                    _that.Methods.LoadData.getIncoterm();
                                    _that.Methods.LoadData.getMarket();
                                    _that.Methods.LoadData.getCustomerMarket();
                                    _that.Methods.LoadData.getWeek();
                                    _that.Methods.LoadData.getDestination();
                                    _that.Methods.LoadData.getBrand();
                                    _that.Methods.LoadData.getVariety();
                                    _that.Methods.LoadData.getPresentation();
                                    _that.Methods.LoadData.getTypBox();
                                    _that.Methods.LoadData.getSize();
                                    _that.Methods.LoadData.getPriceCondition();

                                    //_that.Methods.LoadData.clearDetails();
                                    ctrl._divDetails.find('details#objDetails').attr('open', '');
                                    ctrl._lblTitleModalSOcropBlu.html(ctrl._btnSOwithoutProgram.text() + ' - ' + localStorage.cropID)
                                    _idModalSO = 'modalSOcropBlu';
                                    break;
                                default:
                                    break;
                            }
                            _objModalSO = $('#' + _idModalSO);
                            AvoidCloseModalByAnyKey(_idModalSO);
                            _objModalSO.modal('show');
                            Swal.close();
                        }

                    })
                });
                ctrl._selectMarket.change(function () {
                    let dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == this.value.trim());
                    let dataCustomer = dataCustomerFilter.map(
                        obj => {
                            return {
                                "id": obj.customerID,
                                "name": obj.customer
                            }
                        }
                    );
                    loadCombo(dataCustomer, 'selectCustomer', true);
                    $('#selectCustomer').selectpicker('refresh');
                    loadCombo(dataCustomer, 'selectFinalCustomer_customize', true);
                    $('#selectFinalCustomer_customize').selectpicker('refresh');

                    let customerToSelect = localStorage.CustomerToSelectSOwithoutProgram;
                    if (customerToSelect != '') {
                        let customerIdSelect = dataCustomer.filter(item => item.name == localStorage.CustomerToSelectSOwithoutProgram)[0].id;
                        ctrl._selectCustomer.val(customerIdSelect).change();
                        ctrl._selectCustomer.prop("disabled", true);

                        ctrl._selectFinalCustomer_customize.val(customerIdSelect).change();
                        ctrl._selectFinalCustomer_customize.prop("disabled", true);
                    }

                    _that.Methods.LoadData.getVia();
                });
                ctrl._selectCustomer.change(function () {
                    let dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val());
                    let dataDestination = dataDestinationFilter.map(
                        obj => {
                            return {
                                "id": obj.destinationID,
                                "name": obj.destination
                            }
                        }
                    );
                    loadCombo(dataDestination, 'selectDestination', true);
                    $('#selectDestination').selectpicker('refresh');

                    _that.Methods.LoadData.getWow();
                    _that.Methods.LoadData.getPaymentTerm();
                });
                ctrl._selectDestination.change(function () {
                    let dataWowFilter = dataWow.filter(item => item.destinationID == this.value);
                    let _dataWow = dataWowFilter.map(
                        obj => {
                            return {
                                "id": obj.wowID,
                                "name": obj.consignee
                            }
                        }
                    );
                    loadComboConsignnee(_dataWow, 'selectConsignee', true);
                    $('#selectConsignee').selectpicker('refresh');

                    let consigneeToSelect = localStorage.ConsigneeToSelectSOwithoutProgram;
                    if (consigneeToSelect != '') {
                        let consigneeIdSelect;
                        $.each(_dataWow, function (row, elem) {
                            if (elem.name.indexOf(localStorage.ConsigneeToSelectSOwithoutProgram) > -1) {
                                consigneeIdSelect = elem.id;
                                return;
                            }
                        });
                        ctrl._selectConsignee.val(consigneeIdSelect).change();
                        ctrl._selectConsignee.prop("disabled", true);
                    }
                });
                ctrl._selectVia.change(function () {
                    let dataCodepackFilter = dataCodepack.filter(item => item.viaID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val()).map(
                        obj => {
                            return {
                                "id": obj.codePackID,
                                "name": obj.description
                            }
                        }
                    );
                    loadCombo(dataCodepackFilter, 'selectCodepack_customize', true);
                    $('#selectCodepack_customize').change().selectpicker('refresh');

                    let dataIncotermFilter = dataIncoterm.filter(item => item.viaID == $("#selectVia option:selected").val())
                    loadCombo(dataIncotermFilter, 'selectIncoterm_customize', true);
                    $('#selectIncoterm_customize').selectpicker('refresh');

                });
                ctrl._selectCategory_customize.change(function () {
                    _that.Methods.LoadData.generarCodepack();
                });
                ctrl._selectVariety_customize.change(function () {
                    _that.Methods.LoadData.generarCodepack();
                });
                ctrl._selectPackage_customize.change(function () {
                    _that.Methods.LoadData.generarCodepack();
                });
                ctrl._selectPresentation_customize.change(function () {
                    _that.Methods.LoadData.generarCodepack();
                });
                ctrl._selectSize_customize.change(function () {
                    _that.Methods.LoadData.generarCodepack();
                });
                ctrl._selectCodepack_customize.change(function () {
                    $(txtBoxPallet_customize).val(0);
                    $(txtBoxContainer_customize).val(0);
                    $(txtBoxes_customize).val(0);
                    $(txtNet_customize).val(0);
                    $(txtPallets_customize).val(0);
                    $(txtTotalPrice_customize).val(0);
                    $(txtBoxes_customize).prop("disabled", true);;
                    if (ctrl._selectCodepack_customize.val() > 0) {
                        let boxPalletVal = dataCodepack.filter(element => element.codePackID == ctrl._selectCodepack_customize.val())[0].boxesPerPallet
                        let boxContainerVal = dataCodepack.filter(element => element.codePackID == ctrl._selectCodepack_customize.val())[0].boxPerContainer
                        $(txtBoxPallet_customize).val(boxPalletVal);
                        $(txtBoxContainer_customize).val(boxContainerVal);
                        $(txtBoxes_customize).attr("step", boxPalletVal);
                        $(txtMinBoxes_customize).attr("step", boxPalletVal);
                        $(txtBoxes_customize).prop("disabled", false);;
                    }

                    let dataPresentationFilter = dataCodepackWithPresentation.filter(item => item.codePackID == ctrl._selectCodepack_customize.val()).map(
                        obj => {
                            return {
                                "id": obj.presentationID,
                                "name": obj.presentation + obj.moreInfo
                            }
                        });
                    loadCombo(dataPresentationFilter, 'selectPresentation_customize', false);
                    $('#selectPresentation_customize').change().selectpicker('refresh');

                    _that.Methods.LoadData.generarCodepack();
                });
                ctrl._txtPrice_customize.change(function () {
                    ctrl._txtBoxes_customize.change();
                })
                ctrl._txtBoxes_customize.change(function () {
                    var currentBoxes = $(this).val();

                    var codePackID = $("#selectCodepack_customize").val();
                    var codePack;
                    $.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
                        if (e.codePackID == codePackID) {
                            codePack = e
                        }
                    })

                    var weight
                    if (codePack != undefined) weight = parseInt(currentBoxes) * parseFloat(codePack.weight);
                    else weight = 0;

                    $('#txtNet_customize').val(weight.toFixed(2))
                    var boxPallet = parseInt($('#txtBoxPallet_customize').val());
                    var pallets = parseInt(currentBoxes) / boxPallet;

                    $('#txtPallets_customize').val(pallets.toFixed(2))

                    var currentPrice = parseFloat($('#txtPrice_customize').val());
                    var totalPrice = (currentPrice * currentBoxes).toFixed(2);

                    $('#txtTotalPrice_customize').val(totalPrice);
                });
                ctrl._btnAdd_customize.click(function () {
                    _that.Methods.LoadData.addCatVar();
                });
                ctrl._btnSaveSOwithoutProgram.click(function () {
                    _that.Methods.LoadData.saveSOwithoutProgram();
                });
                ctrl._btnEdit_customize.click(function () {
                    //if (!obligatoryRow()) {
                    //    return;
                    //}

                    $('#btnEdit_customize').addClass('display-none')
                    $('#btnAdd_customize').removeClass('display-none')

                    $("#tblCatVar>tbody>tr").each(function (index, tr) {
                        $(tr).removeClass("row-edit-sales")
                    });
                    var categoryID = $("#selectCategory_customize").val();
                    var category = []
                    $.each(dataBrandByCrop, function (i, e) {
                        if (e.id == categoryID) {
                            category.push(e.name)
                        }
                    });

                    var varietyID = $("#selectVariety_customize").val();
                    var variety = [];
                    $.each(varietyID, function (ii, ee) {
                        $.each(dataVarietyByCrop, function (i, e) {
                            if (e.id == ee) {
                                variety.push(e.name)
                            }
                        });
                    });

                    var codePackID = $("#selectCodepack_customize").val();
                    var codePack = [];

                    $.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
                        if (e.codePackID == codePackID) {
                            codePack.push(e.description)
                        }
                    })

                    var sizeID = $("#selectSize_customize").val();
                    var packageProductID = $("#selectPackage_customize").val();
                    var quantity = $("#txtBoxes_customize");
                    var quantityMinimum = $("#txtMinBoxes_customize");
                    var xchkFlexible = 1
                    var net = $("#txtNet_customize");
                    var price = $("#txtPrice_customize");
                    var conditionPaymentID = $("#selectPriceContition_customize option:selected");
                    var incotermID = $("#selectIncoterm_customize option:selected");
                    var paymentTerm = $("#selectPayment_customize option:selected");
                    var presentation = $("#selectPresentation_customize option:selected")
                    var pallets = $("#txtPallets_customize").val();
                    var totalPrice = $("#txtTotalPrice_customize").val();
                    var codePack2 = $("#txtCodePack_customize").val();
                    var finalcustomerID = $("#selectFinalCustomer_customize option:selected").val()
                    var finalcustomer = ""
                    if (finalcustomerID.length > 0) {
                        finalcustomer = $("#selectFinalCustomer_customize option:selected").text()
                    }

                    var row = $(txtRow_customize).val();
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(2).text(categoryID);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(3).text(category);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(4).text(varietyID);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(5).text(variety);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(6).text(sizeID);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(7).text(codePackID);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(8).text(codePack);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(9).text($(presentation).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(10).text($(presentation).text());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(11).text($(quantity).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(12).text($(quantityMinimum).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(13).text(xchkFlexible);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(14).text($(net).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(15).text($(price).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(16).text($(packageProductID).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(17).text($(incotermID).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(18).text($(incotermID).text());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(19).text($(conditionPaymentID).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(20).text($(conditionPaymentID).text());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(21).text($(paymentTerm).text());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(23).text(pallets);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(24).text(totalPrice);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td.tdFinalCustomer').text(finalcustomer);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td.tdFinalCustomerID').text(finalcustomerID);
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td.tdPaymentTermID').text($(paymentTerm).val());
                    $("#tblCatVar>tbody").find('tr').eq(row).find('td.tdcodePack2').text(codePack2);

                    _that.Methods.LoadData.calcTotal();
                    _that.Methods.LoadData.clearDetails();

                    $('#btnEdit_customize').addClass('display-none');
                    $('#btnAdd_customize').removeClass('display-none');

                    ctrl._divDetails.find('details#objDetails').removeAttr('open');
                })
            }
        };

        return _that;
    }

    $(document).ready(function () {
        Handler().Init();
    });
    $(document).on("click", ".row-edit", function () {
        $("#tblCatVar>tbody>tr").each(function (index, tr) {
            $(tr).removeClass("row-edit-sales")
        })

        $('#divDetails').find('details#objDetails').attr('open', '');
        $('#btnAdd_customize').addClass('display-none');
        $('#btnEdit_customize').removeClass('display-none');

        var tr_index = $(this).parent().parent().index();
        var objTr = $(this).parents('tr')
        $(objTr).addClass("row-edit-sales");

        var categoryID = $(objTr).find('td.tdcategoryID_02').text().trim();
        var varietyID = $(objTr).find('td.tdvarietyID_04').text().trim();
        var sizeID = $(objTr).find('td.tdsizeID_06').text().trim();
        var codePackID = $(objTr).find('td.tdcodePackID_07').text().trim();
        var presentationID = $(objTr).find('td.tdpresentationID_09').text().trim();
        var quantity = $(objTr).find('td.tdquantity_11').text().trim();
        var quantityMinumun = $(objTr).find('td.tdBoxesMin_12').text().trim();
        var flexible = $(objTr).find('td.tdxchkFlexible_13').text().trim();
        var price = $(objTr).find('td.tdprice_15').text().trim().trim();
        var packageProductID = $(objTr).find('td.tdpackageProductID_16').text().trim();
        var incotermID = $(objTr).find('td.tdincotermID_17').text().trim().trim();
        var conditionPaymentID = $(objTr).find('td.tdconditionPaymentID_19').text().trim();
        var paymentTerm = $(objTr).find('td.tdpaymentTerm_21').text().trim();
        var finalCustomerID = $(objTr).find('td.tdFinalCustomerID_25').text().trim();
        var paymentTermID = $(objTr).find('td.tdPaymentTermID_26').text().trim();
        var codePack2 = $(objTr).find('td.tdcodePack2_27').text().trim();

        $('#selectCategory_customize').selectpicker('val', categoryID.trim())
        $('#selectVariety_customize').selectpicker('val', varietyID.split(',')).selectpicker('refresh');
        $('#selectCodepack_customize').selectpicker('val', codePackID.trim()).selectpicker('refresh');
        $('#selectPresentation_customize').selectpicker('val', presentationID.trim())
        $('#selectSize_customize').selectpicker('val', sizeID.split(',')).selectpicker('refresh');
        $('#selectPackage_customize').selectpicker('val', packageProductID.trim())
        $('#selectIncoterm_customize').selectpicker('val', incotermID.trim()).selectpicker('refresh');
        $('#selectPriceContition_customize').selectpicker('val', conditionPaymentID.trim()).selectpicker('refresh');
        $('#selectPayment_customize').selectpicker('val', paymentTermID.trim())
        $('#txtPaymentTerm_customize').val(paymentTerm.trim());
        $('#txtCodePack_customize').val(codePack2.trim());
        $('#txtBoxes_customize').val(quantity.trim()).change();
        $('#txtPrice_customize').val(price.trim()).change();
        $('#selectFinalCustomer_customize').selectpicker('val', finalCustomerID.trim()).selectpicker('refresh');
        $('#txtRow_customize').val(tr_index);

        if (flexible == 1) {
            $('#txtMinBoxes_customize').prop('disabled', false);

        } else {
            $('#txtMinBoxes_customize').prop('disabled', true);
        }

        $('#txtMinBoxes_customize').val(quantityMinumun);

    });
    $(document).on("click", ".row-remove", function () {
        $(this).parents('tr').detach();
        Handler().Methods.LoadData.calcTotal();
    });
})();

$(function () {
    sDepartureWeekSelected = "0";
    sDepartureWeekEnd = "0";
    loadWeek();
    loadDefaultValues();
    showDataSearched();

    let btnshowmodal;
    switch (_cropID.toLowerCase()) {
        case 'blu':
            btnshowmodal = $(btnShowModalForCodePack);
            btnshowmodal.attr("data-target", "#blu");
            break;
        default:
            btnshowmodal = $(btnShowModalForCodePack);
            btnshowmodal.attr("data-target", "#demo");
            break;
    }
    btnshowmodal.addClass('collapsed');

    $(btnSearch).click(function () {
        Swal.fire({
            imageUrl: '../images/AgricolaAndrea71x64.png',
            title: 'AGROCOM',
            html: 'Loading information...',
            onBeforeOpen() {
                Swal.showLoading();
            },
            onOpen() {
                sDepartureWeekSelected = $("#selectWeekProduction").val();
                sDepartureWeekEnd = $("#selectWeekProductionEnd").val();
                showDataSearched();
                Swal.hideLoading();
                Swal.close();
            },
            onAfterClose() {

            },
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: false,
        });
    })

    $(document).on("input", ".txtPalletsConfirm", function (e) {
        sumPalletsConfirm();
        $(e).closest('tr').find('td.boxPallet')
    });

    $('#btnSave').click(function () {
        SaveOPdetail();
    });

    $('#btnCloseModalOPdetail').click(function () {
        $('#modalOPdetail').hide();
    });
    $('#btnShowModalForCodePack').click(function () {

        switch (_cropID.toLowerCase()) {
            case 'blu':

                $(_btnEditPOdetail).prop('hidden', true);
                $(_btnAddPOdetail).removeAttr('hidden');

                $(_txtBoxes).prop('disabled', false);
                GetFillSelectBrand();
                GetFillSelectVariety();
                GetFillSelectFormat(_viaID, _marketID);
                GetFillSelectTypeBox();
                GetFillSelectSize();
                cleanConstrol();
                break;
            default:

                $(btnUpdatePOdetail).prop('hidden', true);
                $(btnAddPOdetail).removeAttr('hidden');

                $("#selectWeightBox").prop('disabled', false);
                $("#selectTypeBox").prop('disabled', false);
                $("#selectBrand").prop('disabled', false);
                $("#selectLabel").prop('disabled', false);
                $("#SelectSizeBox").prop('disabled', false);
                $(txtBoxes).prop('disabled', false);
                $(txtPallets).prop('disabled', true);
                loadDataForCodePack();
        }

    });

    function GetFillSelectSize() {

        var opt = 'cro';
        var id = localStorage.cropID;
        $.ajax({
            type: "GET",
            url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
            async: false,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': '0'
            },
            success: function (data) {

                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataSizes = dataJson.map(
                        obj => {
                            return {
                                "id": obj.sizeID,
                                "name": obj.sizeID
                            }
                        }
                    );
                }
            }
        });

        $(_selectSize).empty();

        $.each(dataSizes, function (x, evt) {
            $('#_selectSize').append($('<option></option>').attr('value', jQuery.trim(evt.id)).text(evt.name));
        });
        $('#_selectSize').selectpicker('refresh').change();
    }

    function GetFillSelectBrand() {

        var opt = "cro";
        var gro = "";
        $.ajax({
            type: "GET",
            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: "/Sales/CategoryGetByCrop?opt=" + opt + "&id=" + _cropID + "&gro=" + gro,
            async: false,
            success: function (data) {
                if (data.length > 0) {

                    var dataJson = JSON.parse(data);
                    dataCategory = dataJson.map(
                        obj => {
                            return {
                                "categoryID": obj.categoryID,
                                "description": obj.description
                            }
                        }
                    );

                }
            }
        });

        $(_selectBrand).empty();

        let data = dataCategory.map(
            obj => {
                return {
                    "id": obj.categoryID,
                    "name": obj.description
                }
            }
        );
        loadCombo(data, '_selectBrand', true);
        $('#_selectBrand').selectpicker('refresh');
    }

    function GetFillSelectVariety() {

        var opt = 'cro';
        var log = '';
        var sUrlApi = "/CommercialPlan/VarietyByCrop?opt=" + opt + "&id=" + _cropID + "&log=" + log
        $.ajax({
            type: "GET",
            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: sUrlApi,
            async: false,
            success: function (data) {
                if (data == null || data.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    var dataJson = JSON.parse(data);
                    if (dataJson == null || dataJson.length == 0) {
                        sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                    } else {
                        dataVariety = dataJson.map(
                            obj => {
                                return {
                                    abbreviation: obj.abbreviation,
                                    id: obj.varietyID,
                                    description: obj.name
                                }
                            }
                        );
                    }
                }
            },
            error: function (datoEr) {
                sweet_alert_info('Information', "There was an issue trying to list data.");
            },
        });
        $(_selectVariety).empty();

        let data = dataVariety.map(
            obj => {
                return {
                    "id": obj.id,
                    "name": obj.description
                }
            }
        );

        $.each(data, function (x, evt) {
            $('#_selectVariety').append($('<option></option>').attr('value', jQuery.trim(evt.id)).text(evt.name));

        });
        SettingMultiselect($('#_selectVariety'));
    }

    function GetFillSelectFormat(_viaID, _marketID) {

        var opt = 'cro';
        var id = _cropID;
        $.ajax({
            type: "GET",
            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
            async: false,
            success: function (data) {
                _dataCodepack = JSON.parse(data);
            }
        });

        let dataCodepackSelect = _dataCodepack.filter(item => item.viaID == _viaID && item.marketID.trim() == _marketID.trim()).map(
            obj => {
                return {
                    "id": obj.codePackID,
                    "name": obj.description

                }
            }
        );

        $(_selectFormat).empty();

        loadCombo(dataCodepackSelect, '_selectFormat', true);
        $('#_selectFormat').selectpicker('refresh').change();

    }

    function GetFillSelectTypeBox() {

        var optID = "pkc";
        var filter01 = '';
        $.ajax({
            type: "GET",
            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: "/Sales/GetPackageProductAllMC?opt=" + optID + "&id=" + filter01,
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataTypeBox = dataJson.map(
                        obj => {
                            return {
                                "id": obj.packageProductID,
                                "description": obj.packageProduct,
                                "abbreviation": obj.abbreviation
                            }
                        }
                    );
                }
            }
        });

        $(_selectTypeBox).empty();

        let data = dataTypeBox.map(
            obj => {
                return {
                    "id": obj.id,
                    "name": obj.description
                }
            }
        );
        loadCombo(data, '_selectTypeBox', true);
        $('#_selectTypeBox').selectpicker('refresh');
    }

    function GetFillSelectPresentation(formatID) {

        var opt = 'pre';
        id = '';
        $.ajax({
            type: "GET",
            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: "/Sales/GetCodepackWithPresentation?opt=" + opt + "&id=" + id,
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    dataPresentation = dataJson
                }
            }
        });

        $(_selectPresentation).empty();

        let data = dataPresentation.filter(item => item.codePackID == formatID).map(
            obj => {
                return {
                    "id": obj.presentationID,
                    "name": obj.presentation + obj.moreInfo
                }
            });

        loadCombo(data, '_selectPresentation', false);
        $('#_selectPresentation').selectpicker('refresh').change();
    }

    function CalculateBoxPerPalletAndContainer() {

        if ($(_selectFormat).val() > 0) {
            var boxPallet = _dataCodepack.filter(element => element.codePackID == $(_selectFormat).val())[0].boxesPerPallet
            var boxContainer = _dataCodepack.filter(element => element.codePackID == $(_selectFormat).val())[0].boxPerContainer
            $(_txtBoxes).val('0');
            $(_txtBoxPerPallet).val(boxPallet);
            $(_txtBoxPerPerContainer).val(boxContainer);
        }
    }

    function calculeCodepack() {
        if ($("#_selectBrand option:selected").val() == 0) {
            return false;
        }
        if ($("#_selectFormat option:selected").val() == 0) {
            return false;
        }
        if ($("#_selectTypeBox option:selected").val() == 0) {
            return false;
        }
        let varietyID = '';
        let arrayVarietyID = $(_selectVariety).val();
        $.each(arrayVarietyID, function (index, value) {
            varietyID += value + ',';
        });
        if (varietyID.length == 0) {
            $(_txtCodePack).val('');
            return false;
        }

        let formatID;
        let typeBoxID;
        let brandID;
        let presentationID;
        var varietyCode;
        var formatCode;
        var typeBoxCode;
        var brandCode;
        var presentationCode;
        var codepack = '';
        let codepackAux = '';

        $.each(arrayVarietyID, function (index, value) {
            varietyID = value;
            formatID = $(_selectFormat).val();
            typeBoxID = $(_selectTypeBox).val();
            brandID = $(_selectBrand).val();
            viaID = _viaID;
            presentationID = $(_selectPresentation).val();

            varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
            formatCode = _dataCodepack.filter(item => item.codePackID == formatID && item.viaID == viaID)[0].weight;
            typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation;
            brandCode = dataCategory.filter(item => item.categoryID == brandID)[0].description;
            presentationCode = dataPresentation.filter(item => item.presentationID == presentationID && item.codePackID == formatID)[0].presentation;

            codepackAux = varietyCode.trim() + '_' +
                formatCode +
                typeBoxCode.slice(0, 1) +
                brandCode.slice(0, 1) + '_' +
                presentationCode;

            codepack = (codepackAux.trim() == '') ? codepack : codepackAux + ',' + codepack;

        });
        codepack = codepack.substring(0, codepack.length - 1);
        let arrayCodePack = codepack.split(',');
        let codepacksToStore = '';
        codepack = '';
        $.each(arrayCodePack, function (iRowCode, oCode) {
            codepack = (oCode + '_+' + $('#_selectSize option:selected').val().substring(1, $('#_selectSize option:selected').val().length) + '_' + 1) + '\n' + codepack;
            codepacksToStore = (oCode + '_+' + $('#_selectSize option:selected').val().substring(1, $('#_selectSize').val().length) + '_' + 1) + ',' + codepacksToStore;
        });
        localStorage.codepacksToStore = codepacksToStore;
        $(_txtCodePack).val(codepack);

    }

    $('#_txtBoxes').change(function () {
        var currentBoxes = $(this).val();
        var boxPallet = parseInt($('#_txtBoxPerPallet').val());
        var pallets = parseInt(currentBoxes) / boxPallet;

        $('#_txtPallets').val(pallets.toFixed(2))
    });

    $('#chkplu').change(function () {
        createCodePack();
    });

    $(document).on("change", "#_selectFormat", function () {
        GetFillSelectPresentation($('#_selectFormat option:selected').val());
        CalculateBoxPerPalletAndContainer();
        calculeCodepack();
    });

    $(document).on("change", "#_selectBrand", function () {
        calculeCodepack();
    });

    $(document).on("change", "#_selectVariety", function () {
        calculeCodepack();
    });

    $(document).on("change", "#_selectTypeBox", function () {
        calculeCodepack();
    });

    $(document).on("change", "#_selectPresentation", function () {
        calculeCodepack();
    });
    $(document).on("change", "#_selectSize", function () {
        calculeCodepack();
    });
    $('#btnUpdatePOdetail').click(function () {
        sweet_alert_progressbar();
        if (!UpdateOPdetail()) {
            sweet_alert_progressbar_cerrar();
            sweet_alert_error('Error', 'There were errors during the update');
            return;
        }
        sweet_alert_progressbar_cerrar();
        sweet_alert_success('Information for user!', 'Successful update');
        $("#demo").css({ "border": "none" })
        $("#blu").css({ "border": "none" })
        $(btnUpdatePOdetail).prop('hidden', true);
        $(btnAddPOdetail).removeAttr('hidden');
    });

    $('#_btnEditPOdetail').click(function () {
        sweet_alert_progressbar();
        if (!UpdateOPdetail()) {
            sweet_alert_progressbar_cerrar();
            sweet_alert_error('Error', 'There were errors during the update');
            return;
        }
        sweet_alert_progressbar_cerrar();
        sweet_alert_success('Information for user!', 'Successful update');
        $("#demo").css({ "border": "none" })
        $("#blu").css({ "border": "none" })
        $(_btnEditPOdetail).prop('hidden', true);
        $(_btnAddPOdetail).removeAttr('hidden');
        cleanConstrol();
    });

    $('#_btnAddPOdetail').click(function () {
        sweet_alert_progressbar();
        let bRsl = false;
        switch (_cropID.toLowerCase()) {
            case 'blu':
                if (!addOPdetailBLU()) {
                    return;
                }
                else bRsl = true;
                break;
            default:
                if (!addOPdetail()) {
                    return;
                } else bRsl = true;
                break;
        }
        if (!bRsl) {
            sweet_alert_progressbar_cerrar();
            sweet_alert_error('Error', 'There were errors during the add');
            return;
        }
        sweet_alert_success('Information for user!', 'Successful upload');
        $(btnUpdatePOdetail).prop('hidden', true);
        $(btnAddPOdetail).removeAttr('hidden');
        cleanConstrol();
    });

    $('#btnAddPOdetail').click(function () {
        sweet_alert_progressbar();
        let bRsl = false;
        switch (_cropID.toLowerCase()) {
            case 'blu':
                if (!addOPdetailBLU()) {
                    return;
                } else bRsl = true;
                break;
            default:
                if (!addOPdetail()) {
                    return;
                } else bRsl = true;
                break;
        }
        if (!bRsl) {
            sweet_alert_progressbar_cerrar();
            sweet_alert_error('Error', 'There were errors during the add');
            return;
        }
        sweet_alert_progressbar_cerrar();
        sweet_alert_success('Information for user!', 'Successful upload');
        $(btnUpdatePOdetail).prop('hidden', true);
        $(btnAddPOdetail).removeAttr('hidden');
        cleanConstrol();
    });

    $("#selectVariety").change(function () {
        createCodePack();
    });

    $('#selectCategory').change(function () {
        loadCombo([], 'selectBrand', true);
        loadCombo([], 'selectLabel', true);
        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#selectCategory option:selected").val().trim().length > 0) {
            loadDataBrandByCategory($("#selectCategory option:selected").val().trim())
        }
        if (dataBrand.length > 0) {
            loadCombo(dataBrand, 'selectBrand', false);
            loadCombo(dataLabel, 'selectLabel', false);
            $('#selectBrand').selectpicker('refresh');
            $('#selectLabel').selectpicker('refresh');

        } else {
            loadCombo([], 'selectBrand', true);
            loadCombo([], 'selectLabel', true);
        }
        if (EndFirstLoading == 1) createCodePack();
    });

    $('#selectWeightBox').change(function () {
        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#selectWeightBox option:selected").val().trim().length > 0) {
            if ($("#selectTypeBox option:selected").val().trim().length > 0) {
                var format = ($("#selectWeightBox option:selected").val().trim());
                var typeBox = ($("#selectTypeBox option:selected").val().trim());
                getBoxPerPalletContainer(format, typeBox);
            }
        }
        if (dataCodePack2.length > 0) {
            loadCombo(dataCodePack2, 'SelectSizeBox', false);
            $('#SelectSizeBox').selectpicker('refresh');
            $('#SelectSizeBox').change();
            $('#txtBoxes').val(0);
            $('#txtPallets').val(0);

        } else {
            loadComboSizeBox([], 'SelectSizeBox', false, "")//Indico el llenado del sizeBox
            $('#SelectSizeBox').selectpicker('refresh');
            $('#txtBoxPallet').val(0);
            $('#txtBoxes').val(0);
            $('#txtPallets').val(0);

        }
        calculatePallets();
        if (EndFirstLoading == 1) createCodePack();
    });

    $('#selectTypeBox').change(function () {
        loadCombo([], 'SelectSizeBox', true);

        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#selectTypeBox option:selected").val().trim().length > 0) {
            if ($("#selectWeightBox option:selected").val().trim().length > 0) {
                var format = ($("#selectWeightBox option:selected").val().trim());
                var typeBox = ($("#selectTypeBox option:selected").val().trim());

                getBoxPerPalletContainer(format, typeBox);
            }
        }
        if (dataCodePack2.length > 0) {
            loadCombo(dataCodePack2, 'SelectSizeBox', false);
            $('#SelectSizeBox').selectpicker('refresh');
            $('#SelectSizeBox').change();
            $('#txtBoxes').val(0);
            $(txtPallets).val(0);
        } else {
            loadComboSizeBox([], 'SelectSizeBox', false, "")
            $('#SelectSizeBox').selectpicker('refresh');
            $('#txtBoxPallet').val(0);
            $(txtPallets).val(0);
        }
        if (EndFirstLoading == 1) createCodePack();
    });

    $(selectPresentation).change(function () {
        createCodePack();
    });

    $(selectLabel).change(function () {
        createCodePack();
    });

    $(SelectSizeBox).change(function () {
        $(txtBoxPerPallet).val(0);
        $(txtBoxPerContainer).val(0);
        $(txtPallets).val(0);
        if (dataCodePack2.length > 0) {
            var dataFilter = dataCodePack2.filter(item => item.id == $(SelectSizeBox).val())[0];
            $(txtBoxPerPallet).val(dataFilter.boxesPerPallet);
            $(txtBoxPerContainer).val(dataFilter.boxPerContainer);
            $('#txtPallets').val(0);
            if (EndFirstLoading == 1) createCodePack();
        }
    });

    $('#txtPallets').change(function () {
        var currentBoxes = $(this).val();
        var boxPallet = parseInt($('#txtBoxPerPallet').val());
        var pallets = $(this).val();
        quatityBoxes = (boxPallet * pallets);
        $(txtBoxes).val(quatityBoxes);

    })

    $('#txtBoxes').change(function () {
        var currentBoxes = $(this).val();
        var boxPallet = parseInt($('#txtBoxPerPallet').val());
        var pallets = parseInt(currentBoxes) / boxPallet;
        var boxes = parseInt($(txtBoxes).val());

        if ($('#chkpadlock').is(":checked")) {
            pallets = pallets + 0.2;
        } else {
            if (pallets % 1 == 0) {
                pallets = pallets;
            } else {
                pallets = pallets;
                $(txtBoxes).val(0);
            }
        }

        if (boxes == 0) {
            $(txtPallets).val(0);
        } else {
            $('#txtPallets').val(pallets.toFixed(2));
        }

    });

    $(document).on("click", ".row-remove", function () {
        $(this).parents('tr').detach();
        calcTotal();
    });

    if (_cropID == 'UVA') {
        $("#chkpadlock").attr("disabled", "disabled");
        $("#chkplu").attr("disabled", "disabled");
    } else {
        $("#chkpadlock").removeAttr("disabled");
        $("#chkplu").removeAttr("disabled");
    }
});

function cleanConstrol() {
    switch (_cropID.toLowerCase()) {
        case 'blu':

            $(_txtCodePack).val("");
            $(_txtPriceBox).val("");
            $(_txtBoxes).val(0);
            $(_txtPallets).val(0);
            $(_txtBoxPerPerContainer).val(0);
            $(_txtBoxPerPallet).val(0);
            $("#_selectBrand").val("").selectpicker('refresh');
            $("#_selectFormat").val("").selectpicker('refresh').change();
            $("#_selectTypeBox").val("").selectpicker('refresh');
            $('#_selectVariety').multiselect('deselectAll', false);
            $('#_selectVariety').multiselect('refresh');
            break;
        default:
            $(txtCodePack).val("");
            $(txtPallets).val(0);
            $(txtBoxes).val(0);
    }
}

function EditOPdetail(xthis) {
    $("#demo").css({ "border": "1px solid red" })
    $("#blu").css({ "border": "1px solid red" })
    let _varietyID;
    let _sizeID;
    let _categoryID;
    let _weightBoxID;
    let _typeBoxID;
    let _brandID;
    let _presentationID;
    let _labelID;
    let _sizeBoxID;
    let _boxPerPallet;
    let _boxPerContainer;
    let _boxes;
    let _pallets;
    let _codePack;
    let _pricebox;
    sweet_alert_progressbar();
    switch (_cropID.toLowerCase()) {        
        case 'blu':
            trRowEdit = $(xthis).closest("tr");
            $(_btnAddPOdetail).prop('hidden', true);
            $(_btnEditPOdetail).removeAttr('hidden');

            _varietyID = $(xthis).closest('tr').find(".tdVarietyID").text();
            _sizeID = $(xthis).closest('tr').find(".tdSizeBoxID").text();
            _categoryID = $(xthis).closest('tr').find(".tdCategoryID").text();
            _weightBoxID = $(xthis).closest('tr').find(".tdWeightBoxID").text();
            _typeBoxID = $(xthis).closest('tr').find(".tdTypeBoxID").text();
            _brandID = $(xthis).closest('tr').find(".tdBrandID").text();
            _presentationID = $(xthis).closest('tr').find(".tdPresentationID").text();
            _labelID = $(xthis).closest('tr').find(".tdLabelID").text();
            _sizeBoxID = $(xthis).closest('tr').find(".tdSizeBoxID").text();
            _boxPerPallet = $(xthis).closest('tr').find("#tdBoxPerPallet").text();
            _boxPerContainer = $(xthis).closest('tr').find("#tdBoxPerContainer").text();
            _boxes = $(xthis).closest('tr').find("#tdBoxes").text();
            _pallets = $(xthis).closest('tr').find("#tdPallets").text();
            _codePack = $(xthis).closest('tr').find("#tdCodePack").text();
            let arrayCodePack = _codePack.split(',');
            let codepackToShow = '';
            $.each(arrayCodePack, function (iRowCode, oCode) {
                codepackToShow = oCode + '\n' + codepackToShow;
            });
            localStorage.codepacksToStore = _codePack;
            $(_selectVariety).multiselect('select', $(xthis).closest('tr').find(".tdVarietyID").text().split(','));
            $(_selectSize).val($(xthis).closest('tr').find(".tdSizeBoxID").text()).selectpicker('refresh');
            $("#_selectBrand").val(_categoryID).selectpicker('refresh');
            $("#_selectFormat").val(_weightBoxID).selectpicker('refresh').change();
            $("#_selectTypeBox").val(_typeBoxID).change().selectpicker('refresh');
            $("#_selectPresentation").val(_presentationID).selectpicker('refresh');
            $(_txtBoxPerPallet).val(_boxPerPallet);
            $(_txtBoxPerPerContainer).val(_boxPerContainer);
            $(_txtBoxes).val(_boxes).change();
            $(_txtCodePack).text(codepackToShow);
            if ($(xthis).closest('tr').find("td.tdWorkOrder").text().toLowerCase() == 'YES') {
                $('#chkWorkOrder').attr('checked');
            } else {
                $('#chkWorkOrder').removeAttr('checked');
            }
            break;
        default:
            trRowEdit = $(xthis).closest("tr");
            $(btnAddPOdetail).prop('hidden', true);
            $(btnUpdatePOdetail).removeAttr('hidden');
            _varietyID = $(xthis).closest('tr').find(".tdVarietyID").text();
            console.log(_varietyID);
            _categoryID = $(xthis).closest('tr').find(".tdCategoryID").text();
            _weightBoxID = $(xthis).closest('tr').find(".tdWeightBoxID").text();
            _typeBoxID = $(xthis).closest('tr').find(".tdTypeBoxID").text();
            _brandID = $(xthis).closest('tr').find(".tdBrandID").text();
            _presentationID = $(xthis).closest('tr').find(".tdPresentationID").text();
            _labelID = $(xthis).closest('tr').find(".tdLabelID").text();
            _sizeBoxID = $(xthis).closest('tr').find(".tdSizeBoxID").text();
            _boxPerPallet = $(xthis).closest('tr').find("#tdBoxPerPallet").text();
            _boxPerContainer = $(xthis).closest('tr').find("#tdBoxPerContainer").text();
            _boxes = $(xthis).closest('tr').find("#tdBoxes").text();
            _pallets = $(xthis).closest('tr').find("#tdPallets").text();
            _codePack = $(xthis).closest('tr').find("#tdCodePack").text();
            $("#selectVariety").val(varietyID).selectpicker('refresh');
            $("#selectCategory").val(categoryID).selectpicker('refresh');
            $("#selectWeightBox").val(weightBoxID).selectpicker('refresh');
            $("#selectTypeBox").val(typeBoxID).change().selectpicker('refresh');
            $("#selectBrand").val(brandID).selectpicker('refresh');
            $("#selectPresentation").val(presentationID).selectpicker('refresh');
            $("#selectLabel").val(labelID).selectpicker('refresh');
            $("#SelectSizeBox").val(sizeBoxID).selectpicker('refresh');
            $("#txtBoxPerPallet").val(boxPerPallet);
            $("#txtBoxPerContainer").val(boxPerContainer);
            $("#txtBoxes").val(boxes).change();
            $("#txtCodePack").text(codePack);
            if ($(xthis).closest('tr').find("td.tdWorkOrder").text().toLowerCase() == 'si') {
                $('#chkWorkOrder').attr('checked');
            } else {
                $('#chkWorkOrder').removeAttr('checked');
            }            
    }
    sweet_alert_progressbar_cerrar();
}

function loadDataForCodePack() {
    var opt = 'cro';
    var id = 0;
    var log = 0;
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/VarietyGetByCropMC?opt=" + opt + "&id=" + localStorage.cropID + "&log=" + log,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataVariety = dataJson.map(
                    obj => {
                        return {
                            "id": obj.varietyID,
                            "name": obj.name,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        },
        complete: function () {

        }
    });

    var opt = 'cro';
    var gro = '';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/CategoryGetByCropMC?opt=" + opt + "&id=" + localStorage.cropID + "&gro=" + gro,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataCategory = dataJson.map(
                    obj => {
                        return {
                            "id": obj.categoryID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    var opt = 'fci';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/GetFormatByCropIDMC?opt=" + opt + "&id=" + localStorage.cropID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataFormat = dataJson.map(
                    obj => {
                        return {
                            "id": obj.formatID,
                            "abreviation": obj.format,
                            "name": obj.format,
                            "weight": obj.weight
                        }
                    }
                );
            }
        }
    });

    var opt = 'pkc';
    var id = '';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/GetPackageProductAll?opt=" + opt + "&id=" + id,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataTypeBox = dataJson.map(
                    obj => {
                        return {
                            "id": obj.packageProductID,
                            "name": obj.packageProduct,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

    var opt = 'pci';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/GetPresentByCropMC?opt=" + opt + "&id=" + localStorage.cropID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataPresentation = dataJson.map(
                    obj => {
                        return {
                            "id": obj.presentationID,
                            "name": obj.description,
                            "presentation": obj.presentation
                        }
                    }
                );
            }
        }
    });

    loadCombo(dataVariety, 'selectVariety', false);
    $('#selectVariety').prop("disabled", false);
    $('#selectVariety').selectpicker('refresh');

    loadCombo(dataCategory, 'selectCategory', false);
    $('#selectCategory').prop("disabled", false);
    $('#selectCategory').selectpicker('refresh');

    loadCombo(dataFormat, 'selectWeightBox', false);
    $('#selectWeightBox').prop("disabled", false);
    $('#selectWeightBox').selectpicker('refresh');

    loadCombo(dataTypeBox, 'selectTypeBox', false);
    $('#selectTypeBox').prop("disabled", false);
    $('#selectTypeBox').selectpicker('refresh');

    loadCombo(dataPresentation, 'selectPresentation', false);
    $('#selectPresentation').prop("disabled", false);
    $('#selectPresentation').selectpicker('refresh');

    $('#selectCategory').change();
    $('#selectWeightBox').change();

    $('#selectTypeBox').change();
    createCodePack();
    EndFirstLoading = 1;

}

function loadWeek() {

    var campaignID = localStorage.campaignID;
    var cropID = localStorage.cropID;

    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/forecast/ListWeekByCampaign?campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataAllWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    var opt = 'act';
    var val = '';
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/GetCurrentWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                ActualyWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    var opt = 'all';
    var id = '';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/AllPlant?opt=" + opt + "&id=" + id,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataPlant = dataJson.map(
                    obj => {
                        return {
                            "id": obj.packingID,
                            "name": obj.packing

                        }
                    }
                );
            }
        }
    });

    var opt = 'act';
    var plantID = '';
    var weekProduction = 0;
    var weekProductionEnd = 0;
    var statusConfirmID = 0;
    var userID = 0;
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/GetPOstatusConfirm?opt=" + opt + "&plantID=" + plantID + "&weekProduction=" + weekProduction + "&weekProductionEnd=" + weekProductionEnd + "&statusConfirmID=" + statusConfirmID + "&campaignID=" + campaignID + "&userID=" + userID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {
                        return {
                            "id": obj.statusConfirmID,
                            "name": obj.statusConfirm
                        }
                    }
                );
            }
        }
    });
}

function loadDefaultValues() {

    loadComboFilter(dataAllWeek, 'selectWeekProduction', false, '[ALL]');
    $('#selectWeekProduction').selectpicker('refresh');
    sDepartureWeekSelected = $("#selectWeekProduction option:selected").val();

    loadComboFilter(dataAllWeek, 'selectWeekProductionEnd', false, '[ALL]');
    $('#selectWeekProductionEnd').selectpicker('refresh');
    sDepartureWeekEnd = $("#selectWeekProductionEnd option:selected").val();

    loadComboFilter(dataPlant, 'selectPlant', true, '[ALL]');
    $('#selectPlant').selectpicker('refresh');

    loadComboFilter(dataStatus, 'selectStatusConfirm', true, '[ALL]');
    $('#selectStatusConfirm').selectpicker('refresh');

}

function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    if (data != null)
        for (var i = 0; i < data.length; i++) {
            content += "<option value='" + data[i].id + "'>";
            content += data[i].name;
            content += "</option>";
        }
    $('#' + control).empty().append(content);
}

function saveConfirmationPendingConfirm(xthis) {

    if ($(xthis).closest("tr").find('#txtCompleteDate').val() == "") {
        $(xthis).closest("tr").find('#txtCompleteDate').css({ 'border-color': 'red' });
        sweet_alert_warning('Warning!', 'Enter date');
        return;
    } else {
        $(xthis).closest("tr").find('#txtCompleteDate').css({ 'border-color': '#d1d3e2' });
    }

    if ($(xthis).closest("tr").find('#txtVGM').val() == "") {
        $(xthis).closest("tr").find('#txtVGM').css({ 'border-color': 'red' });
        sweet_alert_warning('Warning!', 'Enter VGM');
        return;
    } else {
        $(xthis).closest("tr").find('#txtVGM').css({ 'border-color': '#d1d3e2' });
    }

    $(xthis).closest("tr").find('#statusID').text("1");
    $(xthis).closest("tr").find('#status').text("Pending btnConfirm");
    $(xthis).closest("tr").find('#status').css({ 'background-color': '#ffc107', 'font-weight': 'bold', 'color': 'black' });

    $(xthis).closest("tr").find('#btnReject').removeAttr('hidden');
    $(xthis).closest("tr").find('#btnConfirm').removeAttr('hidden');


    $(xthis).closest("tr").find('#btnRequestLoading').prop('hidden', true);
    $(xthis).closest("tr").find('#btnModify').prop('hidden', true);


    ConfirmBeforeSaving(xthis);
}

function RejectPO(xthis) {
    if (jQuery.trim($(xthis).closest('tr').find('#comments').val()) == '') {
        sweet_alert_error('Error', 'Fill a comment');
        return;
    }
    ConfirmBeforeSaving(xthis);
}

function ConfirmPO(xthis) {
    if (!ValidatePackingSelected(xthis)) {
        return;
    }
    ConfirmBeforeSaving(xthis);
}

function AcceptPO(xthis) {
    if (!ValidatePackingSelected(xthis)) {
        return;
    }
    ConfirmBeforeSaving(xthis);
}

function ChangePO(xthis) {
    if (!ValidatePackingSelected(xthis)) {
        return;
    }
    ConfirmBeforeSaving(xthis);
}

function ValidatePackingSelected(xthis) {
    var sPackingID = '';
    var aPacking = $(xthis).closest("tr").find('#selectPacking').val();
    $.each(aPacking, function (index, value) {
        sPackingID += value + ',';
    });
    if (sPackingID.length == 0) {
        sweet_alert_error('Error', 'Choose a plant');
        return false;
    }
    return true;
}

function RequestLoadingPO(xthis) {
    if ($(xthis).closest("tr").find('#tdvalVariety').text() != "") {
        _valVariety = $(xthis).closest("tr").find('#tdvalVariety').text()
        var _arrayVarietyID = [];
        _arrayVarietyID = _valVariety.split(",");

        var bol = false;
        $.each(_arrayVarietyID, function (index, value) {
            if (_arrayVarietyID[index] > 1) {
                bol = true;
            }
        });

        if (bol == true) {
            sweet_alert_error('Error!', 'Select only 1 variety in every detail. Try again.');
            return;
        }
    }
    let _valSize
    if ($(xthis).closest("tr").find('#tdvalSize').text() != "") {
        _valSize = $(xthis).closest("tr").find('#tdvalSize').text()
        var _arraySizeID = [];
        _arraySizeID = _valSize.split(",");

        var bSize = false;
        $.each(_arraySizeID, function (index, value) {
            if (_arraySizeID[index] > 1) {
                bSize = true;
            }
        });

        if (bSize) {
            sweet_alert_error('Error!', 'Select only 1 size in every detail. Try again.');
            return;
        }
    }
    if ($(xthis).closest("tr").find('#txtCompleteDate').val() == "") {
        $(xthis).closest("tr").find('#txtCompleteDate').css({ 'border-color': 'red' });
        sweet_alert_warning('Warning!', 'Enter completed date.');
        return;
    } else {
        $(xthis).closest("tr").find('#txtCompleteDate').css({ 'border-color': '#d1d3e2' });
    }
    if ($(xthis).closest("tr").find('#txtLoadingDate').val() == "") {
        $(xthis).closest("tr").find('#txtLoadingDate').css({ 'border-color': 'red' });
        sweet_alert_warning('Warning!', 'Enter loading date.');
        return;
    } else {
        $(xthis).closest("tr").find('#txtLoadingDate').css({ 'border-color': '#d1d3e2' });
    }

    if ($(xthis).closest("tr").find('#txtVGM').val() == "") {
        $(xthis).closest("tr").find('#txtVGM').css({ 'border-color': 'red' });

        sweet_alert_warning('Warning!', 'Enter VGM.');
        return;
    } else {
        $(xthis).closest("tr").find('#txtVGM').css({ 'border-color': '#d1d3e2' });
    }

    if (!ValidatePackingSelected(xthis)) {
        return;
    }
    ConfirmBeforeSaving(xthis);
}

function ConfirmBeforeSaving(xthis) {
    Swal.fire({
        title: '¿Want to save?',
        text: "Changes made to the Request?.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            SaveDataPO(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            $('#tableList>tbody>tr').each(function (i, e) {
                var index = $(xthis).closest("tr").index();
                if (i == index) {
                    $(e).find('#btnEdit').removeAttr('hidden');
                    $(e).find('#btnExit').prop('hidden', true);
                    $(e).find('#btnAccept').prop('hidden', true);
                    $(e).find('#btnConfirm').prop('hidden', true);
                    $(e).find('#btnReject').prop('hidden', true);
                    $(e).find('#btnChange').prop('hidden', true);
                    $(e).find('#btnModify').prop('hidden', true);
                    $(e).find('#btnRequestLoading').prop('hidden', true);
                    $(e).find('#comments').prop('readonly', true);
                    $(e).find('#txtCompleteDate').prop('readonly', true);
                }
            });
            sweet_alert_error('Canceled!', 'Save process was canceled.')
        }
    });
}

function EditData(xthis) {
    var index = $(xthis).closest("tr").index();
    $('table#tableList>tbody>tr').each(function (i, e) {
        if (i == index) {
            $(e).find('#comments').removeAttr('readonly');
            $(e).find('#btnEdit').prop('hidden', true);
            var statusID = $(e).closest('tr').find('#statusID').text();
            switch (statusID) {
                case '1':	//Pending
                    $(e).find('td').find('#selectPacking').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').multiselect('refresh')
                    $(e).find('#btnExit').removeAttr('hidden');
                    $(e).find('#btnAccept').removeAttr('hidden');
                    $(e).find('#btnReject').removeAttr('hidden');
                    break;
                case '2':	//Accepted
                    $(e).find('td').find('#selectPacking').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').multiselect('refresh')
                    $(e).find('#btnExit').removeAttr('hidden');
                    $(e).find('#btnConfirm').removeAttr('hidden');
                    $(e).find('#btnReject').removeAttr('hidden');
                    break;
                case '3':	//Confirm					
                    $(e).find('#txtCompleteDate').removeAttr('disabled');
                    $(e).find('#txtLoadingDate').removeAttr('disabled');
                    $(e).find('#txtVGM').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').multiselect('refresh')
                    $(e).find('#btnExit').removeAttr('hidden');
                    $(e).find('#btnChange').removeAttr('hidden');
                    $(e).find('#btnRequestLoading').removeAttr('hidden');
                    $(e).find('#btnModify').removeAttr('hidden');
                    break;
                case '7':	//Modification Accepted					
                    $(e).find('#txtCompleteDate').removeAttr('disabled');
                    $(e).find('#txtLoadingDate').removeAttr('disabled');
                    $(e).find('#txtVGM').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').multiselect('refresh')
                    $(e).find('#btnExit').removeAttr('hidden');
                    $(e).find('#btnModify').removeAttr('hidden');
                    $(e).find('#btnReject').removeAttr('hidden');
                    $(e).find('#btnRequestLoading').removeAttr('hidden');
                    break;
                case '5':	//Request Loading for only edit packing					
                    if (parseInt($(e).closest('tr').find('#tdStatusIE').text()) == 2) {
                        $(e).find('#btnEdit').removeAttr('hidden');
                        sweet_alert_error('Error', 'Orden production already has a shipment instruction');
                        return;
                    }
                    let TotAttemptEditPacking = parseInt($(e).closest('tr').find('#tdTotAttemptEditPacking').text());
                    if (TotAttemptEditPacking == 0) {
                        $(e).find('#btnEdit').removeAttr('hidden');
                        sweet_alert_error('Error', 'Total of attempts is cero');
                        return;
                    }
                    if (parseInt($(e).closest('tr').find('#tdNroAttemptEditPacking').text()) == TotAttemptEditPacking) {
                        $(e).find('#btnEdit').removeAttr('hidden');
                        sweet_alert_error('Error', 'Number of attempts passed');
                        return;
                    }
                    $(e).find('td').find('#selectPacking').removeAttr('disabled');
                    $(e).find('td').find('#selectPacking').multiselect('refresh')
                    $(e).find('#btnExit').removeAttr('hidden');
                    $(e).find('#btnModifyPacking').removeAttr('hidden');
                    break;
            }
        }
        else {
            $(e).find('#comments').prop('readonly', true);
            $(e).find('#txtCompleteDate').prop('disabled', true);
            $(e).find('#txtLoadingDate').prop('disabled', true);
            $(e).find('#txtVGM').prop('disabled', true);
            $(e).find('#selectPacking').prop('disabled', false);
            $(e).find('#selectPacking').multiselect('refresh')
            $(e).find('#btnEdit').removeAttr('hidden');
            $(e).find('#btnExit').prop('hidden', true);
            $(e).find('#btnAccept').prop('hidden', true);
            $(e).find('#btnConfirm').prop('hidden', true);
            $(e).find('#btnReject').prop('hidden', true);
            $(e).find('#btnChange').prop('hidden', true);
            $(e).find('#btnModify').prop('hidden', true);
            $(e).find('#btnRequestLoading').prop('hidden', true);
            $(e).find('#btnModifyPacking').prop('hidden', true);
            if ($(e).closest('tr').find('#comments').val() != $(e).closest('tr').find('#commentsAux').text()) {
                var commentsAux = $(e).closest('tr').find('#commentsAux').text();
                $(e).closest('tr').find('#comments').val(commentsAux);
            }
        }
    })

}

function ModifyPackingAfterRequest(xthis) {
    if (!ValidatePackingSelected(xthis)) {
        return;
    }
    SaveDataPO(xthis);
}

function Exit(xthis) {
    var index = $(xthis).closest("tr").index();
    $('table#tableList>tbody>tr').each(function (i, e) {
        if (i == index) {
            $(e).find('#comments').prop('readonly', true);
            $(e).find('#txtCompleteDate').prop('disabled', true);
            $(e).find('#txtLoadingDate').prop('disabled', true);
            $(e).find('#txtVGM').prop('disabled', true);
            $(e).find('#selectPacking').prop('disabled', false);
            $(e).find('#selectPacking').multiselect('refresh')
            $(e).find('#btnEdit').removeAttr('hidden');
            $(e).find('#btnExit').prop('hidden', true);
            $(e).find('#btnAccept').prop('hidden', true);
            $(e).find('#btnConfirm').prop('hidden', true);
            $(e).find('#btnReject').prop('hidden', true);
            $(e).find('#btnChange').prop('hidden', true);
            $(e).find('#btnModify').prop('hidden', true);
            $(e).find('#btnRequestLoading').prop('hidden', true);
            $(e).find('#btnModifyPacking').prop('hidden', true);

            if ($(e).closest('tr').find('#comments').val() != $(e).closest('tr').find('#commentsAux').text()) {
                var commentsAux = $(e).closest('tr').find('#commentsAux').text();
                $(e).closest('tr').find('#comments').val(commentsAux);
            }
        }

    })
}

function VerifyCompletedDay(xthis) {
    if (ContBlur > 0) {
        ContBlur = 0;
        return;
    }
    var _aShipmentDateTime = $(xthis).closest('tr').find('#txtDateAux').text().split(" "); //format from bd:dd/mm/yyyy
    var _aShipmentDate = _aShipmentDateTime[0].split("/");
    var _sShipmentDate = _aShipmentDate[2] + '/' + _aShipmentDate[1] + '/' + _aShipmentDate[0];
    if (jQuery.trim($(xthis).val()) != '') {
        var aCompletedDateTime = $(xthis).val().split(" ");
        var aCompletedDate = aCompletedDateTime[0].split("/");

        //<<<Verificar que el número de semana seleccionado del calendario este entre la semana de producción y de salida
        var sShipmentDate = aCompletedDate[2] + '/' + aCompletedDate[1] + '/' + aCompletedDate[0];
        var day = NameWeekDay(sShipmentDate);
        $(xthis).closest('tr').find('#day').text(day);
    }
    else {
        $(xthis).closest('tr').find('#day').text('');
        $(xthis).val('');
        ContBlur += 1;
        $(xthis).blur();
    }
}

Date.prototype.getWeekNumber = function () {
    if (this == null || this == undefined) return 0;
    else {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    }
}

function VerifyLoadingDate(xthis) {
    if (jQuery.trim($(xthis).closest('tr').find('#txtCompleteDate').val()) == '') {
        sweet_alert_error('Error', 'Select a complete date.');
        return;
    }
    if ((new Date(jQuery.trim($(xthis).closest('tr').find('#txtCompleteDate').val())).getTime() >
        new Date(jQuery.trim($(xthis).closest('tr').find('#txtLoadingDate').val())).getTime())) {
        sweet_alert_error('Error', 'Complete date must be minor than loading date.');
        return;
    }

}

function SaveDataPO(xthis) {
    sweet_alert_progressbar();
    var sCompletedDateTime = '';
    var sLoadingDateTime = '';
    var _nroAttemptsPacking = 0;
    switch (xthis.id) {
        case 'btnAccept':
            if (jQuery.trim($(xthis).closest('tr').find('#txtCompleteDate').val()) != '') {
                var aCompletedDateTime = $(xthis).closest('tr').find('#txtCompleteDate').val().split(" ");
                var aCompletedDate = aCompletedDateTime[0].split("/");
                sCompletedDateTime = aCompletedDate[2] + '-' + aCompletedDate[1] + '-' + aCompletedDate[0] + ' ' + aCompletedDateTime[1];
                $(xthis).closest('tr').find('#txtDateAux').text(aCompletedDate[0] + '/' + aCompletedDate[1] + '/' + aCompletedDate[2] + ' ' + aCompletedDateTime[1]);

            }
            break;
        case 'btnConfirm':
            if (jQuery.trim($(xthis).closest('tr').find('#txtCompleteDate').val()) != '') {
                var aCompletedDateTime = $(xthis).closest('tr').find('#txtCompleteDate').val().split(" ");
                var aCompletedDate = aCompletedDateTime[0].split("/");
                sCompletedDateTime = aCompletedDate[2] + '-' + aCompletedDate[1] + '-' + aCompletedDate[0] + ' ' + aCompletedDateTime[1];
                $(xthis).closest('tr').find('#txtDateAux').text(aCompletedDate[0] + '/' + aCompletedDate[1] + '/' + aCompletedDate[2] + ' ' + aCompletedDateTime[1]);

            }

            break;
        case 'btnChange':
            if (jQuery.trim($(xthis).closest('tr').find('#txtCompleteDate').val()) != '') {
                var aCompletedDateTime = $(xthis).closest('tr').find('#txtCompleteDate').val().split(" ");
                var aCompletedDate = aCompletedDateTime[0].split("/");
                sCompletedDateTime = aCompletedDate[2] + '-' + aCompletedDate[1] + '-' + aCompletedDate[0] + ' ' + aCompletedDateTime[1];
                $(xthis).closest('tr').find('#txtDateAux').text(aCompletedDate[0] + '/' + aCompletedDate[1] + '/' + aCompletedDate[2] + ' ' + aCompletedDateTime[1]);

            }

            break;
        case 'btnRequestLoading':
            if (jQuery.trim($(xthis).closest('tr').find('#txtCompleteDate').val()) != '') {
                var aCompletedDateTime = $(xthis).closest('tr').find('#txtCompleteDate').val().split(" ");
                var aCompletedDate = aCompletedDateTime[0].split("/");
                sCompletedDateTime = aCompletedDate[2] + '-' + aCompletedDate[1] + '-' + aCompletedDate[0] + ' ' + aCompletedDateTime[1];


            }
            else {
                sCompletedDateTime = GetCurrentDateTime();
                $(xthis).closest('tr').find('#txtDateAux').text(sCompletedDateTime);
            }
            if (jQuery.trim($(xthis).closest('tr').find('#txtLoadingDate').val()) != '') {
                var aLoadingDateTime = $(xthis).closest('tr').find('#txtLoadingDate').val().split(" ");
                var aLoadingDate = aLoadingDateTime[0].split("/");
                sLoadingDateTime = aLoadingDate[2] + '-' + aLoadingDate[1] + '-' + aLoadingDate[0] + ' ' + aLoadingDateTime[1];
            }
            else {
                sLoadingDateTime = GetCurrentDateTime();
            }
            break;
        case 'btnModifyPacking':
            _nroAttemptsPacking = parseInt($(xthis).closest('tr').find('#tdNroAttemptEditPacking').text()) + 1;
    }

    var _orderProductionID = $(xthis).closest('tr').find('#orderProductionID').text();
    var _statusConfirm = (xthis.id == 'btnReject') ? 4 : ($(xthis).closest('tr').find('#statusID').text() == 1) ? 2 : ($(xthis).closest('tr').find('#statusID').text() == 2) ? 3 : (xthis.id == 'btnChange') ? 3 : ($(xthis).closest('tr').find('#statusID').text() == 7) ? 5 : 5;
    var _day = $(xthis).closest('tr').find('#day').text();
    var _comments = $(xthis).closest('tr').find('#comments').val();
    $(xthis).closest('tr').find('#commentsAux').text(_comments);
    var sPackingID = '';
    var aPacking = $(xthis).closest('tr').find('#selectPacking').val();
    $.each(aPacking, function (index, value) {
        sPackingID += value + ',';
    });
    var stextVGM = ($(xthis).closest('tr').find('#txtVGM').val() == '') ? 0 : ($(xthis).closest('tr').find('#txtVGM').val());//  sí txtvgm es = '' será 0 sino 

    var _OPdetail = [];
    var _objOPdetail = {};
    _objOPdetail["Quantity"] = '0';
    _objOPdetail["categoryID"] = '';
    _objOPdetail["varietyID"] = '0';
    _objOPdetail["formatID"] = '0';
    _objOPdetail["packageProductID"] = '0';
    _objOPdetail["brandID"] = '0';
    _objOPdetail["presentationID"] = '0';
    _objOPdetail["labelID"] = '0';
    _objOPdetail["codepackID"] = '0';
    _objOPdetail["codepack"] = '';
    _OPdetail.push(_objOPdetail);
    let jsonObjOPdetail = JSON.stringify({
        cropID: _cropID,
        orderProductionID: _orderProductionID,
        statusConfirmID: _statusConfirm,
        completedDate: sCompletedDateTime,
        loadingDate: sLoadingDateTime,
        vgm: stextVGM,
        day: _day,
        userID: sessionUserID,
        comments: _comments,
        packingID: sPackingID,
        editAttemptsPacking: _nroAttemptsPacking,
        orderProductionDetail: _OPdetail,

    });

    var sUrlApi = "/PO/OrderProductionConfirmStatus";
    var bRsl = false;
    $.ajax({
        type: 'POST',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: sUrlApi,
        data: jsonObjOPdetail,
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
            } else {
                bRsl = true;
            }
        },
        error: function (datoError) {
            sweet_alert_info('Error!', 'Fail PO Updated.');
        },
        complete: function () {
            if (bRsl) {

                sweet_alert_success('Good Job!', 'PO Saved.');
                Exit(xthis);
                showDataSearched();
            }
        }
    });

}

function GetSummary() {
    $("table#tblDetails tbody").html('');
    var plantID = $("#selectPlant").val();
    if (plantID == 'All') {
        plantID = '';
    }

    var opt = "res";
    var weekProduction = 0;
    var weekProductionEnd = 0;
    var statusConfirmID = $("#selectStatusConfirm").val();
    if (statusConfirmID == 'All') {
        statusConfirmID = 0;
    }
    var campaignID = localStorage.campaignID;
    var userID = 0;

    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/GetPOConfirmResumen?opt=" + opt + "&plantID=" + plantID +
            "&weekProduction=" + weekProduction +
            "&weekProductionEnd=" + weekProductionEnd +
            "&statusConfirmID=" + statusConfirmID +
            "&campaignID=" + campaignID +
            "&userID=" + userID,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataSummary = JSON.parse(data);
                var tr = '';
                $.each(dataSummary, function (i, e) {
                    tr += '<tr>';
                    tr += '<td style="text-align: center; font-size:12px; display:none" id="productionWeek">' + e.projectedWeekID + '</td>';
                    tr += '<td style="text-align: center; font-size:12px" id="productionWeek">' + e.projectedWeek + '</td>';
                    tr += '<td style="text-align: center; font-size:12px" id="assigned">' + e.assigned + '</td>';
                    tr += '<td style="text-align: center; font-size:12px" id="pendingConfirm">' + e.pendingConfirm + '</td>';
                    tr += '<td style="text-align: center; font-size:12px; id="confirm">' + e.confirm + '</td>';
                    tr += '<td style="text-align: center; font-size:12px; id="completed">' + e.completed + '</td>';
                    tr += '<td style="text-align: center; font-size:12px; id="reject">' + e.reject + '</td>';
                    tr += '<td style="text-align: center; font-size:12px; id="fulfillment">' + e.fulfillment + '</td>';
                    tr += '</tr>';

                });
                $('table#tblDetails tbody').empty().append(tr);
            }

        },
        complete: function () {
            $('#tblDetails').DataTable({
                "order": [[1, "DESC"]],
                destroy: true
            });
            $('#tblDetails').dataTable().fnDestroy();
        }
    });

}

function showDataSearched() {
    var aCompletedDateTime = [];
    var _completedDate;
    var _loadingDate;
    var sShipmentDate;
    var dia;
    var fechamin;
    var fechamax;
    var frommin;
    var frommax;

    var plantID = $("#selectPlant").val();
    if (plantID == 'All') {
        plantID = '';
    }
    var statusConfirmID = $("#selectStatusConfirm").val();
    if (statusConfirmID == 'All') {
        statusConfirmID = 0;
    }
    var opt = "lis";
    var campaignID = localStorage.campaignID;
    var sUrlApi = "/PO/GetPOConfirm?opt=" + opt + "&plantID=" + plantID + "&weekProduction=" + sDepartureWeekSelected + "&weekProductionEnd=" + sDepartureWeekEnd + "&statusConfirmID=" + statusConfirmID +
        "&campaignID=" + campaignID + "&userID=" + sessionUserID;
    var html = '';
    var bRsl = false;
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: sUrlApi,
        async: false,
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'Nothing to show according to filters. Try again.');
                } else {
                    $.each(dataJson, function (i, e) {

                        if (e.statusConfirmID == 1) {
                            html += '<tr  class="table-warning">';
                            color = 'badge-warning';
                        }
                        if (e.statusConfirmID == 2) {
                            html += '<tr  class="table-info">';
                            color = 'badge-info';
                        }
                        if (e.statusConfirmID == 3) {
                            html += '<tr  class="table-primary">';
                            color = 'badge-primary';
                        }
                        if (e.statusConfirmID == 4) {
                            html += '<tr  class="table-danger">';
                            color = 'badge-danger';
                        }
                        if (e.statusConfirmID == 5) {
                            html += '<tr  class="table-success">';
                            color = 'badge-success';
                        }
                        if (e.statusConfirmID == 7) {
                            html += '<tr  class="table-secondary">';
                            color = 'badge-secondary';
                        }
                        if (e.statusConfirmID == 8) {
                            html += '<tr  class="table-secondary ">';
                            color = 'badge-secondary';
                        }
                        html += '<td style="display:none" id="orderProductionID">' + e.orderProductionID + '</td>';
                        html += '<td style="text-align: center; font-size:12px; max-width:100px;min-width:100px; display:none">' + e.packingList + '</td>';
                        if (e.statusConfirmID == 4 || e.statusConfirmID == 8) {
                            html += '<td class="sticky" ></td>';
                        } else {
                            html += '<td class="sticky" style="text-align:center;font-size:12px;min-width:350px!important;max-width:350px!important">';
                            html += '<button id="btnEdit" class="btn btn-sm btn-secondary" style="border:none;outline:none!important; background-color: #142694" onclick="EditData(this)"><i class="fas fa-edit fa-sm"></i> Edit</button>';
                            html += '<button id="btnExit" class="btn btn-sm btn-dark" style="border:none!important;outline:none!important; background-color: #58E53F" onclick="Exit(this)" hidden><i class="fa fa-sign-out fa-sm"></i> Exits</button>';
                            html += '<button id="btnReject" class="btn btn-sm btn-danger" style="border:none;outline:none!important" onclick="RejectPO(this)" hidden><i class="fa fa-times-circle fa-sm"></i> Reject</button>';
                          html += '<button id="btnChange" class="btn btn-sm btn-secondary" style="border:none;outline:none!important" onclick="ChangePO(this)" hidden><i class="fas fa-exchange-alt fa-sm"></i> Change Plant</button>';
                          html += '<button id="btnModify" class="btn btn-sm btn-info" style="border:none;outline:none!important;background-color: purple; color:white;" onclick="ModifyData(this)" hidden><i class="fa fa-edit fa-sm"></i> Modify</button>';
                            html += '<button id="btnAccept" class="btn btn-sm btn-info" style="border:none;outline:none!important" onclick="AcceptPO(this)" hidden><i class="fa fa-check fa-sm"></i> Accept</button>';
                            html += '<button id="btnConfirm" class="btn btn-sm btn-primary" style="border:none;outline:none!important" onclick="ConfirmPO(this)" hidden><i class="fa fa-check fa-sm"></i> Confirm</button>';
                            html += '<button id="btnRequestLoading" class="btn btn-sm btn-success" style="border:none;outline:none!important" onclick="RequestLoadingPO(this)" hidden><i class="fa fa-check-circle fa-sm"></i> Request Loading</button>';
                          html += '<button id="btnModifyPacking" class="btn btn-sm btn-success" style="border:none;outline:none!important" onclick="ModifyPackingAfterRequest(this)" hidden><i class="fa fa-check-circle fa-sm"></i> Modify Packing</button>';
                          html += '<button id="btnViewClientSpecification01" class="btn btn-sm btn-success" style="border:none;outline:none!important; background-color: #709414" data-toggle="tooltip" title="Client Specs"><i class="fa fa-search fa-sm"></i> Specs</button>';
                            //html += '<button id="btnViewClientSpecification01" class="btn btn-sm btn-success" style="border:none;outline:none!important; background-color: #709414" data-toggle="tooltip" title="Client Specs">';
                            //html += '<i class="fas fa-search fa-sm"></i><span>Specs</span>';
                            //html += '</button > ';
                            html += '</td>';
                        }
                        html += '<td class="sticky2" style="text-align: center; font-size:12px; max-width:120px;min-width:120px" id="tdOpCode">' + e.orderProduction + '</td>';
                        html += '<td class="sticky3" style="text-align: center; font-size:12px; max-width:95px;min-width:95px" id="dayproductionWeek">' + e.productionWeek + '</td>';
                        html += '<td class="sticky4" style="text-align: center; font-size:12px; max-width:90px;min-width:90px" id="daydepartureWeek">' + e.departureWeek + '</td>';
                        aCompletedDateTime = [];
                        _completedDate = e.completedDate;
                        _loadingDate = e.loadingDate;
                        sShipmentDate = '';
                        dia = '';
                        fechamin = '';
                        fechamax = '';
                        frommin = e.dateboardMin.split("-");
                        frommax = e.dateboardMax.split("-");
                        fechamin = frommin[0] + "/" + frommin[1] + "/" + frommin[2];
                        fechamax = frommax[0] + "/" + frommax[1] + "/" + frommax[2];
                        if (_completedDate != null && _completedDate != "") {
                            aCompletedDateTime = e.completedDate.split(" ");
                            var aCompletedDate = aCompletedDateTime[0].split("/");
                            sShipmentDate = aCompletedDate[2] + '/' + aCompletedDate[1] + '/' + aCompletedDate[0];
                            dia = NameWeekDay(sShipmentDate);
                            _loadingDate = (_loadingDate == null) ? "" : _loadingDate;
                        }
                        else {
                            _completedDate = "";
                            _loadingDate = "";
                            sShipmentDate = "";
                            dia = "";
                        }
                        html += '<td class="" style="text-align: center; font-size:12px;display:none;max-width:100px;min-width:100px" id="txtPacking">' + e.packingID + '</td>';
                        html += '<td class=""  style="text-align: center; font-size:12px;max-width:200px;min-width:200px">';
                        html += '<select id ="selectPacking" class="form-control form-control-sm" multiple="multiple" style="font-size:12px!important">';
                        html += '</select>';
                        html += '</td>';
                        html += '<td style="text-align: center; font-size:12px; display:none" id="statusID">' + e.statusConfirmID + '</td>';
                        html += '<td style="font-size: 12px;max-width:100px;min-width:100px;text-align: center;" ><div id="status" class="badge ' + color + ' badge-pill">' + e.statusConfirm + '</div></td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="txtDateAux">' + sShipmentDate + ' ' + aCompletedDateTime[1] + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width:130px;min-width:130px">';
                        html += '<input type="text" id="txtMinMaxCompleteDate" class="form-control form-control-sm" value="' + fechamin + '-' + fechamax + '" style="display:none;">';
                        html += '<input type="text" id="txtMinMaxLoadingDate" class="form-control form-control-sm" value="' + e.minForLoadingDate + '-' + e.maxForLoadingDate + '" style="display:none;">';
                        html += '<input type="text" id="txtCompleteDate" class="form-control form-control-sm" onchange="VerifyCompletedDay(this)" disabled value="' + _completedDate + '" />';
                        html += '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width:130px;min-width:130px">';
                        html += '<input type="text" id="txtLoadingDate" class="form-control form-control-sm" onchange="VerifyLoadingDate(this)" disabled value="' + _loadingDate + '"/>';
                        html += '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width:80px;min-width:80px">';
                        html += '<input type="number" min="0" id="txtVGM" class="form-control form-control-sm" onchange="(this)" disabled value="' + e.vgm + '"/></td>';
                        html += '<td style="text-align: center; font-size:12px;max-width:180px;min-width:180px;" ><textarea class="form-control form-control-sm" rows="1" id="comments" readonly>' + e.commentaryForConfirm + '</textarea></td>';

                        if (e.statusModified == false) {
                            color2 = 'badge-rejectedPO';
                            html += '<td style="font-size: 12px;max-width:90px;min-width:90px;text-align: center;" ><div id="status" class="badge ' + color2 + ' badge-pill"> ' + e.nameStatusModified + ' </div></td>';
                            html += '<td style="text-align: center; font-size:12px;max-width:170px;min-width:170px;" ><textarea class="form-control form-control-sm" rows="1" id="commentsReject" readonly>' + e.commentaryRejection + '</textarea></td>';
                        }
                        else if (e.statusModified == true) {
                            color2 = 'badge-successPO';
                            html += '<td style="font-size: 12px;max-width:90px;min-width:90px;text-align: center;" ><div id="status" class="badge ' + color2 + ' badge-pill">' + e.nameStatusModified + ' </div></td>';
                            html += '<td style="text-align: center; font-size:12px;max-width:170px;min-width:170px;" ><textarea class="form-control form-control-sm" rows="1" id="commentsReject" readonly>' + e.commentaryRejection + '</textarea></td>';
                        } else if (e.statusModified == null) {
                            html += '<td style="font-size: 12px;max-width:90px;min-width:90px;text-align: center;" ><div id="status"></div>' + e.nameStatusModified + '</td>';
                            html += '<td style="text-align: center; font-size:12px;max-width:170px;min-width:170px;" ><textarea class="form-control form-control-sm" rows="1" id="commentsReject" readonly> </textarea></td>';
                        }

                        html += '<td style="text-align: center; font-size:12px;display:none" id="txtGrowerId">' + e.growerID + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="txtfarmID">' + jQuery.trim(e.farmID) + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width:150px;min-width:150px;display:none" id="Farm">';
                        html += '<select id ="selectFarm" class="form-control selectpicker  form-control-sm " data-live-search="true" disabled>';
                        html += '</select>';
                        html += '</td>';
                        html += '<td style="text-align: center; font-size:12px" id="day">' + dia + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px;" hidden>' + e.grower + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px; display:none">' + e.market + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px; display:none " id="tdDestination">' + e.destination + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none" id="tdviaID">' + e.viaID + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none" id="tdmarketID">' + e.marketID + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none">' + e.via + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px;display:none">' + e.priority + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px;display:none" id="tdCustomer">' + e.customer + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 200px; min-width: 200px;display:none">' + e.format + '</td>';
                        html += '<td style="text-align: center; font-size:12px; max-width: 100px; min-width: 100px;display:none">' + e.brand + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px;display:none"><textarea class="form-control form-control-sm" rows="1" id="variety" readonly>' + e.variety + '</textarea></td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none"><textarea class="form-control form-control-sm" rows="1" id="size" readonly>' + e.size + '</textarea></td>';
                        html += '<td style="text-align: center; font-size:12px; display:none">' + e.pallets + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none">' + e.totalBoxes + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none">' + e.pallets * e.totalBoxes + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none">' + e.totalClamshell + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none">' + e.weightPerBox + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 100px; min-width: 100px;display:none">' + e.totalWeight + '</td>';
                        html += '<td style="text-align: center; font-size:12px;max-width: 150px; min-width: 150px;display:none"><textarea class="form-control form-control-sm" rows="1" id="comments" readonly>' + e.commentaryPO + '</textarea></td>';
                        html += '<td style="text-align: center; font-size:12px;display:none">' + e.dateConfirm + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdNroAttemptEditPacking">' + e.nroAttemptEditPacking + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdTotAttemptEditPacking">' + e.totAttemptEditPacking + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdStatusIE">' + e.statusWithIE + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdConsignee">' + e.consignee + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdvalVariety">' + e.valVariety + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdvalSize">' + e.valSize + '</td>';
                        html += '<td style="text-align: center; font-size:12px;display:none" id="tdcustomerID">' + e.customerID + '</td>';
                        html += '<td style="text-align: center;"><button class="" style="padding: 0 0 0 0!important;background:none;border:none;outline:none !important;max-width: 50px; min-width: 50px;" onclick="generateFilePdfGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf text-danger fa-md"></i></button></td>';
                        html += '<td style="text-align: center;"><button class="" style="padding: 0 0 0 0!important;background:none;border:none;outline:none !important;max-width: 50px; min-width: 50px;" onclick="generateFileSIGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                        html += '<td style="text-align: center; display:none"><button class="" style="padding: 0 0 0 0!important;background:none;border:none;outline:none !important;max-width: 50px; min-width: 50px;" onclick="generateFileSpecs(' + e.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                        html += '<td style="text-align: center; display:none"><button class="" style="padding: 0 0 0 0!important;background:none;border:none;outline:none !important;max-width: 50px; min-width: 50px;" onclick="generateFileBillingGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                        html += '<td style="text-align: center;"><button class="" style="padding: 0 0 0 0!important;background:none;border:none;outline:none !important;max-width: 50px; min-width: 50px;" onclick="generatePdf(' + e.shippingLoadingID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                        html += '</tr>';
                    });	//End each
                    $("table#tableList").DataTable().destroy();
                    bRsl = true;                                            
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {                                
                $("table#tableList>tbody").empty().append(html);
                $('#tableList>tbody>tr').each(function (_i, _e) {
                    var _GrowerId = $(_e).find('#txtGrowerId').text();
                    var _farmID = $(_e).find('#txtfarmID').text();
                    var _aPackingID = $(_e).find('#txtPacking').text().split(",");
                    var _aPacking = [];
                    var _aSelPack = '';
                    if (_aPackingID.length > 0) {
                        $.each(_aPackingID, function (x, evt) {
                            _aPacking = _aPackingID[x].split("-");
                            $(_e).find('#selectPacking').append($('<option></option>').attr('value', jQuery.trim(_aPacking[1])).text(_aPacking[2]));
                            if (_aPacking[0] == '1') _aSelPack += _aPacking[1] + ',';
                        });
                        SettingMultiselect($(_e).find('#selectPacking'));
                        $(_e).find('#selectPacking').multiselect('select', _aSelPack.split(','));
                    }
                });
                let aMinMaxCompleteDate = [];
                let aMinMaxLoadingDate = [];
                $('#tableList>tbody>tr').each(function (i, e) {
                    aMinMaxCompleteDate = $(e).find('td').find('#txtMinMaxCompleteDate').val().split("-");
                    aMinMaxLoadingDate = $(e).find('td').find('#txtMinMaxLoadingDate').val().split("-");
                    $(e).find('td').find('#txtCompleteDate').datetimepicker({
                        format: 'd/m/Y H:i',
                        closeOnDateSelect: true,
                        //minDate: aMinMaxCompleteDate[0],
                        //maxDate: aMinMaxCompleteDate[1]
                    });
                    $(e).find('td').find('#txtLoadingDate').datetimepicker({
                        format: 'd/m/Y H:i',
                        closeOnDateSelect: true,
                        //minDate: aMinMaxLoadingDate[0],
                        //maxDate: aMinMaxLoadingDate[1],
                    });
                    $(e).find('td').find('#btnEdit').removeAttr('hidden');
                    $(e).find('td').find('#btnExit').prop('hidden', true);
                    $(e).find('td').find('#btnAccept').prop('hidden', true);
                    $(e).find('td').find('#btnConfirm').prop('hidden', true);
                    $(e).find('td').find('#btnReject').prop('hidden', true);
                    $(e).find('td').find('#btnChange').prop('hidden', true);
                    $(e).find('td').find('#btnRequestLoading').prop('hidden', true);
                });	//End Each
                $("#tableList").DataTable({
                    "paging": true,
                    "ordering": false,
                    "info": true,
                    "responsive": true,
                    lengthMenu: [
                        [10, 25, 50, -1],
                        ['10 rows', '25 rows', '50 rows', 'All']
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
                            exportOptions: {
                                columns: [3, 4, 5, 7, 8, 9, 11, 14, 15, 17, 18, 19, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 39]
                            }
                        },
                        'pageLength'
                    ],
                });
            }
        }
    });
    GetSummary();
            
}

function SettingMultiselect(obj) {
    var orderCount = 0;
    $(obj).multiselect({
        buttonWidth: '200px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select expertise!',
        maxHeight: '1000px',
        dropUp: true,
        enableFiltering: true,
        onChange: function (option, checked) {
            if (checked) {
                orderCount++;
                $(option).data('order', orderCount);
            }
            else {
                $(option).data('order', '');
            }
        },
        buttonText: function (options) {
            if (options.length === 0) {
                return 'None selected';
            }
            else if (options.length > 3) {
                return options.length + ' selected';
            }
            else {
                var selected = [];
                options.each(function () {
                    selected.push([$(this).text(), $(this).data('order')]);
                });

                selected.sort(function (a, b) {
                    return a[1] - b[1];
                });

                var text = '';
                for (var i = 0; i < selected.length; i++) {
                    text += selected[i][0] + ', ';
                }

                return text.substr(0, text.length - 2);
            }
        },
    });
}

function openModal(poID) {
    $(btnDecline).removeAttr('disabled');
    $(btnConfirm).removeAttr('disabled');
    var cabPO = [];
    var opt = '';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/GetConfirmById?opt=" + opt + "&orderProductionID=" + poID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                cabPO = JSON.parse(data)[0];
                $(lblPO).text(cabPO.requestNumber)
                $(lblId).text(cabPO.orderProductionID)
                $(lblFarm).text(cabPO.farm)
                $(lblDestination).text(cabPO.destination)
                $(lblWeek).text(cabPO.week)
                $(lblConsignee).text(cabPO.consignee)
                $(lblIncoterm).text(cabPO.incotem)
                $(lblNotify).text(cabPO.notify)
                $(lblVia).text(cabPO.via)
                if (cabPO.dateboard == '1900-01-01') {
                    $(txtCompleteDate).val('');
                    //$(txtCompleteDate).attr({
                    //	"min": cabPO.dateboardMin,
                    //	"max": cabPO.dateboardMax
                    //});
                }
                if (cabPO.statusConfirm === 'PE') {

                } else {
                    $(btnDecline).attr('disabled', true);
                    $(btnConfirm).attr('disabled', true);
                }
            }
        }
    });

    $.get("GetDetailConfirmByIdMC?Option=" + Option + "&IdPO=" + poID, function (data) {
        $('#tblDetailOP>tbody').html('');
        if (data.length > 0) {
            dataJson = JSON.parse(data);
            $.each(dataJson, function (i, e) {
                if (e.pallets.toFixed(2) == 0) { e.pallets = 0.01 }
                if (e.palletsConfirm.toFixed(2) == 0) { e.palletsConfirm = 0.01 }
                min = 0
                max = 20
                if (e.palletsConfirm.toFixed(2) < 2) {
                    min = 0
                }
                else {
                    min = parseInt(e.palletsConfirm.toFixed(2)) - 2;
                }
                max = parseInt(e.palletsConfirm.toFixed(2)) + 2;
                if (max > 20) { max = 20 }

                var tr = '<tr>';
                tr += '<td style="display:none">' + e.orderProductionDetailID + '</td>';
                tr += '<td>' + e.brand + '</td>';
                tr += '<td>' + e.varierty + '</td>';
                tr += '<td>' + e.size + '</td>';
                tr += '<td>' + e.presentation + '</td>';
                tr += '<td style="text-align: right">' + e.weightSubTotal.toFixed(2) + '</td>';
                tr += '<td style="text-align: right" class="txtPallets">' + e.pallets.toFixed(2) + '</td>';
                tr += '<td><input class="txtPalletsConfirm" type="number" min=' + min + ' max=' + max + ' style="text-align: right;min-width: 100%;" value="' + e.palletsConfirm.toFixed(2) + '" max="20" min="0"/></td>'// + e.palletsConfirm + '</td>';
                tr += '<td style="text-align: right">' + e.weightSubTotalConfirm.toFixed(2) + '</td>';
                tr += '<td style="text-align: right">' + e.weightxPallet.toFixed(2) + '</td>';
                tr += '<td style="text-align: right">' + e.boxConfirm.toFixed(2) + '</td>';
                tr += '<td class="boxPallet" style="font-size: 12px;display:none">' + e.boxesPerPallet + '</td>';
                tr += '</tr>';
                $('table#tblDetailOP>tbody').append(tr);

            })
        }
        sumPalletsConfirm();
        sumPallets();
        $tdSumPalletsConfirm = parseFloat($('.tdSumPalletsConfirm').text())
        $tdSumPalletsConfirm = Math.floor($tdSumPalletsConfirm).toFixed(2)
        $('.tdSumPalletsConfirm').text($tdSumPalletsConfirm)

        $tdSumPallets = parseFloat($('.tdSumPallets ').text())
        $tdSumPallets = Math.floor($tdSumPallets).toFixed(2)
        $('.tdSumPallets ').text($tdSumPallets)
    })

}

function calculatePallets() {
    var boxPallet = $("#txtBoxPallet").val();
    var boxes = $("#txtBoxes").val();
    var pallets = (boxes / boxPallet);
    $("#txtPallets").val(pallets);
}

function sumPalletsConfirm() {
    var pallets = 0;
    $('input.txtPalletsConfirm').each(function (i, e) {
        pallets += parseFloat($(e).val());
    })
    $('.tdSumPalletsConfirm').text(parseFloat(pallets).toFixed(2))
}

function sumPallets() {
    var pallets = 0;
    $('.txtPallets').each(function (i, e) {
        pallets += parseFloat($(e).text());
    })
    $('.tdSumPallets').text(parseFloat(pallets).toFixed(2))
}


function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
    $('#' + control).selectpicker('refresh');
}

function ModifyData(xthis) {
    Swal.fire({
        imageUrl: '../images/AgricolaAndrea71x64.png',
        title: 'AGROCOM',
        html: 'Loading information...',
        onBeforeOpen() {
            Swal.showLoading();
        },
        onOpen() {
            GetDataToModify(xthis);
            Swal.hideLoading();
            Swal.close();
        },
        onAfterClose() {

        },
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showConfirmButton: false,
    });
}

function GetDataToModify(xthis) {
    let bRsl = false;
    let correlativeSaleOrder = $(xthis).closest('tr').find('#tdOpCode').text().trim();
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/VerifySaleOrderInProcess?correlativeSaleOrder=" + correlativeSaleOrder,
        async: false,
        success: function (data) {
            if (data == "true") {
                sweet_alert_info('Information', "Sale Order in process. Can't modify.");
                bRsl = true;
            }
        }
    });

    if (bRsl) return;
    $('#chkplu').prop('checked', false);
    $('#chkpadlock').prop('checked', false);
    trRowModify = $(xthis).closest("tr").index();
    iOrderProdId = $(xthis).closest('tr').find('#orderProductionID').text();
    $('#txtPO').val($(xthis).closest("tr").find('#tdOpCode').text());
    $('#txtCustomer').val($(xthis).closest("tr").find('#tdCustomer').text());
    $('#txtDepartureWeek').val($(xthis).closest("tr").find('#daydepartureWeek').text());
    $('#txtDestination').val($(xthis).closest("tr").find('#tdDestination').text());
    $('#txtConsignee').val($(xthis).closest("tr").find('#tdConsignee').text());
    $("#tblPOdetail>tbody").empty();
    $('.totalPallets').html('0.00');

    _viaID = $(xthis).closest("tr").find('#tdviaID').text()
    _marketID = $(xthis).closest("tr").find('#tdmarketID').text()

    $('#btnShowModalForCodePack').click();
    let arrayCodePack;
    let codepacksToShow = '';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/GetForPdfMC?IdPO=" + iOrderProdId,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                if (dataJson.length > 0) {
                    let _html = '';
                    $('table#tblDetailOPactual>tbody').empty();
                    $.each(dataJson, function (i, e) {
                        _html += '<tr style="">';
                        _html += '<td style="max-width: 120px; min-width: 150px;text-align: center; font-size:12px;">' + e.variety + '</td>';
                        _html += '<td style="max-width: 80px; min-width: 80px;text-align: center; font-size:12px;">' + e.typeofbox + '</td>';
                        _html += '<td style="max-width: 80px; min-width: 80px;text-align: center; font-size:12px;">' + e.category + '</td>';
                        _html += '<td style="max-width: 80px; min-width: 80px;text-align: center; font-size:12px;">' + e.boxNetWeight + '</td>';
                        _html += '<td style="max-width: 80px; min-width: 80px;text-align: center; font-size:12px;">' + e.brand + '</td>';
                        if (localStorage.cropID == 'BLU') {
                            _html += '<td style="text-align: center; display:none; font-size:12px;">' + e.label + '</td>';
                            $('#tdLabel').attr('style', 'display:none');
                        } else {
                            _html += '<td style="max-width: 80px; min-width: 80px;text-align: center; font-size:12px;">' + e.label + '</td>';

                        }
                        arrayCodePack = e.codepack.split(',');
                        codepacksToShow = '';
                        $.each(arrayCodePack, function (iRowCode, oCode) {
                            codepacksToShow = oCode + ',' + codepacksToShow;
                        });
                        localStorage.codepacksToStore = codepacksToShow;
                        _html += '<td>';
                        _html += '<textarea class="form-control form-control-sm" rows="1" id="comments" readonly>' + codepacksToShow + '</textarea>';
                        _html += '</td>';
                        _html += '<td style="max-width: 50px; min-width: 50px;text-align: center; font-size:12px;">' + e.kgsnet + '</td>';
                        _html += '<td style="max-width: 50px; min-width: 50px;text-align: center; font-size:12px;">' + e.boxes + '</td>';
                        _html += '</tr>';
                    });
                    $('table#tblDetailOPactual>tbody').append(_html);
                }
            }
        }
    });

    $('#modalOPdetail').modal({
        backdrop: 'static',
        keyboard: false
    });
}

function createCodePack() {
    if ($("#selectVariety").val() == null) {
        return;
    } if ($("#selectWeightBox").val() == null) {
        return;
    } if ($("#selectTypeBox").val() == null) {
        return;
    }
    if ($("#selectBrand").val() == null) {
        return;
    } if ($("#selectLabel").val() == null) {
        return;
    } if ($("#selectPresentation").val() == null) {
        return;
    }

    var varietyID = $("#selectVariety option:selected").val();
    var formatID = $("#selectWeightBox option:selected").val();
    var typeBoxID = $("#selectTypeBox option:selected").val();
    var brandID = $("#selectBrand option:selected").val();
    var presentationID = $("#selectPresentation option:selected").val();
    var labelID = $("#selectLabel option:selected").val();

    var labelCodeP = 'G';
    var varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
    var formatCode = dataFormat.filter(item => item.id == formatID)[0].abreviation
    var typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation
    var brandCode = dataBrand.filter(item => item.id == brandID)[0].abbreviation
    var presentationCode = dataPresentation.filter(item => item.id == presentationID)[0].presentation
    var labelCode = dataLabel.filter(item => item.id == labelID)[0].abbreviation

    if ($('#chkplu').is(":checked")) {
        labelCodeP = 'P';
    }

    switch (_cropID.toLowerCase()) {
        case 'cit':
            var codePack = varietyCode.trim() + '_' + formatCode + typeBoxCode.slice(0, 1) + brandCode.trim() + '_' + (typeBoxCode.slice(0, 1) + brandCode.trim() + labelCodeP).slice(0, 3);
            break;
        default:
            var codePack = varietyCode.trim() + '_' + formatCode + typeBoxCode.slice(0, 1) + brandCode.trim() + '_ ' + (presentationCode + labelCode).slice(0, 3);
    }

    $('#txtCodePack').val(codePack);
}

function loadDataBrandByCategory(categoryID) {
    var opt = 'cat';
    var cropID = localStorage.cropID

    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/BrandGetByCateID?opt=" + opt + "&cropID=" + categoryID,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataBrand = dataJson.map(
                    obj => {
                        return {
                            "id": obj.brandID,
                            "name": obj.brand,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

    var opt = 'lbl';
    $.ajax({
        type: "GET",
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/GetLabelByCropIDMC?opt=" + opt + "&cropID=" + cropID,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataLabel = dataJson.map(
                    obj => {
                        return {
                            "id": obj.labelID,
                            "name": obj.label,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

}

function getBoxPerPalletContainer(format, typeBox) {
    var opt = 'pcr';
    var formatID = format;
    var packageProductID = typeBox;
    $.ajax({
        type: 'POST',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/CodepackByFormatIDPackageProductIDMC?opt=" + opt + "&viaid=" + formatID + "&cropID=" + packageProductID,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataCodePack2 = dataJson.map(
                    obj => {
                        return {
                            "id": obj.codePackID,
                            "name": obj.codePack,
                            "boxPerContainer": obj.boxPerContainer,
                            "boxesPerPallet": obj.boxesPerPallet
                        }
                    }
                );
            }
        }
    });
}

function loadComboSizeBox(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='0'>" + textFirstElement + "</option>";
    }
    $('#' + control).empty().append(content);
}

function addOPdetailBLU() {
    sweet_alert_progressbar();
    let bRslt = false;
    let TotPallet = 0;

    if ($("#_selectSize option:selected").val() == 0) {
        sweet_alert_warning('Error!', 'Choose size box')
        return false;
    }
    $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
        let objPallets = $(objRow).find('#tdPallets');
        TotPallet += parseFloat((objPallets.text() == "") ? 0 : objPallets.text());

    });
    TotPallet += parseFloat($(_txtPallets).val());
    TotPallet = parseFloat(TotPallet.toFixed(2));
    dMonTotPallet = TotPallet;

    if (TotPallet > 21 && _viaID != 2) {
        sweet_alert_error('Error', 'Total pallets must be 20. Can not enter more pallets');
        return bRslt;
    }

    if ($("#_txtBoxes").val() == null || parseInt($("#_txtBoxes").val()) == 0) {
        sweet_alert_error('Error', 'Enter boxes')
        return bRslt;
    }

    if ($(_txtCodePack).val() == '') {
        sweet_alert_error('Error', 'Choose codepack')
        return bRslt;
    }

    let _objVariety;
    let _objCategory;
    let _objWeightBox;
    let _objTypeBox;
    let _objBrand;
    let _objPresentation;
    let _objLabel;
    let _objSizeBox;
    let _objBoxPerPallet;
    let _objBoxPerContainer;
    let _objBoxes;
    let _objPallets;
    let _objCodePack;

    try {
        _objVariety = $("#_selectVariety option:selected");
        _objWeightBox = $("#_selectFormat option:selected");
        _objTypeBox = $("#_selectTypeBox option:selected");
        _objBrand = $("#_selectBrand option:selected");
        _objPresentation = $("#_selectPresentation option:selected");
        _objSizeBox = $("#_selectSize option:selected");
        _objBoxPerPallet = $(_txtBoxPerPallet);
        _objBoxPerContainer = $(_txtBoxPerPerContainer);
        _objBoxes = $(_txtBoxes);
        _objPallets = $(_txtPallets);
        _objCodePack = localStorage.codepacksToStore;

        //var dataFilter = _dataCodepack.filter(item => item.viaID == _viaID && item.marketID == _marketID && item.codePackID == _objWeightBox.val());

        var table = ''
        table += '<tr style="text-align:center ">';
        table += '<td style="background-color:#F9E79F;min-width:80px!important;max-width: 80px!important;" class="tdBtn" >';
        table += '<button id="btnRowEdit" onclick="EditOPdetail(this)" class="btn btn-primary btn-sm " style="font-size:12px"><i class="fas fa-edit"></i></button>';
        table += '<button onclick="RemoveOPdetail(this)" style="font-size:12px" class="btn btn-danger btn-sm "><i class="fas fa-remove"></i></button>';
        table += '</td> ';      //00
        table += '<td style="display:none" class="tdPlanCustomerVarietyID">0</td>';     //01
        table += '<td style="display:none" class="tdVarietyID">' + $('#_selectVariety').val() + '</td>';        //02
        table += '<td style="min-width:300px!important;max-width: 300px!important;" id="tdVariety">' + _objVariety.text() + '</td>';        //03
        table += '<td style="display:none" class="tdCategoryID">' + _objBrand.val() + '</td>';      //04
        table += '<td id="tdCategory"  style="min-width:40px!important;max-width: 40px!important;">' + _objBrand.text() + '</td>';      //05
        table += '<td style="display:none" class="tdWeightBoxID">' + _objWeightBox.val() + '</td>';     //06
        table += '<td id="tdWeightBox"  style="min-width:80px!important;max-width: 80px!important;">' + _objWeightBox.text() + '</td>';     //07
        table += '<td style="display:none" class="tdTypeBoxID">' + _objTypeBox.val() + '</td>';     //08
        table += '<td id="tdTypeBox" style="min-width:80px!important;max-width: 80px!important;">' + _objTypeBox.text() + '</td>';      //09
        table += '<td style="display:none" class="tdBrandID">' + "" + '</td>';      //10
        table += '<td id="tdBrand" style="min-width:80px!important;max-width: 80px!important;">' + _objBrand.text() + '</td>';      //11
        table += '<td style="display:none" class="tdPresentationID">' + _objPresentation.val() + '</td>';       //12
        table += '<td id="tdPresentation" style="min-width:80px!important;max-width: 80px!important;">' + _objPresentation.text() + '</td>';        //13
        table += '<td style="display:none" class="tdLabelID">' + "" + '</td>';      //14
        if (localStorage.cropID.toLowerCase() == 'blu') {
            table += '<td style="text-align: center; display:none; font-size:12px;" id="tdLabel">' + '' + '</td>';
        } else {
            table += '<td id="tdLabel">' + "" + '</td>';                         
        }       //15
        table += '<td style="display:none" class="tdSizeBoxID">' + $('#_selectSize option:selected').val() + '</td>';       //16
        table += '<td id="tdSizeBox">' + _objSizeBox.text() + '</td>';      //17
        table += '<td id="tdCodePack" style="min-width:500px!important;max-width: 500px!important;">' + _objCodePack + '</td>';     //18
        table += '<td id="tdBoxPerPallet">' + _objBoxPerPallet.val() + '</td>';     //19
        table += '<td id="tdBoxPerContainer">' + _objBoxPerContainer.val() + '</td>';       //20
        table += '<td id="tdBoxes">' + _objBoxes.val() + '</td>';       //21
        table += '<td style="display:none" id="tdPriceBox">' + 0 + '</td>';     //22
        if ($('#chkWorkOrder').is(':checked')) {
            table += '<td class="tdWorkOrder">' + 'YES' + '</td>';
        } else {
            table += '<td class="tdWorkOrder">' + 'NO' + '</td>';
        }       //23
        table += '<td id="tdPallets">' + _objPallets.val() + '</td>';       //24
        table += '</tr>'

        /*
        if ($.fn.DataTable.isDataTable('table#tblPOdetail')) {
            if ($("table#tblPOdetail").dataTable().length == 0) {
                $("table#tblPOdetail>tbody").empty();
            }
            $("table#tblPOdetail>tbody").append(table);

        } else {
            $("table#tblPOdetail").DataTable({
                destroy: true,
                "ordering": false,
                "info": false,
                "responsive": true,
                "paging": true,

            });
            if (($("table#tblPOdetail").dataTable().fnSettings().aoData.length) == 0) {
                $("table#tblPOdetail>tbody").empty();
            }
            $("table#tblPOdetail>tbody").append(table);
        }
        */
        $("table#tblPOdetail>tbody").append(table);

        bRslt = true;
        $('table#tblPOdetail>tfoot').find('tr').find('#totalPallets').text(TotPallet);
        if (TotPallet == 20) {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
        } else {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
        }
    }
    catch (error) {

    }
    return bRslt;
    sweet_alert_progressbar_cerrar();
}

function addOPdetail() {
    sweet_alert_progressbar();
    let bRslt = false;
    let TotPallet = 0;

    $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
        let objPallets = $(objRow).find('#tdPallets');
        TotPallet += parseFloat((objPallets.text() == "") ? 0 : objPallets.text());
    });

    TotPallet += parseFloat($(txtPallets).val());
    TotPallet = parseFloat(TotPallet.toFixed(2));
    dMonTotPallet = TotPallet;

    switch (_cropID.toLowerCase()) {
        case 'cit':
            if (TotPallet > 21) {
                sweet_alert_error('Error', 'Total pallets must be 21. Can not enter more pallets');
                return bRslt;
            }
            break;
        default:
            if (TotPallet > 20) {
                sweet_alert_error('Error', 'Total pallets must be 20. Can not enter more pallets');
                return bRslt;
            }
    }

    if ($("#SelectSizeBox").val() == null) {
        sweet_alert_error('Error', 'Choose size box')
        return bRslt;
    }
    if ($("#txtBoxes").val() == null || parseInt($("#txtBoxes").val()) == 0) {
        sweet_alert_error('Error', 'Enter boxes')
        return bRslt;
    }
    if ($(txtCodePack).val() == '') {
        sweet_alert_error('Error', 'Choose codepack')
        return bRslt;
    }

    let _objVariety;
    let _objCategory;
    let _objWeightBox;
    let _objTypeBox;
    let _objBrand;
    let _objPresentation;
    let _objLabel;
    let _objSizeBox;
    let _objBoxPerPallet;
    let _objBoxPerContainer;
    let _objBoxes;
    let _objPallets;
    let _objCodePack;

    try {
        _objVariety = $("#selectVariety option:selected");
        _objCategory = $("#selectCategory option:selected");
        _objWeightBox = $("#selectWeightBox option:selected");
        _objTypeBox = $("#selectTypeBox option:selected");
        _objBrand = $("#selectBrand option:selected");
        _objPresentation = $("#selectPresentation option:selected");
        _objLabel = $("#selectLabel option:selected");
        _objSizeBox = $("#SelectSizeBox option:selected");
        _objBoxPerPallet = $(txtBoxPerPallet);
        _objBoxPerContainer = $(txtBoxPerContainer);
        _objBoxes = $(txtBoxes);
        _objPallets = $(txtPallets);
        _objCodePack = $(txtCodePack);

        var table = ''
        table += '<tr style="text-align:center ">';
        table += '<td style="background-color:#F9E79F; max-width: 80px; min-width:80px" class="tdBtn" >';
        table += '<button id="btnRowEdit" onclick="EditOPdetail(this)" class="btn btn-primary btn-sm " style="font-size:12px"><i class="fas fa-edit"></i></button>';
        table += '<button onclick="RemoveOPdetail(this)" style="font-size:12px" class="btn btn-danger btn-sm "><i class="fas fa-remove"></i></button>';
        table += '</td> ';      //00
        table += '<td style="display:none" class="tdPlanCustomerVarietyID">0</td>';     //01
        table += '<td style="display:none" class="tdVarietyID">' + _objVariety.val() + '</td>';     //02
        table += '<td id="tdVariety">' + _objVariety.text() + '</td>';      //03
        table += '<td style="display:none" class="tdCategoryID">' + _objCategory.val() + '</td>';       //04
        table += '<td id="tdCategory">' + _objCategory.text() + '</td>';        //05
        table += '<td style="display:none" class="tdWeightBoxID">' + _objWeightBox.val() + '</td>';     //06
        table += '<td id="tdWeightBox">' + _objWeightBox.text() + '</td>';      //07
        table += '<td style="display:none" class="tdTypeBoxID">' + _objTypeBox.val() + '</td>';     //08
        table += '<td id="tdTypeBox">' + _objTypeBox.text() + '</td>';      //09
        table += '<td style="display:none" class="tdBrandID">' + _objBrand.val() + '</td>';     //10
        table += '<td id="tdBrand">' + _objBrand.text() + '</td>';      //11
        table += '<td style="display:none" class="tdPresentationID">' + _objPresentation.val() + '</td>';       //12
        table += '<td id="tdPresentation">' + _objPresentation.text() + '</td>';        //13
        table += '<td style="display:none" class="tdLabelID">' + _objLabel.val() + '</td>';     //14
        table += '<td id="tdLabel">' + _objLabel.text() + '</td>';      //15
        table += '<td style="display:none" class="tdSizeBoxID">' + _objSizeBox.val() + '</td>';     //16
        table += '<td id="tdSizeBox">' + _objSizeBox.text() + '</td>';      //17		
        table += '<td id="tdCodePack">' + _objCodePack.val() + '</td>';     //18
        table += '<td id="tdBoxPerPallet">' + _objBoxPerPallet.val() + '</td>';     //19
        table += '<td id="tdBoxPerContainer">' + _objBoxPerContainer.val() + '</td>';       //20
        table += '<td id="tdBoxes">' + _objBoxes.val() + '</td>';       //21
        table += '<td id="tdPallets">' + _objPallets.val() + '</td>';       //22
        if ($('#chkWorkOrder').is(':checked')) {
            table += '<td id="tdWorkOrder">' + 'YES' + '</td>';
        } else {
            table += '<td id="tdWorkOrder">' + 'NO' + '</td>';
        }       //23
        table += '<td id="tdPallets">' + _objPallets.val() + '</td>';       //24
        table += '</tr>'

        if ($.fn.DataTable.isDataTable('table#tblPOdetail')) {
            if ($("table#tblPOdetail").dataTable().length == 0) {
                $("table#tblPOdetail>tbody").empty();
            }
            $("table#tblPOdetail>tbody").append(table);

        } else {
            $("table#tblPOdetail").DataTable({
                destroy: true,
                "ordering": false,
                "info": false,
                "responsive": true,
                "paging": true,
            });
            if (($("table#tblPOdetail").dataTable().fnSettings().aoData.length) == 0) {
                $("table#tblPOdetail>tbody").empty();
            }
            $("table#tblPOdetail>tbody").append(table);
        }

        bRslt = true;
        $('table#tblPOdetail>tfoot').find('tr').find('#totalPallets').text(TotPallet);
        $('#tblPOdetail .totalPallets').html(TotPallet);

        if (TotPallet == 21) {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
        } else if (TotPallet == 20) {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
        } else {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
        }

    } catch (error) {

    }
    return bRslt;
    sweet_alert_progressbar_cerrar();
}

function UpdateOPdetail() {
    var bRslt = false;
    let _objVariety;
    let _objCategory;
    let _objWeightBox;
    let _objTypeBox;
    let _objBrand;
    let _objPresentation;
    let _objLabel;
    let _objSizeBox;
    let _objBoxPerPallet;
    let _objBoxPerContainer;
    let _objBoxes;
    let _objPallets;
    let _objCodePack;

    try {

        switch (_cropID.toLowerCase()) {
            case 'blu':
                _objVariety = $("#_selectVariety option:selected");
                _objSizeBox = $("#_selectSize option:selected");
                _objCategory = $("#selectCategory option:selected");
                _objWeightBox = $("#_selectFormat option:selected");
                _objTypeBox = $("#_selectTypeBox option:selected");
                _objBrand = $("#_selectBrand option:selected");
                _objPresentation = $("#_selectPresentation option:selected");
                _objBoxPerPallet = $(_txtBoxPerPallet);
                _objBoxPerContainer = $(_txtBoxPerPerContainer);
                _objBoxes = $(_txtBoxes);
                _objPallets = $(_txtPallets);
                _objCodePack = localStorage.codepacksToStore;
                _pricebox = $(_txtPriceBox);

                var dataFilter = _dataCodepack.filter(item => item.viaID == _viaID && item.marketID == _marketID && item.codePackID == _objWeightBox.val());

                $(trRowEdit).find(".tdVarietyID").text($('#_selectVariety').val());
                $(trRowEdit).find("#tdVariety").text(_objVariety.text());
                $(trRowEdit).find(".tdCategoryID").text(_objBrand.val());
                $(trRowEdit).find("#tdCategory").text(_objBrand.text());
                $(trRowEdit).find(".tdWeightBoxID").text(_objWeightBox.val());
                $(trRowEdit).find("#tdWeightBoxID").text(_objWeightBox.text());
                $(trRowEdit).find(".tdTypeBoxID").text(_objTypeBox.val());
                $(trRowEdit).find("#tdTypeBox").text(_objTypeBox.text());
                $(trRowEdit).find(".tdBrandID").text("");
                $(trRowEdit).find("#tdBrand").text(_objBrand.text());
                $(trRowEdit).find(".tdPresentationID").text(_objPresentation.val());
                $(trRowEdit).find("#tdPresentation").text(_objPresentation.text());
                $(trRowEdit).find(".tdLabelID").text("");
                $(trRowEdit).find("#tdLabel").text("");
                $(trRowEdit).find(".tdSizeBoxID").text($('#_selectSize option:selected').val());
                $(trRowEdit).find("#tdSizeBox").text(_objSizeBox.text());
                $(trRowEdit).find("#tdBoxPerPallet").text(_objBoxPerPallet.val());
                $(trRowEdit).find("#tdBoxPerContainer").text(_objBoxPerContainer.val());
                $(trRowEdit).find("#tdBoxes").text(_objBoxes.val());
                $(trRowEdit).find("#tdPallets").text(_objPallets.val());
                $(trRowEdit).find("#tdCodePack").text(_objCodePack);                
                if ($('#chkWorkOrder').is(':checked')) {
                    $(trRowEdit).find('td.tdWorkOrder').text('YES');
                } else {
                    $(trRowEdit).find('td.tdWorkOrder').text('NO');
                }
                break;
            default:
                _objVariety = $("#selectVariety option:selected");
                _objCategory = $("#selectCategory option:selected");
                _objWeightBox = $("#selectWeightBox option:selected");
                _objTypeBox = $("#selectTypeBox option:selected");
                _objBrand = $("#selectBrand option:selected");
                _objPresentation = $("#selectPresentation option:selected");
                _objLabel = $("#selectLabel option:selected");
                _objSizeBox = $("#SelectSizeBox option:selected");
                _objBoxPerPallet = $(txtBoxPerPallet);
                _objBoxPerContainer = $(txtBoxPerContainer);
                _objBoxes = $(txtBoxes);
                _objPallets = $(txtPallets);
                _objCodePack = $(txtCodePack);

                $(trRowEdit).find(".tdVarietyID").text(_objVariety.val());
                $(trRowEdit).find("#tdVariety").text(_objVariety.text());
                $(trRowEdit).find(".tdCategoryID").text(_objCategory.val());
                $(trRowEdit).find("#tdCategory").text(_objCategory.text());
                $(trRowEdit).find(".tdWeightBoxID").text(_objWeightBox.val());
                $(trRowEdit).find("#tdWeightBoxID").text(_objWeightBox.text());
                $(trRowEdit).find(".tdTypeBoxID").text(_objTypeBox.val());
                $(trRowEdit).find("#tdTypeBox").text(_objTypeBox.text());
                $(trRowEdit).find(".tdBrandID").text(_objBrand.val());
                $(trRowEdit).find("#tdBrand").text(_objBrand.text());
                $(trRowEdit).find(".tdPresentationID").text(_objPresentation.val());
                $(trRowEdit).find("#tdPresentation").text(_objPresentation.text());
                $(trRowEdit).find(".tdLabelID").text(_objLabel.val());
                $(trRowEdit).find("#tdLabel").text(_objLabel.text());
                $(trRowEdit).find(".tdSizeBoxID").text(_objSizeBox.val());
                $(trRowEdit).find("#tdSizeBox").text(_objSizeBox.text());
                $(trRowEdit).find("#tdBoxPerPallet").text(_objBoxPerPallet.val());
                $(trRowEdit).find("#tdBoxPerContainer").text(_objBoxPerContainer.val());
                $(trRowEdit).find("#tdBoxes").text(_objBoxes.val());
                $(trRowEdit).find("#tdPallets").text(_objPallets.val());
                $(trRowEdit).find("#tdCodePack").text(_objCodePack.val());
                if ($('#chkWorkOrder').is(':checked')) {
                    $(trRowEdit).find('td.tdWorkOrder').text('YES');
                } else {
                    $(trRowEdit).find('td.tdWorkOrder').text('NO');
                }
        }       

        let TotPallet = 0;
        $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
            let objPallets = $(objRow).find('#tdPallets');
            TotPallet += parseFloat(objPallets.text());
        });
        TotPallet = parseFloat(TotPallet.toFixed(2));
        $('#tblPOdetail .totalPallets').html(TotPallet)
        dMonTotPallet = TotPallet;

        switch (_cropID.toLowerCase()) {
            case 'blu':
                if (TotPallet > 20 && _viaID != 2) {
                    sweet_alert_error('Error', 'Total pallets must be 20. Can not enter more pallets');
                    return bRslt;
                }
                break;
            case 'cit':
                if (TotPallet > 21) {
                    sweet_alert_error('Error', 'Total pallets must be 21. Can not enter more pallets');
                    return bRslt;
                }
                break;
            default:
                if (TotPallet > 20) {
                    sweet_alert_error('Error', 'Total pallets must be 20. Can not enter more pallets');
                    return bRslt;
                }
        }

        if (TotPallet == 20) {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
        } else if (TotPallet == 21) {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
        } else {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
        }


        bRslt = true;
    } catch (error) {

    }

    return bRslt;
}

function SaveOPdetail() {

    switch (_cropID.toLowerCase()) {
        case 'blu':
            break;
        case 'cit':
            if ((dMonTotPallet < 20) || (dMonTotPallet > 21)) {
                sweet_alert_warning('Error!', 'Total pallets must be 20 or 21.');
                return;
            }
            break;
        default:

    }

    var sCompletedDateTime = '';
    var sLoadingDateTime = '';
    var _nroAttemptsPacking = 0;

    var _orderProductionID = iOrderProdId;
    var _statusConfirm = 8;
    var _day = '';
    var _comments = '';
    var sPackingID = '';    
    var stxtVGM = 0;

    var _OPdetail = [];
    var _objOPdetail;
    var dbl_price = 0;

    switch (_cropID.toLowerCase()) {
        case 'blu':
            $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
                _objOPdetail = {};
                _objOPdetail["Quantity"] = $(objRow).find('#tdBoxes').text();
                _objOPdetail["categoryID"] = $(objRow).find('.tdCategoryID').text();
                _objOPdetail["varietyID"] = $(objRow).find('.tdVarietyID').text();
                _objOPdetail["formatID"] = $(objRow).find('.tdWeightBoxID').text();
                _objOPdetail["packageProductID"] = $(objRow).find('.tdTypeBoxID').text();
                _objOPdetail["brandID"] = $(objRow).find('.tdBrandID').text();
                _objOPdetail["presentationID"] = $(objRow).find('.tdPresentationID').text();
                _objOPdetail["labelID"] = $(objRow).find('.tdLabelID').text();
                _objOPdetail["codepackID"] = $(objRow).find('.tdWeightBoxID').text();
                _objOPdetail["codepack"] = $(objRow).find('#tdCodePack').text();
                _objOPdetail["sizeID"] = $(objRow).find('.tdSizeBoxID').text();
                _objOPdetail["priceBox"] = 0;
                _objOPdetail["workOrder"] = ($(objRow).find('.tdWorkOrder').text().toLowerCase() == 'NO') ? 0 : 1;
                _OPdetail.push(_objOPdetail);

            });
            break;
        default:
            $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
                _objOPdetail = {};
                _objOPdetail["Quantity"] = $(objRow).find('#tdBoxes').text();
                _objOPdetail["categoryID"] = $(objRow).find('.tdCategoryID').text();
                _objOPdetail["varietyID"] = $(objRow).find('.tdVarietyID').text();
                _objOPdetail["formatID"] = $(objRow).find('.tdWeightBoxID').text();
                _objOPdetail["packageProductID"] = $(objRow).find('.tdTypeBoxID').text();
                _objOPdetail["brandID"] = $(objRow).find('.tdBrandID').text();
                _objOPdetail["presentationID"] = $(objRow).find('.tdPresentationID').text();
                _objOPdetail["labelID"] = $(objRow).find('.tdLabelID').text();
                _objOPdetail["codepackID"] = $(objRow).find('.tdSizeBoxID').text();
                _objOPdetail["codepack"] = $(objRow).find('#tdCodePack').text();
                _objOPdetail["sizeID"] = $(objRow).find('.tdSizeBoxID').text();
                _objOPdetail["priceBox"] = 0;
                _objOPdetail["workOrder"] = ($(objRow).find('.tdWorkOrder').text().toLowerCase() == 'NO') ? 0 : 1;
                _OPdetail.push(_objOPdetail);

            });
    }

    _PromiseCompliance = $('#chkPromiseCompliance').is(":checked") ? 1 : 0;

    let jsonObjOPdetail = JSON.stringify({
        cropID: _cropID,
        orderProductionID: _orderProductionID,
        statusConfirmID: _statusConfirm,
        completedDate: sCompletedDateTime,
        loadingDate: sLoadingDateTime,
        vgm: stxtVGM,
        day: _day,
        userID: sessionUserID,
        comments: _comments,
        packingID: sPackingID,
        editAttemptsPacking: _nroAttemptsPacking,
        PromiseCompliance: _PromiseCompliance,
        orderProductionDetail: _OPdetail,
    });

    $.ajax({
        type: 'POST',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/PO/OrderProductionConfirmStatus",
        data: jsonObjOPdetail,
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        //async: false,
        error: function (datoError) {
            sweet_alert_warning('Error!', 'Fail PO Updated.');
        },
        success: function (data) {
            sweet_alert_success('Good Job!', 'PO Saved.');
            Exit(trRowModify);
            $(modalOPdetail).modal('hide');
            showDataSearched();
        },
        complete: function () {

        }
    });
}

function RemoveOPdetail(xthis) {

    try {
        var indice = $(xthis).closest('tr').index();
        var table = ''
        $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
            if (iRow != indice) {
                table += '<tr style="text-align:center ">';
                table += '<td style="background-color:#F9E79F; max-width: 80px; min-width:80px" class="tdBtn" >';
                table += '<button id="btnRowEdit" onclick="EditOPdetail(this)" class="btn btn-primary btn-sm " style="font-size:12px"><i class="fas fa-edit"></i></button>';
                table += '<button onclick="RemoveOPdetail(this)" style="font-size:12px" class="btn btn-danger btn-sm "><i class="fas fa-remove"></i></button>';
                table += '</td> ';                                                                                          //00
                table += '<td style="display:none" class="tdPlanCustomerVarietyID">' + $(objRow).closest("tr").find(".tdPlanCustomerVarietyID").text() + '</td>';                                 //01
                table += '<td style="display:none" class="tdVarietyID">' + $(objRow).closest("tr").find(".tdVarietyID").text() + '</td>';                     //02
                table += '<td id="tdVariety" style="max-width: 300px; min-width:300px">' + $(objRow).closest("tr").find("#tdVariety").text() + '</td>';                     //03
                table += '<td style="display:none" class="tdCategoryID">' + $(objRow).closest("tr").find(".tdCategoryID").text() + '</td>';                   //04
                table += '<td id="tdCategory">' + $(objRow).closest("tr").find("#tdCategoryID").text() + '</td>';                   //05
                table += '<td style="display:none" class="tdWeightBoxID">' + $(objRow).closest("tr").find(".tdWeightBoxID").text() + '</td>';                    //06
                table += '<td id="tdWeightBox">' + $(objRow).closest("tr").find("#tdWeightBox").text() + '</td>';                    //07
                table += '<td style="display:none" class="tdTypeBoxID">' + $(objRow).closest("tr").find(".tdTypeBoxID").text() + '</td>';                     //08
                table += '<td id="tdTypeBox">' + $(objRow).closest("tr").find("#tdTypeBox").text() + '</td>';                     //09
                table += '<td style="display:none" class="tdBrandID">' + $(objRow).closest("tr").find(".tdBrandID").text() + '</td>';                          //10
                table += '<td id="tdBrand">' + $(objRow).closest("tr").find("#tdBrand").text() + '</td>';                         //11
                table += '<td style="display:none" class="tdPresentationID">' + $(objRow).closest("tr").find(".tdPresentationID").text() + '</td>';           //12
                table += '<td id="tdPresentation">' + $(objRow).closest("tr").find("#tdPresentation").text() + '</td>';           //13
                table += '<td style="display:none" class="tdLabelID">' + $(objRow).closest("tr").find(".tdLabelID").text() + '</td>';                         //14
                table += '<td id="tdLabel">' + $(objRow).closest("tr").find("#tdLabel").text() + '</td>';                         //15
                table += '<td style="display:none" class="tdSizeBoxID">' + $(objRow).closest("tr").find(".tdSizeBoxID").text() + '</td>';                     //16
                table += '<td id="tdSizeBox">' + $(objRow).closest("tr").find("#tdSizeBox").text() + '</td>';                     //17
                table += '<td id="tdBoxPerPallet">' + $(objRow).closest("tr").find("#tdBoxPerPallet").text() + '</td>';            //18
                table += '<td id="tdBoxPerContainer">' + $(objRow).closest("tr").find("#tdBoxPerContainer").text() + '</td>';      //19
                table += '<td id="tdBoxes">' + $(objRow).closest("tr").find("#tdBoxes").text() + '</td>';                          //20
                table += '<td id="tdPallets">' + $(objRow).closest("tr").find("#tdPallets").text() + '</td>';                      //21
                table += '<td id="tdCodePack">' + $(objRow).closest("tr").find("#tdCodePack").text() + '</td>';                    //22
                table += '</tr>'
            }

        })

        if ($.fn.DataTable.isDataTable('table#tblPOdetail')) {

            $("table#tblPOdetail>tbody").empty();
            $("table#tblPOdetail>tbody").append(table);

        } else {
            $("table#tblPOdetail").DataTable({
                destroy: true,
                "ordering": false,
                "info": false,
                "responsive": true,
                "paging": true,

            });
            $("table#tblPOdetail>tbody").empty();
            $("table#tblPOdetail>tbody").append(table);
        }

        let TotPallet = 0;
        $('table#tblPOdetail>tbody>tr').each(function (iRow, objRow) {
            let objPallets = $(objRow).find('#tdPallets');
            TotPallet += parseFloat(objPallets.text());

        });
        $('#tblPOdetail .totalPallets').html(TotPallet)
        dMonTotPallet = parseFloat(TotPallet.toFixed(2));

        if (TotPallet == 20) {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
        } else {
            $(btnSave).removeClass();
            $(btnSave).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
        }

    } catch (error) {

    }
}

function GetValuesPackingSelectedByOrder(objSelPack) {
    var selected = [];
    $(objSelPack).each(function () {
        selected.push([$(this).val(), $(this).data('order')]);
    });
    selected.sort(function (a, b) {
        return a[1] - b[1];
    });

    var text = '';
    for (var i = 0; i < selected.length; i++) {
        text += selected[i][0] + ', ';
    }
    text = text.substring(0, text.length - 2);
    return text;
}

function MassiveConfirm() {
    sweet_alert_progressbar();
    var objCabecera = {}
    objCabecera.PObyPlanta = [];
    var Okey = false;
    $('#tableList>tbody>tr').each(function (i, e) {
        var statusId = parseInt($(e).closest("tr").find('#statusID').text());
        if (statusId != 1) {
            return;
        }

        var sPackingID = GetValuesPackingSelectedByOrder($(e).closest("tr").find('#selectPacking option:selected'));
        if (sPackingID == "") {
            return;
        }
        var objPo = {}
        objPo.orderProductionID = parseInt($(e).closest("tr").find('#orderProductionID').text());
        objPo.packingID = sPackingID;
        objCabecera.PObyPlanta.push(objPo);
        Okey = true;
    });

    if (!Okey) {
        sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
        return;
    }

    var sUrlApi = "/PO/GetPObyPlant?userID=" + sessionUserID;
    var bRsl = false;
    $.ajax({
        type: 'POST',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: sUrlApi,
        async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(objCabecera),
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
            }
            else {
                var confirmacionPO = data[0].column1;
                if (confirmacionPO == '1') {
                    bRsl = true;
                }
            }
        },
        error: function () {
            sweet_alert_info("Error", "Data was not saved");
        },
        complete: function () {
            if (bRsl) {
                sweet_alert_progressbar_cerrar();
                sweet_alert_success('Good Job!', 'Data Saved.');
                showDataSearched();
            }
        }
    })
}

function SettingCustomizedMultiselect(obj) {
    var orderCount = 0;
    $(obj).multiselect({
        buttonWidth: '200px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select expertise!',
        maxHeight: '1000px',
        dropUp: true,
        enableFiltering: true,
        onChange: function (option, checked) {
            if (checked) {
                orderCount++;
                $(option).data('order', orderCount);
            }
            else {
                $(option).data('order', '');
            }
        },
        buttonText: function (options) {
            if (options.length === 0) {
                return 'None selected';
            }
            else if (options.length > 3) {
                return options.length + ' selected';
            }
            else {
                var selected = [];
                options.each(function () {
                    selected.push([$(this).text(), $(this).data('order')]);
                });

                selected.sort(function (a, b) {
                    return a[1] - b[1];
                });

                var text = '';
                for (var i = 0; i < selected.length; i++) {
                    text += selected[i][0] + ', ';
                }

                return text.substr(0, text.length - 2);
            }
        },
    });
}
