﻿var sessionUserID = $('#lblSessionUserID').text()
var _saleID;
var _dataSale;
var _saleRequest;
var _dataSaleDetails;
var content1 = '';
var dataMarket = []
var dataAllWeek = []
var ActualyWeek = []
var dataStatus = []
var lastSelect = []
var flagSelectMarket = false;
var _userID = $("#lblSessionUserID").text();
var campaignID = localStorage.campaignID;

var switchON = true;

$(function () {
    loadData();
    loadDefaultValues();

    var marketID = [];
    $('#selectMarketFilter :selected').each(function (i, selected) {
        marketID[i] = $(selected).val();
    });
    var weekIni = $('#cboSemanaIni').val();
    var weekEnd = $('#cboSemanaFin').val();
    $('#selectStatusFilter').val('All').change();
    $("#selectStatusFilter").selectpicker('refresh');
    var statusID = $('#selectStatusFilter').val();
    var growerID = $('#selectGrowerFilter').val();

    createList(marketID, weekIni, weekEnd, statusID, growerID, campaignID);

    $('#ckhFilterSize').change(function () {
        if ($(this).is(':checked') == true) {
            $("table#tblDetailSales2>tbody>tr.withoutSize").show()
        } else {
            $("table#tblDetailSales2>tbody>tr.withoutSize").hide()
        }

    });

    $('#selectMarketFilter').selectpicker().change(function () {

        var newSelect = false;
        if ($("#selectMarketFilter option:selected").val() == 'All') {
            newSelect = true;
        }

        if (lastSelect == true) {
            if (newSelect == false) {
                if (flagSelectMarket == false) {
                    $("#selectMarketFilter").selectpicker('deselectAll');
                }

            } else {
                if (dataMarketUser.length + 1 > $("#selectMarketFilter").val().length) {
                    flagSelectMarket = true;
                    var setearNewSelect = $("#selectMarketFilter").val()
                    setearNewSelect.splice(0, 1);
                    console.log(setearNewSelect);
                    $("#selectMarketFilter").selectpicker('val', setearNewSelect)
                    flagSelectMarket = false;
                }
            }
        } else {
            if (newSelect == true) {

                $("#selectMarketFilter").selectpicker('selectAll');

            } else {

            }
        }

        //console.log($(this));
        //if ($("#selectMarketFilter option:selected").val() == 'All') {
        //  $("#selectMarketFilter option").each(function () {
        //    $(this).prop('selected', true);
        //    $("#selectMarketFilter").selectpicker('refresh');
        //  })
        //}
        //  else {
        //    $("#selectMarketFilter option").each(function () {
        //      console.log($(this));
        //    })
        //}
        //else {
        //$('#selectSectorModal').multiselect('deselect', arregloActual, true)
        //  $("#selectMarketFilter option").each(function () {
        ////    if ($(this).is(':selected').val() != 'All') {
        ////       $(this).prop('selected', false);
        ////    $("#selectMarketFilter").selectpicker('refresh');

        ////}
        //    $(this).prop('selected', false);
        //    $("#selectMarketFilter").selectpicker('refresh');
        //  })
        //}
        $('#selectMarketFilter').selectpicker('refresh')
        lastSelect = false;
        if ($("#selectMarketFilter option:selected").val() == 'All') {
            lastSelect = true;
        }
    });

    $(document).on("click", ".row-remove", function () {
        console.log($(this).parents('table').attr('id'));
        $idSaleDetail = $(this).parents('tr').children().eq(1).text();
        console.log($idSaleDetail);
        $idtable = $(this).parents('table').attr('id')
        $(this).parents('tr').detach();

    });

    $(document).on("click", ".fa-fast-backward", function () {
        console.log($(this).parents('table').attr('id'));
        //$idSaleDetail = $(this).parents('tr').children().eq(1).text();
        projectedProductionSizeCategorySummaryID = parseInt($(this).parents('tr').find('td.projectedProductionSizeCategorySummaryID').text());
        saleDetailSummaryID = parseInt($(this).parents('tr').find('td.saleDetailSummaryID').text());
        console.log($idSaleDetail);
        $idtable = $(this).parents('table').attr('id');
        var CommitSummary = $(this).closest('tr').find('td.commited').text();

        $("#tblDetailSales>tbody>tr").each(function (i, e) {
            if ($(e).find("td.saleDetailID").text() == saleDetailSummaryID) {
                var commited = parseInt($(e).find("input#Commit").val()) - parseInt(CommitSummary)
                $(e).find("input#Commit").val(commited)

                var pending = parseInt($(e).find("input#Pending").val()) + parseInt(CommitSummary)
                $(e).find("input#Pending").val(pending)
            }
        })

        $("#tblDetailSales2>tbody>tr").each(function (i, e) {
            var projectedProductionSizeCategoryID = parseInt($(e).find("#projectedProductionSizeCategoryID").val());
            if (projectedProductionSizeCategoryID == projectedProductionSizeCategorySummaryID) {

                var boxavailable = $(e).find('td.boxavailable').text();

                $(e).find('td.boxavailable').text(parseInt(boxavailable) + parseInt(CommitSummary));
                $(e).find('#Box').val('0');
                $(e).find('#Pallet').val('0');
                $(e).find('#Kg').val('0');
            }
            //$(e).find('td.boxavailable').text();
        })

        console.log(CommitSummary);
        //console.log(CommitSale);
        //tblDetailSales
        //$(this).closest('tr').find('#Commit').val('254');
        $(this).parents('tr').detach();

    });


    //$('button.row-detalle').click(function (e) {
    $(document).on("click", "button.row-detalle", function () {
        //console.log($(this).parents('table').attr('id'));
        $idSaleDetail = $(this).parents('tr').children().eq(0).text();

        $("table#tblDetailSales>tbody>tr").removeClass("row-select2");
        //$(this).parents('tr').attr("css", { backgroundColor: "gray" });
        $(this).parents('tr').addClass("row-select2");

        //$("#wait").css("display", "block");
        //$("#loader").css("display", "block")
        //console.log('wait')
        //$(".transact").attr("disabled", true);

        //loadDataDetailSale($idSaleDetail);
        var opt = "edi";
        var growerID = '';
        $.ajax({
            type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            url: "/PO/ENGetVarietyForecastDetailForPOEdit?opt=" + opt + "&growerID=" + growerID + "&saleDetailID=" + $idSaleDetail,
            async: true,
            success: function (data) {
                //demo();
                //alert(123)
                _dataSaleDetails = []
                if (data.length > 2) {
                    var dataJson = JSON.parse(data);
                    _dataSaleDetails = dataJson
                    //_dataSaleDetails = dataJson.map(
                    //    obj => {
                    //        return {

                    //            "grower": obj.grower,
                    //            "farm": obj.farm,
                    //            "brand": obj.brand,
                    //            "varietyID": obj.varietyID,
                    //            "variety": obj.variety,
                    //            "size": obj.size,
                    //            "year": obj.year,
                    //            "week": obj.week,
                    //            "boxpallet": obj.boxpallet,
                    //            "palletavailable": obj.palletavailable,
                    //            "boxavailable": obj.boxavailable,
                    //            "available": obj.available,
                    //            "kgbox": obj.kgbox,
                    //            "format": obj.format,
                    //            "projectedProductionSizeCategoryID": obj.projectedProductionSizeCategoryID,
                    //            "growerID": obj.growerID,
                    //            "farmID": obj.farmID,
                    //            "varieties": obj.varieties,
                    //            "sizes": obj.sizes,
                    //            "saleDetailID": obj.saleDetailID,
                    //            "format": obj.format,
                    //            "categoryID": obj.categoryID


                    //        }
                    //    }
                    //);

                }
                $("#tblDetailSales2>tbody").empty()
                $("#ckhFilterSize").prop("checked", false);
                loadDataDetailSaleTable();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
                console.log(ajaxOptions);
                console.log(thrownError);
            }
        });


    });

    $(document).on("click", "#btnSearch", function () {


        var marketID = [];
        $('#selectMarketFilter :selected').each(function (i, selected) {
            marketID[i] = $(selected).val();
        });
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();
        var growerID = $('#selectGrowerFilter').val();

        createList(marketID, weekIni, weekEnd, statusID, growerID, campaignID);
    })

    $(document).on("click", "#btnSalesModified", function () {
        $("#selectMarketFilter").selectpicker('deselectAll');
        $('#selectMarketFilter').selectpicker('refresh');
        $('#cboSemanaIni').val(dataAllWeek[0].id).change();
        $('#cboSemanaFin').val(dataAllWeek[dataAllWeek.length - 1].id).change();
        var SalesModified = dataStatus.filter(item => item.id == 5)
        $('#selectStatusFilter').val(SalesModified[0].id).change();
        $('#selectStatusFilter').selectpicker('refresh');
        $('#selectGrowerFilter').val('All').change();
        var marketID = '';
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();
        var growerID = $('#selectGrowerFilter').val();

        createList(marketID, weekIni, weekEnd, statusID, growerID, campaignID);

    })

    $(document).on("click", "#btnRejected", function () {
        $("#selectMarketFilter").selectpicker('deselectAll');
        $('#selectMarketFilter').selectpicker('refresh');
        $('#cboSemanaIni').val(dataAllWeek[0].id).change();
        $('#cboSemanaFin').val(dataAllWeek[dataAllWeek.length - 1].id).change();
        var Rejected = dataStatus.filter(item => item.id == 4)
        $('#selectStatusFilter').val(Rejected[0].id).change();
        $('#selectStatusFilter').selectpicker('refresh');
        $('#selectGrowerFilter').val('All').change();

        var marketID = '';
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();
        var growerID = $('#selectGrowerFilter').val();

        createList(marketID, weekIni, weekEnd, statusID, growerID, campaignID);
    })



    $('#btnConfirm').click(function (e) {
        if (!validConfirm()) {
            return;
        }

        var userCreated = $("#lblSessionUserID").text();
        var orderProductionID = $(lblId).text();
        var statusID = 1;
        
        $.ajax({
            type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            url: "POModify?orderProductionID=" + orderProductionID + "&userID=" + userCreated + "&statusID=" + statusID,
            //data: JSON.stringify(Object.fromEntries(frmDet)),
            contentType: "application/json",
            Accept: "application/json",
            dataType: 'json',
            async: true,
            error: function (datoEr) {
                console.log("Hubo un error: " + JSON.stringify(datoEr));
                isOK = false;
            },
            success: function (data) {
                //var objData = JSON.parse(data);
                console.log("CreateUpdatePO: " + JSON.stringify(data));

                $('table#tblDetailSales3>tbody>tr').each(function (i, e) {
                    console.log(e);
                    //console.log($(e).find('td.growerID').text())
                    //console.log($(e).find('td.orderProductionDetailID').text())
                    //console.log($(e).find('td.commited').text())
                    //console.log($(e).find('td.projectedProductionSizeCategorySummaryID').text())
                    //console.log($(e).find('td.saleDetailSummaryID').text())
                    //console.log($(e).find('td.sizeSummary').text())

                    var projectedProductionDetailID = $(e).find('td.projectedProductionSizeCategorySummaryID').text()
                    var saleDetailID = $(e).find('td.saleDetailSummaryID').text();
                    var Quantity = $(e).find('td.commited').text()
                    var size = $(e).find('td.sizeSummary').text()


                    var frmDet = new FormData();

                    frmDet.append("orderProductionDetailID", 0);
                    frmDet.append("orderProductionID", orderProductionID);
                    frmDet.append("userCreated", userCreated);

                    frmDet.append("projectedProductionDetailID", projectedProductionDetailID);
                    frmDet.append("saleDetailID", saleDetailID);
                    frmDet.append("Quantity", Quantity);

                    frmDet.append("sizeID", size);

                    $.ajax({
                        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                        url: "CreateUpdatePODetail",
                        data: JSON.stringify(Object.fromEntries(frmDet)),
                        contentType: "application/json",
                        Accept: "application/json",
                        dataType: 'json',
                        async: false,
                        error: function (datoEr) {
                            console.log("Hubo un error: " + JSON.stringify(datoEr));
                            isOK = false;
                        },
                        success: function (data) {
                            //var objData = JSON.parse(data);
                            console.log("CreateUpdatePO: " + JSON.stringify(data));
                        },
                    });
                })

                Swal.fire("Good job!", "PO Saved", "success");
                //list(1, 74, 0);
                $(btnSearch).click();
                $('#myModal').modal('hide');
            },
        });
        //POModify

        //$('table#tblDetailSales3>tbody>tr').each(function (i, e) {
        //    console.log(e);
        //    //console.log($(e).find('td.growerID').text())
        //    //console.log($(e).find('td.orderProductionDetailID').text())
        //    //console.log($(e).find('td.commited').text())
        //    //console.log($(e).find('td.projectedProductionSizeCategorySummaryID').text())
        //    //console.log($(e).find('td.saleDetailSummaryID').text())
        //    //console.log($(e).find('td.sizeSummary').text())

        //    var projectedProductionDetailID = $(e).find('td.projectedProductionSizeCategorySummaryID').text()
        //    var saleDetailID = $(e).find('td.saleDetailSummaryID').text();
        //    var Quantity = $(e).find('td.commited').text()

        //    frmDet.append("orderProductionDetailID", 0);
        //    frmDet.append("orderProductionID", orderProductionID);
        //    frmDet.append("userCreated", userCreated);

        //    frmDet.append("projectedProductionDetailID", projectedProductionDetailID);
        //    frmDet.append("saleDetailID", saleDetailID);
        //    frmDet.append("Quantity", Quantity);

        //    frmDet.append("sizeID", CreatePODetail[i][0].psize);

        //    $.ajax({
        //        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        //        url: "CreateUpdatePODetail",
        //        data: JSON.stringify(Object.fromEntries(frmDet)),
        //        contentType: "application/json",
        //        Accept: "application/json",
        //        dataType: 'json',
        //        async: false,
        //        error: function (datoEr) {
        //            console.log("Hubo un error: " + JSON.stringify(datoEr));
        //            isOK = false;
        //        },
        //        success: function (data) {
        //            //var objData = JSON.parse(data);
        //            console.log("CreateUpdatePO: " + JSON.stringify(data));
        //        },
        //    });
        //})


    });


    $(document).on("change", "#Box", function () {
        //$(this).closest('tr').find('#Kg').val('123')
        var box = parseInt($(this).closest('tr').find('#Box').val());
        var boxavailable = parseInt($(this).closest('tr').find('td.boxavailable').text());
        var boxpallet = parseInt($(this).closest('tr').find('#boxpallet').val());
        var box = parseInt($(this).closest('tr').find('#Box').val());

        //$(this).closest('tr').find('td.boxavailable').text(boxavailableTotal);
        $(this).closest('tr').find('#Pallet').val((box / boxpallet).toFixed(2));
        var kgbox = parseFloat($(this).closest('tr').find('#kgbox').val()).toFixed(2);
        $(this).closest('tr').find('#Kg').val((kgbox * box).toFixed(2));

    })

    $(document).on("change", "input#boxes", function () {
        var boxes = parseInt($(this).val());
        var tr = $(this).closest('tr');

        var commited = parseInt($(tr).find('td>input#Commit').val())
        var pending = 0;

        if (boxes >= commited) {
            pending = boxes - commited;
            $(tr).find('td>input#Pending').val(pending);
        } else {
            $(tr).find('td>input#boxes').val($(tr).find('td>input#boxesSuggest').val())
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Invalid amount, undo the commitment first",
                //footer: '<a href>Why do I have this issue?</a>'
            })
        }

        //var pending = parseInt($(tr).find('td>input#Pending').val())


    })
})

function validConfirm() {

    if ($("#tblDetailSales3>tbody>tr").length == 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Enter boxes",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }

    //var totalBoxes = 0;
    //$("#tblDetailSales>tbody>tr>td.boxes").each(function (i, e) {
    //    totalBoxes = totalBoxes + parseInt($(e).find('input').val());
    //})

    //var totalCommited = 0
    //$("#tblDetailSales3>tbody>tr>td.commited").each(function (i, e) {
    //    totalCommited = totalCommited +parseInt($(e).text());
    //})    


    //if (totalBoxes > totalCommited) {
    //    Swal.fire({
    //        icon: 'error',
    //        title: 'Oops...',
    //        text: "Check the total boxes",
    //        //footer: '<a href>Why do I have this issue?</a>'
    //    })
    //    return false;
    //}
    //if (totalBoxes < totalCommited) {
    //    Swal.fire({
    //        icon: 'error',
    //        title: 'Oops...',
    //        text: "Check the total boxes",
    //        //footer: '<a href>Why do I have this issue?</a>'
    //    })
    //    return false;
    //}

    var totalPending = 0
    $("#tblDetailSales>tbody>tr>td>input#Pending").each(function (i, e) {
        totalPending = totalPending + parseInt($(e).val());
    })

    if (totalPending > 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Check pending boxes ",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }


    var totalPalletMinimun = 0;
    $("#tblDetailSales>tbody>tr>td>input#palletSuggest").each(function (i, e) {
        totalPalletMinimun = totalPalletMinimun + parseFloat($(e).val());
    })

    var totalPalletCommited = 0
    $("#tblDetailSales3>tbody>tr>td.palletSummary").each(function (i, e) {
        totalPalletCommited = totalPalletCommited + parseFloat($(e).text());
    })

    if (totalPalletMinimun > totalPalletCommited) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "This Order Need " + totalPalletMinimun.toFixed(0) + " Pallets",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }
    if (totalPalletMinimun < totalPalletCommited) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "This Order only Need " + totalBoxes.toFixed(0) + " Pallets",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }

    return true;
}

function CalcularCommit(xthis) {

    //content = '';
    //var boxpallet = parseInt($('#boxpallet').val());
    var projectedProductionSizeCategoryID = $(xthis).closest('tr').find('#projectedProductionSizeCategoryID').val();
    var saleDetailID = $(xthis).closest('tr').find('#saleDetailID').val();
    var sizes = $(xthis).closest('tr').find('#sizes').val();
    var box = $(xthis).closest('tr').find('#Box').val();
    var Pallet = $(xthis).closest('tr').find('#Pallet').val();
    var Kg = $(xthis).closest('tr').find('#Kg').val();
    var boxavailable = parseInt($(xthis).closest('tr').find('td.boxavailable').text());
    var kgbox = $(xthis).closest('tr').find('#kgbox').val();
    var boxpallet = $(xthis).closest('tr').find('#boxpallet').val();
    var grower = $(xthis).closest('tr').find('td.grower').text()
    var farm = $(xthis).closest('tr').find('td.farm').text()
    var varietyID = $(xthis).closest('tr').find('td.varietyID').text()
    var categoryID = $(xthis).closest('tr').find('td.categoryID').text()
    var variety = $(xthis).closest('tr').find('td.variety').text()
    var category = $(xthis).closest('tr').find('td.category').text()
    var format = $(xthis).closest('tr').find('td.format').text()
    var week = $(xthis).closest('tr').find('td.week').text()
    var growerID = $(xthis).closest('tr').find('td.growerID').text()
    if (box == 0) {
        sweet_alert_warning('Warning!', 'Enter number of boxes');
        return;
    }

    if (box > boxavailable) {
        sweet_alert_warning('Warning!', 'Quantity exceeds the available stock.');
        return;
    }

    if (sizes == "") {
        sweet_alert_warning('Warning!', 'Select size');
        return;
    }
    if (sizes == null) {
        sweet_alert_warning('Warning!', 'Select size');
        return;
    }
    if (sizes == "") {
        sweet_alert_warning('Warning!', 'Select size');
        return;
    }

    console.log(saleDetailID);
    var boxPendingSales = 0;
    $("#tblDetailSales>tbody>tr").each(function (itr, tr) {
        saleDetailTableID = $(tr).find('td.saleDetailID').text();
        if (saleDetailID == saleDetailTableID) {
            var Commit = parseInt($(tr).find('td').find('#Commit').val()) + parseInt(box);
            var boxSale = parseInt($(tr).find('td.boxes>input').val());

            boxPendingSales = parseInt(boxSale) - parseInt(Commit);
            //if (Commit>=boxavailable  ) {
            //  sweet_alert_warning('Warning!', 'Quantity exceeds the available stock.');
            //  return;
            //}
            if (boxSale >= Commit) {
                $(tr).find('input#Commit').val(Commit);
                $(tr).find('input#Pending').val(boxPendingSales);
            } else {
                if (boxPendingSales > 0) {
                    sweet_alert_warning('Warning!', 'Only 20 boxes needed');
                } else {
                    sweet_alert_warning('Warning!', 'No more boxes needed');
                }

                return;
            }
        }

    })
    if (boxPendingSales >= 0) {
        content1 = '';
        content1 += '    <tr >';
        content1 += '<td  style="display:none" class="projectedProductionSizeCategorySummaryID" >' + projectedProductionSizeCategoryID + '</td>';
        content1 += '<td  style="display:none" class="saleDetailSummaryID">' + saleDetailID + '</td>';
        content1 += '<td  style="display:none" class="orderProductionDetailID">0</td>';
        content1 += '<td  style="display:none" class="growerID">' + growerID + '</td>';
        content1 += '      <td style="font-size: 14px;max-width:120px;min-width:120px;">' + grower + '</td>';
        content1 += '      <td style="font-size: 14px;max-width:100px;min-width:100px;">' + farm + '</td>';
        content1 += '      <td style="font-size: 14px">' + week + '</td>';
        content1 += '      <td style="font-size: 14px">' + category + '</td>';
        content1 += '      <td style="display:none">' + categoryID + '</td>';
        content1 += '      <td style="font-size: 14px">' + variety + '</td>';
        content1 += '      <td style="display:none">' + varietyID + '</td>';
        content1 += '      <td style="font-size: 14px">' + format + '</td>';
        content1 += '      <td style="font-size: 14px" class="sizeSummary">' + sizes + '</td>';
        content1 += '      <td style="font-size: 14px" class="commited">' + box + '</td>';
        content1 += '      <td style="font-size: 14px" class="kgSummary">' + Kg + '</td>';
        content1 += '      <td style="font-size: 14px" class="palletSummary">' + Pallet + '</td>';
        content1 += '     <td style="font-size: 14px;text-align: center;"><button class="btn btn-black " style="color: darkred" onclick=""><i class="fas fa-fast-backward fa-sm"></i></button></td>';
        content1 += '</tr>';

        if ($("#tblDetailSales3>tbody>tr").length == 0) {
            $("#tblDetailSales3>tbody").append(content1);
            //var Commited = $("#tblDetailSales3>tbody>tr").find("td.commited").text();
            //var kgSummary = Kg;//parseInt(Commited) * Kg;
            //var palletSummary = Pallet;//parseInt(Commited) * Kg;
            //$("#tblDetailSales3>tbody>tr").find("td.kgSummary").text(kgSummary);
            //$("#tblDetailSales3>tbody>tr").find("td.palletSummary").text(palletSummary);
        } else {
            var exists = false;
            $("#tblDetailSales3>tbody>tr").each(function (i, e) {

                projectedProductionSizeCategorySummaryID = $(e).find('input#projectedProductionSizeCategorySummaryID').val();
                saleDetailSummaryID = $(e).find('#saleDetailSummaryID').val();
                sizeSummary = $(e).find('td.sizeSummary').text();

                if ((parseInt(projectedProductionSizeCategorySummaryID) == parseInt(projectedProductionSizeCategoryID)) && (parseInt(saleDetailSummaryID) == parseInt(saleDetailID)) && (sizeSummary == sizes)) {
                    //if (projectedProductionSizeCategorySummaryID == projectedProductionSizeCategoryID) {
                    var Commited = parseInt($(e).find('td.commited').text()) + parseInt(box);
                    var kgSummary = parseFloat(Commited) * parseFloat(kgbox) //parseInt($(e).find('td.kgSummary').text()) + parseInt(Kg);
                    var palletSummary = parseFloat(Commited) / parseFloat(boxpallet)//parseInt($(e).find('td.palletSummary').text()) + parseInt(Pallet);
                    //var kgSummary = parseInt(Commited) * Pallet;
                    //var palletSummary = parseInt(Commited) * Kg;
                    $(e).find('td.commited').text(Commited);
                    $(e).find('td.kgSummary').text(parseInt(kgSummary));
                    $(e).find('td.palletSummary').text(parseInt(palletSummary));
                    exists = true;
                }
            })

            if (exists == false) {
                $("#tblDetailSales3>tbody").append(content1);
                console.log('New')
            }
        }
        $(xthis).closest('tr').find('td.boxavailable').text(boxavailable - box);
    }
    console.log(saleDetailID);

}



function createList(marketID, weekini, weekEnd, statusID, growerID, campaignID) {
    var opt = "det";
    if (growerID == 'All') {
        growerID = '';
    }
    if (statusID == 'All') {
        statusID = '';
    }
    $('#tableList').DataTable().clear().destroy()
    $("table#tableList>tbody").empty()
    $.ajax({
        type: "GET",
        url: "/PO/GetAllForEditFiltered?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekEnd + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID,
        //async: true,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                $.each(dataJson, function (i, e) {
                    var color = '';
                    if (e.statusId == 1) {
                        color = 'badge-warning';
                    }
                    if (e.statusId == 4) {
                        color = 'badge-danger';
                    }
                    if (e.statusId == 2) {
                        color = 'badge-info';
                    }
                    if (e.statusId == 5) {
                        color = 'badge-primary';
                    }
                    if (e.statusId == 3) {
                        color = 'badge-success';
                    }

                    var tr = '';
                    if (e.statusId == 1) {
                        tr += '<tr  class="table-warning">';
                    } else if (e.statusId == 3) {
                        tr += '<tr  class="table-success">';
                    } else if (e.statusId == 4) {
                        tr += '<tr  class="table-danger">';
                    } else if (e.statusId == 2) {
                        tr += '<tr  class="table-info">';
                    } else if (e.statusId == 5) {
                        tr += '<tr  class="table-primary">';
                    }
                    tr += '<td style="display:none">' + e.orderProductionID + '</td>';
                    tr += '<td style="font-size: 12px">' + e.requestNumber + '</td>';
                    tr += '<td style="font-size: 12px">' + e.grower + '</td>';
                    tr += '<td style="font-size: 12px">' + e.farm + '</td>';
                    tr += '<td style="font-size: 12px">' + e.week + '</td>';
                    tr += '<td style="font-size: 12px">' + e.destination + '</td>';
                    tr += '<td style="font-size: 12px">' + e.via + '</td>';
                    tr += '<td style="font-size: 12px;max-width:150px;min-width:150px;text-align: center;" ><div class="badge ' + color + ' badge-pill">' + e.status + '</div></td>';
                    tr += '<td style="font-size: 12px">' + e.create + '</td>';
                    tr += '<td style="display:none">' + e.expireIN + '</td>';
                    tr += '<td style="display:none">' + e.time + '</td>';

                    tr += '<td style="text-align: center;"><button class="btn  btn-sm btn-primary" onclick="openModal(' + e.orderProductionID + ')" style="" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-sm "></i></button></td>';
                    //<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm"></i></button>
                    //tr += '<button class="btn btn-danger btn-sm" onclick="generateFilePdf(' + e.orderProductionID + ')"><i class="fas fa-file-pdf"></i></button></td>';
                    tr += '<td style="text-align: center;"><button class="btn btn-danger btn-sm" onclick="generateFilePdf(' + e.orderProductionID + ')" style=""><i class="fas fa-file-pdf "></i></button></td>';
                    tr += '<td style="text-align: center;"><button class="btn btn-danger btn-sm" onclick="generateFileSI(' + e.orderProductionID + ')"  style=""><i class="fas fa-file-pdf "></i></button></td>';
                  tr += '<td style="font-size: 12px; max-width:150px;min-width:150px;text-align: center;"><button class="btn btn-sm btn-danger" style=""  onclick="generateFileSpecs(' + e.orderProductionID + ')"><i class="fas fa-file-pdf "></i></button></td>';
                  tr += '<td style="text-align: center;"><button class="btn btn-sm btn-danger" style="" onclick="generateFileBilling(' + e.orderProductionID + ')"><i class="fas fa-file-pdf "></i></button></td>';
                    //tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm"></i></button></td>';

                    //tr += '<td><button class="btn btn-danger btn-sm" onclick="sendMail(' + e.orderProductionID + ')"><i class="fas fa-envelope"></i></i></button></td>';
                    tr += '</tr>';
                    //console.log(tr);
                    $('table#tableList tbody').append(tr);

                })

                $('#tableList').DataTable({
                    "order": [[7, "ASC"]]
                });

            }
        }
    });
    //loadQuantityByStatus();
}

function openModal(poID) {
    var opt = "det";
    var cabPO = []
    ClearModal()
    $.ajax({
        type: "GET",
        url: "/PO/GetConfirmById?opt=" + opt + "&orderProductionID=" + poID,
        //async: true,
        success: function (data) {
            if (data.length > 0) {
                cabPO = JSON.parse(data)[0];
                _saleID = cabPO.saleID;
                _saleRequest = cabPO.saleRequest
                console.log(JSON.stringify(cabPO))
                $(lblPO).text(cabPO.requestNumber)
                $(lblId).text(cabPO.orderProductionID)
                $(lblFarm).text(cabPO.farm)
                $(lblDestination).text(cabPO.destination)
                $(lblWeek).text(cabPO.week)
                $(lblConsignee).text(cabPO.consignee)
                $(lblIncoterm).text(cabPO.incotem)
                $(lblNotify).text(cabPO.notify)
                $(lblVia).text(cabPO.via)
                $(txtDateBoard).val(cabPO.dateboard)
                $(lblStatus).text(cabPO.status)
                $(txtCommentsPO).val(cabPO.comments)
                $(txtCommentsSales).val(cabPO.commentary)


                //$(txtDateBoard).text()

                loadDataSale(_saleID);

            }

        }
    });



}



function loadDataSale(saleID) {
    var opt = "poe";
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    var campaignID = 0;
    $.ajax({
        type: "GET",
        url: "/PO/GetDetailByIDForPOEdit?opt=" + opt + "&val=" + saleID + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                //_dataSale = dataJson.map(
                //    obj => {
                //        return {
                //            "saleID": obj.saleID,
                //            "nroSALES": obj.nroSALES,
                //            "condition": obj.condition,
                //            "saleDetailID": obj.saleDetailID,
                //            "customer": obj.customer,
                //            "destination": obj.destination,
                //            "consignee": obj.consignee,
                //            "brand": obj.brand,
                //            "varieties": obj.varieties,
                //            "via": obj.via,
                //            "format": obj.format,
                //            "weight": obj.weight,
                //            "boxes": obj.boxes,
                //            "sizes": obj.sizes

                //        }
                //    }
                //);
                LoadDataSaleTable(dataJson);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });


}

function ClearModal() {
    $('#tblDetailSales>tbody').html('')
    $('#tblDetailSales2>tbody').html('')
    $('#tblDetailSales3>tbody').html('')
}

function LoadDataSaleTable(dataSale) {
    document.getElementById("labelSaleID").innerHTML = _saleRequest;
    $(txtCommentsSales).text(dataSale[0].commentary)
    content = '';
    content += '</tr>';
    $.each(dataSale, function (i, e) {
        content += '<tr >';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;; display:none" class="saleDetailID" >' + e.saleDetailID + '</td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;"><button class="btn btn-black btn-sm row-detalle" onclick=""><i class="fas fa-arrow-alt-circle-right fa-sm row-detalle"></i></button></td>';
        content += '<td  style="font-size: 14px;max-width: 100px;min-width: 100px;"> ' + e.brand + '</td >';
        content += '<td  style="font-size: 14px;max-width: 180px;min-width: 180px;"><textarea class="form-control form-control-sm" rows="1">' + e.varieties + '</textarea></td>';
        content += '<td  style="font-size: 14px;max-width: 120px;min-width: 120px;"><textarea class="form-control form-control-sm" rows="1">' + e.sizes + '</textarea></td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px; display:none" >' + e.via + '</td>';
        content += '<td  style="font-size: 14px;max-width: 120px;min-width: 120px;">' + e.format + '</td>';
        content += '<td  style="font-size: 14px;max-width: 55px;min-width: 55px;text-align: right; ">' + e.weight + '</td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;text-align: right; display:none" ><input id="palletSuggest" class="form-control form-control-sm" type="number" value="' + e.pallet + '" disabled></td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;text-align: right; display:none" ><input id="boxesSuggest" class="form-control form-control-sm" type="number" value="' + e.boxes + '" disabled></td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;text-align: right; " ><input id="boxesMinimun" class="form-control form-control-sm" type="number" value="' + e.quantityMinimun + '" disabled></td>';
        if (e.flexible == 1) {
            content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;text-align: right;" class="boxes"> <input id="boxes" class="form-control form-control-sm" type="number" onkeypress="return false;" min="' + e.quantityMinimun + '" max="' + e.maxBoxes + '" step="' + e.boxesPerPallet + '" value="' + e.boxes + '"></td>';
        } else {
            content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;text-align: right;" class="boxes"> <input id="boxes" class="form-control form-control-sm" type="number" onkeypress="return false;" min="' + e.quantityMinimun + '" max="' + e.maxBoxes + '" step="' + e.boxesPerPallet + '" value="' + e.boxes + '" disabled></td>';
        }


        //content += '<td  style="font-size: 14px"></td>';
        //content += '<td  style="font-size: 14px"></td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;"><input id="Commit" value="0" class="form-control form-control-sm" disabled></td>';
        content += '<td  style="font-size: 14px;max-width: 80px;min-width: 80px;"><input id="Pending" value="' + e.boxes + '" class="form-control form-control-sm" disabled></td>';
        content += '<td  style="font-size: 14px;max-width: 120px;min-width: 120px;text-align: right;" class=""><input id="txtCustomerPrice" class="form-control form-control-sm" type="number" value="' + e.price + '" disabled></td>';
        content += '<td  style="font-size: 14px;max-width: 120px;min-width: 120px;text-align: right;" class=""><input id="txtBillingPrice" class="form-control form-control-sm" type="number" value="' + e.priceBilling + '"></td>';
        //content += '	<td style="font-size: 14px"><button class="btn btn-black btn-sm" style="color: #7B7DDF" onclick="openModal(0)" data-toggle="modal" data-target="#myModalDetalle"  ><i class="fas fa-edit fa-sm"></i></button></td>';
        content += '</tr>';
    })

    $("#tblDetailSales>tbody").empty().append(content);



};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function demo() {
    console.log('Taking a break...');
    await sleep(2000);
    console.log('Two seconds later, showing sleep in a loop...');

    // Sleep in loop
    for (let i = 0; i < 5; i++) {
        if (i === 3)
            await sleep(2000);
        console.log(i);
    }
}

function loadDataDetailSale(saleDetailID) {
    var opt = "edi";
    var growerID = '';
    $.ajax({
        type: "GET",
        url: "/PO/ENGetVarietyForecastDetailForPOEdit?opt=" + opt + "&growerID=" + growerID + "&saleDetailID=" + saleDetailID,
        //async: true,
        success: function (data) {
            demo();
            alert(123)
            _dataSaleDetails = []
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                _dataSaleDetails = dataJson
                //_dataSaleDetails = dataJson.map(
                //    obj => {
                //        return {

                //            "grower": obj.grower,
                //            "farm": obj.farm,
                //            "brand": obj.brand,
                //            "varietyID": obj.varietyID,
                //            "variety": obj.variety,
                //            "size": obj.size,
                //            "year": obj.year,
                //            "week": obj.week,
                //            "boxpallet": obj.boxpallet,
                //            "palletavailable": obj.palletavailable,
                //            "boxavailable": obj.boxavailable,
                //            "available": obj.available,
                //            "kgbox": obj.kgbox,
                //            "format": obj.format,
                //            "projectedProductionSizeCategoryID": obj.projectedProductionSizeCategoryID,
                //            "growerID": obj.growerID,
                //            "farmID": obj.farmID,
                //            "varieties": obj.varieties,
                //            "sizes": obj.sizes,
                //            "saleDetailID": obj.saleDetailID,
                //            "format": obj.format,
                //            "categoryID": obj.categoryID


                //        }
                //    }
                //);

            }
            $("#tblDetailSales2>tbody").empty()
            $("#ckhFilterSize").prop("checked", false);
            loadDataDetailSaleTable();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });



}


function loadDataDetailSaleTable() {
    content = '';
    content += '</tr>';
    var sizes = []
    $.each(_dataSaleDetails, function (index, row) {
        content += '    <tr >';
        content += '<td  style="display:none"><input id="kgbox"  value="' + row.kgbox + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"></input></td>';
        content += '<td  style="display:none"><input id="boxpallet"  value="' + row.boxpallet + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"></input></td>';
        content += '<td  style="display:none"><input id="projectedProductionSizeCategoryID"  value="' + row.projectedProductionSizeCategoryID + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"></input></td>';
        content += '<td  style="display:none"><input id="saleDetailID"  value="' + row.saleDetailID + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"></input></td>';
        content += '      <td class="growerID" style="font-size: 14px;display:none">' + row.growerID + '</td>';
        //content += '     <td style="font-size: 14px"><button class="btn btn-black btn-sm row-remove" style="color: darkred" onclick=""><i class="fas fa-trash fa-sm"></i></button></td>';
        content += '      <td class="grower" style="font-size: 14px">' + row.grower + '</td>';
        content += '      <td class="farm" style="font-size: 14px">' + row.farm + '</td>';
        content += '      <td class="week" style="font-size: 14px">' + row.week + '</td>';
        content += '      <td class="category" style="font-size: 14px">' + row.brand + '</td>';
        content += '      <td class="categoryID" style="display:none">' + row.categoryID + '</td>';
        content += '      <td class="variety" style="font-size: 14px">' + row.variety + '</td>';
        content += '      <td class="varietyID" style="display:none">' + row.varietyID + '</td>';
        content += '      <td class="format" style="font-size: 14px;display:none">' + row.format + '</td>';
        content += '      <td class="size" style="font-size: 14px">' + row.size + '</td>';
        content += '      <td style="font-size: 14px;text-align: right;">' + parseFloat(row.available).toFixed(0) + '</td>';
        content += '      <td style="font-size: 14px;text-align: right;" class="boxavailable">' + parseFloat(row.boxavailable).toFixed(0) + '</td>';
        content += '<td  style="font-size: 14px"><input id="Box"  value="0" class=" form-control form-control-sm " step=' + row.boxpallet + ' min="0" max=' + row.boxavailable + '   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"></input></td>';
        content += '<td style="font-size: 14px"><button class="btn btn-black btn-sm" style="color: darkred" onclick="CalcularCommit(this)"><i class="fas fa fa-fast-forward fa-sm"></i></button></td>';
        content += '<td style="min-width: 80px;"><select id="sizes" class=" form-control form-control-sm td-width80" >';
        $.each(row.sizes, function (i, s) {
            console.log(s);
            content += "<option value='" + s.sizeID + "'>";
            content += s.size;
            content += "</option>";
        });
        content += '</select></td>';
        content += '<td  style="font-size: 14px;min-width: 80px;max-width: 80px" class="td-width80"><input value="0" id="Pallet" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
        content += '<td  style="font-size: 14px;width: 80px;" class="td-width80"><input value="0" id="Kg" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly></input></td>';
        sizes = row.sizes;
    })

    content += '</tr>';

    $('table#tblDetailSales2').DataTable().destroy();
    $("#tblDetailSales2>tbody").empty().append(content);
    //var tabla = $('table#tblDetailSales2').DataTable()
    //tabla.destroy();

    $('table#tblDetailSales2').DataTable({
        //destroy: true,
        "pageLength": 50
    })

    loadSelectionSizeByDefault(sizes)
    $("table#tblDetailSales2>tbody>tr.withoutSize").hide()
}

function loadSelectionSizeByDefault(sizes) {
    console.log(JSON.stringify(sizes))
    $("#tblDetailSales2>tbody>tr").each(function (i, e) {
        var osize = $(this).find("td.size").text();
        $(e).find("select#sizes").empty()
        //$('select#sizes').empty()
        //$(this).find("select#sizes").val(osize).change();
        console.log(osize);
        var withSize = false;
        $.each(sizes, function (is, s) {
            if (withSize == false) {
                console.log(s)
                if (s.sizeID == "T12" || s.sizeID == "T14") {
                    if (osize == "T12" || osize == "T14") {
                        $(e).find("select#sizes").append('<option value="' + s.sizeID + '">' + s.sizeID + '</option>')
                        withSize = true
                    }
                }
                if (s.sizeID == "T16" || s.sizeID == "T18") {
                    if (osize == "T16" || osize == "T18") {
                        $(e).find("select#sizes").append('<option value="' + s.sizeID + '">' + s.sizeID + '</option>')
                        withSize = true
                    }
                }
                if (s.sizeID == "T20") {
                    console.log('20000')
                    console.log(osize)
                    console.log(s.sizeID)
                    if (osize == "T20") {
                        $(e).find("select#sizes").append('<option value="' + s.sizeID + '">' + s.sizeID + '</option>')
                        withSize = true
                    }
                }
            }

        })

        if (withSize == false) {
            $(e).addClass("withoutSize");

        }
    });
}


function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadDefaultValues() {
    //filtros
    loadComboFilter(dataMarket, 'selectMarketFilter', true, '[ALL]');
    $('#selectMarketFilter').selectpicker('refresh');

    loadComboFilter(dataAllWeek, 'cboSemanaIni', false, '[ALL]');
    $('#cboSemanaIni').selectpicker('refresh');
    //$('#cboSemanaIni').val(ActualyWeek[0].id).change();
    $('#cboSemanaIni').val(dataAllWeek[0].id).change();

    loadComboFilter(dataAllWeek, 'cboSemanaFin', false, '[ALL]');
    $('#cboSemanaFin').selectpicker('refresh');
    $('#cboSemanaFin').val(dataAllWeek[dataAllWeek.length - 1].id).change();

    loadComboFilter(dataStatus, 'selectStatusFilter', true, '[ALL]');
    $('#selectStatusFilter').selectpicker('refresh');
    $('#selectStatusFilter').val('All').change();

    loadComboFilter(dataGrower, 'selectGrowerFilter', true, '[ALL]');
    $('#selectGrowerFilter').selectpicker('refresh');

    var Rejected = dataStatus.filter(item => item.id == 4)
    document.getElementById("Rejected").innerHTML = Rejected[0].quantity;

    var SalesModified = dataStatus.filter(item => item.id == 5)
    document.getElementById("SalesModified").innerHTML = SalesModified[0].quantity;


}

function loadData() {
    var campaignID = localStorage.campaignID;
    var opt = "all";
    var id = '';
    $.ajax({
        type: "GET",
        url: "/PO/GetMarketAll?opt=" + opt + "&id=" + id,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataMarket = dataJson.map(
                    obj => {
                        return {
                            "id": obj.marketID,
                            "name": obj.name
                        }
                    }
                );
            }
        }
    });

    opt = "lwi";
    var val = "";
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    $.ajax({
        type: "GET",
        url: "/Sales/GetAllWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataAllWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    opt = "sta";
    var marketID = '';
    weekIni = 0;
    weekEnd = 0;
    var statusID = '';
    var growerID = '';
    $.ajax({
        type: "GET",
        url: "/PO/GetStatus?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekIni + "&weekFin=" + weekEnd + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {
                        return {
                            "id": obj.statusID,
                            "name": obj.status,
                            "quantity": obj.quantity
                        }
                    }
                );
            }
        }
    });

    opt = 'act';
    val = '';
    $.ajax({
        type: "GET",
        url: "/PO/GetCurrentWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                ActualyWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    opt = "gro";
    $.ajax({
        type: "GET",
        url: "/PO/GetGrowerAll?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekIni + "&weekFin=" + weekEnd + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataGrower = dataJson.map(
                    obj => {
                        return {
                            "id": obj.growerID,
                            "name": obj.grower
                        }
                    }
                );
            }
        }
    });


}

function loadQuantityByStatus() {
    var opt = 'sta';
    var marketID = '';
    var weekini = 0;
    var weekFin = 0;
    var statusID = '';
    var growerID = '';

    $.ajax({
        type: "GET",
        url: "/PO/GetStatus?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + growerID + "&campaignID=" + localStorage.campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {
                        return {
                            "id": obj.statusID,
                            "name": obj.status,
                            "quantity": obj.quantity
                        }
                    }
                );

                //var x1 = dataStatus.filter(item => item.id == 4)[0].quantity;
                //var x2 = dataStatus.filter(item => item.id == 5)[0].quantity;
                //document.getElementById("Rejected").innerHTML = x1;
                //document.getElementById("SalesModified").innerHTML = x2;
            }
        }
    });
}
