﻿var sessionUserID = $('#lblSessionUserID').text()
var _saleID;
var _dataSale;
var _saleRequest;
var _dataSaleDetails;
var content1 = '';

$(function () {
  callWeekpending();
    
  sweet_alert_progressbar_cerrar();

})

$('#ckhFilterSize').change(function () {
  if ($(this).is(':checked') == true) {
    $("table#tblDetailSales2>tbody>tr.withoutSize").show()
  } else {
    $("table#tblDetailSales2>tbody>tr.withoutSize").hide()
  }

});

$(document).on("click", ".row-remove", function () {
  var $idSaleDetail = $(this).parents('tr').children().eq(1).text();
  var $idtable = $(this).parents('table').attr('id')
  $(this).parents('tr').detach();
});

$(document).on("click", ".btn-uncommit", function () {
  //$idSaleDetail = $(this).parents('tr').children().eq(1).text();
  projectedProductionSizeCategorySummaryID = parseInt($(this).parents('tr').find('.projectedProductionSizeCategorySummaryID').text());
  saleDetailSummaryID = parseInt($(this).parents('tr').find('.saleDetailSummaryID').text());
  var $idtable = $(this).parents('table').attr('id');
  var CommitSummary = $(this).closest('tr').find('td.commited').text();

  $("#tblDetailSales>tbody>tr").each(function (i, e) {
    if ($(e).find("td.saleDetailID").text() == saleDetailSummaryID) {
      var commited = parseInt($(e).find("input#Commit").val()) - parseInt(CommitSummary)
      $(e).find("input#Commit").val(commited)

      var pending = parseInt($(e).find("input#Pending").val()) + parseInt(CommitSummary)
      $(e).find("input#Pending").val(pending)
    }
  })

  $("#tblDetailSales2>tbody>tr").each(function (i, e) {
    var projectedProductionSizeCategoryID = parseInt($(e).find("#projectedProductionSizeCategoryID").val());
    if (projectedProductionSizeCategoryID == projectedProductionSizeCategorySummaryID) {

      var boxavailable = $(e).find('td.boxavailable').text();

      $(e).find('td.boxavailable').text(parseInt(boxavailable) + parseInt(CommitSummary));
      $(e).find('#Box').val('0');
      $(e).find('#Pallet').val('0');
      $(e).find('#Kg').val('0');
    }
    //$(e).find('td.boxavailable').text();
  })

  //tblDetailSales
  //$(this).closest('tr').find('#Commit').val('254');
  $(this).parents('tr').detach();

});

$(document).on("click", "button.row-detalle", function () {
  var $idSaleDetail = $(this).parents('tr').children().eq(0).text();
  var $growerID = $(selectGrower).val();
  if ($growerID == '') {
    //alert('Select Grower')
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: "Select a Grower"
    })
    return;
  }

  $("table#tblDetailSales>tbody>tr").removeClass("row-select2");
  $(this).parents('tr').addClass("row-select2");

  var opt = "new";

  $.ajax({
    type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
    url: "/PO/ENGetVarietyForecastDetailForPONew?opt=" + opt + "&growerID=" + $growerID + "&saleDetailID=" + $idSaleDetail,

    //async: true,
    success: function (data) {
      //alert(123)
      _dataSaleDetails = []
      if (data.length > 2) {
        var dataJson = JSON.parse(data);
        _dataSaleDetails = dataJson


      }
      $("#tblDetailSales2>tbody").empty()
      $("#ckhFilterSize").prop("checked", false);
      loadDataDetailSaleTable();
    },
    error: function (xhr, ajaxOptions, thrownError) {
    }
  });

});

$(document).on("click", "#btnConfirm", function () {

  if (!validConfirm()) {
    return;
  }

  if ($("#tblDetailSales3>tbody>tr").length == 0) {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: "Enter boxes",
      //footer: '<a href>Why do I have this issue?</a>'
    })
    return;
  }
  var valid = true;
  var pending = 0
  $("#tblDetailSales>tbody>tr").each(function (i, e) {

    if (parseInt($(e).find('input#Pending').val()) > 0) {
      valid = false
      pending = pending + parseInt($(e).find('input#Pending').val())
    }
  })

  if (valid == false) {

    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: pending + " Boxes pending",
      //footer: '<a href>Why do I have this issue?</a>'
    })
    return;

  }

  Swal.fire({
    title: "Are you sure?",
    text: "Review the data before saving the production order.",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
        savePO();
        Swal.fire("Production Order generated!", {
          icon: "success",
        });
        $(btnConfirm).hide();
        $(accordionDetalle3).hide()
        $(accordionDetalle).hide()
        $(selectWeek).change()
      } else {
        //swal("Your imaginary file is safe!");
      }
    });
});

//$(selectGrower).selectpicker('refresh');)
$(document).on("change", "#selectGrower", function () {
  //$("table#tblDetailSales2>tbody").DataTable().destroy();
  $("table#tblDetailSales2>tbody").empty()
  $("table#tblDetailSales3>tbody").empty();

  $("table#tblDetailSales>tbody>tr").removeClass("row-select2");
  $("table#tblDetailSales>tbody>tr>input#Commit").text(0);

  $("#tblDetailSales>tbody>tr").each(function (i, e) {
    //var boxes = $(e).find('td.boxes').text(); Malon
    var boxes = $(e).find('#boxes').val();
    $(e).find("input#Commit").val(0)

    //var pending = parseInt($(e).find("input#Pending").val()) + parseInt(CommitSummary)
    $(e).find("input#Pending").val(boxes)

  })

  //loadDataDetailSale();
});
$(document).on("change", "#Box", function () {

  //$(this).closest('tr').find('#Kg').val('123')
  var box = parseInt($(this).closest('tr').find('#Box').val());
  var boxavailable = parseInt($(this).closest('tr').find('td.boxavailable').text());
  var boxpallet = parseInt($(this).closest('tr').find('#boxpallet').val());
  var box = parseInt($(this).closest('tr').find('#Box').val());

  //$(this).closest('tr').find('td.boxavailable').text(boxavailableTotal);
  $(this).closest('tr').find('#Pallet').val((box / boxpallet).toFixed(2));
  var kgbox = parseFloat($(this).closest('tr').find('#kgbox').val()).toFixed(2);
  $(this).closest('tr').find('#Kg').val((kgbox * box).toFixed(2));
  //var boxavailableTotal = boxavailable - box;
  //if (boxavailableTotal >= 0) {
  //    $(this).closest('tr').find('td.boxavailable').text(boxavailableTotal);
  //    $(this).closest('tr').find('#Pallet').val(box / boxpallet);
  //    var kgbox = parseFloat($(this).closest('tr').find('#kgbox').val());
  //    $(this).closest('tr').find('#Kg').val(kgbox * box);
  //} else {
  //    sweet_alert_warning('Warning!', 'Quantity exceeds the available stock.');
  //}

})

$('#selectWeek').change(function () {

  if ($(this).val().trim() == '') {
    sweet_alert_error('Error!', 'You must select a week');
    return;
  }
  listSalesPending(this.value.trim())
})

$(document).on("change", "input#boxes", function () {
  var boxes = parseInt($(this).val());
  var tr = $(this).closest('tr');

  var commited = parseInt($(tr).find('td>input#Commit').val())
  var pending = 0;

  if (boxes >= commited) {
    pending = boxes - commited;
    $(tr).find('td>input#Pending').val(pending);
  } else {
    //cristian
    $(tr).find('td>input#boxes').val($(tr).find('td>input#Commit').val())
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: "Invalid amount, undo the commitment first",
      //footer: '<a href>Why do I have this issue?</a>'
    })
  }

  //var pending = parseInt($(tr).find('td>input#Pending').val())

})
function validConfirm() {

    if ($("#tblDetailSales3>tbody>tr").length == 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Enter boxes",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }

    //var totalBoxes = 0;
    //$("#tblDetailSales>tbody>tr>td.boxes").each(function (i, e) {
    //    totalBoxes = totalBoxes + parseInt($(e).find('input').val());
    //})

    //var totalCommited = 0
    //$("#tblDetailSales3>tbody>tr>td.commited").each(function (i, e) {
    //    totalCommited = totalCommited +parseInt($(e).text());
    //})    


    //if (totalBoxes > totalCommited) {
    //    Swal.fire({
    //        icon: 'error',
    //        title: 'Oops...',
    //        text: "Check the total boxes",
    //        //footer: '<a href>Why do I have this issue?</a>'
    //    })
    //    return false;
    //}
    //if (totalBoxes < totalCommited) {
    //    Swal.fire({
    //        icon: 'error',
    //        title: 'Oops...',
    //        text: "Check the total boxes",
    //        //footer: '<a href>Why do I have this issue?</a>'
    //    })
    //    return false;
    //}

    var totalPending = 0
    $("#tblDetailSales>tbody>tr>td>input#Pending").each(function (i, e) {
        totalPending = totalPending + parseInt($(e).val());
    })

    if (totalPending > 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Check pending boxes",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }

    var totalPalletMinimun = 0;
    $("#tblDetailSales>tbody>tr>td>input#palletSuggest").each(function (i, e) {
        totalPalletMinimun = totalPalletMinimun + parseFloat($(e).val());
    })

    var totalPalletCommited = 0
    $("#tblDetailSales3>tbody>tr>td.palletSummary").each(function (i, e) {
        totalPalletCommited = totalPalletCommited + parseFloat($(e).text());
    })

    if (totalPalletMinimun > totalPalletCommited) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "This Order Need " + totalPalletMinimun.toFixed(0) + " Pallets",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }
    if (totalPalletMinimun < totalPalletCommited) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "This Order only Need " + totalBoxes.toFixed(0) + " Pallets",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return false;
    }

    return true;
}

function savePO() {
    var campaignID = localStorage.campaignID;
    var userCreated = $("#lblSessionUserID").text();
    var projectedweekID = $(selectWeek).val();
    var saleID = $(lblSaleID).text();
    var commentary = $(textCommentary).val();
    var frmDet = new FormData();

    frmDet.append("orderProductionID", 0);
    frmDet.append("projectedweekID", projectedweekID);
    frmDet.append("saleID", saleID);
    frmDet.append("specificationID", 0);
    frmDet.append("commentary", commentary);
    frmDet.append("userCreated", userCreated);
    frmDet.append("campaignID", campaignID);

    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "CreateUpdatePO",
        data: JSON.stringify(Object.fromEntries(frmDet)),
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        async: true,
        error: function (datoEr) {
            //flag = false;
        },
        success: function (data) {
            if (data.length > 0) {
                var idPO = data[0].orderProductionID
                savePODet(idPO, userCreated);
                getPO(idPO);

            }
        },
    });
}

function getPO(poID) {
    var opt = "det";
    $.ajax({
        type: "GET",
        url: "/PO/GetConfirmById?opt=" + opt + "&orderProductionID=" + poID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var PO = JSON.parse(data)[0]
                var tr = '<tr>';
                tr += '<td hidden>' + PO.orderProductionID + '</td>';
                tr += '<td>' + PO.requestNumber + '</td>';
                tr += '<td align="center">';
                tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFilePdf(' + PO.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                tr += "</td >";
                tr += '<td align="center">';
                tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFileSI(' + PO.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                tr += "</td >";
                tr += '<td align="center">';
                tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFileSpecs(' + PO.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                tr += "</td >";
                tr += '<td align="center">';
                tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFileBilling(' + PO.orderProductionID + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
                tr += "</td >";
                tr += '<td align="center">';
                tr += "</tr>"
            }

            $('table#tblPO>tbody').append(tr);

            //$("#divPOCheck").show();
        }
    });
}

function savePODet(idPO, userCreated) {
    $('table#tblDetailSales3>tbody>tr').each(function (i, e) {
        var projectedProductionDetailID = $(e).find('td.projectedProductionSizeCategorySummaryID').text()
        var saleDetailID = $(e).find('td.saleDetailSummaryID').text();
        var Quantity = $(e).find('td.commited').text()
        var size = $(e).find('td.sizeSummary').text()

        var frmDet = new FormData();

        frmDet.append("orderProductionDetailID", 0);
        frmDet.append("orderProductionID", idPO);
        frmDet.append("userCreated", userCreated);

        frmDet.append("projectedProductionDetailID", projectedProductionDetailID);
        frmDet.append("saleDetailID", saleDetailID);
        frmDet.append("Quantity", Quantity);

        frmDet.append("sizeID", size);

        $.ajax({
            type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            url: "CreateUpdatePODetail",
            data: JSON.stringify(Object.fromEntries(frmDet)),
            contentType: "application/json",
            Accept: "application/json",
            dataType: 'json',
            async: true,
            error: function (datoEr) {
                //isOK = false;
            },
            success: function (data) {
                //var objData = JSON.parse(data);

            },
        });
    })
}

function listGrowerBySaleId() {
    //$("table#TblPOs tbody").html('');
    var saleID = $(lblSaleID).text();
    loadCombo([], 'selectGrower', true)
    $(selectGrower).selectpicker('refresh');
    $.ajax({
        type: "GET",
        url: "GetForecastBySaleID?saleID=" + saleID,
        async: true,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data).map(obj => {
                    return {
                        "id": obj.growerID,
                        "name": obj.grower
                    }
                });;

                loadCombo(dataJson, 'selectGrower', true)
                $(selectGrower).selectpicker('refresh');
            }
        }
    });

}

function listSalesPending(weekID) {
  sweet_alert_progressbar();
    var opt = "pon";
    var projectedweekID = JSON.parse($("#selectWeek").val());;
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    var campaignID = localStorage.campaignID;
    $('#TblPOs').DataTable().destroy();
    $('#TblPOs>tbody').empty();

    $.ajax({
        //async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListSalesPendingByWeekForPONew?opt=" + opt + "&val=" + weekID + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        error: function (er) {
            sweet_alert_error('Error!', 'There is no information to display according to the selected filters ');
        },
        success: function (data) {
            //console.log('data from week ' + weekID + ': ' + data)
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
                return;
            }
            var dataJson = JSON.parse(data);
            if (dataJson.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
                return;
            }
            $.each(dataJson, function (i, e) {
              var tr = '<tr class="table-tr-agrocom">';
                //if (e.statusid == 'PE') {
                //    tr += '<tr  class="table-warning">';
                //} else if (e.statusid == 'CO') {
                //    tr += '<tr  class="table-success">';
                //} else if (e.statusid == 'CA') {
                //    tr += '<tr  class="table-danger">';
                //}
                tr += '<td style="text-align: center;">'
                tr += '<button class="btn btn-sm btn-primary" style="" onclick = "ShowSRPO(' + e.saleID + ')" data-toggle="modal" data-target="#myModal" > <i class="fas fa-edit fa-sm "></i></button >';
                tr += '<button class="btn btn-sm btn-danger"  style="" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm "></i></button>';
                tr += '</td > ';
                tr += '<td>' + e.saleID + '</td>';
                tr += '<td>' + e.referenceNumber + '</td>';
                tr += '<td>' + e.customer + '</td>';
                tr += '<td>' + e.consignee + '</td>';
                tr += '<td>' + e.destination + '</td>';
                tr += '<td>' + e.market + '</td>';
                tr += '<td>' + e.via + '</td>';
                tr += '<td>' + e.brand + '</td>';
                tr += '<td>' + e.varety + '</td>';
                tr += '<td>' + e.codepack + '</td>';
                tr += '<td>' + e.quantity + '</td>';
                //tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm"></i></button></td>';
                //tr += '<td><button class="btn btn-danger btn-sm" onclick="sendMail(' + e.orderProductionID + ')"><i class="fas fa-envelope"></i></i></button></td>';
                tr += '</tr>';
                $('table#TblPOs tbody').append(tr);
            });
            $('#TblPOs').DataTable({
                "order": [[7, "ASC"]]
            });
        }
    });
  sweet_alert_progressbar_cerrar();
}

function callWeekpending() {
    var idUser = $("#lblSessionUserID").text();
    var opt = "ops";
    var id = "";
    var cropID = "";//localStorage.cropID;
    var campaignID = localStorage.campaignID;
    $.ajax({
        //async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListWeekPending?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID,
        success: function (data) {
            var objData = JSON.parse(data);
            if (objData.length > 0) {
                data = objData.map(
                    obj => {
                        return {
                            "id": obj.projectedweekID,
                            "name": obj.week,
                            //"status": obj.enable
                        }
                    }
                );
                loadCombo(data, 'selectWeek', true)
            } else {
                var contenido = "";
                noData($('table#tblSalesPending>tbody'));

                contenido += "<option value='0'>";
                contenido += "-- No Data -- ";
                contenido += "</option>";
                $("#selectWeek").append(contenido);
            }
        }
    });
}

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function CalcularCommit(xthis) {

    //content = '';
    //var boxpallet = parseInt($('#boxpallet').val());
    var projectedProductionSizeCategoryID = $(xthis).closest('tr').find('#projectedProductionSizeCategoryID').val();
    var saleDetailID = $(xthis).closest('tr').find('#saleDetailID').val();
    var sizes = $(xthis).closest('tr').find('#sizes').val();
    var box = $(xthis).closest('tr').find('#Box').val();
    var Pallet = $(xthis).closest('tr').find('#Pallet').val();
    var Kg = $(xthis).closest('tr').find('#Kg').val();
    var boxavailable = parseInt($(xthis).closest('tr').find('td.boxavailable').text());
    var kgbox = $(xthis).closest('tr').find('#kgbox').val();
    var boxpallet = $(xthis).closest('tr').find('#boxpallet').val();
    var grower = $(xthis).closest('tr').find('td.grower').text()
    var farm = $(xthis).closest('tr').find('td.farm').text()
    var varietyID = $(xthis).closest('tr').find('td.varietyID').text()
    var categoryID = $(xthis).closest('tr').find('td.categoryID').text()
    var variety = $(xthis).closest('tr').find('td.variety').text()
    var category = $(xthis).closest('tr').find('td.category').text()
    var format = $(xthis).closest('tr').find('td.format').text()
    var week = $(xthis).closest('tr').find('td.week').text()
    var growerID = $(xthis).closest('tr').find('td.growerID').text()
    if (box == 0) {
        sweet_alert_warning('Warning!', 'Enter number of boxes');
        return;
    }

    if (box > boxavailable) {
        sweet_alert_warning('Warning!', 'Quantity exceeds the available stock.');
        return;
    }

    if (sizes == "") {
        sweet_alert_warning('Warning!', 'Select size');
        return;
    }
    if (sizes == null) {
        sweet_alert_warning('Warning!', 'Select size');
        return;
    }
    if (sizes == "") {
        sweet_alert_warning('Warning!', 'Select size');
        return;
    }

    var boxPendingSales = 0;
    $("#tblDetailSales>tbody>tr").each(function (itr, tr) {
        saleDetailTableID = $(tr).find('td.saleDetailID').text();
        if (saleDetailID == saleDetailTableID) {
            var lastCommit = parseInt($(tr).find('input#Commit').val());
            var lastPending = parseInt($(tr).find('input#Pending').val());
            var newCommit = lastCommit + parseInt(box);
            //var boxSale = parseInt($(tr).find('td.boxes').text());

            boxPendingSales = parseInt(lastPending) - parseInt(box);
            //if (Commit>=boxavailable  ) {
            //  sweet_alert_warning('Warning!', 'Quantity exceeds the available stock.');
            //  return;
            //}
            if (boxPendingSales >= 0) {
                $(tr).find('input#Commit').val(newCommit);
                $(tr).find('input#Pending').val(boxPendingSales);
            } else {
                if (lastPending == 0) {
                    sweet_alert_warning('Warning!', 'No more boxes needed');
                } else {
                    sweet_alert_warning('Warning!', 'Do not exceed the amount requested. Pending Boxes: ' + lastPending.toString());
                    //if (boxPendingSales == 0) {
                    //    sweet_alert_warning('Warning!', 'No more boxes needed');
                    //} else {
                    //    sweet_alert_warning('Warning!', 'Do not exceed the amount requested. Pending Quantity: 10 kg');
                    //}

                }

                return;
            }
        }

    })
    if (boxPendingSales >= 0) {
        content1 = '';
      content1 += '    <tr class="table-tr-agrocom">';
        content1 += '<td  style="display:none" class="projectedProductionSizeCategorySummaryID" >' + projectedProductionSizeCategoryID + '</td>';
        content1 += '<td  style="display:none" class="saleDetailSummaryID">' + saleDetailID + '</td>';
        content1 += '<td  style="display:none" class="orderProductionDetailID">0</td>';
        content1 += '<td  style="display:none" class="growerID">' + growerID + '</td>';
        content1 += '      <td style="max-width: 125px;min-width: 125px;">' + grower + '</td>';
        content1 += '      <td style="max-width: 100px;min-width: 100px;">' + farm + '</td>';
        content1 += '      <td>' + week + '</td>';
        content1 += '      <td>' + category + '</td>';
        content1 += '      <td style="display:none">' + categoryID + '</td>';
        content1 += '      <td style="max-width: 100px;min-width: 100px;">' + variety + '</td>';
        content1 += '      <td style="display:none">' + varietyID + '</td>';
        content1 += '      <td>' + format + '</td>';
        content1 += '      <td class="sizeSummary">' + sizes + '</td>';
        content1 += '      <td class="commited">' + box + '</td>';
        content1 += '      <td class="kgSummary">' + Kg + '</td>';
        content1 += '      <td class="palletSummary">' + Pallet + '</td>';
        content1 += '     <td ><button class="btn btn-black btn-uncommit" style="color: darkred" onclick=""><i class="fas fa-fast-backward fa-sm"></i></button></td>';
        content1 += '</tr>';

        if ($("#tblDetailSales3>tbody>tr").length == 0) {
            $("#tblDetailSales3>tbody").append(content1);
            //var Commited = $("#tblDetailSales3>tbody>tr").find("td.commited").text();
            //var kgSummary = Kg;//parseInt(Commited) * Kg;
            //var palletSummary = Pallet;//parseInt(Commited) * Kg;
            //$("#tblDetailSales3>tbody>tr").find("td.kgSummary").text(kgSummary);
            //$("#tblDetailSales3>tbody>tr").find("td.palletSummary").text(palletSummary);
        } else {
            var exists = false;
            $("#tblDetailSales3>tbody>tr").each(function (i, e) {

                projectedProductionSizeCategorySummaryID = $(e).find('td.projectedProductionSizeCategorySummaryID').text();
                saleDetailSummaryID = $(e).find('td.saleDetailSummaryID').text();
                sizeSummary = $(e).find('td.sizeSummary').text();

                if ((parseInt(projectedProductionSizeCategorySummaryID) == parseInt(projectedProductionSizeCategoryID)) && (parseInt(saleDetailSummaryID) == parseInt(saleDetailID)) && (sizeSummary == sizes)) {
                    //if (projectedProductionSizeCategorySummaryID == projectedProductionSizeCategoryID) {
                    var Commited = parseInt($(e).find('td.commited').text()) + parseInt(box);
                    var kgSummary = parseFloat(Commited) * parseFloat(kgbox) //parseInt($(e).find('td.kgSummary').text()) + parseInt(Kg);
                    var palletSummary = parseFloat(Commited) / parseFloat(boxpallet)//parseInt($(e).find('td.palletSummary').text()) + parseInt(Pallet);
                    //var kgSummary = parseInt(Commited) * Pallet;
                    //var palletSummary = parseInt(Commited) * Kg;
                    $(e).find('td.commited').text(Commited);
                    $(e).find('td.kgSummary').text(parseInt(kgSummary));
                    $(e).find('td.palletSummary').text(parseFloat(palletSummary).toFixed(2));
                    exists = true;
                }
            })

            if (exists == false) {
                $("#tblDetailSales3>tbody").append(content1);
            }
        }
        $(xthis).closest('tr').find('td.boxavailable').text(boxavailable - box);
    }
}

//function createList(wiNI, wEn) {

//    $("table#TblPOs tbody").html('');
//    $.ajax({
//        type: "GET",
//        url: "ListForConfirm/?WIni=" + "11" + "&WFin=" + "56" + "&userID=" + sessionUserID,
//        async: true,
//        success: function (data) {
//            if (data.length > 0) {
//                var dataJson = JSON.parse(data);
//                $.each(dataJson, function (i, e) {
//                    var tr = '';
//                    if (e.statusid == 'PE') {
//                        tr += '<tr  class="table-warning">';
//                    } else if (e.statusid == 'CO') {
//                        tr += '<tr  class="table-success">';
//                    } else if (e.statusid == 'CA') {
//                        tr += '<tr  class="table-danger">';
//                    }
//                    tr += '<td style="display:none">' + e.orderProductionID + '</td>';
//                    tr += '<td style="font-size: 14px">' + e.requestNumber + '</td>';
//                    tr += '<td style="font-size: 14px">' + e.grower + '</td>';
//                    tr += '<td style="font-size: 14px">' + e.farm + '</td>';
//                    tr += '<td style="font-size: 14px">' + e.week + '</td>';
//                    tr += '<td style="font-size: 14px">' + e.status + '</td>';
//                    tr += '<td style="font-size: 14px">' + e.create + '</td>';
//                    tr += '<td style="display:none">' + e.expireIN + '</td>';
//                    tr += '<td style="display:none">' + e.time + '</td>';

//                    tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" onclick="ShowSRPO(' + e.orderProductionID + ')" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-sm"></i></button><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm"></i></button></td>';
//                    //tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm"></i></button></td>';

//                    //tr += '<td><button class="btn btn-danger btn-sm" onclick="sendMail(' + e.orderProductionID + ')"><i class="fas fa-envelope"></i></i></button></td>';
//                    tr += '</tr>';
//                    $('table#TblPOs tbody').append(tr);
//                })
//            }
//            $('#TblPOs').DataTable({
//                "order": [[7, "ASC"]]
//            });

//        }
//    });

//}

function ShowSRPO(saleID) {
    var cabPO = []
    var opt = "det";
    var val = "";
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    var campaignID = localStorage.campaignID;
    ClearModal()
    $(btnConfirm).show();
    $(accordionDetalle3).show()
    //$(accordionDetalle).show()
    $.ajax({
        type: "GET",
        url: "/Sales/ListSaleDetailsByID?opt=" + opt + "&val=" + saleID + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        async: true,
        success: function (data) {
            if (data == null || data.length == 0) {
                return;
            }
            var dataJson = JSON.parse(data);
            if (dataJson.length == 0) {
                return;
            }
            data = JSON.parse(data);
            $(lblSaleID).text(data[0].saleID)
            $(lblPoID).text(0)
            $(lblSaleRequest).text(data[0].referenceNumber)
            $(lblWeek).text(data[0].week)
            $(lblMarket).text(data[0].market)
            $(lblDestination).text(data[0].destination)
            $(lblCustomer).text(data[0].customer)
            $(lblConsignee).text(data[0].consignee)
            $(txtCommentsSales).val(data[0].commentary)
            LoadDataSaleTable(data);
            listGrowerBySaleId()

        }
    });
}

//function loadDataSale(saleID) {

//    $.ajax({
//        type: "GET",
//        url: "/PO/GetDetailByIDForPOEdit?saleID=" + saleID,
//        async: true,
//        success: function (data) {
//            if (data.length > 2) {
//                var dataJson = JSON.parse(data);
//                _dataSale = dataJson.map(
//                    obj => {
//                        return {
//                            "saleID": obj.saleID,
//                            "nroSALES": obj.nroSALES,
//                            "condition": obj.condition,
//                            "saleDetailID": obj.saleDetailID,
//                            "customer": obj.customer,
//                            "destination": obj.destination,
//                            "consignee": obj.consignee,
//                            "brand": obj.brand,
//                            "varieties": obj.varieties,
//                            "via": obj.via,
//                            "format": obj.format,
//                            "weight": obj.weight,
//                            "boxes": obj.boxes,
//                            "sizes": obj.sizes,
//                            "quantityMinimum": obj.quantityMinimum,
//                            "boxesperpallet": obj.boxesperpallet,
//                            "maxboxes": obj.maxboxes,
//                            "flexible": obj.flexible,
//                            "price": obj.price,
//                            "priceBilling": obj.priceBilling
//                        }
//                    }
//                );

//            }
//            LoadDataSaleTable(_dataSale);
//        },
//        error: function (xhr, ajaxOptions, thrownError) {

//        }
//    });
//}

function ClearModal() {
    $('#tblDetailSales>tbody').html('')
    $('#tblDetailSales2>tbody').html('')
    $('#tblDetailSales3>tbody').html('')
}

function LoadDataSaleTable(dataSale) {
    document.getElementById("labelSaleID").innerHTML = dataSale[0].referenceNumber;
    content = '';
    content += '</tr>';
    $.each(dataSale, function (i, e) {

      content += '<tr class="table-tr-agrocom">';
        content += '<td  style=" display:none" class="saleDetailID" >' + e.saleDetailID + '</td>';
        content += '<td><button class="btn btn-black btn-sm row-detalle" onclick=""><i class="fas fa-arrow-alt-circle-right fa-sm row-detalle"></i></button></td>';
        content += '<td  style="max-width: 100px;min-width: 100px;"> ' + e.category + '</td >';
        content += '<td  style="max-width: 200px;min-width: 200px;"><textarea class="form-control form-control-sm" rows="1">' + e.variety + '</textarea></td>';
        content += '<td ><textarea class="form-control form-control-sm" rows="1">' + e.size + '</textarea></td>';
        content += '<td >' + e.via + '</td>';
        content += '<td  style="max-width: 120px;min-width: 120px;">' + e.codepack + '</td>';
        content += '<td  style="max-width: 80px;min-width: 80px;text-align: right; ">' + e.net + '</td>';
        content += '<td  style="max-width: 80px;min-width: 80px;text-align: right; display:none" ><input id="palletSuggest" class="form-control form-control-sm" type="number" value="' + e.pallets + '" disabled></td>';
        content += '<td  style="max-width: 80px;min-width: 80px;text-align: right; display:none" ><input id="boxesSuggest" class="form-control form-control-sm" type="number" value="' + e.quantity + '" disabled></td>';
        content += '<td  style="max-width: 80px;min-width: 80px;text-align: right; " ><input id="boxesMinimun" class="form-control form-control-sm" type="number" value="' + e.quantityMinimum + '" disabled></td>';
        if (e.flexible == 1) {
            content += '<td  style="max-width: 80px;min-width: 80px;text-align: right;" class="boxes"> <input id="boxes" class="form-control form-control-sm" type="number" onkeypress="return false;" min="' + e.quantityMinimum + '" max="' + e.maxBoxes + '" step="' + e.boxesPerPallet + '" value="' + e.quantity + '"></td>';
        } else {
            content += '<td  style="max-width: 80px;min-width: 80px;text-align: right;" class="boxes"> <input id="boxes" class="form-control form-control-sm" type="number" onkeypress="return false;" min="' + e.quantityMinimum + '" max="' + e.maxBoxes + '" step="' + e.boxesPerPallet + '" value="' + e.quantity + '" disabled></td>';
        }

        content += '<td  style="max-width: 80px;min-width: 80px;"><input id="Commit" value="0" class="form-control form-control-sm" disabled></td>';
        content += '<td  style="max-width: 80px;min-width: 80px;"><input id="Pending" value="' + e.quantity + '" class="form-control form-control-sm" disabled></td>';
        content += '<td  style="max-width: 120px;min-width: 120px;text-align: right;" class=""><input id="txtCustomerPrice" class="form-control form-control-sm" type="number" value="' + e.price + '" disabled></td>';
        content += '<td  style="max-width: 100px;min-width: 100px;text-align: right;" class=""><input id="txtBillingPrice" class="form-control form-control-sm" type="number" value="' + e.priceBilling + '"></td>';

        content += '</tr>';
    })

    $("#tblDetailSales>tbody").empty().append(content);

};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function loadDataDetailSale(saleDetailID) {

    var growerID = $(selectGrower).val();
    if (growerID == '') {
        //alert('Select Grower')
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Select a grower",
            //footer: '<a href>Why do I have this issue?</a>'
        })
        return;
    }

    var opt = "new";
    $.ajax({
        type: "GET",
        url: "/PO/ENGetVarietyForecastDetailForPONew?opt=" + opt + "&growerID=" + growerID + "&saleDetailID=" + saleDetailID,
        //async: true,
        success: function (data) {
            _dataSaleDetails = []
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                _dataSaleDetails = dataJson

            }
            $("#tblDetailSales2>tbody").empty()
            $("#ckhFilterSize").prop("checked", false);
            loadDataDetailSaleTable();
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function loadDataDetailSaleTable() {
    content = '';
    //content += '</tr>';
    var sizes = []
    $.each(_dataSaleDetails, function (index, row) {
      content += '<tr class="table-tr-agrocom">';
        content += '<td  style="display:none"><input id="kgbox"  value="' + row.kgbox + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"/></td>';
        content += '<td  style="display:none"><input id="boxpallet"  value="' + row.boxpallet + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"/></td>';
        content += '<td  style="display:none"><input id="projectedProductionSizeCategoryID"  value="' + row.projectedProductionSizeCategoryID + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"/></td>';
        content += '<td  style="display:none"><input id="saleDetailID"  value="' + row.saleDetailID + '" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"/></td>';
        content += '<td class="growerID" style="display:none">' + row.growerID + '</td>';
        //content += '     <td style="font-size: 14px"><button class="btn btn-black btn-sm row-remove" style="color: darkred" onclick=""><i class="fas fa-trash fa-sm"></i></button></td>';
        content += '<td class="grower" style="max-width:125px;min-width:125px">' + row.grower + '</td>';
        content += '<td class="farm" style="max-width:100px;min-width:100px">' + row.farm + '</td>';
        content += '<td class="week" style="max-width:70px;min-width:70px">' + row.week + '</td>';
        content += '<td class="category" style="max-width:80px;min-width:80px">' + row.brand + '</td>';
        content += '<td class="categoryID" style="display:none">' + row.categoryID + '</td>';
        content += '<td class="variety" style="max-width:80px;min-width:80px">' + row.variety + '</td>';
        content += '<td class="varietyID" style="display:none">' + row.varietyID + '</td>';
        content += '<td class="format" style="display:none">' + row.format + '</td>';
        content += '<td class="size" style="max-width:50px;min-width:50px">' + row.size + '</td>';
        content += '<td style="text-align: right;max-width:100px;min-width:100px">' + parseFloat(row.available).toFixed(0) + '</td>';
        content += '<td style="text-align: right;max-width:100px;min-width:100px" class="boxavailable">' + parseFloat(row.boxavailable).toFixed(0) + '</td>';
        content += '<td  style="max-width:100px;min-width:100px"><input id="Box"  value="0" class=" form-control form-control-sm " step=' + row.boxpallet + ' min="0" max=' + row.boxavailable + '   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number"/></td>';
        content += '<td style="max-width:50px;min-width:50px"><button class="btn btn-black btn-sm" style="color: darkred" onclick="CalcularCommit(this)"><i class="fas fa fa-fast-forward fa-sm"></i></button></td>';
        content += '<td style="min-width: 90px;max-width: 90px;">';
        content += '<select id="sizes" class=" form-control form-control-sm td-width80" >';
        $.each(row.sizes, function (i, s) {
            content += "<option value='" + s.sizeID + "'>";
            content += s.size;
            content += "</option>";
        });
        content += '</select></td>';
        content += '<td  style="min-width: 80px;max-width: 80px" class="td-width80"><input value="0" id="Pallet" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly/></td>';
        content += '<td  style="min-width: 80px;max-width: 80px" class="td-width80"><input value="0" id="Kg" class="form-control form-control-sm" min="0"   onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" readonly/></td>';
        sizes = row.sizes;
    })
    content += '</tr>';

    $('table#tblDetailSales2').DataTable().destroy();
    $("#tblDetailSales2>tbody").empty().append(content);
    //var tabla = $('table#tblDetailSales2').DataTable()
    //tabla.destroy();

    loadSelectionSizeByDefault(sizes)

    //$("table#tblDetailSales2>tbody>tr.withoutSize").hide()
    $('table#tblDetailSales2').DataTable({
        //destroy: true,
        "pageLength": 50
    })
}

function loadSelectionSizeByDefault(sizes) {
    $("#tblDetailSales2>tbody>tr").each(function (i, e) {
        var osize = $(this).find("td.size").text();
        $(e).find("select#sizes").empty()
        var withSize = false;
        $.each(sizes, function (is, s) {
            if (withSize == false) {
                if (s.sizeID == "T12" || s.sizeID == "T14") {
                    if (osize == "T12" || osize == "T14") {
                        $(e).find("select#sizes").append('<option value="' + s.sizeID + '">' + s.sizeID + '</option>')
                        withSize = true
                    }
                }
                if (s.sizeID == "T16" || s.sizeID == "T18") {
                    if (osize == "T16" || osize == "T18") {
                        $(e).find("select#sizes").append('<option value="' + s.sizeID + '">' + s.sizeID + '</option>')
                        withSize = true
                    }
                }
                if (s.sizeID == "T20") {
                    if (osize == "T20") {
                        $(e).find("select#sizes").append('<option value="' + s.sizeID + '">' + s.sizeID + '</option>')
                        withSize = true
                    }
                }
            }

        })

        if (withSize == false) {
            $(e).addClass("withoutSize");

        }
    });
}
