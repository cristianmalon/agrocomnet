﻿$(function () {


    $(btnConfirm).click(function () {
        generatePO()
    })

    callWeekpending()
})
$('#selectWeek').change(function () {
  sweet_alert_progressbar();
  listSalesPending()
})
function btnValidation() {

  if ( ($(lblPoID).val()).length > 0) {
    $(btnConfirm).removeClass();
    $(btnConfirm).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');

  } else {
    $(btnConfirm).removeClass();
    $(btnConfirm).prop("hidden",true);
    //$(btnConfirm).prop("disabled",true);
  }

  }


function generatePO() {
    $(lblPoID).text()
    var o = {
        "orderProductionID": 0,
        "saleID": $(lblSaleID).text(),
        "comments": $(textCommentary).val(),
        "userID": $(lblSessionUserID).text(),
        "cropID": localStorage.cropID,
        "campaignID": localStorage.campaignID
  }
  console.log(JSON.stringify(o))
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "POGenerateGrapes",
        //async: true,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            var tr = '<tr>';
            tr += '<td hidden>' + datares[0].orderProductionID + '</td>';
            tr += '<td>' + datares[0].requestNumber + '</td>';
            tr += '<td align="center">';
            tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFilePdfGrapes(' + datares[0].orderProductionID  + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
            tr += "</td >";
            tr += '<td align="center">';
            tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFileSIGrapes(' + datares[0].orderProductionID  + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
            tr += "</td >";
            tr += '<td align="center" hidden>';
            tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFileSpecsGrapes(' + datares[0].orderProductionID  + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
            tr += "</td >";
            tr += '<td align="center">';
            tr += '<button class="btn btn-sm" style="background-color: transparent; border-color:none; padding: 0 0 0 0" onclick="generateFileBillingGrapes(' + datares[0].orderProductionID  + ')"><i class="fas fa-file-pdf text-danger"></i></button></td>';
            tr += "</td >";
            tr += '<td align="center">';
            tr += "</tr>"
            $("#tblPO>tbody").html(tr)
            //debugger;
          listSalesPending()
          btnValidation();
          //window.location.reload();
          sweet_alert_success('Saved!', 'Data saved successfully.');
        }
    })

}


function callWeekpending() {
    var opt = "ops";
    var id = 0;
    var cropID = localStorage.cropID;
    var campaignID = localStorage.campaignID;
    $.ajax({
        //async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListWeekPending?opt=" + opt + "&id=" + id + "&cropID=" + cropID + "&campaignID=" + campaignID,
        success: function (data) {
           
            var objData = JSON.parse(data);
            if (objData.length > 0) {
                //loadWeekPending(objData);

                data = objData.map(
                    obj => {
                        return {
                            "id": obj.projectedweekID,
                            "name": obj.week,
                            //"status": obj.enable
                        }
                    }
                );
                loadCombo(data, 'selectWeek', true)
            } else {
              sweet_alert_info('Information', 'There is no PO created.');

              var contenido = "";
               // noData($('table#tblSalesPending>tbody'));
                contenido += "<option value='0'>";
                contenido += "-- No Data -- ";
                contenido += "</option>";
                $("#selectWeek").append(contenido);
            }
        }
    });
}

function listSalesPending() {
    var opt = "pon";
    var weekID = $(selectWeek).val()
    var projectedWeekID = JSON.parse($("#selectWeek").val());
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    var campaignID = localStorage.campaignID;
    //$(".transact").attr("disabled", true);
    $('#tableList').DataTable().destroy();
    $('#tableList>tbody').empty();

    $.ajax({
        //async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "ListSalesPendingByWeekForPONew/?opt=" + opt + "&val=" + projectedWeekID + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        error: function (er) {
            //noData($('table#tblSalesPending>tbody'));
        },
        success: function (data) {
            //var objData = JSON.parse(data);
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                $.each(dataJson, function (i, e) {
                    var tr = '<tr>';
                    //if (e.statusid == 'PE') {
                    //    tr += '<tr  class="table-warning">';
                    //} else if (e.statusid == 'CO') {
                    //    tr += '<tr  class="table-success">';
                    //} else if (e.statusid == 'CA') {
                    //    tr += '<tr  class="table-danger">';
                    //}
                  tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" style="" onclick="openModal(' + e.saleID + ')" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-sm"></i></button></td>';

                    tr += '<td style="min-width:50px">' + e.saleID + '</td>';
                    tr += '<td>' + e.referenceNumber + '</td>';
                    tr += '<td>' + e.customer + '</td>';
                    tr += '<td>' + e.consignee + '</td>';
                    tr += '<td>' + e.destination + '</td>';
                    tr += '<td>' + e.via + '</td>';
                    tr += '<td>' + e.brand + '</td>';
                    tr += '<td>' + e.varety + '</td>';                    
                    tr += '<td>' + e.codepack + '</td>';
                    tr += '<td>' + e.quantity + '</td>';

                                     //tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash fa-sm"></i></button></td>';

                    //tr += '<td><button class="btn btn-danger btn-sm" onclick="sendMail(' + e.orderProductionID + ')"><i class="fas fa-envelope"></i></i></button></td>';
                    tr += '</tr>';
                    $('table#tableList tbody').append(tr);
                })
            }
            $('#tableList').DataTable({
              select: true,
              lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'All']
              ],
              dom: 'Bfrtip',
              buttons: [
                {
                  extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
                  exportOptions: {
                    columns: [1,2,4,5,6,7,8,9]
                  }
                },
                'pageLength',

                //'colvis' 
              ],              

            })            

        }
    });

    //if (status != 0) {

    //} else {
    //    noData($('table#tblSalesPending>tbody'));
    //}

    sweet_alert_progressbar_cerrar();

}


function openModal(saleID) {
    var cabPO = []
    var opt = "det";
    var weekIni = 0;
    var weekEnd = 0;
    var userID = 0;
    var campaignID = localStorage.campaignID;
  $(btnConfirm).prop("hidden", false);
  $(btnConfirm).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
    var cabPO = []
    ClearModal()
    $(btnConfirm).show();
    //$(accordionDetalle3).show()
    //$(accordionDetalle).show()
    $.ajax({
        type: "GET",
        url: "/Sales/ListSaleDetailsByID?opt=" + opt + "&val=" + saleID + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
        async: true,
        success: function (data) {
            if (data.length > 0) {
                data = JSON.parse(data);
                $(lblSaleID).text(data[0].saleID)
                $(lblPoID).text(0)
                $(lblSaleRequest).text(data[0].referenceNumber)
                $(lblWeek).text(data[0].week)
                $(lblMarket).text(data[0].market)
                $(lblDestination).text(data[0].destination)
                $(lblCustomer).text(data[0].customer)
                $(lblConsignee).text(data[0].consignee)

                LoadDataSaleTable(data);

                $('#accordionRequest').collapse('show')
            }
        }
    });
}

function LoadDataSaleTable(dataSale) {
    document.getElementById("labelSaleID").innerHTML = dataSale[0].referenceNumber;
    content = '';
    content += '</tr>';
    $.each(dataSale, function (i, e) {

        content += '<tr >';
        content += '<td  style="font-size: 12px; display:none" class="saleDetailID" >' + e.saleDetailID + '</td>';
        content += '<td style="font-size: 12px"><button class="btn btn-black btn-sm row-detalle" onclick=""><i class="fas fa-arrow-alt-circle-right fa-sm row-detalle"></i></button></td>';
        content += '<td  style="font-size: 12px;max-width: 120px;min-width: 120px;"> ' + e.category + '</td >';
        content += '<td  style="font-size: 12px">' + e.variety + '</td>';
        
        content += '<td  style="font-size: 12px;max-width: 120px;min-width: 120px;">' + e.codepack2 + '</td>';
        content += '<td  style="font-size: 12px;max-width: 80px;min-width: 80px;text-align: right; ">' + e.net + '</td>';
        content += '<td  style="font-size: 12px;max-width: 80px;min-width: 80px;text-align: right;" class="boxes"> <input id="boxes" class="form-control form-control-sm" type="number" onkeypress="return false;" min="' + e.quantityMinimum + '" max="' + e.maxBoxes + '" step="' + e.boxesPerPallet + '" value="' + e.quantity + '" disabled></td>';
        content += '<td  style="font-size: 12px;max-width: 80px;min-width: 80px;text-align: right;" ><input id="palletSuggest" class="form-control form-control-sm" type="number" value="' + e.pallets + '" disabled></td>';

        content += '<td  style="font-size: 12px;max-width: 120px;min-width: 120px;text-align: right;" class=""><input id="txtCustomerPrice" class="form-control form-control-sm" type="number" value="' + e.price + '" disabled></td>';
        content += '<td  style="font-size: 12px;max-width: 120px;min-width: 120px;text-align: right;" class=""><input id="txtBillingPrice" class="form-control form-control-sm" type="number" value="' + e.price + '" disabled></td>';

        
        content += '</tr>';
    })

    $("#tblDetailSales>tbody").empty().append(content);

};


function ClearModal() {
    $('#tblDetailSales>tbody').html('')
    $('#tblDetailSales2>tbody').html('')
    $('#tblDetailSales3>tbody').html('')
    $('#tblPO>tbody').html('')
    
}