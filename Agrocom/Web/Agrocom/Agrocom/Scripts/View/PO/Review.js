﻿var dataPlant = [];
var sessionUserID = "";
var _perfil = localStorage.perfil;

$(function () {
    sessionUserID = $(lblSessionUserID).text()
    loadData();
    loadDefaultValues();
  loadDataWeek();
  sweet_alert_progressbar_cerrar();

  $(document).on("click", "#btnSearch", function () {
    sweet_alert_progressbar();
        createList()
    })
})

function loadData() {
    var opt = "all";
    var id = '';
    $.ajax({
        type: "GET",
        url: "/PO/AllPlant?opt=" + opt + "&id=" + id,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataPlant = dataJson.map(
                    obj => {
                        return {
                            "id": obj.packingID,
                            //"name": obj.grower
                            "name": obj.packing
                        }
                    }
                );
            }
        }
    });

}

function loadDataWeek() {
    $('#cboSemanaIni').empty()
    $('#cboSemanaFin').empty()
    $.ajax({
        type: "GET",
        url: "/forecast/ListWeekByCampaign?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                $.each(dataJson, function (i, item) {
                    $('#cboSemanaIni')
                        .append($("<option></option>")
                            .attr("value", item.projectedWeekID)
                            .text(item.description));
                    $('#cboSemanaFin')
                        .append($("<option></option>")
                            .attr("value", item.projectedWeekID)
                            .text(item.description));
                })
                $('#cboSemanaIni').selectpicker('refresh');
                $('#cboSemanaFin').selectpicker('refresh')
                loadStatusConfirm();
            }
        }
    });
}

function loadDefaultValues() {

    loadComboFilter(dataPlant, 'selectPlant', true, '[ALL]');
    $('#selectPlant').selectpicker('refresh');

}

function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    if (data != null)
        for (var i = 0; i < data.length; i++) {
            content += "<option value='" + data[i].id + "'>";
            content += data[i].name;
            content += "</option>";
        }
    $('#' + control).empty().append(content);
}


function loadStatusConfirm() {
    $('#selectStatusFilter').empty()

    var opt = "act";
    var plantID = '';
    var weekProduction = 0;
    var weekProductionEnd = 0;
    var statusConfirmID = 0;
    var userID = 0;
    $.ajax({
        type: "GET",
        url: "/PO/GetPOstatusConfirm?opt=" + opt + "&plantID=" + plantID + "&weekProduction=" + weekProduction + "&weekProductionEnd=" + weekProductionEnd + "&statusConfirmID=" + statusConfirmID + "&campaignID=" + localStorage.campaignID + "&userID=" + userID,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                
                $('#selectStatusFilter')
                    .append($("<option value='0'>[ALL]</option>"))

                $.each(dataJson, function (i, item) {
                    $('#selectStatusFilter')
                        .append($("<option></option>")
                            .attr("value", item.statusConfirmID)
                            .text(item.statusConfirm));
                })
                $('#selectStatusFilter').selectpicker('refresh')
            }
        }
    });
}

function createList() {
    var marketID = ''
    var campaignID = localStorage.campaignID;
    var weekini = $('#cboSemanaIni').val()
    var weekFin = $('#cboSemanaFin').val()
    var plantID = $("#selectPlant").val();
    if (plantID == 'All') {
        plantID = '';
    }
    var statusID = $('#selectStatusFilter').val()
    if (statusID == 'All') {
        statusID = 0;
    }
    $('#tableList').DataTable().clear().destroy();
    $("table#tableList>tbody").empty();
    var opt = "rvw";
    var marketID = '';
    $.ajax({
        type: "GET",
        url: "/PO/GetListPOReview?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + plantID + "&campaignID=" + campaignID,
        //async: true,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                $.each(dataJson, function (i, e) {
                    var tr1 = '';
                    tr1 += '<tr class="table-tr-agrocom">';
                    tr1 += '<th class="text-white" style="display:none">Order Production ID</th>';
                    tr1 += '<th class="text-white" style="max-width:120px;min-width:120px;">Sales Order</th>';
                    tr1 += '<th class="text-white" style="max-width:120px;min-width:120px;text-align: center;">Status</th>';
                    tr1 += '<th class="text-white" style="max-width:135px;min-width:135px;">Plant</th>';
                    tr1 += '<th class="text-white" style="max-width:50px;min-width:50px;">Priority</th>';
                    tr1 += '<th class="text-white" style="display:none">Grower</th>';
                    tr1 += '<th class="text-white" style="display:none">Departure Week</th>';
                    tr1 += '<th class="text-white" style="max-width:200px;min-width:200px;">Customer</th>';
                    tr1 += '<th class="text-white" style="max-width:200px;min-width:200px;">Program</th>';
                    tr1 += '<th class="text-white" style="display:none">Market</th>';
                    tr1 += '<th class="text-white" style="max-width:120px;min-width:120px;">Destination</th>';
                    tr1 += '<th class="text-white" style="display:none">Via</th>';
                    tr1 += '<th class="text-white" style="max-width:150px;min-width:120px;">Variety</th>';
                    tr1 += '<th class="text-white" style="max-width:70px;min-width:60px;">Category</th>'; 
                    tr1 += '<th class="text-white" style="max-width:85px;min-width:85px;">Weight</th>';
                    tr1 += '<th class="text-white" style="max-width:100px;min-width:50px;">Brand</th>';                    
                    tr1 += '<th class="text-white" style="max-width:180px;min-width:135px;">CodePack</th>';
                    tr1 += '<th class="text-white" style="max-width:50px;min-width:50px;">Boxes</th>';                    
                    tr1 += '<th class="text-white">[S.O.]</th>';
                    tr1 += '<th class="text-white">[S.I.]</th>';
                    if (_perfil == 'COM' || _perfil == 'COX' || _perfil == 'DEM') {
                        tr1 += '<th class="text-white">[B.I.]</th>';
                    } else {
                        tr1 += '<th class="text-white" hidden>[B.I.]</th>';
                    }                    
                    tr1 += '<th class="text-white">[A.P.]</th>';
                    tr1 += '</tr>';
                    $("table#tableList thead").empty().append(tr1);

                    var color = '';
                    if (e.statusConfirmID == 1) {
                        color = 'badge-warning';
                    }                    
                    if (e.statusConfirmID == 2) {
                        color = 'badge-info';
                    }
                    if (e.statusConfirmID == 3) {
                        color = 'badge-primary';
                    }
                    if (e.statusConfirmID == 4) {
                        color = 'badge-danger';
                    }
                    if (e.statusConfirmID == 5) {
                        color = 'badge-success';
                    }
                    if (e.statusConfirmID == 7) {
                        color = 'badge-secondary';
                    }
                    if (e.statusConfirmID == 8) {
                        color = 'badge-secondary';
                    }

                    var tr = '<tr ';
                    if (e.statusConfirmID == 1) {
                        tr += 'class="table-warning">';
                    } else if (e.statusConfirmID == 2) {
                        tr += 'class="table-info">';
                    } else if (e.statusConfirmID == 3) {
                        tr += 'class="table-primary">';
                    } else if (e.statusConfirmID == 4) {
                        tr += 'class="table-danger">';
                    } else if (e.statusConfirmID == 5) {
                        tr += 'class="table-success">';
                    } else if (e.statusConfirmID == 7) {
                        tr += 'class="table-secondary">';
                    } else if (e.statusConfirmID == 8) {
                        tr += 'class="table-secondary">';
                    }

                    tr += ' >'

                    tr += '<td style="display:none">' + e.orderProductionID + '</td>';
                    tr += '<td style="font-size: 12px;max-width:120px;min-width:120px;">' + e.orderProduction + '</td>';
                    tr += '<td style="font-size: 14px;max-width:120px;min-width:120px;text-align: center;" ><div class="badge ' + color + ' badge-pill">' + e.statusConfirm + '</div></td>';
                    tr += '<td style="font-size: 12px;max-width:135px;min-width:135px;"><textarea style="font-size:12px" class="form-control form-control-sm" id="packing" rows="1" readonly>' + e.packing + '</textarea></td>';
                    tr += '<td style="font-size: 12px;max-width:50px;min-width:50px;">' + e.priority + '</td>';                    
                    tr += '<td style="display:none">' + e.grower + '</td>';
                    tr += '<td style="display:none">' + e.week + '</td>';
                    tr += '<td style="font-size: 12px;max-width:200px;min-width:200px;"><textarea style="font-size:12px" class="form-control form-control-sm" id="customer" rows="1" readonly>' + e.customer + '</textarea></td>';
                    tr += '<td style="font-size: 12px;max-width:200px;min-width:200px;"><textarea style="font-size:12px" class="form-control form-control-sm" id="program" rows="1" readonly>' + e.program + '</textarea></td>';
                    tr += '<td style="display:none">' + e.market + '</td>';
                    tr += '<td style="font-size: 12px;max-width:120px;min-width:120px;">' + e.destination + '</td>';
                    tr += '<td style="display:none">' + e.via + '</td>';
                    tr += '<td style="font-size: 12px;max-width:150px;min-width:120px;"><textarea style="font-size:12px" class="form-control form-control-sm" id="variety" rows="1" readonly>' + e.variety + '</textarea></td>';
                    tr += '<td style="font-size: 12px;max-width:70px;min-width:60px;">' + e.category + '</td>'; 
                    tr += '<td style="font-size: 12px;max-width:85px;min-width:85px;">' + e.weight + '</td>';
                    tr += '<td style="font-size: 12px;max-width:100px;min-width:50px;">' + e.label + '</td>';                    
                    tr += '<td style="font-size: 12px;max-width:180px;min-width:135px;"><textarea style="font-size:12px" class="form-control form-control-sm" id="codepack" rows="1" readonly>' + e.codepack + '</textarea></td>';
                    tr += '<td style="font-size: 12px;max-width:50px;min-width:50px;">' + e.boxes + '</td>'; 
                    tr += '<td style="text-align: center;"><button class="btn btn-sm btn-danger" onclick="generateFilePdfGrapes(' + e.orderProductionID + ')" style=""><i class="fas fa-file-pdf "></i></button></td>';
                    tr += '<td style="text-align: center;"><button class="btn btn-sm btn-danger" onclick="generateFileSIGrapes(' + e.orderProductionID + ')" style=""><i class="fas fa-file-pdf "></i></button></td>';
                    if (_perfil == 'COM' || _perfil == 'COX' || _perfil == 'DEM') {
                        tr += '<td style="text-align: center;"><button class="btn btn-sm btn-danger" style="" onclick="generateFileBillingGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf "></i></button></td>';
                    } else {
                        tr += '<td style="text-align: center;" hidden></td>';
                    }                    
                    tr += '<td style="text-align: center;"><button class="btn btn-sm btn-danger" style="" onclick="generateFileSpecsGrapes(' + e.orderProductionID + ')"><i class="fas fa-file-pdf "></i></button></td>';
                    tr += '</tr>';
                    $('table#tableList tbody').append(tr);

                })

                $("#tableList").dataTable({
                    select: true,
                    lengthMenu: [
                        [10, 25, 50, -1],
                        ['10 rows', '25 rows', '50 rows', 'All']
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
                            }
                        },
                        'pageLength',

                        //'colvis' 
                    ],


                })

            }
        }
    })
  sweet_alert_progressbar_cerrar();
}