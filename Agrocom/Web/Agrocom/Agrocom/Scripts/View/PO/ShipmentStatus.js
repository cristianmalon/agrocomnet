﻿//var uriWs = 'http://localhost/api/';
var gPOID = "";
var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
var SAS_TOKEN = "?sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2021-01-01T04:59:59Z&st=2020-04-06T00:57:30Z&spr=https&sig=0gjOhB1iFXgblSVElnm1Z9w3hAwUvDpFNTqu7J2RMWw%3D";



function fillCombosWeek(data, control1, control2, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    $.each(data, function (i, week) {
        contenido += "<option value='" + week.projectedWeekID + "'>";
        contenido += week.description;
        contenido += "</option>";
    });
    control1.innerHTML = contenido;
    control2.innerHTML = contenido;
    $("#cboSemanaIni").val(data[0].projectedWeekID).change();
    $('#cboSemanaIni').data('pre', data[0].projectedWeekID);

    $("#cboSemanaFin").val(data[0].projectedWeekID).change();
    $('#cboSemanaFin').data('pre', data[0].projectedWeekID);
    ListaSst();
}
function ControlWeek1() {

    var week1 = $("#cboSemanaIni").val();
    var week2 = $("#cboSemanaFin").val();
    console.log("CURRENT1: " + week1 + " //CURRENT2: " + week2);
    if (week1 > week2) {
        $("#cboSemanaFin").val(week1).change();
    } else {
        $("#cboSemanaFin").val(week2).change();
    }

}
//function ControlWeek2() {
//    var week1 = $("#cboSemanaIni").val();
//    var week2 = $("#cboSemanaFin").val();
//    console.log("CURRENT1: " + week1 + " //CURRENT2: " + week2);
//    if (week2 < week1) {
//        alert("Check date ranges !")
//        $("#cboSemanaFin").val(week1).change();
//    }
//}
//function GetWeekDate(weekID) {
//    var ObjData;
//    //$.get("Forecast/GetWeekByID/?idWeek=" + weekID, function (data) {
//    //    var dato = objData = JSON.parse(data);
//    //    //console.log("WEEEEK 1: " + JSON.stringify(objData) + "//INITIAL: " + objData[0].initial);
//    //    objData = dato;
//    //    fn(objData);
//    //});
//    $.ajax({
//        async: false,
//        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//        url: "GetWeekByID/?idWeek=" + weekID,
//        success: function (data) {
//            ObjData = JSON.parse(data);
//            console.log(JSON.parse(data));
//        }
//    });
//    return ObjData;
//}
function fillComboSeasonSst(data, control, primerElemento) {
    var contenido = "";
    if (primerElemento == true) {
        contenido += "<option value='0' selected>--Seleccione--</option>";
    }
    console.log(JSON.stringify(data));
    for (var i = 0; i < data.length; i++) {
        contenido += "<option value='" + data[i].campaignID + "'>";
        contenido += data[i].description;
        contenido += "</option>";
        console.log("SEASON ID: " + JSON.stringify(data[i]));
    }
    control.innerHTML = contenido;
    //$("#cboSeason").val(data[0].campaignID);
    $("#cboSeason").val(data[0].campaignID).change();
    var idseasonval = $("#cboSeason").val();
    console.log("SEASON Id val:" + idseasonval);
    loadCombosWeek();
    //loadComboGrowerF();
    //loadComboListLots();
}
function loadComboSeasonSst() {
    console.log("ENTRADA A CARGA SEASON");
    $.get("ListSeason", function (data) {
        var objData = JSON.parse(data);
        fillComboSeasonSst(objData, document.getElementById("cboSeason"), false)
    })
}
//function loadCombosWeek() {
//    $.get("Forecast/ListWeekByCampaign/?idCampaign=" + $("#cboSeason").val(), function (data) {
//        var ObjData = (JSON.parse(data));
//        //console.log("WEEK DATA: " + JSON.stringify(ObjData));
//        llenarCombo(ObjData, document.getElementById("cboSemanaIni"), false)
//        llenarCombo(ObjData, document.getElementById("cboSemanaFin"), false)
//        var checkBox = document.getElementById("chkweekly");
//        var week1 = $("#cboSemanaIni").val();
//        var week2 = $("#cboSemanaFin").val();
//        //searchFct(checkBox.checked, week1, week2);
//    })
//}
function loadCombosWeek() {
    $.get("ListWeekByCampaign/?idCampaign=" + $("#cboSeason").val(), function (data) {
        var ObjData = JSON.parse(data);
        console.log("ObjeData: " + (data) + " DATA: " + JSON.stringify(ObjData))
        fillCombosWeek(ObjData, document.getElementById("cboSemanaIni"), document.getElementById("cboSemanaFin"), false);
        //fillCombosWeek(data, document.getElementById("cboSemanaFin"), false);
    });

}

!function ($) {
    $(function () {
        //loadCombosWeek();
        loadComboSeasonSst()
        //loadCombosWeek();

    });

}(window.jQuery);
function ListaSst() {
    //$.get(uriWs + "PO/GetAllFiltered/?WeekIniid=" + "11" + "&WeekEndId=" + "34", function (data) {

    var idUser = $('#lblSessionUserID').text();
    var idCboIni = $("#cboSemanaIni").val();
    var idCboFin = $("#cboSemanaFin").val();
    console.log("ID DE USUARIO PRINCIPAL: " + idUser + "IDCBOINI: " + idCboIni + "IDCBOFIN: " + idCboFin);
    //$.get(uriWs + "ShipmentSt/ShipmentStPOByWeekId/?idUser=" + idUser + "&beginDate=" + idCboIni + "&endDate=" + idCboFin, function (data) {
    //    //crearListadoStockGrower(["Id", "GROWER", "AVAILABLE", "WEEK", "KG", "SIZE", "VARIETY", "BRAND", "ORIGIN"], data);
    //    crearListado(data);
    //});
    //var url = "ShipmentStPOByWeekId/?idUser=" + idUser + "&beginDate=" + idCboIni + "&endDate=" + idCboFin;
    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        error: function (error) {
            console.log("ERROOOR: " + error);
        },
        url: "ShipmentStPOByWeekId/?idUser=" + idUser + "&beginDate=" + idCboIni + "&endDate=" + idCboFin,
        success: function (data) {
            //ListModalUpload(data, poID);
            var ObjData = JSON.parse(data);
            console.log("DATA: " + JSON.stringify(data));
            console.log("ObjData: " + JSON.stringify(ObjData));
            if (data.length > 0) {
                crearListadoSst(ObjData);
            } else {
                noData();
            }
        }
    });
}
function noData() {
    var contenido = "";
    console.log(" NO  HAY DATOS ");
    contenido += "<div>";
    contenido += "    <table class='table table - striped jambo_table bulk_action' cellspacing='0' rules='all' border='1' id='ContentPlaceHolder1_datos' style='border - collapse: collapse; '>";
    contenido += "        <tbody><tr class='headings' align='center' valign='middle' style='color:White;background-color:#405467;font-weight:bold;' >";
    contenido += "            <th scope='col'>MESSAGE</th>";
    contenido += "        </tr><tr class='even pointer' align='center'>";
    contenido += "<td>Without results.</td>";
    contenido += "            </tr>";
    contenido += "        </tbody></table>";
    contenido += "</div>";
    document.getElementById("tabla").innerHTML = contenido;
}

function crearListadoSst(dataDetails) {
    var contenido = "";
    console.log("CARGA VALUE UPLOAD: " + JSON.stringify(dataDetails) + "VALUE LENGHT: " + dataDetails.length);

    var arryColumnsDetail = Object.keys(dataDetails[0]);
    contenido += "<tr style='display: none;'><td colspan=12>";
    contenido += "<table  id='TableModal' class='table table-hover table-bordered table-bordered ' >";
    contenido += "<thead class='text-white' style='background-color: #13213d;'>";
    contenido += "<tr>";
    //var myTableModal = $('#TableModal').DataTable();
    console.log("arryColumnsDetail: " + arryColumnsDetail);


    //CARGAMOS HEADERS DE TABLA

    //$.each(arryColumnsDetail, function (index, item) {

    //        contenido += "<th>" + item + "</th>";
    //});

    for (var z = 1; z < arryColumnsDetail.length; z++) {
        contenido += "<th>";
        contenido += arryColumnsDetail[z].toUpperCase();
        contenido += "</th>";
    }
    contenido += "<th>";
    contenido += "Attach";
    contenido += "</th>";
    contenido += "</tr>";
    contenido += "</thead>";
    // CARGAMOS CUERPO
    contenido += "<tbody id='bodyTable'>";
    console.log("data object: " + JSON.stringify(dataDetails));

    $.each(dataDetails, function (index, item) {
        if (item.status == '1') {
            contenido += "<tr class='table-danger '>";
        };
        if (item.status == '2') {
            contenido += "<tr  class='table-success '>";
        };
        if (item.status == '3') {
            contenido += "<tr  class='table-warning '>";
        };
        contenido += "<td >" + item.po + "</td>";
        contenido += "<td >" + item.farm + "</td>";
        contenido += "<td >" + item.destino + "</td>";
        contenido += "<td >" + item.week + "</td>";
        contenido += "<td >" + item.consignee + "</td>";
        contenido += "<td >" + item.notify + "</td>";
        contenido += "<td >" + item.incoterm + "</td>";
        contenido += "<td >" + item.via + "</td>";
        contenido += "<td >" + item.weeK_FINAL + "</td>";
        contenido += "<td >" + item.datE_BOARD + "</td>";
        if (item.status=='1') {
            contenido += "<td >" + "PENDING" + "</td>";
        }if (item.status=='2') {
            contenido += "<td >" + "COMPLETED" + "</td>";
        }if (item.status=='3') {
            contenido += "<td >" + "WITH FILES"+ "</td>";
        }
        
        if (item.status == '1' || item.status == '3') {
            contenido += "<td style='text-align:center'>";
            //contenido += "<button class='btnUpload' type='button' value='" + dataDetails[i][llavesDet[0]] + "' >UPLOAD</button>";
            contenido += "<button class='btnUpload btn-sm btn-dark' type='button' style='background-color: #13213d;' value='" + item.orderProductionID + "'  data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'><span class='icon text-white'><i class='fas fa-upload'></i></span></button>";
            contenido += "</td>";
        } else {
            contenido += "<td style='text-align:center'>";
            contenido += "<button class='btnView btn-sm btn-dark' type='button' style='background-color: #13213d;' value='" + item.orderProductionID + "' data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'><span class='icon text-white'><i class='fas fa-eye'></i></span></button>";
            contenido += "</td>";
            //contenido += "<button class='btnView btn- btn-sm btn-dark' type='button' value='" + item.orderProductionID + "' data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'>VIEW</button>";
            //contenido += "</td>";
        };
        contenido += "</tr>"
    });

    //for (var i = 0; i < dataDetails.length; i++) {
    //    //contenido += "<tr>";
    //    //if (dataDetails[i][llavesDet[6]] == 'CO' || dataDetails[i][llavesDet[6]] == 'PE') {
    //    if (dataDetails[i][llavesDet[11]] == '1') {
    //        contenido += "<tr style='background-color: pink'>";
    //        for (var j = 0; j < llavesDet.length - 1; j++) {
    //            //if (dataDetails[i][llavesDet[10]] == '1') {
    //            var valorLLavesDet = llavesDet[j];
    //            contenido += "<td >";
    //            contenido += dataDetails[i][valorLLavesDet];
    //            contenido += "</td>";
    //            //}
    //        }
    //        console.log("VALUE OPTIONS: " + dataDetails[i][llavesDet[0]]);
    //        contenido += "<td >";
    //        contenido += "<button class='btnUpload' type='button' value='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'>UPLOAD</button>";
    //        //contenido += "<button class='btnUpload' type='button' value='" + dataDetails[i][llavesDet[0]] + "' onclick='loadModal()' data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'>UPLOAD</button>";
    //        contenido += "</td>";
    //    }
    //}
    //for (var i = 0; i < dataDetails.length; i++) {
    //    if (dataDetails[i][llavesDet[11]] == '3') {
    //        contenido += "<tr style='background-color: #EFF764'>";
    //        for (var j = 0; j < llavesDet.length - 1; j++) {
    //            //if (dataDetails[i][llavesDet[9]] == '3') {
    //            var valorLLavesDet = llavesDet[j];
    //            contenido += "<td >";
    //            contenido += dataDetails[i][valorLLavesDet];
    //            contenido += "</td>";
    //            //}
    //        }
    //        contenido += "<td >";
    //        //contenido += "<button class='btnUpload' type='button' value='" + dataDetails[i][llavesDet[0]] + "' >UPLOAD</button>";
    //        contenido += "<button class='btnUpload' type='button' value='" + dataDetails[i][llavesDet[0]] + "'  data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'>UPLOAD</button>";
    //        contenido += "</td>";
    //    }
    //}
    //for (var i = 0; i < dataDetails.length; i++) {
    //    if (dataDetails[i][llavesDet[11]] == '2') {
    //        contenido += "<tr style='background-color: #84F764'>";
    //        for (var j = 0; j < llavesDet.length - 1; j++) {
    //            //if (dataDetails[i][llavesDet[9]] == '2') {
    //            var valorLLavesDet = llavesDet[j];
    //            contenido += "<td >";
    //            contenido += dataDetails[i][valorLLavesDet];
    //            contenido += "</td>";
    //            //}
    //        }
    //        contenido += "<td >";
    //        contenido += "<button class='btnView' type='button' value='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' data-backdrop='static' data-keyboard='false'>VIEW</button>";
    //        contenido += "</td>";
    //    }
    //}
    //contenido += "</tr>";

    contenido += "</tbody>";
    contenido += "</table>";
    contenido += "</tr>";
    document.getElementById("tabla").innerHTML = contenido;
    $("#TableModal").dataTable();
}
function ListTableModal(poID) {
    var type, tbl, description;
    console.log("IN LISTAMODAL");
    var url = "LoadDocuments";
    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: url,
        success: function (data) {
            var ObjData = JSON.parse(data);
            console.log("Data DOCS: " + data + "//ParsedData DOCS: " + JSON.stringify(ObjData) + " POID: " + poID);
            ListModalUpload(ObjData, poID);
        }
    });
    //$.get(uriWs + "ShipmentSt/DocumentsType/", function (data) {
    //    //crearListadoStockGrower(["Id", "GROWER", "AVAILABLE", "WEEK", "KG", "SIZE", "VARIETY", "BRAND", "ORIGIN"], data);

    //})
}
function POModalShipmentST(POID) {
    console.log("POID MODAL: "+POID)
    var url = "GetPOShippingStatusById/?typeID=" + POID;
    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: url,
        success: function (data) {
            console.log("PO DATA: " + JSON.stringify(data));
            var ObjData = JSON.parse(data);
            ListModalUploadPO(ObjData);
        }
    });
    //$.get(uriWs + "ShipmentSt/DocumentsType/", function (data) {
    //    //crearListadoStockGrower(["Id", "GROWER", "AVAILABLE", "WEEK", "KG", "SIZE", "VARIETY", "BRAND", "ORIGIN"], data);

    //})
}
function ListModalUploadPO(dataDetails) {
    var contenido = "";
    var arryColumnsDetail = (dataDetails[0]);
    console.log("data objectPO: " + JSON.stringify(dataDetails));
    //CARGAMOS HEADERS DE TABLA
    for (var a = 1; a < arryColumnsDetail.length; a++) {
        for (var i = 0; i < dataDetails.length; i++) {
            if (a == 1) {
                contenido += "<div style='position: relative; width: 98%; align-items: center; float:right'>";
                contenido += "<label class='label' style='position: relative; width: 12%; float:left'>Prod. Order</label>";
                contenido += "<label class='label' style='position: relative; width: 2%; float:left'> : </label>";
                var valorLLavesDet = arryColumnsDetail[a];
                contenido += "<label class='label' style='position: relative; width: 83%; float:left'>" + dataDetails[i][valorLLavesDet] + "</label>";
                contenido += "</div>";
            }
            if (a == 2) {
                contenido += "<div style='position: relative; width: 98%; align-items: center; float:right'>";
                contenido += "<label class='label' style='position: relative; width: 12%; float:left'>Farm</label>";
                contenido += "<label class='label' style='position: relative; width: 2%; float:left'> : </label>";
                var valorLLavesDet = arryColumnsDetail[a];
                contenido += "<label class='label' style='position: relative; width: 83%; float:left'>" + dataDetails[i][valorLLavesDet] + "</label>";
                contenido += "</div>";
            }
            if (a == 3) {
                contenido += "<div style='position: relative; width: 98%; align-items: center; float:right'>";
                contenido += "<label class='label' style='position: relative; width: 12%; float:left'>Destination</label>";
                contenido += "<label class='label' style='position: relative; width: 2%; float:left'> : </label>";
                var valorLLavesDet = arryColumnsDetail[a];
                contenido += "<label class='label' style='position: relative; width: 83%; float:left'>" + dataDetails[i][valorLLavesDet] + "</label>";
                contenido += "</div>";
            }
            //else {
            if (a == 5) {
                contenido += "<div style='position: relative; width: 98%; align-items: center; float:right'>";
                contenido += "<label class='label' style='position: relative; width: 12%; float:left'>Client</label>";
                contenido += "<label class='label'style='position: relative; width: 2%; float:left'> : </label>";
                var valorLLavesDet = arryColumnsDetail[a];
                contenido += "<label class='label' style='position: relative; width: 83%; float:left'>" + dataDetails[i][valorLLavesDet] + "</label>";
                contenido += "</div>";
            }
            if (a == 10) {
                contenido += "<div style='position: relative; width: 98%; align-items: center; float:right'>";
                contenido += "<label class='label' style='position: relative; width: 12%; float:left'>ETD</label>";
                contenido += "<label class='label'style='position: relative; width: 2%; float:left'> : </label>";
                var valorLLavesDet = arryColumnsDetail[a];
                contenido += "<label class='label' style='position: relative; width: 83%; float:left'>" + dataDetails[i][valorLLavesDet] + "</label>";
                contenido += "</div>";
            }
            //}
        }
    }
    //document.getElementById("uploadModalTbl1").innerHTML = contenido;
}


function ListModalUpload(dataDetails, poID) {
    var contenido = "";
    //var datosDoc = [];
    var url = "LoadPODocsByID/?typeID=" + poID;
    gPOID = poID;
    console.log("ENTRA POID: " + poID);
    $.ajax({
        async: false,
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: url,
        success: function (data) {
            var ObjData = JSON.parse(data);
            //$("#lblPO").text(ObjData[0].);
            console.log("style1: " + JSON.stringify(dataDetails) + " /style2: " + JSON.stringify(ObjData) + "////**: " + dataDetails.length);
            contenido += "<tr style='display: none;'><td colspan=12>";
            contenido += "<table  id='TableModal' class='table table-dark table-bordered table-bordered  table-striped' >";
            contenido += "<thead class='thead-agrocom-light'>";
            //contenido += "<tr>";
            //var myTableModal = $('#TableModal').DataTable();

            //CARGAMOS HEADERS DE TABLA
            //for (var z = 2; z < arryColumnsDetail.length-1; z++) {
            //    contenido += "<th>";
            //    contenido += arryColumnsDetail[z];
            //    contenido += "</th>";
            //}
            //contenido += "</tr>";
            contenido += "</thead>";
            // CARGAMOS CUERPO
            contenido += "<tbody id='bodyTable'>";
            var llavesDet = Object.keys(dataDetails[0]);

            console.log("data object2: " + JSON.stringify(dataDetails) + "/// " + llavesDet);
            var index = [];

            //$.each(dataDetails, function (index, item) {
            //    if (item.orderproductionid != 0) {
            //        contenido += "<tr style='background-color: #84F764'>";
            //        contenido += "<td align='center'>";
            //        contenido += "<img src='/images/user.png' style='position: relative; width: 80 %;' width='32' height='32' />";
            //        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
            //        contenido += "</td>";

            //        var valorLLavesDet = llavesDet[j];
            //        contenido += "<td >";
            //        contenido += dataDetails[a][valorLLavesDet];
            //        console.log("data objectDOCS: " + (dataDetails[a][llavesDet[0]]) + "Tamaño data: " + dataDetails.length);
            //        contenido += "</td>";
            //        //}
            //        console.log(a + "data object3: " + dataDetails[a][llavesDet[0]] + " =" + a + "= " + ObjData[a][itemFiles[2]]);
            //        contenido += "<td align='center' style='vertical-align:middle'>";
            //        contenido += "<button  class='btnDelete' type='button' value='" + ObjData[a][itemFiles[2]] + "' style='vertical-align:middle;float: left; '>DELETE FILE</button>";
            //        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
            //        //contenido += "</td>";
            //        //contenido += "<td >";
            //        contenido += "<label id='label" + ObjData[a][itemFiles[2]] + "' style='vertical-align:middle;float: left; '>" + ObjData[a][itemFiles[3]] + "</label>";
            //        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
            //        //contenido += "<button  class='btnVer' type='button' value='" + data[a][itemFiles[2]] + "' style='vertical-align:middle;float: left; '>VER FILE</button>";
            //        contenido += "</td>";
            //        //var name = ObjData[a][itemFiles[3]];
            //        //var nf = name.trim();
            //        //var nameFile = getLink(nf).trim();
            //        //var theString = nameFile.replace(/%20/g, '')
            //        //console.log("URLINK: " + nameFile);
            //        //contenido += "<a style='color: blue' href='http://www.example.com/myfile.pdf#glossary' target='_blank'>";
            //        //contenido += "<td><a style='color: blue' href='" + theString + "' target='_blank'>View</a>";
            //        contenido += "<td><a style='color: blue'  target='_blank'>View</a>";
            //        //contenido += "<u>View</u></a > ";
            //        contenido += "</td>";
            //        contenido += "<td align='center' style='vertical-align:middle'>";

            //        //contenido += "<label><a style='color: blue' href='" + theString + "' target='_blank' type='application / octet - stream' download='yourpdf.pdf'><u>Download</u></a></label>";
            //        contenido += "<label><a style='color: blue' target='_blank' type='application/octet-stream' download='yourpdf.pdf'><u>Download</u></a></label>";
            //        contenido += "</td>";
            //    }
            //    else {
            //        //datosDoc[a] = "0";
            //        contenido += "<tr>";
            //        contenido += "<td align='center'>";
            //        contenido += "<img src='/images/user.png' style='position: relative; width: 80 %;' width='32' height='32' />";
            //        //for (var i = 2; i < llavesDet.length - 1; i++) {
            //        var valorLLavesDet = llavesDet[i];
            //        contenido += "<td >";
            //        contenido += dataDetails[a][valorLLavesDet];
            //        console.log("//" + dataDetails[a][valorLLavesDet] + " /data objectDOCSN: " + dataDetails[a][llavesDet[0]]);
            //        contenido += "</td>";

            //        contenido += "<td align='center' style='vertical-align:middle'>";
            //        contenido += "<input type='file' id='file" + dataDetails[a][llavesDet[0]] + "' name='file' style='vertical-align:middle;float: left; '/>";

            //        //contenido += "<button  class='btnAttach' type='button' value='" + "' style='vertical-align:middle'>UPLOAD FILE</button>";
            //        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
            //        contenido += "</td>";
            //        contenido += "<td >";
            //        contenido += "<button class='btnAttach' type='button'  id = 'but_upload' value='" + dataDetails[a][llavesDet[0]] + "' style='vertical-align:middle'>Save</button>";
            //        //contenido += "<input class='btnAttach' text='hola' type='button' value='" + data[i][itemFiles[2]] +"'/>";
            //        contenido += "</td>";
            //        //contenido += "</tr>";
            //        //}
            //    }

            //});

            for (var a = 0; a < dataDetails.length; a++) {
                console.log("data objec4: " + JSON.stringify(ObjData[a].orderproductionid));

                if (JSON.stringify(ObjData[a].orderproductionid) != '0') {
                    console.log("WQQQ: " + JSON.stringify(ObjData))
                    var itemFiles = Object.keys(ObjData[a]);
                    //datosDoc[a] = data[a][itemFiles[2]];
                    contenido += "<tr style='background-color: #28a745'>";
                    contenido += "<td align='center'>";
                    contenido += "<img src='/images/user.png' class='img-fluid img-thumbnail' style='position: relative; width: 80 %;' width='32' height='32' />";
                    //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
                    contenido += "</td>";
                    for (var j = 2; j < llavesDet.length; j++) {
                        //if (dataDetails[i][llavesDet[10]] == '1') {
                        var valorLLavesDet = llavesDet[j];
                        contenido += "<td >";
                        contenido += dataDetails[a][valorLLavesDet];
                        console.log("data objectDOCS: " + (dataDetails[a][llavesDet[0]]) + "Tamaño data: " + dataDetails.length);
                        contenido += "</td>";
                        //}
                        console.log(a + "data object3: " + dataDetails[a][llavesDet[0]] + " =" + a + "= " + ObjData[a][itemFiles[2]]);
                        contenido += "<td >";
                        contenido += "<button  class='btn btnDelete  btn-sm' style='background-color:#fff' type='button' value='" + ObjData[a][itemFiles[2]] + "' > <span class='icon text-danger'><i class='fas fa-trash'></i></span></button>";
                        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
                        contenido += " ";
                        //contenido += "<td >";
                        contenido += "<label id='label" + ObjData[a][itemFiles[2]] + "' >" + ObjData[a][itemFiles[3]] + "</label>";
                        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
                        //contenido += "<button  class='btnVer' type='button' value='" + data[a][itemFiles[2]] + "' style='vertical-align:middle;float: left; '>VER FILE</button>";
                        contenido += "</td>";
                        var name = ObjData[a][itemFiles[3]];
                        var nf = name.trim();
                        var nameFile = getLink(nf).trim();
                        var theString = nameFile.replace(/%20/g, '')
                        console.log("URLINK: " + nameFile);
                        //contenido += "<a style='color: blue' href='http://www.example.com/myfile.pdf#glossary' target='_blank'>";
                        //contenido += "<td><a style='color: blue' href='" + theString + "' target='_blank'>View</a>";
                        ////contenido += "<td><a style='color: blue'  target='_blank'>View</a>";
                        ////contenido += "<u>View</u></a > ";
                        //contenido += "</td>";
                        contenido += "<td >";

                        contenido += "<a  href='" + theString + "' target='_blank' type='application / octet - stream' download='yourpdf.pdf'><button  class='btn btnDelete  btn-sm' style='background-color:#fff'><span class='icon text-primary'><i class='fas fa-download'></i></span></button></a>";
                        //contenido += "<label><a style='color: blue' target='_blank' type='application/octet-stream' download='yourpdf.pdf'><u>Download</u></a></label>";
                        contenido += "</td>";
                    }
                    //console.log("data objectDOCS: " + JSON.stringify(data));
                    //console.log("DATA LENGT: " + data.length + " dato1: " + JSON.stringify(data[0]))
                    //contenido += "<td align='center' style='vertical-align:middle'>";
                    //contenido += "<button  class='btnAttach' type='button' value='" + "' style='vertical-align:middle'>UPLOAD FILE</button>";
                    ////contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
                    //contenido += "</td>";
                    contenido += "</tr>";
                }
                else {
                    //datosDoc[a] = "0";
                    contenido += "<tr>";
                    contenido += "<td align='center'>";
                    contenido += "<img src='/images/user.png' class='img-fluid img-thumbnail' style='position: relative; width: 80 %;' width='32' height='32' />";
                    contenido += "</td>";
                    for (var i = 2; i < llavesDet.length; i++) {
                        var valorLLavesDet = llavesDet[i];
                        contenido += "<td >";
                        contenido += dataDetails[a][valorLLavesDet];
                        console.log("//" + dataDetails[a][valorLLavesDet] + " /data objectDOCSN: " + dataDetails[a][llavesDet[0]]);
                        contenido += "</td>";

                        contenido += "<td>";
                        //contenido += "Choose file <label class='btn btn-dark btn-sm' style='background-color:#fff'><span class='icon text-primary'><i class='fa fa-upload'></i></span> <input type='file' style='display: none;' class='' id='file" + dataDetails[a][llavesDet[0]] + "' value='" + dataDetails[a][llavesDet[0]] + "' name='file' /></label>";
                        contenido += "<input type='file'  id='file" + dataDetails[a][llavesDet[0]] + "' value='" + dataDetails[a][llavesDet[0]] + "' name='file' />";

                        //contenido += "<button  class='btnAttach' type='button' value='" + "' style='vertical-align:middle'>UPLOAD FILE</button>";
                        //contenido += "<input class='btnAttach' id='" + dataDetails[i][llavesDet[0]] + "' data-toggle='modal' data-target='#openModal2' type='button' value='" + dataDetails[i][llavesDet[0]] +"'/>";
                        contenido += "</td>";
                        contenido += "<td >";
                        contenido += "<button class='btn btnAttach btn-dark btn-sm' style='background-color:#fff' type='button'  id='but_upload' value='" + dataDetails[a][llavesDet[0]] + "' style='vertical-align:middle'><span class='icon text-success'><i class='fas fa-floppy-o'></i></span></button>";
                        //contenido += "<input class='btnAttach' text='hola' type='button' value='" + data[i][itemFiles[2]] +"'/>";
                        contenido += "</td>";

                    }
                    contenido += "</tr>";
                }
            }
            //contenido += "</tr>";
            contenido += "</tbody>";
            contenido += "</table>";
            //LengthPODocs();
        }
    });


    document.getElementById("uploadModalTbl2").innerHTML = contenido;

}

function LengthPODocs() {
    var i = 0;
    $.get("LoadPODocsByID/?typeID=" + gPOID, function (data) {
        var ObjData = JSON.parse(data);
        console.log("DATALENGHT: " + JSON.stringify(ObjData));
        var poDocsLength = ObjData.length;
        $.each(ObjData, function (index, item) {
            if (item.orderproductionid != 0) {
                i++;
            }
        })
        ConfirmPODocs(i);
    });
    i = 0;
}
function ConfirmPODocs(poDocsLength) {
    var url = "ConfirmPODocs";
    var st;
    if (poDocsLength == 0) {
        st = 1;
    }
    if (poDocsLength == 5) {
        st = 2;
    }
    if (poDocsLength > 0 && poDocsLength < 5) {
        st = 3;
    }
    console.log("POID: " + gPOID + "// statusShipment: " + st + " //length: " + poDocsLength);
    var frmDet = new FormData();
    frmDet.append("id", parseInt(gPOID));
    frmDet.append("poStatusId", parseInt(st));
    console.log("adasd" + JSON.stringify(Object.fromEntries(frmDet)));
    $.ajax({
        url: url,
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        data: JSON.stringify(Object.fromEntries(frmDet)),
        contentType: "application/json",
        Accept: "application/json",
        dataType: 'json',
        async: false,
        error: function (err) {
            console.log("ERROR 123123123: " + JSON.stringify(err));
        },
        success: function (response) {
            if (response == 0) {
                //alert('Documents Saved, Updated PO List ');
                console.log("Documents Saved, Updated PO List " + JSON.stringify(response));
                ListaSst();
                //closeModal(document.getElementById("openModal2"));
                //location.reload(true);
                //$('#openModal2').hide();
            }
            else {
                alert('files not saved');
            }
        },
    });
}

function loadModalSst(POID) {
    console.log("CLICKED UPLOAD1111111111 " + POID);
    ListTableModal(POID);
    POModalShipmentST(POID);
}
$(document).ready(function () {


    $('#cboSemanaIni').change(function (e) {
        var before_change = $(this).data('pre');//get the pre data
        var current = $(this).val();
        console.log("PREV: " + before_change + "CURRENT: " + current);
        //Do your work here

        var week2 = $('#cboSemanaFin').val();
        //if (week2 < current) {
        //    alert("Check week ranges1!");
        //    $("#cboSemanaIni").val(before_change).change();
        //} else {
        //    ListaSst();
        //}
        //////////////////////
        $(this).data('pre', $(this).val());//update the pre data
    });
    $('#cboSemanaFin').change(function (e) {
        var before_change = $(this).data('pre');//get the pre data
        var current = $(this).val();
        console.log("PREV: " + before_change + "CURRENT: " + current);
        //Do your work here
        var week1 = $('#cboSemanaIni').val();
        if (week1 > current) {
            alert("Check week ranges!");
            $("#cboSemanaFin").val(before_change).change();
        } else {
            //ListaSst();
        }
        //////////////
        $(this).data('pre', $(this).val());//update the pre data

    });
    $(document).on("click", ".btnDelete", function () {
        var frmDet = new FormData();
        var type = $(this).val();
        var labDel = "label" + type;
        var labelDel = "#" + labDel;
        var Name = $(labelDel).text();
        var myFileName = Name.trim();
        frmDet.append("id", gPOID);

        frmDet.append("typeDocId", type);
        ///////////////////////
        DeleteFilesOnAzure(myFileName, frmDet, gPOID)

        ////////////////////
        console.log("LABEL NAME: " + myFileName + " TYPE: " + type + "// Deleted?= ");
    });
    $(document).on("click", ".btnUpload", function () {
        console.log("CLICKED UPLOAD" + $(this).val());
        //$('#openModal2').modal('show');
        var current = $(this).val();
        loadModalSst(current);
        console.log("VAL DEL BOTON: " + current)
    });
    $(document).on("click", ".btnView", function () {
        console.log("CLICKED VIEW");
        //$('#openModal2').modal('show');
        var current = $(this).val();
        loadModalSst(current);
        //addCancelModal();
        console.log("VAL DEL BOTON: " + current)
    });
    $(document).on("click", ".btnAttach", function () {

        var fn = "file" + $(this).val();
        var type = $(this).val();
        /////////////////////
        var files = document.getElementById(fn).files;
        var file = files[0];

        var filename2 = file.name;
        var ini = "";
        /////////////////////////+
        if (type == '1') {
            ini = "BL";
        } if (type == '2') {
            ini = "PAL";
        } if (type == '3') {
            ini = "ORI";
        } if (type == '4') {
            ini = "PHY";
        } if (type == '5') {
            ini = "INV";
        }
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var curHour = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
        var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
        today = mm + '/' + dd + '/' + yyyy;
        var now = yyyy + mm + dd + "-" + curHour + curMinute;

        var filename = ini + "-PO" + gPOID + "-" + $(this).val() + "-" + now;
        //// Use a regular expression to pull the file name only
        //var Name = fileInput.value.split(/(\\|\/)/g).pop();
        //uploadAzureLibrary(fn);
        console.log("ALGORITMO NOMBRE: " + filename);
        ////////////////////

        console.log("FN: " + fn + " filename: " + filename);

        //// Use a regular expression to pull the file name only
        //var Name = fileInput.value.split(/(\\|\/)/g).pop();
        //uploadAzureLibrary(fn);

        if (filename != "") {
            console.log("ATACH SAVE CLICKED!!");
            UploadFilesOnAzure(fn, filename, type, gPOID)
        }
        //////////////////////////

    });
    $(document).on("click", ".btnVer", function () {
        var type = $(this).val();
        var labDel = "label" + type;
        var labelDel = "#" + labDel;
        var Name = $(labelDel).text();
        var myFileName = Name.trim();
        getLink(myFileName);
    });
});
function DeleteNameFiles(isDeleted, frmDet, poid) {
    var id, typeDocId;
    if (isDeleted) {
        $.ajax({
            url: "DeletePODocument",
            type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            data: JSON.stringify(Object.fromEntries(frmDet)),
            contentType: "application/json",
            Accept: "application/json",
            dataType: 'json',
            error: function (err) {
                console.log("ERROR DELETING DOCS: " + JSON.stringify(err));
            },
            async: false,
            success: function (response) {
                if (response == 0) {
                    alert('file Deleted');
                    LengthPODocs();
                    ListTableModal(poid);
                }
                else {
                    alert('file is not deleted');
                }
            },
        });
    } else {
        alert('file is not deleted');
    }

}
function UploadNameFile(isUploaded, filename, type, poID) {
    //var isUploaded = UploadFilesOnAzure(fn);
    console.log("isUploaded: " + isUploaded);
    if (isUploaded) {
        console.log("BOTON SAVE: poID:" + poID + "type: " + type + " filename : " + filename);
        var frmDet = new FormData();

        frmDet.append("id", poID);
        frmDet.append("typeDocId", type);
        frmDet.append("nameFile", filename);
        frmDet.append("statusId", "1");

        console.log("BOTON SAVE: name:" + filename + " POID: " + poID + " TYPE: " + type + " ");

        $.ajax({
            url: "InserPODocument",
            type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            data: JSON.stringify(Object.fromEntries(frmDet)),
            contentType: "application/json",
            Accept: "application/json",
            dataType: 'json',
            async: false,
            error: function (err) {
                console.log("ERROR SAVING" + err);
            },
            success: function (response) {
                console.log("RESPONSE AJAX UPDATE: " + response);
                if (response == 0) {
                    alert('file uploaded');
                    ListTableModal(gPOID);
                    LengthPODocs();
                }
                else {
                    alert('file not uploaded');
                }
            },
        });
    } else {
        alert("A file must be Selected");
    }
}
function DeleteFilesOnAzure(myFileName, frmDet, poid) {
    var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);

    fileService.deleteFileIfExists('agrocomshared', '', myFileName, function (error, result, response) {
        if (error) {
            // Delete file error
            console.log("FILE DO NOT DELETED: " + JSON.stringify(response));
            console.log("FILE DO NOT DELETED: " + response.isSuccessful);
            console.log("result: " + result);
            console.log("error: " + error);
            //console.log("FILE UPLOADED: " + Object.keys(response) + "//" + response.isSuccessful);
            //console.log("result: " + Object.keys(result));
        } else {
            // Delete file successfully
            //console.log("FILE DELETED: " + Object.keys(response));
            console.log("error: " + error);
            console.log("result: " + result);
            console.log("FILE DELETED: " + JSON.stringify(response));
            console.log("FILE DELETED: " + response.isSuccessful);
        }
        DeleteNameFiles(response.isSuccessful, frmDet, poid)
    });
}
function getLink(myfile) {
    var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
    var SAS_TOKEN1 = "sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2021-01-01T04:59:59Z&st=2020-04-06T00:57:30Z&spr=https&sig=0gjOhB1iFXgblSVElnm1Z9w3hAwUvDpFNTqu7J2RMWw%3D";
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    var downloadLink = fileService.getUrl('agrocomshared', '', myfile, SAS_TOKEN1);
    console.log("LIIIIIIIIIINK: " + downloadLink);
    return downloadLink;
}
function UploadFilesOnAzure(control, filename, type, poID) {
    var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    // If one file has been selected in the HTML file input element
    var files = document.getElementById(control).files;
    var file = files[0];
    //var speedSummary =
    fileService.createFileFromBrowserFile('agrocomshared', '', filename, file, {}, function (error, result, response) {
        if (error) {
            // upload file error
            console.log("FILE DO NOT UPLOADED: " + response);
            console.log("result: " + result);
        } else {
            // upload file successfully
            console.log("FILE UPLOADED: " + Object.keys(response) + "//" + response.isSuccessful);
            console.log("result: " + Object.keys(result) + " // " + result.etag + " // " + result.share + " // " + result.directory + " // " + result.name + " // " + result.etag + " // " + result.lastModified + " // " + result.requestId);
        }
        UploadNameFile(response.isSuccessful, filename, type, poID);
    });
    console.log("PROCESS: ");
    //speedSummary.on('progress', function () {
    //    var process = speedSummary.getCompletePercent();
    //    //displayProcess(process);
    //    console.log("PROCESS COMPLETE: " + process);
    //});
    //refreshProgress();
}
function addCancelModal() {
    document.getElementById('btnCloseModal').style.display = 'normal';
}
$(document).ajaxStart(function () {
    //$('#loading').modal('show');
    console.log("INICIANDO AJAX...");
}).ajaxComplete(function () {
    //$('#loading').hide();
    //closeModal(document.getElementById("loading"));
    console.log("TERMINANDO AJAX...");
});

function closeModal(control) {
    $(control).removeClass("in");
    $(".modal-backdrop").remove();
    $(control).hide();
}