﻿var campaignID = (localStorage.campaignID);
var dataCustomer = []
var dataDestination = []
var dataNroPackingList = []
var dataManager = []
var dataOperator = []
var dataPlant = []
var dataWeek = []
var dataVia = []
var customerID = 0
var destinationID = 0
var managerID = 0
var logisticOperatorID = 0
var processPlantID = 0
var projectedWeekID = 0

$(function () {
	//$(document).ajaxStart(function () {
	//    $("#wait").css("display", "block");
	//    $("#loader").css("display", "block")

	//    $(".transact").attr("disabled", true);
	//});
	//$(document).ajaxComplete(function () {
	//    $("#wait").css("display", "none");
	//    $("#loader").css("display", "none")

	//    $(".transact").attr("disabled", false);
	//});

	loadData();
	//loadDefaultValues(); 

	$(document).on("click", ".btnSave", function () {
		var tr = $(this).closest('tr');
		Save(tr);
	});

});

$(document).on("click", "#btnSearch", function () {
	sweet_alert_progressbar();
	var customerID = $('#selectCustomer').val();
	var customerID = $('#selectDestination').val();
	var nroPackingList = $('#selectnroPackingList').val();
	var managerID = $('#selectManager').val();
	var logisticOperatorID = $('#selectOperator').val();
	var processPlantID = $('#selectPlant').val();
	var projectedWeekID = $('#selectWeek').val();
	var viaID = $('#selectVia').val();

	list(localStorage.campaignID, customerID, destinationID, nroPackingList, managerID, logisticOperatorID, processPlantID, projectedWeekID, viaID);
})

$(document).on("keyup", ".etaReal", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".invoice", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", "#bl", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".courierDate", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".curierNumber", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".freight", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".container", function (e) {
	$(this).closest('tr').find('.tdBtn').css({ "background-color": "LightGreen" }, { "text-align": "center" })	
	$(this).closest('tr').find(".btnSave").show()
});

function loadComboFilter(data, control, firtElement, textFirstElement) {
	var content = "";
	if (firtElement == true) {
		content += "<option value='All'>" + textFirstElement + "</option>";
	}
	for (var i = 0; i < data.length; i++) {
		content += "<option value='" + data[i].id + "'>";
		content += data[i].name;
		content += "</option>";
	}
	$('#' + control).empty().append(content);
}

function loadDefaultValues() {
	//filtros
	loadComboFilter(dataCustomer, 'selectCustomer', true, '[ALL]');
	$('#selectCustomer').selectpicker('refresh');

	loadComboFilter(dataDestination, 'selectDestination', true, '[ALL]');
	$('#selectDestination').selectpicker('refresh');

	loadComboFilter(dataNroPackingList, 'selectnroPackingList', true, '[ALL]');
	$('#selectnroPackingList').selectpicker('refresh');

	loadComboFilter(dataManager, 'selectManager', true, '[ALL]');
	$('#selectManager').selectpicker('refresh');

	loadComboFilter(dataOperator, 'selectOperator', true, '[ALL]');
	$('#selectOperator').selectpicker('refresh');

	loadComboFilter(dataPlant, 'selectPlant', true, '[ALL]');
	$('#selectPlant').selectpicker('refresh');

	loadComboFilter(dataWeek, 'selectWeek', true, '[ALL]');
	$('#selectWeek').selectpicker('refresh');

	loadComboFilter(dataVia, 'selectVia', true, '[ALL]');
	$('#selectVia').selectpicker('refresh');

	var customerID = $('#selectCustomer').val();
	var customerID = $('#selectDestination').val();
	var nroPackingList = $('#selectnroPackingList').val();
	var managerID = $('#selectManager').val();
	var logisticOperatorID = $('#selectOperator').val();
	var processPlantID = $('#selectPlant').val();
	var projectedWeekID = $('#selectWeek').val();
	var viaID = $('#selectVia').val();

	list(localStorage.campaignID, customerID, destinationID, nroPackingList, managerID, logisticOperatorID, processPlantID, projectedWeekID, viaID);

}

function loadOtherData() {

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByCustomer?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataCustomer = dataJson.map(
					obj => {
						return {
							"id": obj.customerID,
							"name": obj.customer
						}
					}
				);
			}

		}
	});

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByWeek?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.week
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByManager?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataManager = dataJson.map(
					obj => {
						return {
							"id": obj.managerID,
							"name": obj.manager
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByOperator?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataOperator = dataJson.map(
					obj => {
						return {
							"id": obj.logisticOperatorID,
							"name": obj.logisticOperator
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByPlant?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataPlant = dataJson.map(
					obj => {
						return {
							"id": obj.processPlantID,
							"name": obj.processPlant
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByDestination?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataDestination = dataJson.map(
					obj => {
						return {
							"id": obj.destinationID,
							"name": obj.destination
						}
					}
				);
			}
		}
	});

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingByVia?campaignID=" + localStorage.campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataVia = dataJson.map(
					obj => {
						return {
							"id": obj.viaID,
							"name": obj.via
						}
					}
				);
			}
		}
	});
}

function loadData() {

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingPackingList?campaignID=" + localStorage.campaignID,
		//async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataNroPackingList = dataJson.map(
					obj => {
						return {
							"id": obj.nroPackingList,
							"name": obj.nroPackingList
						}
					}
				);
			}
			loadOtherData();
			loadDefaultValues();
		}
	});
	sweet_alert_progressbar_cerrar();
}

function list() {
	var campaignID = localStorage.campaignID
	var customerID = $('#selectCustomer').val();
	var destinationID = $('#selectDestination').val();
	var nroPackingList = $('#selectnroPackingList').val();
	var managerID = $('#selectManager').val();
	var logisticOperatorID = $('#selectOperator').val();
	var processPlantID = $('#selectPlant').val();
	var projectedWeekID = $('#selectWeek').val();
	var viaID = $('#selectVia').val();

	if (customerID == 'All') {
		customerID = 0;
	}
	if (destinationID == 'All') {
		destinationID = 0;
	}
	if (nroPackingList == 'All') {
		nroPackingList = '';
	}
	if (managerID == 'All') {
		managerID = 0;
	}
	if (logisticOperatorID == 'All') {
		logisticOperatorID = 0;
	}
	if (processPlantID == 'All') {
		processPlantID = 0;
	}
	if (projectedWeekID == 'All') {
		projectedWeekID = 0;
	}
	if (viaID == 'All') {
		viaID = 0;
	}

	$.ajax({
		type: "GET",
		url: "/ShippingProgram/GetShippingTrayList?campaignID=" + campaignID + "&customerID=" + customerID + "&destinationID=" + destinationID + "&nroPackingList=" + nroPackingList + "&managerID=" + managerID + "&logisticOperatorID=" + logisticOperatorID + "&processPlantID=" + processPlantID + "&projectedWeekID=" + projectedWeekID + "&viaID=" + viaID,
		//async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataList = JSON.parse(data);
			if (data.length > 0) {
				loadListPrincipal(dataList);
			}

		}
	});
	sweet_alert_progressbar_cerrar();
}

function loadListPrincipal(data) {
	$('#tblShipping').DataTable().clear().destroy()
	$("#tblShipping>tbody").empty()

	$.each(data, function (i, e) {

		var tr = "<tr class='table-tr-agrocom'>";

		tr += "<td class='orderProductionID' style='display:none'>" + e.orderPoductionID + "</td>"
		tr += "<td class='nroPackingListID' style='display:none'>" + e.nroPackingListID + "</td>"
		tr += "<td class='campaignID' style='display:none'>" + e.campaignID + "</td>"
		tr += "<td class='campaign' hidden><textarea style='font-size: 12px' class='form-control form-control-sm' id='customer' rows='1'>" + e.campaign + "</textarea></td>"
		tr += "<td class='projectedWeekID' style='display:none'>" + e.projectedWeekID + "</td>"
		tr += "<td class='customerID' style='display:none'>" + e.customerID + "</td>"
		tr += "<td class='sticky dispatchDate' style='max-width: 90px; min-width:90px;background-color:white'>" + e.dispatchDate + "</td>"
		tr += "<td class='sticky1 nroPackingList' style='max-width: 150px; min-width:150px;background-color:white'>" + e.nroPackingList + "</td>"
		tr += "<td class='sticky2 po' style='max-width: 120px; min-width:120px;background-color:white'>" + e.po + "</td>"		
		tr += "<td class='week' style='max-width: 55px; min-width:55px'>" + e.week + "</td>"
		tr += "<td class='customer' style='max-width: 200px; min-width:200px'><textarea style='font-size: 12px' class='form-control form-control-sm' id='customer' rows='1' readonly>" + e.customer + "</textarea></td>"
		tr += "<td class='destinationID' style='display:none'>" + e.destinationID + "</td>"
		tr += "<td class='destination' style='max-width: 200px; min-width:200px'>" + e.destination + "</td>"
		tr += "<td class='logisticOperatorID' style='display:none'>" + e.logisticOperatorID + "</td>"
		tr += "<td class='logisticOperator' style='max-width: 180px; min-width:180px'><textarea style='font-size: 12px' class='form-control form-control-sm' id='logisticOperator' rows='1' readonly>" + e.logisticOperator + "</textarea></td>"
		tr += "<td class='departureWeek' style='max-width: 60px; min-width:60px'>" + e.quantity + "</td>"
		tr += "<td class='status' style='max-width: 55px; min-width:55px'>" + e.vgm + "</td>"
		tr += "<td class='processPlantID' style='display:none'>" + e.processPlantID + "</td>"
		tr += "<td class='processPlant' style='max-width: 135px; min-width:135px'><textarea style='font-size: 12px' class='form-control form-control-sm' id='processPlant' rows='1' readonly>" + e.processPlant + "</textarea></td>"
		tr += "<td class='managerID' style='display:none'>" + e.managerID + "</td>"
		tr += "<td class='manager' style='max-width: 100px; min-width:100px'>" + e.manager + "</td>"
		tr += "<td class='shippingCompany' style='max-width: 130px; min-width:130px'>" + e.shippingCompany + "</td>"
		tr += "<td class='booking' style='max-width: 100px; min-width:100px'>" + e.booking + "</td>"
		tr += "<td class='etd' style='max-width: 80px; min-width:80px'>" + e.etd + "</td>"
		tr += "<td class='etaReal' contenteditable='true' style='max-width: 80px; min-width:80px;background-color:#A3C1D5'>" + e.etaReal.trim() + "</td>"
		tr += "<td class='invoice' contenteditable='true' style='max-width: 100px; min-width:100px;background-color:#A3C1D5'>" + e.invoice + "</td>"
		tr += "<td class='incoterm' style='max-width: 60px; min-width:60px'>" + e.incoterm + "</td>"
		tr += "<td class='freightType' style='max-width: 80px; min-width:80px'>" + e.freightType + "</td>"
		tr += "<td class='bl' contenteditable='true' style='max-width: 150px; min-width:150px;background-color:#A3C1D5'><textarea style='font-size: 12px;background-color:#A3C1D5' class='form-control form-control-sm' id='bl' rows='1'>" + e.bl + "</textarea></td>"
		tr += "<td class='container' contenteditable='true' style='max-width: 100px; min-width:100px;background-color:#A3C1D5'>" + e.container + "</td>"
		tr += "<td class='freight' contenteditable='true' style='max-width: 50px; min-width:50px;background-color:#A3C1D5'>" + e.freight + "</td>"		
		tr += "<td class='courierDate' contenteditable='true' style='max-width: 80px; min-width:80px;background-color:#A3C1D5'>" + e.courierDate.trim() + "</td>"
		tr += "<td class='curierNumber' contenteditable='true' style='max-width: 100px; min-width:100px;background-color:#A3C1D5'>" + e.curierNumber + "</td>"
		tr += "<td class='shippingLoadingID' style='display:none'>" + e.shippingLoadingID + "</div></td>"
		tr += "<td class='tdBtn' style='max-width: 30px; min-width: 30px;text-align: center!important'><button type='button' class='btn btnSave btn-dark btn-sm' style='font-size: 12px; display: none'><i class='fas fa-save'></i></button></td>"

		tr += "</tr>"
		$("#tblShipping>tbody").append(tr);
	})
	
	table = $('table#tblShipping').DataTable({
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
				exportOptions: {
					columns: [3,6,7,8,9,10,12,14,15,16,18,20,21,22,23,24,25,26,27,28,29,30,31,32]
				},
				sheetName: 'Data',
				title: 'Loading Instruction Report'
			},

			'pageLength'
		],
		orderCellsTop: true,
		fixedHeader: true,
		ordering: false,
		searching: true
	});
	//var columnsToShow = [3, 6, 7, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];
	//CuztomiseHtmlDatatableExport('tblShipping', false, false, columnsToShow, 'Data', 'Loading Instruction Report')

}

table = $('#tblShipping').DataTable({

	dom: 'Bfrtip',

	buttons: [
		{ extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
		'pageLength'
	],
	orderCellsTop: true,
	fixedHeader: true,
	ordering: false,
	searching: true
});

$(document).ready(function () {
	// Setup - add a text input to each footer cell
	$('#tblShipping thead tr').clone(true).appendTo('#tblShipping thead');
	$('#tblShipping thead tr:eq(1) td').each(function (i) {
		if (i > 0) {
			var title = $(this).text();
			$(this).html('<input style="font-size:12px" type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');
		} else {
			$(this).html(' ');
		}
		$('input', this).on('keyup change', function () {
			if (table.column(i).search() !== this.value) {
				table
					.column(i)
					.search(this.value)
					.draw();
			}
		});
	});

});

function Save(tr) {

	var etaRealT = $(tr).find(".etaReal").text();
	if (etaRealT != "" && etaRealT != undefined) {
		var etaRealDateTime = $(tr).find(".etaReal").text().split(" ");
		var etaRealDate = etaRealDateTime[0].split("/");
		etaRealD = etaRealDate[2] + '-' + etaRealDate[1] + '-' + etaRealDate[0]; //+ ' ' + etaRealDateTime[1];
		etaReal = etaRealD;

	} else {
		etaReal = "";
	}

	if ($(tr).find(".invoice").text() != "") {
		invoice = $(tr).find(".invoice").text()
	} else {
		invoice = ""
	}

	if ($(tr).find("#bl").val() != "") {
		bl = $(tr).find("#bl").val()
	} else {
		bl = ""
	}

	var courierDateN = $(tr).find(".courierDate").text();
	if (courierDateN != "" && courierDateN != undefined) {
		var courierDateTime = $(tr).find(".courierDate").text().split(" ");
		var courierDateC = courierDateTime[0].split("/");
		courierDat = courierDateC[2] + '-' + courierDateC[1] + '-' + courierDateC[0];
		courierDate = courierDat;
	} else {
		courierDate = "";
	}

	if ($(tr).find(".curierNumber").text() != "") {
		curierNumber = $(tr).find(".curierNumber").text()
	} else {
		curierNumber = ""
	}

	if ($(tr).find(".container").text() != "") {
		container = $(tr).find(".container").text()
	} else {
		container = ""
	}

	if ($(tr).find(".freight").text() != "") {
		freight = $(tr).find(".freight").text()
	} else {
		freight = ""
	}

	shippingLoadingID = $(tr).find(".shippingLoadingID").text()

	$(tr).find(".btnSave").hide()
	$(tr).find(".tdBtn").removeAttr("style")

	var o = {

		"shippingLoadingID": $(tr).find(".shippingLoadingID").text(),
		"etaReal": etaReal,
		"invoice": invoice,
		"bl": bl,
		"courierDate": courierDate,
		"curierNumber": curierNumber,
		"container": container,
		"freight": freight
	}

	$.ajax({
		type: 'POST',
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: "/ShippingProgram/GetDateUpdate?opt=" + "cur" + "&id=" + shippingLoadingID + "&data1=" + etaReal + "&data2=" + invoice + "&data3=" + bl + "&data4=" + courierDate + "&data5=" + curierNumber + "&data6=" + container + "&data7=" + freight,
		//async: true,
		contentType: "application/json",
		dataType: 'json',
		data: JSON.stringify(o),
		success: function (data) {
			//if (data.length == 0) {
			sweet_alert_success('Good Joob!', 'successfully saved');
			//} 
		},
		error: function (datoEr) {
			//console.log("Hubo un error: " + JSON.stringify(datoEr));
			sweet_alert_error("Error", "There has been a problem!");
		}
	})
}