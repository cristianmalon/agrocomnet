﻿var customer;
(function () {
    var Handler = function () {
        //>>>Variables Globales

		//<<<Variables Globales

        //>>>Definición de controles
        var ctrl = {
            _lblTitleClientSpecification: $('#lblTitleClientSpecification'),
            _tblSaleOrder: $('#tableList'),
        };
        //<<<Definición de controles

        var _that = {
            Init: function () {
                this.Events();                
            },
            Methods: {
                Init: function () {

                },
                GetClientSpecification: function () {
                    
                    _modalClientSpecification = $('#modalClientSpecification');
                    _modalClientSpecification.modal('show');
                    let sUrlEndPoint = getPath() + HOST_APIAGROCOM_SPECIFICATION + ENDPOINT_GET_CLIENT_SPECIFICATION;
                    
                    $.ajax({
                        type: "GET",
                        url: sUrlEndPoint + "?opt=" + 'pro' + "&campaignID=" + localStorage.campaignID,
                        //async: false,
                        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                        success: function (data) {
                            let dataViewSpecification = data;
                            let dataFilter = dataViewSpecification.filter((item) => item.customer == customer);
                            /*console.log(dataFilter);*/

                            ctrl._lblTitleClientSpecification.html('CLIENT SPECIFICATIONS');
                            let tr = '';
                            tr += '<center>';
                            tr += '<table class="table table-hover table-bordered table-striped no-footer dataTable" style="background-color:#ffdc37" id="tblExportClientSpecification">';
                            tr += '<thead class="thead-agrocom">';
                            tr += '<tr>';
                            tr += '<th style="text-align:center;">CUSTOMER</th><th style="text-align:center;">DESTINATION</th><th style="text-align:center;">PRODUCT</th><th style="text-align:center;">PRIORITY</th><th style="text-align:center;">MINIMUM(%)</th><th style="text-align:center;">MAXIMUM(%)</th>';
                            tr += '</tr>';
                            tr += '</thead>';
                            tr += '<tbody>';
                            $.each(dataFilter, function (i, e) {
                                tr += "<tr style='text-align:center;font-size:13px'>";
                                tr += '<td style="text-align:center;max-width: 220px; min-width:220px" class="customer">' + e.customer + '</td>';
                                //tr += '<td style="text-align:center;max-width: 120px; min-width:120px" class="crop" >' + e.crop + '</td>';
                                tr += '<td style="text-align:center;max-width: 120px; min-width:120px" class="destination">' + e.destination + '</td>';
                                tr += '<td style="text-align:center;max-width: 200px; min-width:200px" class="product">' + e.product + '</td>';
                                //tr += '<td style="text-align:center;max-width: 300px; min-width:300px" class="description"><textarea style="font-size:13px" class="form-control form-control-sm" id="description" rows="1">' + '' + '</textarea></td>';
                                tr += '<td style="text-align:center;max-width: 100px; min-width:100px" class="priority">' + e.priority + '</td>';
                                tr += '<td style="text-align:center;max-width: 100px; min-width:100px" class="minimum">' + e.minimum + '</td>';
                                tr += '<td style="text-align:center;max-width: 100px; min-width:100px" class="maximum">' + e.maximum + '</td>';
                                tr += '</tr>';
                            });
                            tr += '</tbody>';
                            tr += '</table>';
                            tr += '</center>';
                            $("div#resultSuccess").empty().append(tr);
                            
                        },
                        complete: function () {                            
                            
                        }

                    });                    
                }
            },
            Events: function () {
                Swal.fire({
                    imageUrl: '../images/AgricolaAndrea71x64.png',
                    title: 'AGROCOM',
                    html: 'Loading information...',
                    onBeforeOpen() {
                        Swal.showLoading();
                    },
                    onOpen() {
                        _that.Methods.GetClientSpecification();
                        Swal.hideLoading();
                        Swal.close();
                    },
                    onAfterClose() {

                    },
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    showConfirmButton: false,
                });
            }
        };

        return _that;
    }

    //$(document).ready(function () {
    //    Handler().Init();
    //});
    $(document).on("click", "#btnViewClientSpecification01", function () {
        let tr = $(this).parents('tr');
        customer = tr.find('td#tdCustomer')[0].innerText;
        Handler().Init();
    });
    $(document).on("click", "#btnViewClientSpecification02", function () {
        let div = $(this).parents('div');
        customer = div.find('input#txtCustomer').val();
        Handler().Init();
    });
})();