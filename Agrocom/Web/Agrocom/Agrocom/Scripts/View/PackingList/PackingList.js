﻿//$(document).ready(function () {
var sessionUserID = $('#lblSessionUserID').text()
var paletaTabla = [];



//    //$(document).on("change", ".txtPalletsConfirm", function () {        
//    //    sumPalletsConfirm()
//    //});
//})

var dataVariety = [];
var dataFarm = [];
var dataCodePack = [];
var dataLabel = [];
var arreglo = [];
var dataListTable = [];
var dataHeader = [];
var dataDetails = [];
var dataDetailsSum = [];
var _packingListID = 0;
var _orderProductionID = 0;
var dataSize = [];
//data size




//Crear Cabecera Packinlist

dataHeader = [
    {
        "packinglist": "CAL-ARA 0004/20019",
        "exported": "OZBLU",
        "grower": "OZB",
        "invoice": "001-0004-0000054",
        "guide": "GRR 0023-00",
        "senasa": "126503",
        "custom": "011LA094",
        "thermogregistersNo": "GDEQN",
        "thermogregistersLocation": "OZPO400",
        "sensors": "S1/S2/S3",
        "sensorsL": "5254",
        "customer": "OZBLU",
        "totalCases": "7800",
        "packingLoading": "23/0/2019",
        "vessel": "MAERSK ",
        "containerNo": "MNBU36",
        "shippingSealNo": "HSA4",
        "originPort": "CALLAO-PERU",
        "destinationport": "SHANGHAI-CHINA",
        "ETA": "25/10/2019",
        "ETD": "25/10/2019"
    },

]



function validar() {
  if ($('#tablas1>tbody>tr').length == 0) {
    sweet_alert_warning('Warning!', 'Insert Details');
    return;
  }
  if ($("#tblPallets>tbody>tr").length == 0) {
    sweet_alert_warning('Warning!', 'Insert Pallets Order');
    return;
	}
  var verificar = 0; 
  $("#tablas1>tbody>tr").each(function (itr, tr) {
  
    if ($(tr).find('#numberPallet').text() == "") {
      ($(tr).closest("tr").find("#numberPallet")).css({ 'border': '1px solid red' });
      verificar = 1;
    } else {
      ($(tr).closest("tr").find("#numberPallet")).css({ 'border': '0px solid red' });
    }
    if ($(tr).find('#FechaTabla').val() == "") {
      ($(tr).closest("tr").find("#FechaTabla")).css({ 'border': '1px solid red' });
      verificar = 2;
    } else {
      ($(tr).closest("tr").find("#FechaTabla")).css({ 'border': '0px solid red' });
    }
    if ($(tr).find('#subtotalRow').text() == "" || $(tr).find('#subtotalRow').text()=="0") {
      ($(tr).closest("tr").find("#subtotalRow")).css({ 'border': '1px solid red' });
      verificar = 3;
    } else {
      ($(tr).closest("tr").find("#subtotalRow")).css({ 'border': '0px solid black' });
    }
    if ($(tr).find('#commentary').text() == "") {
      ($(tr).closest("tr").find("#commentary")).css({ 'border': '1px solid red' });
      verificar = 4
    } else {
      ($(tr).closest("tr").find("#commentary")).css({ 'border': '0px solid red' });
    }   
   
    if (verificar > 0) {
      sweet_alert_warning('Warning!', 'Enter all required fields');
    } else {
      PreGuardar();
    }
    
  })
//  for (var i = 0; i < dataSize.length; i++) {
//    sum = 0;
//    $("#tablas1>tbody>tr>td." + dataSize[i]['name'] + ">input").each(function (itr, tr) {
//        sum = sum + parseInt($(tr).val());
//    })
//    if (sum == 0) {
//      sweet_alert_warning('Advertencia!', 'Debes ingresar al menos una talla.')
//    }
//  }
}

function formatDate(date = new Date(), format = "yyyy/mm/dd") {

  const map = {

    mm: date.getMonth() + 1,

    dd: date.getDate(),

    yyyy: date.getFullYear()

  }



  return format.replace(/mm|dd|yyyy/gi, matched => {

    return (map[matched].toString().length < 2)

      ?

      `0${map[matched]}`

      :

      map[matched];

  })

}

$(function () {

    //$('#tableList').DataTable();
    createList(1, 2);
    //createHeader(1, dataHeader);
    
  loadData();
  CabeceraPackingList();
    loadDefaultValues();

    $(document).on("click", "#btnAdd", function () {
        AddCatVar();

    });
    $(document).on("click", "#btnAddPallets", function () {
        AddPallets();

    });

    $(document).ready(function () {
        $.fn.datepicker.defaults.format = "dd/mm/yyyy";
        var datepicker = $.fn.datepicker.noConflict();
        $.fn.bootstrapDP = datepicker;
    })

    $(document).on("click", ".row-up", function () {
        var $row = $(this).parents('tr');
        if ($row.index() === 0) return; // Don't go above the header
        $row.prev().before($row.get(0));
    });
    //Btn sube Fila
    $(document).on("click", ".row-down", function () {
        var $row = $(this).parents('tr');
        var verify = $row.next().attr('class');
        console.log('down');
        console.log('down');
        if (verify == 'text-align-left') {
            //if (typeof verify === 'undefined') {
            $row.next().after($row.get(0));
        }
    });

    //$("#tablas1>tbody>tr>td.label").find("select").find('#selectLabelTabla').change(function () {
    //    console.log(123);
    //})


    $('table#tableList').DataTable({
        "order": [[7, "ASC"]]
    });
    //$(btnConfirm).click(function () {
    
    //})

    //$(btnDeclinePO).click(function () {
    //    saveDecline();
    //})


    $(document).on("click", ".row-remove", function () {
        console.log($(this).parents('table').attr('id'));
        $idSaleDetail = $(this).parents('tr').children().eq(1).text();
        console.log($idSaleDetail);
        $idtable = $(this).parents('table').attr('id')
        $(this).parents('tr').detach();
        Calcularsubtotal();
        printSummary();
    });
})

function isNumberKeyKG(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
        return false;
    return true;
}

function loadDataCodePack(viaID) {
  $.ajax({
    type: "GET",
    url: "/PO/GetListCodepackByVia?viaID="+viaID,
    async: false,
    success: function (data) {
      var dataJson = JSON.parse(data);
      if (data.length > 0) {
        dataCodePack = dataJson.map(
          obj => {
            return {
              "id": obj.presentationID,
              "name": obj.presentation,
              "weight": obj.weight,
              "type_pack": obj.type_pack
            }
          }
        );
      }
    }
  });
}

function loadData() {

  $.ajax({
    type: "GET",
    url: "/PO/ListSize",
    async: false,
    success: function (data) {
      var dataJson = JSON.parse(data);
      if (data.length > 0) {
        dataSize = dataJson.map(
          obj => {
            return {
              "id": obj.sizeID,
              "name": obj.code,
            }
          }
        );
      }
    }
  });

    $.ajax({
        type: "GET",
        url: "/PO/ListLabel",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataLabel = dataJson.map(
                    obj => {
                        return {
                            "id": obj.categoryID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });


    $.ajax({
        type: "GET",
        url: "/PO/ListVariety",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataVariety = dataJson.map(
                    obj => {
                        return {
                            "id": obj.varietyID,
                          "name": obj.name,
                          "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

  //$.ajax({
  //  type: "GET",
  //    url: "/PO/ListFarm?idGrower=" + $(txtGrowerID).val(),
  //  async: false,
  //  success: function (data) {
  //    var dataJson = JSON.parse(data);
  //    if (data.length > 0) {
  //      dataFarm = dataJson.map(
  //        obj => {
  //          return {
  //            "id": obj.farmID,
  //            "name": obj.description
  //          }
  //        }
  //      );
  //    }
  //  }
  //});

}

function ajaxFarmByGrowerID(growerID) {
    dataFarm = [];
    return $.ajax({
        url: "/PO/ListFarm?idGrower=" + growerID, // fake URL, will throw error     
        type: 'GET',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataFarm = dataJson.map(
                    obj => {
                        return {
                            "id": obj.farmID,
                            "name": obj.description
                        }
                    }
                );
                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //alert("error 1");
        },
        complete: function () {
            loadCombo(dataFarm, 'selectFarm', false);
            $(selectFarm).selectpicker('refresh')
        }
    });
}
function loadDefaultValues() {

    loadCombo(dataVariety, 'selectVariety', false);
    $('#selectVariety').selectpicker('refresh');
    loadCombo([], 'selectCode', false);
    $('#selectCode').selectpicker('refresh');
    loadCombo(dataLabel, 'selectLabel', false);
    $('#selectLabel').selectpicker('refresh');
  loadCombo(dataFarm, 'selectFarm', false);
  $('#selectFarm').selectpicker('refresh');
}

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='0'>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function clearControls() {
    $("#txtPackingList").val('')
    $(txtPackingListID).val('')
    $(txtGrowerID).val('')
    $("#txtExported").val('')
    $("#txtCustomer").val('')
    $("#txtGrower").val('')
    $("#TotalCabecera").val('')

    $("#txtInvoiceNo").val('')
    $("#FechaPLD").val('')
    $("#txtGuideNo").val('')
    $("#txtVessel").val('')

    $("#txtSenasaSealNo").val('')
    $("#txtContainerNo").val('')
    $("#txtCustomSealNo").val('')
    $("#txtShippingSealNo").val('')

    $("#txtThermogregistersNo").val('')
    $("#txtOriginPort").val('')
    $("#txtThermogregistersLocation").val('')
    $("#txtDestinationPort").val('')

    $("#txtSensors").val('')
    $("#FechaETA").val('')
    $("#txtSensorsLocation").val('')
    $("#FechaETD").val('')
}

function createHeader(arrayColumnas, dataHeader) {
    $("#txtPackingList").val(dataHeader[0]['packingList']);
    $(txtOrderProduction).val(dataHeader[0]['orderProduction']);
    
    $(txtPackingListID).val(dataHeader[0]['packingListID'])
    $(txtGrowerID).val(dataHeader[0]['growerID'])
  $("#txtExported").val(dataHeader[0]['grower']);
  $("#txtCustomer").val(dataHeader[0]['customer']);
  $("#txtGrower").val(dataHeader[0]['grower']);
  $("#TotalCabecera").val(dataHeader[0]['totalBoxes']);

  $("#txtInvoiceNo").val(dataHeader[0]['invoice']);
  $("#FechaPLD").val(dataHeader[0]['packingLoadingDate']); 
  $("#txtGuideNo").val(dataHeader[0]['nroGuide']); 
  $("#txtVessel").val(dataHeader[0]['vessel']); 

  $("#txtSenasaSealNo").val(dataHeader[0]['senasaSeal']); 
  $("#txtContainerNo").val(dataHeader[0]['container']); 
  $("#txtCustomSealNo").val(dataHeader[0]['customSeal']); 
  $("#txtShippingSealNo").val(dataHeader[0]['shippingLine']); 

  $("#txtThermogregistersNo").val(dataHeader[0]['thermogRegisters']); 
  $("#txtOriginPort").val(dataHeader[0]['originPort']); 
  $("#txtThermogregistersLocation").val(dataHeader[0]['thermogRegistersLocation']); 
  $("#txtDestinationPort").val(dataHeader[0]['destinationPort']); 

  $("#txtSensors").val(dataHeader[0]['sensors']); 
  $("#FechaETA").val(dataHeader[0]['eta']); 
  $("#txtSensorsLocation").val(dataHeader[0]['sensorsLocation']); 
  $("#FechaETD").val(dataHeader[0]['etd']); 

}
function CabeceraPackingList() {
    var content = "";
    content += "<table id='tablas1' class='table table-hover table-bordered table-striped '  >";
    content += "<thead style='background-color: #13213d' >";
    content += "<tr style='font-size: 13px'>";
    content += "<td syle='min-width: 10px;' class='text-white paleta'>Number of Pallet</td>";
    content += "<td syle='min-width: 10px;' class='text-white fundo'>Farm</td>";
    content += "<td syle='max-width: 60px;' class='text-white paleta'>Production Date</td>";
    content += "<td syle='min-width: 10px;' class='text-white'>Variety</td>";
    content += "<td syle='min-width: 10px;' class='text-white'>Code Pack</td>";
    content += "<td syle='min-width: 10px;' class='text-white'>Label</td>";
    content += "<td syle='min-width: 10px;' class='text-white'>Pack</td>";

  $.each(dataSize, function (i, e) {
    content += "<td syle='min-width: 10px;' class='text-white  " + e.name + "'>";
    content += e.name;
    content += "</td>";
  })
  content += "<td syle='min-width: 10px;' class='text-white'>Total Boxes</td>";
  content += "<td syle='min-width: 10px;' class='text-white fundo'>Description</td>";
  content += "<td style='max-width:80px;' class='text-white'>Operations</td>";
    content += "</tr>";
    content += "</thead><tbody></tbody>";
    content += "<tfoot>"
    content += "<tr>"
  content += "<td colspan = '7' style='background-color: #B6B8C5; text-align: right'> Total</td>"
    for (var i = 0; i < dataSize.length; i++) {
      content += "<td style='min-width: 10px; text-align: right' class='" + dataSize[i]['name'] + "'>";
        content += 0;
        content += "</td>";

    }
    content += "</tr>"
    content += "</tfoot>"
    content += "</table>"
    document.getElementById("tblPL").innerHTML = content;
}


$(document).on("keyup", ".paleta", function (e) {
    SelectPaleta();
})
$(document).on("keyup", ".size", function (e) {

   

});



function Calcularsubtotal() {



    var sum = 0;
    var sum1 = 0;
    for (var i = 0; i < dataSize.length; i++) {
        console.log("#tablas1>tbody>tr>td." + dataSize[i]['name']);
        sum = 0;
        $("#tablas1>tbody>tr>td." + dataSize[i]['name'] + ">input").each(function (itr, tr) {
            sum = sum + parseInt($(tr).val());
        })
        console.log(sum)

        $("#tablas1>tfoot>tr>td." + dataSize[i]['name']).each(function (itr, tr) {
            $(tr).text(sum)
        })
    }



    $("#tablas1>tbody>tr").each(function (itr, tr) {
        sum = 0
        $(tr).find("td.size>input").each(function (itd, td) {
            sum = sum + parseInt($(td).val());
        })
        console.log('fIla')
        console.log(sum)
        sum1 = parseInt(sum + sum1);
        console.log(sum1);

        $(tr).find("td.subtotalRow").text(sum)
    })
    $("#tablas>tbody>tr>td.TotalCabecera>div>input").each(function (itr, td) {

        $(td).val(sum1);

    })
}
function AddCatVar() {

  var VarietyID = $("#selectVariety").val();
  var Variety = []
  //$.each(categoryID, function (ii, ee) {
  $.each(dataVariety, function (i, e) {
    if (e.id == VarietyID) {
      Variety.push(e.abbreviation)
    }
  })
  var FarmID = $("#selectFarm").val();
  var Farm = []
  $.each(dataFarm, function (i, e) {
    if (e.id == FarmID) {
      Farm.push(e.name)
    }
  })

  

    var LabelID = $("#selectLabel").val();

    var Label = []
    //$.each(categoryID, function (ii, ee) {
    $.each(dataLabel, function (i, e) {
        if (e.id == LabelID) {
          Label.push(e.name)
        }
    })


    var CodePackID = $("#selectCode").val();

    var CodePack = [];
  var Weight = [];
  var type_pack = [];

    //$.each(categoryID, function (ii, ee) {
    $.each(dataCodePack, function (i, e) {
        if (e.id == CodePackID) {
            CodePack.push(e.name);
            Weight.push(e.weight);
          type_pack.push(e.type_pack);
        }
    })

    var Pallet = $("#txtPallet").val();
    var VarietyString = Variety.toString();
  var Pack = VarietyString + "_" + Weight + type_pack + "_" + CodePack;
  var Description = "";
    var content = "";
  

  content += '<tr class="text-align-right">';
  content += '<td id="packingListDetailID" class="packingListDetailID" contenteditable="true" style="min-width: 10px;text-align: left; display: none">0</td>';
  content += '<td id="numberPallet" class="paleta" contenteditable="true" style="min-width: 10px;text-align: left">' + Pallet + '</td>';
  content += '<td  class="Farm" style="min-width: 10px;"><select id="farmID" class="form-control form-control-sm" >';
  $.each(dataFarm, function (i, s) {

      if (s.id == FarmID) {
          content += "<option value='" + s.id + "'  selected='selected'>";
          content += s.name;
          content += "</option>";
      } else {
          content += "<option value='" + s.id + "'>";
          content += s.name;
          content += "</option>";
      }

  });
  content += '</select></td>';
  content += "<td class='Date' style='max-width: 80px'><div class='input-group date' data-provide='datepicker'><input type='text' value='' class='form-control form-control-sm' id = 'FechaTabla'  data-date-format='dd/mm/yyyy'> <div class='input-group-addon input-group-sm'><span class='form-control input-group-text input-group-sm form-control-sm'><i class='fa fa-calendar-o fa-sm'></i></span></div></div ></td> ";
  content += '<td  class="variedad" style="min-width: 10px;"><select class="form-control form-control-sm" id="varietyID" onchange="printSummary()">';
    $.each(dataVariety, function (i, s) {

        if (s.id == VarietyID) {
            content += "<option value='" + s.id + "'  selected='selected'>";
            content += s.name;
            content += "</option>";
        } else {
            content += "<option value='" + s.id + "'>";
            content += s.name;
            content += "</option>";
        }
        
    });
  content += '</select></td>';

  
  content += '<td  class="code" style="min-width: 10px;"><select id="codepackID" class="form-control form-control-sm" onchange="printSummary()">';
    $.each(dataCodePack, function (i, s) {

        if (s.id == CodePackID) {
            content += "<option value='" + s.id + "'  selected='selected'>";
            content += s.name;
            content += "</option>";
        } else {
            content += "<option value='" + s.id + "'>";
            content += s.name;
            content += "</option>";
        }

        
    });
    content += '</select></td>';
  content += '<td  class="label" style="min-width: 10px;"><select id="labelID" class="form-control form-control-sm" onchange="printSummary()">';
    $.each(dataLabel, function (i, s) {

        if (s.id == LabelID) {
            content += "<option value='" + s.id + "'  selected='selected'>";
            content += s.name;
            content += "</option>";
        } else {
            content += "<option value='" + s.id + "'>";
            content += s.name;
            content += "</option>";
        }
        
    });
  content += '</select></td>';


  content += '<td class="pack" style="min-width: 10px;text-align: left" id="codepack">' + Pack + '</td>';

    for (var i = 0; i < dataSize.length; i++) {
      content += "<td  style='max-width:40px; text-align: right;' class='size " + dataSize[i]['name'] + " sizeInput' >";
      content += "<input id='size' value='0' style='text-align: right' class='form-control form-control-sm' min='0' pattern='[0 - 9]{ 10 }'  onkeypress='return event.charCode >= 48 && event.charCode <= 57' type='number'></input>";
        content += "</td>";
    }


  content += "<td class='subtotalRow' id='subtotalRow' style='text-align: right'>";
    content += 0;
  content += "</td>";
  content += '<td id="commentary" class="Description" contenteditable="true" style="min-width: 10px;">' + Description + '</td>';
  content += '<td style="text-align: center; max-width:80px"> <span class="icon text-danger"><i class="fas fa-trash row-remove"></i><span></td >';

  content += '</tr>';
  



    //content += "<tbody >";
    //content += "</table >";
    $("#tablas1>tbody").append(content);





    Calcularsubtotal();
    SelectPaleta();
    //SummaryContainer();
    //document.getElementById("").innerHTML = content;




}

function printSummary() {
    $("#tablas1>tbody>tr").each(function (iTr, tr) {
        var row = {}
        var VarietyString = '';
        var CodeName = '';
        var VarietyName = '';
        var Weight = '';
        VarietyString = $(tr).find("td.variedad>select option:selected").text();
        CodeName = $(tr).find("td.code>select option:selected").text();
        VarietyName = ((VarietyString.substring(0, 5).replace(/ /g, ""))).toUpperCase();


        $.each(dataCodePack, function (i, e) {
            if (e.name == CodeName) {
                Weight = e.weight;
            }
        })


        //$("#tablas1>tbody>tr>td.pack").each(function (itr, tr) {
        var Pack = VarietyName + "_" + Weight + "_" + CodeName;
        $(this).find("td.pack").text(Pack);
        //$(this).$("td.pack").text(Pack);
        //$("td.pack").text(Pack);
        //})

    })

    //    var Pack = VarietyName + "_" + Weight + "_" + CodeName;
    //var oSizeVarCod = []
    //$.each(dataSize, function () {
    //    var oSize = { size: dataSize.name}
    //    $("#tablas1>td." + dataSize.name).each(function (i, e) {
    //    });
    //})
    var allrows = []
    $("#tablas1>tbody>tr").each(function (iTr, tr) {
        var row = {}

        var variety = $(tr).find("td.variedad>select option:selected").text();
        var code = $(tr).find("td.code>select option:selected").text();
        var label = $(tr).find("td.label>select option:selected").text();

        row = {
            variety: variety,
            code: code,
            label: label,
            size: 0,
            box: 0
        }

        $.each(dataSize, function (i, e) {
            console.log(e)
            //$(tr).find("td." + dataSize.name).text('aaaaaaaaaaaaaa');
            //$(tr).find("td.20").text('aaaaaaaaaaaaaa');
            var box = $(tr).find("td." + e.name + ">input").val();
            //console.log(box)
            if (parseInt(box) > 0) {
                row["size"] = e.name;
                row["box"] = box;
                row[e.name] = box;
            }


        })

        allrows.push(row);
    });
    console.log(JSON.stringify(allrows))

    let uniqsVarieties = allrows.map(item => item.variety)
        .filter((value, index, self) => self.indexOf(value) === index);

    let uniqsSizes = allrows.map(item => item.size)
        .filter((value, index, self) => self.indexOf(value) === index);

    let uniqscode = allrows.map(item => item.code)
        .filter((value, index, self) => self.indexOf(value) === index);

    let uniqslabel = allrows.map(item => item.label)
        .filter((value, index, self) => self.indexOf(value) === index);

    //console.log(JSON.stringify(uniqsVarieties));


    var finallist = []
    $.each(uniqscode, function (iCode, code) {
        let rowsByCode = allrows.filter(item => item.code == code)

        $.each(uniqsVarieties, function (iVariety, variety) {
            let rowsByCodebyVariety = rowsByCode.filter(item => item.variety == variety);

            $.each(uniqsSizes, function (iSize, size) {
                let rowsByCodebyVarietybySize = rowsByCodebyVariety.filter(item => item.size == size);

                $.each(uniqslabel, function (iLabel, label) {
                    rowsCVSByLabel = rowsByCodebyVarietybySize.filter(item => item.label == label);

                    var boxes = 0
                    $.each(rowsCVSByLabel, function (i, e) {
                        boxes = boxes + parseInt(e.box);
                    })

                    if (parseInt(boxes) > 0) {
                        finallist.push({ code: code, variety: variety, size: size, label: label, box: boxes });
                    }
                })
            })

        })

    })
    console.log(JSON.stringify(finallist));
    //var type_pack = dataCodePack[$("#selectCode option:selected").val()]['type_pack'];

    //var packing = type_pack + '_' + labelName;


    $('#tblSummary>tbody').empty();
    $.each(finallist, function (i, e) {
        var packing = '';
        //var labelString = label.toString();
        //var labelName = ((labelString.substring(0, 3).replace(/ /g, ""))).toUpperCase();
        ///Michelle

        $.each(dataCodePack, function (codei, codee) {

            if (codee.name == e.code) {
                labelString = (e.label).toString();
                labelName = ((labelString.substring(0, 3).replace(/ /g, ""))).toUpperCase();
                packing = codee.type_pack + '_' + labelName;
                console.log(packing);
            }


        })
        //

        var tr = '<tr>';
        tr += '<td style="font-size: 13px">1</td>'
        tr += '<td style="font-size: 13px">' + e.variety + '</td>'
        tr += '<td style="font-size: 13px">' + packing + '</td>'
      tr += '<td style="font-size: 13px">' + e.size + '</td>'
      tr += '<td style= "text-align: center;font-size: 13px">' + e.box + '</td></tr>'
        console.log(tr);
        $('#tblSummary>tbody').append(tr);
    })
}

function groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}


function getTable1(data) {
    var indice = -1;
    var tr = "";
    console.log(data);

    $('table#tblSummary>tbody>tr').each(function (i, e) {
        var Ssize = $(this).find("td.sizeS").text();
        var Scode = $(this).find("td.codeS").text();
        //console.log("size"+Ssize);
        var Svariety = $(this).find("td.varietyS").text();
        //console.log("variety" + Svariety);
        //console.log("variety2" + data);

        if (Svariety == data.variety && Ssize == data.size && Scode == data.codepack) {
            indice = $(this).index();
        };
    })
    if (indice != -1) {
        var boxes = $("table#tblSummary>tbody>tr").find("td.boxes").text();
        var TotalBoxes = parseInt(boxes) + parseInt(data.value);
        $("table#tblSummary>tbody>tr").find("td.boxes").text(TotalBoxes);
    }
    else {

        tr += "<tr>";
        tr += "<td>" + "1" + "</td>";
        tr += "<td class='varietyS'>" + data.variety + "</td>";
        tr += "<td>" + data.packing + "</td>";
        tr += "<td class='sizeS'>" + data.size + "</td>";
        tr += "<td class='boxes'>" + data.value + "</td>";
        tr += "<td style='display:none' class='codeS'>" + data.codepack + "</td>";
        tr += "</tr>";
    }
    $("#tblSummary>tbody").append(tr);
}

function SummaryContainer() {
    $("#tblSummary>tbody>tr").each(function (e) {

    })
    $("#tablas1>tbody>tr").each(function (itr, tr) {
        console.log(arreglo);
        $(tr).find("td.variedad").each(function (itd, td) {
            //arreglo.Variedad = $("#selectVariety option:selected").text();
            arreglo.push($("#selectVariety option:selected").text());
        })

        for (var i = 0; i < dataSize.length; i++) {


            $(tr).find("td." + dataSize[i]['name']).each(function (itd, td) {

                arreglo.push($(td).text());

            })


        }



    })
}

function SelectPaleta() {
    paleta = [];

    $("#tablas1>tbody>tr>td.paleta").each(function (itr, tr) {
        var alert = 0;
        $.each(paleta, function (i, e) {
            if (e == $(tr).text()) {
                alert = 1;
            }
        })
        if (alert == 0) {
            paleta.push($(tr).text());
            console.log(paleta);
        }
    })
    content = "";
    content += '<tr >';
  content += '<td colspan="2" style="min-width: 180px;"><select id="SelectPaletaTabla" class="form-control form-control-sm">';
    j = 0;
    $.each(paleta, function (i, s) {
        content += "<option value='" + j + "' >";
        content += s;
        content += "</option>";

        j = j + 1;
    })
    content += '</select></td>';
  content += '<td  style="max-width: 20px;"> <button id="btnAddPallets" type="button" class="btn  btn-sm  text-white btn-block form-control-sm" style=" background-color: #13213d;">Add  <span class="icon text-white"><i class="fas fa-plus-circle fa-sm"></i></span></button></td></tr>';
    $("#tblPallets>tbody").empty().append(content);
    //$("#tblPallets>tbody").append(content);
}

function AddPallets() {

  var selectPaleta = $("#SelectPaletaTabla option:selected").text();
  
  $("#tblPallets>tfoot>tr>td.paleta").each(function (itr, tr) {
    var alert = 0;
    $.each(paletaTabla, function (i, e) {
      if (e == selectPaleta) {
        alert = 1;
      }
    })
    if (alert == 0) {
      paletaTabla.push(selectPaleta);


    }
  })
  

  var content2 = '';
  $.each(paletaTabla, function (i, tr) {
    
    content2 += '<tr class="text-align-left">';
    content2 += '<td style="max-width:2px; text-align: center; font-size: 13px" >&nbsp;<i class="fas fa-arrow-up row-up"></i>&nbsp;<i class="fas fa-arrow-down row-down "></i></td>';
    content2 += '<td class="paleta" style="font-size: 13px">';
    content2 += tr;
    content2 += '</td>';
    content2 += '<td style="text-align: center;"> <span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td >';
    content2 += '</tr >';


  })
  $("#tblPallets>tfoot").empty().append(content2);

}



function createList(wiNI, wEn) {

    $("table#tableList tbody").html('');
    $.ajax({
        type: "GET",
        url: "/PO/ListGetAll",
        //async: false,
        success: function (data) {
            if (data.length > 0) {
              var dataJson = JSON.parse(data);
              dataListTable = JSON.parse(data);
                $.each(dataJson, function (i, e) {
                  var tr = '';
                  tr += '<tr>';
                   tr += '<td style="display:none">' + e.orderProductionID + '</td>';
                  tr += '<td style="display:none">' + e.packingListID + '</td>';
                  tr += '<td style="display:none">' + e.viaID + '</td>';
                  tr += '<td>' + e.po + '</td>';
                  tr += '<td>' + e.packingList + '</td>';
                  tr += '<td>' + e.grower + '</td>';
                  tr += '<td>' + e.customer + '</td>';
                  tr += '<td>' + e.destinationPort + '</td>';
                  tr += '<td>' + e.totalBoxes + '</td>';
                  tr += '<td >' + e.etd + '</td>';
                  tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" onclick="openModal(' + e.orderProductionID + ', ' + e.packingListID+','+ e.viaID+')" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-sm"></i></button></td>';
                    tr += '</tr>';
                    console.log(tr);
                    $('table#tableList tbody').append(tr);
                })
            }

        }
    });

}
function cleanControlsModal() {
  //$('table#tablas>tbody').empty();
  //$('#tablas').DataTable().clear();
  //$('#tablas').DataTable().destroy(); 
    clearControls();
  $('table#tablas1>tbody').empty();
  //$('table#tablas1>tfoot').empty();
  //$('#tablas1').DataTable().clear();
  //$('#tablas1').DataTable().destroy(); 
  loadDefaultValues();
  $('table#tblSummary>tbody').empty();
  //$('#tblSummary').DataTable().clear();
  //$('#tblSummary').DataTable().destroy();
  $('table#tblPallets>tbody').empty();
  //$('#tblPallets').DataTable().clear();
  //$('#tblPallets').DataTable().destroy(); 
}

function loadDetails(packingListID,viaID) {
    ajaxFarmByGrowerID($(txtGrowerID).val());

    loadDataCodePack(viaID);
    loadCombo(dataCodePack, 'selectCode', false);
    $('#selectCode').selectpicker('refresh');

    $.ajax({
        type: "GET",
        url: "/PO/ListDetailsAll?packingListID=" + packingListID,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataDetails = JSON.parse(data);
                $.each(dataJson, function (i, e) {

                    var tr = '';
                    tr += '<tr style="font-size: 13px" class="text-align-right">';
                    tr += '<td id="packingListDetailID" class="packingListDetailID" contenteditable="true" style="min-width: 10px;text-align: left; display: none">' + e.packingListDetailID + '</td>';
                    tr += '<td id="numberPallet" class="paleta" contenteditable="true" style="min-width: 10px;text-align: left">' + e.numberPallet + '</td>';
                    tr += '<td  class="Farm" style="min-width: 10px;"><select id="farmID" class="form-control form-control-sm" >';
                    $.each(dataFarm, function (i, s) {

                        if (s.id.trim() == e.farmID) {
                            tr += "<option value='" + s.id + "'  selected='selected'>";
                            tr += s.name;
                            tr += "</option>";
                        } else {
                            tr += "<option value='" + s.id + "'>";
                            tr += s.name;
                            tr += "</option>";
                        }
                        
                    });
                    tr += '</select></td>';

                    tr += "<td class='Date' style='max-width: 80px'><div class='input-group date' data-provide='datepicker'><input type='text' value='" + e.dateProduction + "' class='form-control form-control-sm' id = 'FechaTabla'  data-date-format='dd/mm/yyyy'> <div class='input-group-addon input-group-sm'><span class='form-control input-group-text input-group-sm form-control-sm'><i class='fa fa-calendar-o fa-sm'></i></span></div></div ></td> ";
                    tr += '<td class="variedad" style="min-width: 10px;"><select id="varietyID" class="form-control form-control-sm" onchange="printSummary()">';
                    $.each(dataVariety, function (i, s) {

                        if (s.name == e.variety) {
                            tr += "<option value='" + s.id + "'  selected='selected'>";
                            tr += s.name;
                            tr += "</option>";
                        } else {
                            tr += "<option value='" + s.id + "'>";
                            tr += s.name;
                            tr += "</option>";
                        }
                        
                    });
                    tr += '</select></td>';

                    tr += '<td class="code" style="min-width: 10px;"><select id="codepackID" class="form-control form-control-sm" onchange="printSummary()">';
                    $.each(dataCodePack, function (i, s) {

                        if (s.name == e.codepack) {
                            tr += "<option value='" + s.id + "'  selected='selected'>";
                            tr += s.name;
                            tr += "</option>";
                        } else {
                            tr += "<option value='" + s.id + "'>";
                            tr += s.name;
                            tr += "</option>";
                        }
                        
                    });
                    tr += '</select></td>';

                    tr += '<td class="label" style="min-width: 10px;"><select id="labelID" class="form-control form-control-sm" onchange="printSummary()">';
                    $.each(dataLabel, function (i, s) {

                        if (s.name == e.label) {
                            tr += "<option value='" + s.id + "'  selected='selected'>";
                            tr += s.name;
                            tr += "</option>";
                        } else {
                            tr += "<option value='" + s.id + "'>";
                            tr += s.name;
                            tr += "</option>";
                        }
                        
                    });
                    tr += '</select></td>';
                    tr += '<td class="pack" style="min-width: 10px;;text-align: left" id="codepack">' + e.codepack + '</td>';
                    //for (var i = 0; i < dataSize.length; i++) {
                    //  tr += "<td  style='max-width:40px; text-align: right;' class='size " + dataSize[i]['name'] + " sizeInput' >";
                    //  tr += "<input value='"+e.totalBoxes+"' style='text-align: right' class='form-control form-control-sm' min='0' pattern='[0 - 9]{ 10 }'  onkeypress='return event.charCode >= 48 && event.charCode <= 57' type='number'></input>";
                    //  tr += "</td>";
                    //}

                    $.each(dataSize, function (i, element) {
                        tr += "<td  style='max-width:40px; text-align: right;' class='size " + element.name + " sizeInput' >";
                        if (e.size == element.id) {
                            tr += "<input id='size' value='" + e.totalBoxes + "' style='text-align: right' class='form-control form-control-sm' min='0' pattern='[0 - 9]{ 10 }'  onkeypress='return event.charCode >= 48 && event.charCode <= 57' type='number'></input>";
                        } else {
                            tr += "<input id='size' value='0' style='text-align: right' class='form-control form-control-sm' min='0' pattern='[0 - 9]{ 10 }'  onkeypress='return event.charCode >= 48 && event.charCode <= 57' type='number' ></input>";
                        }

                        tr += "</td>";
                    })

                    tr += "<td id='subtotalRow' class='subtotalRow' style='text-align: right'>";
                    tr += 0;
                    tr += "</td>";
                    tr += '<td id="commentary" class="Description" contenteditable="true" style="min-width: 10px;text-align: left">' + e.commentary + '</td>';
                    tr += '<td style="text-align: center; max-width:80px"> <span class="icon text-danger"><i class="fas fa-trash row-remove"></i><span></td >';

                    tr += '</tr>';

                    $("#tablas1>tbody").append(tr);
                    Calcularsubtotal();
                    SelectPaleta();
                    //$('table#tableList tbody').append(tr);
                })
            }
        }
    });
    $.ajax({
        type: "GET",
        url: "/PO/ListGetDetSum?packingListID=" + packingListID,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataDetailsSum = JSON.parse(data);
            }
        }
    })

    $.each(dataDetailsSum, function (codei, e) {

        var tr = '<tr style="font-size: 13px">';
        tr += '<td>' + e.cat + '</td>'
        tr += '<td>' + e.variety + '</td>'
        tr += '<td>' + e.packing + '</td>'
        tr += '<td>' + e.size + '</td>'
        tr += '<td style= "text-align: right;">' + e.totalboxes + '</td></tr>'
        console.log(tr);
        $('#tblSummary>tbody').append(tr);

    })
}
function openModal(orderProductionID, packingListID, viaID) {
  dataHeader = [];
  _orderProductionID = orderProductionID;
  _packingListID = packingListID;
  cleanControlsModal();
  $.ajax({
    type: "GET",
    url: "/PO/ListHeaderAll?orderProductionID="+orderProductionID,
    //async: false,
    success: function (data) {
      if (data.length > 0) {
        var dataJson = JSON.parse(data);
          dataHeader = JSON.parse(data);
          createHeader(0, dataHeader);
          loadDetails(packingListID,viaID);
      }
    }
  });



  

 

}





function sumPalletsConfirm() {
    var pallets = 0;
    $('input.txtPalletsConfirm').each(function (i, e) {
        pallets += parseInt($(e).val());
    })
    $('.tdSumPalletsConfirm').text(pallets)
}

function sumPallets() {
    var pallets = 0;
    $('.txtPallets').each(function (i, e) {
        pallets += parseInt($(e).text());
    })
    $('.tdSumPallets').text(pallets)
}
$(document).ready(function () {
    $(document).on('focusin', 'input', function () {
        //console.log("Saving value " + $(this).val());
        $(this).data('val', $(this).val());
    }).on('change', 'td.sizeInput>input', function () {

        var td = $(this).parents('td');
        var tr = $(this).parents('tr');



        var colIndex = td.parent().children().index(td);
        //var rowIndex = tr.parent().children().index(tr);
        console.log("COLORED: " + colIndex);
        var size;

        var variety = $(this).parent().parent().find("td.variedad").find("select").find('option:selected').text();
        var codepack = $(this).parent().parent().find("td.code").find("select").find('option:selected').text();
        var type_pack = dataCodePack[$("#selectCode option:selected").val()]['type_pack'];
        var label = $(this).parent().parent().find("td.label").find("select").find('option:selected').text();

        var labelString = label.toString();
        var labelName = ((labelString.substring(0, 3).replace(/ /g, ""))).toUpperCase();
        var packing = type_pack + '_' + labelName;

        console.log(packing);

        $("#tablas1>thead>tr>td").each(function (i, e) {
            console.log("INDICES: " + i + " //" + colIndex)
            if (i == colIndex) {
                size = $(this).text();
            }
        });

        var data = {};
        data.size = size;
        data.value = $(this).val();
        data.variety = variety;
        data.codepack = codepack;
        data.packing = packing;

        if (parseInt($(this).val()) > 0) {
            $(tr).find("td.sizeInput>input").each(function (iInput, input) {
                $(input).prop("disabled", true);
            })
            $(this).prop("disabled", false);
        } else {
            $(tr).find("td.sizeInput>input").each(function (iInput, input) {
                $(input).prop("disabled", false);
            })

        }


        //alert(123)
        //getTable1(data);
        printSummary();
        Calcularsubtotal();
    });
});



function PreGuardar() {
  Swal.fire({
    title: '¿Desea guardar?',
    text: "Los datos serán almacenados permanentemente en el sistema.",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Sí, guardar!',
    cancelButtonText: 'No, cancelar!',
    confirmButtonColor: '#4CAA42',
    cancelButtonColor: '#d33'
  }).then((result) => {
      if (result.value) {
          btnSave_Click()
          //Promise.all([btnSave_Click(), ]).then(() => { // try removing ajax 1 or replacing with ajax2
          //    alert('All Ajax done with success!')
          //    //sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
          //    ////loadListPrincipal();
          //    //$('#myModal').modal('hide');
          //    //btnSaveDetails_Click()
          //}).catch((response) => {
          //    //alert('All Ajax done: some failed!')
          //})

          //Promise.
      //btnSave_Click();
      //btnSaveDetails_Click();
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      sweet_alert_error('Cancelado!', 'Se canceló el proceso de guardado.')
    }
  });
}

function btnSave_Click() {
  var details = []
  var packingListID = 0;
  var orderProductionID = _orderProductionID;
  var nroPackingList = "";
  var customerID = "";
  var growerID = "";
  var totalBoxes = "";
  var invoiceGrower = "";
  var packingLoadDate = "";
  var nroGuide = "";
  var vessel = "";
  var senasaSeal = "";
  var container = "";
  var customSeal = "";
  var shippingLine = "";
  var thermogRegisters = "";
  var originPort = "";
  var thermogRegistersLocation = "";
  var destinationPort = "";
  var sensors = "";
  var ETA = "";
  var sensorsLocation = "";
  var ETD = "";

  nroPackingList= $("#txtPackingList").val();

  //$("#txtExported").val();
  //customerID= $("#txtCustomer").val();
  customerID = 0;
  growerID= $("#txtGrower").val();
  totalBoxes= $("#TotalCabecera").val();

  invoiceGrower= $("#txtInvoiceNo").val();
  //packingLoadDate= $("#FechaPLD").val();
  nroGuide= $("#txtGuideNo").val();
  vessel= $("#txtVessel").val();

  senasaSeal= $("#txtSenasaSealNo").val();
  container= $("#txtContainerNo").val();
  customSeal= $("#txtCustomSealNo").val();
  shippingLine= $("#txtShippingSealNo").val();

  thermogRegisters= $("#txtThermogregistersNo").val();
  originPort = $("#txtOriginPort").val();
  thermogRegistersLocation= $("#txtThermogregistersLocation").val();
  destinationPort = $("#txtDestinationPort").val();

  sensors = $("#txtSensors").val();
  //ETA = $("#FechaETA").val();
  sensorsLocation = $("#txtSensorsLocation").val();
  //ETD = $("#FechaETD").val(); 




  totalBoxes = $('#TotalCabecera').val();

  packingLoadDate1 = $('#FechaPLD').val();
  frompackingLoadDate = packingLoadDate1.split("/");
  packingLoadDate = frompackingLoadDate[2] + frompackingLoadDate[1] + frompackingLoadDate[0];

  ETA1 = $('#FechaETA').val();
  fromETA = ETA1.split("/");
  ETA = fromETA[2] + fromETA[1] + fromETA[0];

  ETD1 = $('#FechaETD').val();
  fromETD = ETD1.split("/");
  ETD = fromETD[2] + fromETD[1] + fromETD[0];

  var booking = 0;

  var BLAWB = 0;

  var viaID = 1;




  var packingList = false;
  if (1 == 1) {
    var frmPackingList = new FormData();
    frmPackingList.append("packingListID", packingListID);
    frmPackingList.append("orderProductionID", orderProductionID);
    frmPackingList.append("nroPackingList", nroPackingList);
    frmPackingList.append("customerID", customerID);
    frmPackingList.append("growerID", growerID);
    frmPackingList.append("totalBoxes", totalBoxes);
    frmPackingList.append("invoiceGrower", invoiceGrower);
    frmPackingList.append("packingLoadDate", packingLoadDate);
    frmPackingList.append("nroGuide", nroGuide);
    frmPackingList.append("vessel", vessel);
    frmPackingList.append("senasaSeal", senasaSeal);
    frmPackingList.append("container", container);
    frmPackingList.append("customSeal", customSeal);
    frmPackingList.append("shippingLine", shippingLine);
    frmPackingList.append("thermogRegisters", thermogRegisters);
    frmPackingList.append("originPort", originPort);
    frmPackingList.append("thermogRegistersLocation", thermogRegistersLocation);
    frmPackingList.append("destinationPort", destinationPort);
    frmPackingList.append("sensors", sensors);
    frmPackingList.append("ETA", ETA);
    frmPackingList.append("sensorsLocation", sensorsLocation);
    frmPackingList.append("ETD", ETD);
    frmPackingList.append("booking", booking);
    frmPackingList.append("BLAWB", BLAWB);
    frmPackingList.append("viaID", viaID);
    console.log(frmPackingList);

    var api = "/PO/SavePkPO"
    console.log(api);
    $.ajax({
      type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
      url: api,
      //async: false,
      contentType: "application/json",
      dataType: 'json',
      data: JSON.stringify(Object.fromEntries(frmPackingList)),
      success: function (datares) {
        if (datares.length > 0) {
          //sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
          ////loadListPrincipal();
          //$('#myModal').modal('hide');
            _packingListID = datares[0].packingListID;
            btnSaveDetails_Click();
        } else {
          sweet_alert_error("Error", "NO se guardaron los datos!");
          console.log('Ocurrió un error al grabar el registro')
          console.log(api)
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        sweet_alert_error("Error", "NO se guardaron los datos!");
        console.log(api)
        console.log(xhr);
        console.log(ajaxOptions);
        console.log(thrownError);
      }
    });

  }

 
}

function btnSaveDetails_Click() {


  var allrows = []
  $("#tablas1>tbody>tr").each(function (iTr, tr) {
    var row = {}
    //Preguntar si en el detalle va fundo y la fecha cual es la que se guardaba?
    //var farm = $(tr).find("td.Farm>select option:selected").val();
    //var pallet = $(tr).find("td.paleta").text();
    //var variety = $(tr).find("td.variedad>select option:selected").val();
    //var code = $(tr).find("td.code>select option:selected").val();
    //var label = $(tr).find("td.label>select option:selected").val();
    //var pack = $(tr).find("td.pack").text();
    //var totalboxes = parseInt($(tr).find("td.subtotalRow").text());
    //var description = $(tr).find("td.Description").text();
    //var date1 = $('#FechaTabla').val();
    
    
    
    var packingListDetailID = parseInt(($(tr).find("#packingListDetailID").text()) == "" ? 0 : $(tr).find("#packingListDetailID").text());
    var farm = $(tr).find("#farmID").val();
    var pallet = $(tr).find("#numberPallet").text();
    var variety = $(tr).find("#varietyID").val();
    var code = $(tr).find("#codepackID").val();
    var label = $(tr).find("#labelID").val();
    var pack = $(tr).find("#codepack").text();
    var totalboxes = parseInt($(tr).find("#subtotalRow").text());
    var description = $(tr).find("#commentary").text();
    var date1 = $('#FechaTabla').val();
    var fromdate = date1.split("/");
    var date = fromdate[2] + fromdate[1] + fromdate[0];
    row = {
      packingListDetailID: packingListDetailID,
      farm: farm,
      pallet: pallet,
      variety: variety,
      code: code,
      label: label,
      pack: pack,
      totalboxes: totalboxes,
      description: description,
      date: date,
      size: 0,
      box: 0,
      order: 0
    }

    $.each(dataSize, function (i, e) {
      console.log(e)
      //$(tr).find("td." + dataSize.name).text('aaaaaaaaaaaaaa');
      //$(tr).find("td.20").text('aaaaaaaaaaaaaa');
      var box = $(tr).find("td." + e.name + ">input").val();
      //console.log(box)
      if (parseInt(box) > 0) {
        row["size"] = e.id;
        row["box"] = box;
        row[e.name] = box;
      }
    })

    $('#paleta').each(function (i, e) {
      if (pallet == $(e).text()) {
        row["order"] = parseInt($(e).index());
      }

      //$('tblPallets>tfoot>tr').each(function (i, e) {
      //if (pallet == $(e).$('#paleta').text()) {
      //    row["order"] = (parseInt($(e).index())+1);
      //}

    });
    allrows.push(row);
  });
  console.log(allrows);

  var packingList = false;
  if (1 == 1) {
    var frmPackingList = new FormData();
    $.each(allrows, function (i, e) {
      frmPackingList.append("packingListDetailID", e.packingListDetailID);
      frmPackingList.append("packingListID", _packingListID);
      frmPackingList.append("numberPallet", e.pallet);
      frmPackingList.append("orderPallet", e.order);
      frmPackingList.append("farmID", e.farm);
      frmPackingList.append("codePackID", e.code);
      frmPackingList.append("packageProductID", 1);
      frmPackingList.append("categoryID", e.label);
      frmPackingList.append("varietyID", e.variety);
      frmPackingList.append("sizeID", e.size);
      frmPackingList.append("packingDate", e.date);
      frmPackingList.append("quantityBoxes", e.box);
      frmPackingList.append("description", e.description);
      frmPackingList.append("packingListDetailByVarietyID", 0);


      var api = "/PO/SaveDetailPkPO"
      console.log(api);
      $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(Object.fromEntries(frmPackingList)),
        success: function (datares) {
          if (datares.length > 0) {
            //sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
            ////loadListPrincipal();
            //$('#myModal').modal('hide');
          } else {
            sweet_alert_error("Error", "NO se guardaron los datos!");
            console.log('Ocurrió un error al grabar el registro')
            console.log(api)
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          sweet_alert_error("Error", "NO se guardaron los datos!");
          console.log(api)
          console.log(xhr);
          console.log(ajaxOptions);
          console.log(thrownError);
        }
      });
      //$.ajax({
      //  type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
      //  url: api,
      //  async: false,
      //  contentType: "application/json",
      //  dataType: 'json',
      //  data: JSON.stringify(Object.fromEntries(frmPackingList)),
      //  success: function (datares) {
      //    console.log(datares)
      //    if (datares.length > 0) {
      //      packingList = true;
      //    } else {
      //      console.log('Ocurrió un error al grabar la receta')
      //      console.log(datares)
      //    }
      //  },
      //  error: function (jqXHR, textStatus, errorThrown) {
      //    console.log(errorThrown);
      //    console.log(textStatus);
      //    console.log(jqXHR);
      //  }
      //});

    })
  }

  if (packingList) {
    //alert("okok");
    //list(1, 74, 0);
    $('#myModal').modal('hide');
    //cleanControls();
  }



}
