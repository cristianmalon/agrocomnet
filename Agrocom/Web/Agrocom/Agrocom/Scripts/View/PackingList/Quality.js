﻿//$(document).ready(function () {
var sessionUserID = $('#lblSessionUserID').text()
var paletaTabla = [];
var dataVariety = [];
var dataFarm = [];
var dataCodePack = [];
var dataLabel = [];
var arreglo = [];
var dataListTable = [];
var dataHeader = [];
var dataDetails = [];
var dataDetailsSum = [];
var _packingListID = 0;
var _orderProductionID = 0;
var dataSize = [];
var _Total = 0;

dataHeader = [
    {
        "packinglist": "CAL-ARA 0004/20019",
        "exported": "OZBLU",
        "grower": "OZB",
        "invoice": "001-0004-0000054",
        "guide": "GRR 0023-00",
        "senasa": "126503",
        "custom": "011LA094",
        "thermogregistersNo": "GDEQN",
        "thermogregistersLocation": "OZPO400",
        "sensors": "S1/S2/S3",
        "sensorsL": "5254",
        "customer": "OZBLU",
        "totalCases": "7800",
        "packingLoading": "23/0/2019",
        "vessel": "MAERSK ",
        "containerNo": "MNBU36",
        "shippingSealNo": "HSA4",
        "originPort": "CALLAO-PERU",
        "destinationport": "SHANGHAI-CHINA",
        "ETA": "25/10/2019",
        "ETD": "25/10/2019"
    },

]

function btnSaveRow_Click(xthis) {
    var x = 0;
    var TShippingQO = parseInt($(xthis).closest("tr").find('#AcceptedQO').text()) + parseInt($(xthis).closest("tr").find('#OutSpecsQO').text());
    if (TShippingQO == parseInt($(xthis).closest("tr").find('#totalBoxesRow').text())) {
        sweet_alert_success('Save!', 'The data has been saved.');
        ($(xthis).closest("tr").find(".TShippingQO")).css({ 'border': '0px solid black' })
    } else {
        x = 1;

        ($(xthis).closest("tr").find(".TShippingQO")).css({ 'border': '1px solid red' })//9;

    }

    if (x > 0) {
        sweet_alert_warning('Warning!', 'Missing boxes to assign.');
    } else {
        sweet_alert_success('Save!', 'The data has been saved row.');
    }

}


function formatDate(date = new Date(), format = "yyyy/mm/dd") {

    const map = {

        mm: date.getMonth() + 1,

        dd: date.getDate(),

        yyyy: date.getFullYear()

    }


    return format.replace(/mm|dd|yyyy/gi, matched => {

        return (map[matched].toString().length < 2)

            ?

            `0${map[matched]}`

            :

            map[matched];

    })

}
function QuitarCheck(x) {
    Swal.fire({
        title: '¿Are you sure?',
        text: "Information related to this quality control will be removed.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, removed!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            if (x == 1) {
                $(".QCOrigen").toggle();
                //$(".QCOrigen").prop('hidden', true);
                $('#txtInspectedByQCO').prop('disabled', true);
                $('#txtInspectedDateQCO').prop('disabled', true);
                $('#txtQCReportQCO').prop('disabled', true);
                $('#txtNotesQCO').prop('disabled', true);

                $('#txtInspectedByQCO2').prop('disabled', true);
                $('#txtInspectedDateQCO2').prop('disabled', true);
                $('#txtQCReportQCO2').prop('disabled', true);
                $('#txtNotesQCO2').prop('disabled', true);

                $('#txtInspectedByQCO3').prop('disabled', true);
                $('#txtInspectedDateQCO3').prop('disabled', true);
                $('#txtQCReportQCO3').prop('disabled', true);
                $('#txtNotesQCO3').prop('disabled', true);

                $('#txtInspectedByQCO').prop('disabled', true);
                $('#txtInspectedDateQCO').prop('disabled', true);
                $('#txtQCReportQCO').prop('disabled', true);
                $('#txtNotesQCO').prop('disabled', true);

                $('#txtInspectedByQCO').val('');
                $('#txtInspectedDateQCO').val('');
                $('#txtQCReportQCO').val('');
                $('#txtNotesQCO').val('');

                $('#txtInspectedByQCO2').val('');
                $('#txtInspectedDateQCO2').val('');
                $('#txtQCReportQCO2').val('');
                $('#txtNotesQCO2').val('');

                $('#txtInspectedByQCO3').val('');
                $('#txtInspectedDateQCO3').val('');
                $('#txtQCReportQCO3').val('');
                $('#txtNotesQCO3').val('');

                $('#txtInspectedByQCO4').val('');
                $('#txtInspectedDateQCO4').val('');
                $('#txtQCReportQCO4').val('');
                $('#txtNotesQCO4').val('');

                $('#tablas1>tbody>tr').each(function (i, e) {
                    $(e).find('input#AcceptedQO').val("0");
                    $(e).find('input#OutSpecsQO').val("0");
                    $(e).find('input#RejectedQO').val("0");
                    $(e).find('#AdittionalNQO').text("");
                    $(e).find('input#TShippingQO').val("0");

                })


            }
            if (x == 2) {
                $(".QCDestination").toggle();
                //$('#QCDestination').prop('hidden', true);  
                $('#txtInspectedByQCD').prop('disabled', true);
                $('#txtInspectedDateQCD').prop('disabled', true);
                $('#txtQCReportQCD').prop('disabled', true);
                $('#txtNotesQCD').prop('disabled', true);
                $('#txtInspectedByQCD').val('');
                $('#txtInspectedDateQCD').val('');
                $('#txtQCReportQCD').val('');
                $('#txtNotesQCD').val('');
                $('#tablas1>tbody>tr').each(function (i, e) {
                    $(e).find('input#AcceptedQD').val("0");
                    $(e).find('input#OutSpecsQD').val("0");
                    $(e).find('input#RejectedQD').val("0");
                    $(e).find('#AdittionalNQD').text("");
                    $(e).find('input#TShippingQD').val("0");

                })
            }
            if (x == 3) {
                $(".CustomerDestination").toggle();
                //$('#CustomerDestination').prop('hidden', true);  
                $('#txtInspectedByQCCD').prop('disabled', true);
                $('#txtInspectedDateQCCD').prop('disabled', true);
                $('#txtQCReportQCCD').prop('disabled', true);
                $('#txtNotesQCCD').prop('disabled', true);
                $('#txtInspectedByQCCD').val('');
                $('#txtInspectedDateQCCD').val('');
                $('#txtQCReportQCCD').val('');
                $('#txtNotesQCCD').val('');
                $('#tablas1>tbody>tr').each(function (i, e) {
                    $(e).find('input#AcceptedQCD').val("0");
                    $(e).find('input#OutSpecsQCD').val("0");
                    $(e).find('input#RejectedQCD').val("0");
                    $(e).find('#AdittionalNQCD').text("");
                    $(e).find('input#TShippingQCD').val("0");

                })
            }
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Check was canceled.')
            if (x == 1) {
                $("#checkQCOrigen").prop('checked', true);
            }
            if (x == 2) {
                $("#checkQCDestination").prop('checked', true);
            }
            if (x == 3) {
                $("#checkCustomerDestination").prop('checked', true);
            }
        }
    });
}

function Onchange_checkQCOrigen(xthis) {
    if (xthis.checked == true) {
        $(".QCOrigen").toggle();
        //$(".QCOrigen").prop('hidden', false);

        $('#txtInspectedByQCO').prop('disabled', false);
        $('#txtInspectedDateQCO').prop('disabled', false);
        $('#txtQCReportQCO').prop('disabled', false);
        $('#txtNotesQCO').prop('disabled', false);

        $('#txtInspectedByQCO2').prop('disabled', false);
        $('#txtInspectedDateQCO2').prop('disabled', false);
        $('#txtQCReportQCO2').prop('disabled', false);
        $('#txtNotesQCO2').prop('disabled', false);

        $('#txtInspectedByQCO3').prop('disabled', false);
        $('#txtInspectedDateQCO3').prop('disabled', false);
        $('#txtQCReportQCO3').prop('disabled', false);
        $('#txtNotesQCO3').prop('disabled', false);

        $('#txtInspectedByQCO4').prop('disabled', false);
        $('#txtInspectedDateQCO4').prop('disabled', false);
        $('#txtQCReportQCO4').prop('disabled', false);
        $('#txtNotesQCO4').prop('disabled', false);
    }
    else {
        //$(".QCOrigen").toggle();
        QuitarCheck(1);
    }
}
function Onchange_checkQCDestination(xthis) {
    if (xthis.checked == true) {
        $(".QCDestination").toggle();
        //$(".QCDestination").prop('hidden', false);

        //$('#QCDestination').prop('hidden', false);
        $('#txtInspectedByQCD').prop('disabled', false);
        $('#txtInspectedDateQCD').prop('disabled', false);
        $('#txtQCReportQCD').prop('disabled', false);
        $('#txtNotesQCD').prop('disabled', false);
    }
    else {

        QuitarCheck(2);
    }
}
function Onchange_checkCustomerDestination(xthis) {
    if (xthis.checked == true) {
        $(".CustomerDestination").toggle();
        //$(".CustomerDestination").prop('hidden', false);
        //$('#CustomerDestination').prop('hidden', false);
        $('#txtInspectedByQCCD').prop('disabled', false);
        $('#txtInspectedDateQCCD').prop('disabled', false);
        $('#txtQCReportQCCD').prop('disabled', false);
        $('#txtNotesQCCD').prop('disabled', false);
    }
    else {

        QuitarCheck(3);
    }
}


$(function () {
    createList(1, 2);

    loadData();
    //CabeceraPackingList();
    loadDefaultValues();

    $(document).on("click", "#btnAdd", function () {
        AddCatVar();

    });
    $(document).on("click", "#btnAddPallets", function () {
        AddPallets();

    });

    $(document).ready(function () {
        $.fn.datepicker.defaults.format = "dd/mm/yyyy";
        var datepicker = $.fn.datepicker.noConflict();
        $.fn.bootstrapDP = datepicker;
    })

    $('#checkQCOrigen').change(function () {
        Onchange_checkQCOrigen(this);
    });

    $('#checkQCDestination').change(function () {
        Onchange_checkQCDestination(this);
    });

    $('#checkCustomerDestination').change(function () {
        Onchange_checkCustomerDestination(this);
    });

    //$(document).on("change", "#checkQCOrigen", function () {
    //    Onchange_checkQCOrigen(this);
    //});

    $(document).on("click", ".row-up", function () {
        var $row = $(this).parents('tr');
        if ($row.index() === 0) return; // Don't go above the header
        $row.prev().before($row.get(0));
    });
    //Btn sube Fila
    $(document).on("click", ".row-down", function () {
        var $row = $(this).parents('tr');
        var verify = $row.next().attr('class');
        console.log('down');
        console.log('down');
        if (verify == 'text-align-left') {
            //if (typeof verify === 'undefined') {
            $row.next().after($row.get(0));
        }
    });

    $(btnConfirm).click(function () {
        PreGuardar();
    })

    $(btnDeclinePO).click(function () {
        saveDecline();
    })


    $(document).on("click", ".row-remove", function () {
        console.log($(this).parents('table').attr('id'));
        $idSaleDetail = $(this).parents('tr').children().eq(1).text();
        console.log($idSaleDetail);
        $idtable = $(this).parents('table').attr('id')
        $(this).parents('tr').detach();
        Calcularsubtotal();
        printSummary();
    });
})

function isNumberKeyKG(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
        return false;
    return true;
}

function loadData() {

    $.ajax({
        type: "GET",
        url: "/PO/ListSize",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataSize = dataJson.map(
                    obj => {
                        return {
                            "id": obj.sizeID,
                            "name": obj.code,
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/PO/ListLabel",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataLabel = dataJson.map(
                    obj => {
                        return {
                            "id": obj.categoryID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });


    $.ajax({
        type: "GET",
        url: "/PO/ListVariety",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataVariety = dataJson.map(
                    obj => {
                        return {
                            "id": obj.varietyID,
                            "name": obj.name,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/PO/ListFarm?idGrower=AGA",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataFarm = dataJson.map(
                    obj => {
                        return {
                            "id": obj.farmID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

}
function loadDefaultValues() {

    //  loadCombo(dataVariety, 'selectVariety', false);
    //  $('#selectVariety').selectpicker('refresh');
    //  loadCombo([], 'selectCode', false);
    //  $('#selectCode').selectpicker('refresh');
    //  loadCombo(dataLabel, 'selectLabel', false);
    //  $('#selectLabel').selectpicker('refresh');
    //loadCombo(dataFarm, 'selectFarm', false);
    //$('#selectFarm').selectpicker('refresh');
}

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='0'>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}



function createHeader(dataHeader) {
    $("#txtPackingList").val(dataHeader[0]['packingList']);
    $(txtPackingListID).val(dataHeader[0]['packingListID'])
    $("#txtOrderProduction").val(dataHeader[0]['orderProduction']);

    $("#txtCustomer").val(dataHeader[0]['customer']);
    $("#txtGrower").val(dataHeader[0]['grower']);
    $("#TotalCabecera").val(dataHeader[0]['boxes']);

    $("#txtVessel").val(dataHeader[0]['vessel']);
    $("#txtContainerNo").val(dataHeader[0]['container']);
    $("#txtVia").val(dataHeader[0]['via']);

    $("#FechaPLD").val(dataHeader[0]['packingLoadDate']);
    $("#FechaETA").val(dataHeader[0]['eta']);
    $("#FechaETD").val(dataHeader[0]['etd']);

    $("#weekEtd").val(dataHeader[0]['weekEtd']);
    $("#txtOriginPort").val(dataHeader[0]['destination']);
    //$("#txtDestinationPort").val(dataHeader[0]['destinationPort']);   

    $(checkQCOrigen).prop('checked', dataHeader[0]['qcOrigin'])//.change();
    $(checkQCDestination).prop('checked', dataHeader[0]['qcDestination'])//.change();
    $(checkCustomerDestination).prop('checked', dataHeader[0]['qcCustomer'])//.change();

    

    $(txtInspectedByQCO).val(dataHeader[0]['qcOriginInspectedBy'])
    $(txtInspectedDateQCO).val(dataHeader[0]['qcOriginInspectedDate'])
    $(txtQCReportQCO).val(dataHeader[0]['qcOriginReport'])
    $(txtNotesQCO).val(dataHeader[0]['qcOriginNotes'])

    $(txtInspectedByQCO2).val(dataHeader[0]['qcOriginInspectedBy2'])
    $(txtInspectedDateQCO2).val(dataHeader[0]['qcOriginInspectedDate2'])
    $(txtQCReportQCO2).val(dataHeader[0]['qcOriginReport2'])
    $(txtNotesQCO2).val(dataHeader[0]['qcOriginNotes2'])

    $(txtInspectedByQCO3).val(dataHeader[0]['qcOriginInspectedBy3'])
    $(txtInspectedDateQCO3).val(dataHeader[0]['qcOriginInspectedDate3'])
    $(txtQCReportQCO3).val(dataHeader[0]['qcOriginReport3'])
    $(txtNotesQCO3).val(dataHeader[0]['qcOriginNotes3'])

    $(txtInspectedByQCO4).val(dataHeader[0]['qcOriginInspectedBy4'])
    $(txtInspectedDateQCO4).val(dataHeader[0]['qcOriginInspectedDate4'])
    $(txtQCReportQCO4).val(dataHeader[0]['qcOriginReport4'])
    $(txtNotesQCO4).val(dataHeader[0]['qcOriginNotes4'])

    $(txtInspectedByQCD).val(dataHeader[0]['qcDestinationInspectedBy'])
    $(txtInspectedDateQCD).val(dataHeader[0]['qcDestinationInspectedDate'])
    $(txtQCReportQCD).val(dataHeader[0]['qcDestinationReport'])
    $(txtNotesQCD).val(dataHeader[0]['qcDestinationNotes'])

    $(txtInspectedByQCCD).val(dataHeader[0]['qcCustomerInspectedBy'])
    $(txtInspectedDateQCCD).val(dataHeader[0]['qcCustomerInspectedDate'])
    $(txtQCReportQCCD).val(dataHeader[0]['qcCustomerReport'])
    $(txtNotesQCCD).val(dataHeader[0]['qcCustomerNotes'])

}

function verifyCheck() {
    if ($('#checkQCOrigen').is(":checked")) {
        $(".QCOrigen").toggle();
        $('#txtInspectedByQCO').prop('disabled', false);
        $('#txtInspectedDateQCO').prop('disabled', false);
        $('#txtQCReportQCO').prop('disabled', false);
        $('#txtNotesQCO').prop('disabled', false);

        $('#txtInspectedByQCO2').prop('disabled', false);
        $('#txtInspectedDateQCO2').prop('disabled', false);
        $('#txtQCReportQCO2').prop('disabled', false);
        $('#txtNotesQCO2').prop('disabled', false);

        $('#txtInspectedByQCO3').prop('disabled', false);
        $('#txtInspectedDateQCO3').prop('disabled', false);
        $('#txtQCReportQCO3').prop('disabled', false);
        $('#txtNotesQCO3').prop('disabled', false);

        $('#txtInspectedByQCO4').prop('disabled', false);
        $('#txtInspectedDateQCO4').prop('disabled', false);
        $('#txtQCReportQCO4').prop('disabled', false);
        $('#txtNotesQCO4').prop('disabled', false);
    }
    if ($('#checkQCDestination').is(":checked")) {
        $(".QCDestination").toggle();
        $('#txtInspectedByQCD').prop('disabled', false);
        $('#txtInspectedDateQCD').prop('disabled', false);
        $('#txtQCReportQCD').prop('disabled', false);
        $('#txtNotesQCD').prop('disabled', false);
    }

    if ($('#checkCustomerDestination').is(":checked")) {
        $(".CustomerDestination").toggle();
        $('#txtInspectedByQCCD').prop('disabled', false);
        $('#txtInspectedDateQCCD').prop('disabled', false);
        $('#txtQCReportQCCD').prop('disabled', false);
        $('#txtNotesQCCD').prop('disabled', false);
    }

}
/*
function CabeceraPackingList() {
    var content = "";
    content += "<table id='tablas1' class='table table-cebra table-responsive table-hover table-bordered '  width='100%'>";
    content += "<thead style='background-color: #13213d' >";
    content += '<tr>';
    content += "<td colspan = '7' style='background-color: #000; text-align: right' class='sticky'> </td>"
    content += '<td style="font-size: 13px;display: none; text-align: center" class="QCOrigen" colspan="6" align="center" bgcolor="666666"><font color="#FFFFFF">Qima Origin</td>';
    content += '<td style="font-size: 13px;display: none;text-align: center" colspan="6" class="QCDestination" align="center" bgcolor="666666"><font color="#FFFFFF">Qima Destination</td>';
    content += '<td style="font-size: 13px;display: none;text-align: center" class="CustomerDestination" colspan="6" align="center" bgcolor="666666"><font color="#FFFFFF">Customer Destination</td>';
    content += '</tr>';
    content += "<tr style='font-size: 13px'>";
    content += "<td style='max-width: 100px;min-width: 100px; border-color:#000; background-color:#000; color:white; ' class='text-white hidden'></td>";
    content += "<td style='max-width: 100px;min-width: 100px; border-color:#000; background-color:#000; color:white; ' class='text-white sticky'>Guardar</td>";
    content += "<td style='max-width: 100px;min-width: 100px;border-color:#000; background-color:#000; color:white;' class='text-white sticky2'>Nº of Pallet</td>";
    content += "<td style='max-width: 150px;min-width: 150px;border-color:#000; background-color:#000; color:white;' class='text-white sticky3' >Label</td>";
    content += "<td style='max-width: 150px;min-width: 150px;border-color:#000; background-color:#000; color:white;' class='text-white sticky4'>Variety</td>";
    content += "<td style='max-width: 150px;min-width: 150px;border-color:#000; background-color:#000; color:white;' class='text-white sticky5'>Size</td>";
    content += "<td style='max-width: 150px;min-width: 150px;border-color:#000; background-color:#000; color:white;' class='text-white sticky6' >Pack</td>";
    content += "<td style='max-width: 100px;min-width: 100px;border-color:#000; background-color:#000; color:white;' class='text-white sticky7' data-toggle='tooltip' data-placement='top' title='Total Boxes'>T. Boxes</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCOrigen'>Accepted";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCOrigen'>Out Specs";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCOrigen'>Rejected";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCOrigen'>QC Score";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCOrigen' data-toggle='tooltip' data-placement='top' title='Adittional Notes'>Adittional N.";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white QCOrigen' data-toggle='tooltip' data-placement='top' title='Total Shipping'>T. Shipping";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white QCDestination'>Accepted";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCDestination'>Out Specs";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white QCDestination' >Rejected";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCDestination'>QC Score";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white QCDestination' data-toggle='tooltip' data-placement='top' title='Adittional Notes'>Adittional N.";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white QCDestination' data-toggle='tooltip' data-placement='top' title='Total Received'>T. Received";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white CustomerDestination'>Accepted";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white CustomerDestination'>Out Specs";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white CustomerDestination' >Rejected";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white CustomerDestination'>QC Score";
    content += "</td>";
    content += "<td  style='max-width:100px; min-width:100px;display: none' class='text-white CustomerDestination' data-toggle='tooltip' data-placement='top' title='Adittional Notes'>Adittional N.";
    content += "</td>";
    content += "<td style='max-width:100px; min-width:100px;display: none' class='text-white CustomerDestination' data-toggle='tooltip' data-placement='top' title='Total Received'>T. Received";
    content += "</td>";
    content += "</tr>";
    content += "</thead><tbody></tbody>";
    content += "<tfoot>"
    content += "<tr>"
    content += "<td colspan = '6' style='background-color: #B6B8C5; text-align: right' class='sticky'> Total</td>"
    content += "<td style='min-width: 80px;max-width: 80px; text-align: right'  class='sticky7' id='TotalAbajo' class=''>";
    content += _Total;
    content += "</td>";
    content += "</tr>"
    content += "</tfoot>"
    content += "</table>"
    document.getElementById("tblPL").innerHTML = content;
}
*/

$(document).on("keyup", ".paleta", function (e) {
    SelectPaleta();
})
$(document).on("keyup", ".size", function (e) {

});



function Calcularsubtotal() {
    $("#TotalAbajo").text(_Total);
}


function printSummary() {
    $("#tablas1>tbody>tr").each(function (iTr, tr) {
        var row = {}
        var VarietyString = '';
        var CodeName = '';
        var VarietyName = '';
        var Weight = '';
        VarietyString = $(tr).find("td.variedad>select option:selected").text();
        CodeName = $(tr).find("td.code>select option:selected").text();
        VarietyName = ((VarietyString.substring(0, 5).replace(/ /g, ""))).toUpperCase();

        $.each(dataCodePack, function (i, e) {
            if (e.name == CodeName) {
                Weight = e.weight;
            }
        })
        var Pack = VarietyName + "_" + Weight + "_" + CodeName;
        $(this).find("td.pack").text(Pack);

    })

    var allrows = []
    $("#tablas1>tbody>tr").each(function (iTr, tr) {
        var row = {}

        var variety = $(tr).find("td.variedad>select option:selected").text();
        var code = $(tr).find("td.code>select option:selected").text();
        var label = $(tr).find("td.label>select option:selected").text();

        row = {
            variety: variety,
            code: code,
            label: label,
            size: 0,
            box: 0
        }

        $.each(dataSize, function (i, e) {
            console.log(e)

            var box = $(tr).find("td." + e.name + ">input").val();

            if (parseInt(box) > 0) {
                row["size"] = e.name;
                row["box"] = box;
                row[e.name] = box;
            }


        })

        allrows.push(row);
    });
    console.log(JSON.stringify(allrows))

    let uniqsVarieties = allrows.map(item => item.variety)
        .filter((value, index, self) => self.indexOf(value) === index);

    let uniqsSizes = allrows.map(item => item.size)
        .filter((value, index, self) => self.indexOf(value) === index);

    let uniqscode = allrows.map(item => item.code)
        .filter((value, index, self) => self.indexOf(value) === index);

    let uniqslabel = allrows.map(item => item.label)
        .filter((value, index, self) => self.indexOf(value) === index);


    var finallist = []
    $.each(uniqscode, function (iCode, code) {
        let rowsByCode = allrows.filter(item => item.code == code)

        $.each(uniqsVarieties, function (iVariety, variety) {
            let rowsByCodebyVariety = rowsByCode.filter(item => item.variety == variety);

            $.each(uniqsSizes, function (iSize, size) {
                let rowsByCodebyVarietybySize = rowsByCodebyVariety.filter(item => item.size == size);

                $.each(uniqslabel, function (iLabel, label) {
                    rowsCVSByLabel = rowsByCodebyVarietybySize.filter(item => item.label == label);

                    var boxes = 0
                    $.each(rowsCVSByLabel, function (i, e) {
                        boxes = boxes + parseInt(e.box);
                    })

                    if (parseInt(boxes) > 0) {
                        finallist.push({ code: code, variety: variety, size: size, label: label, box: boxes });
                    }
                })
            })

        })

    })
    console.log(JSON.stringify(finallist));


    $('#tblSummary>tbody').empty();
    $.each(finallist, function (i, e) {
        var packing = '';

        $.each(dataCodePack, function (codei, codee) {

            if (codee.name == e.code) {
                labelString = (e.label).toString();
                labelName = ((labelString.substring(0, 3).replace(/ /g, ""))).toUpperCase();
                packing = codee.type_pack + '_' + labelName;
                console.log(packing);
            }
        })

        var tr = '<tr>';
        tr += '<td style="font-size: 13px">1</td>'
        tr += '<td style="font-size: 13px">' + e.variety + '</td>'
        tr += '<td style="font-size: 13px">' + packing + '</td>'
        tr += '<td style="font-size: 13px">' + e.size + '</td>'
        tr += '<td style= "text-align: center;font-size: 13px">' + e.box + '</td></tr>'
        console.log(tr);
        $('#tblSummary>tbody').append(tr);
    })
}

function groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}


function getTable1(data) {
    var indice = -1;
    var tr = "";
    console.log(data);

    $('table#tblSummary>tbody>tr').each(function (i, e) {
        var Ssize = $(this).find("td.sizeS").text();
        var Scode = $(this).find("td.codeS").text();
        //console.log("size"+Ssize);
        var Svariety = $(this).find("td.varietyS").text();
        //console.log("variety" + Svariety);
        //console.log("variety2" + data);

        if (Svariety == data.variety && Ssize == data.size && Scode == data.codepack) {
            indice = $(this).index();
        };
    })
    if (indice != -1) {
        var boxes = $("table#tblSummary>tbody>tr").find("td.boxes").text();
        var TotalBoxes = parseInt(boxes) + parseInt(data.value);
        $("table#tblSummary>tbody>tr").find("td.boxes").text(TotalBoxes);
    }
    else {

        tr += "<tr>";
        tr += "<td>" + "1" + "</td>";
        tr += "<td class='varietyS'>" + data.variety + "</td>";
        tr += "<td>" + data.packing + "</td>";
        tr += "<td class='sizeS'>" + data.size + "</td>";
        tr += "<td class='boxes'>" + data.value + "</td>";
        tr += "<td style='display:none' class='codeS'>" + data.codepack + "</td>";
        tr += "</tr>";
    }
    $("#tblSummary>tbody").append(tr);
}

function SummaryContainer() {
    $("#tblSummary>tbody>tr").each(function (e) {

    })
    $("#tablas1>tbody>tr").each(function (itr, tr) {
        console.log(arreglo);
        $(tr).find("td.variedad").each(function (itd, td) {
            //arreglo.Variedad = $("#selectVariety option:selected").text();
            arreglo.push($("#selectVariety option:selected").text());
        })

        for (var i = 0; i < dataSize.length; i++) {
            $(tr).find("td." + dataSize[i]['name']).each(function (itd, td) {
                arreglo.push($(td).text());
            })
        }
    })
}

//function SelectPaleta() {
//    paleta = [];

//    $("#tablas1>tbody>tr>td.paleta").each(function (itr, tr) {
//        var alert = 0;
//        $.each(paleta, function (i, e) {
//            if (e == $(tr).text()) {
//                alert = 1;
//            }
//        })
//        if (alert == 0) {
//            paleta.push($(tr).text());
//            console.log(paleta);
//        }
//    })
//    content = "";
//    content += '<tr >';
//    content += '<td colspan="2" style="min-width: 180px;"><select id="SelectPaletaTabla" class="form-control form-control-sm">';
//    j = 0;
//    $.each(paleta, function (i, s) {
//        content += "<option value='" + j + "' >";
//        content += s;
//        content += "</option>";

//        j = j + 1;
//    })
//    content += '</select></td>';
//    content += '<td  style="max-width: 20px;"> <button id="btnAddPallets" type="button" class="btn  btn-sm  text-white btn-block form-control-sm" style=" background-color: #13213d;">Add  <span class="icon text-white"><i class="fas fa-plus-circle fa-sm"></i></span></button></td></tr>';
//    $("#tblPallets>tbody").empty().append(content);
//}

//function AddPallets() {

//  var selectPaleta = $("#SelectPaletaTabla option:selected").text();

//  $("#tblPallets>tfoot>tr>td.paleta").each(function (itr, tr) {
//    var alert = 0;
//    $.each(paletaTabla, function (i, e) {
//      if (e == selectPaleta) {
//        alert = 1;
//      }
//    })
//    if (alert == 0) {
//      paletaTabla.push(selectPaleta);


//    }
//  })


//  var content2 = '';
//  $.each(paletaTabla, function (i, tr) {

//    content2 += '<tr class="text-align-left">';
//    content2 += '<td style="max-width:2px; text-align: center; font-size: 13px" >&nbsp;<i class="fas fa-arrow-up row-up"></i>&nbsp;<i class="fas fa-arrow-down row-down "></i></td>';
//    content2 += '<td class="paleta" style="font-size: 13px">';
//    content2 += tr;
//    content2 += '</td>';
//    content2 += '<td style="text-align: center;"> <span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td >';
//    content2 += '</tr >';


//  })
//  $("#tblPallets>tfoot").empty().append(content2);

//}



function createList(wiNI, wEn) {

    $("table#tableList tbody").html('');
    $.ajax({
        type: "GET",
        url: "/packinglist/GetAllForQA",
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataListTable = JSON.parse(data);
                $.each(dataJson, function (i, e) {
                    var tr = '';
                    tr += '<tr>';
                    tr += '<td style="display:none">' + e.orderProductionID + '</td>';
                    tr += '<td style="display:none">' + e.packingListID + '</td>';
                    tr += '<td>' + e.orderProduction + '</td>';
                    tr += '<td>' + e.packingList + '</td>';
                    tr += '<td>' + e.grower + '</td>';
                    tr += '<td>' + e.customer + '</td>';
                    //tr += '<td style="display:none">' + e.customer + '</td>';
                    tr += '<td>' + e.boxes + '</td>';
                    tr += '<td>' + e.via + '</td>';
                    tr += '<td>' + e.destination + '</td>';
                    tr += '<td>' + e.withPL + '</td>';
                    tr += '<td >' + e.weekEtd + '</td>';
                    tr += '<td >' + e.etd + '</td>';
                    tr += '<td >' + e.qcOrigin + '</td>';
                    tr += '<td >' + e.qcDestination + '</td>';
                    tr += '<td >' + e.qcCustomer + '</td>';

                    tr += '<td style="text-align: center;"><button class="btn btn-primary btn-sm" onclick="openModal(' + e.orderProductionID + ',' + e.packingListID + ')" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-sm"></i></button></td>';
                    tr += '</tr>';
                    console.log(tr);
                    $('table#tableList tbody').append(tr);


                })


            }


        },
        complete: function () {
            $("table#tableList").DataTable({

                dom: 'Bfrtip',
              buttons: [
                { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                'pageLength'
              ],
                responsive: true,
                lengthMenu: [15, 25, 50],
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //]
            });;
        }
    });

}
function cleanControlsModal() {
    //$('table#tablas>tbody').empty();
    //$('#tablas').DataTable().clear();
    //$('#tablas').DataTable().destroy(); 
    $('table#tablas1>tbody').empty();
    //$('table#tablas1>tfoot').empty();
    //$('#tablas1').DataTable().clear();
    //$('#tablas1').DataTable().destroy(); 
    loadDefaultValues();
    $('table#tblSummary>tbody').empty();
    //$('#tblSummary').DataTable().clear();
    //$('#tblSummary').DataTable().destroy();
    $('table#tblPallets>tbody').empty();
    //$('#tblPallets').DataTable().clear();
    //$('#tblPallets').DataTable().destroy(); 
}
function openModal(orderProductionID, packingListID) {
    dataHeader = [];
    _orderProductionID = orderProductionID;
    _packingListID = packingListID;
    //_Total = 0;

    $("#checkQCOrigen").prop('checked', false);
    $("#checkQCDestination").prop('checked', false)
    $("#checkCustomerDestination").prop('checked', false);
    $(".QCOrigen").css({ 'display': 'none' });
    $(".QCDestination").css({ 'display': 'none' });
    $(".CustomerDestination").css({ 'display': 'none' });


    $('#txtInspectedByQCO').prop('disabled', true);
    $('#txtInspectedDateQCO').prop('disabled', true);
    $('#txtQCReportQCO').prop('disabled', true);
    $('#txtNotesQCO').prop('disabled', true);

    $('#txtInspectedByQCO2').prop('disabled', true);
    $('#txtInspectedDateQCO2').prop('disabled', true);
    $('#txtQCReportQCO2').prop('disabled', true);
    $('#txtNotesQCO2').prop('disabled', true);

    $('#txtInspectedByQCO3').prop('disabled', true);
    $('#txtInspectedDateQCO3').prop('disabled', true);
    $('#txtQCReportQCO3').prop('disabled', true);
    $('#txtNotesQCO3').prop('disabled', true);

    $('#txtInspectedByQCO4').prop('disabled', true);
    $('#txtInspectedDateQCO4').prop('disabled', true);
    $('#txtQCReportQCO4').prop('disabled', true);
    $('#txtNotesQCO4').prop('disabled', true);

    $('#txtInspectedByQCD').prop('disabled', true);
    $('#txtInspectedDateQCD').prop('disabled', true);
    $('#txtQCReportQCD').prop('disabled', true);
    $('#txtNotesQCD').prop('disabled', true);

    $('#txtInspectedByQCCD').prop('disabled', true);
    $('#txtInspectedDateQCCD').prop('disabled', true);
    $('#txtQCReportQCCD').prop('disabled', true);
    $('#txtNotesQCCD').prop('disabled', true);

    $(txtInspectedByQCO).val('')
    $(txtInspectedDateQCO).val('')
    $(txtQCReportQCO).val('')
    $(txtNotesQCO).val('')

    $(txtInspectedByQCD).val('')
    $(txtInspectedDateQCD).val('')
    $(txtQCReportQCD).val('')
    $(txtNotesQCD).val('')

    $(txtInspectedByQCCD).val('')
    $(txtInspectedDateQCCD).val('')
    $(txtQCReportQCCD).val('')
    $(txtNotesQCCD).val('')
    //$(".QCOrigen").prop('hidden', true);
    //$(".QCDestination").prop('hidden', true);
    //$(".CustomerDestination").prop('hidden', true);
    //$('#QCOrigen').prop('hidden', false);

    cleanControlsModal();
    $.ajax({
        type: "GET",
        url: "/PackingList/GeByIDForQA?orderProductionID=" + orderProductionID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataHeader = JSON.parse(data);
            }
        }
    });
    createHeader(dataHeader);

    //loadDataCodePack(viaID);
    //loadCombo(dataCodePack, 'selectCode', false);
    //$('#selectCode').selectpicker('refresh');

    $.ajax({
        type: "GET",
        url: "/PO/ListDetailsAll?packingListID=" + packingListID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataDetails = JSON.parse(data);
                $.each(dataJson, function (i, e) {

                    var tr = '';
                    tr += '<tr style="font-size: 13px" class="text-align-right">';
                    tr += '<td id="packingListDetailID" class="packingListDetailID"  style="min-width: 10px;text-align: left; display: none">' + e.packingListDetailByVarietyID + '</td>';
                    //tr += '<td id="operacionID" class="paleta sticky"  style="min-width: 100px; max-width: 100px; text-align: center"><button type="button" id="btnSaveRow" onclick="btnSaveRow_Click(this)" class=" btn btn-sm btn-success" ><span class="icon text-white"><i class="fas  fa-floppy-o fa-sm"></i></span></button></td>';
                    tr += '<td id="numberPallet" class="paleta sticky"  style="min-width: 120px; max-width: 120px; text-align: left" disable>' + e.numberPallet + '</td>';
                    tr += '<td class="sticky2" style="max-width: 100px; min-width: 100px;text-align: left" id="codepack" readonly>' + e.label + '</td>'
                    tr += '<td class="sticky3" style="max-width: 120px; min-width: 120px;text-align: left" id="codepack" readonly>' + e.variety + '</td>'
                    tr += '<td class="sticky4" style="max-width: 50px; min-width: 50px;text-align: left" id="codepack" readonly>' + e.size + '</td>'
                    tr += '<td class="pack sticky5" style="max-width: 150px; min-width: 150px;text-align: left" id="codepack" readonly>' + e.codepack + '</td>';
                    tr += "<td  id='totalBoxesRow' class='sticky6' style='text-align: right; max-width:60px; min-width: 60px;'>";
                    tr += e.totalBoxes;
                    _Total = e.totalBoxes + _Total;
                    tr += "</td>";
                    tr += "<td id='TShippingQO' style='text-align: right;max-width:100px;min-width: 100px; display: none'  class='QCOrigen TShippingQO'>";
                    tr += "<input id='TShippingQO' type='number' style='width:90%;text-align:right' value=0 min=0></td>";
                    tr += "<td id='AcceptedQO' style='text-align: right;max-width:100px;min-width: 100px; display: none' class='QCOrigen'>";
                    tr += "<input id='AcceptedQO' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id='OutSpecsQO' style='text-align: right;max-width:100px;min-width: 100px; display: none' class='QCOrigen'>";
                    tr += "<input id='OutSpecsQO' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id='RejectedQO' style='text-align: right;max-width:100px;min-width: 100px; display: none' class='QCOrigen'>";
                    tr += "<input id='RejectedQO' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id ='AdittionalNQO' style='text-align: right;max-width:100px;min-width: 100px; display: none' contenteditable='true'  class='QCOrigen'>";
                    tr += "</td>";

                    tr += "<td id='TShippingQD' style='text-align: right;max-width:100px;min-width: 100px;display: none'  class='TShippingQD QCDestination'>";
                    tr += "<input id='TShippingQD' type='number' style='width:90%;text-align:right' value=0 min=0></td>";
                    tr += "<td id='AcceptedQD' style='text-align: right;max-width:100px;min-width: 100px;display: none'   class='QCDestination'>";
                    tr += "<input id='AcceptedQD' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id='OutSpecsQD' style='text-align: right;max-width:100px;min-width: 100px;display: none'   class='QCDestination'>";
                    tr += "<input id='OutSpecsQD' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id='RejectedQD' style='text-align: right;max-width:100px;min-width: 100px;display: none' class='QCDestination'>";
                    tr += "<input id='RejectedQD' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id ='AdittionalNQD' style='text-align: right;max-width:100px;min-width: 100px;display: none' contenteditable='true' class='QCDestination'>";
                    tr += "</td>";

                    tr += "<td id='TShippingQCD'style='text-align: right;max-width:100px;min-width: 100px;display: none' class='CustomerDestination TShippingQCD'>";
                    tr += "<input id='TShippingQCD' type='number' style='width:90%;text-align:right' value=0 min=0></td>";
                    tr += "<td id='AcceptedQCD' style='text-align: right;max-width:100px;min-width: 100px;display: none' class='CustomerDestination'>";
                    tr += "<input id='AcceptedQCD' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id='OutSpecsQCD' style='text-align: right;max-width:100px;min-width: 100px;display: none' class='CustomerDestination'>";
                    tr += "<input id='OutSpecsQCD' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id='RejectedQCD' style='text-align: right;max-width:100px;min-width: 100px;display: none' class='CustomerDestination'>";
                    tr += "<input id='RejectedQCD' type='number' style='width:90%;text-align:right' value=0 min=0 max=100></td>";
                    tr += "<td id ='AdittionalNQCD' style='text-align: right;max-width:100px;min-width: 100px;display: none' contenteditable='true' class='CustomerDestination'>";
                    tr += "</td>";

                    tr += '</tr>';

                    $("#tablas1>tbody").append(tr);
                    Calcularsubtotal();

                })
            }
        },
        complete: function () {
            verifyCheck();
        }
    });



}

$(document).ready(function () {
    $(document).on('focusin', 'input', function () {
        //console.log("Saving value " + $(this).val());
        $(this).data('val', $(this).val());
    }).on('change', 'td.sizeInput>input', function () {

        var td = $(this).parents('td');
        var tr = $(this).parents('tr');



        var colIndex = td.parent().children().index(td);
        //var rowIndex = tr.parent().children().index(tr);
        console.log("COLORED: " + colIndex);
        var size;

        var variety = $(this).parent().parent().find("td.variedad").find("select").find('option:selected').text();
        var codepack = $(this).parent().parent().find("td.code").find("select").find('option:selected').text();
        var type_pack = dataCodePack[$("#selectCode option:selected").val()]['type_pack'];
        var label = $(this).parent().parent().find("td.label").find("select").find('option:selected').text();

        var labelString = label.toString();
        var labelName = ((labelString.substring(0, 3).replace(/ /g, ""))).toUpperCase();
        var packing = type_pack + '_' + labelName;

        console.log(packing);

        $("#tablas1>thead>tr>td").each(function (i, e) {
            console.log("INDICES: " + i + " //" + colIndex)
            if (i == colIndex) {
                size = $(this).text();
            }
        });

        var data = {};
        data.size = size;
        data.value = $(this).val();
        data.variety = variety;
        data.codepack = codepack;
        data.packing = packing;

        if (parseInt($(this).val()) > 0) {
            $(tr).find("td.sizeInput>input").each(function (iInput, input) {
                $(input).prop("disabledd", true);
            })
            $(this).prop("disabledd", false);
        } else {
            $(tr).find("td.sizeInput>input").each(function (iInput, input) {
                $(input).prop("disabledd", false);
            })

        }


        //alert(123)
        //getTable1(data);
        printSummary();
        Calcularsubtotal();
    });
});



function PreGuardar() {
    Swal.fire({
        title: '¿Desea guardar?',
        text: "Los datos serán almacenados permanentemente en el sistema.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, guardar!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            btnSave_Click();
            //btnSaveDetails_Click();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancelado!', 'Se canceló el proceso de guardado.')
        }
    });
}

function validar() {

}

function btnSave_Click() {

    var o = {}
    var packingListID = ""
    var qcOrigin = ""
    var qcOriginInspectedBy = ""
    var qcOriginInspectedDate = ""
    var qcOriginReport = ""
    var qcOriginNotes = ""
    var qcDestination = ""
    var qcDestinationInspectedBy = ""
    var qcDestinationInspectedDate = ""
    var qcDestinationReport = ""
    var qcDestinationNotes = ""
    var qcCustomer = ""
    var qcCustomerInspectedBy = ""
    var qcCustomerInspectedDate = ""
    var qcCustomerReport = ""
    var qcCustomerNotes = ""

    o.packingListID = $(txtPackingListID).val();
    o.qcOrigin = $(checkQCOrigen).is(":checked") ? 1 : 0;
    o.qcOriginInspectedBy = $(txtInspectedByQCO).val();
    o.qcOriginInspectedDate = $(txtInspectedDateQCO).val()
    o.qcOriginReport = $(txtQCReportQCO).val()
    o.qcOriginInspectedBy2 = $(txtInspectedByQCO2).val();
    o.qcOriginInspectedDate2 = $(txtInspectedDateQCO2).val()
    o.qcOriginReport2 = $(txtQCReportQCO2).val()
    o.qcOriginInspectedBy3 = $(txtInspectedByQCO3).val();
    o.qcOriginInspectedDate3 = $(txtInspectedDateQCO3).val()
    o.qcOriginReport3 = $(txtQCReportQCO3).val()
    o.qcOriginNotes3 = $(txtInspectedByQCO3).val();
    o.qcOriginInspectedBy4 = $(txtInspectedByQCO4).val();
    o.qcOriginInspectedDate4 = $(txtInspectedDateQCO4).val()
    o.qcOriginReport4 = $(txtQCReportQCO4).val()
    o.qcOriginNotes4 = $(txtInspectedByQCO4).val();
    o.qcDestination = $(checkQCDestination).is(":checked") ? 1 : 0;
    o.qcDestinationInspectedBy = $(txtInspectedByQCD).val();
    o.qcDestinationInspectedDate = $(txtInspectedDateQCD).val();
    o.qcDestinationReport = $(txtQCReportQCD).val();
    o.qcDestinationNotes = $(txtNotesQCD).val();
    o.qcCustomer = $(checkCustomerDestination).is(":checked") ? 1 : 0;
    o.qcCustomerInspectedBy = $(txtInspectedByQCCD).val();
    o.qcCustomerInspectedDate = $(txtInspectedDateQCCD).val();
    o.qcCustomerReport = $(txtQCReportQCCD).val();
    o.qcCustomerNotes = $(txtNotesQCCD).val();

    var details = []

    $("table#tablas1>tbody>tr").each(function (i, tr) {
        console.log(tr)
        var det = {}
        det.packingListDetailByVarietyID = $(tr).find('td.packingListDetailID').text();

        det.qcOriginQuantityBoxes = $(tr).find('input#TShippingQO').val()
        det.qcOriginAprobe = $(tr).find('input#AcceptedQO').val()
        det.qcOriginOutSpecs = $(tr).find('input#OutSpecsQO').val()
        det.qcOriginRejected = $(tr).find('input#RejectedQO').val()
        det.qcOriginAdditionalNotes = $(tr).find('td#AdittionalNQO').text()

        det.qcDestinationQuantityBoxes = $(tr).find('input#TShippingQD').val()
        det.qcDestinationAprobe = $(tr).find('input#AcceptedQD').val()
        det.qcDestinationOutSpecs = $(tr).find('input#OutSpecsQD').val()
        det.qcDestinationRejected = $(tr).find('input#RejectedQD').val()
        det.qcDestinationAdditionalNotes = $(tr).find('td#AdittionalNQD').text()

        det.qcCustomerQuantityBoxes = $(tr).find('input#TShippingQCD').val()
        det.qcCustomerAprobe = $(tr).find('input#AcceptedQCD').val()
        det.qcCustomerOutSpecs = $(tr).find('input#OutSpecsQCD').val()
        det.qcCustomerRejected = $(tr).find('input#RejectedQCD').val()
        det.qcCustomerAdditionalNotes = $(tr).find('td#AdittionalNQCD').text()

        det.qcPotentialClaim = "";//$(tr).find('td.packingListDetailID').text()
        details.push(det);

    })
    o.packingListDetails = details;

    var api = "/PackingList/SaveQA"
    console.log(api);
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.length > 0) {
                sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
                //loadListPrincipal();
                $('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error", "NO se guardaron los datos!");
                console.log('Ocurrió un error al grabar el registro')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", "NO se guardaron los datos!");
        }
    });

    //console.log(o);

    //if (1 == 1) {
    //  var frmPackingList = new FormData();
    //  frmPackingList.append("packingListID", packingListID);
    //  frmPackingList.append("orderProductionID", orderProductionID);
    //  frmPackingList.append("nroPackingList", nroPackingList);
    //  frmPackingList.append("customerID", customerID);
    //  frmPackingList.append("growerID", growerID);
    //  frmPackingList.append("totalBoxes", totalBoxes);
    //  frmPackingList.append("invoiceGrower", invoiceGrower);
    //  frmPackingList.append("packingLoadDate", packingLoadDate);
    //  frmPackingList.append("nroGuide", nroGuide);
    //  frmPackingList.append("vessel", vessel);
    //  frmPackingList.append("senasaSeal", senasaSeal);
    //  frmPackingList.append("container", container);
    //  frmPackingList.append("customSeal", customSeal);
    //  frmPackingList.append("shippingLine", shippingLine);
    //  frmPackingList.append("thermogRegisters", thermogRegisters);
    //  frmPackingList.append("originPort", originPort);
    //  frmPackingList.append("thermogRegistersLocation", thermogRegistersLocation);
    //  frmPackingList.append("destinationPort", destinationPort);
    //  frmPackingList.append("sensors", sensors);
    //  frmPackingList.append("ETA", ETA);
    //  frmPackingList.append("sensorsLocation", sensorsLocation);
    //  frmPackingList.append("ETD", ETD);
    //  frmPackingList.append("booking", booking);
    //  frmPackingList.append("BLAWB", BLAWB);
    //  frmPackingList.append("viaID", viaID);
    //  console.log(frmPackingList);

    //  var api = "/PO/SavePkPO"
    //  console.log(api);
    //  $.ajax({
    //    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
    //    url: api,
    //    async: false,
    //    contentType: "application/json",
    //    dataType: 'json',
    //    data: JSON.stringify(Object.fromEntries(frmPackingList)),
    //    success: function (datares) {
    //      if (datares.length > 0) {
    //        sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
    //        //loadListPrincipal();
    //        $('#myModal').modal('hide');
    //      } else {
    //        sweet_alert_error("Error", "NO se guardaron los datos!");
    //        console.log('Ocurrió un error al grabar el registro')
    //        console.log(api)
    //      }
    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {
    //      sweet_alert_error("Error", "NO se guardaron los datos!");
    //      console.log(api)
    //      console.log(xhr);
    //      console.log(ajaxOptions);
    //      console.log(thrownError);
    //    }
    //  });

    //}


}

function btnSaveDetails_Click() {


    var allrows = []
    $("#tablas1>tbody>tr").each(function (iTr, tr) {
        var row = {}
        //Preguntar si en el detalle va fundo y la fecha cual es la que se guardaba?
        //var farm = $(tr).find("td.Farm>select option:selected").val();
        //var pallet = $(tr).find("td.paleta").text();
        //var variety = $(tr).find("td.variedad>select option:selected").val();
        //var code = $(tr).find("td.code>select option:selected").val();
        //var label = $(tr).find("td.label>select option:selected").val();
        //var pack = $(tr).find("td.pack").text();
        //var totalboxes = parseInt($(tr).find("td.subtotalRow").text());
        //var description = $(tr).find("td.Description").text();
        //var date1 = $('#FechaTabla').val();



        var packingListDetailID = parseInt(($(tr).find("#packingListDetailID").text()) == "" ? 0 : $(tr).find("#packingListDetailID").text());
        var farm = $(tr).find("#farmID").val();
        var pallet = $(tr).find("#numberPallet").text();
        var variety = $(tr).find("#varietyID").val();
        var code = $(tr).find("#codepackID").val();
        var label = $(tr).find("#labelID").val();
        var pack = $(tr).find("#codepack").text();
        var totalboxes = parseInt($(tr).find("#subtotalRow").text());
        var description = $(tr).find("#commentary").text();
        var date1 = $('#FechaTabla').val();
        var fromdate = date1.split("/");
        var date = fromdate[2] + fromdate[1] + fromdate[0];
        row = {
            packingListDetailID: packingListDetailID,
            farm: farm,
            pallet: pallet,
            variety: variety,
            code: code,
            label: label,
            pack: pack,
            totalboxes: totalboxes,
            description: description,
            date: date,
            size: 0,
            box: 0,
            order: 0
        }

        $.each(dataSize, function (i, e) {
            console.log(e)
            //$(tr).find("td." + dataSize.name).text('aaaaaaaaaaaaaa');
            //$(tr).find("td.20").text('aaaaaaaaaaaaaa');
            var box = $(tr).find("td." + e.name + ">input").val();
            //console.log(box)
            if (parseInt(box) > 0) {
                row["size"] = e.id;
                row["box"] = box;
                row[e.name] = box;
            }
        })

        $('#paleta').each(function (i, e) {
            if (pallet == $(e).text()) {
                row["order"] = parseInt($(e).index());
            }

            //$('tblPallets>tfoot>tr').each(function (i, e) {
            //if (pallet == $(e).$('#paleta').text()) {
            //    row["order"] = (parseInt($(e).index())+1);
            //}

        });
        allrows.push(row);
    });
    console.log(allrows);

    var packingList = false;
    if (1 == 1) {
        var frmPackingList = new FormData();
        $.each(allrows, function (i, e) {
            frmPackingList.append("packingListDetailID", e.packingListDetailID);
            frmPackingList.append("packingListID", _packingListID);
            frmPackingList.append("numberPallet", e.pallet);
            frmPackingList.append("orderPallet", e.order);
            frmPackingList.append("farmID", e.farm);
            frmPackingList.append("codePackID", e.code);
            frmPackingList.append("packageProductID", 1);
            frmPackingList.append("categoryID", e.label);
            frmPackingList.append("varietyID", e.variety);
            frmPackingList.append("sizeID", e.size);
            frmPackingList.append("packingDate", e.date);
            frmPackingList.append("quantityBoxes", e.box);
            frmPackingList.append("description", e.description);
            frmPackingList.append("packingListDetailByVarietyID", 0);


            var api = "/PO/SaveDetailPkPO"
            console.log(api);
            $.ajax({
                type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
                url: api,
                async: false,
                contentType: "application/json",
                dataType: 'json',
                data: JSON.stringify(Object.fromEntries(frmPackingList)),
                success: function (datares) {
                    if (datares.length > 0) {
                        sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
                        //loadListPrincipal();
                        $('#myModal').modal('hide');
                    } else {
                        sweet_alert_error("Error", "NO se guardaron los datos!");
                        console.log('Ocurrió un error al grabar el registro')
                        console.log(api)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    sweet_alert_error("Error", "NO se guardaron los datos!");
                    console.log(api)
                    console.log(xhr);
                    console.log(ajaxOptions);
                    console.log(thrownError);
                }
            });
            //$.ajax({
            //  type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            //  url: api,
            //  async: false,
            //  contentType: "application/json",
            //  dataType: 'json',
            //  data: JSON.stringify(Object.fromEntries(frmPackingList)),
            //  success: function (datares) {
            //    console.log(datares)
            //    if (datares.length > 0) {
            //      packingList = true;
            //    } else {
            //      console.log('Ocurrió un error al grabar la receta')
            //      console.log(datares)
            //    }
            //  },
            //  error: function (jqXHR, textStatus, errorThrown) {
            //    console.log(errorThrown);
            //    console.log(textStatus);
            //    console.log(jqXHR);
            //  }
            //});

        })
    }

    if (packingList) {
        //alert("okok");
        //list(1, 74, 0);
        $('#myModal').modal('hide');
        //cleanControls();
    }



}
