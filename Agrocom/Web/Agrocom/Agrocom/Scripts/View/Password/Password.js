﻿$(function () {
	$.ajax({
		url: "test.html"
	}).fail(function (jqXHR, textStatus, errorThrown) {

	});
	//$(document).ajaxStart(function () {

	//	$("#wait").css("display", "block");
	//	$("#loader").css("display", "block");

	//});
	//$(document).ajaxComplete(function () {
	//	$("#wait").css("display", "none");
	//	$("#loader").css("display", "none");
	//});

	var form = $("#Formulario");
	form.removeClass('was-validated')
});

$('#btnSave').click(function (e) {
	var form = $("#Formulario");
	form.addClass('was-validated');
	if (form[0].checkValidity() === false) {
		event.preventDefault();
		event.stopPropagation();
		sweet_alert_warning("Warning", "You must complete the highlighted fields.");
	} else {
		if ($("#password").val() == $("#confirm").val()) {
			PreGuardar();
		} else {
			sweet_alert_warning('Advertencia', "La contraseña y su confirmación no son iguales.");
		}
	}
});

function PreGuardar() {
	Swal.fire({
		title: '¿Desea guardar?',
		text: "Los datos serán almacenados permanentemente en el sistema.",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Sí, guardar!',
		cancelButtonText: 'No, cancelar!',
		confirmButtonColor: '#4CAA42',
		cancelButtonColor: '#d33'
	}).then((result) => {
		if (result.value) {
			//var index = $(xthis).closest("tr").index();
			save3();//envio la variable
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			sweet_alert_error('Cancelado!', 'Se canceló el proceso de guardado.')
		}
	});
}


function save3() {
	//Recibo variables del FORM Input text

	var firstName = $("#firstName").val();
	var secondName = $("#secondName").val();
	var paternalLastName = $("#paternalLastName").val();
	var maternalLastName = $("#maternalLastName").val();
	var DNI = ($("#DNI").val() == "") ? "0" : $("#DNI").val();
	var phone = $("#phone").val();
	var email = $("#email").val();
	var login = $("#login").val();
	var passwdHash = $("#password").val();



	//


	var sales = false;
	if (1 == 1) {
		//Envio de datos a la api 
		var frmUser = new FormData();
		frmUser.append("login", login);
		frmUser.append("passwdHash", passwdHash);
		frmUser.append("DNI", DNI);
		frmUser.append("firstName", firstName);
		frmUser.append("secondName", secondName);
		frmUser.append("paternalLastName", paternalLastName);
		frmUser.append("maternalLastName", maternalLastName);
		frmUser.append("email", email);
		frmUser.append("phone", phone);


		//var api = "/Password/Save" 
		console.log(api);
		$.ajax({
			type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
			url: api,
			async: false,
			contentType: "application/json",
			dataType: 'json',
			data: JSON.stringify(Object.fromEntries(frmUser)),
			success: function (datares) {
				if (datares.length > 0) {
					sweet_alert_success('Guardado!', 'Los datos han sido guardados.');
					location.reload();
				} else {
					sweet_alert_error("Error", "NO se guardaron los datos!");
					console.log('Ocurrió un error al grabar el Producto')
					console.log(api)
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				sweet_alert_error("Error", "NO se guardaron los datos!");
				console.log(api)
				console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
			}
		});
	}

}

