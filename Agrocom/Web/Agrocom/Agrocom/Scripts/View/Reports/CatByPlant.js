﻿function loadChart() {
    var chartDom = document.getElementById('categorys');
    var myChart = echarts.init(chartDom);
    var option;

    option = {
        legend: {},
        tooltip: {},
        dataset: {
            source: [
                ['product', 'CAT 1', 'CAT P', 'CAT S', 'Local'],
                ['California', 855450, 184695, 566, 0],
                ['Carrizales', 158466, 4320, 0, 13170],
                ['Natalia', 303482, 20568, 10784, 0]
            ]
        },
        xAxis: [
            { type: 'category', gridIndex: 0 },
            { type: 'category', gridIndex: 1 }
        ],
        yAxis: [
            { gridIndex: 0 },
            { gridIndex: 1 }
        ],
        grid: [
            { bottom: '55%' },
            { top: '55%' }
        ],
        series: [
            // These series are in the first grid.
            { type: 'bar', seriesLayoutBy: 'row' },
            { type: 'bar', seriesLayoutBy: 'row' },
            { type: 'bar', seriesLayoutBy: 'row' },
            // These series are in the second grid.
            { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
            { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
            { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
            { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 }
        ]
    };

    option && myChart.setOption(option);
}

window.onload = function () {
    loadChart();
}