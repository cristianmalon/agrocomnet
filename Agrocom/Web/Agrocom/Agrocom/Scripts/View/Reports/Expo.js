﻿var dataClients = []

$(function () {

    //$(document).ajaxStart(function () {
    //    $("#wait").css("display", "block");
    //    //$("#loader").css("display", "block")
    //    $(".transact").attr("disabled", true);
    //});
    //$(document).ajaxComplete(function () {
    //    $("#wait").css("display", "none");
    //    //$("#loader").css("display", "none")
    //    $(".transact").attr("disabled", false);
    //});

    createList();
    createList2();
    loadCombos();

    $("#tablaexpo").DataTable({
        "pageLength": 10,
        "paging": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        //dom: 'Bfrtip',
        //buttons: ['copy', 'excel', 'pdf', 'csv'],
    });

    //$("#tablaexpo2")({
    //    //"pageLength": 10,
    //    //"paging": true,
    //    "ordering": false,
    //    "info": false,
    //    "responsive": true,
    //    //dom: 'Bfrtip',
    //    //buttons: ['copy', 'excel', 'pdf', 'csv'],
    //});

})

$('#btnLoad').click(function (e) {
    save();
});

$('#selectPO').change(function () {
    filterList1();
    filterList2();
})

$('#selectGrower').change(function () {
    filterList1();
    filterList2();
})

$('#selectCustomer').change(function () {
    filterList1();
    filterList2();

})

$('#selectDestination').change(function () {
    filterList1();
    filterList2();
})

function save() {
    var pl = false
    if (1 == 1) {
        console.log('OK')
        api = "/Reports/GetSincroPL";
        console.log(api);
        $.ajax({
            type: "GET",
            url: api,
            //async: false,
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    //console.log(JSON.stringify(dataJson));
                    pl = true;

                    $("table#expo").DataTable().destroy();
                    $("table#expo>tbody").empty();
                    createList()
                } else {
                    console.log('An error occurred while loading the PackingList')
                    console.log(api)
                }
            },
            complete: function () {

            }
        });
    }
    else {
        sweet_alert_error("an equal record already exists");
        return;
    }

    if (pl) {
        alert("CORRECTLY CHARGED!");
    }
}

function filterList1() {
    $('table#expo').DataTable().destroy();
    $('table#expo>tbody').empty();
    console.log(dataClients.filter(item => (item.orderProductionID == $("#selectPO option:selected").val() || $("#selectPO option:selected").val() == "") && (item.growerID == $("#selectGrower option:selected").val() || $("#selectGrower option:selected").val() == "")))
    $.each(dataClients.filter(item => (item.orderProductionID == $("#selectPO option:selected").val() || $("#selectPO option:selected").val() == "") && (item.growerID == $("#selectGrower option:selected").val() || $("#selectGrower option:selected").val() == "") && (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {
        var exists = false;
        var trfind = [];
        
        if (exists == false) {
            var tr = '';
            tr += "<tr>";
            tr += '<td class="sticky" data-name="po" >' + e.po + '</td>';
            tr += '<td class="sticky2" data-name="origin">' + e.origin + '</td>';
            tr += '<td class="sticky3" data-name="grower">' + e.grower + '</td>';
            tr += '<td class="sticky4" data-name="growerCode"></td>';
            tr += '<td class="sticky5" data-name="dc"></td>';
            tr += '<td class="sticky6"  data-name="week">' + e.week + '</td>';
            tr += '<td class="sticky7"  data-name="loadingDate">' + e.loadingDate + '</td>';
            tr += '<td class="sticky8" data-name="time">' + e.time + '</td>';
            tr += '<td class="sticky9" data-name="funmigated"></td>';
            tr += '<td class="sticky10" data-name="templates"></td>';
            tr += '<td class="sticky11" data-name="palletType"></td>';
            tr += '<td class="sticky12" data-name="packingList">' + e.packingList + '</td>';
            tr += '<td class="sticky13" data-name="pallet" >' + e.pallet + '</td>';
            tr += '<td class="sticky14" data-name="productCode">' + e.productCode + '</td>';
            tr += '<td class="sticky15" data-name="brand">' + e.brand + '</td>';
            tr += '<td class="sticky16"  data-name="fruitType">' + e.fruitType + '</td>';
            tr += '<td class="sticky17"  data-name="packingDate">' + e.packingDate + '</td>';
            tr += '<td class="sticky73" data-name="codePack">' + e.codePack + '</td>';
            tr += '<td class="sticky18" data-name="variety">' + e.variety + '</td>';
            tr += '<td class="sticky74" data-name="size">' + e.size + '</td>';
            tr += '<td class="sticky19" data-name="boxes">' + e.boxes + '</td>';
            tr += '<td class="sticky20" data-name="volumeKg" >' + e.volumeKg + '</td>';

            tr += '<td class="sticky21" data-name="qcReport" ></td>';
            tr += '<td class="sticky22" data-name="inspectedBy" ></td>';
            tr += '<td class="sticky23" data-name="sealNumber" ></td>';
            tr += '<td class="sticky24" data-name="inspectedDate" ></td>';
            tr += '<td class="sticky25" data-name="qcScore" ></td>';
            tr += '<td class="sticky26" data-name="potentialClaim" ></td>';
            tr += '<td class="sticky27" data-name="additionalNotes" ></td>';

            tr += '<td class="sticky28" data-name="invoice">' + e.invoice + '</td>';
            tr += '<td class="sticky30" data-name="price"></td>';
            tr += '<td class="sticky31" data-name="FOBPriceperKilo"></td>';

            tr += '<td class="sticky29" data-name="container">' + e.container + '</td>';
            tr += '<td class="sticky32"  data-name="vesselAirline">' + e.vesselAirline + '</td>';
            tr += '<td class="sticky33"  data-name="shippingLine">' + e.shippingLine + '</td>';
            tr += '<td class="sticky34" data-name="blawb">' + e.blawb + '</td>';
            tr += '<td class="sticky35" data-name="weekETD">' + e.weekETD + '</td>';
            tr += '<td class="sticky36" data-name="etd" >' + e.etd + '</td>';
            tr += '<td class="sticky37" data-name="weekETA">' + e.weekETA + '</td>';
            tr += '<td class="sticky38" data-name="eta">' + e.eta + '</td>';
            tr += '<td class="sticky39"  data-name="market">' + e.market + '</td>';
            tr += '<td class="sticky40"  data-name="port">' + e.port + '</td>';
            tr += '<td class="sticky41" data-name="dc2"></td>';
            tr += '<td class="sticky42" data-name="importer">' + e.importer + '</td>';
            tr += '<td class="sticky43" data-name="customer">' + e.customer + '</td>';

            tr += '<td class="sticky44" data-name="weekofArrival"></td>';
            tr += '<td class="sticky45" data-name="arrivalDate"></td>';
            tr += '<td class="sticky46" data-name="dc3"></td>';
            tr += '<td class="sticky47" data-name="inspectedDate"></td>';
            tr += '<td class="sticky48" data-name="inspectedBy"></td>';
            tr += '<td class="sticky49" data-name="sealNumber"></td>';
            tr += '<td class="sticky50" data-name="qcReport3"></td>';

            tr += '<td class="sticky51" data-name="templateRecInfo"></td>';
            tr += '<td class="sticky52" data-name="qcScore"></td>';
            tr += '<td class="sticky53" data-name="potentialClaim"></td>';
            tr += '<td class="sticky54" data-name="toGrower"></td>';
            tr += '<td class="sticky55" data-name="additionalNotes3"></td>';

            tr += '<td class="sticky56" data-name="claim"></td>';
            tr += '<td class="sticky57" data-name="claimDated"></td>';
            tr += '<td class="sticky58" data-name="customerClaim"></td>';
            tr += '<td class="sticky59" data-name="date"></td>';
            tr += '<td class="sticky60" data-name="growerAlert"></td>';
            tr += '<td class="sticky75" data-name="insuranceClaim"></td>';

            tr += '<td class="sticky61" data-name="custmerfinalprice"></td>';

            tr += '<td class="sticky62" data-name="claimCharges"></td>';
            tr += '<td class="sticky63" data-name="customerCommission"></td>';
            tr += '<td class="sticky64" data-name="oZbluMktgCommission"></td>';
            tr += '<td class="sticky65" data-name="promotionFee"></td>';
            tr += '<td class="sticky66" data-name="importServices"></td>';
            tr += '<td class="sticky67" data-name="packing"></td>';
            tr += '<td class="sticky68" data-name="re-packing"></td>';
            tr += '<td class="sticky69" data-name="sorting"></td>';
            tr += '<td class="sticky70" data-name="coldStorage"></td>';
            tr += '<td class="sticky71" data-name="packaging Material"></td>';
            tr += '<td class="sticky72" data-name="freight"></td>';
            tr += '<td class="sticky76" data-name="insuranceFee"></td>';
            tr += '<td class="sticky77" data-name="totalCharges"></td>';
            tr += '<td class="sticky78" data-name="returntotheGrowerFOB"></td>';
            tr += '</tr>';
            //console.log(tr);         

          $("table#expo tbody").append(tr);

        }

    })
    var datafind = []

    $("#expo").dataTable({
        "pageLength": 10,
        "paging": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        //dom: 'Bfrtip',
        //buttons: ['copy', 'excel', 'pdf', 'csv'],
        //"pageLength": 15,
    });
}

function filterList2() {
    //$('table#expo2').DataTable().destroy();
    $('table#expo2>tbody').empty();
    console.log(dataClients.filter(item => (item.orderProductionID == $("#selectPO option:selected").val() || $("#selectPO option:selected").val() == "") && (item.growerID == $("#selectGrower option:selected").val() || $("#selectGrower option:selected").val() == "")))
    $.each(dataClients.filter(item => (item.orderProductionID == $("#selectPO option:selected").val() || $("#selectPO option:selected").val() == "") && (item.growerID == $("#selectGrower option:selected").val() || $("#selectGrower option:selected").val() == "") && (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {
        var exists = false;
        var trfind = [];

        if (exists == false) {
            var tr = '';
            tr += "<tr>";
            tr += '<td class="sticky" data-name="po" >' + e.po + '</td>';
            tr += '<td class="sticky2" data-name="origin">' + e.origin + '</td>';
            tr += '<td class="sticky3" data-name="grower">' + e.grower + '</td>';
            tr += '<td class="sticky4" data-name="growerCode"></td>';
            tr += '<td class="sticky5" data-name="dc"></td>';
            tr += '<td class="sticky6"  data-name="week">' + e.week + '</td>';
            tr += '<td class="sticky7"  data-name="loadingDate">' + e.loadingDate + '</td>';
            tr += '<td class="sticky8" data-name="time">' + e.time + '</td>';
            tr += '<td class="sticky9" data-name="funmigated"></td>';
            tr += '<td class="sticky10" data-name="templates"></td>';
            tr += '<td class="sticky11" data-name="palletType"></td>';
            tr += '<td class="sticky12" data-name="packingList">' + e.packingList + '</td>';
            tr += '<td class="sticky13" data-name="pallet" >' + e.pallet + '</td>';
            tr += '<td class="sticky14" data-name="productCode">' + e.productCode + '</td>';
            tr += '<td class="sticky15" data-name="brand">' + e.brand + '</td>';
            tr += '<td class="sticky16"  data-name="fruitType">' + e.fruitType + '</td>';
            tr += '<td class="sticky17"  data-name="packingDate">' + e.packingDate + '</td>';
            tr += '<td class="sticky73" data-name="codePack">' + e.codePack + '</td>';
            tr += '<td class="sticky18" data-name="variety">' + e.variety + '</td>';
            tr += '<td class="sticky74" data-name="size">' + e.size + '</td>';
            tr += '<td class="sticky19" data-name="boxes">' + e.boxes + '</td>';
            tr += '<td class="sticky20" data-name="volumeKg" >' + e.volumeKg + '</td>';

            tr += '<td class="sticky21" data-name="qcReport" ></td>';
            tr += '<td class="sticky22" data-name="inspectedBy" ></td>';
            tr += '<td class="sticky23" data-name="sealNumber" ></td>';
            tr += '<td class="sticky24" data-name="inspectedDate" ></td>';
            tr += '<td class="sticky25" data-name="qcScore" ></td>';
            tr += '<td class="sticky26" data-name="potentialClaim" ></td>';
            tr += '<td class="sticky27" data-name="additionalNotes" ></td>';

            tr += '<td class="sticky28" data-name="invoice">' + e.invoice + '</td>';
            tr += '<td class="sticky30" data-name="price"></td>';
            tr += '<td class="sticky31" data-name="FOBPriceperKilo"></td>';

            tr += '<td class="sticky29" data-name="container">' + e.container + '</td>';
            tr += '<td class="sticky32"  data-name="vesselAirline">' + e.vesselAirline + '</td>';
            tr += '<td class="sticky33"  data-name="shippingLine">' + e.shippingLine + '</td>';
            tr += '<td class="sticky34" data-name="blawb">' + e.blawb + '</td>';
            tr += '<td class="sticky35" data-name="weekETD">' + e.weekETD + '</td>';
            tr += '<td class="sticky36" data-name="etd" >' + e.etd + '</td>';
            tr += '<td class="sticky37" data-name="weekETA">' + e.weekETA + '</td>';
            tr += '<td class="sticky38" data-name="eta">' + e.eta + '</td>';
            tr += '<td class="sticky39"  data-name="market">' + e.market + '</td>';
            tr += '<td class="sticky40"  data-name="port">' + e.port + '</td>';
            tr += '<td class="sticky41" data-name="dc2"></td>';
            tr += '<td class="sticky42" data-name="importer">' + e.importer + '</td>';
            tr += '<td class="sticky43" data-name="customer">' + e.customer + '</td>';

            tr += '<td class="sticky44" data-name="weekofArrival"></td>';
            tr += '<td class="sticky45" data-name="arrivalDate"></td>';
            tr += '<td class="sticky46" data-name="dc3"></td>';
            tr += '<td class="sticky47" data-name="inspectedDate"></td>';
            tr += '<td class="sticky48" data-name="inspectedBy"></td>';
            tr += '<td class="sticky49" data-name="sealNumber"></td>';
            tr += '<td class="sticky50" data-name="qcReport3"></td>';

            tr += '<td class="sticky51" data-name="templateRecInfo"></td>';
            tr += '<td class="sticky52" data-name="qcScore"></td>';
            tr += '<td class="sticky53" data-name="potentialClaim"></td>';
            tr += '<td class="sticky54" data-name="toGrower"></td>';
            tr += '<td class="sticky55" data-name="additionalNotes3"></td>';

            tr += '<td class="sticky56" data-name="claim"></td>';
            tr += '<td class="sticky57" data-name="claimDated"></td>';
            tr += '<td class="sticky58" data-name="customerClaim"></td>';
            tr += '<td class="sticky59" data-name="date"></td>';
            tr += '<td class="sticky60" data-name="growerAlert"></td>';
            tr += '<td class="sticky75" data-name="insuranceClaim"></td>';

            tr += '<td class="sticky61" data-name="custmerfinalprice"></td>';

            tr += '<td class="sticky62" data-name="claimCharges"></td>';
            tr += '<td class="sticky63" data-name="customerCommission"></td>';
            tr += '<td class="sticky64" data-name="oZbluMktgCommission"></td>';
            tr += '<td class="sticky65" data-name="promotionFee"></td>';
            tr += '<td class="sticky66" data-name="importServices"></td>';
            tr += '<td class="sticky67" data-name="packing"></td>';
            tr += '<td class="sticky68" data-name="re-packing"></td>';
            tr += '<td class="sticky69" data-name="sorting"></td>';
            tr += '<td class="sticky70" data-name="coldStorage"></td>';
            tr += '<td class="sticky71" data-name="packaging Material"></td>';
            tr += '<td class="sticky72" data-name="freight"></td>';
            tr += '<td class="sticky76" data-name="insuranceFee"></td>';
            tr += '<td class="sticky77" data-name="totalCharges"></td>';
            tr += '<td class="sticky78" data-name="returntotheGrowerFOB"></td>';
            tr += '</tr>';
            //console.log(tr);         

            $("table#expo2 tbody").append(tr);

        }

    })
    var datafind = []

    //$("#expo2").dataTable({
    //    //"pageLength": 10,
    //    //"paging": true,
    //    "ordering": false,
    //    "info": false,
    //    "responsive": true,
    //});
}

function loadCombos() {
    $.ajax({
        type: "GET",
        url: "/Reports/expoListByPO",
        //async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.orderProductionID,
                            "name": obj.po
                        }
                    }
                );
                loadCombo(dataSelect, 'selectPO', true, 'Choose');
                $('#selectPO').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/expoListByGrower",
        //async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.growerID,
                            "name": obj.grower
                        }
                    }
                );
                loadCombo(dataSelect, 'selectGrower', true, 'Choose');
                $('#selectGrower').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/expoListByCustomer",
        //async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.customerID,
                            "name": obj.customer
                        }
                    }
                );
                loadCombo(dataSelect, 'selectCustomer', true, 'Choose');
                $('#selectCustomer').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });


    $.ajax({
        type: "GET",
        url: "/Reports/expoListByDestination",
        //async: false,
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationID,
                            "name": obj.port
                        }
                    }
                );
                loadCombo(dataSelect, 'selectDestination', true, 'Choose');
                $('#selectDestination').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });

}

function loadCombo(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function createList(wiNI, wEn) {



    dataClients = []
    $.ajax({
        type: "GET",
        url: "expoList",
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients = dataJson;
                //console.log(JSON.stringify(dataClients));

                $.each(dataClients, function (i, e) {

                    var exists = false;
                    var trfind = [];
                    
                    if (exists == false) {
                        var tr = '';
                  
                      tr += "<tr>";
                      tr += '<td class="sticky" data-name="po" >' + e.po + '</td>';
                      tr += '<td class="sticky2" data-name="origin">' + e.origin + '</td>';
                      tr += '<td class="sticky3" data-name="grower">' + e.grower + '</td>';
                      tr += '<td class="sticky4" data-name="growerCode"></td>';
                      tr += '<td class="sticky5" data-name="dc"></td>';
                      tr += '<td class="sticky6"  data-name="week">' + e.week + '</td>';
                      tr += '<td class="sticky7"  data-name="loadingDate">' + e.loadingDate + '</td>';
                      tr += '<td class="sticky8" data-name="time">' + e.time + '</td>';
                      tr += '<td class="sticky9" data-name="funmigated"></td>';
                      tr += '<td class="sticky10" data-name="templates"></td>';
                      tr += '<td class="sticky11" data-name="palletType"></td>';
                      tr += '<td class="sticky12" data-name="packingList">' + e.packingList + '</td>';
                      tr += '<td class="sticky13" data-name="pallet" >' + e.pallet + '</td>';
                      tr += '<td class="sticky14" data-name="productCode">' + e.productCode + '</td>';
                      tr += '<td class="sticky15" data-name="brand">' + e.brand + '</td>';
                      tr += '<td class="sticky16"  data-name="fruitType">' + e.fruitType + '</td>';
                      tr += '<td class="sticky17"  data-name="packingDate">' + e.packingDate + '</td>';
                      tr += '<td class="sticky73" data-name="codePack">' + e.codePack + '</td>';
                      tr += '<td class="sticky18" data-name="variety">' + e.variety + '</td>';
                      tr += '<td class="sticky74" data-name="size">' + e.size + '</td>';
                      tr += '<td class="sticky19" data-name="boxes">' + e.boxes + '</td>';
                      tr += '<td class="sticky20" data-name="volumeKg" >' + e.volumeKg + '</td>';

                      tr += '<td class="sticky21" data-name="qcReport" ></td>';
                      tr += '<td class="sticky22" data-name="inspectedBy" ></td>';
                      tr += '<td class="sticky23" data-name="sealNumber" ></td>';
                      tr += '<td class="sticky24" data-name="inspectedDate" ></td>';
                      tr += '<td class="sticky25" data-name="qcScore" ></td>';
                      tr += '<td class="sticky26" data-name="potentialClaim" ></td>';
                      tr += '<td class="sticky27" data-name="additionalNotes" ></td>';

                      tr += '<td class="sticky28" data-name="invoice">' + e.invoice + '</td>';                      
                      tr += '<td class="sticky30" data-name="price"></td>';
                      tr += '<td class="sticky31" data-name="FOBPriceperKilo"></td>';

                      tr += '<td class="sticky29" data-name="container">' + e.container + '</td>';
                      tr += '<td class="sticky32"  data-name="vesselAirline">' + e.vesselAirline + '</td>';
                      tr += '<td class="sticky33"  data-name="shippingLine">' + e.shippingLine + '</td>';
                      tr += '<td class="sticky34" data-name="blawb">' + e.blawb + '</td>';
                      tr += '<td class="sticky35" data-name="weekETD">' + e.weekETD + '</td>';
                      tr += '<td class="sticky36" data-name="etd" >' + e.etd + '</td>';
                      tr += '<td class="sticky37" data-name="weekETA">' + e.weekETA + '</td>';
                      tr += '<td class="sticky38" data-name="eta">' + e.eta + '</td>';
                      tr += '<td class="sticky39"  data-name="market">' + e.market + '</td>';
                      tr += '<td class="sticky40"  data-name="port">' + e.port + '</td>';
                      tr += '<td class="sticky41" data-name="dc2"></td>';
                      tr += '<td class="sticky42" data-name="importer">' + e.importer + '</td>';    
                      tr += '<td class="sticky43" data-name="customer">' + e.customer + '</td>';

                      tr += '<td class="sticky44" data-name="weekofArrival"></td>';
                      tr += '<td class="sticky45" data-name="arrivalDate"></td>';
                      tr += '<td class="sticky46" data-name="dc3"></td>';
                      tr += '<td class="sticky47" data-name="inspectedDate"></td>';
                      tr += '<td class="sticky48" data-name="inspectedBy"></td>';
                      tr += '<td class="sticky49" data-name="sealNumber"></td>';
                      tr += '<td class="sticky50" data-name="qcReport3"></td>';

                      tr += '<td class="sticky51" data-name="templateRecInfo"></td>';
                      tr += '<td class="sticky52" data-name="qcScore"></td>';
                      tr += '<td class="sticky53" data-name="potentialClaim"></td>';
                      tr += '<td class="sticky54" data-name="toGrower"></td>';
                      tr += '<td class="sticky55" data-name="additionalNotes3"></td>';

                      tr += '<td class="sticky56" data-name="claim"></td>';
                      tr += '<td class="sticky57" data-name="claimDated"></td>';
                      tr += '<td class="sticky58" data-name="customerClaim"></td>';
                      tr += '<td class="sticky59" data-name="date"></td>';
                      tr += '<td class="sticky60" data-name="growerAlert"></td>';
                      tr += '<td class="sticky75" data-name="insuranceClaim"></td>';

                      tr += '<td class="sticky61" data-name="custmerfinalprice"></td>';

                      tr += '<td class="sticky62" data-name="claimCharges"></td>';
                      tr += '<td class="sticky63" data-name="customerCommission"></td>';
                      tr += '<td class="sticky64" data-name="oZbluMktgCommission"></td>';
                      tr += '<td class="sticky65" data-name="promotionFee"></td>';
                      tr += '<td class="sticky66" data-name="importServices"></td>';
                      tr += '<td class="sticky67" data-name="packing"></td>';
                      tr += '<td class="sticky68" data-name="re-packing"></td>';
                      tr += '<td class="sticky69" data-name="sorting"></td>';
                      tr += '<td class="sticky70" data-name="coldStorage"></td>';
                      tr += '<td class="sticky71" data-name="packaging Material"></td>';
                      tr += '<td class="sticky72" data-name="freight"></td>';
                      tr += '<td class="sticky76" data-name="insuranceFee"></td>';
                      tr += '<td class="sticky77" data-name="totalCharges"></td>';
                      tr += '<td class="sticky78" data-name="returntotheGrowerFOB"></td>';
                      tr += '</tr>';
                   
                        //console.log(tr);

                        $("table#expo tbody").append(tr);

                    }
                })
            }            
        },

        complete: function () {
            $("#expo").dataTable({
                "pageLength": 10,
                "paging": true,
                "ordering": false,
                "info": false,
                "responsive": true,
                //dom: 'Bfrtip',
               //buttons: ['copy', 'excel', 'pdf', 'csv'],
              //  [{
              //  extend: 'excelHtml5',
              //  customize: function (xlsx) {
              //    var sheet = xlsx.xl.worksheets['sheet1.xml'];

              //    // Loop over the cells in column `C`
              //    $('row c[r^="B"]', sheet).each(function () {
              //      // Get the value
              //      if ($('is t', this).text() == 'Origin') {
                
              //        $(this).attr('s', '15');
              //      }
              //    });
              //  }
              //}],

                //"pageLength": 15,
            });
        }
    });

    var datafind = []

}

function createList2(wiNI, wEn) {
    dataClients = []
    $.ajax({
        type: "GET",
        url: "expoList",
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients = dataJson;
                //console.log(JSON.stringify(dataClients));

                $.each(dataClients, function (i, e) {

                    var exists = false;
                    var trfind = [];

                    if (exists == false) {
                        var tr = '';

                        tr += "<tr>";
                        tr += '<td class="sticky" data-name="po" >' + e.po + '</td>';
                        tr += '<td class="sticky2" data-name="origin">' + e.origin + '</td>';
                        tr += '<td class="sticky3" data-name="grower">' + e.grower + '</td>';
                        tr += '<td class="sticky4" data-name="growerCode"></td>';
                        tr += '<td class="sticky5" data-name="dc"></td>';
                        tr += '<td class="sticky6"  data-name="week">' + e.week + '</td>';
                        tr += '<td class="sticky7"  data-name="loadingDate">' + e.loadingDate + '</td>';
                        tr += '<td class="sticky8" data-name="time">' + e.time + '</td>';
                        tr += '<td class="sticky9" data-name="funmigated"></td>';
                        tr += '<td class="sticky10" data-name="templates"></td>';
                        tr += '<td class="sticky11" data-name="palletType"></td>';
                        tr += '<td class="sticky12" data-name="packingList">' + e.packingList + '</td>';
                        tr += '<td class="sticky13" data-name="pallet" >' + e.pallet + '</td>';
                        tr += '<td class="sticky14" data-name="productCode">' + e.productCode + '</td>';
                        tr += '<td class="sticky15" data-name="brand">' + e.brand + '</td>';
                        tr += '<td class="sticky16"  data-name="fruitType">' + e.fruitType + '</td>';
                        tr += '<td class="sticky17"  data-name="packingDate">' + e.packingDate + '</td>';
                        tr += '<td class="sticky73" data-name="codePack">' + e.codePack + '</td>';
                        tr += '<td class="sticky18" data-name="variety">' + e.variety + '</td>';
                        tr += '<td class="sticky74" data-name="size">' + e.size + '</td>';
                        tr += '<td class="sticky19" data-name="boxes">' + e.boxes + '</td>';
                        tr += '<td class="sticky20" data-name="volumeKg" >' + e.volumeKg + '</td>';

                        tr += '<td class="sticky21" data-name="qcReport" ></td>';
                        tr += '<td class="sticky22" data-name="inspectedBy" ></td>';
                        tr += '<td class="sticky23" data-name="sealNumber" ></td>';
                        tr += '<td class="sticky24" data-name="inspectedDate" ></td>';
                        tr += '<td class="sticky25" data-name="qcScore" ></td>';
                        tr += '<td class="sticky26" data-name="potentialClaim" ></td>';
                        tr += '<td class="sticky27" data-name="additionalNotes" ></td>';

                        tr += '<td class="sticky28" data-name="invoice">' + e.invoice + '</td>';
                        tr += '<td class="sticky30" data-name="price"></td>';
                        tr += '<td class="sticky31" data-name="FOBPriceperKilo"></td>';

                        tr += '<td class="sticky29" data-name="container">' + e.container + '</td>';
                        tr += '<td class="sticky32"  data-name="vesselAirline">' + e.vesselAirline + '</td>';
                        tr += '<td class="sticky33"  data-name="shippingLine">' + e.shippingLine + '</td>';
                        tr += '<td class="sticky34" data-name="blawb">' + e.blawb + '</td>';
                        tr += '<td class="sticky35" data-name="weekETD">' + e.weekETD + '</td>';
                        tr += '<td class="sticky36" data-name="etd" >' + e.etd + '</td>';
                        tr += '<td class="sticky37" data-name="weekETA">' + e.weekETA + '</td>';
                        tr += '<td class="sticky38" data-name="eta">' + e.eta + '</td>';
                        tr += '<td class="sticky39"  data-name="market">' + e.market + '</td>';
                        tr += '<td class="sticky40"  data-name="port">' + e.port + '</td>';
                        tr += '<td class="sticky41" data-name="dc2"></td>';
                        tr += '<td class="sticky42" data-name="importer" style="max-width: 200px; min-width:200px; font-size: "><textarea style="font-size:13px" class="form-control form-control-sm" id="paymentTerm" rows="1">' + e.importer + '</textarea></td>';
                        tr += '<td class="sticky43" data-name="customer" style="max-width: 200px; min-width:200px; font-size: "><textarea style="font-size:13px" class="form-control form-control-sm" id="paymentTerm" rows="1">' + e.customer + '</textarea></td>';

                        tr += '<td class="sticky44" data-name="weekofArrival"></td>';
                        tr += '<td class="sticky45" data-name="arrivalDate"></td>';
                        tr += '<td class="sticky46" data-name="dc3"></td>';
                        tr += '<td class="sticky47" data-name="inspectedDate"></td>';
                        tr += '<td class="sticky48" data-name="inspectedBy"></td>';
                        tr += '<td class="sticky49" data-name="sealNumber"></td>';
                        tr += '<td class="sticky50" data-name="qcReport3"></td>';

                        tr += '<td class="sticky51" data-name="templateRecInfo"></td>';
                        tr += '<td class="sticky52" data-name="qcScore"></td>';
                        tr += '<td class="sticky53" data-name="potentialClaim"></td>';
                        tr += '<td class="sticky54" data-name="toGrower"></td>';
                        tr += '<td class="sticky55" data-name="additionalNotes3"></td>';

                        tr += '<td class="sticky56" data-name="claim"></td>';
                        tr += '<td class="sticky57" data-name="claimDated"></td>';
                        tr += '<td class="sticky58" data-name="customerClaim"></td>';
                        tr += '<td class="sticky59" data-name="date"></td>';
                        tr += '<td class="sticky60" data-name="growerAlert"></td>';
                        tr += '<td class="sticky75" data-name="insuranceClaim"></td>';

                        tr += '<td class="sticky61" data-name="custmerfinalprice"></td>';

                        tr += '<td class="sticky62" data-name="claimCharges"></td>';
                        tr += '<td class="sticky63" data-name="customerCommission"></td>';
                        tr += '<td class="sticky64" data-name="oZbluMktgCommission"></td>';
                        tr += '<td class="sticky65" data-name="promotionFee"></td>';
                        tr += '<td class="sticky66" data-name="importServices"></td>';
                        tr += '<td class="sticky67" data-name="packing"></td>';
                        tr += '<td class="sticky68" data-name="re-packing"></td>';
                        tr += '<td class="sticky69" data-name="sorting"></td>';
                        tr += '<td class="sticky70" data-name="coldStorage"></td>';
                        tr += '<td class="sticky71" data-name="packaging Material"></td>';
                        tr += '<td class="sticky72" data-name="freight"></td>';
                        tr += '<td class="sticky76" data-name="insuranceFee"></td>';
                        tr += '<td class="sticky77" data-name="totalCharges"></td>';
                        tr += '<td class="sticky78" data-name="returntotheGrowerFOB"></td>';
                        tr += '</tr>';

                        //console.log(tr);

                        $("table#expo2 tbody").append(tr);

                    }
                })
            }
        },

        complete: function () {
            //$("#expo2").dataTable({
            //    //"pageLength": 10,
            //    //"paging": true,
            //    "ordering": false,
            //    "info": false,
            //    "responsive": true,

            //});
        }
    });

    var datafind = []

}

function fnExcelReport() {
  var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
  tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

  tab_text = tab_text + '<x:Name>Expo Master</x:Name>';

  tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
  tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

  tab_text = tab_text + "<table border='1px'>";
  tab_text = tab_text + $('#expo2').html();
  tab_text = tab_text + '</table></body></html>';

  var data_type = 'data:application/vnd.ms-excel';

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    if (window.navigator.msSaveBlob) {
      var blob = new Blob([tab_text], {
        type: "application/csv;charset=utf-8;"
      });
      navigator.msSaveBlob(blob, 'ExpoMaster.xls');
    }
  } else {
    $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
      $('#test').attr('download', 'ExpoMaster.xls');
  }

}

