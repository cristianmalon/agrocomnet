﻿var json1
var json2
var json3

function loadFirstForecast() {

    $.ajax({
        type: "GET",
        url: "/Forecast/GetFirstForecast",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            console.log(dataJson);
            json1 = {
                type: "column",
                name: "Forecast May",
                showInLegend: true,
                yValueFormatString: "$#,##0",
                dataPoints: dataJson
            }

            loadCurrentlyForecast();
        }
    });
}

function loadCurrentlyForecast() {

    $.ajax({
        type: "GET",
        url: "/Forecast/GetCurrentlyForecast",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            console.log(dataJson);
            json2 = {
                type: "line",
                name: "Forecast Actual",
                showInLegend: true,
                yValueFormatString: "$#,##0",
                dataPoints: dataJson
            }
            loadRealForecast()
        }
    });
}

function loadRealForecast() {

    $.ajax({
        type: "GET",
        url: "/Forecast/GetRealForecast",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            console.log(dataJson);
            json3 = {
                type: "area",
                name: "Forecast Real",
                markerBorderColor: "white",
                markerBorderThickness: 2,
                showInLegend: true,
                yValueFormatString: "$#,##0",
                dataPoints: dataJson
            }

            loadCanvas()
        }
    });
}

function loadCanvas() {
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Last Update Forecast vs Real Exported"
        },
        //axisX: {
        //	valueFormatString: "MMM"
        //},
        axisY: {
            suffix: " Tn",
            labelFormatter: addSymbols
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },

        data: [json1, json2, json3]
    });
    chart.render();
}

function addSymbols(e) {
    var suffixes = ["", "K", "M", "Tn"];
    var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);

    if (order > suffixes.length - 1)
        order = suffixes.length - 1;

    var suffix = suffixes[order];
    return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    } else {
        e.dataSeries.visible = true;
    }
    e.chart.render();
}

window.onload = function () {
    loadFirstForecast();

}