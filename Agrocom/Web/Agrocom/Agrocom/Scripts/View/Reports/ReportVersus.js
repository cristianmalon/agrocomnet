﻿var json1
var json2
var json3

function loadForecast() {

    $.ajax({
        type: "GET",
        url: "/Reports/GetreportVsFor",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            json1 = {
                type: "column",
                name: "Harvest Forecast",
                showInLegend: true,
                yValueFormatString: "Tn#,##0",
                dataPoints: dataJson
            }

            loadSales();
        }
    });
}

function loadSales() {

    $.ajax({
        type: "GET",
        url: "/Reports/GetreportVsSR",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            json2 = {
                type: "column",
                name: "Sales Request",
                showInLegend: true,
                yValueFormatString: "Tn#,##0",
                dataPoints: dataJson
            }
            loadPO()
        }
    });
}

function loadPO() {

    $.ajax({
        type: "GET",
        url: "/Reports/GetreportVsPO",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            json3 = {
                type: "column",
                name: "Production Orders",
                markerBorderColor: "white",
                markerBorderThickness: 2,
                showInLegend: true,
                yValueFormatString: "Tn#,##0",
                dataPoints: dataJson
            }

            loadCanvas()
        }
    });
}

function loadCanvas() {
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Report Versus PO vs SR vs FOR"
        },
        axisY: {
            title: "quantity (KG)"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [json1, json2, json3]
    });
    chart.render();
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

window.onload = function () {
    loadForecast();
}

