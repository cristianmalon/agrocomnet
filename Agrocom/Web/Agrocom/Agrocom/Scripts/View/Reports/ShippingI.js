﻿var dataClients = []
var collapsedGroups = {};
var table;
$('#shipping tbody').on('click', 'tr.dtrg-start', function () {

  var name = $(this).data('name');
  collapsedGroups[name] = !collapsedGroups[name];
  table.draw(false);
});
$(function () {

    createList();
    loadCombos();

    $("#tablashipping").DataTable({
        "paging": true,
        "ordering": false,
        "info": true,
        "responsive": true,
        //"destroy": true,
        dom: 'Bfrtip',
        //select: true,
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'All']
        ],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });


    sweet_alert_progressbar_cerrar();
})
$(document).on("click", ".btnSave", function (e) {
    var nroPacking = $(this).closest('tr').find('td.packingListNumber').text()
    createdSalesA(nroPacking, $(this).closest('tr'));
});

$('#selectPO').change(function () {
    filterList1();
})

$('#selectPL').change(function () {
    filterList1();
})

$('#selectCustomer').change(function () {
    filterList1();
})

$('#selectLogistic').change(function () {
    filterList1();
})

$('#selectManager').change(function () { 
    filterList1();
})

$('#selectWeek').change(function () {
    filterList1();
})

$('#selectDestination').change(function () {
    filterList1();
})
$(document).on("click", "#btnSearch", function () {
  filterList1();
})
function createdSalesA(nroPackingList, tr) {
    $.ajax({
        type: 'POST', 
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: "/Reports/SaveSAllocated?nroPackingList=" + nroPackingList,
        //async: false,
        success: function (data) {
            if (data.length > 0) {

                var po = JSON.parse(data)[0].po;
                $(tr).find('td:eq(0)').text(po)

            }
        }
    });
}

function filterList1() {
    sweet_alert_progressbar();
    $('table#shipping').DataTable().destroy();
    $('table#shipping>tbody').empty();
    var colorFila = "";
    var paso = 1;
    var packingListNumberAnterior = "";
    $.each(dataClients.filter(item => (item.orderProductionID == $("#selectPO option:selected").val() || $("#selectPO option:selected").val() == "") && (item.nroPackingList == $("#selectPL option:selected").val() || $("#selectPL option:selected").val() == "") && (item.finalCustomerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.logisticOperatorID == $("#selectLogistic option:selected").val() || $("#selectLogistic option:selected").val() == "") && (item.managerID == $("#selectManager option:selected").val() || $("#selectManager option:selected").val() == "") && (item.projectedWeekID == $("#selectWeek option:selected").val() || $("#selectWeek option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {
        var exists = false;
        var trfind = [];

        if (exists == false) {
            var tr = '';
            if (i > 0) {
                if (packingListNumberAnterior == (e.nroPackingList).trim()) {
                    tr += "<tr style='font-size:12px!important; background-color:transparent!important  '>";
                } else {
                    if (paso == 1) {
                        tr += "<tr  style='background-color: transparent!important;font-size:12px!important'>";
                      colorFila = "transparent;";
                        paso = 0;
                    } else {
                        tr += "<tr  style='background-color: transparent!important;font-size:12px!important'>";
                      colorFila = "transparent;";
                        paso = 1
                    }
                }

                packingListNumberAnterior = (e.nroPackingList).trim();

            } else {
                packingListNumberAnterior = e.nroPackingList.trim();
                tr += "<tr style='background-color: transparent!important;font-size:13px!important'>";
              colorFila = "transparent;";
            }
            if (e.orderProduction == "") {
                tr += '<td class="sticky productionOrderNumber" style="background-color:white!important ">';
                tr += '<button class="btn btn-success btnSave btn-sm " style="border:none;outline:none!important"   data-toggle="tooltip" data-placement="top" title="Load S. Allocation">Load S. Allocation <i class="fas fa-save fa-sm text-white"></i></button>'
                tr += '</td>';
            } else {
                tr += '<td class="sticky productionOrderNumber" style="background-color: white!important ">';
                tr += e.orderProduction;
                tr += '</td>';
            }

            tr += '<td class="sticky1" style="background-color: white!important;max-width:50px; min-width:50px" data-name="week">' + e.week + '</td>';
            tr += '<td class="sticky2" style="background-color: white!important;max-width:150px; min-width:150px" data-name="nroPackingList">' + e.nroPackingList + '</td>';
            tr += '<td style="max-width:120px; min-width:120px" data-name="processingPlant"><textarea style="font-size:13px" class="form-control form-control-sm" id="processingPlant" rows="1" readonly>' + e.processingPlant + '</textarea></td>';
            tr += '<td style="background-color: white!important;max-width:80px; min-width:80px" data-name="loadingDay">' + e.loadingDay + '</td>';
            tr += '<td style="background-color: white!important;max-width:80px; min-width:80px" data-name="uploadDate">' + e.uploadDate + '</td>';
            tr += '<td style="max-width:100px; min-width:100px" data-name="chargingTime" >' + e.chargingTime + '</td>';
            tr += '<td style="max-width:200px; min-width:200px" data-name="logisticOperator"><textarea style="font-size:13px" class="form-control form-control-sm" id="logisticOperator" rows="1" readonly>' + e.logisticOperator + '</textarea></td>';
            tr += '<td style="max-width:150px; min-width:150px" data-name="shippingCompany">' + e.shippingCompany + '</td>';
            tr += '<td style="max-width:30px; min-width:30px" data-name="tt">' + e.tt + '</td>';
            tr += '<td style="max-width:170px; min-width:170px" data-name="vessel"><textarea style="font-size:13px" class="form-control form-control-sm" id="vessel" rows="1" readonly>' + e.vessel + '</textarea></td>';
            tr += '<td style="max-width:80px; min-width:80px" data-name="etd">' + e.etd + '</td>';
            tr += '<td style="max-width:80px; min-width:80px" data-name="eta">' + e.eta + '</td>';
            tr += '<td style="max-width:80px; min-width:80px" data-name="etaReal" >' + e.etaReal + '</td>';
            tr += '<td style="max-width:100px; min-width:100px" data-name="booking">' + e.booking + '</td>';
            tr += '<td style="max-width:200px; min-width:200px" data-name="customer"><textarea style="font-size:13px" class="form-control form-control-sm" id="customer" rows="1" readonly>' + e.customer + '</textarea></td>';
            tr += '<td style="max-width:200px; min-width:200px" data-name="finalCustomer"><textarea style="font-size:13px" class="form-control form-control-sm" id="finalCustomer" rows="1" readonly>' + e.finalCustomer + '</textarea></td>';
            tr += '<td style="max-width:80px; min-width:80px" data-name="originPort">' + e.originPort + '</td>';
            tr += '<td style="max-width:150px; min-width:150px" data-name="destinationPort"><textarea style="font-size:13px" class="form-control form-control-sm" id="logisticOperator" rows="1" readonly>' + e.destinationPort + '</textarea></td>';
            tr += '<td style="max-width:120px; min-width:120px" data-name="manager">' + e.manager + '</td>';
            tr += '<td style="max-width:80px; min-width:80px" data-name="brand">' + e.brand + '</td>';
            tr += '<td style="max-width:60px; min-width:60px" data-name="category" >' + e.category + '</td>';
            tr += '<td style="max-width:180px; min-width:180px" data-name="package">' + e.package + '</td>';
            tr += '<td style="max-width:50px; min-width:50px" data-name="color">' + e.color + '</td>';
            tr += '<td style="max-width:50px; min-width:50px" data-name="weight">' + e.weight + '</td>';
            tr += '<td style="max-width:110px; min-width:110px" data-name="variety"><textarea style="font-size:13px" class="form-control form-control-sm" id="variety" rows="1" readonly>' + e.variety + '</textarea></td>';
            tr += '<td style="max-width:50px; min-width:50px" data-name="quantity">' + e.quantity + '</td>';
            tr += '<td style="max-width:100px; min-width:100px" data-name="container">' + e.container + '</td>';
            tr += '<td style="max-width:150px; min-width:150px" data-name="bl" ><textarea style="font-size:13px" class="form-control form-control-sm" id="bl" rows="1" readonly>' + e.bl + '</textarea></td>';
            tr += '</tr>';
            $("table#shipping tbody").append(tr);

        }

    })
    table= $("#shipping").DataTable({
        rowGroup: {
      // Uses the 'row group' plugin
      dataSrc: 0,
      startRender: function (rows, group) {
        var collapsed = !!collapsedGroups[group];

        rows.nodes().each(function (r) {
          r.style.display = collapsed ? 'none' : '';
        });

        // Add category name to the <tr>. NOTE: Hardcoded colspan
        return $('<tr/>')
          .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
          .attr('data-name', group)
          .toggleClass('collapsed', collapsed);
      }
    },
        "paging": true,
        "ordering": false,
        "info": true,
        "responsive": true,
        //"destroy": true,
        dom: 'Bfrtip',
        //select: true,
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'All']
        ],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });
    sweet_alert_progressbar_cerrar();
}

function loadCombos() {
    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByPO/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.orderProductionID,
                            "name": obj.orderProduction
                        }
                    }
                );
                loadCombo(dataSelect, 'selectPO', true, 'Choose');
                $('#selectPO').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByPL/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.nroPackingList,
                            "name": obj.nroPackingList
                        }
                    }
                );
                loadCombo(dataSelect, 'selectPL', true, 'Choose');
                $('#selectPL').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByCustomer/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.finalCustomerID,
                            "name": obj.finalCustomer
                        }
                    }
                );
                loadCombo(dataSelect, 'selectCustomer', true, 'Choose');
                $('#selectCustomer').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByManager/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.managerID,
                            "name": obj.manager
                        }
                    }
                );
                loadCombo(dataSelect, 'selectManager', true, 'Choose');
                $('#selectManager').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByLogistic/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.logisticOperatorID,
                            "name": obj.logisticOperator
                        }
                    }
                );
                loadCombo(dataSelect, 'selectLogistic', true, 'Choose');
                $('#selectLogistic').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByWeek/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.week
                        }
                    }
                );
                loadCombo(dataSelect, 'selectWeek', true, 'Choose');
                $('#selectWeek').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/shippingIListByDestination/?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationID,
                            "name": obj.destination
                        }
                    }
                );
                loadCombo(dataSelect, 'selectDestination', true, 'Choose');
                $('#selectDestination').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function loadCombo(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function createList(wiNI, wEn) {

    $("table#shipping tbody").html('');

    let opt = 'all';
    dataClients = []
    $.ajax({
        type: "GET",
        url: "/Reports/ShippingIList?opt=" + opt + "&campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients = dataJson;
                var Instruccion = 0;
                var colorFila = "";
                var paso = 1;
                var packingListNumberAnterior = "";
                $.each(dataClients, function (i, e) {

                    var exists = false;
                    var trfind = [];

                    if (exists == false) {

                        var tr = '';
                        if (i > 0) {
                            if (packingListNumberAnterior == (e.nroPackingList).trim()) {
                                tr += "<tr style='font-size:12px!important; background-color: " + colorFila + "!important  '>";
                            } else {
                                if (paso == 1) {
                                    tr += "<tr  style='background-color: transparent!important;font-size:12px!important'>";
                                  colorFila = "transparent;";
                                    paso = 0;
                                } else {
                                    tr += "<tr  style='background-color: transparent!important;font-size:12px!important'>";
                                  colorFila = "transparent;";
                                    paso = 1
                                }
                            }

                            packingListNumberAnterior = (e.nroPackingList).trim();

                        } else {
                            packingListNumberAnterior = e.nroPackingList.trim();
                            tr += "<tr style='background-color: transparent!important;font-size:12px!important'>";
                          colorFila = "transparent;";
                        }

                        if (e.orderProduction == "") {
                            if (Instruccion != e.shippingLoadingID) {
                                Instruccion = e.shippingLoadingID;
                                tr += '<td class="sticky productionOrderNumber" style="background-color: white!important;max-width:150px;min-width:150px; ">';
                                tr += '<button class="btn btn-success btn-sm btnSave" style="border:none;outline:none!important"  data-toggle="tooltip" data-placement="top" title="Load S. Allocation">Load S. Allocation <i class="fas fa-save fa-sm text-white"></i></button>'
                                tr += '</td>';
                            } else {
                                tr += '<td class="sticky productionOrderNumber" style="background-color: white!important;max-width:120px;min-width:120px; ">';
                                tr += e.orderProduction;
                                tr += '</td>';
                            }
                        } else {
                            tr += '<td class="sticky productionOrderNumber" style="background-color: white!important;max-width:120px;min-width:120px; ">';
                            tr += e.orderProduction;
                            tr += '</td>';
                        }

                        tr += '<td class="sticky1" style="background-color: white!important;max-width:50px; min-width:50px" data-name="week">' + e.week + '</td>';
                        tr += '<td class="sticky2" style="background-color: white!important;max-width:150px; min-width:150px" data-name="nroPackingList">' + e.nroPackingList + '</td>';
                        tr += '<td style="max-width:130px; min-width:130px" data-name="processingPlant"><textarea style="font-size:13px" class="form-control form-control-sm" id="processingPlant" rows="1" readonly>' + e.processingPlant + '</textarea></td>';
                        tr += '<td style="background-color: white!important;max-width:80px; min-width:80px" data-name="loadingDay">' + e.loadingDay + '</td>';
                        tr += '<td style="background-color: white!important;max-width:80px; min-width:80px" data-name="uploadDate">' + e.uploadDate + '</td>';
                        tr += '<td style="max-width:100px; min-width:100px" data-name="chargingTime" >' + e.chargingTime + '</td>';
                        tr += '<td style="max-width:200px; min-width:200px" data-name="logisticOperator"><textarea style="font-size:13px" class="form-control form-control-sm" id="logisticOperator" rows="1" readonly>' + e.logisticOperator + '</textarea></td>';
                        tr += '<td style="max-width:150px; min-width:150px" data-name="shippingCompany">' + e.shippingCompany + '</td>';
                        tr += '<td style="max-width:30px; min-width:30px" data-name="tt">' + e.tt + '</td>';
                        tr += '<td style="max-width:170px; min-width:170px" data-name="vessel"><textarea style="font-size:13px" class="form-control form-control-sm" id="vessel" rows="1" readonly>' + e.vessel + '</textarea></td>';
                        tr += '<td style="max-width:80px; min-width:80px" data-name="etd">' + e.etd + '</td>';
                        tr += '<td style="max-width:80px; min-width:80px" data-name="eta">' + e.eta + '</td>';
                        tr += '<td style="max-width:80px; min-width:80px" data-name="etaReal" >' + e.etaReal + '</td>';
                        tr += '<td style="max-width:100px; min-width:100px" data-name="booking">' + e.booking + '</td>';
                        tr += '<td style="max-width:200px; min-width:200px" data-name="customer"><textarea style="font-size:13px" class="form-control form-control-sm" id="customer" rows="1" readonly>' + e.customer + '</textarea></td>';
                        tr += '<td style="max-width:200px; min-width:200px" data-name="finalCustomer"><textarea style="font-size:13px" class="form-control form-control-sm" id="finalCustomer" rows="1" readonly>' + e.finalCustomer + '</textarea></td>';
                        tr += '<td style="max-width:80px; min-width:80px" data-name="originPort">' + e.originPort + '</td>';
                        tr += '<td style="max-width:150px; min-width:150px" data-name="destinationPort"><textarea style="font-size:13px" class="form-control form-control-sm" id="logisticOperator" rows="1" readonly>' + e.destinationPort + '</textarea></td>';
                        tr += '<td style="max-width:120px; min-width:120px" data-name="manager">' + e.manager + '</td>';
                        tr += '<td style="max-width:80px; min-width:80px" data-name="brand">' + e.brand + '</td>';
                        tr += '<td style="max-width:60px; min-width:60px" data-name="category" >' + e.category + '</td>';
                        tr += '<td style="max-width:180px; min-width:180px" data-name="package">' + e.package + '</td>';
                        tr += '<td style="max-width:50px; min-width:50px" data-name="color">' + e.color + '</td>';
                        tr += '<td style="max-width:50px; min-width:50px" data-name="weight">' + e.weight + '</td>';
                        tr += '<td style="max-width:110px; min-width:110px" data-name="variety"><textarea style="font-size:13px" class="form-control form-control-sm" id="variety" rows="1" readonly>' + e.variety + '</textarea></td>';
                        tr += '<td style="max-width:50px; min-width:50px" data-name="quantity">' + e.quantity + '</td>';
                        tr += '<td style="max-width:120px; min-width:120px" data-name="container">' + e.container + '</td>';
                        tr += '<td style="max-width:120px; min-width:120px" data-name="bl" ><textarea style="font-size:13px" class="form-control form-control-sm" id="bl" rows="1" readonly>' + e.bl + '</textarea></td>';
                        tr += '</tr>';
                        $("table#shipping tbody").append(tr);

                    }
                })
            }
        },
        error: function (request, status, error) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            table= $("#shipping").DataTable({
                rowGroup: {
              // Uses the 'row group' plugin
              dataSrc: 0,
              startRender: function (rows, group) {
                var collapsed = !!collapsedGroups[group];

                rows.nodes().each(function (r) {
                  r.style.display = collapsed ? 'none' : '';
                });

                // Add category name to the <tr>. NOTE: Hardcoded colspan
                return $('<tr/>')
                  .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
                  .attr('data-name', group)
                  .toggleClass('collapsed', collapsed);
              }
            },
                "paging": true,
                "ordering": false,
                "info": true,
                "responsive": true,
                //"destroy": true,
                dom: 'Bfrtip',
                //select: true,
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'All']
                ],
                dom: 'Bfrtip',
                buttons: [

                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'
                ],
            });

        }
    });
}
