﻿var dataClients = []
var _cropID = localStorage.cropID;
var collapsedGroups = {};
var _table1;
var _perfil = localStorage.perfil;

window.onload = function () {

    createList();
    //loadCombos(); 

    $(document).on("click", ".btnSave", function () {
        var tr = $(this).closest('tr');
        Save(tr);
    });

    $('#summary thead tr').clone(true).appendTo('#summary thead');
    $('#summary thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input style="font-size:12px!important;color: black" class="form-control form-control-sm" type="text" placeholder="Filter" />');

        $('input', this).on('keyup change', function () {
            if (_table1.column(i).search() !== this.value) {
                _table1
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    //table = $('#summary').DataTable({
    //  dom: 'Bfrtip',
    //  buttons: [
    //    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
    //    'pageLength'
    //  ],
    //  orderCellsTop: true,
    //  fixedHeader: true,
    //  ordering: false,
    //  searching: true
    //});

    //$("#tablasummary").DataTable({
    //  "paging": true,
    //  "ordering": false,
    //  "info": true,
    //  "responsive": true,
    //  //"destroy": true,
    //  dom: 'Bfrtip',
    //  //select: true,
    //  lengthMenu: [
    //    [10, 25, 50, -1],
    //    ['10 rows', '25 rows', '50 rows', 'All']
    //  ],
    //  dom: 'Bfrtip',
    //  buttons: [
    //    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
    //    'pageLength'
    //  ],
    //});

    sweet_alert_progressbar_cerrar();
}

$('#summary tbody').on('click', 'tr.dtrg-start', function () {

    var name = $(this).data('name');
    collapsedGroups[name] = !collapsedGroups[name];
    table.draw(false);
});

$(document).on("keyup", "#qcOrigin", function (e) {
    $(this).closest('tr').find('.tdBtn').attr("style", "background-color:LightGreen")
    $(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", "#qcDestination", function (e) {
    $(this).closest('tr').find('.tdBtn').attr("style", "background-color:LightGreen")
    $(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".orderOZM", function (e) {
    $(this).closest('tr').find('.tdBtn').attr("style", "background-color:LightGreen")
    $(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", "#claim", function (e) {
    $(this).closest('tr').find('.tdBtn').attr("style", "background-color:LightGreen")
    $(this).closest('tr').find(".btnSave").show()
});

$(document).on("keyup", ".claimDate", function (e) {
    $(this).closest('tr').find('.tdBtn').attr("style", "background-color:LightGreen")
    $(this).closest('tr').find(".btnSave").show()
});

$(document).on("click", "#btnSearch", function () {
    filterList1();
})

$('#selectPO').change(function () {
    filterList1();
})

$('#selectWeek').change(function () {
    filterList1();
})

$('#selectPackingList').change(function () {
    filterList1();
})

$('#selectCustomer').change(function () {
    filterList1();
})

$('#selectStatus').change(function () {
    filterList1();
})

$('#selectDestination').change(function () {
    filterList1();
})

function filterList1() {
    sweet_alert_progressbar();
    $('table#summary').DataTable().destroy();
    $('table#summary>tbody').empty();
    var orderProduction = '';
    var color = "";
    var paso = 1;
    $.each(dataClients.filter(item => (item.orderProductionID == $("#selectPO option:selected").val() || $("#selectPO option:selected").val() == "") && (item.projectedWeekID == $("#selectWeek option:selected").val() || $("#selectWeek option:selected").val() == "") && (item.nroPackingList == $("#selectPackingList option:selected").val() || $("#selectPackingList option:selected").val() == "") && (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.status == $("#selectStatus option:selected").val() || $("#selectStatus option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {
        var exists = false;
        var trfind = [];

        if (exists == false) {
            var tr1 = '';
            var tr = '';
            if (i > 0) {

                tr1 += '<tr class="table-tr-agrocom">';
                tr1 += '<th class="sticky" style="max-width:120px; min-width:120px" data-toggle="tooltip" data-placement="top" title="Sale Order">Sales Order</th>';
                tr1 += '<th hidden> orderProductionID</th>';
                tr1 += '<th class="sticky1" style="max-width: 80px; min-width:80px">S.O. Date</th>';
                tr1 += '<th class="sticky2" style="max-width: 80px; min-width:80px">Status</th>';
                tr1 += '<th class="sticky3" style="max-width: 100px; min-width:100px">Order OZM</th>';
                tr1 += '<th class="sticky4" style="max-width: 60px; min-width:60px">D. Week</th>';
                tr1 += '<th class="sticky5" style="max-width: 200px; min-width:200px">Customer</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px" hidden>Consignee</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">Final Customer</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Origin</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Grower</th>';
                tr1 += '<th style="max-width: 130px; min-width:130px">Farm</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Loading Instruction</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Loading Date</th>';
                tr1 += '<th style="max-width: 60px; min-width:60px">Hour</th>';
                tr1 += '<th style="max-width: 120px; min-width:120px">Official</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Variety</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Weight</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Boxes</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Brand</th>';
                tr1 += '<th style="max-width: 140px; min-width:140px">CodePack</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Size</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px" hidden>Fruit Condition</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px" hidden>Format</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Pallets</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Net Weight</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Gross Weight</th>';
                tr1 += '<th style="max-width: 110px; min-width:110px">Market</th>';
                tr1 += '<th style="max-width: 120px; min-width:120px">Destination</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Via</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Week ETD</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">ETD</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Week ETA</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">ETA</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Invoice Grower</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px" hidden>Invoice MKT</th>';
                if (_perfil == 'CLD') {
                    tr1 += '<th style="display:none"></th>';
                } else {
                    tr1 += '<th style="max-width: 110px; min-width:110px;text-align:center;">G. Price USD/Box</th>';
                }
                tr1 += '<th style="max-width: 110px; min-width:110px;text-align:center;">Amount Invoice G.</th>';
                tr1 += '<th style="max-width: 60px; min-width:60px">Incoterm</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Sale Term</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">Payment term</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">BL or AWB</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Freight</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Booking</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Shipping Line</th>';
                tr1 += '<th style="max-width: 180px; min-width:180px">Vessel/Airlines</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Container</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Week Real</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">ETA Real</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">QC Report Origin</th>';
                tr1 += '<th style="max-width: 30px; min-width:30px"> </th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">QC Report Destination</th>';
                tr1 += '<th style="max-width: 30px; min-width:30px"> </th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">Claim</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Claim Date</th>';
                tr1 += '<th style="max-width: 30px; min-width:30px"> [ ** ] </th>';
                tr1 += '</tr>';
                $("table#summary thead").empty().append(tr1);

                if (e.statusID == 1) {
                    tr += '<tr  class="table-warning">';
                    color = 'badge-warning';
                }
                if (e.statusID == 2) {
                    tr += '<tr  class="table-danger">';
                    color = 'badge-danger';
                }
                if (e.statusID == 3) {
                    tr += '<tr  class="table-info">';
                    color = 'badge-info';
                }
                if (e.statusID == 4) {
                    tr += '<tr  class="table-primary">';
                    color = 'badge-primary';
                }
                if (e.statusID == 5) {
                    tr += '<tr  class="table-success">';
                    color = 'badge-success';
                }
                if (e.statusID == 7) {
                    tr += '<tr  class="table-secondary">';
                    color = '#d6d8db';
                }
                if (e.statusID == 8) {
                    tr += '<tr  class="table-secondary ">';
                    color = '#d6d8db';
                }

                orderProduction = (e.orderProduction).trim();

            } else {

                orderProduction = e.orderProduction.trim();

                if (e.statusID == 1) {
                    tr += '<tr  class="table-warning">';
                    color = 'badge-warning';
                }
                if (e.statusID == 2) {
                    tr += '<tr  class="table-danger">';
                    color = 'badge-danger';
                }
                if (e.statusID == 3) {
                    tr += '<tr  class="table-info">';
                    color = 'badge-info';
                }
                if (e.statusID == 4) {
                    tr += '<tr  class="table-primary">';
                    color = 'badge-primary';
                }
                if (e.statusID == 5) {
                    tr += '<tr  class="table-success">';
                    color = 'badge-success';
                }
                if (e.statusID == 7) {
                    tr += '<tr  class="table-secondary">';
                    color = '#d6d8db';
                }
                if (e.statusID == 8) {
                    tr += '<tr  class="table-secondary ">';
                    color = '#d6d8db';
                }
            }

            if (e.orderProduction == "") {
                tr += '<td class="sticky" class="productionOrderNumber" style="background-color: ' + color + '!important;max-width:120px; min-width:120px ">';
                tr += '</td>';
            } else {
                tr += '<td class="sticky" class="productionOrderNumber" style="background-color: ' + color + '!important;max-width:120px; min-width:120px ">';
                tr += e.orderProduction;
                tr += '</td>';
            }

            //tr += '<td class="sticky" data-name="orderProduction" >' + e.orderProduction + '</td>';
            tr += '<td class="orderProductionID" hidden>' + e.orderProductionID + '</td>';
            tr += '<td style="background-color: ' + color + '!important;max-width: 80px; min-width:80px" class="dateCreated sticky1">' + e.dateCreated + '</td>';
            tr += '<td style="background-color: ' + color + '!important;max-width: 80px; min-width:80px;font-size:16px" class="status sticky2"><div id="status" class="badge ' + color + ' badge - pill">' + e.status + '</div></td>';
            if (_cropID == 'BLU') {
                tr += '<td style="max-width: 100px; min-width:100px" class="orderOZM sticky3" contenteditable="true">' + e.orderOZM + '</td>';
            } else {
                tr += '<td style="max-width: 100px; min-width:100px" class="saleRequest sticky3">' + e.saleRequest + '</td>';
            }
            tr += '<td style="background-color: ' + color + '!important;max-width: 60px; min-width:60px" class="week sticky4">' + e.week + '</td>';
            tr += '<td style="background-color: ' + color + '!important;max-width: 200px; min-width:200px" class="customer sticky5"><textarea style="font-size:13px" class="form-control form-control-sm" id="customer" rows="1" readonly>' + e.customer + '</textarea></td>';
            tr += '<td style="background-color: ' + color + '!important;max-width: 200px; min-width:200px" class="consignee" hidden><textarea style="font-size:13px" class="form-control form-control-sm" id="consignee" rows="1">' + e.consignee + '</textarea></td>';
            tr += '<td style="background-color: ' + color + '!important;max-width: 200px; min-width:200px" class="finalCustomer"><textarea style="font-size:13px" class="form-control form-control-sm" id="finalCustomer" rows="1" readonly>' + e.finalCustomer + '</textarea></td>';
            tr += '<td style="max-width: 50px; min-width:50px" class="origin">' + e.origin + '</td>';
            tr += '<td style="max-width: 150px; min-width:150px" class="grower">' + e.grower + '</td>';
            tr += '<td style="max-width: 130px; min-width:130px" class="farm"><textarea style="font-size:13px" class="form-control form-control-sm" id="farm" rows="1" readonly>' + e.farm + '</textarea></td>';
            tr += '<td style="max-width: 150px; min-width:150px" class="nroPackingList">' + e.nroPackingList + '</td>';
            tr += '<td style="max-width: 80px; min-width:80px" class="loadingDate">' + e.loadingDate + '</td>';
            tr += '<td style="max-width: 60px; min-width:60px" class="hour">' + e.hour + '</td>';
            tr += '<td style="max-width: 120px; min-width:120px" class="official">' + e.official + '</td>';
            tr += '<td style="max-width: 150px; min-width:150px" class="variety"><textarea style="font-size:13px" class="form-control form-control-sm" id="variety" rows="1" readonly>' + e.variety + '</textarea></td>';
            tr += '<td style="max-width: 50px; min-width:50px" class="weight">' + e.weight + '</td>';
            tr += '<td style="max-width: 50px; min-width:50px;text-align:right;" class="boxes">' + (e.boxes) + '</td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="brand">' + e.brand + '</td>';
            tr += '<td style="max-width: 140px; min-width:140px" class="presentation">' + e.presentation + '</td>';
            tr += '<td style="max-width: 50px; min-width:50px" class="size">' + e.size + '</td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="condition" hidden>' + e.condition + '</td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="format" hidden>' + e.format + '</td>';
            tr += '<td style="max-width: 50px; min-width:50px;text-align:right" class="pallets">' + (e.pallets) + '</td>';
            tr += '<td style="max-width: 70px; min-width:70px;text-align:right" class="netWeight">' + (e.netWeight) + '</td>';
            tr += '<td style="max-width: 80px; min-width:80px;text-align:right" class="vgm">' + (e.vgm) + '</td>';
            tr += '<td style="max-width: 110px; min-width:110px" class="market">' + e.market + '</td>';
            tr += '<td style="max-width: 120px; min-width:120px" class="destination">' + e.destination + '</td>';
            tr += '<td style="max-width: 50px; min-width:50px"  class="via">' + e.via + '</td>';
            tr += '<td style="max-width: 70px; min-width:70px" class="weekETD">' + e.weekETD + '</td>';
            tr += '<td style="max-width: 80px; min-width:80px" class="etd">' + e.etd + '</td>';
            tr += '<td style="max-width: 70px; min-width:70px" class="weekETA">' + e.weekETA + '</td>';
            tr += '<td style="max-width: 80px; min-width:80px" class="eta" >' + e.eta + '</td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="invoiceGrower">' + e.invoiceGrower + '</td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="invoiceMKT" hidden>' + e.invoiceMKT + '</td>';
            if (_perfil == 'CLD') {
                tr += '<td style="display:none"></td>';
            } else {
                tr += '<td style="max-width: 110px; min-width:110px;text-align:right;" class="growerPriceUSD">' + (e.growerPriceUSD) + '</td>';
            }
            tr += '<td style="max-width: 110px; min-width:110px;text-align:right;" class="amountInvoiceGrower">' + (e.amountInvoiceGrower) + '</td>';
            tr += '<td style="max-width: 60px; min-width:60px" class="incoterm">' + e.incoterm + '</td>';
            tr += '<td style="max-width: 80px; min-width:80px" class="saleTerm">' + e.saleTerm + '</td>';
            tr += '<td style="max-width: 200px; min-width:200px; font-size:" class="paymentTerm"><textarea style="font-size:13px" class="form-control form-control-sm" id="paymentTerm" rows="1" readonly>' + e.paymentTerm + '</textarea></td >';
            tr += '<td style="max-width: 150px; min-width:150px" class="blorAWB">' + e.blorAWB + '</td>';
            tr += '<td style="max-width: 50px; min-width:50px" class="freight">' + e.freight + '</td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="booking" >' + e.booking + '</td>';
            tr += '<td style="max-width: 150px; min-width:150px" class="shippingLine">' + e.shippingLine + '</td>';
            tr += '<td style="max-width: 170px; min-width:170px" class="veseel"><textarea style="font-size:13px" class="form-control form-control-sm" id="veseel" rows="1" readonly>' + e.veseel + '</textarea></td>';
            tr += '<td style="max-width: 100px; min-width:100px" class="container">' + e.container + '</td>';
            tr += '<td style="max-width: 70px; min-width:70px" class="weekReal">' + e.weekReal + '</td>';
            tr += '<td style="max-width: 80px; min-width:80px" class="etaReal">' + e.etaReal + '</td>';
            tr += '<td class="qcOrigin" style="max-width: 200px; min-width:200px" contenteditable="true"><textarea style="font-size:13px" class="form-control form-control-sm" id="qcOrigin" rows="1">' + e.qcOrigin + '</textarea></td>';
            tr += '<td style="max-width: 20px; min-width:20px" class="linkOrigin"><a target = "_blank" href="' + e.qcOrigin + '"><button class="btn btn-sm btn-success" style="font-size: 12px;"><i class="fas fa-arrow-right fa-sm" ></i></button></a></td>';
            tr += '<td class="qcDestination" style="max-width: 200px; min-width:200px" contenteditable="true"><textarea style="font-size:13px" class="form-control form-control-sm" id="qcDestination" rows="1">' + e.qcDestination + '</textarea></td>';
            tr += '<td style="max-width: 20px; min-width:20px" class="linkDestination"><a target = "_blank" href="' + e.qcDestination + '"><button class="btn btn-sm btn-success" style="font-size: 12px;"><i class="fas fa-arrow-right fa-sm" ></i></button></a></td>';
            tr += '<td class="claim" style="max-width: 200px; min-width:200px" contenteditable="true"><textarea style="font-size:13px" class="form-control form-control-sm" id="claim" rows="1">' + e.claim + '</textarea></td>';
            tr += '<td class="claimDate" style="max-width: 80px; min-width:80px" contenteditable="true">' + e.claimDate + '</td>';
            tr += '<td class="tdBtn" style="max-width: 30px; min-width:30px"><button class="btnSave" style="font-size: 14px; display: none"><i class="fas fa-save"></i></button></td>';
            tr += '</tr>';
            $("table#summary tbody").append(tr);
        }
    })
    var datafind = []

    _table1 = $('#summary').DataTable({
        "scrollY": "400px",
        "scrollX": false,
        scrollCollapse: true,
        //'rowsGroup': [0],
        rowGroup: {
            // Uses the 'row group' plugin
            dataSrc: 0,
            startRender: function (rows, group) {
                var collapsed = !!collapsedGroups[group];

                rows.nodes().each(function (r) {
                    r.style.display = collapsed ? 'none' : '';
                });

                // Add category name to the <tr>. NOTE: Hardcoded colspan
                return $('<tr/>')
                    .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
                    .attr('data-name', group)
                    .toggleClass('collapsed', collapsed);
            }
        },
        orderCellsTop: false,
        fixedHeader: false,
        searching: true,
        "ordering": false,
        info: true,
        "searchable": true,
        //dom: 'Bfrtip',    
        "bPaginate": true,
        "paging": false
    });

    //table = $('table#summary').DataTable({
    //  dom: 'Bfrtip',
    //  buttons: [
    //    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
    //    'pageLength'
    //  ],
    //  orderCellsTop: true,
    //  fixedHeader: true,
    //  ordering: false,
    //  searching: true
    //});

    //table = $("#summary").DataTable({
    //    //'rowsGroup': [0],
    //    rowGroup: {
    //    // Uses the 'row group' plugin
    //    dataSrc: 0,
    //    startRender: function (rows, group) {
    //      var collapsed = !!collapsedGroups[group];
    //      rows.nodes().each(function (r) {
    //        r.style.display = collapsed ? 'none' : '';
    //      });
    //      // Add category name to the <tr>. NOTE: Hardcoded colspan
    //      return $('<tr/>')
    //        .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
    //        .attr('data-name', group)
    //        .toggleClass('collapsed', collapsed);
    //    }
    //  },
    //  "paging": true,
    //  "ordering": false,
    //  fixedHeader: true,
    //  "info": true,
    //  "responsive": true,
    //  //"destroy": true,
    //  dom: 'Bfrtip',
    //  //select: true,
    //  lengthMenu: [
    //    [10, 25, 50, -1],
    //    ['10 rows', '25 rows', '50 rows', 'All']
    //  ],
    //  dom: 'Bfrtip',
    //  buttons: [
    //      {
    //          extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
    //          exportOptions: {
    //              columns: [0, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 53, 54]
    //          }
    //      },
    //    'pageLength'
    //  ],
    //});

    sweet_alert_progressbar_cerrar();
}

function loadCombos() {
    $.ajax({
        type: "GET",
        url: "/Reports/summaryListByPO?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.orderProductionID,
                            "name": obj.orderProduction
                        }
                    }
                );
                loadCombo(dataSelect, 'selectPO', true, 'Choose');
                $('#selectPO').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/summaryListByWeek?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.week
                        }
                    }
                );
                loadCombo(dataSelect, 'selectWeek', true, 'Choose');
                $('#selectWeek').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/summaryListByPacking?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.nroPackingList,
                            "name": obj.nroPackingList
                        }
                    }
                );
                loadCombo(dataSelect, 'selectPackingList', true, 'Choose');
                $('#selectPackingList').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/summaryListByCustomer?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.customerID,
                            "name": obj.customer
                        }
                    }
                );
                loadCombo(dataSelect, 'selectCustomer', true, 'Choose');
                $('#selectCustomer').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/summaryListByStatus?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.status,
                            "name": obj.status
                        }
                    }
                );
                loadCombo(dataSelect, 'selectStatus', true, 'Choose');
                $('#selectStatus').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

    $.ajax({
        type: "GET",
        url: "/Reports/summaryListByDestination?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationID,
                            "name": obj.destination
                        }
                    }
                );
                loadCombo(dataSelect, 'selectDestination', true, 'Choose');
                $('#selectDestination').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });

}

function loadCombo(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function createList(wiNI, wEn) {

    $("table#summary tbody").html('');

    dataClients = []
    $.ajax({
        type: "GET",
        url: "summaryList?campaignID=" + localStorage.campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0'
        },
        success: function (data) {
            var tr1 = '';
            if (data.length > 0) {
                tr1 += '<tr class="table-tr-agrocom">';
                tr1 += '<th class="sticky" style="max-width:120px; min-width:120px" data-toggle="tooltip" data-placement="top" title="Sale Order">Sales Order</th>';
                tr1 += '<th hidden> orderProductionID</th>';
                tr1 += '<th class="sticky1" style="max-width: 80px; min-width:80px">S.O. Date</th>';
                tr1 += '<th class="sticky2" style="max-width: 80px; min-width:80px">Status</th>';
                tr1 += '<th class="sticky3" style="max-width: 100px; min-width:100px">Order OZM</th>';
                tr1 += '<th class="sticky4" style="max-width: 60px; min-width:60px">D. Week</th>';
                tr1 += '<th class="sticky5" style="max-width: 200px; min-width:200px">Customer</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px" hidden>Consignee</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">Final Customer</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Origin</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Grower</th>';
                tr1 += '<th style="max-width: 130px; min-width:130px">Farm</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Loading Instruction</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Loading Date</th>';
                tr1 += '<th style="max-width: 60px; min-width:60px">Hour</th>';
                tr1 += '<th style="max-width: 120px; min-width:120px">Official</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Variety</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Weight</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Boxes</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Brand</th>';
                tr1 += '<th style="max-width: 140px; min-width:140px">CodePack</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Size</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px" hidden>Fruit Condition</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px" hidden>Format</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Pallets</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Net Weight</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Gross Weight</th>';
                tr1 += '<th style="max-width: 110px; min-width:110px">Market</th>';
                tr1 += '<th style="max-width: 120px; min-width:120px">Destination</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Via</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Week ETD</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">ETD</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Week ETA</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">ETA</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Invoice Grower</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px" hidden>Invoice MKT</th>';
                if (_perfil == 'CLD') {
                    tr1 += '<th style="display:none"></th>';
                } else {
                    tr1 += '<th style="max-width: 110px; min-width:110px;text-align:center;">G. Price USD/Box</th>';
                }
                tr1 += '<th style="max-width: 110px; min-width:110px;text-align:center;">Amount Invoice G.</th>';
                tr1 += '<th style="max-width: 60px; min-width:60px">Incoterm</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Sale Term</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">Payment term</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">BL or AWB</th>';
                tr1 += '<th style="max-width: 50px; min-width:50px">Freight</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Booking</th>';
                tr1 += '<th style="max-width: 150px; min-width:150px">Shipping Line</th>';
                tr1 += '<th style="max-width: 180px; min-width:180px">Vessel/Airlines</th>';
                tr1 += '<th style="max-width: 100px; min-width:100px">Container</th>';
                tr1 += '<th style="max-width: 70px; min-width:70px">Week Real</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">ETA Real</th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">QC Report Origin</th>';
                tr1 += '<th style="max-width: 30px; min-width:30px"> </th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">QC Report Destination</th>';
                tr1 += '<th style="max-width: 30px; min-width:30px"> </th>';
                tr1 += '<th style="max-width: 200px; min-width:200px">Claim</th>';
                tr1 += '<th style="max-width: 80px; min-width:80px">Claim Date</th>';
                tr1 += '<th style="max-width: 30px; min-width:30px"> [ ** ] </th>';
                tr1 += '</tr>';
                $("table#summary thead").empty().append(tr1);
                $('#summary thead tr').clone(true).appendTo('#summary thead');
                $('#summary thead tr:eq(1) th').each(function (i) {
                    var title = $(this).text();
                    $(this).html('<input style="font-size:12px!important;color: black" class="form-control form-control-sm" type="text" placeholder="Filter" />');

                    $('input', this).on('keyup change', function () {
                        if (_table1.column(i).search() !== this.value) {
                            _table1
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                });

                var dataJson = JSON.parse(data);
                dataClients = dataJson;
                var Instruccion = 0;
                var orderProduction = '';
                var color = "";
                var paso = 1;
                $.each(dataClients, function (i, e) {

                    var exists = false;
                    var trfind = [];

                    if (exists == false) {

                        var tr = '';
                        if (i > 0) {
                            if (e.statusID == 1) {
                                tr += '<tr  class="table-warning">';
                                color = 'badge-warning';
                            }
                            if (e.statusID == 2) {
                                tr += '<tr  class="table-danger">';
                                color = 'badge-danger';
                            }
                            if (e.statusID == 3) {
                                tr += '<tr  class="table-info">';
                                color = 'badge-info';
                            }
                            if (e.statusID == 4) {
                                tr += '<tr  class="table-primary">';
                                color = 'badge-primary';
                            }
                            if (e.statusID == 5) {
                                tr += '<tr  class="table-success">';
                                color = 'badge-success';
                            }
                            if (e.statusID == 7) {
                                tr += '<tr  class="table-secondary">';
                                color = '#d6d8db';
                            }
                            if (e.statusID == 8) {
                                tr += '<tr  class="table-secondary ">';
                                color = '#d6d8db';
                            }
                            //if (orderProduction == (e.orderProduction).trim()) {
                            //   tr += "<tr style='font-size:13px!important; background-color: " + color + "!important  '>";
                            //} else {
                            //    if (paso == 1) {
                            //        tr += "<tr  style='background-color: #f8f9fc!important;font-size:13px!important'>";
                            //        color = "#f8f9fc;";
                            //        paso = 0;
                            //    } else {
                            //        tr += "<tr  style='background-color: #D0DAF1!important;font-size:13px!important'>";
                            //        color = "#D0DAF1;";
                            //        paso = 1
                            //    }
                            //}

                            orderProduction = (e.orderProduction).trim();

                        } else {
                            orderProduction = e.orderProduction.trim();
                            if (e.statusID == 1) {
                                tr += '<tr  class="table-warning">';
                                color = 'badge-warning';
                            }
                            if (e.statusID == 2) {
                                tr += '<tr  class="table-danger">';
                                color = 'badge-danger';
                            }
                            if (e.statusID == 3) {
                                tr += '<tr  class="table-info">';
                                color = 'badge-info';
                            }
                            if (e.statusID == 4) {
                                tr += '<tr  class="table-primary">';
                                color = 'badge-primary';
                            }
                            if (e.statusID == 5) {
                                tr += '<tr  class="table-success">';
                                color = 'badge-success';
                            }
                            if (e.statusID == 7) {
                                tr += '<tr  class="table-secondary">';
                                color = '#d6d8db';
                            }
                            if (e.statusID == 8) {
                                tr += '<tr  class="table-secondary ">';
                                color = '#d6d8db';
                            }
                        }

                        if (e.orderProduction == "") {
                            if (Instruccion != e.orderProductionID) {
                                Instruccion = e.orderProductionID;
                                tr += '<td class="sticky" class="productionOrderNumber" style="background-color: ' + color + '!important;max-width:120px;min-width:120px; ">';
                                tr += '</td>';
                            } else {
                                tr += '<td class="sticky" class="productionOrderNumber" style="background-color: ' + color + '!important;max-width:120px;min-width:120px; ">';
                                tr += e.orderProduction;
                                tr += '</td>';
                            }
                        } else {
                            tr += '<td class="sticky" class="productionOrderNumber" style="background-color: ' + color + '!important;max-width:120px;min-width:120px; ">';
                            tr += e.orderProduction;
                            tr += '</td>';
                        }

                        //tr += '<td class="sticky" >' + e.orderProduction + '</td>';
                        tr += '<td class="orderProductionID" hidden>' + e.orderProductionID + '</td>';
                        tr += '<td style="background-color: ' + color + '!important;max-width: 80px; min-width:80px" class="dateCreated sticky1">' + e.dateCreated + '</td>';
                        tr += '<td style="background-color: ' + color + '!important;max-width: 80px; min-width:80px ;font-size:16px" class="status sticky2"><div id="status" class="badge ' + color + ' badge - pill">' + e.status + '</div></td>';
                        if (_cropID == 'BLU') {
                            tr += '<td style="background-color: ' + color + '!important;max-width: 100px; min-width:100px" class="orderOZM sticky3" contenteditable="true">' + e.orderOZM + '</td>';
                        } else {
                            tr += '<td style="background-color: ' + color + '!important;max-width: 100px; min-width:100px" class="saleRequest sticky3">' + e.saleRequest + '</td>';
                        }
                        tr += '<td style="background-color: ' + color + '!important;max-width: 60px; min-width:60px" class="week sticky4">' + e.week + '</td>';
                        tr += '<td style="background-color: ' + color + '!important;max-width: 200px; min-width:200px" class="customer sticky5"><textarea style="font-size:13px" class="form-control form-control-sm" id="customer" rows="1" readonly>' + e.customer + '</textarea></td>';
                        tr += '<td style="background-color: ' + color + '!important;max-width: 200px; min-width:200px" class="consignee" hidden><textarea style="font-size:13px" class="form-control form-control-sm" id="consignee" rows="1">' + e.consignee + '</textarea></td>';
                        tr += '<td style="max-width: 200px; min-width:200px" class="finalCustomer"><textarea style="font-size:13px" class="form-control form-control-sm" id="finalCustomer" rows="1" readonly>' + e.finalCustomer + '</textarea></td>';
                        tr += '<td style="max-width: 50px; min-width:50px" class="origin">' + e.origin + '</td>';
                        tr += '<td style="max-width: 150px; min-width:150px" class="grower">' + e.grower + '</td>';
                        tr += '<td style="max-width: 130px; min-width:130px" class="farm"><textarea style="font-size:13px" class="form-control form-control-sm" id="farm" rows="1" readonly>' + e.farm + '</textarea></td>';
                        tr += '<td style="max-width: 150px; min-width:150px" class="nroPackingList">' + e.nroPackingList + '</td>';
                        tr += '<td style="max-width: 80px; min-width:80px" class="loadingDate">' + e.loadingDate + '</td>';
                        tr += '<td style="max-width: 60px; min-width:60px" class="hour">' + e.hour + '</td>';
                        tr += '<td style="max-width: 120px; min-width:120px" class="official">' + e.official + '</td>';
                        tr += '<td style="max-width: 150px; min-width:150px" class="variety"><textarea style="font-size:13px" class="form-control form-control-sm" id="variety" rows="1" readonly>' + e.variety + '</textarea></td>';
                        tr += '<td style="max-width: 50px; min-width:50px" class="weight">' + e.weight + '</td>';
                        tr += '<td style="max-width: 50px; min-width:50px;text-align:right;" class="boxes">' + (e.boxes) + '</td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="brand">' + e.brand + '</td>';
                        tr += '<td style="max-width: 140px; min-width:140px" class="presentation">' + e.presentation + '</td>';
                        tr += '<td style="max-width: 50px; min-width:50px" class="size">' + e.size + '</td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="condition" hidden>' + e.condition + '</td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="format" hidden>' + e.format + '</td>';
                        tr += '<td style="max-width: 50px; min-width:50px;text-align:right" class="pallets">' + (e.pallets) + '</td>';
                        tr += '<td style="max-width: 70px; min-width:70px;text-align:right" class="netWeight">' + (e.netWeight) + '</td>';
                        tr += '<td style="max-width: 80px; min-width:80px;text-align:right" class="vgm">' + (e.vgm) + '</td>';
                        tr += '<td style="max-width: 110px; min-width:110px" class="market">' + e.market + '</td>';
                        tr += '<td style="max-width: 120px; min-width:120px" class="destination">' + e.destination + '</td>';
                        tr += '<td style="max-width: 50px; min-width:50px"  class="via">' + e.via + '</td>';
                        tr += '<td style="max-width: 70px; min-width:70px" class="weekETD">' + e.weekETD + '</td>';
                        tr += '<td style="max-width: 80px; min-width:80px" class="etd">' + e.etd + '</td>';
                        tr += '<td style="max-width: 70px; min-width:70px" class="weekETA">' + e.weekETA + '</td>';
                        tr += '<td style="max-width: 80px; min-width:80px" class="eta" >' + e.eta + '</td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="invoiceGrower">' + e.invoiceGrower + '</td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="invoiceMKT" hidden>' + e.invoiceMKT + '</td>';
                        if (_perfil == 'CLD') {
                            tr += '<td style="display:none"></td>';
                        } else {
                            tr += '<td style="max-width: 110px; min-width:110px;text-align:right;" class="growerPriceUSD">' + (e.growerPriceUSD) + '</td>';
                        }
                        tr += '<td style="max-width: 110px; min-width:110px;text-align:right;" class="amountInvoiceGrower">' + (e.amountInvoiceGrower) + '</td>';
                        tr += '<td style="max-width: 60px; min-width:60px" class="incoterm">' + e.incoterm + '</td>';
                        tr += '<td style="max-width: 80px; min-width:80px" class="saleTerm">' + e.saleTerm + '</td>';
                        tr += '<td style="max-width: 200px; min-width:200px; font-size:" class="paymentTerm"><textarea style="font-size:13px" class="form-control form-control-sm" id="paymentTerm" rows="1" readonly>' + e.paymentTerm + '</textarea></td >';
                        tr += '<td style="max-width: 150px; min-width:150px" class="blorAWB">' + e.blorAWB + '</td>';
                        tr += '<td style="max-width: 50px; min-width:50px" class="freight">' + e.freight + '</td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="booking" >' + e.booking + '</td>';
                        tr += '<td style="max-width: 150px; min-width:150px" class="shippingLine">' + e.shippingLine + '</td>';
                        tr += '<td style="max-width: 170px; min-width:170px" class="veseel"><textarea style="font-size:13px" class="form-control form-control-sm" id="veseel" rows="1" readonly>' + e.veseel + '</textarea></td>';
                        tr += '<td style="max-width: 100px; min-width:100px" class="container">' + e.container + '</td>';
                        tr += '<td style="max-width: 70px; min-width:70px" class="weekReal">' + e.weekReal + '</td>';
                        tr += '<td style="max-width: 80px; min-width:80px" class="etaReal">' + e.etaReal + '</td>';
                        tr += '<td class="qcOrigin" style="max-width: 200px; min-width:200px" contenteditable="true"><textarea style="font-size:13px" class="form-control form-control-sm" id="qcOrigin" rows="1">' + e.qcOrigin + '</textarea></td>';
                        tr += '<td style="max-width: 20px; min-width:20px" class="linkOrigin"><a target = "_blank" href="' + e.qcOrigin + '"><button class="btn btn-sm btn-success"><i class="fas fa-arrow-right fa-sm" ></i></button></a></td>';
                        tr += '<td class="qcDestination" style="max-width: 200px; min-width:200px" contenteditable="true"><textarea style="font-size:13px" class="form-control form-control-sm" id="qcDestination" rows="1">' + e.qcDestination + '</textarea></td>';
                        tr += '<td style="max-width: 20px; min-width:20px" class="linkDestination"><a target = "_blank" href="' + e.qcDestination + '"><button class="btn btn-sm btn-success"><i class="fas fa-arrow-right fa-sm" ></i></button></a></td>';
                        tr += '<td class="claim" style="max-width: 200px; min-width:200px" contenteditable="true"><textarea style="font-size:13px" class="form-control form-control-sm" id="claim" rows="1">' + e.claim + '</textarea></td>';
                        tr += '<td class="claimDate" style="max-width: 80px; min-width:80px" contenteditable="true">' + e.claimDate + '</td>';
                        tr += '<td class="tdBtn" style="max-width: 30px; min-width:30px"><button class="btnSave" style="font-size: 14px; display: none"><i class="fas fa-save"></i></button></td>';
                        tr += '</tr>';
                        $("table#summary tbody").append(tr);

                    }
                })
            }
        },

        complete: function () {

            _table1 = $('#summary').DataTable({
                "scrollY": "650px",
                "scrollX": false,
                scrollCollapse: true,
                dom: 'Bfrtip',
                lengthMenu: [
                    [-1],
                    ['All']
                ],
                buttons: [
                    {
                        extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
                        exportOptions: {
                            columns: [0, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 53, 54]
                        },
                        sheetName: 'Summary',
                        title: 'Shipments Summary Report'
                    }
                ],
                //'rowsGroup': [0],
                rowGroup: {
                    // Uses the 'row group' plugin
                    dataSrc: 0,
                    startRender: function (rows, group) {
                        var collapsed = !!collapsedGroups[group];

                        rows.nodes().each(function (r) {
                            r.style.display = collapsed ? 'none' : '';
                        });

                        // Add category name to the <tr>. NOTE: Hardcoded colspan
                        return $('<tr/>')
                            .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
                            .attr('data-name', group)
                            .toggleClass('collapsed', collapsed);
                    }
                },
                orderCellsTop: true,
                fixedHeader: false,
                searching: true,
                "ordering": false,
                info: true,
                "searchable": true,
                "bPaginate": true,
                "paging": false

            });

            //table = $('table#summary').DataTable({
            //  dom: 'Bfrtip',
            //  buttons: [
            //    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            //    'pageLength'
            //  ],
            //  orderCellsTop: true,
            //  fixedHeader: true,
            //  ordering: false,
            //  searching: true
            //});

            //table =   $("#summary").DataTable({
            //    //'rowsGroup': [0],
            //    rowGroup: {
            //      // Uses the 'row group' plugin
            //      dataSrc: 0,
            //      startRender: function (rows, group) {
            //        var collapsed = !!collapsedGroups[group];

            //        rows.nodes().each(function (r) {
            //          r.style.display = collapsed ? 'none' : '';
            //        });

            //        // Add category name to the <tr>. NOTE: Hardcoded colspan
            //        return $('<tr/>')
            //          .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
            //          .attr('data-name', group)
            //          .toggleClass('collapsed', collapsed);
            //      }
            //    },
            //        "paging": true,
            //        "ordering": false,
            //        "info": true,
            //        fixedHeader: true,
            //        "responsive": true,
            //        //"destroy": true,
            //        dom: 'Bfrtip',
            //        //select: true,
            //        lengthMenu: [
            //            [10, 25, 50, -1],
            //            ['10 rows', '25 rows', '50 rows', 'All']
            //        ],
            //        dom: 'Bfrtip',
            //        buttons: [
            //            {
            //                extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
            //                exportOptions: {
            //                    columns: [0, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 53, 54]
            //                }
            //            },
            //            'pageLength'
            //        ],
            //    });
        }

    });

}

function Save(tr) {

    if ($(tr).find("#qcOrigin").val() != "") {
        qcOrigin = $(tr).find("#qcOrigin").val()
    } else {
        qcOrigin = ""
    }

    if ($(tr).find("#qcDestination").val() != "") {
        qcDestination = $(tr).find("#qcDestination").val()
    } else {
        qcDestination = ""
    }

    if ($(tr).find(".orderOZM").text() != "") {
        orderOZM = $(tr).find(".orderOZM").text()
    } else {
        orderOZM = ""
    }

    if ($(tr).find("#claim").val() != "") {
        claim = $(tr).find("#claim").val()
    } else {
        claim = ""
    }

    var claimDateT = $(tr).find(".claimDate").text();
    if (claimDateT != "" && claimDateT != undefined) {
        var claimDateTime = $(tr).find(".claimDate").text().split(" ");
        var claimDateC = claimDateTime[0].split("/");
        claimDateD = claimDateC[2] + '-' + claimDateC[1] + '-' + claimDateC[0];
        claimDate = claimDateD;

    } else {
        claimDate = "";
    }

    orderProductionID = $(tr).find(".orderProductionID").text()

    $(tr).find(".btnSave").hide()
    $(tr).find(".tdBtn").removeAttr("style")

    var o = {

        "orderProductionID": $(tr).find(".orderProductionID").text(),
        "qcOrigin": qcOrigin,
        "qcDestination": qcDestination,
        "orderOZM": orderOZM,
        "claim": claim,
        "claimDate": claimDate
    }

    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/ShippingProgram/GetDateUpdate?opt=" + "lin" + "&id=" + orderProductionID + "&data1=" + qcOrigin + "&data2=" + qcDestination + "&data3=" + orderOZM + "&data4=" + '' + "&data5=" + claim + "&data6=" + claimDate + "&data7=" + '',
        //async: true,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (data) {
            sweet_alert_success('Good Joob!', 'successfully saved');
        },
        error: function (datoEr) {
            sweet_alert_error("Error", "There has been a problem!");
        }
    })
}