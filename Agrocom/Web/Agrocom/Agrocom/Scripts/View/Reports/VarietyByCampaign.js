﻿var jsonvar;

window.onload = function () {
    loadChart();
    loadVariety();
}

function loadVariety() {

    $.ajax({
        type: "GET",
        url: "/Reports/GePackinglistApache?opt=" + "var",
        //async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            console.log(dataJson);
            //jsonvar = {
            //    type: "pie",
            //    name: "Boxes By Variety",
            //    showInLegend: true,
            //    yValueFormatString: "Tn#,##0",
            //    dataPoints: dataJson
            //}
            //loadChart()
        }
    });
}


function loadChart() {
    var chartDom = document.getElementById('variety');
    var myChart = echarts.init(chartDom);
    var option;

    setTimeout(function () {

        option = {
            legend: {},
            tooltip: {
                trigger: 'axis',
                showContent: true
            },
            dataset: {
                source: [
                    ['product', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November','December'],
                    ['Sweet Globe', 223025, 129324, 0, 0, 0, 0, 0, 0, 0, 0, 0, 137338],
                    ['Sweet Sapphire', 56034, 32315, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    ['Sweet Celebration', 67282, 6541, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16245],
                    ['Candy Snaps', 22080, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    ['Cotton Candy', 86395, 14969, 0, 0, 0, 0, 0, 0, 0, 0, 0, 480],
                    ['Red Globe', 11310, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    ['Jack´s Salute', 30975, 132558, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73840]
                ]
            },
            xAxis: { type: 'category' },
            yAxis: { gridIndex: 0 },
            grid: { top: '50%' },
            series: [
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                { type: 'line', smooth: true, seriesLayoutBy: 'row', emphasis: { focus: 'series' } },
                {
                    type: 'pie',
                    id: 'pie',
                    radius: '35%',
                    center: ['50%', '25%'],
                    emphasis: { focus: 'data' },
                    label: {
                        formatter: '{b}: {@January} ({d}%)'
                    },
                    encode: {
                        itemName: 'product',
                        value: 'January',
                        tooltip: 'January'
                    }
                }
            ]
        };

        myChart.on('updateAxisPointer', function (event) {
            var xAxisInfo = event.axesInfo[0];
            if (xAxisInfo) {
                var dimension = xAxisInfo.value + 1;
                myChart.setOption({
                    series: {
                        id: 'pie',
                        label: {
                            formatter: '{b}: {@[' + dimension + ']} ({d}%)'
                        },
                        encode: {
                            value: dimension,
                            tooltip: dimension
                        }
                    }
                });
            }
        });

        myChart.setOption(option);

    });

    option && myChart.setOption(option);
}

