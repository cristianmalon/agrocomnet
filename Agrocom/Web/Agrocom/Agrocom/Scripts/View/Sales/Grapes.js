﻿var _bRsl;
var dataVariety = [];
var dataCategory = [];
var dataFormat = [];
var dataTypeBox = [];
var dataBrand = [];
var dataLabel = [];
var dataPresentation = [];
var dataSize = [];
var dataMarket = [];
var dataVia = [];
var dataWow = []
var dataDestinationPort = []
var dataCustomerMarket = []
var dataDestinationCustomer = []
var dataConditionPayment = []
var dataWeek = []
var dataStatus = []
var dataMarketUser = []
var dataAllWeek = []
var ActualyWeek = []
var NextWeek = []
var dataCodePack = []
var dataCodePack2 = []
var _userID = $("#lblSessionUserID").text();
var _campaignID = [];
var table;
var trEdit = '';
var _statuSale = [];
var dataIncoterm = [];
var _cropID = localStorage.cropID;
var _statusID = 0;

$(function () {
    LoadData();
    $('#selectStatusFilter').val('All').change();
    $("#selectStatusFilter").selectpicker('refresh');

    $(document).on("click", "#btnLoaded", function () {
        $("#selectMarketFilter").selectpicker('deselectAll');
        $('#selectMarketFilter').selectpicker('refresh');

        var Loaded = dataStatus.filter(item => item.id == 6)
        $('#selectStatusFilter').val(Loaded[0].id).change();
        var marketID = ''//$('#selectMarketFilter').val();
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();

        //list(marketID, weekIni, weekEnd, statusID);
        loadListPrincipal();
    })

    $(document).on("click", "#btnConfirmed", function () {
        $("#selectMarketFilter").selectpicker('deselectAll');
        $('#selectMarketFilter').selectpicker('refresh');
        //$('#cboSemanaIni').val(ActualyWeek[0].id).change();
        //$('#cboSemanaFin').val(dataAllWeek[dataAllWeek.length - 1].id).change();
        //$('#cboSemanaFin').val(dataAllWeek[dataAllWeek.length - 1].id).change();
        var Confirmed = dataStatus.filter(item => item.id == 5)
        $('#selectStatusFilter').val(Confirmed[0].id).change();

        var marketID = ''//$("#selectMarketFilter").val();
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();

        //list(marketID, weekIni, weekEnd, statusID);
        loadListPrincipal();
    })

    $(document).on("click", "#btnModified", function () {
        $("#selectMarketFilter").selectpicker('deselectAll');
        $('#selectMarketFilter').selectpicker('refresh');
        var Modified = dataStatus.filter(item => item.id == 8)
        $('#selectStatusFilter').val(Modified[0].id).change();

        var marketID = ''//$("#selectMarketFilter").val();
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();

        //list(marketID, weekIni, weekEnd, statusID);
        loadListPrincipal();
    })

    $(document).on("click", "#btnPendingConfirmed", function () {
        $("#selectMarketFilter").selectpicker('deselectAll');
        $('#selectMarketFilter').selectpicker('refresh');
        var Modified = dataStatus.filter(item => item.id == 4)
        $('#selectStatusFilter').val(Modified[0].id).change();

        var marketID = ''//$("#selectMarketFilter").val();
        var weekIni = $('#cboSemanaIni').val();
        var weekEnd = $('#cboSemanaFin').val();
        var statusID = $('#selectStatusFilter').val();

        //list(marketID, weekIni, weekEnd, statusID);
        loadListPrincipal();
    })

    $(document).on("click", ".row-remove", function () {
        $(this).parents('tr').detach();
        calcTotal();
        btnValidation();
    });

  $(document).on("click", "#btnSearch", function () {
    sweet_alert_progressbar();
        loadListPrincipal();
    })

    $(document).on("click", "#btnEdit", function () {

        $('#btnEdit').addClass('display-none')
        $('#btnAdd').removeClass('display-none')

        $("#tblCatVar>tbody>tr").each(function (index, tr) {
            $(tr).removeClass("row-edit-sales")
        })

        var saleDetailID = $(txtSaleDetailID).val()
        var saleID = $("#lblSaleID").text();
        var varietyID = $('#selectVariety option:selected').val()
        var variety = $('#selectVariety option:selected').text()
        var categoryID = $('#selectCategory option:selected').val()
        var category = $('#selectCategory option:selected').text()
        var formatID = $('#selectFormat option:selected').val()
        var format = $('#selectFormat option:selected').text()
        var packageProductID = $('#selectTypeBox option:selected').val()
        var packageProduct = $('#selectTypeBox option:selected').text()
        var brandID = $('#selectBrand option:selected').val()
        var brand = $('#selectBrand option:selected').text()
        var presentationID = $('#selectPresentation option:selected').val()
        var presentation = $('#selectPresentation option:selected').text()
        var labelID = $('#selectLabel option:selected').val()
        var label = $('#selectLabel option:selected').text()
        var codePackID = $('#SelectSizeBox option:selected').val()
        var codePack = $('#SelectSizeBox option:selected').text()
        var boxesPerPallet = $('#txtBoxPallet').val()
        var quantity = $('#txtBoxes').val()
        var net = $('#txtNet').val()
        var pallets = $('#txtPallets').val()
        var price = $('#txtPrice').val()
        var paymentTermID = $('#selectPayment option:selected').val()
        var paymentTerm = $('#selectPayment option:selected').text()
        var conditionPaymentID = $('#selectPriceContition option:selected').val()
        var conditionPayment = $('#selectPriceContition option:selected').text()
        var incotermID = $('#selectIncoterm option:selected').val()
        var incoterm = $('#selectIncoterm option:selected').text()
        var addBoxes = $('#txtaddBoxes').val()
        var sizeID = $('#selectSize').val();
        var size = [];
        $.each(sizeID, function (ii, ee) {
            $.each(dataSize, function (i, e) {
                if (e.id == ee) {
                    size.push(e.name)
                }
            })
        })
        var codepack1 = $('#txtCodePack').val()
        var finalCustomerID = $('#SelectFinalCustomer option:selected').val()
        var finalCustomer = $('#SelectFinalCustomer option:selected').text()        

        var padlock = 0;
        var plu = 0;
        if ($('#chkpadlock').is(":checked")) {
            padlock = 1;
        }

        if ($('#chkplu').is(":checked")) {
            plu = 1;
        }

        var row = $(txtRow).val()

        //$(trEdit).find('td.saleDetailID').text(saleDetailID);
        $(trEdit).find('td.saleID').text(saleID);
        $(trEdit).find('td.varietyID').text(varietyID);
        $(trEdit).find('.variety').text(variety);
        $(trEdit).find('td.categoryID').text(categoryID);
        $(trEdit).find('td.category').text(category);
        $(trEdit).find('td.formatID').text(formatID);
        $(trEdit).find('td.format').text(format);
        $(trEdit).find('td.typeBoxID').text(packageProductID);
        $(trEdit).find('td.typeBox').text(packageProduct);
        $(trEdit).find('td.brandID').text(brandID);
        $(trEdit).find('td.brand').text(brand);
        $(trEdit).find('td.presentationID').text(presentationID);
        $(trEdit).find('.presentation').text(presentation);
        $(trEdit).find('td.labelID').text(labelID);
        $(trEdit).find('td.label').text(label);
        $(trEdit).find('td.sizeBoxID').text(codePackID);
        $(trEdit).find('td.sizeBox').text(codePack);
        $(trEdit).find('td.boxesPerPallet').text(boxesPerPallet);
        $(trEdit).find('td.txtBoxes').text(quantity);
        $(trEdit).find('td.txtNet').text(net);
        $(trEdit).find('td.addBoxes').text(addBoxes);

        $(trEdit).find('td.paymentID').text(paymentTermID);
        $(trEdit).find('.payment').text(paymentTerm);
        $(trEdit).find('td.priceContitionID').text(conditionPaymentID);
        $(trEdit).find('.priceContition').text(conditionPayment);
        $(trEdit).find('td.IncotermID').text(incotermID);
        $(trEdit).find('.Incoterm').text(incoterm);
        $(trEdit).find('td.sizeID').text(sizeID);
        $(trEdit).find('td.size').text(size);
        $(trEdit).find('td.codePack1').text(codepack1);
        $(trEdit).find('td.finalCustomerID').text(finalCustomerID);
        $(trEdit).find('.finalCustomer').text(finalCustomer);
        $(trEdit).find('td.padlock').text(padlock);
        $(trEdit).find('td.plu').text(plu);
        $(trEdit).find('td.txtPallets').text(pallets);
        $(trEdit).find('td.txtPrice').text(price);
        calcTotal();

        sweet_alert_success('Good!', 'Data Changed.');
                
        $(".collapse").collapse('toggle');
        //$('#Formulario').addClass('display-none');
    })

    $(document).on("click", ".row-edit", function () {
        //$(".collapse").collapse('toggle');
        $('#detalleGrapes').addClass('show')
        //$('.collapse').addClass('show')
        trEdit = $(this).closest('tr');
        $("#tblCatVar>tbody>tr").each(function (index, tr) {
            $(tr).removeClass("row-edit-sales")
        })

        $('details').attr('open', '');
        $('#btnAdd').addClass('display-none')
        $('#btnEdit').removeClass('display-none')

        $idSaleDetail = $(this).parents('tr').children().eq(1).text();
        var row = $(this).parent().parent().index();

        var e = $(this).parents('tr')
        $(e).addClass("row-edit-sales");

        var saleDetailID = $(e).find('td.saleDetailID').text();
        var SaleID = $(e).find('td.saleID').text();

        var varietyID = $(e).find('td.varietyID').text();
        var categoryID = $(e).find('td.categoryID').text();
        var formatID = $(e).find('td.formatID').text();
        var packageProductID = $(e).find('td.typeBoxID').text();

        var brandID = $(e).find('td.brandID').text();
        var presentationID = $(e).find('td.presentationID').text();
        var labelID = $(e).find('td.labelID').text();
        var codePackID = $(e).find('td.sizeBoxID').text();

        var boxesPerPallet = $(e).find('td.boxesPerPallet').text();
        var quantity = $(e).find('td.txtBoxes').text();
        var net = $(e).find('.txtNet').text();
        var pallets = $(e).find('td.txtPallets').text();
        var addBoxes = $(e).find('td.addBoxes').text();

        var price = $(e).find('td.txtPrice').text();
        var paymentTermID = $(e).find('td.paymentID').text();
        var conditionPaymentID = $(e).find('td.priceContitionID').text();
        var incotermID = $(e).find('td.IncotermID').text();

        var sizeID = $(e).find('td.sizeID').text();
        var codepack1 = $(e).find('td.codepack1').text();
        var finalCustomerID = $(e).find('td.finalCustomerID').text();

        var padlock = $(e).find('td.padlock').text();
        if (padlock == 1) {
            $('#chkpadlock').prop('checked', true);

        } else {
            $('#chkpadlock').prop('checked', false);
        }

        var plu = $(e).find('td.plu').text();
        if (plu == 1) {
            $('#chkplu').prop('checked', true);
        } else {
            $('#chkplu').prop('checked', false);
        }

        $(txtSaleDetailID).selectpicker('val', saleDetailID);

        $(selectVariety).selectpicker('val', varietyID).selectpicker('refresh');;

        $(selectCategory).selectpicker('val', categoryID);
        $(selectFormat).selectpicker('val', formatID);
        $(selectTypeBox).selectpicker('val', packageProductID);

        $(selectBrand).selectpicker('val', brandID);
        $(selectPresentation).selectpicker('val', presentationID)
        $(selectLabel).selectpicker('val', labelID);
        $(SelectSizeBox).selectpicker('val', codePackID);

        $(txtBoxPallet).selectpicker('val', boxesPerPallet);
        $(txtBoxes).val(quantity);
        $(txtNet).val(net);
        $(txtPallets).val(pallets);
        $(txtaddBoxes).selectpicker('val', addBoxes);

        $(txtPrice).selectpicker('val', price);
        $(selectPayment).selectpicker('val', paymentTermID);
        $(selectPriceContition).selectpicker('val', conditionPaymentID);
        $(selectIncoterm).selectpicker('val', incotermID);
        $(selectIncoterm).selectpicker('refresh');

        $(selectSize).selectpicker('val', sizeID.split(','))
        $(txtCodePack).selectpicker('val', codepack1);
        $(SelectFinalCustomer).selectpicker('val', finalCustomerID);

    });

    $('#btnSavePO').click(function (e) {
        if (obligatory()) {
            if (_statusID != 6) {
                SavePO();
            } else {
                sweet_alert_error('Error', 'You cannot modify the sales order, your shipping instruction has already been generated');
                $('#ModalSaleRequest').modal('hide');
            }
        }        

    });

    $('#btnSavePOModified').click(function (e) {
        if (obligatory()) {
            SavePOModified();
        }

    });

    $('#btnSaveReject').click(function (e) {
        SavePORejected();
    });

    $('#btnRejectPOModified').click(function (e) {
        $('#btnRejectPOModified').modal('toggle');
    });

    $('#selectMarket').change(function () {
        loadCombo([], 'selectCustomer', true);
        loadCombo([], 'SelectFinalCustomer', false);
        loadCombo([], 'selectDestination', true);
        loadComboConsignnee([], 'selectConsignee', true);
        loadCombo([], 'selectDestinationPort', true);
        //$('#selectCodepack').selectpicker('refresh');
        loadCombo(dataVia, 'selectVia', true);
        $('#selectVia').change()
        if (this.value == "") {
            return;
        }
        $('#selectVia').selectpicker('refresh');

        //List Customer
        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == this.value.trim());
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );
        loadCombo(data, 'selectCustomer', false);
        loadComboE(data, 'SelectFinalCustomer', true);
        $("#selectCustomer").selectpicker('refresh');
        $("#SelectFinalCustomer").selectpicker('refresh');

    });

    $('#selectCustomer').change(function () {
        loadCombo([], 'selectDestination', true);
        loadCombo([], 'selectDestinationPort', true);

        if (this.value == "") {
            return;
        }
        /************/
        if ($("#selectCustomer option:selected").val() > 0) {
            loadDataWow($("#selectCustomer option:selected").val())
            loadComboConsignnee([], 'selectConsignee', true);
            if (this.value == "") {
                return;
            }
        }
        AjaxGetPaymentTerm(this.value);

        //List Destination
        if ($dataCustomerFilter.length > 0) {

            $dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val());
            data = $dataDestinationFilter.map(
                obj => {
                    return {
                        "id": obj.destinationID,
                        "name": obj.destination
                    }
                }
            );
            loadCombo(data, 'selectDestination', true);
            $('#selectDestination').selectpicker('refresh');

        } else {
            loadCombo([], 'selectDestination', true);
            $('#selectDestination').selectpicker('refresh');
        }

    });

    $('#selectDestination').change(function () {
        $dataWowFilter = dataWow.filter(item => item.destinationID == this.value);
        data = $dataWowFilter.map(
            obj => {
                return {
                    "id": obj.wowID,
                    "name": obj.consignee
                }
            }
        );
        if (data.length > 0) {
            loadComboConsignnee(data, 'selectConsignee', true);
        } else {
            loadComboConsignnee([], 'selectConsignee', true);
        }

        $('#selectConsignee').selectpicker('refresh');
        let data2 = dataDestinationPort.filter(item => item.viaID == $("#selectVia option:selected").val() && item.destinationID == $("#selectDestination option:selected").val())
        if (data2.length > 0) {
            loadCombo(data2, 'selectDestinationPort', false);
        } else {
            loadCombo([], 'selectDestinationPort', true);
        }

    })

    $('#selectVia').change(function () {
        if (this.value == "") {
            loadCombo([], 'selectCodepack', true);
            $('#selectCodepack').change().selectpicker('refresh');
            //$('#selectCodepack').change();
            loadCombo([], 'selectDestinationPort', true);
            loadCombo([], 'selectIncoterm', true);
            return;
        }

        let data2 = dataDestinationPort.filter(item => item.viaID == $("#selectVia option:selected").val() && item.destinationID == $("#selectDestination option:selected").val())
        if (data2.length > 0) {
            loadCombo(data2, 'selectDestinationPort', false);
        } else {
            loadCombo([], 'selectDestinationPort', true);
        }

        let data3 = dataIncoterm.filter(item => item.viaID == $("#selectVia option:selected").val())
        if (data3.length > 0) {
            loadCombo(data3, 'selectIncoterm', true);
        } else {
            loadCombo([], 'selectIncoterm', true);
        }
        $('#selectIncoterm').selectpicker('refresh');
    });

    $(selectVariety).change(function () {
        createCodePack();
    })

    $('#selectCategory').change(function () {

        loadCombo([], 'selectBrand', true);

        loadCombo([], 'selectLabel', true);
        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#selectCategory option:selected").val().trim().length > 0) {
            //createCodePack();
            loadDataBrandByCategory($("#selectCategory option:selected").val().trim())
        }

        //List 
        if (dataBrand.length > 0) {
            loadCombo(dataBrand, 'selectBrand', false);
            loadCombo(dataLabel, 'selectLabel', false);
            $('#selectBrand').selectpicker('refresh');
            $('#selectLabel').selectpicker('refresh');

        } else {
            loadCombo([], 'selectBrand', true);
            loadCombo([], 'selectLabel', true);
        }
    })

    $(selectFormat).change(function () {
        createCodePack();
    })

    $(selectCategory).change(function () {
        createCodePack();
    })

    $(selectBrand).change(function () {
        createCodePack();
    })

    $(selectPresentation).change(function () {
        createCodePack();
    })

    $(selectLabel).change(function () {
        createCodePack();
    })

    $('#chkplu').change(function () {
        createCodePack();
    })    

    $('#txtBoxes').change(function () {

        var currentBoxes = $(this).val();        
        var boxPallet = parseInt($('#txtBoxPallet').val());
        var pallets = parseInt(currentBoxes) / boxPallet;
        var boxes = parseInt($(txtBoxes).val());
        var addBoxes = parseInt($('#txtaddBoxes').val());        

        if ($('#chkpadlock').is(":checked")) {
            boxes = boxes - addBoxes;
            //pallets = (boxes / boxPallet) + 1;
            pallets = pallets + 0.2;
        } else {            
            if (pallets % 1 == 0) {
                pallets = pallets;
                //alert("Es un numero entero");
            } else {
                //alert("decimal error");
                pallets = pallets;
                $(txtBoxes).val(0);
            }
        }
        
        if (boxes == 0) {
            $('#txtPallets').val(0);
        } else {            
            $('#txtPallets').val(pallets.toFixed(2));
        }

        var currentPrice = parseFloat($('#txtPrice').val());
        var totalPrice = (currentPrice * currentBoxes).toFixed(2);

        $('#txtTotalPrice').val(totalPrice);

        //obteniendo el peso segun el formato seleccionado
        var formatSelected = dataFormat.filter(item => item.id == $(selectFormat).val())
        //si encuentra un formato:
        if (formatSelected.length > 0) {
            //peso del formato
            var weightPerBox = parseFloat(formatSelected[0].weight)
            var boxes = parseInt($(txtBoxes).val())
            var totalNet = (weightPerBox * boxes).toFixed(2);
            $(txtNet).val(totalNet)
        }

    })

    $('#txtPrice').change(function () {
        $('#txtBoxes').change()
    })

    //$('#txtBoxPallet').change(function () {

    //})
    $('#txtPrice').val('0')
    $('#txtBoxes').val('0')
    $('#txtNet').val('0')
    $('#txtPaymentTerm').val('')
    $('#txtBoxPallet').val('0')
    $('#txtPallets').val('0')
    $('#txtaddBoxes').val('0')

    $('.totalPallets').html('0.00')
    $('.totalPrice').html('0.00')
    loadQuantityByStatus();
    loadDefaultValuesFilter();
    loadDefaultValues();

    $('#selectFormat').change(function () {
        //createCodePack();
        loadCombo([], 'SelectSizeBox', true);

        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#selectFormat option:selected").val().trim().length > 0) {

            if ($("#selectTypeBox option:selected").val().trim().length > 0) {
                var format = ($("#selectFormat option:selected").val().trim());
                var typeBox = ($("#selectTypeBox option:selected").val().trim());

                loadCodePack(format, typeBox);
            }
        }

        //List 
        if (dataCodePack2.length > 0) {
            loadCombo(dataCodePack2, 'SelectSizeBox', false);

            $('#SelectSizeBox').selectpicker('refresh');
            $('#SelectSizeBox').change();
            $('#txtBoxes').val(0)
            $('#txtNet').val(0)
            $(txtPallets).val(0)

        } else {
            loadComboSizeBox([], 'SelectSizeBox', false, "")//Indico el llenado del sizeBox
            $('#SelectSizeBox').selectpicker('refresh');
            $('#txtBoxPallet').val(0)
            $('#txtBoxes').val(0)
            $('#txtNet').val(0)
            $(txtPallets).val(0)

        }
        calculoPallets();

    })

    $('#selectTypeBox').change(function () {

        loadCombo([], 'SelectSizeBox', true);

        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#selectTypeBox option:selected").val().trim().length > 0) {

            var format = $("#selectFormat option:selected").val();

            if ($("#selectFormat option:selected").val().trim().length > 0) {
                var format = ($("#selectFormat option:selected").val().trim());
                var typeBox = ($("#selectTypeBox option:selected").val().trim());

                loadCodePack(format, typeBox);
            }

        }
        //List 
        if (dataCodePack2.length > 0) {

            createCodePack();
            loadCombo(dataCodePack2, 'SelectSizeBox', false);
            $('#SelectSizeBox').selectpicker('refresh');
            $('#SelectSizeBox').change();
            $('#txtBoxes').val(0)
            $('#txtNet').val(0)
            $(txtPallets).val(0)
        } else {
            //loadCombo([], 'SelectSizeBox', false);
            //loadComboSizeBox([], 'SelectSizeBox', true);
            loadComboSizeBox([], 'SelectSizeBox', false, "")
            $('#SelectSizeBox').selectpicker('refresh');
            $('#txtBoxPallet').val(0)
            $('#txtNet').val(0)
            $('#txtNet').val(0)
            $(txtPallets).val(0)
        }
    })

    $('#SelectSizeBox').change(function () {

        $('#txtBoxPallet').val("");

        //sí el data de Category está vacío retorna vacío
        if (this.value == "") {
            return;
        }
        //Cuando tengo seleccion del campo
        if ($("#SelectSizeBox option:selected").val().trim().length > 0) {

            $('#txtBoxPallet').val(dataCodePack2[0].boxesPerPallet);

            var boxPallet = $('#txtBoxPallet').val();
            $(txtBoxPallet).val(boxPallet);

            //if ($('#chkpadlock').is(":checked")) {
            //} else {
            //    $(txtBoxes).attr("step", boxPallet);
            //}
            
            $(txtBoxes).val(boxPallet)
            //$(txtBoxes).prop("disabled", false);

            var pallet = ($(txtBoxes) / $(txtBoxPallet));
            $(txtPallets).val(pallet);

        }
        else {
            $('#txtBoxPallet').val(0)
            $(txtPallets).val(0);
        }
    })

    $('#btnAdd').click(function (e) {
        if (obligatoryRow()) {
            var form = $("#Formulario");
            form.addClass('was-validated');
            if (form[0].checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                sweet_alert_warning('Warning', "You must complete the highlighted fields.");
            } else {
                addtblCatVar();
            }
        }
    });

    if (_cropID == 'UVA') {
        $("#chkpadlock").attr("disabled", "disabled");
        $("#chkplu").attr("disabled", "disabled");    
    } else {
        $("#chkpadlock").removeAttr("disabled");
        $("#chkplu").removeAttr("disabled");
    }
});

function loadDetailModified() {

    //$('#btnEdit').removeClass('display-none')

    $("table#tblPOModified tbody").empty()
    var api = "/Sales/GetSalesDetailModifiedPO?saleID=" + $("#lblSaleID").text();
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data.length > 0) {
                $('#btnSavePO').addClass('display-none')
                var dataList = JSON.parse(data);
                var content = "";
                $.each(dataList, function (index, row) {

                    content += "<tr>";
                    content += '<td style="min-width:150px; max-width:150px;font-size: 13px;" >' + row.variety + '</td>'
                    content += '<td style="min-width:150px; max-width:150px;font-size: 13px;" >' + row.category + '</td>'
                    content += '<td style="min-width:150px; max-width:150px;font-size: 13px;" >' + row.codepack + '</td>'
                    content += '<td style="min-width:150px; max-width:150px;font-size: 13px;" >' + row.brand + '</td>'
                    content += '<td style="min-width:130px; max-width:130px;font-size: 13px;" >' + row.boxes + '</td>'
                    content += '<td style="min-width:130px; max-width:130px;font-size: 13px;" >' + row.pallets + '</td>'

                    content += "</tr>";
                })

                $("table#tblPOModified tbody").empty().append(content);

                $("table#tblSales>tbody>tr>td.statusID").each(function (i, e) {
                    var saleID2 = $(e).closest('tr').find('td.saleID').text();

                    if (_statuSale == saleID2) {
                        if (parseInt($(e).closest('tr').find('td.statusID').text()) == '4') {

                            $(btnRejectPOModified).removeClass();
                            $(btnSavePOModified).removeClass();
                            $(btnSavePO).removeClass();
                            $('#btnSavePOModified').addClass('display-none')
                            $('#btnRejectPOModified').addClass('display-none')
                            $(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');

                        }

                        if (parseInt($(e).closest('tr').find('td.statusID').text()) == '8') {

                            $(btnRejectPOModified).removeClass();
                            $(btnSavePOModified).removeClass();
                            $(btnSavePO).removeClass();
                            $('#btnSavePO').addClass('display-none')
                            $(btnRejectPOModified).addClass('btn btn-danger btn-sm btn-icon-split transact pull-right');
                            $(btnSavePOModified).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');

                        } else {
                            $('#btnSavePO').removeClass();
                            $('#btnSavePO').addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
                            $(btnRejectPOModified).removeClass();
                            $(btnSavePOModified).removeClass();
                            $(btnRejectPOModified).addClass('display-none')
                            $(btnSavePOModified).addClass('display-none')
                        }

                    }

                })

            }
        }
    })
}

function loadListPrincipal() {

    var marketID = $('#selectMarketFilter').val();
    var weekEnd = $('#cboSemanaFin').val();
    var weekIni = $('#cboSemanaIni').val();
    var statusID = $('#selectStatusFilter').val();
    if (statusID == null || statusID == 'All') {
        statusID = 0;
    }

    var opt = 'lic';
    var val = marketID;
    var sUrlApi = "/Sales/GetListByCampaignID?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + statusID +
        "&campaignID=" + localStorage.campaignID;
    var html = "";
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataList = JSON.parse(data);
                if (dataList == null || dataList.length == 0) {
                    sweet_alert_info('Information', 'Nothing to show according to filters. Try again.');
                } else {
                    $.each(dataList, function (index, row) {
                        if (row.statusId == 1) {
                            html += '<tr  class="table-warning">';
                            color = 'badge-warning';
                        }
                        if (row.statusId == 2) {
                            html += '<tr  class="table-danger">';
                            color = 'badge-danger';
                        }
                        if (row.statusId == 3) {
                            html += '<tr  class="table-ligth">';
                            color = 'badge-ligth';
                        }
                        if (row.statusId == 4) {
                            html += '<tr  class="table-warning">';
                            color = 'badge-warning';
                        }
                        if (row.statusId == 5) {
                            html += '<tr  class="table-primary">';
                            color = 'badge-primary';
                        }
                        if (row.statusId == 6) {
                            html += '<tr  class="table-success">';
                            color = 'badge-success';
                        }
                        if (row.statusId == 7) {//?
                            html += '<tr  class="table-dark">';
                            color = 'badge-dark';
                        }
                        if (row.statusId == 8) {
                            html += '<tr  class="table-secondary">';
                            color = 'badge-secondary';
                        }
                        html += "<td class='saleID' style='display:none'>" + row.saleID + " </td>";
                        html += "<td class='sticky' style='text-align:center;max-width: 80px; min-width: 80px !important;'>";
                        html += "<button class='btn  btn-sm btn-primary' style='' onclick='openModal(" + row.saleID + ")' data-toggle='modal' data-target='#ModalSaleRequest'><span class='icon '><i class='fas fa-edit fa-sm'></span></i></button> ";
                        html += "<button class='btn btn-sm btn-danger' style='' onclick='deletesr(this)' ><span class='icon '><i class='fas fa-trash-alt fa-sm'></i></span></button> ";
                        html += "</td>";
                        html += "<td class='' hidden>" + row.referenceNumber + "</td>";
                        html += "<td style='max-width:120px;min-width:120px; class='sticky2'>" + row.pO_NUMBER + "</td>";
                        html += "<td style='max-width:60px;min-width:60px;' class='sticky3'>" + row.week + "</td>";                        
                        html += "<td style='max-width:140px;min-width:140px;' class=''>" + row.varety + "</td>";
                        html += "<td style='max-width:60px;min-width:60px;' class=''>" + row.brand + "</td>";
                        html += "<td style='max-width:50px;min-width:50px;' class=''>" + row.priority + "</td>";
                        html += "<td style='max-width:200px;min-width:200px; class=''><textarea id='customer' class='form-control form-control-sm' rows='1'>" + row.customer + "</textarea> </td>";
                        html += "<td style='max-width:110px;min-width:110px; class=''>" + row.market + "</td>";
                        html += "<td style='max-width:120px;min-width:120px;' class=''><textarea id='destination' class='form-control form-control-sm' rows='1'>" + row.destination + "</textarea></td>";
                        html += "<td style='max-width:40px;min-width:40px;' class=''>" + row.via + "</td>";
                        html += "<td style='max-width:50px;min-width:50px;' class=''>" + row.quantity + "</td>";
                        html += "<td class='statusID' hidden >" + row.statusId + " </td>";
                        html += '<td style="font-size:14px;max-width:120px;min-width:120px;text-align:center;" ><div id="status" class="badge ' + color + ' badge-pill">' + row.status + '</div></td>';
                        html += "<td style='max-width:150px;min-width:150px; class=''><textarea id='codepack2' class='form-control form-control-sm' rows='1'>" + row.codepack2 + "</textarea></td> ";
                        html += "<td style='max-width:60px;min-width:60px;' class=''>" + row.incoterm + "</td>";
                        html += "<td style='max-width:80px;min-width:80px;' class=''>" + row.user + "</td>";
                        html += "</tr>";
                    })
                    $("table#tblSales").DataTable().destroy();

                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {
                //sweet_alert_progressbar();
                $("table#tblSales tbody").empty().append(html);
                $("#tblSales").DataTable({
                    "paging": true,
                    "ordering": false,
                    "info": true,
                    "responsive": true,
                    //"destroy": true,
                    dom: 'Bfrtip',
                    //select: true,
                    lengthMenu: [
                        [10, 25, 50, -1],
                        ['10 rows', '25 rows', '50 rows', 'All']
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
                            exportOptions: {
                                columns: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16]
                            }
                        },
                        'pageLength'
                    ],
                });
            }         
        },
    });
  loadQuantityByStatus();
  sweet_alert_progressbar_cerrar();
}

//$(document).ready(function () {
//    // Setup - add a text input to each footer cell
//    $('#tblSales thead tr').clone(true).appendTo('#tblSales thead');
//    $('#tblSales thead tr:eq(1) td').each(function (i) {
//        if (i > 0) {
//            var title = $(this).text();
//            $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');
//        } else {
//            $(this).html(' ');
//        }
//        $('input', this).on('keyup change', function () {
//            if (table.column(i).search() !== this.value) {
//                table
//                    .column(i)
//                    .search(this.value)
//                    .draw();
//            }
//        });
//    });

//});

function loadCodePack(format, typeBox) {
    var opt = 'pcr'

    $.ajax({
        type: 'GET',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/CodepackByFormatIDPackageProductID?opt=" + opt + "&viaid=" + format + "&cropID=" + typeBox,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataCodePack2 = dataJson.map(
                    obj => {
                        return {
                            "id": obj.codePackID,
                            "name": obj.codePack,
                            "boxPerContainer": obj.boxPerContainer,
                            "boxesPerPallet": obj.boxesPerPallet
                        }
                    }
                );
            }
        }
    });

}

function loadQuantityByStatus() {
    var marketID = $(selectMarketFilter).val();
    var weekIni = $(cboSemanaIni).val();
    var weekEnds = $(cboSemanaFin).val();
    $.ajax({
        type: "GET",
        url: "/Sales/GetMountByStatus?campaignID=" + localStorage.campaignID,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {
                        return {
                            "id": obj.statusID,
                            "name": obj.status,
                            "quantity": obj.quantity
                        }
                    }
                );

                var x1 = dataStatus.filter(item => item.id == 6)[0].quantity;
                var x2 = dataStatus.filter(item => item.id == 5)[0].quantity;
                var x3 = dataStatus.filter(item => item.id == 8)[0].quantity;
                var x4 = dataStatus.filter(item => item.id == 4)[0].quantity;
                //var Rejected = dataStatus.filter(item => item.id == 2)
                document.getElementById("Confirmed").innerHTML = x2;
                document.getElementById("Loaded").innerHTML = x1;
                document.getElementById("Modified").innerHTML = x3;
                document.getElementById("PendingConfirmed").innerHTML = x4;

            }
        }
    });
}

function loadComboConsignnee(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--To be Confirmed--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
    $('#' + control).selectpicker('refresh');

}

function AjaxGetPaymentTerm(customerID) {
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - begin
    var opt = "CUS";

    $.ajax({
        type: 'GET',
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: "/Sales/ListPaymentTermByCustomerMC?opt=" + opt + "&customerID=" + customerID + "&cropiD=" + localStorage.cropID,
        //url: "/Sales/ListPaymentTermByCustomer?customerID=" + customerID + "&cropiD=" + "UVA",
        async: true,
        success: function (data) {
            //var dataList = JSON.parse(data);//data;

            if (data.length > 0) {
                var dataList = JSON.parse(data);
                dataJson = dataList.map(
                    obj => {
                        return {
                            "id": obj.paymentTermID,
                            "name": obj.description
                        }
                    }
                );
            }

            loadCombo(dataJson, 'selectPayment', false)
            $(selectPayment).selectpicker('refresh');
        }
    });
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - end
}

function loadDataWeek() {

    $.ajax({
        type: "GET",
        url: "/Sales/ListWeekBySeason?idSeason=" + localStorage.campaignID,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    //fnloadWeekByCampaign(localStorage.campaignID, fnsuccessListWeekByCampaign);
}

var fnsuccessListWeekByCampaign = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataWeek = dataJson.map(
            obj => {
                return {
                    "id": obj.projectedWeekID,
                    "name": obj.description
                }
            }
        );
    }
}

function loadDataDestination(marketID) {
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - begin
    var opt = "wcu";
    var id = "";
    var mar = "";
    var via = "";

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListDestinationAllWithCustomerMC?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
        //url: "/CommercialPlan/ListDestinationAllWithCustomer",
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            dataDestinationCustomer = JSON.parse(data);
        }
    });
}

function loadDataWow(customerID) {
    $.ajax({
        type: "GET",
        url: "/Sales/ListWowByCustomerCrop?customerID=" + customerID + "&cropID=" + localStorage.cropID,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data.length > 0) {
                dataWow = JSON.parse(data);
            }
        }
    });
}
//function loadDataDestinationPort() {
//	$.ajax({
//		type: "GET",
//		url: "/Sales/GetAllPort",
//		async: false,
//		success: function (data) {

//			if (data.length > 0) {
//				var dataJson = JSON.parse(data);
//				dataDestinationPort = dataJson.map(
//					obj => {
//						return {
//							"id": obj.destinationPortID,
//							"name": obj.port,
//							"viaID": obj.viaID,
//							"destinationID": obj.destinationID
//						}
//					}
//				);
//			}
//		}
//	});

//}
function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadComboE(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''> </option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadComboSizeBox(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='0'>" + textFirstElement + "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadDefaultValuesFilter() {

    loadComboFilter(dataMarketUser, 'selectMarketFilter', true, '[ALL]');
    $('#selectMarketFilter').selectpicker('refresh');

    loadComboFilter(dataWeek, 'cboSemanaIni', false, '[ALL]');
    $('#cboSemanaIni').val(dataWeek[0].id).change();
    $('#cboSemanaIni').selectpicker('refresh');

    loadComboFilter(dataWeek, 'cboSemanaFin', false, '[ALL]');
    //$('#cboSemanaFin').val(dataAllWeek[dataAllWeek.length - 1].id).change();
    $('#cboSemanaFin').val(dataWeek[0].id).change();
    $('#cboSemanaFin').selectpicker('refresh');

    loadComboFilter(dataStatus, 'selectStatusFilter', true, '[ALL]');
    //$('#selectStatusFilter').val(dataStatus[0].id).change();
    $('#selectStatusFilter').prop('selectedIndex', 0);
    $('#selectStatusFilter').selectpicker('refresh');

    loadListPrincipal();
}

function loadDefaultValues() {

    loadCombo(dataMarket, 'selectMarket', true);
    $('#selectMarket').prop("disabled", false);
    $('#selectMarket').selectpicker('refresh');

    loadCombo([], 'selectCustomer', true);
    $('#selectCustomer').prop("disabled", false);
    $('#selectCustomer').selectpicker('refresh');

    loadCombo(dataWeek, 'selectWeek', true);
    $('#selectWeek').selectpicker('refresh');

    loadComboConsignnee([], 'selectConsignee', true);
    $('#selectConsignee').selectpicker('refresh');

    loadCombo([], 'selectDestination', true);
    $('#selectDestination').selectpicker('refresh');

    loadCombo([], 'selectVia', true);
    $('#selectVia').selectpicker('refresh');

    loadCombo(dataConditionPayment, 'selectPriceContition', false);
    $('#selectPriceContition').selectpicker('refresh');

    loadCombo([], 'selectIncoterm', false);
    $('#selectIncoterm').selectpicker('refresh');

    loadCombo(dataVariety, 'selectVariety', false);
    $('#selectVariety').prop("disabled", false);
    $('#selectVariety').selectpicker('refresh');

    loadCombo(dataFormat, 'selectFormat', false);
    $('#selectFormat').prop("disabled", false);
    $('#selectFormat').selectpicker('refresh');
    $('#selectFormat').change();

    loadCombo(dataTypeBox, 'selectTypeBox', false);
    $('#selectTypeBox').prop("disabled", false);
    $('#selectTypeBox').selectpicker('refresh');
    $('#selectTypeBox').change();

    loadCombo(dataCategory, 'selectCategory', false);
    $('#selectCategory').prop("disabled", false);
    $('#selectCategory').selectpicker('refresh');
    $('#selectCategory').change();

    loadCombo(dataPresentation, 'selectPresentation', false);
    $('#selectPresentation').prop("disabled", false);
    $('#selectPresentation').selectpicker('refresh');

    loadCombo(dataSize, 'selectSize', false);
    $('#selectSize').prop("disabled", false);
    $('#selectSize').selectpicker('refresh');

    loadComboSizeBox([], 'SelectSizeBox', false, 0)
    $('#SelectSizeBox').selectpicker('refresh');

    $('#txtPrice').val('0')
    $(txtSaleDetailID).val(0);

    $('#txtPrice').val('0')
    $('#txtBoxes').val('0')
    $('#txtNet').val('0')
    $('#txtPaymentTerm').val('')
    $('#txtBoxPallet').val('0')
    $('#txtaddBoxes').val('0')

    $('.totalPallets').html('0.00')
    $('.totalPrice').html('0.00')

    $(txtSaleRequest).val('')
    $(txtSalesOrder).val('')

    $(txtStatus).val('')
    $(txtPO).val('')
    $('#txtPriority').val('0')

    $('#RejectionComments').text('');
    $('#txtUserRejection').val('');
    $('#txtDateRejection').val('');

    //$('#selectDestination').prop("disabled", false);
    $('#selectVia').prop("disabled", false);
    $('#selectCodepack').prop("disabled", false);
    $('#selectConsignee').prop("disabled", false);

    $('#selectDestinationPort').prop("disabled", false);

    //$(selectIncoterm).selectpicker('val', '')
    $(selectPriceContition).selectpicker('val', '')
    $(txtSaleDetailID).val(0);
    $(txtRow).val(0);

    loadCombo([], 'selectPayment', false)
    $(selectPayment).selectpicker('refresh');

}

function loadDataBrandByCategory(categoryID) {

    var cropID = localStorage.cropID
    var opt = "cat";
    $.ajax({
        type: "GET",
        url: "/Sales/BrandGetByCateID?opt=" + opt + "&cropID=" + categoryID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataBrand = dataJson.map(
                    obj => {
                        return {
                            "id": obj.brandID,
                            "name": obj.brand,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

    opt = "lbl";
    $.ajax({
        type: "GET",
        url: "/Sales/GetLabelByCropIDMC?opt=" + opt + "&cropID=" + cropID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataLabel = dataJson.map(
                    obj => {
                        return {
                            "id": obj.labelID,
                            "name": obj.label,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });
}

function LoadData() {
    loadDataWeek();
    $.ajax({
        type: "GET",
        url: "/Sales/GetIncotermMC?opt=sal&cropID=" + localStorage.cropID + "&viaID=0",
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataIncoterm = dataJson.map(
                    obj => {
                        return {
                            "id": obj.incotermID,
                            "name": obj.name,
                            "viaID": obj.viaID
                        }
                    }
                );
            }
        }
    });

    _campaignID = (localStorage.campaignID);
    var loadCropID = (localStorage.cropID);

    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - begin
    var opt = "nwk";
    var val = "";
    var weekIni = 0;
    var weekEnd = 0;
    var userID = localStorage.userID;
    /*
    $.ajax({
        type: "GET",
        //url: "/Sales/GetNextWeek",
        url: "/Sales/GetNextWeekMC?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + _userID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                NextWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });
    */
    fnloadNextWeek(weekIni, weekEnd, _userID, successloadNextWeek);

    /*
    opt = "act";
    $.ajax({
        type: "GET",
        url: "/Sales/GetNextWeekMC?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + _userID,
        //url: "/Sales/GetCurrentWeek",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                ActualyWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });
    */
    fnloadActualyWeek(weekIni, weekEnd, _userID, successloadActualyWeek);

    /*
    opt = "LWI";
    $.ajax({
        type: "GET",
        url: "/Sales/GetNextWeekMC?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + _userID,
        //url: "/Sales/GetAllWeek",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataAllWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });
    */
    fnloadAllWeek(weekIni, weekEnd, _userID, localStorage.campaignID, successloadAllWeek);

    var campaignID = localStorage.campaignID;
    val = campaignID;
    opt = "STC";

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/GetNextWeekMC?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + _userID,
        //url: "/Sales/GetStatusByCampaign?campaignID=" + campaignID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataStatus = dataJson.map(
                    obj => {

                        return {
                            "id": obj.statusID,
                            "name": obj.status,
                            "quantity": obj.quantity
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataStatus(localStorage.campaignID, weekIni, weekEnd, _userID);
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - end

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/ListMarket",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataMarket = dataJson.map(
                    obj => {
                        return {
                            "id": obj.marketID,
                            "name": obj.name
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataMarket(successloadDataMarket);

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/ListAllWithMarket",
        async: false,
        success: function (data) {
            dataCustomerMarket = JSON.parse(data);
        }
    });
    */
    fnloadDataCustomerMarket(successloadDataCustomerMarket);

    //$.ajax({
    //	type: "GET",
    //	url: "/Sales/GetAllPort",
    //	async: false,
    //	success: function (data) {
    //		if (data.length > 0) {
    //			var dataJson = JSON.parse(data);
    //			dataDestinationPort = dataJson.map(
    //				obj => {
    //					return {
    //						"id": obj.destinationPortID,
    //						"name": obj.port,
    //						"viaID": obj.viaID,
    //						"destinationID": obj.destinationID
    //					}
    //				}
    //			);
    //		}
    //	},
    //});
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - begin

    var opt = "wcu";
    var id = "";
    var mar = "";
    var via = "";

    /*
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListDestinationAllWithCustomerMC?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
        //url: "/CommercialPlan/ListDestinationAllWithCustomer",
        async: false,
        success: function (data) {
            dataDestinationCustomer = JSON.parse(data);
        }
    });
    */
    fnloadDataDestinationCustomer(successloadDataDestinationCustomer);

    opt = "all";
    id = "";
    /*
    $.ajax({
        type: "GET",
        url: "/Sales/ListViaMC?opt=" + opt + "&id=" + id,
        //url: "/Sales/ListVia" 
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataVia = dataJson.map(
                    obj => {
                        return {
                            "id": obj.viaID,
                            "name": obj.name
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataVia(successsloadDataVia);

    opt = "all";
    id = "";
    /*
    $.ajax({
        type: "GET",
        url: "/Sales/ListConditionPaymentMC?opt=" + opt + "&id=" + id,
        //url: "/Sales/ListConditionPayment",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataConditionPayment = dataJson.map(
                    obj => {
                        return {
                            "id": obj.conditionPaymentID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataConditionPayment(successloadDataConditionPayment);
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - end

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/VarietyGetByCrop?idCrop=" + loadCropID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataVariety = dataJson.map(
                    obj => {
                        return {
                            "id": obj.varietyID,
                            "name": obj.name,
                            "abbreviation": obj.abbreviation

                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataVariety(loadCropID, successloadDataVariety);

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/GetFormatByCrop?cropID=" + loadCropID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataFormat = dataJson.map(
                    obj => {
                        return {
                            "id": obj.formatID,
                            "abreviation": obj.format,
                            "name": obj.format,
                            "weight": obj.weight
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataFormat(loadCropID, successloadDataFormat);

    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - begin

    opt = "pkc";
    id = "";
    /*
    $.ajax({
        type: "GET",
        url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
        //url: "/Sales/GetPackageProductAll",
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataTypeBox = dataJson.map(
                    obj => {
                        return {
                            "id": obj.packageProductID,
                            "name": obj.packageProduct,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataTypeBox(successloadDataTypeBox);
    //<<<<<<<<<<<<<<<<<<<<<<<<<KAC - end

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/CategoryGetByCrop?cropID=" + loadCropID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataCategory = dataJson.map(
                    obj => {
                        return {
                            "id": obj.categoryID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataCategory(loadCropID, successloadDataCategory);

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/GetPresentByCrop?cropID=" + loadCropID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataPresentation = dataJson.map(
                    obj => {
                        return {
                            "id": obj.presentationID,
                            "name": obj.description,
                            "presentation": obj.presentation
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataPresentation(loadCropID, successloadDataPresentation);

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/ListSizeGetByCrop?cropID=" + loadCropID,
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataSize = dataJson.map(
                    obj => {
                        return {
                            "id": obj.sizeID,
                            "name": obj.sizeID
                        }
                    }
                );
            }
        }
    });
    */
    fnloadDataSize(loadCropID, successloadDataSize);

    /*
    $.ajax({
        type: "GET",
        url: "/Sales/GetMarketByUserID?userID=" + _userID,
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataMarketUser = dataJson.map(
                    obj => {
                        return {
                            "id": obj.marketID,
                            "name": obj.market
                        }
                    }
                );
            }
        }
    });
    */
    fnGetMarketByUserID(_userID, successloadDataMarketUser);
}

var successloadNextWeek = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        NextWeek = dataJson.map(
            obj => {
                return {
                    "id": obj.projectedWeekID,
                    "name": obj.description
                }
            }
        );
    }
}

var successloadActualyWeek = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        ActualyWeek = dataJson.map(
            obj => {
                return {
                    "id": obj.projectedWeekID,
                    "name": obj.description
                }
            }
        );
    }
}

var successloadAllWeek = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataAllWeek = dataJson.map(
            obj => {
                return {
                    "id": obj.projectedWeekID,
                    "name": obj.description
                }
            }
        );
    }
}

var successloadDataMarket = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataMarket = dataJson.map(
            obj => {
                return {
                    "id": obj.marketID,
                    "name": obj.name
                }
            }
        );
    }
}

var successloadDataCustomerMarket = function (data) {
    dataCustomerMarket = JSON.parse(data);
}

var successloadDataDestinationCustomer = function (data) {
    dataDestinationCustomer = JSON.parse(data);
}

var successsloadDataVia = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataVia = dataJson.map(
            obj => {
                return {
                    "id": obj.viaID,
                    "name": obj.name
                }
            }
        );
    }
}

var successloadDataConditionPayment = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataConditionPayment = dataJson.map(
            obj => {
                return {
                    "id": obj.conditionPaymentID,
                    "name": obj.description
                }
            }
        );
    }
}

var successloadDataVariety = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        dataVariety = dataJson.map(
            obj => {
                return {
                    "id": obj.varietyID,
                    "name": obj.name,
                    "abbreviation": obj.abbreviation

                }
            }
        );
    }
}

var successloadDataFormat = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        //console.log("format");
        //console.log(data);
        dataFormat = dataJson.map(
            obj => {
                return {
                    "id": obj.formatID,
                    "abreviation": obj.format,
                    "name": obj.format,
                    "weight": obj.weight
                }
            }
        );
    }
}

var successloadDataTypeBox = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        dataTypeBox = dataJson.map(
            obj => {
                return {
                    "id": obj.packageProductID,
                    "name": obj.packageProduct,
                    "abbreviation": obj.abbreviation
                }
            }
        );
    }
}

var successloadDataCategory = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        dataCategory = dataJson.map(
            obj => {
                return {
                    "id": obj.categoryID,
                    "name": obj.description
                }
            }
        );
    }
}

var successloadDataPresentation = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        console.log("DataPresentation");
        console.log(dataJson)
        dataPresentation = dataJson.map(
            obj => {
                return {
                    "id": obj.presentationID,
                    "name": obj.description,
                    "presentation": obj.presentation
                }
            }
        );
    }
}

var successloadDataSize = function (data) {
    if (data.length > 0) {
        var dataJson = JSON.parse(data);
        dataSize = dataJson.map(
            obj => {
                return {
                    "id": obj.sizeID,
                    "name": obj.sizeID
                }
            }
        );
    }
}

var successloadDataMarketUser = function (data) {
    var dataJson = JSON.parse(data);
    if (data.length > 0) {
        dataMarketUser = dataJson.map(
            obj => {
                return {
                    "id": obj.marketID,
                    "name": obj.market
                }
            }
        );
    }
}

function createCodePack() {
    if ($("#selectVariety").val() == null) {
        return;
    } if ($("#selectFormat").val() == null) {
        return;
    } if ($("#selectTypeBox").val() == null) {
        return;
    }
    if ($("#selectBrand").val() == null) {
        return;
    } if ($("#selectLabel").val() == null) {
        return;
    } if ($("#selectPresentation").val() == null) {
        return;
    }

    varietyID = $("#selectVariety option:selected").val();
    formatID = $("#selectFormat option:selected").val();
    typeBoxID = $("#selectTypeBox option:selected").val();
    brandID = $("#selectBrand option:selected").val();
    presentationID = $("#selectPresentation option:selected").val();
    labelID = $("#selectLabel option:selected").val();

    
    var varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
    var formatCode = dataFormat.filter(item => item.id == formatID)[0].abreviation
    var typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation
    var brandCode = dataBrand.filter(item => item.id == brandID)[0].abbreviation
    var presentationCode = dataPresentation.filter(item => item.id == presentationID)[0].presentation
    var labelCode = dataLabel.filter(item => item.id == labelID)[0].abbreviation

    if ($('#chkplu').is(":checked")) {
        var labelCodeP = 'P';
    }
    else {
        var labelCodeP = 'G';
    }

    switch (_cropID.toLowerCase()) {
        case 'cit':
            var codePack = varietyCode.trim() + '_' + formatCode + typeBoxCode.slice(0, 1) + brandCode.trim() + '_' + (typeBoxCode.slice(0, 1) + brandCode.trim() + labelCodeP).slice(0, 3);
            break;
        default:
            var codePack = varietyCode.trim() + '_' + formatCode + typeBoxCode.slice(0, 1) + brandCode.trim() + '_ ' + (presentationCode + labelCode).slice(0, 3);
    } 
    
    $(txtCodePack).val(codePack);
}

function addtblCatVar() {

    var sailDetailID = $(txtSaleDetailID).val()
    var saleID = $("#lblSaleID").text();
    var varietyID = $('#selectVariety option:selected').val()
    var variety = $('#selectVariety option:selected').text()
    var categoryID = $('#selectCategory option:selected').val()
    var category = $('#selectCategory option:selected').text()
    var formatID = $('#selectFormat option:selected').val()
    var format = $('#selectFormat option:selected').text()
    var packageProductID = $('#selectTypeBox option:selected').val()
    var packageProduct = $('#selectTypeBox option:selected').text()
    var brandID = $('#selectBrand option:selected').val()
    var brand = $('#selectBrand option:selected').text()
    var presentationID = $('#selectPresentation option:selected').val()
    var presentation = $('#selectPresentation option:selected').text()
    var labelID = $('#selectLabel option:selected').val()
    var label = $('#selectLabel option:selected').text()
    var codePackID = $('#SelectSizeBox option:selected').val()
    var sizeBox = $('#SelectSizeBox option:selected').text()
    var boxPallet = $('#txtBoxPallet').val()
    var txtBoxes = $('#txtBoxes').val()
    var txtNet = $('#txtNet').val()
    var pallets = $('#txtPallets').val()
    var txtPrice = $('#txtPrice').val()
    var paymentID = $('#selectPayment option:selected').val()
    var payment = $('#selectPayment option:selected').text()
    var priceContitionID = $('#selectPriceContition option:selected').val()
    var priceContition = $('#selectPriceContition option:selected').text()
    var IncotermID = $('#selectIncoterm option:selected').val()
    var Incoterm = $('#selectIncoterm option:selected').text()
    var addBoxes = $('#txtaddBoxes').val()
    //var sizeID = $('#selectSize option:selected').val()
    //var size = $('#selectSize option:selected').text()

    var sizeID = $("#selectSize").val();
    var size = [];
    $.each(sizeID, function (ii, ee) {
        $.each(dataSize, function (i, e) {
            if (e.id == ee) {
                size.push(e.name)
            }
        })
    })

    var codepack1 = $('#txtCodePack').val()
    var finalCustomerID = $('#SelectFinalCustomer option:selected').val()
    var finalCustomer = $('#SelectFinalCustomer option:selected').text()

    var padlock = 0;
    var plu = 0;
    if ($('#chkpadlock').is(":checked")) {
        padlock = 1;
    }

    if ($('#chkplu').is(":checked")) {
        plu = 1;
    }

    var content = "";
    content += '<tr>';
    content += '<td style="text-align:center; min-width:100px">';
    content += ' <span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td >';
    content += '<td class=" saleDetailID" hidden>' + sailDetailID + '</td > ';
    content += '<td class=" saleID" hidden>' + saleID + '</td > ';
    content += '<td class=" varietyID" hidden>' + varietyID + "</td >  ";
    content += '<td class=" " ><textarea rows="1" class="variety form-control form-control-sm">' + variety + "</textarea></td >";
    content += '<td class=" categoryID" hidden>' + categoryID + "</td > 	";
    content += '<td class=" category">' + category + "</td > 	";
    content += '<td class=" formatID" hidden>' + formatID + "</td > 	";
    content += '<td class=" format" style="tex-align:right">' + format + "</td > 	";
    content += '<td class=" typeBoxID" hidden>' + packageProductID + "</td > 	";
    content += '<td class=" typeBox" style="tex-align:right">' + packageProduct + "</td > 	";
    content += '<td class=" brandID " hidden>' + brandID + "</td > 	";
    content += '<td class=" brand">' + brand + "</td > 	";
    content += '<td class=" presentationID " hidden>' + presentationID + "</td > 	";
    content += '<td class="">' + presentation + "</td > 	";
    content += '<td class=" labelID" hidden>' + labelID + "</td > 	";
    content += '<td class="label">' + label + "</td>";
    content += '<td class=" sizeBoxID" hidden>' + codePackID + "</td > 	";
    content += '<td class=" sizeBox " style="tex-align:right" hidden>' + sizeBox + "</td > 	";
    content += '<td class=" boxPallet " style="tex-align:right"hidden>' + boxPallet + "</td > 	";
    content += '<td class=" txtBoxes">' + txtBoxes + "</td > 	";
    content += '<td class="txtNet" style="tex-align:right">' + txtNet + "</td>";
    content += '<td class="codePack1">' + codepack1 + "</td>";
    content += '<td class=" IncotermID " hidden>' + IncotermID + "</td > 	";
    content += '<td class=" ">' + Incoterm + "</td > 	";
    content += '<td class=" priceContitionID" hidden>' + priceContitionID + "</td > 	";
    content += '<td class="">' + priceContition + "</td>";
    content += '<td class="paymentID" hidden>' + paymentID + "</td > 	";
    content += '<td class=" "><textarea rows="1" class="payment form-control form-control-sm">' + payment + "</textarea></td >";    
    content += '<td class=" sizeID" hidden>' + sizeID + "</td > 	";
    content += '<td class=" size " hidden>' + sizeID + "</td > 	";    
    content += '<td class=" finalCustomerID   " hidden>' + finalCustomerID + "</td > 	";
    content += '<td class=" "> <textarea rows="1" class="finalCustomer form-control form-control-sm">' + finalCustomer + "</textarea></td >";
    content += '<td class=" txtPallets" style="tex-align:right" >' + pallets + "</td > 	";
    content += '<td class=" txtPrice" style="text-align:right;" > ' + txtPrice + "</td >";
    content += '<td class="padlock" style="text-align:right;" hidden> ' + padlock + "</td >";
    content += '<td class="plu" style="text-align:right;" hidden> ' + plu + "</td >";
    content += '<td class="addBoxes" style="text-align:right;" hidden> ' + addBoxes + "</td >";
    content += '</tr>';

    $("table#tblCatVar tbody").append(content);
    calcTotal();

    let TotPallets = 0;
    $('table#tblCatVar tfoot tr .totalPallets').each(function () {
        TotPallets += parseFloat($(this).text());

    });
    TotPallets = parseFloat($('table#tblCatVar tfoot tr .totalPallets').text());

    switch (_cropID.toLowerCase()) {
        case 'cit':
            if (TotPallets == 20) {
                $(btnSavePO).removeClass();
                $(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
            } else if (TotPallets == 21) {
                $(btnSavePO).removeClass();
                $(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
            } else {
                $(btnSavePO).removeClass();
                $(btnSavePO).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
            }
            break;
        default:
            if (TotPallets == 20) {
                $(btnSavePO).removeClass();
                $(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
            } else {
                $(btnSavePO).removeClass();
                $(btnSavePO).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
            }
    }  

    $(".collapse").collapse('toggle');
    sweet_alert_success('Good!', 'Data Added.');
}

function calculoPallets() {
    var boxPallet = $("#txtBoxPallet").val();
    var boxes = $("#txtBoxes").val();
    var pallets = (boxes / boxPallet);
    $("#txtPallets").val(pallets);
}

function calcTotal() {
    var txtPallets = 0
    var txtPrice = 0
    var txtBoxes = 0
    $("#tblCatVar tbody tr td.txtPallets").each(function () {
        txtPallets += parseFloat($(this).text());
    })

    $("#tblCatVar tbody tr td.txtPrice").each(function () {
        txtPrice += parseFloat($(this).text());
    })

    $("#tblCatVar tbody tr td.txtBoxes").each(function () {
        txtBoxes += parseFloat($(this).text());
    })

    $('#tblCatVar .totalPallets').html(txtPallets.toFixed(2))
    $('#tblCatVar .totalPrice').html(txtPrice.toFixed(2))

}

function SavePORejected() {
    saleID = $("#lblSaleID").text();
    commentary = $("#commentary").val();

    /*
    var sUrlApi = "/Sales/GetStatusRejectPO?saleID=" + saleID + "&commentary=" + commentary
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
            }
            else {
                bRsl = true;
            }
        },
        error: function () {
            sweet_alert_info("Error", "Data was not saved");
        },
        complete: function () {
            if (bRsl) {
                sweet_alert_progressbar();
                sweet_alert_success('Saved!', 'Data saved successfully.');
                $("#exampleModal").modal('hide');
                $("#ModalSaleRequest").modal('hide');
                loadListPrincipal();
            }
        }
    })
    */
    fnGetStatusRejectPO(saleID, commentary, successGetStatusRejectPO);
}

var successGetStatusRejectPO = function (data) {
    if (data == null || data.length == 0) {
        sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
    }
    else {
        //sweet_alert_progressbar();
        sweet_alert_success('Saved!', 'Data saved successfully.');
        $("#exampleModal").modal('hide');
        $("#ModalSaleRequest").modal('hide');
        loadListPrincipal();
    }
}

function SavePOModified() {
    var saleID = $("#lblSaleID").text();
    fnGetStatusByMod(saleID, successGetStatusByMod);
}

var successGetStatusByMod = function (data) {
    if (data == null || data.length == 0) {
        sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
    }
    else {
        sweet_alert_progressbar();
        sweet_alert_success('Saved!', 'Data saved successfully.');
        $("#ModalSaleRequest").modal('hide');
        loadListPrincipal();
    }
}

function SavePO() {
    var o = {}
    o.saleID = $("#lblSaleID").text();
    //o.referenceNumber = "string";
    o.userCreated = _userID;
    o.projectedWeekID = $("#selectWeek option:selected").val();
    o.customerID = $("#selectCustomer option:selected").val();
    o.statusID = $('#txtPriority').val();
    o.destinationID = $("#selectDestination option:selected").val();
    o.commentary = "";//$('#txtComments').val();
    o.campaignID = _campaignID;
    o.wowID = $("#selectConsignee option:selected").val();
    if (o.wowID === '') { o.wowID = '0' }
    o.details = []

    $('table#tblCatVar>tbody>tr').each(function (i, e) {
        var item = {}
        item.saleDetailID = $(e).find('td.saleDetailID').text();
        item.saleID = $(e).find('td.saleID ').text();
        item.quantity = $(e).find('td.txtBoxes ').text();
        item.price = $(e).find('td.txtPrice ').text();
        item.paymentTerm = $(e).find('td.payment').text();
        item.conditionPaymentID = $(e).find('td.priceContitionID ').text();
        item.incotermID = $(e).find('td.IncotermID ').text();
        item.userCreated = _userID;
        item.categoryID = $(e).find('td.categoryID ').text();
        item.varietyID = $(e).find('td.varietyID ').text();
        item.codePackID = $(e).find('td.sizeBoxID ').text();
        item.presentationID = $(e).find('td.presentationID ').text();
        item.finalCustomerID = $(e).find('td.finalCustomerID').text();
        item.paymentTermID = $(e).find('td.paymentID ').text();
        item.userModifyID = 0;
        item.flexible = $(e).find('td.padlock').text();
        item.quantityMinimun = 0;
        item.priceBilling = 0;
        item.kamID = $(e).find('td.plu').text();
        item.priorityID = 0;
        item.formatID = $(e).find('td.formatID').text();
        item.packageProductID = $(e).find('td.typeBoxID').text();
        item.brandID = $(e).find('td.brandID').text();
        item.labelID = $(e).find('td.labelID').text();
        item.codepack = $(e).find('td.codePack1').text();
        item.varieties = $(e).find('td.varietyID').text();
        item.sizes = $(e).find('td.sizeID').text();
        o.details.push(item)
        //console.log(o.details);
    });
    //console.log(JSON.stringify(o));

    /*
    var sUrlApi = "/Sales/SaveSalePC";
    var bRsl = false;
    $.ajax({
        type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        url: sUrlApi,
        async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
            }
            else {
                bRsl = true;
            }
        },
        error: function () {
            sweet_alert_info("Error", "Data was not saved");
        },
        complete: function () {
            if (bRsl) {
                sweet_alert_progressbar();
                sweet_alert_success('Saved!', 'Data saved successfully.');
                $("#ModalSaleRequest").modal('hide');
                loadListPrincipal();
            }
        }
    })
    */
    fnSaveSalePC(JSON.stringify(o), successSaveSalePC, errorSaveSalePC, completeSaveSalePC);
}

var successSaveSalePC = function (data) {
    _bRsl = false;
    if (data == null || data.length == 0) {
        sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
    }
    else {
        _bRsl = true;
    }
}

var errorSaveSalePC = function () {
    sweet_alert_info("Error", "Data was not saved");
}

var completeSaveSalePC = function () {
    if (_bRsl) {
        sweet_alert_progressbar();
        sweet_alert_success('Saved!', 'Data saved successfully.');
        $("#ModalSaleRequest").modal('hide');
        loadListPrincipal();
    }
}

function cleanControls() {
    //Modal Nuevo
    $('table#tblCatVar>tbody').empty();
    $("table#tblPOModified tbody").empty()
    $('#lblSaleID').text("0");
    $("#SelectFinalCustomer").val("").selectpicker('refresh');
    $('#btnRejectPOModified').addClass('display-none')
    $('#btnSavePOModified').addClass('display-none')
    $('#btnEdit').addClass('display-none')
    $('#btnAdd').removeClass('display-none')
    $('#chkplu').prop('checked', false);
    $('#chkpadlock').prop('checked', false);
}

function openModal(id) {
    //Modal Edit 
    $('#detalleGrapes').removeClass('show')
      
    //$(".collapse").collapse('toggle');
    var form = $("#Formulario");
    form.removeClass('was-validated')

    cleanControls();
    if (id == 0) {
        $('#selectMarket').prop("disabled", false);
        $('#selectCustomer').prop("disabled", false);
        $('#selectDestination').prop("disabled", false);
        $('#selectVia').prop("disabled", false);
        $('#selectConsignee').prop("disabled", false);
        $('#selectDestinationPort').prop("disabled", false);
        loadDefaultValues();
        btnValidation();

    }
    else {

        $.ajax({
            type: "GET",
            url: "/Sales/GetSaleDetailList?saleID=" + id,
            async: true,
            headers: {
                'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
            success: function (data) {
                if (data.length > 0) {
                    var dataJson = JSON.parse(data);
                    _statuSale = dataJson[0].saleID;
                    _orderProductionID = dataJson[0].orderProductionID;
                    _statusID = dataJson[0].statusID;
                    $('#lblSaleID').text(dataJson[0].saleID);
                    $(txtSaleRequest).val(dataJson[0].referenceNumber);
                    $(txtSalesOrder).val(dataJson[0].orderProduction);
                    $(txtPriority).val(dataJson[0].priority);
                    $('#selectMarket').val(dataJson[0].marketID.trim()).change();
                    $('#selectMarket').selectpicker("refresh");
                    $('#selectCustomer').val(dataJson[0].customerID).change();
                    $('#selectWeek').val(dataJson[0].projectedWeekID).change();
                    $('#selectDestination').val(dataJson[0].destinationID).change();
                    $('#selectVia').val(dataJson[0].viaID).change();
                    $('#selectConsignee').val(dataJson[0].wowID).change();;
                    $('#selectConsignee').selectpicker("refresh");

                    $('#selectVariety').val(dataJson[0].varietyID).change();
                    $('#selectCategory').val(dataJson[0].categoryID).change();
                    $('#selectCategory').selectpicker("refresh");
                    $('#selectFormat').val(dataJson[0].formatID).change();
                    $('#selectTypeBox').val(dataJson[0].packageProductID).change();
                    $('#selectBrand').val(dataJson[0].brandID).change();
                    $('#selectPresentation').val(dataJson[0].presentationID).change();
                    $('#selectLabel').val(dataJson[0].labelID).change();
                    $('#SelectSizeBox').val(dataJson[0].codePackID).change();
                    $('#txtBoxPallet').val(dataJson[0].boxesPerPallet).change();
                    $('#txtBoxes').val(dataJson[0].quantity).change();
                    $('#txtNet').val(dataJson[0].net).change();
                    $('#txtPallets').val(dataJson[0].pallets).change();
                    $('#txtPrice').val(dataJson[0].price);
                    $('#selectPayment').val(dataJson[0].paymentTermID).change();
                    $('#selectPayment').selectpicker("refresh");
                    $('#selectPriceContition').val(dataJson[0].conditionPaymentID).change();
                    $('#selectIncoterm').val(dataJson[0].incotermID).change();
                    $('#txtaddBoxes').val(dataJson[0].addBoxes).change();
                    $(selectSize).selectpicker('val', (dataJson[0].sizeID).split(','))
                    //$('#selectSize').val(dataJson[0].sizeID).change();

                    $('#txtCodePack').val(dataJson[0].codepack1).change();
                    $('#SelectFinalCustomer').val(dataJson[0].finalCustomerID).change();

                    $('#txtStatus').val(dataJson[0].status);
                    $('#txtComments').val(dataJson[0].commentary);
                    $('#txtUserCreated').val(dataJson[0].userCreated);
                    $('#txtDateCreated').val(dataJson[0].dateCreated);

                    $('#txtUserModify').val(dataJson[0].userModify);
                    $('#txtDateModify').val(dataJson[0].dateModified);
                    $('#txtPO').val(dataJson[0].orderProduction);

                    $('#RejectionComments').text(dataJson[0].commentaryRejection);
                    $('#txtUserRejection').val(dataJson[0].userRejection);
                    $('#txtDateRejection').val(dataJson[0].dateRejection);

                    //$('#selectConsignee').val('');

                    $('#selectMarket').prop("disabled", true);
                    $('#selectCustomer').prop("disabled", true);
                    $('#selectDestination').prop("disabled", true);
                    $('#selectVia').prop("disabled", true);
                    $('#selectConsignee').prop("disabled", false);
                    $('#selectDestinationPort').prop("disabled", true);                    

                    //$('table#tblCatVar>tbody').empty()
                    //Tabla Edit
                    $.each(dataJson, function (i, e) {

                        var content = '<tr>';

                        content += '<td style="text-align:center; min-width: 100px">';
                        content += '<button style="font-size:12px" class="row-remove btn btn-sm btn-danger"><i class="fas fa-trash"></i></button><button class="row-edit btn btn-sm btn-primary" style="font-size:12px"><i class="fas fa-edit">';
                        //<i style="color: darkblue" class="fas fa-edit fa-sm row-edit"></i> & nbsp; 
                        //<i style="color: darkred" class="fas fa-trash fa-sm row-remove"></i>
                        content += '</td >';

                        content += '<td class="saleDetailID"hidden>' + e.saleDetailID + '</td > ';
                        content += '<td class=" saleID" hidden>' + e.saleID + '</td > ';
                        content += '<td class=" varietyID" hidden>' + e.varietyID + "</td >  ";
                        content += '<td class=" "> <textarea rows="1" class="variety form-control form-control-sm">' + e.variety + "</textarea></td>";
                        content += '<td class=" categoryID " hidden>' + e.categoryID + "</td > 	";
                        content += '<td class=" category ">' + e.category + "</td > 	";
                        content += '<td class=" formatID" hidden>' + e.formatID + "</td > 	";
                        content += '<td class=" format " style="text-align: right;">' + e.format + "</td > 	";
                        content += '<td class=" typeBoxID" hidden>' + e.packageProductID + "</td > 	";
                        content += '<td class=" typeBox ">' + e.packageProduct + "</td > 	";
                        content += '<td class=" brandID " hidden>' + e.brandID + "</td > 	";
                        content += '<td class=" brand">' + e.brand + "</td > 	";
                        content += '<td class=" presentationID " hidden>' + e.presentationID + "</td > 	";
                        content += '<td class="">' + e.presentation + "</td >";
                        content += '<td class=" labelID" hidden>' + e.labelID + "</td > 	";
                        content += '<td class=" label">' + e.label + "</td > 	";
                        content += '<td class=" sizeBoxID" hidden>' + e.codePackID + "</td > 	";
                        content += '<td class=" sizeBox" style="text-align: right;"hidden>' + e.codepack + "</td > 	";
                        content += '<td class=" boxesPerPallet" style="text-align: right;" hidden>' + e.boxesPerPallet + "</td > 	";
                        content += '<td class=" txtBoxes" style="text-align: right;">' + e.quantity + "</td >";
                        content += '<td class="txtNet" style="text-align: right;">' + e.net.toFixed(2) + "</td >";
                        content += '<td class="codePack1">' + e.codepack1 + "</td >";
                        content += '<td class=" IncotermID" hidden >' + e.incotermID + "</td > 	";
                        content += '<td class=" ">' + e.incoterm + "</td > 	";
                        content += '<td class=" priceContitionID" hidden>' + e.conditionPaymentID + "</td > 	";
                        content += '<td class="  ">' + e.conditionPayment + "</td>";
                        content += '<td class=" paymentID" hidden>' + e.paymentTermID + "</td > 	";
                        content += '<td class=" "><textarea rows="1" class=" payment form-control form-control-sm">' + e.paymentTerm + "</textarea></td>";                                                
                        content += '<td class=" sizeID" hidden>' + e.sizeID + "</td > 	";
                        content += '<td class=" size" hidden>' + e.sizeID + "</td > 	";
                        content += '<td class=" finalCustomerID "hidden>' + e.finalCustomerID + "</td >	";
                        content += '<td class=" "><textarea rows="1" class="finalCustomer form-control form-control-sm">' + e.finalCustomer + "</textarea></td >";
                        content += '<td class="txtPallets" style="text-align: right;">' + e.pallets + "</td>";
                        content += '<td class="txtPrice" style="text-align: right;" >' + e.price + "</td >";
                        content += '<td class="padlock" style="text-align: right;" hidden>' + e.padlock + "</td>";
                        content += '<td class="plu" style="text-align: right;" hidden>' + e.plu + "</td >";
                        content += '<td class="addBoxes" style="text-align: right;" hidden>' + e.addBoxes + "</td >";
                        content += '</tr>';

                        //$("table#tblCatVar").DataTable().clear();
                        //$("table#tblCatVar").DataTable().destroy();
                        $("table#tblCatVar tbody").append(content);

                    })

                    calcTotal();
                    btnValidation();
                    loadDetailModified()
                }

                if ($('#txtStatus').val() == 'REJECTED') {
                    $('div.modal-footer').addClass('display-none')
                } else $('div.modal-footer').removeClass('display-none');
            }
        });
    }
}

function btnValidation() {
    let TotPallets = 0;
    TotPallets = parseFloat($('table#tblCatVar tfoot tr .totalPallets').text());

    if (TotPallets == 20) {
        $(btnSavePO).removeClass();
        $(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
    } else if (TotPallets == 21) {
        $(btnSavePO).removeClass();
        $(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
    } else {
        $(btnSavePO).removeClass();
        $(btnSavePO).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
           }
}

function obligatory() {
    //$("#tblCatVar>tbody").find('tr').eq(row).find('td.totalPallets').text();
    //var totalpallets = $("#totalPallets").val();
    var totalpallets = parseInt($("#tblCatVar>tfoot>tr>td.totalPallets").text())

    switch (_cropID.toLowerCase()) {
        case 'cit':
            if (totalpallets < 20) {
                Swal.fire('Warning', 'Yoy must complete the 20 pallets to save', 'warning')
                return false;

            } else if (totalpallets > 21) {
                Swal.fire('Warning', 'You must complete just 20 pallets to save', 'warning')
                return false;
            }
            break;
        default:
            if (totalpallets < 20) {
                Swal.fire('Warning', 'Yoy must complete the 20 pallets to save', 'warning')
                return false;

            } else if (totalpallets > 20) {
                Swal.fire('Warning', 'You must complete just 20 pallets to save', 'warning')
                return false;
            }
    }   

    return true;
}

function obligatoryRow() {

    var market = $("#selectMarket").val();
    if (market == '') {
        Swal.fire('Warning', 'Choose a Market', 'warning')
        return false;
    }

    var customer = $("#selectCustomer option:selected").val()
    if (customer == "") {
        Swal.fire('Warning', 'Choose a Customer', 'warning')
        return false;
    }

    var week = $("#selectWeek option:selected").val()
    if (week == "") {
        Swal.fire('Warning', 'Choose a week', 'warning')
        return false;
    }

    var priority = $("#txtPriority").val();
    if (priority == '' || priority == 0) {
        Swal.fire(
            'Warning',
            'Please, choose a priority to the sales order',
            'warning'
        )
        return false;
    }

    var category = $("#selectCategory").val();
    if (category == '') {
        Swal.fire('Warning', 'Choose a category', 'warning')
        return false;
    }

    var format = $("#selectFormat").val();
    if (format == '' || format == 0) {
        Swal.fire('Warning', 'Choose a format', 'warning')
        return false;
    }

    var typeBox = $("#selectTypeBox").val();
    if (typeBox == '' || typeBox == 0) {
        Swal.fire('Warning', 'Choose a typeBox', 'warning')
        return false;
    }

    var sizeBox = $("#SelectSizeBox").val();
    if (sizeBox == '' || sizeBox == 0) {
        Swal.fire(
            'Warning',
            'Choose a type box and Net weight to generate the size box',
            'warning'
        )
        return false;
    }

    var boxPallet = $("#txtBoxPallet").val();
    if (boxPallet == '' || boxPallet == 0) {
        Swal.fire(
            'Warning',
            'Please, choose a size box to generate the box x pallets',
            'warning'
        )
        return false;
    }

    var boxes = $("#txtBoxes").val();
    if ((boxes == '') || (boxes == 0)) {
        Swal.fire(
            'Warning',
            'Please, choose a number of boxes greater than zero',
            'warning'
        )
        return false;
    }

    var pallets = $("#txtPallets").val();
    if (pallets == '' || pallets == 0) {
        Swal.fire(
            'Warning',
            'Please, choose a boxPallet and a box to generate a pallet ',
            'warning'
        )
        return false;
    }

    var priceCondition = $("#selectPriceContition").val();
    if (priceCondition == null) {
        Swal.fire('Warning', 'You must choose a Price Condition', 'warning')
        return false;
    }

    var incoterm = $("#selectIncoterm").val();
    if (incoterm == '') {
        Swal.fire('Warning', 'You must choose an Incoterm', 'warning')
        return false;
    }

    return true;
}

function deletesr(xthis) {
    let statusID = $(xthis).closest('tr').find('td.statusID').text();
    if (statusID != 6) {
        deleteSalesRequest($(xthis).closest('tr').find('td.saleID').text());
    } else {
        sweet_alert_error('Error', 'You cannot reject the sales order, your shipping instruction has been generated');
    }
}

function deleteSalesRequest(saleID) {

    var userID = $("#lblSessionUserID").text();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, reject it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {

        if (result.isConfirmed) {
            swalWithBootstrapButtons.fire(
                'Rejected!',
                'Your file has been rejected.',
                'success'
            )
            $.ajax({
                type: 'POST',
                headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
                url: "/Sales/DeleteSales?saleID=" + saleID + "&userID=" + userID,
                async: true,
                success: function (data) {
                    var dataList = JSON.parse(data);
                    if (data.length > 0) {
                        loadListPrincipal();
                    }
                }
            });

        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Cancel!', 'Reject process was canceled.')
        }
    });
}

$(document).ready(function () {
    $(".btn-primary").click(function () {
        $(".collapse").collapse('toggle');
    });

})

$(document).ready(function () {
    $("fas fa-edit fa-sm row-edit").click(function () {
        $(".collapse").collapse('toggle');
    });
})


