﻿var dataMarket = [];
var dataCustomer = [];
var dataCustomerMarket = [];
var dataWeek = [];
var dataDestinationCustomer = [];
var dataVia = [];
var dataCodepack = [];
var dataBrand = [];
var dataVariety = [];
var dataConditionPayment = [];
var dataDestinationPort = [];
var dataSizes = [];
var dataPresentation = [];
var dataDestinationS = [];
var dataMarketUser = [];
var dataAllWeek = [];
var dataTypeBox = [];
var ActualyWeek = [];
var NextWeek = [];
var dataStatus = [];
var lastSelect = [];
var dataWow = [];
var cropId = localStorage.cropID;
var flagSelectMarket = false;
var _orderProductionID = 0;
var _userID = $("#lblSessionUserID").text();
var dataIncoterm = [];
var dataDocumentsS = [];
var dataPackingListS = [];
var dataDocumentsSeaS = [];
var dataDocumentsAirS = [];
var _statusID = 0;

//var dataCertificationS = [{ "certificationID": 1, "description": "GFS" },
//{ "certificationID": 2, "description": "GLOBAL GAP" }]

$(function () {
	loadData();
	//cleanPackingList();
	$('#selectStatusFilter').val('All').change();
	$("#selectStatusFilter").selectpicker('refresh');

	//loadDataS();
	loadDefaultValues();
	var marketID = "";

	var weekIni = $('#cboSemanaIni').val();
	var weekEnd = $('#cboSemanaFin').val();
	var statusID = $('#selectStatusFilter').val();

	list(marketID, weekIni, weekEnd, statusID);

	$(document).on("click", "#btnAdd", function () {
		AddCatVar();
	});

	$(selectMarketS).change(function () {
		let data = dataDestinationS.filter(item => item.marketID.trim() == this.value.trim()).map(
			obj => {
				return {
					"id": obj.destinationID,
					"name": obj.destinaion
				}
			}
		);

		loadCombo(data, 'selectDestinationS', false);
		$('#selectDestinationS').selectpicker('refresh');
		$('#selectDestinationS').selectpicker('val', '0');
	});

	$('#btnReject').click(function () {
		if ($("#lblSaleID").text() == '0') {
			sweet_alert_error('Cancel!', 'Save process was canceled');
			$('#ModalDataSR').modal('hide');
			return;
		}

		if (_statusID == 2) {
			$('#ModalDataSR').modal('hide');
			return;
		}

		if ($(txtPO).val().length > 0) {
			sweet_alert_warning('info!', 'Cannot be rejected when a production order exists');
			return;
		}

		var rejectcomments = $("#txtComments").val();

		if (rejectcomments.length == 0) {

			sweet_alert_warning('info!', 'Write the reason for the rejection in the comments.');
			$("#txtComments").focus();
			return;
		}

		Swal.fire({
			title: 'Are you sure?',
			text: "Are you sure you want to reject?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, Save!',
			cancelButtonText: 'No, Cancel!',
			confirmButtonColor: '#4CAA42',
			cancelButtonColor: '#d33'
		}).then((result) => {
			if (result.value) {
				rejected();
				sweet_alert_success('Rejected!', 'Sale Request Rejected.');
				$('#ModalDataSR').modal('hide');
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				sweet_alert_error('Cancel!', 'Reject process was canceled.')
			}
		});
	})

	$('#txtPrice').change(function () {
		$('#txtBoxes').change()
	})

	$('#txtBoxes').change(function () {
		var currentBoxes = $(this).val();

		if (!$(chkFlexible).is(':checked')) {
			$(txtMinBoxes).val(currentBoxes)
		}

		var codePackID = $("#selectCodepack").val();
		var codePack;
		$.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
			if (e.codePackID == codePackID) {
				codePack = e
			}
		})

		var weight
		if (codePack != undefined) weight = parseInt(currentBoxes) * parseFloat(codePack.weight);
		else weight = 0;

		$('#txtNet').val(weight.toFixed(2))
		var boxPallet = parseInt($('#txtBoxPallet').val());
		var pallets = parseInt(currentBoxes) / boxPallet;

		$('#txtPallets').val(pallets.toFixed(2))

		var currentPrice = parseFloat($('#txtPrice').val());
		var totalPrice = (currentPrice * currentBoxes).toFixed(2);

		$('#txtTotalPrice').val(totalPrice);
	})

	$('#selectMarket').change(function () {
		loadCombo([], 'selectCustomer', true);
		loadCombo([], 'selectDestination', true);
		loadCombo([], 'selectCodepack', false);
		loadComboConsignnee([], 'selectConsignee', true);
		loadCombo([], 'selectDestinationPort', true);
		$('#selectCodepack').selectpicker('refresh');
		loadCombo(dataVia, 'selectVia', true);
		$('#selectVia').change()
		if (this.value == "") {
			return;
		}
		$('#selectVia').selectpicker('refresh');

		$dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == this.value.trim());
		data = $dataCustomerFilter.map(
			obj => {
				return {
					"id": obj.customerID,
					"name": obj.customer
				}
			}
		);
		loadCombo(data, 'selectCustomer', true);
		$('#selectCustomer').selectpicker('refresh');
		loadFinalCustomer()
	});

	$('#selectDestination').change(function () {
		$dataWowFilter = dataWow.filter(item => item.destinationID == this.value);
		data = $dataWowFilter.map(
			obj => {
				return {
					"id": obj.wowID,
					"name": obj.consignee
				}
			}
		);
		if (data.length > 0) {
			loadComboConsignnee(data, 'selectConsignee', true);
		} else {
			loadComboConsignnee([], 'selectConsignee', true);
		}
		$('#selectConsignee').selectpicker('refresh');
		let data2 = dataDestinationPort.filter(item => item.viaID == $("#selectVia option:selected").val() && item.destinationID == $("#selectDestination option:selected").val())
		if (data2.length > 0) {
			loadCombo(data2, 'selectDestinationPort', false);
		} else {
			loadCombo([], 'selectDestinationPort', true);
		}
	})

	$('#selectCustomer').change(function () {
		loadCombo([], 'selectDestination', true);
		loadCombo([], 'selectDestinationPort', true);

		if (this.value == "") {
			return;
		}
		if ($("#selectCustomer option:selected").val() > 0) {
			loadDataWow($("#selectCustomer option:selected").val())
			loadComboConsignnee([], 'selectConsignee', true);
			if (this.value == "") {
				return;
			}
		}
		AjaxGetPaymentTerm(this.value);

		if ($dataCustomerFilter.length > 0) {

			$dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val());
			data = $dataDestinationFilter.map(
				obj => {
					return {
						"id": obj.destinationID,
						"name": obj.destination
					}
				}
			);
			loadCombo(data, 'selectDestination', true);
			$('#selectDestination').selectpicker('refresh');

			loadFinalCustomer()

		} else {
			loadCombo([], 'selectDestination', true);
			$('#selectDestination').selectpicker('refresh');
		}

	});

	$('#selectVia').change(function () {
		if (this.value == "") {
			loadCombo([], 'selectCodepack', true);
			$('#selectCodepack').change().selectpicker('refresh');
			loadCombo([], 'selectDestinationPort', true);
			loadCombo([], 'selectIncoterm', true);
			return;
		}

		let data = dataCodepack.filter(item => item.viaID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val()).map(
			obj => {
				return {
					"id": obj.codePackID,
					"name": obj.description
				}
			}
		);
		loadCombo(data, 'selectCodepack', true);
		$('#selectCodepack').change().selectpicker('refresh');

		let data2 = dataDestinationPort.filter(item => item.viaID == $("#selectVia option:selected").val() && item.destinationID == $("#selectDestination option:selected").val())
		if (data2.length > 0) {
			loadCombo(data2, 'selectDestinationPort', false);
		} else {
			loadCombo([], 'selectDestinationPort', true);
		}

		let data3 = dataIncoterm.filter(item => item.viaID == $("#selectVia option:selected").val())
		if (data3.length > 0) {
			loadCombo(data3, 'selectIncoterm', true);
		} else {
			loadCombo([], 'selectIncoterm', true);
		}
		$('#selectIncoterm').selectpicker('refresh');

	});

	$('#selectCodepack').change(function () {
		$(txtBoxPallet).val(0);
		$(txtBoxContainer).val(0);
		$(txtBoxes).val(0);
		$(txtNet).val(0);
		$(txtPallets).val(0);
		$(txtTotalPrice).val(0);
		$(txtBoxes).prop("disabled", true);;
		if ($(selectCodepack).val() > 0) {
			var boxPallet = dataCodepack.filter(element => element.codePackID == $(selectCodepack).val())[0].boxesPerPallet
			var boxContainer = dataCodepack.filter(element => element.codePackID == $(selectCodepack).val())[0].boxPerContainer
			$(txtBoxPallet).val(boxPallet);
			$(txtBoxContainer).val(boxContainer);
			$(txtBoxes).attr("step", boxPallet);
			$(txtMinBoxes).attr("step", boxPallet);
			$(txtBoxes).prop("disabled", false);;
		}

		let data = dataPresentation.filter(item => item.codePackID == $(selectCodepack).val()).map(
			obj => {
				return {
					"id": obj.presentationID,
					"name": obj.presentation + obj.moreInfo
				}
			});
		loadCombo(data, 'selectPresentation', false);
		$('#selectPresentation').change().selectpicker('refresh');

		calculeCodepack();
	});

	//$('#btnSavePO').click(function (e) {
	//	if (_statusID != 6) {
	//		SaveDataSR();
	//	} else {
	//		sweet_alert_error('Error', 'You cannot modify the sales order, your shipping instruction has already been generated');
	//		$('#ModalDataSR').modal('hide');
	//	}
	//});

	$('#btnSavePO').click(function (e) {
			SaveDataSR();		
	});

	$('#btnCancel').click(function (e) {
		$('#ModalDataSR').modal('hide');
	});

	$(document).on("click", ".row-remove", function () {
		$idSaleDetail = $(this).parents('tr').children().eq(1).text();
		$idtable = $(this).parents('table').attr('id')
		$(this).parents('tr').detach();
		calcTotal();
	});

	$(document).on("click", "#btnSearch", function () {
		var marketID = [];
		$('#selectMarketFilter :selected').each(function (i, selected) {
			marketID[i] = $(selected).val();
		});
		var weekIni = $('#cboSemanaIni').val();
		var weekEnd = $('#cboSemanaFin').val();
		var statusID = $('#selectStatusFilter').val();

		list(marketID, weekIni, weekEnd, statusID);
	})

	$(document).on("click", "#btnPendingConfirmed", function () {
		$("#selectMarketFilter").selectpicker('deselectAll');
		$('#selectMarketFilter').selectpicker('refresh');
		var PendingConfirmed = dataStatus.filter(item => item.id == 4)
		$('#selectStatusFilter').val(PendingConfirmed[0].id).change();
		var marketID = '';
		var weekIni = $('#cboSemanaIni').val();
		var weekEnd = $('#cboSemanaFin').val();
		var statusID = $('#selectStatusFilter').val();

		list(marketID, weekIni, weekEnd, statusID);

	})

	$(document).on("click", "#btnModified", function () {
		$("#selectMarketFilter").selectpicker('deselectAll');
		$('#selectMarketFilter').selectpicker('refresh');
		var Modified = dataStatus.filter(item => item.id == 8)
		$('#selectStatusFilter').val(Modified[0].id).change();

		var marketID = $("#selectMarketFilter").val();
		var weekIni = $('#cboSemanaIni').val();
		var weekEnd = $('#cboSemanaFin').val();
		var statusID = $('#selectStatusFilter').val();

		list(marketID, weekIni, weekEnd, statusID);
	})

	$(document).on("click", "#btnConfirmed", function () {
		$("#selectMarketFilter").selectpicker('deselectAll');
		$('#selectMarketFilter').selectpicker('refresh');
		var Confirmed = dataStatus.filter(item => item.id == 5)
		$('#selectStatusFilter').val(Confirmed[0].id).change();

		var marketID = $("#selectMarketFilter").val();
		var weekIni = $('#cboSemanaIni').val();
		var weekEnd = $('#cboSemanaFin').val();
		var statusID = $('#selectStatusFilter').val();

		list(marketID, weekIni, weekEnd, statusID);
	})

	$(document).on("click", "#btnLoaded", function () {
		$("#selectMarketFilter").selectpicker('deselectAll');
		$('#selectMarketFilter').selectpicker('refresh');
		var Loaded = dataStatus.filter(item => item.id == 6)
		$('#selectStatusFilter').val(Loaded[0].id).change();

		var marketID = $("#selectMarketFilter").val();
		var weekIni = $('#cboSemanaIni').val();
		var weekEnd = $('#cboSemanaFin').val();
		var statusID = $('#selectStatusFilter').val();

		list(marketID, weekIni, weekEnd, statusID);
	})

	$(document).on("click", "#btnEdit", function () {

		if (!obligatoryRow()) {
			return;
		}

		$('#btnEdit').addClass('display-none')
		$('#btnAdd').removeClass('display-none')

		$("#tblCatVar>tbody>tr").each(function (index, tr) {
			$(tr).removeClass("row-edit-sales")
		})

		var saleID = $("#lblSaleID");

		var categoryID = $("#selectCategory").val();
		var category = []

		$.each(dataBrand, function (i, e) {
			if (e.id == categoryID) {
				category.push(e.name)
			}
		})

		var varietyID = $("#selectVariety").val();
		var variety = [];
		$.each(varietyID, function (ii, ee) {
			$.each(dataVariety, function (i, e) {
				if (e.id == ee) {
					variety.push(e.name)
				}
			})
		})

		var codePackID = $("#selectCodepack").val();
		var codePack = [];

		$.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
			if (e.codePackID == codePackID) {
				codePack.push(e.description)
			}
		})

		var sizeID = $("#selectSize").val();
		var packageProductID = $("#selectPackage").val();

		var quantity = $("#txtBoxes");
		var quantityMinimum = $("#txtMinBoxes");
		var xchkFlexible = 0
		if ($(chkFlexible).is(':checked')) {
			xchkFlexible = 1;
		}

		var net = $("#txtNet");
		var price = $("#txtPrice");
		var conditionPaymentID = $("#selectPriceContition option:selected");
		var incotermID = $("#selectIncoterm option:selected");
		var paymentTerm = $("#selectPayment option:selected");
		var content = '<tr class="text-align-right">';

		var presentation = $("#selectPresentation option:selected")

		var pallets = $("#txtPallets").val();
		var totalPrice = $("#txtTotalPrice").val();
		var codePack2 = $("#txtCodePack").val();

		var finalcustomerID = $("#selectFinalCustomer option:selected").val()
		var finalcustomer = ""
		if (finalcustomerID.length > 0) {
			finalcustomer = $("#selectFinalCustomer option:selected").text()
		}

		var workOrder = false;
		if ($('#chkWorkOrder').is(":checked")) {
			workOrder = true;
		}

		var row = $(txtRow).val()

		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(2).text(categoryID);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(3).text(category);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(4).text(varietyID);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(5).text(variety);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(6).text(sizeID);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(7).text(codePackID);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(8).text(codePack);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(9).text($(presentation).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(10).text($(presentation).text());

		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(11).text($(quantity).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(12).text($(quantityMinimum).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(13).text(xchkFlexible);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(14).text($(net).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(15).text($(price).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(16).text($(packageProductID).val());

		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(17).text($(incotermID).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(18).text($(incotermID).text());

		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(19).text($(conditionPaymentID).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(20).text($(conditionPaymentID).text());		

		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(21).text($(paymentTerm).text());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(23).text(pallets);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td').eq(24).text(totalPrice);

		$("#tblCatVar>tbody").find('tr').eq(row).find('td.tdFinalCustomer').text(finalcustomer);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td.tdFinalCustomerID').text(finalcustomerID);

		$("#tblCatVar>tbody").find('tr').eq(row).find('td.tdPaymentTermID').text($(paymentTerm).val());
		$("#tblCatVar>tbody").find('tr').eq(row).find('td.tdcodePack2').text(codePack2);
		$("#tblCatVar>tbody").find('tr').eq(row).find('td.tdworkOrder').text(workOrder);

		calcTotal();
		clearDetails();
	})

	$(document).on("click", ".row-edit", function () {

		$("#tblCatVar>tbody>tr").each(function (index, tr) {
			$(tr).removeClass("row-edit-sales")
		})

		$('details').attr('open', '');
		$('#btnAdd').addClass('display-none')
		$('#btnEdit').removeClass('display-none')

		$idSaleDetail = $(this).parents('tr').children().eq(1).text();
		var row = $(this).parent().parent().index();

		var e = $(this).parents('tr')
		$(e).addClass("row-edit-sales");

		var saleDetailID = $(e).find('td:eq(1)').text();

		var categoryID = $(e).find('td:eq(2)').text();
		var varietyID = $(e).find('td:eq(4)').text();
		var sizeID = $(e).find('td:eq(6)').text();
		var codePackID = $(e).find('td:eq(7)').text();
		var presentationID = $(e).find('td:eq(9)').text();

		var quantity = $(e).find('td:eq(11)').text();
		var quantityMinumun = $(e).find('td:eq(12)').text();
		var flexible = $(e).find('td:eq(13)').text();

		var price = $(e).find('td:eq(15)').text().trim();
		var packageProductID = $(e).find('td:eq(16)').text();
		
		var incotermID = $(e).find('td:eq(17)').text();
		var conditionPaymentID = $(e).find('td:eq(19)').text();
		var paymentTerm = $(e).find('td:eq(21)').text();		

		var finalCustomerID = $(e).find('td.tdFinalCustomerID').text();
		var paymentTermID = $(e).find('td.tdPaymentTermID').text();
		var codePack2 = $(e).find('td.tdcodePack2').text();

		var workOrder = $(e).find('td.tdworkOrder').text();
		if (workOrder == "true") {
			$('#chkWorkOrder').prop('checked', true);

		} else {
			$('#chkWorkOrder').prop('checked', false);
		}

		$(selectCategory).selectpicker('val', categoryID)
		$(selectVariety).selectpicker('val', varietyID.split(','))
		$(selectCodepack).selectpicker('val', codePackID.split(','))
		$(selectPresentation).selectpicker('val', presentationID.split(','))
		$(selectSize).selectpicker('val', sizeID.split(','))
		$(selectPackage).selectpicker('val', packageProductID.split(','))

		$(selectIncoterm).selectpicker('val', incotermID.split(','))
		$(selectPriceContition).selectpicker('val', conditionPaymentID.split(','))
		$(selectPayment).selectpicker('val', paymentTermID)
		$(txtPaymentTerm).val(paymentTerm);
		$(txtCodePack).val(codePack2);

		$(txtBoxes).val(quantity).change();
		$(txtPrice).val(price).change();		
		$(selectFinalCustomer).val(finalCustomerID);
		$(txtSaleDetailID).val(saleDetailID);

		$(txtRow).val(row);				

		if (flexible == 1) {
			$(chkFlexible).prop('checked', true).attr('checked', 'checked')
			$(txtMinBoxes).prop('disabled', false)

		} else {
			$(chkFlexible).prop('checked', false).removeAttr('checked');
			$(txtMinBoxes).prop('disabled', true)

		}

		$(txtMinBoxes).val(quantityMinumun)

	});

	$(chkFlexible).click(function () {
		if ($(chkFlexible).prop('checked')) {
			$(txtMinBoxes).prop("disabled", false);
			$(txtMinBoxes).val(0)
			$(chkFlexible).attr('checked', 'checked');
		} else {
			$(txtMinBoxes).prop("disabled", true);
			$(txtMinBoxes).val($(txtBoxes).val())
			$(chkFlexible).removeAttr('checked');
		}
	})

	function calculeCodepack() {
		let varietyID = '';
		let arrayVarietyID = $(selectVariety).val();
		$.each(arrayVarietyID, function (index, value) {
			varietyID += value + ',';
		});
		if (varietyID.length == 0) {
			$(txtCodePack).val('');
			return false;
		}

		if ($("#selectCodepack option:selected").val() == 0) {
			return false;
		}

		if ($("#selectPackage option:selected").val() == 0) {
			return false;
		}

		if ($("#selectCategory option:selected").val() == 0) {
			return false;
		}

		if ($("#selectSize option:selected").val() == 0) {
			return false;
		}

		let arraySizeID = $(selectSize).val();

		let codePackID;
		let typeBoxID;
		let categoryID;
		let presentationID;
		var varietyCode;
		var formatCode;
		var typeBoxCode;
		var brandCode;
		var presentationCode;
		var codepack = '';
		let codepackToShow = '';
		$.each(arrayVarietyID, function (index, value) {
			varietyID = value;
			codePackID = $(selectCodepack).val();
			typeBoxID = $(selectPackage).val();
			categoryID = $(selectCategory).val();
			presentationID = $(selectPresentation).val();

			varietyCode = dataVariety.filter(item => item.id == varietyID)[0].abbreviation;
			formatCode = dataCodepack.filter(item => item.codePackID == codePackID)[0].weight;
			typeBoxCode = dataTypeBox.filter(item => item.id == typeBoxID)[0].abbreviation;
			brandCode = dataBrand.filter(item => item.id == categoryID)[0].name;
			presentationCode = dataPresentation.filter(item => item.presentationID == presentationID)[0].presentation;

			codepack = varietyCode.trim() + '_' +
				formatCode +
				typeBoxCode.slice(0, 1) +
				brandCode.slice(0, 1) + '_' +
				presentationCode;

			codepackToShow = (codepack.trim() == '') ? codepackToShow : codepack + ',' + codepackToShow;

		});
		codepackToShow = codepackToShow.substring(0, codepackToShow.length - 1);
		let arrayCodePack = codepackToShow.split(',');
		let codepacksToStore = '';
		codepackToShow = '';
		$.each(arraySizeID, function (iRowSize, oSize) {
			$.each(arrayCodePack, function (iRowCode, oCode) {
				codepackToShow = (oCode + '_+' + oSize.substring(1, oSize.length) + '_' + 1) + '\n' + codepackToShow;
				codepacksToStore = (oCode + '_+' + oSize.substring(1, oSize.length) + '_' + 1) + ',' + codepacksToStore;
			});
		});
		$(txtCodePack).val(codepacksToStore);

	}

	$('#btnSavePOModified').click(function (e) {		
		SavePOModified();
	});

	$('#btnRejectPOModified').click(function (e) {
		$('#btnRejectPOModified').modal('toggle');
	});

	$('#btnSaveReject').click(function (e) {
		SavePORejected();
	});

	$(document).on("change", "#selectCategory", function () {
		calculeCodepack();
	});

	$(document).on("change", "#selectVariety", function () {
		calculeCodepack();
	});

	$(document).on("change", "#selectPackage", function () {
		calculeCodepack();
	});

	$(document).on("change", "#selectPresentation", function () {
		calculeCodepack();
	});

	$(document).on("change", "#selectSize", function () {
		calculeCodepack();
	});

	$("#chkWorkOrder").attr("disabled", "disabled");
})

//function VerDetalle(xthis) {
//	document.getElementById('Detalle').open = true;
//}

//function cleanPackingList() {
//	document.getElementById('Detalle').open = false;
//	$("#txtPackingList").val('');
//	$("#txtETD").val('');
//	$("#txtETA").val('');
//	$("table#tblDetailPacking tbody").html('');
//	$('#tblDetailPacking .totalPallets').html('')
//	$('#tblDetailPacking .totalBoxes').html('')
//	$('#tblDetailPacking .totalNetWeight').html('')
//}

/*function CargarPackingList(xthis) {

	var orderProductionID = _orderProductionID;
	var dataOutSpecs = [];
	$("table#tblDetailPacking").DataTable().destroy();
	$("table#tblDetailPacking tbody").html('');

	$.ajax({
		type: "GET",
		url: "/Sales/GetsalesOutSpecs?orderProductionID=" + orderProductionID,
		//async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			if (data.length > 2) {
				dataOutSpecs = JSON.parse(data);
				$("#txtPackingList").val(dataOutSpecs[0].packingList);
				$("#txtETD").val(dataOutSpecs[0].etd);
				$("#txtETA").val(dataOutSpecs[0].eta);
				$.each(dataOutSpecs, function (i, e) {
					var tr = '';
					tr += '<tr class="table-tr-agrocom">';
					tr += '<td style="text-align: center;" id="brand">' + e.brand + '</td>';
					tr += '<td style="text-align: center;" id="codePack">' + e.codePack + '</td>';
					tr += '<td style="text-align: center;text-align:right;" id="boxes">' + e.boxes + '</td>';
					tr += '<td style="text-align: center;text-align:right;" id="pallet">' + e.pallet + '</td>';
					tr += '<td style="text-align: center;text-align:right;" id="netWeight">' + e.netWeight + '</td>';
					if (e.out_specs == 'NO') {
						tr += '<td style="text-align: center;background-color: red;color:white" id="fulfillment">' + e.out_specs + '</td>';
					} else {
						tr += '<td style="text-align: center;" id="fulfillment">' + e.out_specs + '</td>';
					}
					tr += '</tr>';
					$('table#tblDetailPacking tbody').append(tr);
				})
			}

		},
		complete: function () {
			$('table#tblDetailPacking').DataTable({
				//"order": [[7, "DESC"], [6, "DESC"], [2, "DESC"]]
				"order": [[1, "DESC"]],
				destroy: true,
				searching: false,
				"bInfo": false,
				"bLengthChange": false,

			});
			if (dataOutSpecs.length > 0) {
				CalcularTotalPackingList();
				VerDetalle(xthis);
			} else {
				sweet_alert_warning('Warning!', 'Packing list not yet created');
			}

		}
	});
}*/

/*function CalcularTotalPackingList() {
	var pallet = 0
	var boxes = 0
	var netWeight = 0
	$("#tblDetailPacking tbody tr").find("#pallet").each(function () {
		pallet += parseFloat($(this).text());

	})

	$("#tblDetailPacking tbody tr").find("#boxes").each(function () {
		boxes += parseInt($(this).text());

	})

	$("#tblDetailPacking tbody tr").find("#netWeight").each(function () {
		netWeight += parseFloat($(this).text());

	})

	$('#tblDetailPacking .totalPallets').html(pallet.toFixed(2))
	$('#tblDetailPacking .totalBoxes').html(boxes)
	$('#tblDetailPacking .totalNetWeight').html(netWeight.toFixed(2))

}*/

function loadQuantityByStatus(marketID, weekIni, weekEnd) {
	$.ajax({
		type: "GET",
		url: "/Sales/GetMountByStatus?campaignID=" + localStorage.campaignID,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataStatus = dataJson.map(
					obj => {
						return {
							"id": obj.statusID,
							"name": obj.status,
							"quantity": obj.quantity
						}
					}
				);

				var x1 = dataStatus.filter(item => item.id == 4)[0].quantity;
				var x2 = dataStatus.filter(item => item.id == 8)[0].quantity;
				var x3 = dataStatus.filter(item => item.id == 5)[0].quantity;
				var x4 = dataStatus.filter(item => item.id == 6)[0].quantity;
				document.getElementById("PendingConfirmed").innerHTML = x1;
				document.getElementById("Modified").innerHTML = x2;
				document.getElementById("Confirmed").innerHTML = x3;
				document.getElementById("Loaded").innerHTML = x4;
			}
		}
	});
}

function loadFinalCustomer() {
	if ($("#selectCustomer option:selected").val() != "0") {
		$(selectFinalCustomer).show(1000)

		$dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == $("#selectMarket option:selected").val());
		data = $dataCustomerFilter.map(
			obj => {
				return {
					"id": obj.customerID,
					"name": obj.customer
				}
			}
		);
		loadCombo(data, 'selectFinalCustomer', true);

		$("#labelFinalCustomer").show(100)
		$("#selectFinalCustomer").show(100)

	} else {
		$("#selectFinalCustomer").hide()
		$("#labelFinalCustomer").hide()
		loadCombo([], 'selectFinalCustomer', true);
	}

}

function loadDataCustomer(marketID) {
	$dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == this.value.trim());
	data = $dataCustomerFilter.map(
		obj => {
			return {
				"id": obj.customerID,
				"name": obj.customer
			}
		}
	);
}

function calcTotal() {
	var pallet = 0
	var price = 0
	$("#tblCatVar tbody tr td.tdPallet").each(function () {
		pallet += parseFloat($(this).text());
	})

	$("#tblCatVar tbody tr td.tdPrice").each(function () {
		price += parseFloat($(this).text());
	})

	$('#tblCatVar .totalPallets').html(pallet.toFixed(2))
	$('#tblCatVar .totalPrice').html(price.toFixed(2))

}

function loadDataWeek(idSeason) {
	$.ajax({
		type: "GET",
		url: "/Sales/ListWeekBySeason?idSeason=" + idSeason,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description
						}
					}
				);
			}
		}
	});
}

function loadDataWow(customerID) {
	$.ajax({
		type: "GET",
		url: "/Sales/ListWowByCustomer?customerID=" + customerID + "&cropID=" + localStorage.cropID,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			if (data.length > 0) {
				dataWow = JSON.parse(data);
			}
		}
	});
}

function loadDataDestination(marketID) {
	var opt = 'wcu';
	var id = '';
	var mar = '';
	var via = '';
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListDestinationAllWithCustomer?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			dataDestinationCustomer = JSON.parse(data);
		}
	});
}

function loadComboFilter(data, control, firtElement, textFirstElement) {
	var content = "";
	if (firtElement == true) {
		content += "<option value='All'>" + textFirstElement + "</option>";
	}
	for (var i = 0; i < data.length; i++) {
		content += "<option value='" + data[i].id + "'>";
		content += data[i].name;
		content += "</option>";
	}
	$('#' + control).empty().append(content);
}

function loadDefaultValues() {
	loadComboFilter(dataMarketUser, 'selectMarketFilter', true, '[ALL]');
	$('#selectMarketFilter').selectpicker('refresh');

	loadComboFilter(dataWeek, 'cboSemanaIni', false, '[ALL]');
	$('#cboSemanaIni').val(dataWeek[0].id).change();
	$('#cboSemanaIni').selectpicker('refresh');

	loadComboFilter(dataWeek, 'cboSemanaFin', false, '[ALL]');
	$('#cboSemanaFin').val(dataWeek[0].id).change();
	$('#cboSemanaFin').selectpicker('refresh');

	loadComboFilter(dataStatus, 'selectStatusFilter', true, '[ALL]');
	//$('#selectStatusFilter').val(dataStatus[0].id).change();
	$('#selectStatusFilter').selectpicker('refresh');

	var PendingConfirmed = dataStatus.filter(item => item.id == 4)
	document.getElementById("PendingConfirmed").innerHTML = PendingConfirmed[0].quantity;

	var Modified = dataStatus.filter(item => item.id == 8)
	document.getElementById("Modified").innerHTML = Modified[0].quantity;

	var Confirmed = dataStatus.filter(item => item.id == 5)
	document.getElementById("Confirmed").innerHTML = Confirmed[0].quantity;

	var Loaded = dataStatus.filter(item => item.id == 6)
	document.getElementById("Loaded").innerHTML = Loaded[0].quantity;

	loadCombo(dataMarket, 'selectMarket', true);
	$('#selectMarket').prop("disabled", false);
	$('#selectMarket').selectpicker('refresh');

	loadCombo([], 'selectCustomer', true);
	$('#selectCustomer').prop("disabled", false);
	$('#selectCustomer').selectpicker('refresh');

	loadCombo(dataWeek, 'selectWeek', true);
	$('#selectWeek').selectpicker('refresh');

	loadComboConsignnee([], 'selectConsignee', true);
	$('#selectConsignee').selectpicker('refresh');

	loadCombo([], 'selectDestination', true);
	$('#selectDestination').selectpicker('refresh');

	loadCombo([], 'selectVia', true);
	$('#selectVia').selectpicker('refresh');

	loadCombo([], 'selectCodepack', true);
	$('#selectCodepack').selectpicker('refresh');
	loadCombo([], 'selectDestinationPort', true);
	loadCombo(dataSizes, 'selectSize', false);
	$('#selectSize').selectpicker('refresh');
	$('#selectCodepack').change().selectpicker('refresh');

	loadCombo(dataBrand, 'selectCategory', true);
	$('selectCategory').selectpicker('val', []).selectpicker('refresh');
	$('#selectCategory').selectpicker('refresh');
	loadCombo(dataVariety, 'selectVariety', false);
	$('#selectVariety').selectpicker('refresh');

	loadCombo(dataTypeBox, 'selectPackage', true);
	$('#selectPackage').selectpicker('refresh');

	loadCombo(dataConditionPayment, 'selectPriceContition', false);
	$('#selectPriceContition').selectpicker('refresh');

	loadCombo([], 'selectIncoterm', true);
	$('#selectIncoterm').selectpicker('refresh');

	$('#txtPrice').val('0')
	$('#txtBoxes').val('0')
	$('#txtNet').val('0')
	$('#txtPaymentTerm').val('')
	$('#txtBoxPallet').val('0')
	$('#txtBoxContainer').val('0')
	$('#txtCodePack').val('')
	$('.totalPallets').html('0.00')
	$('.totalPrice').html('0.00')

	$(txtSaleRequest).val('')
	$(txtSalesOrders).val('')
	$(txtUserCreated).val('')
	$(txtDateCreated).val('')
	$(txtUserModify).val('')
	$(txtDateModify).val('')
	$(txtStatus).val('')
	$(txtPO).val('')
	$('#txtPriority').val('0')

	$('#RejectionComments').text('');
	$('#txtUserRejection').val('');
	$('#txtDateRejection').val('');
	$('#selectVia').prop("disabled", false);
	$('#selectCodepack').prop("disabled", false);
	$('#selectConsignee').prop("disabled", false);
	$('#selectDestinationPort').prop("disabled", false);
	$(selectPriceContition).selectpicker('val', '')
	$(txtSaleDetailID).val(0);
	$(txtRow).val(0);

	loadCombo([], 'selectPayment', true)
	$(selectPayment).selectpicker('refresh');

	$(chkFlexible).prop('checked', false).removeAttr('checked');
	$(txtMinBoxes).prop('disabled', true)
	$(txtMinBoxes).prop('value', 0)

	$('#spanObligatory').text('');

}

function loadData() {

	loadDataWeek(localStorage.campaignID);
	$.ajax({
		type: "GET",
		url: "/Sales/GetIncotermMC?opt=sal&cropID=" + localStorage.cropID + "&viaID=0",
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataIncoterm = dataJson.map(
					obj => {
						return {
							"id": obj.incotermID,
							"name": obj.name,
							"viaID": obj.viaID
						}
					}
				);
			}
		}
	});

	//var valueDocument = "";
	//$.ajax({
	//	type: "GET",
	//	url: "/Sales/GetDocument?opt=pl&value=" + valueDocument,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {
	//		var dataJson = JSON.parse(data);
	//		if (data.length > 0) {
	//			dataDocumentsS = dataJson.map(
	//				obj => {
	//					return {
	//						"documentExportID": obj.documentExportID,
	//						"description": obj.description,
	//						"statusID": obj.statusID
	//					}
	//				}
	//			);

	//			dataDocumentsSeaS = dataDocumentsS.filter(item => item.documentExportID != 3);
	//			dataDocumentsAirS = dataDocumentsS.filter(item => item.documentExportID == 3);
	//		}
	//	}
	//});

	//$.ajax({
	//	type: "GET",
	//	url: "/Sales/GetDocument?opt=doc&value=" + valueDocument,
	//	async: false,
	//	headers: {
	//		'Cache-Control': 'no-cache, no-store, must-revalidate',
	//		'Pragma': 'no-cache',
	//		'Expires': '0'
	//	},
	//	success: function (data) {
	//		var dataJson = JSON.parse(data);
	//		if (data.length > 0) {
	//			dataPackingListS = dataJson;
	//		}
	//	}
	//});

	var opt = 'nwk';
	var val = '';
	var weekIni = 0;
	var weekEnd = 0;
	var userID = 0;
	$.ajax({
		type: "GET",
		url: "/Sales/GetNextWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				NextWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	opt = 'all';
	id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/ListMarket?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataMarket = dataJson.map(
					obj => {
						return {
							"id": obj.marketID,
							"name": obj.name
						}
					}
				);
			}
		}
	});

	opt = 'lim';
	val = '';
	weekIni = 0;
	weekEnd = 0;
	campaignID = localStorage.campaignID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetMarketByUserID?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + _userID + "&campaignID=" + campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataMarketUser = dataJson.map(
					obj => {
						return {
							"id": obj.marketID,
							"name": obj.market
						}
					}
				);
			}
		}
	});

	opt = 'wcu';
	id = '';
	mar = '';
	via = '';
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListDestinationAllWithCustomer?opt=" + opt + "&id=" + id + "&mar=" + mar + "&via=" + via,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			dataDestinationCustomer = JSON.parse(data);
		}
	});

	opt = 'wmr';
	id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/ListAllWithMarket?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			dataCustomerMarket = JSON.parse(data);
		}
	});

	opt = 'all';
	id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/ListVia?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataVia = dataJson.map(
					obj => {
						return {
							"id": obj.viaID,
							"name": obj.name
						}
					}
				);
			}
		}
	});

	var opt = 'cro';
	var id = localStorage.cropID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			dataCodepack = JSON.parse(data);
		}
	});

	opt = "pkc";
	id = "";    
    $.ajax({
        type: "GET",
        url: "/Sales/GetPackageProductAllMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataTypeBox = dataJson.map(
                    obj => {
                        return {
                            "id": obj.packageProductID,
                            "name": obj.packageProduct,
                            "abbreviation": obj.abbreviation
                        }
                    }
                );
            }
        }
    });

	var opt = 'cro';
	var id = localStorage.cropID;
	var growerID = '';
	$.ajax({
		type: "GET",
		url: "/Sales/ListBrandMC?opt=" + opt + "&id=" + id + "&growerID=" + growerID,
		async: false,
		headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataBrand = dataJson.map(
					obj => {
						return {
							"id": obj.categoryID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	var opt = 'cro';
	var id = localStorage.cropID;
	var log = '';
	$.ajax({
		type: "GET",
		url: "/Sales/VarietyGetByCropMC?opt=" + opt + "&id=" + id + "&log=" + log,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataVariety = dataJson.map(
					obj => {
						return {
							"id": obj.varietyID,
							"name": obj.name,
							"abbreviation": obj.abbreviation
						}
					}
				);
			}
		}
	});

	opt = 'all';
	id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/ListConditionPayment?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataConditionPayment = dataJson.map(
					obj => {
						return {
							"id": obj.conditionPaymentID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	var opt = 'cro';
	var id = localStorage.cropID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetAllSizesMC?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataSizes = dataJson.map(
					obj => {
						return {
							"id": obj.sizeID,
							"name": obj.code
						}
					}
				);
			}
		}
	});

	opt = 'pre';
	id = '';
	$.ajax({
		type: "GET",
		url: "/Sales/GetCodepackWithPresentation?opt=" + opt + "&id=" + id,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			if (data.length > 0) {
				var dataJson = JSON.parse(data);
				dataPresentation = dataJson
			}
		}
	});

	var opt = '';
	var val = '';
	var weekIni = 0;
	var weekEnd = 0;
	var userID = 0;
	var campaignID = localStorage.campaignID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetAllWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataAllWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	var opt = 'sta';
	var val = '';
	var weekIni = 0;
	var weekEnd = 0;
	var userID = 0;
	$.ajax({
		type: "GET",
		url: "/Sales/GetStatusMC?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataStatus = dataJson.map(
					obj => {
						return {
							"id": obj.statusID,
							"name": obj.status,
							"quantity": obj.quantity
						}
					}
				);
			}
		}
	});

	opt = 'act';
	val = '';
	weekIni = 0;
	weekEnd = 0;
	userID = 0;
	$.ajax({
		type: "GET",
		url: "/Sales/GetCurrentWeek?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				ActualyWeek = dataJson.map(
					obj => {
						return {
							"id": obj.projectedWeekID,
							"name": obj.description
						}
					}
				);
			}
		}
	});

	sweet_alert_progressbar_cerrar();
}

function loadCombo(data, control, firtElement) {
	var content = "";
	if (firtElement == true) {
		content += "<option value=''>--Choose--</option>";
	}
	for (var i = 0; i < data.length; i++) {
		content += "<option value='" + data[i].id + "'>";
		content += data[i].name;
		content += "</option>";
	}
	$('#' + control).empty().append(content);
}

function loadComboConsignnee(data, control, firtElement) {
	var content = "";
	if (firtElement == true) {
		content += "<option value=''>--To be Confirmed--</option>";
	}
	for (var i = 0; i < data.length; i++) {
		content += "<option value='" + data[i].id + "'>";
		content += data[i].name;
		content += "</option>";
	}
	$('#' + control).empty().append(content);
	$('#' + control).selectpicker('refresh');

}

function loadDetailModified() {

	$("table#tblPOModified tbody").empty()
	var api = "/Sales/GetSalesDetailModifiedPO?saleID=" + $("#lblSaleID").text();
	$.ajax({
		type: "GET",
		url: api,
		//async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			if (data.length > 0) {
				$('#btnSavePO').addClass('display-none')
				var dataList = JSON.parse(data);
				var content = "";
				$.each(dataList, function (index, row) {
					content += "<tr>";
					content += '<td style="min-width:200px; max-width:200px;font-size: 13px;" ><textarea class="form-control form-control-sm" rows="1" id="varieties" readonly>' + row.variety + '</textarea></td>'
					content += '<td style="min-width:100px; max-width:100px;font-size: 13px;" >' + row.category + '</td>'
					content += '<td style="min-width:200px; max-width:200px;font-size: 13px;" ><textarea class="form-control form-control-sm" rows="1" id="codepack2" readonly>' + row.codepack + '</textarea></td>'
					content += '<td style="min-width:100px; max-width:100px;font-size: 13px;" >' + row.sizes + '</td>'
					content += '<td style="min-width:100px; max-width:100px;font-size: 13px;" >' + row.boxes + '</td>'
					content += '<td style="min-width:100px; max-width:100px;font-size: 13px;" >' + row.pallets + '</td>'
					content += "</tr>";
				})

				$("table#tblPOModified tbody").empty().append(content);

				$("table#tblSales>tbody>tr>td.statusID").each(function (i, e) {
					var saleID2 = $(e).closest('tr').find('td.saleID').text();

					if ($("#lblSaleID").text() == saleID2) {
						if (parseInt($(e).closest('tr').find('td.statusID').text()) == '4' || parseInt($(e).closest('tr').find('td.statusID').text()) == '5') {

							$(btnRejectPOModified).removeClass();
							$(btnSavePOModified).removeClass();
							$(btnSavePO).removeClass();
							$('#btnSavePOModified').addClass('display-none')
							$('#btnRejectPOModified').addClass('display-none')
							$(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');

						}

						if (parseInt($(e).closest('tr').find('td.statusID').text()) == '8') {

							$(btnRejectPOModified).removeClass();
							$(btnSavePOModified).removeClass();
							$(btnSavePO).removeClass();
							$('#btnSavePO').addClass('display-none')
							$(btnRejectPOModified).addClass('btn btn-danger btn-sm btn-icon-split transact pull-right');
							$(btnSavePOModified).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
							$('#selectWeek').prop("disabled", true);
							$('#selectDestination').prop("disabled", true);
							$('#selectConsignee').prop("disabled", true);
							$('#txtPriority').attr("disabled", "disabled");
							jQuery('.row-edit').prop("disabled", true);
							jQuery('.row-remove').prop("disabled", true);

						} else {
							$('#btnSavePO').removeClass();
							$('#btnSavePO').addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
							$(btnRejectPOModified).removeClass();
							$(btnSavePOModified).removeClass();
							$(btnRejectPOModified).addClass('display-none')
							$(btnSavePOModified).addClass('display-none')
						}

					}

				})

			}
		}
	})
}

function SavePOModified() {
	var saleID = $("#lblSaleID").text();
    
    var sUrlApi = "/Sales/GetStatusByMod?saleID=" + saleID
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
            }
            else {
                bRsl = true;
            }
        },
        error: function () {
            sweet_alert_info("Error", "Data was not saved");
        },
        complete: function () {
            if (bRsl) {
                sweet_alert_progressbar();
                sweet_alert_success('Saved!', 'Data saved successfully.');
				$("#ModalDataSR").modal('hide');
				var marketID = "";
				var weekIni = $('#cboSemanaIni').val();
				var weekEnd = $('#cboSemanaFin').val();
				var statusID = 0;//$('#selectStatusFilter').val();
				list(marketID, weekIni, weekEnd, statusID);
            }
        }
    })   
}

function SavePORejected() {
	saleID = $("#lblSaleID").text();
	commentary = $("#commentary").val();
    
    var sUrlApi = "/Sales/GetStatusRejectPO?saleID=" + saleID + "&commentary=" + commentary
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
		async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when saving the information. Try again.');
            }
            else {
                bRsl = true;
            }
        },
        error: function () {
            sweet_alert_info("Error", "Data was not saved");
        },
        complete: function () {
            if (bRsl) {
                sweet_alert_progressbar();
                sweet_alert_success('Saved!', 'Data saved successfully.');
                $("#exampleModal").modal('hide');
				$("#ModalDataSR").modal('hide');
				var marketID = "";
				var weekIni = $('#cboSemanaIni').val();
				var weekEnd = $('#cboSemanaFin').val();
				var statusID = 0;//$('#selectStatusFilter').val();
				list(marketID, weekIni, weekEnd, statusID);
            }
        }
    })    
	
}

function list(marketID, weekIni, weekEnd, statusID) {
	sweet_alert_progressbar();
	if (statusID == 'All') {
		statusID = 0;
	}
	var opt = 'lif';
	var val = marketID;
	var userID = statusID;
	$.ajax({
		type: "GET",
		url: "/Sales/GetAllFiltered?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + localStorage.campaignID,
		//async: false,
		headers: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0'
		},
		success: function (data) {
			var dataList = JSON.parse(data);

			if (data.length > 0) {
				listaSales(dataList);
				loadQuantityByStatus(marketID, weekIni, weekEnd);
				sweet_alert_progressbar_cerrar();
			}

		},
		error: function () {
			sweet_alert_info('Information', "There was an issue trying to list data.");
        }
	});
	
}

function cleanControls() {

	$('table#tblCatVar>tbody').empty();
	$('#lblSaleID').text("0");

	$('#btnEdit').addClass('display-none')
	$('#btnAdd').removeClass('display-none')

	$('#btnRejectPOModified').addClass('display-none')
	$('#btnSavePOModified').addClass('display-none')
}

//function clearControlsShipping() {
//	$(selectCustomerS).selectpicker('val', '0');
//	$(selectMarketS).selectpicker('val', '0');
//	$(selectDestinationS).selectpicker('val', '0');
//	$(selectConsigneeS).selectpicker('val', '0');
//	$(selectCertificationS).selectpicker('val', '0');

//	$(chkDirectInvoiceS).prop('checked', false);

//	$(txtDocumentsInfoS).val('');

//	$(chkPackingListS).prop('checked', false);

//	$(selectPackingListS).selectpicker('val', '0');

//	$(chkInsuranceCertificateS).prop('checked', false);

//	$(chkPhytosanitaryS).prop('checked', false);

//	$(txtPhytosanitaryInfoS).val('');

//	$(chkCertificateOriginS).prop('checked', false);

//	$(txtCertificateOriginInfoS).val('');

//	$(selectConsigneeS).selectpicker('val', '0');
//	$(txtConsigneeAddress).val('');
//	$(txtConsigneeContact).val('');
//	$(txtConsigneePhone).val('');

//	$(selectNotify1).selectpicker('val', '0');
//	$(txtNotify1Address).val('');
//	$(txtNotify1Contact).val('');
//	$(txtNotify1Phone).val('');

//	$(selectNotify2).selectpicker('val', '0');
//	$(txtNotify2Address).val('');
//	$(txtNotify2Contact).val('');
//	$(txtNotify2Phone).val('');

//	$(selectNotify3).selectpicker('val', '0');
//	$(txtNotify3Address).val('');
//	$(txtNotify3Contact).val('');
//	$(txtNotify3Phone).val('');

//	$(txtDocsCopyEmailInfo).val('');
//	$(txtNoted).val('');
//}
/*
function openModalShipping() {
	clearControlsShipping();
	var wowID = $('#selectConsignee').val();
	var opt = 'id';

	$.ajax({
		type: "GET",
		url: "/Wow/GetById?opt=" + opt + "&id=" + wowID + "&cropID=" + localStorage.cropID,
		async: false,
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		success: function (data) {
			var xJson = JSON.parse(data)
			if (xJson.length > 0) {
				console.log(data);
				var dataJson = JSON.parse(data)[0];
				$(lblWowID).text(dataJson.wowID);
				$('#selectCustomerS').val(dataJson.customerID).change();
				$('#selectMarketS').val(dataJson.marketID.trim()).change();
				$('#selectDestinationS').val(dataJson.destinationID).change();
				$('#selectConsigneeS').val(dataJson.consigneeID).change();
				$('#selectCertificationS').val(dataJson.certificationID.split(',')).change();
				$('#chkDirectInvoiceS').prop('checked', dataJson.directInvoice);
				$('#txtDirectInvoiceMoreInfoS').val(dataJson.directInvoiceMoreInfo);

				$('#chkPackingListS').prop('checked', dataJson.packingListID);
				$('#selectDocumentsSeaS').val(dataJson.documentExportIDSea).change();
				$('#selectDocumentsAirS').val(dataJson.documentExportIDAir).change();
				$('#txtDocumentsInfoS').val(dataJson.documentsInfo);

				$('#txtMoreDocumentsS').val(dataJson.moreInfo).change();
				$('#txtPLMoreInfoS').val(dataJson.packingListMoreInfoS).change();
				$('#selectPackingListS').val(dataJson.packingList).change();
				$('#chkInsuranceCertificateS').prop('checked', dataJson.insuranceCertificate);

				$('#chkPhytosanitaryS').prop('checked', dataJson.phytosanitary);
				$('#txtPhytosanitaryInfoS').val(dataJson.phytosanitaryInfo)

				$('#chkCertificateOriginS').prop('checked', dataJson.certificateOrigin).change();
				$('#txtCertificateOriginInfoS').val(dataJson.certificateOriginInfo)

				$('#selectConsignee2').val(dataJson.consigneeID).change();
				$('#txtConsigneeAddress').val(dataJson.csGaddress)
				$('#txtConsigneeContact').val(dataJson.csGcontact)
				$('#txtConsigneePhone').val(dataJson.csGphone)
				$('#txtConsigneeOther').val(dataJson.csGmoreinfo)

				$('#selectNotify1').val(dataJson.ntF1notifyID).change();
				$('#txtNotify1Address').val(dataJson.ntF1address)
				$('#txtNotify1Contact').val(dataJson.ntF1contact)
				$('#txtNotify1Phone').val(dataJson.ntF1phone)
				$('#txtNotify1Other').val(dataJson.ntF1moreinfo)

				$('#selectNotify2').val(dataJson.ntF2notifyID).change();
				$('#txtNotify2Address').val(dataJson.ntF2address)
				$('#txtNotify2Contact').val(dataJson.ntF2contact)
				$('#txtNotify2Phone').val(dataJson.ntF2phone)
				$('#txtNotify2Other').val(dataJson.ntF2moreinfo)

				$('#selectNotify3').val(dataJson.ntF3notifyID).change();
				$('#txtNotify3Address').val(dataJson.ntF3address)
				$('#txtNotify3Contact').val(dataJson.ntF3contact)
				$('#txtNotify3Phone').val(dataJson.ntF3phone)
				$('#txtNotify3Other').val(dataJson.ntF3moreinfo)

				$('#txtDocsCopyEmailInfo').val(dataJson.docsCopyEmailInfo)
				$('#txtNoted').val(dataJson.noted)
			}
		}
	});
}*/

function ShowDataSR(id) {
	//cleanPackingList();
	cleanControls();
	if (id == 0) {
		loadDefaultValues();
		$("#txtcancel").text("Cancel");
	}
	else {
		var opt = "det";
		var val = id;
		var weekIni = 0;
		var weekEnd = 0;
		var userID = 0;
		var campaignID = 0;
		$("#txtcancel").text("Reject");

		$.ajax({
			type: "GET",
			url: "/Sales/ListSaleDetailsByID?opt=" + opt + "&val=" + val + "&weekIni=" + weekIni + "&weekEnd=" + weekEnd + "&userID=" + userID + "&campaignID=" + campaignID,
			async: true,
			headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
			success: function (data) {
				if (data.length > 0) {
					var dataJson = JSON.parse(data);

					_orderProductionID = dataJson[0].orderProductionID;
					_statusID = dataJson[0].statusID;

					//si el estado es rechazado ocultamos los comentarios y los botones de grabar y rechazar
					if (_statusID == 2) {
						$('.showcomments').hide();
						$("#txtcancel").text("Close");
						$(btnSavePO).removeClass();
						$(btnSavePO).addClass('btn btn-secondary btn-sm btn-icon-split transact pull-right');
					} else {
						$('.showcomments').show();
						$(btnSavePO).removeClass();
						$(btnSavePO).addClass('btn btn-success btn-sm btn-icon-split transact pull-right');
					}
					$(txtPriority).val(dataJson[0].priority);
					$('#selectMarket').val(dataJson[0].marketID.trim()).change();
					$('#selectCustomer').val(dataJson[0].customerID).change();

					$('#selectWeek').val(dataJson[0].projectedWeekID).change();
					$('#selectDestination').val(dataJson[0].destinationID).change();
					$('#selectVia').val(dataJson[0].viaID).change();
					$('#selectCodepack').val(dataJson[0].codePackID).change();

					$('#selectCategory').val(dataJson[0].categoryID).change();
					$('#selectVariety').val(dataJson[0].varietyID).change();
					$('#selectPackage').val(dataJson[0].packageProductID).change();

					$('#selectPriceContition').val(dataJson[0].conditionPaymentID).change();
					$('#selectIncoterm').val(dataJson[0].incotermID).change();
					$('#selectIncoterm').selectpicker("refresh");

					$('#txtPrice').val(dataJson[0].price).change();
					$('#txtBoxes').val(dataJson[0].quantity).change();
					$('#txtNet').val(dataJson[0].net);
					$('#txtPaymentTerm').val(dataJson[0].paymentTerm);
					$('#lblSaleID').text(dataJson[0].saleID);					

					$('#selectConsignee').val(dataJson[0].wowID);
					$('#selectConsignee').selectpicker("refresh");

					$('#txtStatus').val(dataJson[0].status);
					$('#txtComments').val(dataJson[0].commentary);
					$('#txtUserCreated').val(dataJson[0].userCreated);
					$('#txtDateCreated').val(dataJson[0].dateCreated);

					$('#txtUserModify').val(dataJson[0].userModify);
					$('#txtDateModify').val(dataJson[0].dateModified);
					$('#txtPO').val(dataJson[0].orderProduction);
					$('#txtCodePack').val(dataJson[0].codepack2);
					$('#txtCodePack').val(dataJson[0].codepack2);
					
					$('#RejectionComments').text(dataJson[0].commentaryRejection);
					$('#txtUserRejection').val(dataJson[0].userRejection);
					$('#txtDateRejection').val(dataJson[0].dateRejection);

					$(txtSaleRequest).val(dataJson[0].referenceNumber);
					$(txtSalesOrders).val(dataJson[0].orderProduction);
					
					if ($('#selectCustomer option:selected').text() == CustomerToBeConfirm) {
						$('#selectMarket').prop("disabled", false);
						$('#selectCustomer').prop("disabled", false);
						$('#selectVia').prop("disabled", false);
					} else {
						//$('#selectMarket').prop("disabled", true);
						//$('#selectCustomer').prop("disabled", true);
						$('#selectVia').prop("disabled", true);
                    }
					
					$('#selectDestination').prop("disabled", false);					
					$('#selectConsignee').prop("disabled", false);
					$('#selectDestinationPort').prop("disabled", true);
					$('table#tblCatVar>tbody').empty()
					$.each(dataJson, function (i, e) {
						var content = '<tr class="text-align-right table-tr-agrocom" >';
						content += '<td  style="text-align:center">';
						content += '<button class="btn btn-primary btn-sm  row-edit" ><i class="fas fa-edit"></i></button>';
						content += '&nbsp;';
						content += '<button class="btn btn-danger btn-sm  row-remove" ><i class="fas fa-trash"></i></button></td>';			
						content += '<td style="display:none">' + e.saleDetailID + '</td>';
						content += '<td style="display:none">' + e.categoryID + '</td>';
						content += '<td >' + e.category + '</td >';
						content += '<td style="display:none">' + e.varietyID + '</td>';
						content += '<td > <textarea rows="1" class="variety form-control form-control-sm" readonly>' + e.variety + '</textarea></td >';
						content += '<td >' + e.size + '</td>';
						content += '<td style="display:none">' + e.codePackID + '</td>';
						content += '<td > ' + e.codepack + '</td >';
						content += '<td style="display:none">' + e.presentationID + '</td>';
						content += '<td > ' + e.presentation + '</td >';
						content += '<td>' + e.quantity + '</td>';
						content += '<td style = "max-width: 70px;min-width: 70px" class="trBoxesMin" hidden>' + e.quantityMinimum + '</td >'; 
						content += '<td style = "display:none"> ' + e.flexible + '</td >';
						content += '<td > ' + e.net + '</td >'; 
						content += '<td > ' + e.price + '</td >';
						content += '<td style="display:none">' + e.packageProductID + '</td >';
						content += '<td style="display:none">' + e.incotermID + '</td>';
						content += '<td > ' + e.incoterm + '</td >';
						content += '<td style="display:none">' + e.conditionPaymentID + '</td>';
						content += '<td > ' + e.conditionPayment + '</td >';
						content += '<td ><textarea rows="1" class="payment form-control form-control-sm" readonly>' + e.paymentTerm + '</textarea></td>';
						content += '<td class="tdFinalCustomer"><textarea rows="1" class="final form-control form-control-sm" readonly>' + e.finalCustomer + '</textarea></td>';
						content += '<td class="tdPallet">' + e.pallet.toFixed(2) + '</td>';
						content += '<td class="tdPrice">' + e.totalPrice.toFixed(2) + '</td>';						
						content += '<td class="tdFinalCustomerID" style="display:none">' + e.finalCustomerID + '</td>';
						content += '<td class="tdPaymentTermID" style="display:none">' + e.paymentTermID + '</td>';
						content += '<td class="tdcodePack2" style="display:none">' + e.codepack2 + '</td>';
						content += '<td class="tdworkOrder" style="display:none">' + e.workOrder + '</td>';
						content += '</tr>';
						$("table#tblCatVar tbody").append(content);
					})

					calcTotal();
					loadDetailModified()
				}

				if ($('#txtStatus').val() == 'REJECTED') {
					$('div.modal-footer').addClass('display-none')
				} else $('div.modal-footer').removeClass('display-none');
			}
		});
	}

}

function SaveDataSR() {

	if (!obligatory()) {
		return
	}

	var vcustomerFinal = customerFinal()

	$('#spanObligatory').text('');
	if ($('#tblCatVar>tbody>tr').length == 0) {
		$('#spanObligatory').text('Insert Details');
		return;
	}

	var saleID = $("#lblSaleID").text();

	var destinationID = $("#selectDestination option:selected").val()
	var customerID = $("#selectCustomer option:selected").val()
	var projectedWeekID = $("#selectWeek option:selected").val()
	var wowID = $("#selectConsignee option:selected").val()

	var comments = $('#txtComments').val()
	if (wowID === '') { wowID = '0' }
	var details = []

	var details2 = []

	$('#tblCatVar>tbody>tr').each(function (i, e) {

		var saleDetailID = $(e).find('td:eq(1)').text();
		var categoryID = $(e).find('td:eq(2)').text();
		var varietyID = $(e).find('td:eq(4)').text();
		var sizeID = $(e).find('td:eq(6)').text();
		var codePackID = $(e).find('td:eq(7)').text();
		var presentationID = $(e).find('td:eq(9)').text();

		var quantity = $(e).find('td:eq(11)').text();
		var quantityMinimun = $(e).find('td:eq(12)').text();
		var flexible = $(e).find('td:eq(13)').text();
		var price = $(e).find('td:eq(15)').text();

		var packageProductID = $(e).find('td:eq(16)').text();
		var incotermID = $(e).find('td:eq(17)').text();
		var conditionPaymentID = $(e).find('td:eq(19)').text();		
		var paymentTerm = $(e).find('td:eq(21)').text();		

		var finalCustomerID = $(e).find('td.tdFinalCustomerID').text();
		var paymentTermID = $(e).find('td.tdPaymentTermID').text();
		var codePack2 = $(e).find('td.tdcodePack2').text();
		var workOrder = $(e).find('td.tdworkOrder').text();//$(e).find('#chkWorkOrder').is(':checked') ? 1 : 0; 

		if (finalCustomerID == "") finalCustomerID = 0

		var detail = {
			"int1": quantity, "int2": conditionPaymentID, "int3": incotermID, "int4": codePackID, "int5": saleDetailID, "int6": presentationID,
			"description1": categoryID, "description2": varietyID, "description3": paymentTerm, "description4": sizeID, "description5": finalCustomerID,
			"decimal1": price, "decimal2": paymentTermID, "decimal3": 0, "decimal4": 0
		}
		var detail2 = {
			"quantity": quantity, "conditionPaymentID": conditionPaymentID, "incotermID": incotermID, "codePackID": codePackID, "saleDetailID": saleDetailID,
			"presentationID": presentationID, "categoryID": categoryID, "varieties": varietyID, "paymentTerm": paymentTerm, "sizes": sizeID, "packageProductID": packageProductID,
			"finalCustomerID": finalCustomerID, "price": price, "paymentTermID": paymentTermID, "quantityMinimun": quantityMinimun, "flexible": flexible, "codePack": codePack2, "workOrder": workOrder 
		}
		details.push(detail);
		details2.push(detail2);

	})
	var sales = false;
	var saleIDSaved = 0;
	var userID = $("#lblSessionUserID").text();
	var o = {}
	o.saleID = saleID
	o.destinationID = destinationID
	o.projectedWeekID = projectedWeekID
	o.customerID = customerID
	o.userCreated = userID
	o.wowID = wowID
	o.commentary = comments
	//if (saleID == 0) { o.statusID = 4 }
	//else { o.statusID = _statusID }
	o.statusID = $('#txtPriority').val();
	o.campaignID = localStorage.campaignID
	o.details = details2

	var api = "/Sales/SaveSaleEN"
	$.ajax({
		type: 'POST',
		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
		url: api,
		contentType: "application/json",
		dataType: 'json',
		data: JSON.stringify(o),
		success: function (datares) {
			if (datares.length > 0) {
				sales = true;
				saleIDSaved = datares[0].saleID
				var e = datares[0];
				var color = ''
				if (e.statusID == 1) {
					color = 'badge-warning';
				}
				if (e.statusID == 2) {
					color = 'badge-danger';
				}
				if (e.statusID == 4) {
					color = 'badge-warning';
				}
				if (e.statusID == 5) {
					color = 'badge-primary';
				}
				if (e.statusID == 6) {
					color = 'badge-success';
				}
				if (e.statusID == 8) {
					color = 'badge-secondary';
				}
				var tr = "";
				tr += "<td style='text-align:center;  min-width: 100px !important;'>";
				tr += "<button class='btn  btn-sm btn-primary' style='' onclick='ShowDataSR(" + e.saleID + ")' data-toggle='modal' data-target='#ModalDataSR'><span class='icon '><i class='fas fa-edit fa-sm'></i></span></button> "
				tr += "<button class='btn btn-sm btn-danger' style='' onclick='deletesr(this)' ><span class='icon '><i class='fas fa-trash-alt fa-sm'></i></span></button> "
				tr += "</td>"
				tr += "<td style='display:none' class='saleID'>" + e.saleID + "</td>"
				tr += "<td style='max-width: 100px; min-width:100px' hidden>" + e.referenceNumber + "</td>"
				tr += "<td style='max-width: 100px; min-width:100px'>" + e.pO_NUMBER + "</td>"
				tr += "<td style='max-width: 60px; min-width:60px'>" + e.week + "</td>"
				tr += "<td style='max-width: 110px; min-width:110px'>" + e.market + "</td>"
				tr += "<td style='max-width: 200px; min-width:200px'><textarea class='form-control form-control-sm' rows='1' readonly>" + e.customer + "</textarea></td>"
				tr += "<td style='max-width: 100px; min-width:100px'>" + e.destination + "</td>"
				tr += "<td style='max-width: 30px; min-width:30px'>" + e.via + "</td>"				
				tr += "<td style='max-width: 100px; min-width:100px'>" + e.brand + "</td>"
				tr += "<td style='max-width: 200px; min-width:200px'><textarea class='form-control form-control-sm' rows='1' readonly>" + e.varety + "</textarea></td>"
				tr += "<td style='max-width: 150px; min-width:150px'>" + e.codepack + "</td>"
				tr += "<td style='max-width: 50px; min-width:50px'>" + e.quantity + "</td>"
				tr += "<td class='statusID' style='max-width: 50px; min-width:50px;font-size:12px!important' hidden>" + e.statusID + "</td>"
				tr += "<td style='max-width: 120px; min-width:120px'><div class='badge " + color + " badge-pill'>" + e.status + "</div></td>"
				tr += "<td style='max-width: 60px; min-width:60px'>" + e.incoterm + "</td>"
				tr += "<td style='max-width: 100px; min-width:100px'>" + e.user + "</td>"

				if (o.saleID == 0) {
					$("#tblSales>tbody").append("<tr style='font-size: 12px'>" + tr + "</tr>");
				} else {
					$("#tblSales>tbody>tr>td.saleID").each(function (i, e) {
						if ($(e).text() == o.saleID) {
							$(e).closest('tr').html(tr)
						}
					})

				}
				var marketID = $("#selectMarketFilter").val();
				var weekIni = $('#cboSemanaIni').val();
				var weekEnd = $('#cboSemanaFin').val();
				loadQuantityByStatus(marketID, weekIni, weekEnd);

			} else {

			}

			if (sales) {
				if (vcustomerFinal == true) {
					sweet_alert_success('Success!', 'Sale Request Saved!');
				} else {
					sweet_alert_success('Success!', "All end customers are ozblu marketing. Don't forget to update them later.Sale Request Saved!");
				}
				$('#ModalDataSR').modal('hide');
				if (o.saleID == 0) {
					var marketID = [];
					$('#selectMarketFilter :selected').each(function (i, selected) {
						marketID[i] = $(selected).val();
					});
					var weekIni = $('#cboSemanaIni').val();
					var weekEnd = $('#cboSemanaFin').val();
					var statusID = $('#selectStatusFilter').val();

					list(marketID, weekIni, weekEnd, statusID);
				}
				cleanControls();
			}
		}
	});
}

function listaSales(data) {
	$('#tblSales').DataTable().clear().destroy()
	$("#tblSales>tbody").empty()
	var color = '';
	var tr = '';
	$.each(data, function (i, e) {
		if (e.statusID == 1) {
			tr += '<tr  class="table-warning">';
			color = 'badge-warning';
		}
		if (e.statusID == 2) {
			tr += '<tr  class="table-danger">';
			color = 'badge-danger';
		}
		if (e.statusID == 4) {
			tr += '<tr  class="table-warning">';
			color = 'badge-warning';
		}
		if (e.statusID == 5) {
			tr += '<tr  class="table-primary">';
			color = 'badge-primary';
		}
		if (e.statusID == 6) {
			tr += '<tr  class="table-success">';
			color = 'badge-success';
		}
		if (e.statusID == 8) {
			tr += '<tr  class="table-secondary">';
			color = 'badge-secondary';
		}
		tr += "<td style='text-align:center;  min-width: 100px !important;'>";
		tr += "<button class='btn btn-sm btn-primary' style='' onclick='ShowDataSR(" + e.saleID + ")' data-toggle='modal' data-target='#ModalDataSR'><span class='icon '><i class='fas fa-edit fa-sm'></i></span></button> "
		tr += "<button class='btn btn-sm btn-danger'  style='' onclick='deletesr(this)' ><span class='icon'><i class='fas fa-trash-alt fa-sm'></i></span></button> "
		tr += "</td>"
		tr += "<td style='display:none' class='saleID'>" + e.saleID + "</td>"
		tr += "<td style='max-width: 100px; min-width:100px' hidden>" + e.referenceNumber + "</td>"
		tr += "<td style='max-width: 100px; min-width:100px'>" + e.pO_NUMBER + "</td>"
		tr += "<td style='max-width: 60px; min-width:60px'>" + e.week + "</td>"
		tr += "<td style='max-width: 110px; min-width:110px'>" + e.market + "</td>"
		tr += "<td style='max-width: 200px; min-width:200px'><textarea class='form-control form-control-sm' rows='1' readonly>" + e.customer + "</textarea></td>"
		tr += "<td style='max-width: 100px; min-width:100px'>" + e.destination + "</td>"
		tr += "<td style='max-width: 30px; min-width:30px'>" + e.via + "</td>"
		tr += "<td style='max-width: 100px; min-width:100px'>" + e.brand + "</td>"
		tr += "<td style='max-width: 200px; min-width:200px'><textarea class='form-control form-control-sm' rows='1' readonly>" + e.varety + "</textarea></td>"
		tr += "<td style='max-width: 150px; min-width:150px'>" + e.codepack + "</td>"
		tr += "<td style='max-width: 50px; min-width:50px'>" + e.quantity + "</td>"
		tr += "<td class='statusID' style='max-width: 50px; min-width:50px;font-size:12px!important' hidden>" + e.statusID + "</td>"
		tr += "<td style='font-size: 14px; max-width: 120px; min-width:120px'><div id='status' class='badge " + color + " badge-pill'>" + e.status + "</div></td>"
		tr += "<td style='max-width: 60px; min-width:60px'>" + e.incoterm + "</td>"
		tr += "<td style='max-width: 100px; min-width:100px'>" + e.user + "</td>"
		tr += "</tr>"		
	});
	$("table#tblSales tbody").empty().append(tr);

	$("#tblSales").dataTable({
		"paging": true,
		"ordering": false,
		"info": true,
		"responsive": true,
		//"destroy": true,
		dom: 'Bfrtip',
		//select: true,
		lengthMenu: [
			[10, 25, 50, -1],
			['10 rows', '25 rows', '50 rows', 'All']
		],
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
				exportOptions: {
					columns: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15]
				}
			},
			'pageLength'
		],
		
	});
}

function deletesr(tr) {
	let statusID = $(tr).closest('tr').find('td.statusID').text();
	if (statusID != 6) {
		deleteSalesRequest($(tr).closest('tr'));
	} else {
		sweet_alert_error('Error', 'You cannot reject the sales order, your shipping instruction has been generated');
	}	
}

function deleteSalesRequest(tr) {
	
	Swal.fire({
			title: 'Are you sure?',
			text: "Are you sure you want to reject?",
			icon: 'warning',
			showCancelButton: true,
			dangerMode: true,
			confirmButtonText: 'Yes, Reject!',
			cancelButtonText: 'No, Cancel!',
			confirmButtonColor: '#4CAA42',
			cancelButtonColor: '#d33'
	}).then((result) => {
			if (result.value) {

				$.ajax({
					type: 'POST',
					headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
					url: "/Sales/DeleteSales?saleID=" + $(tr).find('td.saleID').text() + "&userID=" + $("#lblSessionUserID").text(),
					async: true,
					success: function (data) {
						var dataList = JSON.parse(data);
						if (data.length > 0) {
							if (dataList[0].success == 0) {

							} else {
								sweet_alert_success('Success!', 'Sales Order Rejected!');
								let marketID = "";
								let weekIni = $('#cboSemanaIni').val();
								let weekEnd = $('#cboSemanaFin').val();
								let statusID = 0;
								list(marketID, weekIni, weekEnd, statusID);
								//var table = $('table#tblSales').DataTable();
								//table
								//	.row($(tr))
								//	.remove()
								//	.draw();
							}
						}
					}
				});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				sweet_alert_error('Cancel!', 'Reject process was canceled.')
			}
	});	
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function AjaxGetPaymentTerm(customerID) {
	$.ajax({
		type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		url: "/Sales/ListPaymentTermByCustomer?customerID=" + customerID + "&cropiD=" + localStorage.cropID,
		async: true,
		success: function (data) {
			if (data.length > 0) {
				var dataList = JSON.parse(data);
				dataJson = dataList.map(
					obj => {
						return {
							"id": obj.paymentTermID,
							"name": obj.description
						}
					}
				);
			}

			loadCombo(dataJson, 'selectPayment', true)
			$(selectPayment).selectpicker('refresh');
		}
	});
}

function AjaxDeleteSR(tr) {
	var rpt = []
	$.ajax({
		type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
		url: "/Sales/DeleteSales?saleID=" + $(tr).find('td.saleID').text() + "&userID=" + $("#lblSessionUserID").text(),
		async: true,
		success: function (data) {
			var dataList = JSON.parse(data);

			if (data.length > 0) {
				dataList
				var table = $('table#tblSales').DataTable();

				table
					.row($(tr))
					.remove()
					.draw();
			}

		}
	});
}

function obligatory() {
	$('#spanObligatory').text('');
	var response = false;

	var market = $("#selectMarket option:selected").val()
	if (market == "") {
		$('#spanObligatory').text('choose Market');
		return response;
	}

	var customer = $("#selectCustomer option:selected").val()
	if (customer == "") {
		$('#spanObligatory').text('choose Customer');
		return response;
	}

	var customer = $("#selectWeek option:selected").val()
	if (customer == "") {
		$('#spanObligatory').text('choose Week');
		return response;
	}

	var destination = $("#selectDestination option:selected").val()
	if (destination == "") {
		$('#spanObligatory').text('choose Destination');
		return response;
	}

	var via = $("#selectVia option:selected").val()
	if (via == "") {
		$('#spanObligatory').text('choose Via');
		return response;
	}

	if ($("#selectVia option:selected").val() == 1) {
		var pallets = $('.totalPallets').text()
		if (pallets < 20) {
			$('#spanObligatory').text('you must complete 20 pallets');
			return response;
		} else if (pallets > 20) {
			$('#spanObligatory').text('you must complete only 20 pallets');
			return response;
		}
	}

	var priority = $("#txtPriority").val()
	if (priority = 0) {
		$('#spanObligatory').text('Enter Priority');
		return response;
	}

	$('#spanObligatory').text('');
	response = true;
	return response;
}

function customerFinal() {
	var valid = false;
	if ($("#selectCustomer option:selected").val() == "42") {
		valid = false;
		$("#tblCatVar>tbody>tr").each(function (i, e) {
			if ($(e).find('td.tdFinalCustomerID').text() != "42") {
				valid = true

				return valid;
			}
		})


	} else {
		valid = true;
	}
	return valid;
}

function obligatoryRow() {
	$('#spanObligatoryRow').text('');
	var response = false;

	var categoryID = $("#selectCategory").val();
	if (categoryID == '') {
		$('#spanObligatoryRow').text('choose brand');
		return response;
	}

	var varietyID = $("#selectVariety").val();
	if (varietyID == '') {
		$('#spanObligatoryRow').text('choose variety');
		return response;
	}

	var codepackID = $("#selectCodepack").val();
	if (codepackID == '') {
		$('#spanObligatoryRow').text('choose Codepack');
		return response;
	}

	var presenationID = $("#selectPresentation").val();
	if (presenationID == '') {
		$('#spanObligatoryRow').text('choose presentation');
		return response;
	}

	var packageProductID = $("#selectPackage").val();
	if (packageProductID == '') {
		$('#spanObligatoryRow').text('choose Type Box');
		return response;
	}

	var size = $("#selectSize").val()
	if (size.length == 0) {
		$('#spanObligatoryRow').text('choose Size');
		return response;
	}

	var priceCondition = $("#selectPriceContition option:selected")
	if ($(priceCondition).val() == 0) {
		$('#spanObligatoryRow').text('choose Price Condition');
		return response;
	}
	if ($(priceCondition).val() == '') {
		$('#spanObligatoryRow').text('choose Price Condition');
		return response;
	}

	var incoterm = $(selectIncoterm).selectpicker('val')
	if ($(incoterm).val() == 0 || incoterm == null) {
		$('#spanObligatoryRow').text('choose incoterm');
		return response;
	}

	var quantity = $("#txtBoxes").val();
	if (quantity <= 0) {
		$('#spanObligatoryRow').text('Enter Boxes');
		return response;
	}

	var Presentation = $("#selectPresentation").val()
	if (Presentation == null) {
		$('#spanObligatoryRow').text('choose codepack');
		return response;
	}
	if (Presentation.length == 0) {
		$('#spanObligatoryRow').text('choose codepack');
		return response;
	}

	if ($("#selectCustomer option:selected").val() != "0") {
		if ($("#selectFinalCustomer option:selected").val() == "") {
			$('#spanObligatoryRow').text('choose Final Customer ');
			return response;
		}
	} else {
		loadCombo([], 'selectFinalCustomer', true);
	}

	var paymentTerm = $(selectPayment).selectpicker('val')
	if ($(paymentTerm).val() == '' || paymentTerm == null) {
		$('#spanObligatoryRow').text('choose paymment Term');
		return response;
	}

	$('#spanObligatoryRow').text('');
	response = true;
	return response;
}

function AddCatVar() {
	if (!obligatoryRow()) {
		return;
	}
	var saleID = $("#lblSaleID");
	var categoryID = $("#selectCategory").val();
	var category = []
	$.each(dataBrand, function (i, e) {
		if (e.id == categoryID) {
			category.push(e.name)
		}
	})

	var varietyID = $("#selectVariety").val();
	var variety = [];
	$.each(varietyID, function (ii, ee) {
		$.each(dataVariety, function (i, e) {
			if (e.id == ee) {
				variety.push(e.name)
			}
		})
	})

	var codePackID = $("#selectCodepack").val();
	var codePack = [];
	$.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
		if (e.codePackID == codePackID) {
			codePack.push(e.description)
		}
	})

	var sizeID = $("#selectSize").val();
	var quantity = $("#txtBoxes");
	var quantityMinimun = $("#txtMinBoxes");
	var net = $("#txtNet");
	var price = $("#txtPrice");
	var packageProductID = $("#selectPackage option:selected");
	var conditionPaymentID = $("#selectPriceContition option:selected");
	var incotermID = $("#selectIncoterm option:selected");
	var paymentTerm = $(selectPayment).find("option:selected")
	var content = '<tr class="text-align-right">';

	var xchkFlexible = 0;
	if ($(chkFlexible).is(':checked')) {
		xchkFlexible = 1;
	}
	var presentation = $("#selectPresentation option:selected")

	var finalcustomerID = $("#selectFinalCustomer option:selected").val()
	var finalcustomer = ""
	if (finalcustomerID.length > 0) {
		finalcustomer = $("#selectFinalCustomer option:selected").text()
	}

	var workOrder = 0;
	if ($('#chkWorkOrder').is(":checked")) {
		workOrder = 1;
	}

	var pallets = $("#txtPallets").val();
	var totalPrice = $("#txtTotalPrice").val();
	var codepack2 = $("#txtCodePack").val();
	content += '<td style="text-align:left"> ';
	content += '<button class="btn btn-primary btn-sm  row-edit" ><i class="fas fa-edit"></i></button>';
	content += '&nbsp;';
	content += '<button class="btn btn-danger btn-sm  row-remove" ><i class="fas fa-trash"></i></button></td>';
	content += '<td style="display:none">' + "0" + '</td>';
	content += '<td style="display:none">' + categoryID + '</td>';
	content += '<td > ' + category + '</td >';
	content += '<td style="display:none">' + varietyID + '</td>';
	content += '<td ><textarea rows="1" class="variety form-control form-control-sm" readonly>' + variety + '</textarea></td >';
	content += '<td>' + sizeID + '</td>';
	content += '<td style="display:none">' + codePackID + '</td>';
	content += '<td > ' + codePack + '</td >';
	content += '<td style="display:none">' + $(presentation).val() + '</td>';
	content += '<td > ' + $(presentation).text() + '</td >';
	content += '<td >' + $(quantity).val() + '</td>';
	content += '<td style = "max-width: 60px;min-width: 60px" class="trBoxesMin" hidden > ' + $(quantityMinimun).val() + '</td >';
	content += '<td style = "display:none" > ' + xchkFlexible + '</td >';
	content += '<td > ' + $(net).val() + '</td >';
	content += '<td > ' + $(price).val() + '</td >';
	content += '<td style="display:none"> ' + $(packageProductID).val()  + '</td >';
	content += '<td style="display:none">' + $(incotermID).val() + '</td>';
	content += '<td > ' + $(incotermID).text() + '</td > ';
	content += '<td style="display:none">' + $(conditionPaymentID).val() + '</td>';
	content += '<td > ' + $(conditionPaymentID).text() + '</td >';	
	content += '<td ><textarea rows="1" class="payment form-control form-control-sm" readonly>' + $(paymentTerm).text() + '</textarea></td>';
	content += '<td class="tdFinalCustomer"><textarea rows="1" class="final form-control form-control-sm" readonly>' + finalcustomer + '</textarea></td>';
	content += '<td class="tdPallet">' + pallets + '</td>';
	content += '<td class="tdPrice">' + totalPrice + '</td>';	
	content += '<td class="tdFinalCustomerID" style="display:none">' + finalcustomerID + '</td>';
	content += '<td class="tdPaymentTermID" style="display:none" >' + $(paymentTerm).val() + '</td>';
	content += '<td class="tdcodePack2" style="display:none" >' + codepack2 + '</td>';
	content += '<td class="tdworkOrder" style="display:none" >' + workOrder + '</td>';
	content += '</tr>';
	$("table#tblCatVar tbody").append(content);
	calcTotal();
	clearDetails();
}

function clearDetails() {
	$(selectCategory).selectpicker('val', '0');
	$(selectVariety).selectpicker('val', '0');
	$(selectCodepack).selectpicker('val', '0');
	$(selectPresentation).selectpicker('val', '0');
	$(selectPackage).selectpicker('val', '0');
	$(selectSize).selectpicker('val', '0');
	$(selectPayment).selectpicker('val', '0');
	$(selectPriceContition).selectpicker('val', '0');
	$(selectIncoterm).selectpicker('val', '0');

	$(txtBoxPallet).val(0);
	$(txtBoxContainer).val(0);
	$(txtBoxes).val(0);
	$(txtPrice).val(0);
	$(txtMinBoxes).val(0);
	$(txtNet).val(0);
	$(txtPallets).val(0);
	$(txtCodePack).val('');
	$(chkFlexible).prop('checked', false).removeAttr('checked');
	//$(chkWorkOrder).prop('checked', true).removeAttr('checked');
}

//function loadDataS() {
//	$.ajax({
//		type: "GET",
//		url: "/Wow/ListDestinationAll?opt=all&id=1&mar=0&via=0",
//		async: false,
//		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
//		success: function (data) {
//			if (data.length > 0) {
//				dataDestinationS = JSON.parse(data);
//			}
//		}
//	});

//	$.ajax({
//		type: "GET",
//		url: "/Wow/ListMarket?opt=all&id=1",
//		async: false,
//		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
//		success: function (data) {
//			if (data.length > 0) {
//				dataMarketS = JSON.parse(data);
//				data3 = dataMarketS.map(
//					obj => {
//						return {
//							"id": obj.marketID,
//							"name": obj.name
//						}
//					}
//				);
//				loadCombo(data3, 'selectMarketS', false);
//				$('#selectMarketS').selectpicker('refresh');
//			}
//		}
//	});

//	var option = 'all';
//	$.ajax({
//		type: "GET",
//		url: "/Wow/ListAllWithMarket?opt=" + option + "&id= 1",
//		async: false,
//		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
//		success: function (data) {
//			if (data.length > 0) {
//				dataCustomerS = JSON.parse(data);
//				data2 = dataCustomerS.map(
//					obj => {
//						return {
//							"id": obj.customerID,
//							"name": obj.name
//						}
//					}
//				);
//				loadCombo(data2, 'selectCustomerS', false);
//				$('#selectCustomerS').selectpicker('refresh');
//				loadCombo(data2, 'selectNotify1', false);
//				$('#selectNotify1').selectpicker('refresh');
//				loadCombo(data2, 'selectNotify2', true);
//				$('#selectNotify2').selectpicker('refresh');
//				loadCombo(data2, 'selectNotify3', true);
//				$('#selectNotify3').selectpicker('refresh');
//				loadCombo(data2, 'selectConsigneeS', false);
//				$('#selectConsigneeS').selectpicker('refresh');
//				loadCombo(data2, 'selectConsignee2', false);
//				$('#selectConsignee2').selectpicker('refresh');
//			}
//		}
//	});

//	$.ajax({
//		type: "GET",
//		url: "/Wow/GetAllCetrifications?opt=all&id=0",
//		async: false,
//		headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
//		success: function (data) {
//			if (data.length > 0) {
//				dataCertificationS = JSON.parse(data);
//				data3 = dataCertificationS.map(
//					obj => {
//						return {
//							"id": obj.certificationID,
//							"name": obj.certification
//						}
//					}
//				);
//				loadCombo(data3, 'selectCertificationS', false);
//				$('#selectCertificationS').selectpicker('refresh');

//				data3 = dataDocumentsSeaS.map(
//					obj => {
//						return {
//							"id": obj.documentExportID,
//							"name": obj.description
//						}
//					}
//				);
//				loadCombo(data3, 'selectDocumentsSeaS', true);
//				$('#selectDocumentsSeaS').selectpicker('refresh');

//				data3 = dataDocumentsAirS.map(
//					obj => {
//						return {
//							"id": obj.documentExportID,
//							"name": obj.description
//						}
//					}
//				);
//				loadCombo(data3, 'selectDocumentsAirS', true);
//				$('#selectDocumentsAirS').selectpicker('refresh');

//				data3 = dataPackingListS.map(
//					obj => {
//						return {
//							"id": obj.packingListID,
//							"name": obj.description
//						}
//					}
//				);
//				loadCombo(data3, 'selectPackingListS', true);
//				$('#selectPackingListS').selectpicker('refresh');

//			}
//		}
//	});
//}

function toggleSelectAll(control) {
	var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
	function valuesOf(elements) {
		return $.map(elements, function (element) {
			return element.value;
		});
	}

	if (control.data('allOptionIsSelected') != allOptionIsSelected) {
		// User clicked 'All' option
		if (allOptionIsSelected) {
			// Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
			control.selectpicker('val', valuesOf(control.find('option')));
		} else {
			control.selectpicker('val', []);
		}
	} else {
		// User clicked other option
		if (allOptionIsSelected && control.val().length != control.find('option').length) {
			// All options were selected, user deselected one option
			// => unselect 'All' option
			control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
			allOptionIsSelected = false;
		} else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
			// Not all options were selected, user selected all options except 'All' option
			// => select 'All' option too
			control.selectpicker('val', valuesOf(control.find('option')));
			allOptionIsSelected = true;
		}
	}
	control.data('allOptionIsSelected', allOptionIsSelected);

}

$(document).ready(function () {
	$(".btn-primary").click(function () {
		$(".collapse").collapse('toggle');
	});

});