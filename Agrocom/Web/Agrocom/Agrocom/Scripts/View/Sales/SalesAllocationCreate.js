﻿var dataMartet = []
var dataFormat = []
var dataFormatMarket = []
var dataDestinationAll = []
var dataDestinationCustomer = []
var dataCustomerMarket = []
var dataBrand = []
var dataWeeks = [];
var idSeason = 2
var dataPriority = [
	{ "priorityID": 1, "priority": "Strategic" },
	{ "priorityID": 2, "priority": "Tactical" },
	{ "priorityID": 3, "priority": "Development" }
];
var tr = ''
var tdFormatID = ''
var tdFormat = ''
var tdWeight = ''
var tdBoxPerPallet = ''
var tdLabelID = ''
var tdLabel = ''
var tdWeight = ''
var marketId  = ''
$(function () {

	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListWeekBySeason?idSeason=" + idSeason,
		async: false,
		success: function (data) {
			dataWeeks = JSON.parse(data);
			//console.log(data);
		}
	});
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListMarket",
		async: false,
		success: function (data) {
			dataMartet = JSON.parse(data);
			console.log(data);
		}
	});

	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListCodepack",
		async: false,
		success: function (data) {
			dataFormat = JSON.parse(data);
			//console.log(data);
		}
	});
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListCodepackWithVia/?cropID=BLU",
		async: false,
		success: function (data) {
			dataFormatMarket = JSON.parse(data);
			//console.log(data);
		}
	});


	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListDestinationAll",
		async: false,
		success: function (data) {
			dataDestinationAll = JSON.parse(data);
		}
	});
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListDestinationAllWithCustomer",
		async: false,
		success: function (data) {
			dataDestinationCustomer = JSON.parse(data);
		}
	});
	$.ajax({
		type: "GET",
		url: "/CommercialPlan/ListAllWithMarket",
		async: false,
		success: function (data) {
			dataCustomerMarket = JSON.parse(data);
		}
	});

	$.ajax({
		type: "GET",
		url: "/Sales/ListBrand",
		async: false,
		success: function (data) {
			var dataJson = JSON.parse(data);
			if (data.length > 0) {
				dataBrand = dataJson.map(
					obj => {
						return {
							"id": obj.categoryID,
							"name": obj.description
						}
					}
				);
			}
		}
	});
	
	loadTablePromises();
	
	$(document).on("click", ".btnChangeLabel", function () {
		var labelId = $(this).closest('tr').find('td.tdSelectLabelIntableID').text()
		$("#selectChangeLabel").selectpicker('deselectAll')
		$("#selectChangeLabel").selectpicker('val', labelId.split(','))
		//tr = $(this).closest('tr');

		tdLabelID = $(this).closest('tr').find('td.tdSelectLabelIntableID')
		tdLabel = $(this).closest('tr').find('td.tdSelectLabelIntable')

		$(modalChangeLabel).show()
	})
	$(document).on("click", "#btnCloseChangeLabel", function () {
		$(modalChangeLabel).hide()
	})
	
	$(document).on("click", "#btnSaveChangeLabel", function () {
		


		var labelID = $("#selectChangeLabel").val()
		var label = []
		$.each(labelID, function (iii, eee) {
			$.each(dataBrand, function (ii, ee) {
				if (ee.id == eee) {
					label.push(ee.name);
				}
			})
		})
		$(tdLabelID).html(labelID.toString())
		$(tdLabel).html('<button class="btnChangeWeekLabel"><i class="fas fa-edit"></i></button>' + label.toString())
		//$(tr).find('td.tdSelectLabelIntable').html( labelID.toString())
		//$(tr).find('td.tdSelectLabelIntableID').html('<button data-toggle="modal" data-target="#modalChangeLabel" ><i class="fas fa-edit"></i></button>' +label.toString())

		var formatID = $("#selectChangeFormat").val()
		var format = []

		var codepackFilter = dataFormatMarket.filter(element => element.marketID.trim() == $marketId && element.viaID == "1");

		$.each(formatID, function (iii, eee) {
			$.each(codepackFilter, function (ii, ee) {
				if (ee.codePackID == eee) {
					format.push(ee.description);
				}
			})
		})


		$(tdFormat).html(format)
		$(tdFormatID).html(formatID)

		$(modalChangeLabel).hide()
	})

	

	$(document).on("click", ".btnChangeCustomer", function () {
		$(this).closest("div.tab-pane").find("select.selectCustomer").find('option').clone().appendTo('#selectChangeCustomer')
		$(this).closest("div.tab-pane").find("select.selectCustomer").find('option').clone().appendTo('#selectChangeConsignee')

		tr = $(this).closest('tr')
		var clientID= $(this).closest('tr').find('td.clientID').text()
		var consigneeID = $(this).closest('tr').find('td.consigneeID').text()

		$(selectChangeCustomer).val(clientID)
		$(selectChangeConsignee).val(consigneeID)
		
	})
	$(document).on("click", ".btnChangeWeekLabel", function () {
		//$(this).closest('td');
		marketId = $(this).closest("div.tab-pane").find("label.lblIdMarket").text()

		tdFormatID = $(this).closest('td').prev().prev().prev().prev().prev();
		tdFormat = $(this).closest('td').prev().prev().prev().prev();
		tdWeight = $(this).closest('td').prev().prev().prev();
		tdBoxPerPallet = $(this).closest('td').prev().prev();
		tdLabelID = $(this).closest('td').prev();
		tdLabel = $(this).closest('td');
		tdWeight = $(this).closest('td').next();

		var labelId = tdLabelID.text()

		$("#selectChangeLabel").selectpicker('deselectAll')
		$("#selectChangeLabel").selectpicker('val', labelId.split(','))

		$('#selectChangeFormat').empty().selectpicker('refresh')

		var formatID = tdFormatID.text()
		$(this).closest("div.tab-pane").find("select.selectFormat").find('option').clone().appendTo('select#selectChangeFormat')
		$('#selectChangeFormat').selectpicker('refresh')
		$("#selectChangeFormat").selectpicker('val', formatID.split(','))

		//tr = $(this).closest('tr')
		$(modalChangeLabel).show()
	})
	

	$(document).on("click", "#btnSaveChangeCustomer", function () {
		var selectChangeCustomer = $("#selectChangeCustomer option:selected").html();
		var selectChangeCustomerID = $("#selectChangeCustomer").val();

		var selectChangeConsignee = $("#selectChangeConsignee option:selected").html();
		var selectChangeConsigneeID = $("#selectChangeConsignee").val();

		$(tr).find('td.clientID').text(selectChangeCustomerID)
		$(tr).find('td.client').text(selectChangeCustomer)
		$(tr).find('td.consigneeID').text(selectChangeConsigneeID)
		$(tr).find('td.consignee').text(selectChangeConsignee)
	})
	

	$(document).on("click", ".btnAddClient", function () {
		$marketId = $(this).parent().parent().parent().parent().find("label.lblIdMarket").text();
		$idPlanAlternative = $(this).parent().parent().parent().parent().parent().parent().find("label#lblPlanAlternativeID").text();
		$divId = $(this).parent().parent().parent().parent().parent().parent().attr("id");
		console.log('------');
		console.log($idPlanAlternative);
		console.log($marketId);


		$idAlternative = $(this).attr('id').replace('btnAddClient-', '');
		$currenTblId = "tblClient-" + $idAlternative;

		$currentSelClientID = '#selectClient-' + $idAlternative;
		$clientText = $($currentSelClientID + " option:selected").text();
		$clientId = $($currentSelClientID).val();

		$currentPriorityID = '#selectPriority-' + $idAlternative;
		$priorityText = $($currentPriorityID + " option:selected").text();
		$priorityID = $($currentPriorityID).val();

		$currentFormatID = '#selectFormat-' + $idAlternative;
		$formatText = $($currentFormatID + " option:selected").text();
		$formatID = $($currentFormatID).val();

		$currentDestinationID = '#selectDestination-' + $idAlternative;
		$destinationText = $($currentDestinationID + " option:selected").text();
		$destinationID = $($currentDestinationID).val();

		$currentViaID = '#selectVia-' + $idAlternative;
		$viaText = $($currentViaID + " option:selected").text();
		$viaID = $($currentViaID).val();

		$currentLabelID = '#selectLabel-' + $idAlternative;
		$labelID = $($currentLabelID).val()
		$label = $($currentLabelID + " option:selected").text()

		console.log('-----');
		console.log($divId);
		var planCustomerRequestID = 0
		if ($divId == 'divPromises') {
			//planCustomerRequestID = saveplanCustomerRequestCreate(idPlan.trim(), $clientId, $destinationID, $formatID, $marketId, sessionUserID);
		} else {
			//allocation
			console.log('allocation');
			//planCustomerRequestID = SaveCustomerFormat($formatID, $idPlanAlternative, $marketId, $destinationID, $clientId, $priorityID, 1, 1, sessionUserID);
			//codepackID, alternativeID, marketID, destinationID, customerID, priorityID, wildcard, order, userID) 
		}


		//if (planCustomerRequestID == 0) {
		//	console.log('Ocurrió un error')
		//	return;
		//}

		var row = '<tr class="text-align-right trCustomerFormat">';
		row += '<td ><button><i class="fas fa-trash row-remove"></i></button><button><i class="fas fa-arrow-up row-up"></i></button><button><i class="fas fa-arrow-down row-down"></i></button><button data-toggle="modal" data-target="#modalChangeCustomer" class="btnChangeCustomer"><i class="fas fa-user-edit row-down"></i></button> </td>';
		row += '<td style="display:none">' + planCustomerRequestID + '</td><td class="sticky">' + $priorityText + '</td>';
		row += '<td style="display:none" class="clientID">' + $clientId + '</td><td class="sticky2 client">' + $clientText + '</td>';
		row += '<td style="display:none" class="consigneeID">' + $clientId + '</td><td class="sticky3 consignee">' + $clientText + '</td>';

		var codepackFilter = dataFormatMarket.filter(element => element.codePackID == $formatID && element.marketID.trim() == $marketId)[0];
		var countryFilter = dataDestinationAll.filter(element => element.destinationID == $destinationID)[0];

		row += '<td style="display:none">' + $formatID + '</td><td >' + $formatText + '</td><td >' + codepackFilter.weight + '</td>';
		row += '<td class="tdSelectLabelIntableID">' + $labelID +'</td>'
		row += '<td class="tdSelectLabelIntable"><button class="btnChangeLabel" ><i class="fas fa-edit"></i></button>' + $label+'</td>'

		if ($viaID == '1') {
			row += '<td >' + codepackFilter.boxPerContainer + '</td>';
		} else {
			row += '<td contenteditable="true">' + codepackFilter.boxesPerPallet + '</td>';
		}

		//console.log(JSON.stringify(codepackFilter));

		row += '<td style="display:none">' + $destinationID + '</td><td >' + $destinationText + '</td><td class="tdCountryId" style="display:none">' + countryFilter.originID + '</td>';
		row += '<td style="display:none">' + $viaID + '</td><td >' + $viaText + '</td>';

		for (var x = 0; x < dataWeeks.length; x++) {
			//row += '<td class="tdWeekFormatID"></td>'
			//row += '<td class="tdWeekFormat"></td>'
			row += '<td class="tdWeekFormatID">'+$formatID+'</td>'
			row += '<td class="tdWeekFormat">'+$formatText+'</td>'

			row += '<td class="tdWeightPerBox"></th>'
			row += '<td class="tdBoxPerPallet"></th>'
			row += '<td class="tdWeekLabelID">' + $labelID + '</td>'
			row += '<td class="tdWeekLabel" ><button class="btnChangeWeekLabel" ><i class="fas fa-edit"></i></button>' + $label + '</td>'
			row += '<td class="tdWeekBoxes" style="max-width: 80px;min-width: 80px"><input type="number" step="1" min="0" value="0" style="width:100%;text-align: right"/></td>'
			row += '<td class="tdWeekWeight">Weight</td>';
		}
		row += '</tr>';
		$("#" + $currenTblId + " tbody").append(row);

		$('.selectLabelIntable').selectpicker('refresh')
		//console.log('aaaaaa')
		//addSubTotalClient($currenTblId);
	});

});

function loadTablePromises() {

	var conntentPromises = ''
	var id = 'promises';

	conntentPromises += '<ul class="nav nav-pills mb-3 nav" id="pills-promises" role="tablist">';
	for (var i = 0; i < dataMartet.length; i++) {
		var active = 'false';
		var active2 = '';
		if (i == 0) { active = 'true'; active2 = 'active' };
		conntentPromises += '<li class="nav-item">';
		conntentPromises += '<a class="nav-link ' + active2 + '" id="pills-tab' + i + '" data-toggle="pill" href="#pills-' + id + '-' + dataMartet[i].marketID + '" role="tab" aria-controls="pills-' + dataMartet[i].name + '" aria-selected="' + active + '">' + dataMartet[i].name + '</a>';
		conntentPromises += '</li>';
	}
	conntentPromises += '</ul>';

	conntentPromises += '<div class="tab-content" id="pills-tabContent-' + id + '">';

	for (var i = 0; i < dataMartet.length; i++) {

		var promisesId = id + '-' + dataMartet[i].marketID;

		var active = '';
		if (i == 0) active = 'show active';
		conntentPromises += '<div class="tab-pane fade ' + active + '" id="pills-' + promisesId + '" role="tabpanel" aria-labelledby="pills-home-tab" style="border-color: #666666; border: groove;">';

		conntentPromises += '<label for="lblIdMarket-' + promisesId + '" ><h6 class=" font-weight-bold" bgcolor="#13213d">Market ID:&nbsp;&nbsp;</h6></label>'
		conntentPromises += '<label class="lblIdMarket" id="lblIdMarket-' + promisesId + '"><h6 bgcolor="#13213d">' + dataMartet[i].marketID + '</h6></label>';

		conntentPromises += '<form><div class="form-row">'

		conntentPromises += '<div class="form-group-sm col-2">'
		conntentPromises += '<label for="selectClient-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6></label> <a href="/Maintenance/Customer" target="_blank"><i class="fas fa-user-plus"></i></a>'
		conntentPromises += '<select class="custom-select  mr-sm-2 selectCustomer" id="selectClient-' + promisesId + '"> </select>'
		conntentPromises += '</div>';
		conntentPromises += '<div class="form-group-sm col-2">'
		conntentPromises += '<label for="selectPriority-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Priority</h6></label>'
		conntentPromises += '<select class="custom-select  mr-sm-2" id="selectPriority-' + promisesId + '"></select>'
		conntentPromises += '</div>';

		conntentPromises += '<div class="form-group-sm col-2">'
		conntentPromises += '<label for="selectDestination-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Destination</h6></label>'
		conntentPromises += '<select class="custom-select  mr-sm-2 selectDestination" id="selectDestination-' + promisesId + '"> </select>'
		conntentPromises += '</div>';
		conntentPromises += '<div class="form-group-sm col-2" style="display:none">'
		conntentPromises += '<label for="selectVia-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Via</h6></label>'
		conntentPromises += '<select class="custom-select  mr-sm-2 selectVia" id="selectVia-' + promisesId + '"> <option value="1" selected>Sea</option><option value="2" >Air</option> </select>'

		conntentPromises += '</div>';
		conntentPromises += '<div class="form-group-sm col-2">'
		conntentPromises += '<label for="selectFormat-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Format</h6></label>'
		conntentPromises += '<select class="custom-select  mr-sm-2 selectFormat" id="selectFormat-' + promisesId + '"> </select>'
		conntentPromises += '</div>';

		//conntentPromises += '</div>';
		conntentPromises += '<div class="form-group-sm col-2">'
		conntentPromises += '<label for="selectLabel-' + promisesId + '"><h6 class=" font-weight-bold" bgcolor="#13213d">Label</h6></label>'
		conntentPromises += '<select class="selectLabel selectpicker" id="selectLabel-' + promisesId + '" multiple data-live-search="true"> </select>'
		conntentPromises += '</div>';

		conntentPromises += '<div class="form-group-sm col-2">'
		conntentPromises += '<br><br><button type="button" class="btn btn-info mb-1 btnAddClient btn-sm" id="btnAddClient-' + promisesId + '">Add <span class="icon text-white"><i class="fas fa-plus"></i></span></button>';
		conntentPromises += '</div>';

		conntentPromises += '</div></form>';


		var tableCliente = '';
		tableCliente += '<br><table style="width:100%;min-height: 200px;" class ="table table-bordered table-cebra table-responsive tblClient-promises table-striped" id="tblClient-' + promisesId + '"> ';
		tableCliente += '<thead class="table-thead-agrocom">';
		tableCliente += '<tr >';
		tableCliente += '<th rowspan="2" style="color:white;background-color:#13213d; max-width:121px;min-width:121px;">[]</th>';
		tableCliente += '<th rowspan="2" style="display:none">IdPrioridad</th>';
		tableCliente += '<th rowspan="2" class="sticky" width="70%">Priority</th>';
		tableCliente += '<th rowspan="2" style="display:none">IdClient</th>';
		tableCliente += '<th rowspan="2" class="sticky2" style="color:white;background-color:#13213d; max-width:200px;min-width:200px;">Customer</th>';
		tableCliente += '<th rowspan="2" style="display:none">IdConsignee</th>';
		tableCliente += '<th rowspan="2" class="sticky3" style="color:white;background-color:#13213d; max-width:200px;min-width:200px;">Consignee</th>';
		tableCliente += '<th rowspan="2" style="display:none">IdCodepack</th>';
		tableCliente += '<th rowspan="2" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFormat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>';
		tableCliente += '<th rowspan="2" >Weight&nbspBox</th>';
		tableCliente += '<th rowspan="2" >LabelID</th>';
		tableCliente += '<th rowspan="2" style="min-width: 180px;">Label</th>';
		tableCliente += '<th rowspan="2" >Box&nbspContainer/Pallet</th>';
		tableCliente += '<th rowspan="2" style="display:none">IdDestino</th>';
		tableCliente += '<th rowspan="2" >Destination</th>';
		tableCliente += '<th rowspan="2" style="display:none">CountryIdId</th>';
		tableCliente += '<th rowspan="2" style="display:none">IdVia</th>';
		tableCliente += '<th rowspan="2" >&nbsp&nbsp&nbspVia&nbsp&nbsp&nbsp</th>';
		for (var x = 0; x < dataWeeks.length; x++) {
			tableCliente += "<th colspan='8'> Week " + dataWeeks[x].number.toString();
			tableCliente += "</th>";
		}

		tableCliente += '</tr>';
		tableCliente += '<tr>';
		for (var x = 0; x < dataWeeks.length; x++) {
			tableCliente += '<th>FormatID</th>'
			tableCliente += '<th>Format</th>'
			tableCliente += '<th>WeightPerBox</th>'
			tableCliente += '<th>BoxPerPallet</th>'
			tableCliente += '<th>LabelID</th>'
			tableCliente += '<th style="min-width:180px;max-width:180px">Label</th>'
			tableCliente += '<th>Boxes</th>'
			tableCliente += '<th>Weight</th>';			
		}
		tableCliente += '</tr>';


		tableCliente += '</thead>';
		tableCliente += '<tbody>'
		tableCliente += '</tbody>'
		tableCliente += '</table>';


		conntentPromises += tableCliente;
		conntentPromises += '<br/>';


		conntentPromises += '' + promisesId + ' -' + dataMartet[i].marketID + '</div>';
	}

	conntentPromises += '</div>';

	$("#divPromises").append(conntentPromises);


	$.each(dataMartet, function (index, element) {
		//let data = dataFormat.filter(element => element.viaID == "1").map(
		let data = dataFormatMarket.filter(item => item.viaID == "1" && item.marketID.trim() == element.marketID.trim()).map(
			obj => {
				return {
					"id": obj.codePackID,
					"name": obj.description
				}
			}
		);


		loadControlSelect($('#selectFormat-promises-' + element.marketID), data);


		console.log('dataCustomerMarket')
		console.log(JSON.stringify(dataCustomerMarket))
		$dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == element.marketID.trim());
		data = $dataCustomerFilter.map(
			obj => {
				return {
					"id": obj.customerID,
					"name": obj.customer
				}
			}
		);

		loadControlSelect($('#selectClient-promises-' + element.marketID), data);

		if ($dataCustomerFilter.length > 0) {
			$dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == $dataCustomerFilter[0].customerID && item.marketID.trim() == element.marketID.trim());
			data = $dataDestinationFilter.map(
				obj => {
					return {
						"id": obj.destinationID,
						"name": obj.destination
					}
				}
			);
			//loadControlSelect($('#selectDestination-' + element.planAlternativeIDMarketID), data);
			loadControlSelect($('#selectDestination-promises-' + element.marketID), data);
		}

		data = dataPriority.map(
			obj => {
				return {
					"id": obj.priorityID,
					"name": obj.priority
				}
			}
		);

		loadControlSelect($('#selectPriority-promises-' + element.marketID), data);
		loadControlSelect($('#selectLabel-promises-' + element.marketID), dataBrand);
		
		
		//var dataDestination = []
		//$.ajax({
		//	type: "GET",
		//	url: "/CommercialPlan/ListDestination?marketID=" + element.marketID,
		//	async: false,
		//	success: function (data) {
		//		dataDestination = JSON.parse(data);
		//	}
		//});

		//data = dataDestination.map(
		//	obj => {
		//		return {
		//			"id": obj.destinationID,
		//			"name": obj.origin.trim() + '-' + obj.description
		//		}
		//	}
		//);

		//loadControlSelect($('#selectDestination-promises-' + element.marketID), data);
	})
	loadControlSelect($(selectChangeLabel), dataBrand);
	//$("selectChangeLabel").selectpicker('refresh')
	$('.selectLabel').selectpicker('refresh')
}

function loadControlSelect(control, data) {
	var contenido = "";
	for (var i = 0; i < data.length; i++) {
		contenido += "<option value='" + data[i].id + "'>";
		contenido += data[i].name;
		contenido += "</option>";
	}

	$(control).append(contenido);
}