﻿var dataMarket = [];
var dataCustomer = [];
var dataCustomerMarket = [];
var dataConditionPayment = [];
var dataDestinationFilter = [];
var dataDestinationCustomer = [];
var dataVia = [];
var dataCodepack = [];
var dataDestinationPort = [];
var dataWow = [];
var dataBrand = [];
var dataSizes = [];
var dataPresentation = [];
var dataIncoterm = [
    {
        "id": 2,
        "name": "F.O.B.",
        "viaID": 1
    },
    {
        "id": 3,
        "name": "CFR",
        "viaID": 1
    },
    {
        "id": 5,
        "name": "CIF",
        "viaID": 1
    },
    {
        "id": 1,
        "name": "CPT",
        "viaID": 2
    },
    {
        "id": 2,
        "name": "F.O.B.",
        "viaID": 2
    }
]

$(function () {
    //$(document).ajaxStart(function () {
    //  $("#wait").css("display", "block");
    //  $("#loader").css("display", "block")

    //  $(".transact").attr("disabled", true);
    //});
    //$(document).ajaxComplete(function () {
    //  $("#wait").css("display", "none");
    //  $("#loader").css("display", "none")

    //  $(".transact").attr("disabled", false);
    //});

    loadData();
    loadDefaultValues();

    $('#selectMarket').change(function () {
        loadCombo([], 'selectCustomer', true);
        loadCombo([], 'selectCodepack', false);
        $('#selectCodepack').selectpicker('refresh');
        loadCombo(dataVia, 'selectVia', true);
        $('#selectVia').change()
        if (this.value == "") {
            return;
        }
        $('#selectVia').selectpicker('refresh');
        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == this.value.trim());//
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );
        loadCombo(data, 'selectCustomer', true);
        //debugger;
        $('#selectCustomer').selectpicker('refresh');
        //retur;
        loadFinalCustomer()
    });

    $('#selectCustomer').change(function () {
        loadCombo([], 'selectDestination', true);
        //loadCombo([], 'selectDestinationPort', true);


        if (this.value == "") {
            return;
        }

        if ($("#selectCustomer option:selected").val() > 0) {
            loadDataWow($("#selectCustomer option:selected").val())
            //loadComboConsignnee([], 'selectConsignee', true);
            if (this.value == "") {
                return;
            }
        }
        //AjaxGetPaymentTerm(this.value);

        //List Destination
        if ($dataCustomerFilter.length > 0) {

            $dataDestinationFilter = dataDestinationCustomer.filter(item => item.customerID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val());
            data = $dataDestinationFilter.map(
                obj => {
                    return {
                        "id": obj.destinationID,
                        "name": obj.destination
                    }
                }
            );
            loadCombo(data, 'selectDestination', true);
            $('#selectDestination').selectpicker('refresh');

            loadFinalCustomer()


        } else {
            loadCombo([], 'selectDestination', true);
            $('#selectDestination').selectpicker('refresh');
        }

    });

    $('#selectDestination').change(function () {
        $dataWowFilter = dataWow.filter(item => item.destinationID == this.value);
        data = $dataWowFilter.map(
            obj => {
                return {
                    "id": obj.wowID,
                    "name": obj.consignee
                }
            }
        );


        $('#selectVia').change(function () {
            if (this.value == "") {
                loadCombo([], 'selectCodepack', true);
                $('#selectCodepack').change().selectpicker('refresh');
                $('#selectCodepack').change();
                //loadCombo([], 'selectDestinationPort', true);
                loadCombo([], 'selectIncoterm', true);
                return;
            }

            let data = dataCodepack.filter(item => item.viaID == this.value && item.marketID.trim() == $("#selectMarket option:selected").val()).map(
                obj => {
                    return {
                        "id": obj.codePackID,
                        "name": obj.description
                    }
                }
            );
            loadCombo(data, 'selectCodepack', true);
            $('#selectCodepack').change().selectpicker('refresh');

            let data2 = dataDestinationPort.filter(item => item.viaID == $("#selectVia option:selected").val() && item.destinationID == $("#selectDestination option:selected").val())
            if (data2.length > 0) {
                loadCombo(data2, 'selectDestinationPort', false);
            } else {
                loadCombo([], 'selectDestinationPort', true);
            }

            let data3 = dataIncoterm.filter(item => item.viaID == $("#selectVia option:selected").val())
            if (data3.length > 0) {
                loadCombo(data3, 'selectIncoterm', true);
            } else {
                loadCombo([], 'selectIncoterm', true);
            }
            $('#selectIncoterm').selectpicker('refresh');
        });



        $('#txtBoxes').change(function () {
            var currentBoxes = $(this).val();

            //if (!$(chkFlexible).is(':checked')) {
            //  $(txtMinBoxes).val(currentBoxes)
            //}


            var codePackID = $("#selectCodepack").val();
            var codePack;
            $.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
                if (e.codePackID == codePackID) {
                    codePack = e
                }
            })

            var weight = parseInt(currentBoxes) * parseFloat(codePack.weight)
            $('#txtNet').val(weight.toFixed(2))
            //var currentFormat = dataCodepack.filter(element => element.codePackID =)

            var boxPallet = parseInt($('#txtBoxPallet').val());
            var pallets = parseInt(currentBoxes) / boxPallet;

            $('#txtPallets').val(pallets.toFixed(2))

            var currentPrice = parseFloat($('#txtPrice').val());
            var totalPrice = (currentPrice * currentBoxes).toFixed(2);

            $('#txtTotalPrice').val(totalPrice);
        })


        $('#selectCodepack').change(function () {
            $(txtBoxPallet).val(0);
            $(txtBoxes).val(0);
            $(txtNet).val(0);
            $(txtPallets).val(0);
            //$(txtTotalPrice).val(0);
            $(txtBoxes).prop("disabled", true);;
            if ($(selectCodepack).val() > 0) {
                var boxPallet = dataCodepack.filter(element => element.codePackID == $(selectCodepack).val())[0].boxesPerPallet
                $(txtBoxPallet).val(boxPallet);
                $(txtBoxes).attr("step", boxPallet);
                //$(txtMinBoxes).attr("step", boxPallet);
                $(txtBoxes).prop("disabled", false);;
            }

            let data = dataPresentation.filter(item => item.codePackID == $(selectCodepack).val()).map(
                obj => {
                    return {
                        "id": obj.presentationID,
                        "name": obj.presentation + obj.moreInfo
                    }
                });
            loadCombo(data, 'selectPresentation', false);
            $('#selectPresentation').change().selectpicker('refresh');
        });

        $(document).on("click", "#btnAdd", function () {
            AddCatVar();

        });


    })
})

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadDefaultValues() {
    loadCombo(dataMarket, 'selectMarket', true);
    $('#selectMarket').prop("disabled", false);
    $('#selectMarket').selectpicker('refresh');

    loadCombo([], 'selectCustomer', true);
    $('#selectCustomer').prop("disabled", false);
    $('#selectCustomer').selectpicker('refresh');

    loadCombo(dataConditionPayment, 'selectPriceCondition', false);
    $('#selectPriceContition').selectpicker('refresh');

    loadCombo([], 'selectDestination', true);
    $('#selectDestination').selectpicker('refresh');

    loadCombo([], 'selectVia', true);
    $('#selectVia').selectpicker('refresh');

    loadCombo([], 'selectIncoterm', true);
    $('#selectIncoterm').selectpicker('refresh');

    loadCombo([], 'selectCodepack', true);
    $('#selectCodepack').selectpicker('refresh');
    loadCombo([], 'selectDestinationPort', true);
    loadCombo(dataSizes, 'selectSize', false);
    $('#selectSize').selectpicker('refresh');
    $('#selectCodepack').change().selectpicker('refresh');

    loadCombo(dataBrand, 'selectCategory', true);
    //$('selectCategory').val('JBB');
    $('selectCategory').selectpicker('val', []).selectpicker('refresh');
    $('#selectCategory').selectpicker('refresh');


    $('#txtPrice').val('0')
    $('#txtBoxes').val('0')
    $('#txtNet').val('0')
    $('#txtPaymentTerm').val('')
    $('#txtBoxPallet').val('0')

    $('.totalPallets').html('0.00')
    $('.totalPrice').html('0.00')

    //$(txtSaleRequest).val('')
    //$(txtUserCreated).val('')
    //$(txtDateCreated).val('')
    //$(txtUserModify).val('')
    //$(txtDateModify).val('')
    //$(txtStatus).val('')
    //$(txtPO).val('')


}

function loadData() {

    $.ajax({
        type: "GET",
        url: "/Sales/ListMarket",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataMarket = dataJson.map(
                    obj => {
                        return {
                            "id": obj.marketID,
                            "name": obj.name
                        }
                    }
                );
            }
        }
    });
    //
    $.ajax({
        type: "GET",
        url: "/Sales/ListAllWithMarket",
        async: false,
        success: function (data) {
            dataCustomerMarket = JSON.parse(data);
        }
    });
    //
    $.ajax({
        type: "GET",
        url: "/Sales/ListConditionPayment",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataConditionPayment = dataJson.map(
                    obj => {
                        return {
                            "id": obj.conditionPaymentID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListDestinationAllWithCustomer",
        async: false,
        success: function (data) {
            dataDestinationCustomer = JSON.parse(data);
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/ListVia",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataVia = dataJson.map(
                    obj => {
                        return {
                            "id": obj.viaID,
                            "name": obj.name
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/ListCodepackWithVia/?cropID=BLU",
        async: false,
        success: function (data) {
            dataCodepack = JSON.parse(data);
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/GetAllPort",
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataDestinationPort = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationPortID,
                            "name": obj.port,
                            "viaID": obj.viaID,
                            "destinationID": obj.destinationID
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/ListBrand",
        async: false,
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataBrand = dataJson.map(
                    obj => {
                        return {
                            "id": obj.categoryID,
                            "name": obj.description
                        }
                    }
                );
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/GetAllSizes",
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataSizes = dataJson.map(
                    obj => {
                        return {
                            "id": obj.sizeID,
                            "name": obj.code
                        }
                    }
                );
            }
        }
    });


    $.ajax({
        type: "GET",
        url: "/Sales/GetCodepackWithPresentation",
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataPresentation = dataJson
            }
        }
    });


}


function loadFinalCustomer() {
    if ($("#selectCustomer option:selected").val() == "42") {
        $(selectFinalCustomer).show(1000)

        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == $("#selectMarket option:selected").val());
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );
        loadCombo(data, 'selectFinalCustomer', true);

        $("#labelFinalCustomer").show(100)
        $("#selectFinalCustomer").show(100)

    } else {
        $("#selectFinalCustomer").hide()
        $("#labelFinalCustomer").hide()
        loadCombo([], 'selectFinalCustomer', true);
    }

}

function loadDataWow(customerID) {
    $.ajax({
        type: "GET",
        url: "/Sales/ListWowByCustomer?customerID=" + customerID,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                dataWow = JSON.parse(data);
            }
        }
    });
}

function loadDataDestinationPort() {
    $.ajax({
        type: "GET",
        url: "/Sales/GetAllPort",
        async: false,
        success: function (data) {

            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataDestinationPort = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationPortID,
                            "name": obj.port,
                            "viaID": obj.viaID,
                            "destinationID": obj.destinationID
                        }
                    }
                );
            }
        }
    });

}

function AddCatVar() {
    var codePackID = $("#selectCodepack").val();
    var codePack = [];
    $.each(dataCodepack.filter(item => item.viaID == $("#selectVia option:selected").val() && item.marketID.trim() == $("#selectMarket option:selected").val()), function (i, e) {
        if (e.codePackID == codePackID) {
            codePack.push(e.description)
        }
    })

    var presentationID = $("#selectPresentation option:selected").val();
    var presentation = $("#selectPresentation option:selected").text();

    var categoryID = $("#selectCategory").val();

    var category = []
    $.each(dataBrand, function (i, e) {
        if (e.id == categoryID) {
            category.push(e.name)
        }
    })

    var sizeID = $("#selectSize").val();
    var boxPallet = $("#txtBoxPallet").val();
    var quantityBoxes = $("#txtBoxes").val();
    var net = $("#txtNet").val();
    var pallets = $("#txtPallets").val();
    var incotermID = $("#selectIncoterm option:selected").val();
    var incoterm = $("#selectIncoterm option:selected").text();
    var content = '<tr class="text-align-right">';
    var finalcustomerID = $("#selectFinalCustomer option:selected").val()
    var finalcustomer = ""
    if (finalcustomerID.length > 0) {
        finalcustomer = $("#selectFinalCustomer option:selected").text()
    }

    content += '<td> <span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td >';
    content += '<td>' + 0 + '</td>';
    content += '<td>' + codePackID + '</td>';
    content += '<td> ' + codePack + '</td > ';
    content += '<td>' + presentationID + '</td>';
    content += '<td>' + presentation + '</td > ';
    content += '<td>' + categoryID + '</td>';
    content += '<td>' + category + '</td > ';
    content += '<td>' + sizeID + '</td>';
    content += '<td>' + boxPallet + '</td>';
    content += '<td>' + quantityBoxes + '</td>';
    content += '<td>' + net + '</td>';
    content += '<td>' + incotermID + '</td>';
    content += '<td> ' + incoterm + '</td> ';
    content += '<td class="tdPallet">' + pallets + '</td>';
    content += '<td> ' + finalcustomerID + '</td> ';
    content += '<td> ' + finalcustomer + '</td> ';

    content += '</tr>';
    $("table#tblCatVar tbody").append(content);
    calcTotal();
}

function loadDataCustomer(marketID) {
    $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == this.value.trim());
    data = $dataCustomerFilter.map(
        obj => {
            return {
                "id": obj.customerID,
                "name": obj.customer
            }
        }
    );
}

function loadFinalCustomer() {
    if ($("#selectCustomer option:selected").val() == "42") {
        $(selectFinalCustomer).show(1000)

        $dataCustomerFilter = dataCustomerMarket.filter(item => item.marketID.trim() == $("#selectMarket option:selected").val());
        data = $dataCustomerFilter.map(
            obj => {
                return {
                    "id": obj.customerID,
                    "name": obj.customer
                }
            }
        );
        loadCombo(data, 'selectFinalCustomer', true);

        $("#labelFinalCustomer").show(100)
        $("#selectFinalCustomer").show(100)

    } else {
        $("#selectFinalCustomer").hide()
        $("#labelFinalCustomer").hide()
        loadCombo([], 'selectFinalCustomer', true);
    }

}

function calcTotal() {
    var pallet = 0

    $("#tblCatVar tbody tr td.tdPallet").each(function () {
        pallet += parseFloat($(this).text());

    })

    $('#tblCatVar .totalPallets').html(pallet.toFixed(2))
}