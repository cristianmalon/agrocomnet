﻿var dataWeeks = []
var dataClients = []
var dataClients2 = []
var dataClients3 = []
var dataClients4 = []
var dataClients5 = []
var _dataDetalle = [];
var _dataSaleSubTotal = [];
var _dataDetailForescast = [];

$(function () {
  //sweet_alert_progressbar();
    loadWeek();
    createList();
    createList2();
    createList3();
    createList4();
    createList5();
    loadDataForescast();
    loadCombos();
  sweet_alert_progressbar_cerrar();
    $("#tablaDetalleForescast").DataTable({
        "paging": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        //"destroy": true,
        dom: 'Bfrtip',
        //select: true,
        lengthMenu: [
            [-1],
            ['All']
        ],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });    

    $('#tablaDetalleForescast tbody').on('click', 'td.details-control', function (i, e) {
        var growerID = $(this).closest('tr').find('[data-name=growerID]').text()
        if ($($('tr.child-' + growerID)).is(":hidden")) {
            $('tr.child-' + growerID).show().children('td');
        } else {
            $('tr.child-' + growerID).hide().children('td');
        }
    });

})


$('#selectCustomer').change(function () {
  sweet_alert_progressbar();
    filterList1();
    filterList2();
    filterList5();
  sweet_alert_progressbar_cerrar();
})

$('#selectVia').change(function () {
  sweet_alert_progressbar();
    filterList1();
    filterList2();
    filterList3();
    filterList4();
  filterList5();
  sweet_alert_progressbar_cerrar();
})

$('#selectCodepack').change(function () {
  sweet_alert_progressbar();
    filterList1();
    filterList2();
    filterList5();
  sweet_alert_progressbar_cerrar();
})


$('#selectDestination').change(function () {
  sweet_alert_progressbar();
    filterList1();
    filterList2();
    filterList5();
  sweet_alert_progressbar_cerrar();
})

function filterList1() {
    $('#example').DataTable().destroy();
    $('table#example>tbody').empty();
    //console.log(dataClients.filter(item => (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")))
    $.each(dataClients.filter(item => (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "") && (item.codePack == $("#selectCodepack option:selected").val() || $("#selectCodepack option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {

        var exists = false;
        var trfind = [];
        $('table#example > tbody  > tr').each(function (index, tr) {
            var customer = $(tr).find("[data-name='customer']").text();
            var destination = $(tr).find("[data-name='destination']").text();
            var via = $(tr).find("[data-name='via']").text();
            var codePack = $(tr).find("[data-name='codePack']").text();
            var weight = $(tr).find("[data-name='weight']").text();
            var condition = $(tr).find("[data-name='condition']").text();
            var status = $(tr).find("[data-name='status']").text();

            if (e.customer == customer && e.destination == destination && e.via == via && e.codePack == codePack &&
                e.weight == weight && e.condition == condition && e.status == status) {
                exists = true
                trfind = tr;
            }
        });

        if (exists == false) {
            var tr = '';
            tr += "<tr>";
            tr += '<td class="sticky" style="max-width:200px;min-width:200px" data-name="customer" >' + e.customer + '</td>';
            tr += '<td class="sticky2" style="max-width:120px;min-width:120px" data-name="destination">' + e.destination + '</td>';
            tr += '<td class="sticky3" style="max-width:50px;min-width:50px"data-name="via">' + e.via + '</td>';
            tr += '<td class="sticky4" style="max-width:120px;min-width:120px" data-name="codePack">' + e.codePack + '</td>';
            tr += '<td class="sticky5" style="text-align: right !important;max-width:50px;min-width:50px" data-name="weight">' + (parseFloat(e.weight).toFixed(2)) + '</td>';
            tr += '<td class="sticky6" style="max-width:100px;min-width:100px" data-name="condition">' + e.condition + '</td>';
            tr += '<td class="sticky7" style="max-width:80px;min-width:80px" data-name="palletfcl">' + e.palletfcl + '</td>';
            tr += '<td class="sticky8" style="display:none" data-name="status">' + e.status + '</td>';

            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    if (e.status == 1) {
                        tr += '<td style="background-color: #f6c23e; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 2) {
                        tr += '<td style="background-color: #3498DB ;  font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 3) {
                        tr += '<td style="background-color: #1cc88a; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                } else {
                    tr += '<td style="min-width: 50px !important; text-align: right !important" class="mount text-align-right"></td>'
                }
            })
            tr += '<td style="min-width: 50px !important; text-align: right !important" class="subtotalRow text-align-right">';
            tr += '</td>';
            tr += '</tr>';
            //console.log(tr);

            $("table#example tbody").append(tr);

        } else {
            var idx = 0
            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                    $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                    if (e.status == 1) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#f6c23e', 'font-weight': 'bold' });
                    }
                    if (e.status == 2) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#3498DB ', 'font-weight': 'bold' });
                    }
                    if (e.status == 3) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#1cc88a', 'font-weight': 'bold' });
                    }
                }
                idx = parseInt(idx) + 1
            })
        }
    })    

    //addSubTotalClient("example");

    $('table#example>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

    //$("#example").dataTable({
    //    "paging": true,
    //    "ordering": false,
    //    "info": false,
    //    "responsive": true,
    //    //"destroy": true,
    //    dom: 'Bfrtip',
    //    buttons: ['copy', 'excel', 'pdf', 'csv'],
    //});

    $("#example").DataTable({
        "scrollY": "30vh",
        "scrollX": true,
        "scrollCollapse": true,
        paging: false,
        //"order": [[0, "desc"]],
        fixedHeader: true,
        "paging": false,
        "ordering": false,
        //"info": false,
        //"responsive": true,
        dom: 'Bfrtip',
        //lengthMenu: [
        //    [10, 25, 50, -1],
        //    ['10 rows', '25 rows', '50 rows', 'All']
        //],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            //'pageLength'
        ],
    });
}

function filterList3() {
    $('#examplevia').DataTable().destroy();
    $('table#examplevia>tbody').empty();
    //console.log(dataClients3.filter(item => (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")))
    $.each(dataClients3.filter(item => (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")), function (i, e) {

        var exists = false;
        var trfind = [];
        $('table#examplevia > tbody  > tr').each(function (index, tr) {
            var via = $(tr).find("[data-name='via']").text();
            var condition = $(tr).find("[data-name='condition']").text();

            if (e.via == via && e.condition == condition) {
                exists = true
                trfind = tr;
            }

        });

        if (exists == false) {
            var tr = '';
            tr += '<tr>';
            tr += '<td class="sticky9" data-name="via">' + e.via + '</td>';
            tr += '<td class="sticky10" data-name="condition">' + e.condition + '</td>';
            tr += '<td class="sticky11" data-name="palletfcl">' + e.palletfcl + '</td>';

            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                } else {
                    tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right"></td>'
                }
            })
            tr += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
            tr += '</td>';
            tr += '</tr>';
            ////console.log(tr);

            $("table#examplevia tbody").append(tr);

        } else {
            var idx = 0
            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                    $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                }
                idx = parseInt(idx) + 1
            })
        }
    })

    $('table#examplevia>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

    $("#examplevia").DataTable({
        "paging": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        //"destroy": true,
        dom: 'Bfrtip',
        //select: true,
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'All']
        ],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });
}

function filterList4() {
    $('#exampleboxvia').DataTable().destroy();
    $('table#exampleboxvia>tbody').empty();
    //console.log(dataClients4.filter(item => (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")))
    $.each(dataClients4.filter(item => (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")), function (i, e) {
        var exists = false;
        var trfind = [];
        $('table#exampleboxvia > tbody  > tr').each(function (index, tr) {
            var via = $(tr).find("[data-name='via']").text();
            var condition = $(tr).find("[data-name='condition']").text();

            if (e.via == via && e.condition == condition) {
                exists = true
                trfind = tr;
            }

        });

        if (exists == false) {
            var tr = '';
            tr += "<tr>";
            tr += '<td class="sticky9" data-name="via">' + e.via + '</td>';
            tr += '<td class="sticky10" data-name="condition">' + e.condition + '</td>';
            tr += '<td class="sticky11" data-name="measure">' + e.measure + '</td>';

            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                } else {
                    tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
                }
            })
            tr += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
            tr += '</td>';
            tr += '</tr>';
            //console.log(tr);

            $("table#exampleboxvia tbody").append(tr);

        } else {
            //console.log('exists')
            //console.log(trfind);
            var idx = 0
            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                    $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                }
                idx = parseInt(idx) + 1
            })
        }
    })    

    addSubTotalClient2("exampleboxvia");

    $('table#exampleboxvia>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(totalrow);
    })

    $("#exampleboxvia").DataTable({
        "paging": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        //"destroy": true,
        dom: 'Bfrtip',
        //select: true,
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'All']
        ],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });
}

function filterList2() {
    $('#examplebox').DataTable().destroy();
    $('table#examplebox>tbody').empty();
    //console.log(dataClients2.filter(item => (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")))
    $.each(dataClients2.filter(item => (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "") && (item.codePack == $("#selectCodepack option:selected").val() || $("#selectCodepack option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {
        var exists = false;
        var trfind = [];
        $('table#examplebox > tbody  > tr').each(function (index, tr) {
            var customer = $(tr).find("[data-name='customer']").text();
            var destination = $(tr).find("[data-name='destination']").text();
            var via = $(tr).find("[data-name='via']").text();
            var codePack = $(tr).find("[data-name='codePack']").text();
            var weight = $(tr).find("[data-name='weight']").text();
            var condition = $(tr).find("[data-name='condition']").text();
            var status = $(tr).find("[data-name='status']").text();

            if (e.customer == customer && e.destination == destination && e.via == via && e.codePack == codePack &&
                e.weight == weight && e.condition == condition && e.status == status) {
                exists = true
                trfind = tr;
            }

        });

        if (exists == false) {
            var tr = '';
            tr += "<tr>";
            tr += '<td class="sticky" style="max-width:200px;min-width:200px" data-name="customer">' + e.customer + '</td>';
            tr += '<td class="sticky2" style="max-width:120px;min-width:120px" data-name="destination">' + e.destination + '</td>';
            tr += '<td class="sticky3" style="max-width:50px;min-width:50px" data-name="via">' + e.via + '</td>';
            tr += '<td class="sticky4" style="max-width:120px;min-width:120px" data-name="codePack">' + e.codePack + '</td>';
            tr += '<td class="sticky5" style="text-align: right !important;max-width:50px;min-width:50px" data-name="weight">' + (parseFloat(e.weight).toFixed(2)) + '</td>';
            tr += '<td class="sticky6" style="max-width:100px;min-width:100px" data-name="condition">' + e.condition + '</td>';
            tr += '<td class="sticky7" style="max-width:80px;min-width:80px" data-name="measure">' + e.measure + '</td>';
            tr += '<td class="sticky8" style="display:none" data-name="status">' + e.status + '</td>';

            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    if (e.status == 1) {
                        tr += '<td style="background-color: #f6c23e; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 2) {
                        tr += '<td style="background-color: #3498DB ;  font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 3) {
                        tr += '<td style="background-color: #1cc88a; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                } else {
                    tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
                }
            })
            tr += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
            tr += '</td>';
            tr += '</tr>';
            ////console.log(tr);

            $("table#examplebox tbody").append(tr);

        } else {
            var idx = 0
            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                    $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                    if (e.status == 1) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#f6c23e', 'font-weight': 'bold' });
                    }
                    if (e.status == 2) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#3498DB ', 'font-weight': 'bold' });
                    }
                    if (e.status == 3) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#1cc88a', 'font-weight': 'bold' });
                    }
                }
                idx = parseInt(idx) + 1
            })
        }

    })

    addSubTotalClient("examplebox");

    $('table#examplebox>tbody>tr').each(function (i, e) {

        totalrow = parseFloat(mount) + totalrow
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow

        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

    $("#examplebox").DataTable({
        "scrollY": "30vh",
        "scrollX": true,
        "scrollCollapse": true,
        paging: false,
        //"order": [[0, "desc"]],
        fixedHeader: true,
        "paging": false,
        "ordering": false,
        //"info": false,
        //"responsive": true,
        dom: 'Bfrtip',
        //lengthMenu: [
        //    [10, 25, 50, -1],
        //    ['10 rows', '25 rows', '50 rows', 'All']
        //],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            //'pageLength'
        ],
    });
}


function filterList5() {
    $('#exampleGrower').DataTable().destroy();
    $('table#exampleGrower>tbody').empty();
    //console.log(dataClients5.filter(item => (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "")))
    $.each(dataClients5.filter(item => (item.customerID == $("#selectCustomer option:selected").val() || $("#selectCustomer option:selected").val() == "") && (item.viaID == $("#selectVia option:selected").val() || $("#selectVia option:selected").val() == "") && (item.codePack == $("#selectCodepack option:selected").val() || $("#selectCodepack option:selected").val() == "") && (item.destinationID == $("#selectDestination option:selected").val() || $("#selectDestination option:selected").val() == "")), function (i, e) {

        var exists = false;
        var trfind = [];
        $('table#exampleGrower > tbody  > tr').each(function (index, tr) {
            var grower = $(tr).find("[data-name='grower']").text();
            var customer = $(tr).find("[data-name='customer']").text();
            var destination = $(tr).find("[data-name='destination']").text();
            var via = $(tr).find("[data-name='via']").text();
            var codePack = $(tr).find("[data-name='codePack']").text();
            var weight = $(tr).find("[data-name='weight']").text();
            var condition = $(tr).find("[data-name='condition']").text();
            var status = $(tr).find("[data-name='status']").text();

            //console.log(e.grower)
            //console.log(grower)
            if (grower == "NOT ASSIGNED") grower = "";
            if (e.grower == grower && e.customer == customer && e.destination == destination && e.via == via && e.codePack == codePack &&
                e.weight == weight && e.condition == condition && e.status == status) {
                exists = true
                trfind = tr;
            }

        });

        if (exists == false) {
            var tr = '';
            tr += "<tr>";
            if (e.grower == "") {
                tr += '<td class="sticky9 text-danger" style="max-width:150px;min-width:150px" data-name="grower" ><strong>NOT ASSIGNED</strong></td>';
            } else {
                tr += '<td class="sticky9" style="max-width:150px;min-width:150px" data-name="grower" >' + e.grower + '</td>';
            }

            tr += '<td class="sticky12" style="max-width:200px;min-width:200px" data-name="customer" >' + e.customer + '</td>';
            tr += '<td class="sticky13" style="max-width:120px;min-width:120px" data-name="destination">' + e.destination + '</td>';
            tr += '<td class="sticky14" style="max-width:50px;min-width:50px" data-name="via">' + e.via + '</td>';
            tr += '<td class="sticky15" style="max-width:120px;min-width:120px" data-name="codePack">' + e.codePack + '</td>';
            tr += '<td class="sticky16" style="text-align: right !important;max-width:50px;min-width:50px" data-name="weight">' + (parseFloat(e.weight).toFixed(2)) + '</td>';
            tr += '<td class="sticky17" style="max-width:100px;min-width:100px" data-name="condition">' + e.condition + '</td>';
            tr += '<td class="sticky18" style="max-width:80px;min-width:80px" data-name="measure">' + e.measure + '</td>';
            tr += '<td class="sticky9" style="display:none" data-name="status">' + e.status + '</td>';

            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    if (e.status == 1) {
                        tr += '<td style="background-color: #f6c23e; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 2) {
                        tr += '<td style="background-color: #3498DB ;  font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 3) {
                        tr += '<td style="background-color: #1cc88a; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    }
                    if (e.status == 0) {
                        tr += '<td style="color:#fff; background-color: #a94442; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'

                    }
                } else {
                    tr += '<td style="min-width: 50px !important; text-align: right !important" class="mount text-align-right"></td>'
                }
            })
            tr += '<td style="min-width: 50px !important; text-align: right !important" class="subtotalRow1 text-align-right">';
            tr += '</td>';
            tr += '</tr>';
            ////console.log(tr);

            $("table#exampleGrower tbody").append(tr);

        } else {
            var idx = 0
            $.each(dataWeeks, function (iWeek, week) {
                if (e.projectedWeekID == week.projectedWeekID) {
                    //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                    $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                    if (e.status == 1) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#f6c23e', 'font-weight': 'bold' });
                    }
                    if (e.status == 2) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#3498DB ', 'font-weight': 'bold' });
                    }
                    if (e.status == 3) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#1cc88a', 'font-weight': 'bold' });
                    }
                    if (e.status == 0) {
                        $(trfind).find('td.mount:eq(' + idx + ')').css({ 'font-weight': 'bold' });
                    }
                }
                idx = parseInt(idx) + 1
            })
        }

    })

    $('table#exampleGrower>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

    $("#exampleGrower").DataTable({
        "scrollY": "30vh",
        "scrollX": true,
        "scrollCollapse": true,
        paging: false,
        //"order": [[0, "desc"]],
        fixedHeader: true,
        "paging": false,
        "ordering": false,
        //"info": false,
        //"responsive": true,
        dom: 'Bfrtip',
        //lengthMenu: [
        //    [10, 25, 50, -1],
        //    ['10 rows', '25 rows', '50 rows', 'All']
        //],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            //'pageLength'
        ],
    });
}


function loadCombos() {
    var campaignID = localStorage.campaignID;
    $.ajax({
        type: "GET",
        url: "/Sales/ListReportCustomer?opt=" + "cus" + "&campaignID=" + campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.customerID,
                            "name": obj.customer
                        }
                    }
                );
                loadCombo(dataSelect, 'selectCustomer', true, 'Choose');
                $('#selectCustomer').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/ListReportVia?opt=" + "via" + "&campaignID=" + campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.viaID,
                            "name": obj.via
                        }
                    }
                );
                loadCombo(dataSelect, 'selectVia', true, 'Choose');
                $('#selectVia').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });

    $.ajax({
        type: "GET",
        url: "/Sales/ListReportCodepack?opt=" + "cod" + "&campaignID=" + campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.codePackID,
                            "name": obj.codePack
                        }
                    }
                );
                loadCombo(dataSelect, 'selectCodepack', true, 'Choose');
                $('#selectCodepack').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });


    $.ajax({
        type: "GET",
        url: "/Sales/ListReportDestination?opt=" + "des" + "&campaignID=" + campaignID,
        //async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 2) {
                var dataJson = JSON.parse(data);
                var dataSelect = dataJson.map(
                    obj => {
                        return {
                            "id": obj.destinationID,
                            "name": obj.destination
                        }
                    }
                );
                loadCombo(dataSelect, 'selectDestination', true, 'Choose');
                $('#selectDestination').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });

}

function loadCombo(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>" + textFirstElement + "</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadWeek() {
    $.ajax({
        type: "GET",
        url: "/CommercialPlan/ListWeekBySeason?idSeason=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            dataWeeks = JSON.parse(data);
            ////console.log(data);
        }
    });

    $.each(dataWeeks, function (iweek, week) {
        $("table#example thead tr").append('<th style="min-width:50px!important" >' + week.number + '-' + week.year + '</th>');
    })
    $("table#example thead tr").append('<th style="min-width:50px!important" >Total</th>');

    $.each(dataWeeks, function (iweek, week) {
        $("table#examplevia thead tr").append('<th >' + week.number + '-' + week.year + '</th>');
    })
    $("table#examplevia thead tr").append('<th >Total</th>');

    //   By box
    $.each(dataWeeks, function (iweek, week) {
        $("table#examplebox thead tr").append('<th style="min-width:50px!important">' + week.number + '-' + week.year + '</th>');
    })
    $("table#examplebox thead tr").append('<th style="min-width:50px!important">Total</th>');

    $.each(dataWeeks, function (iweek, week) {
        $("table#exampleboxvia thead tr").append('<th >' + week.number + '-' + week.year + '</th>');
    })
    $("table#exampleboxvia thead tr").append('<th >Total</th>');

    $.each(dataWeeks, function (iweek, week) {
        $("table#exampleGrower thead tr").append('<th style="min-width:50px!important">' + week.number + '-' + week.year + '</th>');
    })
    $("table#exampleGrower thead tr").append('<th style="min-width:50px!important">Total</th>');

}

function createList(wiNI, wEn) {

    $("table#example tbody").html('');

    dataClients = []
    $.ajax({
        type: "GET",
        url: "ListReport?campaignID=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients = dataJson;
                ////console.log(JSON.stringify(dataClients));

                $.each(dataClients, function (i, e) {

                    var exists = false;
                    var trfind = [];
                    $('table#example > tbody  > tr').each(function (index, tr) {
                        var customer = $(tr).find("[data-name='customer']").text();
                        var destination = $(tr).find("[data-name='destination']").text();
                        var via = $(tr).find("[data-name='via']").text();
                        var codePack = $(tr).find("[data-name='codePack']").text();
                        var weight = $(tr).find("[data-name='weight']").text();
                        var condition = $(tr).find("[data-name='condition']").text();
                        var status = $(tr).find("[data-name='status']").text();

                        if (e.customer == customer && e.destination == destination && e.via == via && e.codePack == codePack &&
                            e.weight == weight && e.condition == condition && e.status == status) {
                            exists = true
                            trfind = tr;
                        }

                    });

                    if (exists == false) {
                        var tr = '';
                        tr += "<tr>";
                        tr += '<td class="sticky" style="max-width:200px;min-width:200px" data-name="customer" >' + e.customer + '</td>';
                        tr += '<td class="sticky2" style="max-width:120px;min-width:120px" data-name="destination">' + e.destination + '</td>';
                        tr += '<td class="sticky3" style="max-width:50px;min-width:50px" data-name="via">' + e.via + '</td>';
                        tr += '<td class="sticky4" style="max-width:120px;min-width:120px" data-name="codePack">' + e.codePack + '</td>';
                        tr += '<td class="sticky5" style="text-align: right !important;max-width:50px;min-width:50px" data-name="weight">' + (parseFloat(e.weight).toFixed(2)) + '</td>';
                        tr += '<td class="sticky6" style="max-width:100px;min-width:100px" data-name="condition">' + e.condition + '</td>';
                        tr += '<td class="sticky7" style="max-width:80px;min-width:80px" data-name="palletfcl">' + e.palletfcl + '</td>';
                        tr += '<td class="sticky8" style="display:none" data-name="status">' + e.status + '</td>';

                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                if (e.status == 1) {
                                    tr += '<td style="background-color: #f6c23e; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 2) {
                                    tr += '<td style="background-color: #3498DB ;  font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 3) {
                                    tr += '<td style="background-color: #1cc88a; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                            } else {
                                tr += '<td style="min-width: 50px!important; text-align: right !important" class="mount text-align-right"></td>'
                            }
                        })
                        tr += '<td style="min-width: 50px !important; text-align: right !important" class="subtotalRow text-align-right">';
                        tr += '</td>';
                        tr += '</tr>';
                        ////console.log(tr);

                        $("table#example tbody").append(tr);

                    } else {
                        var idx = 0
                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                                $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                                if (e.status == 1) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#f6c23e', 'font-weight': 'bold' });
                                }
                                if (e.status == 2) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#3498DB ', 'font-weight': 'bold' });
                                }
                                if (e.status == 3) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#1cc88a', 'font-weight': 'bold' });
                                }
                            }
                            idx = parseInt(idx) + 1
                        })
                    }
                })
            }
        },

        complete: function () {
            $("#example").DataTable({
                "scrollY": "30vh",
                "scrollX": true,
                "scrollCollapse": true,
                paging: false,
                //"order": [[0, "desc"]],
                fixedHeader: true,
                "paging": false,
                "ordering": false,
                "autoWidth": false,
                //"responsive": true,
                dom: 'Bfrtip',
                //lengthMenu: [
                //    [10, 25, 50, -1],
                //    ['10 rows', '25 rows', '50 rows', 'All']
                //],
                buttons: [

                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    //'pageLength'
                ],
            });
        }
    });

    $('table#example>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

}

function createList3(wiNI, wEn) {
    $("table#examplevia tbody").html('');

    //var dataClients = []
    $.ajax({
        type: "GET",
        url: "ListReportSum?campaignID=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients3 = dataJson;
                //console.log(JSON.stringify(dataClients));
                $.each(dataClients3, function (i, e) {

                    var exists = false;
                    var trfind = [];
                    $('table#examplevia > tbody  > tr').each(function (index, tr) {
                        var via = $(tr).find("[data-name='via']").text();
                        var condition = $(tr).find("[data-name='condition']").text();

                        if (e.via == via && e.condition == condition) {
                            exists = true
                            trfind = tr;
                        }

                    });

                    if (exists == false) {
                        var tr = '';
                        tr += '<tr>';
                        tr += '<td class="sticky9" data-name="via">' + e.via + '</td>';
                        tr += '<td class="sticky10" data-name="condition">' + e.condition + '</td>';
                        tr += '<td class="sticky11" data-name="palletfcl">' + e.palletfcl + '</td>';

                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                            } else {
                                tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right"></td>'
                            }
                        })
                        tr += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
                        tr += '</td>';
                        tr += '</tr>';
                        ////console.log(tr);

                        $("table#examplevia tbody").append(tr);

                    } else {
                        var idx = 0
                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                                $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                            }
                            idx = parseInt(idx) + 1
                        })
                    }
                })
            }
        },
        complete: function () {
            $("#examplevia").DataTable({
                "paging": true,
                "ordering": false,
                "info": false,
                "responsive": true,
                //"destroy": true,
                dom: 'Bfrtip',
                //select: true,
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'All']
                ],
                dom: 'Bfrtip',
                buttons: [

                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'
                ],
            });
        }
    });

    $('table#examplevia>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })

        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

}

function createList2(wiNI, wEn) {
    $("table#examplebox tbody").html('');

    //var dataClients = []
    $.ajax({
        type: "GET",
        url: "ListReportBox?campaignID=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients2 = dataJson;
                //console.log(JSON.stringify(dataClients));

                $.each(dataClients2, function (i, e) {

                    var exists = false;
                    var trfind = [];
                    $('table#examplebox > tbody  > tr').each(function (index, tr) {
                        var customer = $(tr).find("[data-name='customer']").text();
                        var destination = $(tr).find("[data-name='destination']").text();
                        var via = $(tr).find("[data-name='via']").text();
                        var codePack = $(tr).find("[data-name='codePack']").text();
                        var weight = $(tr).find("[data-name='weight']").text();
                        var condition = $(tr).find("[data-name='condition']").text();
                        var status = $(tr).find("[data-name='status']").text();

                        if (e.customer == customer && e.destination == destination && e.via == via && e.codePack == codePack &&
                            e.weight == weight && e.condition == condition && e.status == status) {
                            exists = true
                            trfind = tr;
                        }

                    });

                    if (exists == false) {
                        var tr = '';
                        tr += "<tr>";
                        tr += '<td class="sticky" style="max-width:200px;min-width:200px" data-name="customer">' + e.customer + '</td>';
                        tr += '<td class="sticky2" style="max-width:120px;min-width:120px" data-name="destination">' + e.destination + '</td>';
                        tr += '<td class="sticky3" style="max-width:50px;min-width:50px" data-name="via">' + e.via + '</td>';
                        tr += '<td class="sticky4" style="max-width:120px;min-width:120px" data-name="codePack">' + e.codePack + '</td>';
                        tr += '<td class="sticky5" style="text-align: right !important;max-width:50px;min-width:50px" data-name="weight">' + (parseFloat(e.weight).toFixed(2)) + '</td>';
                        tr += '<td class="sticky6" style="max-width:100px;min-width:100px" data-name="condition">' + e.condition + '</td>';
                        tr += '<td class="sticky7" style="max-width:80px;min-width:80px" data-name="measure">' + e.measure + '</td>';
                        tr += '<td class="sticky8" style="display:none" data-name="status">' + e.status + '</td>';

                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                if (e.status == 1) {
                                    tr += '<td style="background-color: #f6c23e; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 2) {
                                    tr += '<td style="background-color: #3498DB ; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 3) {
                                    tr += '<td style="background-color: #1cc88a;  font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                //tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                            } else {
                                tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
                            }
                        })
                        tr += '<td style="min-width: 50px !important;text-align: right !important" class="subtotalRow text-align-right">';
                        tr += '</td>';
                        tr += '</tr>';
                        ////console.log(tr);

                        $("table#examplebox tbody").append(tr);

                    } else {
                        var idx = 0
                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();

                                $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)));
                                if (e.status == 1) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#f6c23e', 'font-weight': 'bold' });
                                }
                                if (e.status == 2) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#3498DB ', 'font-weight': 'bold' });
                                }
                                if (e.status == 3) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#1cc88a', 'font-weight': 'bold' });
                                }
                            }
                            idx = parseInt(idx) + 1
                        })
                    }
                })
            }
        },
        complete: function () {
            $("#examplebox").DataTable({
                "scrollY": "30vh",
                "scrollX": true,
                "scrollCollapse": true,
                paging: false,
                //"order": [[0, "desc"]],
                fixedHeader: true,
                "paging": false,
                "ordering": false,
                //"info": false,
                //"responsive": true,
                dom: 'Bfrtip',
                //lengthMenu: [
                //    [10, 25, 50, -1],
                //    ['10 rows', '25 rows', '50 rows', 'All']
                //],
                dom: 'Bfrtip',
                buttons: [

                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    //'pageLength'
                ],
            });
        }
    });    

    addSubTotalClient("examplebox");

    $('table#examplebox>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            //totalrow = parseFloat($(td).text()) + totalrow
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow

        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

}

function createList4(wiNI, wEn) {
    $("table#exampleboxvia tbody").html('');

    //var dataClients = []
    $.ajax({
        type: "GET",
        url: "ListReportBoxSum?campaignID=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients4 = dataJson;
                //console.log(JSON.stringify(dataClients));
                $.each(dataClients4, function (i, e) {

                    var exists = false;
                    var trfind = [];
                    $('table#exampleboxvia > tbody  > tr').each(function (index, tr) {
                        var via = $(tr).find("[data-name='via']").text();
                        var condition = $(tr).find("[data-name='condition']").text();

                        if (e.via == via && e.condition == condition) {
                            exists = true
                            trfind = tr;
                        }

                    });

                    if (exists == false) {
                        var tr = '';
                        tr += "<tr>";
                        tr += '<td class="sticky9" data-name="via">' + e.via + '</td>';
                        tr += '<td class="sticky10" data-name="condition">' + e.condition + '</td>';
                        tr += '<td class="sticky11" data-name="measure">' + e.measure + '</td>';

                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                            } else {
                                tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
                            }
                        })
                        tr += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
                        tr += '</td>';
                        tr += '</tr>';
                        //console.log(tr);

                        $("table#exampleboxvia tbody").append(tr);

                    } else {
                        //console.log('exists')
                        //console.log(trfind);
                        var idx = 0
                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                                $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))

                            }
                            idx = parseInt(idx) + 1
                        })
                    }
                })
            }
        },
        complete: function () {
            
            $("#exampleboxvia").DataTable({
                "paging": true,
                "ordering": false,
                "info": false,
                "responsive": true,
                //"destroy": true,
                dom: 'Bfrtip',
                //select: true,
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'All']
                ],
                dom: 'Bfrtip',
                buttons: [

                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    'pageLength'
                ],
            });
        }
    });    

    addSubTotalClient2("exampleboxvia");

    $('table#exampleboxvia>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow;
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })
}


function createList5(wiNI, wEn) {

    $("table#exampleGrower tbody").html('');

    dataClients5 = []
    $.ajax({
        type: "GET",
        url: "ListReportPO?campaignID=" + localStorage.campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataClients5 = dataJson;
                ////console.log(JSON.stringify(dataClients));

                $.each(dataClients5, function (i, e) {

                    var exists = false;
                    var trfind = [];
                    $('table#exampleGrower > tbody  > tr').each(function (index, tr) {

                        var grower = $(tr).find("[data-name='grower']").text();
                        var customer = $(tr).find("[data-name='customer']").text();
                        var destination = $(tr).find("[data-name='destination']").text();
                        var via = $(tr).find("[data-name='via']").text();
                        var codePack = $(tr).find("[data-name='codePack']").text();
                        var weight = $(tr).find("[data-name='weight']").text();
                        var condition = $(tr).find("[data-name='condition']").text();
                        var status = $(tr).find("[data-name='status']").text();

                        //console.log(e.grower)
                        //console.log(grower)
                        if (grower == "NOT ASSIGNED") grower = "";
                        if (e.grower == grower && e.customer == customer && e.destination == destination && e.via == via && e.codePack == codePack &&
                            e.weight == weight && e.condition == condition && e.status == status) {
                            exists = true
                            trfind = tr;
                        }

                    });

                    if (exists == false) {
                        var tr = '';
                        tr += "<tr>";
                        if (e.grower == "") {
                            tr += '<td class="sticky9 text-danger" style="max-width:150px;min-width:150px" data-name="grower" ><strong>NOT ASSIGNED</strong></td>';
                        } else {
                            tr += '<td class="sticky9" style="max-width:150px;min-width:150px" data-name="grower" >' + e.grower + '</td>';
                        }

                        tr += '<td class="sticky12" style="max-width:200px;min-width:200px" data-name="customer" >' + e.customer + '</td>';
                        tr += '<td class="sticky13" style="max-width:120px;min-width:120px" data-name="destination">' + e.destination + '</td>';
                        tr += '<td class="sticky14" style="max-width:50px;min-width:50px" data-name="via">' + e.via + '</td>';
                        tr += '<td class="sticky15" style="max-width:120px;min-width:120px" data-name="codePack">' + e.codePack + '</td>';
                        tr += '<td class="sticky16" style="text-align: right !important;max-width:50px;min-width:50px" data-name="weight">' + (parseFloat(e.weight).toFixed(2)) + '</td>';
                        tr += '<td class="sticky17" style="max-width:100px;min-width:100px" data-name="condition">' + e.condition + '</td>';
                        tr += '<td class="sticky18" style="max-width:80px;min-width:80px" data-name="measure">' + e.measure + '</td>';
                        tr += '<td class="sticky9" style="display:none" data-name="status">' + e.status + '</td>';

                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                if (e.status == 1) {
                                    tr += '<td style="background-color: #f6c23e; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 2) {
                                    tr += '<td style="background-color: #3498DB ;  font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 3) {
                                    tr += '<td style="background-color: #1cc88a; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                                }
                                if (e.status == 0) {
                                    tr += '<td style="color:#fff; background-color: #a94442; font-weight: bold; min-width: 50px !important; text-align: right !important;" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'

                                }
                            } else {
                                tr += '<td style="min-width: 50px !important; text-align: right !important" class="mount text-align-right"></td>'
                            }
                        })
                        tr += '<td style="min-width: 50px !important; text-align: right !important" class="subtotalRow1 text-align-right">';
                        tr += '</td>';
                        tr += '</tr>';
                        //console.log(tr);

                        $("table#exampleGrower tbody").append(tr);

                    } else {
                        var idx = 0
                        $.each(dataWeeks, function (iWeek, week) {
                            if (e.projectedWeekID == week.projectedWeekID) {
                                //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                                $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.quantity).toFixed(2)))
                                if (e.status == 1) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#f6c23e', 'font-weight': 'bold' });
                                }
                                if (e.status == 2) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#3498DB ', 'font-weight': 'bold' });
                                }
                                if (e.status == 3) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'background-color': '#1cc88a', 'font-weight': 'bold' });
                                }
                                if (e.status == 0) {
                                    $(trfind).find('td.mount:eq(' + idx + ')').css({ 'color': '#fff', 'background-color': '#a94442', 'font-weight': 'bold' });
                                }
                            }
                            idx = parseInt(idx) + 1
                        })
                    }
                })
            }
        },
        complete: function () {

            $("#exampleGrower").DataTable({
                "scrollY": "30vh",
                "scrollX": true,
                "scrollCollapse": true,
                paging: false,
                //"order": [[0, "desc"]],
                fixedHeader: true,
                "paging": false,
                "ordering": false,
                //"info": false,
                //"responsive": true,
                dom: 'Bfrtip',
                //lengthMenu: [
                //    [10, 25, 50, -1],
                //    ['10 rows', '25 rows', '50 rows', 'All']
                //],
                dom: 'Bfrtip',
                buttons: [

                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    //'pageLength'
                ],
            });
        
      }
          
    });

    $('table#exampleGrower>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            var mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow1').text(parseFloat(totalrow).toFixed(2));
    })

}

function addSubTotalClient(id) {
    ////console.log('addSubTotalClient');
    //$("#" + id + " tr.trSubtotal").remove();

    var sum = 0.0;
    var dataSubtTotal = [];

    $.each(dataWeeks, function (iWeeks, valWeek) {
        dataSubtTotal.push({ "id": iWeeks, "mount": 0 });
    });

    $('#' + id + ' > tbody > tr').each(function (index, element) {

        var iSpanCols = parseInt(8)

        $.each(dataWeeks, function (index2, value) {

            $valCell = $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text();
            //$valWeight = parseFloat($(element).find('td:eq(7)').text());

            if ($.isNumeric($valCell)) {

            } else {
                $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text('');
                $valCell = 0;
            }

            $.each(dataSubtTotal, function (i, v) {
                if (v.id == index2) {
                    v.mount += parseFloat($valCell);
                    //if (parseFloat(v.mount) > 0) //console.log(v.mount);
                }
            });
        });
    });

    //console.log(JSON.stringify(dataSubtTotal))
    $("table#" + id + " tfoot tr th.text-align-right").remove();
    $.each(dataSubtTotal, function (iweek, week) {
        _dataSaleSubTotal.push(parseFloat(week.mount).toFixed(2));
        //$("table#" + id + " tfoot tr").find
        $("table#" + id + " tfoot tr").append('<th class="text-align-right">' + parseFloat(week.mount).toFixed(2) + '</th>');
    })
    //console.log('holaaa');
    //console.log(_dataSaleSubTotal);
}

function addSubTotalClient2(id) {
    ////console.log('addSubTotalClient');
    //$("#" + id + " tr.trSubtotal").remove();

    var sum = 0.0;
    var dataSubtTotal = [];

    $.each(dataWeeks, function (iWeeks, valWeek) {
        dataSubtTotal.push({ "id": iWeeks, "mount": 0 });
    });

    $('#' + id + ' > tbody > tr').each(function (index, element) {

        var iSpanCols = parseInt(3)

        $.each(dataWeeks, function (index2, value) {

            $valCell = $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text();
            //$valWeight = parseFloat($(element).find('td:eq(7)').text());

            if ($.isNumeric($valCell)) {

            } else {
                $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text('');
                $valCell = 0;
            }

            $.each(dataSubtTotal, function (i, v) {
                if (v.id == index2) {
                    v.mount += parseFloat($valCell);
                    //if (parseFloat(v.mount) > 0) //console.log(v.mount);
                }
            });
        });
    });

    //console.log(JSON.stringify(dataSubtTotal))
    $("table#" + id + " tfoot tr th.text-align-right").remove();
    $.each(dataSubtTotal, function (iweek, week) {
        _dataSaleSubTotal.push(parseFloat(week.mount).toFixed(2));
        //$("table#" + id + " tfoot tr").find
        $("table#" + id + " tfoot tr").append('<th class="text-align-right">' + parseFloat(week.mount).toFixed(2) + '</th>');
    })

    ////console.log(JSON.stringify(dataSubtTotal))
    //$.each(dataSubtTotal, function (iweek, week) {
    //    $("table#" + id + " tfoot tr").append('<th class="text-align-right">' + parseFloat(week.mount).toFixed(2) + '</th>');
    //})
}

//Forescast
//Cargando todos los datos a los controles del Modal
function loadDataForescast() {
    var campaignID = localStorage.campaignID;
    //Cargando el detale del Modal
    $.ajax({
        type: "GET",
        url: "/Sales/ListForecastBySales?opt=" + "all" + "&campaignID=" + campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (dataDetalle) {
            if (dataDetalle.length > 2) {
                _dataDetalle = JSON.parse(dataDetalle);

                $.each(dataWeeks, function (iweek, week) {
                    $("table#tablaDetalleForescast thead tr").append('<th style="min-width: 50px !important;">' + week.number + '-' + week.year + '</th>');
                })
                $("table#tablaDetalleForescast thead tr").append('<th style="min-width: 50px !important;">Total</th>');
                loadDataDetalleForescast2(_dataDetalle);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });

}

function loadDataDetalleForescast2(dataDetailList) {

    $.each(dataDetailList, function (index, row) {
        var exists = false;
        var trfind = [];
        $('table#tablaDetalleForescast > tbody  > tr').each(function (index, tr) {
            var growerID = $(tr).find("[data-name='growerID']").text();

            if (row.growerID == growerID) {
                exists = true
                trfind = tr;
            }

        });

        if (exists == false) {
            var tr = '';
            tr += '<tr class="parent' + row.growerID + ' text-align-left"  style="background-color:#E3E0E0">';
            tr += '<td class=""  style="display: none" data-name="growerID">' + row.growerID + '</td>';
            tr += "<td class='sticky9 details-control'> <span id='expand' class='abrir'><i class='fas fa-plus-circle fa-sm'></i></span><span id='contraer' class='cerrar' style='display:none'><i class='fas fa-minus fa-sm'></i></span></td>";
            tr += "<td class='sticky10' style='max - width: 125px; min - width: 125px'>" + row.grower + "</td>";


            $.each(dataWeeks, function (iWeek, week) {
                if (row.projectedWeekID == week.projectedWeekID) {
                    //tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                    tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right">' + (parseFloat(row.total).toFixed(2)) + '</td>'
                } else {
                    //tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right"></td>'
                    tr += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
                }
            })
            tr += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
            tr += '</td>';
            tr += '</tr>';
            ////console.log(tr);

            $("table#tablaDetalleForescast tbody").append(tr);

        } else {
            var idx = 0
            $.each(dataWeeks, function (iWeek, week) {
                if (row.projectedWeekID == week.projectedWeekID) {
                    //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                    $(trfind).find('td.mount:eq(' + idx + ')').text((parseFloat(row.total).toFixed(2)))
                }
                idx = parseInt(idx) + 1
            })
        }
    })
    addSubTotalForescast("tablaDetalleForescast");
    $('table#tablaDetalleForescast>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

    loadDataSubDetalleForescast2()

}

function loadDataSubDetalleForescast2() {
    var campaignID = localStorage.campaignID;
    var _dataDetailForescast = []
    $.ajax({
        type: "GET",
        url: "/Sales/ListForecastBySalesVar?opt=" + "var" + "&campaignID=" + campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (dataDetalle) {
            if (dataDetalle.length > 2) {
                _dataDetailForescast = JSON.parse(dataDetalle);
                //loadDataDetalleModalSemana(_dataDetailListSemana);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });

    $('table#tablaDetalleForescast > tbody  > tr').each(function (index, tr) {
        var growerID = $(tr).find("[data-name='growerID']").text();
        //$(tr).append("<table><</table>")

        $datafiter = _dataDetailForescast.filter(item => item.growerID == growerID)
        $($datafiter).each(function (i, e) {
            var exists = false;
            var trFind = []
            $('table#tablaDetalleForescast>tbody>tr.subDetail').each(function (iSd, sd) {
                var growerID = $(sd).find("[data-name='growerID']").text();
                var varietyID = $(sd).find("[data-name='varietyID']").text();

                if (e.growerID == growerID && e.varietyID == varietyID) {
                    exists = true;

                    trFind = sd
                }
            })

            if (exists == false) {
                var trNew = "<tr style='background-color:#E5EBFF;display: none;' class='subDetail child-" + e.growerID + "' data-growerid='growerID'><td data-name='growerID' style='display:none'>" + e.growerID + "</td>";
                trNew += "<td style='color: #E5EBFF' data-name='varietyID'>" + e.varietyID + "</td>"
                trNew += "<td >" + e.variety + "</td>"

                $.each(dataWeeks, function (iWeek, week) {
                    if (e.projectedWeekID == week.projectedWeekID) {
                        //tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.quantity).toFixed(2)) + '</td>'
                        trNew += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right">' + (parseFloat(e.total).toFixed(2)) + '</td>'
                        //trNew+="<td></td>"
                    } else {
                        //tr += '<td style="min-width: 50px !important;text-align: right !important" class="mount text-align-right"></td>'
                        trNew += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
                        //trNew += "<td></td>"
                    }
                })
                trNew += '<td style="min-width: 50px!important;text-align: right !important" class="subtotalRow text-align-right">';
                trNew += '</td>';
                trNew += "</tr>"

                $(tr).after(trNew);
            } else {
                var idx = 0
                $.each(dataWeeks, function (iWeek, week) {
                    if (e.projectedWeekID == week.projectedWeekID) {
                        //oldMount = $(trfind).find('td.mount:eq(' + idx + ')').text();
                        $(trFind).find('td.mount:eq(' + idx + ')').text((parseFloat(e.total).toFixed(2)))
                    }
                    idx = parseInt(idx) + 1
                })
            }
        })
    });
    $('table#tablaDetalleForescast>tbody>tr').each(function (i, e) {
        var totalrow = 0
        $(e).find('td.mount').each(function (itd, td) {
            mount = 0
            if ($(td).text() != "") {
                mount = $(td).text()
            }
            totalrow = parseFloat(mount) + totalrow
        })
        $(e).find('td.subtotalRow').text(parseFloat(totalrow).toFixed(2));
    })

}

function loadDataDetalleForescast(dataDetailList) {
    var content = "";

    //Cargar combo lote tabla

    $.each(dataDetailList, function (index, row) {
        content += "<tr class='text-align-left'>";
        content += "<td class='stiky9' style='display:none'>" + row.growerID + "</td>";
        content += "<td class='stiky10 details-control'> <span id='expand' class='abrir'><i class='fas fa-plus-circle fa-sm'></i></span><span id='contraer' class='cerrar' style='display:none'><i class='fas fa-minus fa-sm'></i></span></td>";
        content += "<td class='stiky10' style='max - width: 125px; min - width: 125px'>" + row.grower + "</td>";

        $.each(dataWeeks, function (iWeek, week) {
            if (row.projectedWeekID == week.projectedWeekID) {
                content += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right">' + (parseFloat(row.total).toFixed(2)) + '</td>'
            } else {
                content += '<td style="min-width: 50px!important;text-align: right !important" class="mount text-align-right"></td>'
            }
        })

        content += "    </tr>";

        //$("#tablaDetalleForescast").dataTable().fnDestroy();
        $("#tablaDetalleForescast>tbody").append(content);
        //console.log(content)

    }
    );
    $("#tablaDetalleForescast").DataTable({
        "paging": true,
        "ordering": false,
        "info": false,
        "responsive": true,
        //"destroy": true,
        dom: 'Bfrtip',
        //select: true,
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'All']
        ],
        dom: 'Bfrtip',
        buttons: [

            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });
}

function addSubTotalForescast(id) {
    ////console.log('addSubTotalClient');
    //$("#" + id + " tr.trSubtotal").remove();

    var sum = 0.0;
    var dataSubtTotal = [];

    $.each(dataWeeks, function (iWeeks, valWeek) {
        dataSubtTotal.push({ "id": iWeeks, "mount": 0 });
    });

    $('#' + id + ' > tbody > tr').each(function (index, element) {

        var iSpanCols = parseInt(3)

        $.each(dataWeeks, function (index2, value) {

            $valCell = $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text();
            //$valWeight = parseFloat($(element).find('td:eq(7)').text());

            if ($.isNumeric($valCell)) {

            } else {
                $(element).find('td:eq(' + parseInt(index2 + iSpanCols) + ')').text('');
                $valCell = 0;
            }

            $.each(dataSubtTotal, function (i, v) {
                if (v.id == index2) {
                    v.mount += parseFloat($valCell);
                    //if (parseFloat(v.mount) > 0) //console.log(v.mount);
                }
            });
        });
    });

    //console.log(JSON.stringify(dataSubtTotal))
    content = "";

    $.each(dataSubtTotal, function (iweek, week) {

        content += "<th style='text-align: right'>" + parseFloat(week.mount).toFixed(2) + "</th>";

    })

    $("table#" + id + " tfoot>tr.total1").append(content);

    content1 = "";

    $.each(dataSubtTotal, function (iweek, week) {

        content1 += "<th style='text-align: right'>" + _dataSaleSubTotal[iweek] + "</th>";

    })

    $("table#tablaDetalleForescast>tfoot>tr.total2").append(content1);

    content2 = "";
    saldo = 0;

    $.each(dataSubtTotal, function (iweek, week) {

        saldo = (parseFloat(week.mount) - parseFloat(_dataSaleSubTotal[iweek])).toFixed(2) + saldo;
        content2 += "<th style='text-align: right'>" + parseFloat(saldo).toFixed(2) + "</th>";
        //console.log(saldo);

        //content2 += "<th style='text-align: right'>" + parseFloat(parseFloat(week.mount).toFixed(2) - _dataSaleSubTotal[iweek]).toFixed(2) + "</th>";
        
    })

    $("table#tablaDetalleForescast>tfoot>tr.diferencia").append(content2);

}