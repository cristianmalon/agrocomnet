﻿var dataResult = [];
var _lstSync;
var _lstNoSync;
var _lstSummary;
(function () {
    var Handler = function () {
        //>>>Variables Globales
        var _modalSync = null;
        var _html = '';
        //<<<Variables Globales

        //>>>Definición de controles
        var ctrl = {
            _SyncNetsuiteCP: $('#SyncNetsuiteCP'),
            _SyncNetsuiteSO: $('#SyncNetsuiteSO'),
            _SyncNetsuiteSI: $('#SyncNetsuiteSI'),
            _lblTitleSyncNetsuite: $('#lblTitleSyncNetsuite'),
            _lblsubTitleResultSuccess: $('#lblsubTitleResultSuccess'),
            _lblsubTitleResultFail: $('#lblSubTitleResultFail'),
            _resultSummary: $('#resultSummary'),
            _resultSuccess: $('#resultSuccess'),
            _resultFail: $('#resultFail'),

        };
        //<<<Definición de controles


        var _that = {
            Init: function () {
                if (localStorage.UsrType != undefined) {
                    if (localStorage.UsrType.toLowerCase() == 'dem') ctrl._SyncNetsuiteCP.css('visibility', 'visible');
                    else ctrl._SyncNetsuiteCP.css('visibility', 'hidden');
                    if (localStorage.UsrType.toLowerCase() == 'dem') ctrl._SyncNetsuiteSO.css('visibility', 'visible');
                    else ctrl._SyncNetsuiteSO.css('visibility', 'hidden');
                    if (localStorage.UsrType.toLowerCase() == 'dem') ctrl._SyncNetsuiteSI.css('visibility', 'visible');
                    else ctrl._SyncNetsuiteSI.css('visibility', 'hidden');
                }
                this.Eventos();
                var sUrl = window.location.pathname;
                var arrayUrl = sUrl.split('/');
                var sController = arrayUrl[1];
                var sAction = arrayUrl[2];
                //switch (sController.toLowerCase()) {
                //    case "home": break;
                //    default:
                //        if (sAction == undefined) sAction = "Index";
                //        let dataJson = JSON.parse(localStorage.option);                        
                //        let menuFilter = dataJson.filter((item) => item.cropid == localStorage.cropID
                //            && item.controller == sController && item.action == sAction);
                //        if (menuFilter == undefined || menuFilter.length == 0) {
                //            localStorage.deniedAccess = true;                                                                              
                //        }
                //        break;
                //}
            },
            Metodos: {
                Init: function () {

                },
                ShowResponseCP: function (data) {
                    _lstSync = data.lstPlnComSync;
                    _lstNoSync = data.lstPlnComNoSync;
                    _lstSummary = data.lstPlnSummary;

                    ctrl._lblTitleSyncNetsuite.html('SYNC COMMERCIAL PLAN with NETSUITE');
                    if (_lstSummary != null) {
                        _html = '';
                        _html += '<h5>SEASONS</h5>'
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Campaign</th><th style="text-align:center;">Crop</th><th style="text-align:center;">Week.Ini</th><th style="text-align:center;">Week.End</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstSummary, function (i, e) {
                            _html += '<tr>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].campaignID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].campaign + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].cropID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].weekIni + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].weekFin + '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table></center>';
                        _html += '</div>'
                        ctrl._resultSummary.empty().append(_html);
                    }
                    if (_lstSync != null) {                       
                        _html = '';
                        _html += 'Total CP Synchronized: ' + _lstSync.length;
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">Plan.Id.Agrocom</th><th style="text-align:center;" hidden>Plan.Id.Netsuite</th><th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Obs.</th><th style="text-align:center;">Search</th><th style="text-align:center;">Export</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstSync, function (i, e) {
                            _html += '<tr>';
                            _html += '<td  style="text-align:center;" class="tdidPlan">' + _lstSync[i].idPlan + '</td>';
                            _html += '<td  style="text-align:center;" hidden>' + _lstSync[i].id + '</td>';
                            _html += '<td  style="text-align:center;" class="tdcampaignID">' + _lstSync[i].campaignID + '</td>';
                            _html += '<td  style="text-align:center;" >' + 'Envío exitoso' + '</td>'; //Se cambió _lstSync[i].mensaje por mensaje personalizado
                            _html += '<td style="text-align:center;">';
                            _html += '<button class="btn btn-info btn-sm btn-icon-split transact  btnRowPC">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-search fa-sm" ></i></span ><span class="mr-2 d-none d-lg-inline text">CP Info.</span>';
                            _html += '</button >';
                            _html += '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<input type="hidden" value="commercial_plan"/>';
                            _html += '<a href="#" class="btn btn-success btn-sm btn-icon-split transact  btnExportDataToExcel">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-file-excel fa-sm"></i></span ><span class="mr-2 d-none d-lg-inline text">CP Info.</span>';
                            _html += '</a>';
                            _html += '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._lblsubTitleResultSuccess.html('<h5 class="badge badge-primary badge-pill">DATA SYNC</h5>');
                        ctrl._resultSuccess.empty().append(_html);
                        
                    }
                    if (_lstNoSync != null) {                        
                        _html = '';
                        _html += 'Total CP NO Synchronized: ' + _lstNoSync.length;
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">Plan.Id</th><th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Obs.</th><th style="text-align:center;">Search</th><th style="text-align:center;">Export</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstNoSync, function (i, e) {
                            _html += '<tr>';
                            _html += '<td  style="text-align:center;" class="tdidPlan">' + _lstNoSync[i].idPlan + '</td>';
                            _html += '<td  style="text-align:center;" class="tdcampaignID">' + _lstNoSync[i].campaignID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstNoSync[i].mensaje + '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<button class="btn btn-info btn-sm btn-icon-split transact  btnRowPC">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-search fa-sm" ></i></span ><span class="mr-2 d-none d-lg-inline text">CP Info.</span>';
                            _html += '</button>';
                            _html += '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<input type="hidden" value="commercial_plan"/>';
                            _html += '<a href="#" class="btn btn-success btn-sm btn-icon-split transact  btnExportDataToExcel">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-file-excel fa-sm"></i></span ><span class="mr-2 d-none d-lg-inline text">CP Info.</span>';
                            _html += '</a>';
                            _html += '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._lblsubTitleResultFail.html('<h5 class="badge badge-danger badge-pill">DATA NO SYNC</h5>');
                        ctrl._resultFail.empty().append(_html);
                    }
                },
                ShowResponseSO: function (data) {
                    _lstSync = data.lstSOSync;
                    _lstNoSync = data.lstSONoSync;
                    _lstSummary = data.lstSOSummary;

                    ctrl._lblTitleSyncNetsuite.html('SYNC SALES ORDER with NETSUITE');
                    if (_lstSummary != null) {
                        _html = '<h5>SEASONS</h5>';
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Campaign</th><th style="text-align:center;">Crop</th><th style="text-align:center;">Week.Ini</th><th style="text-align:center;">Week.End</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstSummary, function (i, e) {
                            _html += '<tr>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].campaignID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].campaign + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].cropID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].weekIni + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].weekFin + '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._resultSummary.empty().append(_html);
                    }
                    if (_lstSync != null) {
                        _html = '';
                        _html += 'Total SO Synchronized: ' + _lstSync.length;
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">SO.Id.Agrocom</th><th style="text-align:center;">SO.Id.Netsuite</th><th style="text-align:center;">SO.Number</th><th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">ProjectedWeek.Id</th><th style="text-align:center;">Obs.</th><th style="text-align:center;">Search</th><th style="text-align:center;">Export</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstSync, function (i, e) {
                            _html += '<tr>';
                            _html += '<td  style="text-align:center;" class="tdidSo">' + _lstSync[i].idOS + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSync[i].id + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSync[i].requestNumber + '</td>';
                            _html += '<td  style="text-align:center;" class="tdcampaignID">' + _lstSync[i].campaignID + '</td>';
                            _html += '<td  style="text-align:center;" class="tdprojectedWeekID">' + _lstSync[i].projectedWeekID + '</td>';
                            //_html += '<td  style="text-align:center;" >' + _lstSync[i].error + '</td>';
                            _html += '<td  style="text-align:center;" >' + 'Envío exitoso' + '</td>';
                            _html += '<td  style="text-align:center;" >';
                            _html += '<button class="btn btn-info btn-sm btn-icon-split transact  btnRowSO">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-search fa-sm" ></i></span ><span class="mr-2 d-none d-lg-inline text">SO Info.</span>';
                            _html += '</button >';
                            _html += '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<input type="hidden" value="sale_order"/>';
                            _html += '<a href="#" class="btn btn-success btn-sm btn-icon-split transact  btnExportDataToExcel">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-file-excel fa-sm"></i></span ><span class="mr-2 d-none d-lg-inline text">SO Info.</span>';
                            _html += '</a>';
                            _html += '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._lblsubTitleResultSuccess.html('<h5 class="badge badge-primary badge-pill">DATA SYNC</h5>');
                        ctrl._resultSuccess.empty().append(_html);
                        
                    }
                    if (_lstNoSync != null) {
                        _html = '';                        
                        _html += 'Total SO NO Synchronized: ' + _lstNoSync.length;
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">SO.Id</th><th style="text-align:center;">SO.Number</th><th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">ProjectedWeek.Id</th><th style="text-align:center;">Obs.</th><th style="text-align:center;">Search</th><th style="text-align:center;">Export</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstNoSync, function (i, e) {
                            _html += '<tr>';
                            _html += '<td style="text-align:center;" class="tdidSo">' + _lstNoSync[i].idOS + '</td>';
                            _html += '<td style="text-align:center;" >' + _lstNoSync[i].requestNumber + '</td>';
                            _html += '<td style="text-align:center;" class="tdcampaignID">' + _lstNoSync[i].campaignID + '</td>';
                            _html += '<td style="text-align:center;" class="tdprojectedWeekID">' + _lstNoSync[i].projectedWeekID + '</td>';
                            _html += '<td style="text-align:center;" >' + _lstNoSync[i].mensaje + '</td>';
                            _html += '<td style="text-align:center;" >';
                            _html += '<button class="btn btn-info btn-sm btn-icon-split transact  btnRowSO">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-search fa-sm" ></i></span ><span class="mr-2 d-none d-lg-inline text">SO Info.</span>';
                            _html += '</button>';
                            _html += '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<input type="hidden" value="sale_order"/>';
                            _html += '<a href="#" class="btn btn-success btn-sm btn-icon-split transact  btnExportDataToExcel">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-file-excel fa-sm"></i></span ><span class="mr-2 d-none d-lg-inline text">SO Info.</span>';
                            _html += '</a>';
                            _html += '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._lblsubTitleResultFail.html('<h5 class="badge badge-danger badge-pill">DATA NO SYNC</h5>');
                        ctrl._resultFail.empty().append(_html);
                        
                    }
                },
                ShowResponseIE: function (data) {
                    _lstSync = data.lstIESync;
                    _lstNoSync = data.lstIENoSync;
                    _lstSummary = data.lstPlnSummary;

                    ctrl._lblTitleSyncNetsuite.html('SYNC SHIPPING INSTRUCTION with NETSUITE');
                    if (_lstSummary != null) {
                        _html = '<h5>SEASONS</h5>';
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Campaign</th><th style="text-align:center;">Crop</th><th style="text-align:center;">Week.Ini</th><th style="text-align:center;">Week.End</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstSummary, function (i, e) {
                            _html += '<tr>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].campaignID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].campaign + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].cropID + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].weekIni + '</td>';
                            _html += '<td  style="text-align:center;" >' + _lstSummary[i].weekFin + '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._resultSummary.empty().append(_html);
                    }
                    if (_lstSync != null) {
                        ctrl._lblsubTitleResultSuccess.html();
                        _html = '';
                        _html += 'Total IE Synchronized: ' + _lstSync.length
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">IE.Id.Agrocom</th><th style="text-align:center;">IE.Id.Netsuite</th><th style="text-align:center;">IE.Number</th><th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Obs.</th><th style="text-align:center;">Search</th><th style="text-align:center;">Export</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstSync, function (i, e) {
                            _html += '<tr>';
                            _html += '<td style="text-align:center;" class="tdidIE">' + _lstSync[i].idIE + '</td>';
                            _html += '<td style="text-align:center;" >' + _lstSync[i].id + '</td>';
                            _html += '<td style="text-align:center;" >' + _lstSync[i].packingNumber + '</td>';
                            _html += '<td style="text-align:center;" class="tdcampaignID">' + _lstSync[i].campaignID + '</td>';
                            //_html += '<td style="text-align:center;" >' + _lstSync[i].error + '</td>';
                            _html += '<td  style="text-align:center;" >' + 'Envío exitoso' + '</td>';
                            _html += '<td style="text-align:center;" >';
                            _html += '<button class="btn btn-info btn-sm btn-icon-split transact  btnRowIE">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-search fa-sm" ></i></span ><span class="mr-2 d-none d-lg-inline text">SI Info.</span>';
                            _html += '</button >';
                            _html += '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<input type="hidden" value="shipping_instruction"/>';
                            _html += '<a href="#" class="btn btn-success btn-sm btn-icon-split transact  btnExportDataToExcel">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-file-excel fa-sm"></i></span ><span class="mr-2 d-none d-lg-inline text">SI Info.</span>';
                            _html += '</a>';
                            _html += '</td > ';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._resultSuccess.empty().append(_html);
                        ctrl._lblsubTitleResultSuccess.html('<h5 class="badge badge-primary badge-pill">DATA SYNC</h5>');
                    }
                    if (_lstNoSync != null) {                        
                        _html = '';
                        _html += 'Total IE NO Synchronized: ' + _lstNoSync.length;
                        _html += '<div style="border: 1px solid #000000;">';
                        _html += '<center>';
                        _html += '<table class="table table-hover table-bordered table-striped no-footer dataTable">';
                        _html += '<thead class="thead-agrocom">';
                        _html += '<tr>';
                        _html += '<th style="text-align:center;">IE.Id</th><th style="text-align:center;">IE.Number</th><th style="text-align:center;">Campaign.Id</th><th style="text-align:center;">Obs.</th><th style="text-align:center;">Search</th><th style="text-align:center;">Export</th>';
                        _html += '</tr>';
                        _html += '</thead>';
                        _html += '<tbody>';
                        $.each(_lstNoSync, function (i, e) {
                            _html += '<tr>';
                            _html += '<td style="text-align:center;" class="tdidIE">' + _lstNoSync[i].idIE + '</td>';
                            _html += '<td style="text-align:center;" >' + _lstNoSync[i].packingNumber + '</td>';
                            _html += '<td style="text-align:center;" class="tdcampaignID">' + _lstNoSync[i].campaignID + '</td>';
                            _html += '<td style="text-align:center;" >' + _lstNoSync[i].mensaje + '</td>';
                            _html += '<td style="text-align:center;" >';
                            _html += '<button class="btn btn-info btn-sm btn-icon-split transact  btnRowIE">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-search fa-sm" ></i></span ><span class="mr-2 d-none d-lg-inline text">SI Info.</span>';
                            _html += '</button>';
                            _html += '</td>';
                            _html += '<td style="text-align:center;">';
                            _html += '<input type="hidden" value="shipping_instruction"/>';
                            _html += '<a href="#" class="btn btn-success btn-sm btn-icon-split transact  btnExportDataToExcel">';
                            _html += '<span class="icon text-white-50"><i class="fas fa-file-excel fa-sm"></i></span ><span class="mr-2 d-none d-lg-inline text">SI Info.</span>';
                            _html += '</a>';
                            _html += '</td>';
                            _html += '</tr>'
                        });
                        _html += '</tbody>';
                        _html += '</table>';
                        _html += '</center>';
                        _html += '</div>'
                        ctrl._lblsubTitleResultFail.html('<h5 class="badge badge-danger badge-pill">DATA NO SYNC</h5>');
                        ctrl._resultFail.empty().append(_html);
                        
                    }
                },
                SyncCommercialPlan: function () {
                    _modalSync = $('#modalSyncNetsuite');
                    _modalSync.modal('show');
                    let sUrlEndPoint = getPath() + HOST_NETSUITE + ENDPOINT_PUT_COMMERCIAL_PLAN;

                    $.ajax({
                        type: "PUT",
                        url: sUrlEndPoint,
                        headers: {
                            'Authorization': 'Bearer ' + $('#hToken').val(),
                            'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0'
                        },
                        async: false,
                        success: function (data) {
                            _that.Metodos.ShowResponseCP(data);
                        }
                    });
                },
                SyncSaleOrder: function () {
                    _modalSync = $('#modalSyncNetsuite');
                    _modalSync.modal('show');
                    let sUrlEndPoint = getPath() + HOST_NETSUITE + ENDPOINT_PUT_SALE_ORDER;

                    $.ajax({
                        type: "PUT",
                        url: sUrlEndPoint,
                        headers: {
                            'Authorization': 'Bearer ' + $('#hToken').val(),
                            'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0'
                        },
                        async: false,
                        success: function (data) {
                            _that.Metodos.ShowResponseSO(data);
                        }
                    });
                },
                SyncShippingInstruction: function () {
                    _modalSync = $('#modalSyncNetsuite');
                    _modalSync.modal('show');
                    let sUrlEndPoint = getPath() + HOST_NETSUITE + ENDPOINT_PUT_SHIPPING_INSTRUCTION;

                    $.ajax({
                        type: "PUT",
                        url: sUrlEndPoint,
                        headers: {
                            'Authorization': 'Bearer ' + $('#hToken').val(),
                            'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0'
                        },
                        async: false,
                        success: function (data) {
                            _that.Metodos.ShowResponseIE(data);
                        }
                    });
                },

            },
            Eventos: function () {
                ctrl._SyncNetsuiteCP.click(function () {
                    Swal.fire({
                        imageUrl: '../images/AgricolaAndrea71x64.png',
                        title: 'Agrocom - Netsuite',
                        text: 'Synchronizing commercial plan...',
                        onBeforeOpen() {
                            Swal.showLoading();
                        },
                        onOpen() {
                            _that.Metodos.SyncCommercialPlan();
                            Swal.getTitle().textContent = 'Done!'
                            Swal.hideLoading();
                        },
                        onAfterClose() {

                        },
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                });
                ctrl._SyncNetsuiteSO.click(function () {
                    Swal.fire({
                        imageUrl: '../images/AgricolaAndrea71x64.png',
                        title: 'Agrocom - Netsuite',
                        text: 'Synchronizing sales order...',
                        onBeforeOpen() {
                            Swal.showLoading();
                        },
                        onOpen() {
                            _that.Metodos.SyncSaleOrder();
                            Swal.getTitle().textContent = 'Done!'
                            Swal.hideLoading();
                        },
                        onAfterClose() {

                        },
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                });
                ctrl._SyncNetsuiteSI.click(function () {
                    Swal.fire({
                        imageUrl: '../images/AgricolaAndrea71x64.png',
                        title: 'Agrocom - Netsuite',
                        text: 'Synchronizing shipping instruction...',
                        onBeforeOpen() {
                            Swal.showLoading();
                        },
                        onOpen() {
                            _that.Metodos.SyncShippingInstruction();
                            Swal.getTitle().textContent = 'Done!'
                            Swal.hideLoading();
                        },
                        onAfterClose() {

                        },
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                });
            }
        };

        return _that;
    }

    $(document).ready(function () {
        Handler().Init();
    });
    $(document).on("click", ".btnRowPC", function () {
        let objbtn = $(this);
        Swal.fire({
            title: 'Getting commercial plan information...',
            onBeforeOpen() {
                Swal.showLoading();
            },
            onOpen() {
                GetCP(objbtn);
                Swal.getTitle().textContent = 'Done!'
                Swal.hideLoading();
            },
            onAfterClose() {

            },
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
        })        
    });
    $(document).on("click", ".btnRowSO", function () {
        let objbtn = $(this);
        Swal.fire({
            title: 'Getting sale order information...',
            onBeforeOpen() {
                Swal.showLoading();
            },
            onOpen() {
                GetSO(objbtn);
                Swal.getTitle().textContent = 'Done!'
                Swal.hideLoading();
            },
            onAfterClose() {

            },
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
        })
    });
    $(document).on("click", ".btnRowIE", function () {
        let objbtn = $(this);
        Swal.fire({
            title: 'Getting shipping instruction information...',
            onBeforeOpen() {
                Swal.showLoading();
            },
            onOpen() {
                GetIE(objbtn);
                Swal.getTitle().textContent = 'Done!'
                Swal.hideLoading();
            },
            onAfterClose() {

            },
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
        })
    });
    $(document).on("click", ".btnExportDataJsonSync", function () {
        if (_lstSync.length == 0) {
            sweet_alert_info('Information', 'No records were found to export.');
            return;
        }

        $("<a />", {
            "download": "data_synchronized.json",
            "href": "data:application/json," + encodeURIComponent(JSON.stringify(_lstSync))
        }).appendTo("body")
            .click(function () {
                $(this).remove()
            })[0].click()
    });
    $(document).on("click", ".btnExportDataJsonNoSync", function () {
        if (_lstNoSync.length == 0) {
            sweet_alert_info('Information', 'No records were found to export.');
            return;
        }
        $("<a />", {
            "download": "data_no_synchronized.json",
            "href": "data:application/json," + encodeURIComponent(JSON.stringify(_lstNoSync))
        }).appendTo("body")
            .click(function () {
                $(this).remove()
            })[0].click()
    });
    $(document).on("click", ".btnExportDataToExcel", function () {        
        if (dataResult.length == 0) {
            sweet_alert_info('Information', 'No records were found to export.');
            return;
        }
        let tr = $(this).parent();
        let hfValue = tr.find('input[type="hidden"]').val();
        ExportDataToExcel(this, dataResult, hfValue, 'data');
        dataResult = [];
    });

})();

$(function () {
    var altura = window.screen.height + 100;
    if (window.screen.availWidth < 500) {
        window.parent.document.body.style.zoom = 1;
        $(".alturaP").css({
            height: "auto",
        });
    } else {
        window.parent.document.body.style.zoom = 0.9;
        $(".alturaP").css({
            height: "" + altura + "",
        });
    }
    $(window).resize(function () {
        if (window.screen.availWidth < 500) {
            window.parent.parent.document.body.style.zoom = 1;
            $(".alturaP").css({
                height: "auto",
            });
        } else {
            window.parent.parent.document.body.style.zoom = 0.9;
            $(".alturaP").css({
                height: "" + altura + "",
            });
        }
    });

    if (localStorage.campaignID == undefined || localStorage.campaignID == null) {
        localStorage.clear();
        sessionStorage.clear();
        loadDataCrop();

    } else {
        let dataCrops = JSON.parse(localStorage.crops);
        var dataCampaign = dataCrops.map(
            obj => {
                return {
                    "id": obj.campaignID,
                    "name": obj.campaign
                }
            }
        )

        loadCombo(dataCampaign, "selectCampaignCrop", false);
        $(selectCampaignCrop).selectpicker('val', localStorage.campaignID)
        $(selectCampaignCrop).selectpicker('refresh');
        $(txtCropCampaign).text(localStorage.campaign);        
    }

    getCurrentWeek();
    $('#btnChangeCrop').click(function () {
        changeCrop();
    });    
});

function ClearCache() {
    sweet_alert_progressbar('Clearing Cache', '');
    localStorage.clear();
    sessionStorage.clear();
    deleteAllCookies();
    setTimeout(function () {
        window.location = window.location.origin + "/Login/Index"
    }, 3000);

}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++)
        deleteCookie(cookies[i].split("=")[0]);
}

function setCookie(name, value, expirydays) {
    var d = new Date();
    d.setTime(d.getTime() + (expirydays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + "; " + expires;
}

function deleteCookie(name) {
    setCookie(name, "", -1);
}

function logout() {

    localStorage.clear();
    sessionStorage.clear();
    window.location = window.location.origin + "/Login/Index"
}

function getCurrentWeek() {
    let _opt = 'act';
    let _val = '';
    let _weekIni = 0;
    let _weekEnd = 0;
    let _userID = 0;
    let _campaignID = localStorage.campaignID;
    $.ajax({
        type: "GET",
        url: "/PO/GetCurrentWeek?opt=" + _opt + "&val=" + _val + "&weekIni=" + _weekIni + "&weekEnd=" + _weekEnd + "&userID=" + _userID + "&campaignID=" + _campaignID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            let dataJson = JSON.parse(data);
            let actualyWeek = [];
            if (data.length > 0) {
                actualyWeek = dataJson.map(
                    obj => {
                        return {
                            "id": obj.projectedWeekID,
                            "name": obj.description
                        }
                    }
                );
            }
            $(txtCurrentWeek).text(actualyWeek[0].name);
        }
    });
}

function sweet_alert_error(Tituto, Mensaje) {
    Swal.fire({
        //imageUrl: '../images/AgricolaAndrea71x64.png',
        title: Tituto,
        text: Mensaje,
        timer: 2000,
        icon: 'error',
        showCancelButton: false,
        showConfirmButton: false,
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#3085d6',
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        }
    });
}

function sweet_alert_warning(Tituto, Mensaje) {
    Swal.fire({
        //imageUrl: '../images/AgricolaAndrea71x64.png',
        title: Tituto,
        text: Mensaje,
        timer: 2000,
        icon: 'warning',
        showCancelButton: false,
        showConfirmButton: false,
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#F19B4A',
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        }
    });
}

function sweet_alert_info(Tituto, Mensaje) {
    Swal.fire({
        //imageUrl: '../images/AgricolaAndrea71x64.png',
        title: Tituto,
        text: Mensaje,
        timer: 3000,
        icon: 'info',
        showCancelButton: false,
        showConfirmButton: false,
        confirmButtonText: 'OK',
        confirmButtonColor: '#F19B4A',
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },        
    })
}

function sweet_alert_progressbar() {
    Swal.fire({
        imageUrl: '../images/AgricolaAndrea71x64.png',
        title: 'AGROCOM',
        html: 'Loading information...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
    });
    Swal.showLoading();
}

function sweet_alert_progressbar_cerrar() {
    Swal.getTitle().textContent = 'Done!'
    Swal.hideLoading();    
}

function sweet_alert_success(Tituto, Mensaje) {
    Swal.fire({
        //imageUrl: '../images/AgricolaAndrea71x64.png',
        title: Tituto,
        text: Mensaje,
        timer: 2000,
        icon: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        //confirmButtonText: 'Aceptar',
        //confirmButtonColor: '#3085d6',
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
    });
}

function sweet_alert_success_prg_copytoclipboard(Tituto, Mensaje, Valor) {
    Swal.fire({
        //imageUrl: '../images/AgricolaAndrea71x64.png',
        title: Tituto,
        text: Mensaje + ' ' + Valor,
        timer: 2000,
        icon: 'success',
        //html: '<label id="lblCustomise" data-clipboard-text>' + Valor + '</label>',
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#3085d6',
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        }
    });
}

function sweet_alert_customise(Tituto, Mensaje, Icon) {
    Swal.fire({
        title: Tituto,
        text: Mensaje,
        timer: 2000,
        icon: (Icon == 1) ? 'success' : (Icon == 2) ? 'warning' : (Icon == 3) ? 'error' : 'info',
        showCloseButton: true,
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#3085d6',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        showDenyButton: true,
        denyButtonText: `Don't save`,
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        timer: 1500,
        width: 600,
        padding: '3em',
        background: '#fff url(/images/trees.png)',
        backdrop: `
            rgba(0,0,123,0.4)
            url("/images/nyan-cat.gif")
            left top
            no-repeat
          `,
        footer: '',
        imageUrl: '',
        imageHeight: '',
        imageAlt: '',
    });
}

function sweet_alert_notification(Tituto, Mensaje) {
    Swal.fire({
        title: '<strong><u>' + Tituto + '</u></strong>',
        icon: 'info',
        html: Mensaje,
        width: 1600,
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText:'<i class="fa fa-thumbs-up"></i> Great!',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText:'<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
    }).then((result) => {
        if (result.isConfirmed) {
            let _usrType = localStorage.UsrType.toLowerCase();
            switch (_usrType) {
                case 'gro': window.location = window.location.origin + "/Forecast/Crop"; break;
                //case 'com': window.location = window.location.origin + "/Login/Index";break;
                case 'plc': window.location = window.location.origin + "/PO/Confirm"; break;
                case 'pro': window.location = window.location.origin + "/PO/Confirm"; break;
                case 'cox': window.location = window.location.origin + "/Comex/LoadingInstruction"; break;
                default: break;
            }

        }
    })
}

function loadDataCrop() {
    var opt = 'cro';
    var userID = $(lblSessionUserID).text();
    $.ajax({
        type: "GET",
        url: "/Home/getCropByUserID?opt=" + opt + "&id=" + userID + "&token=" + $('#hToken').val(),
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
                let dataJson = JSON.parse(data);
                var dataCampaign = dataJson.map(
                    obj => {
                        return {
                            "id": obj.campaignID,
                            "name": obj.campaign
                        }
                    }
                )
                loadCombo(dataCampaign, "selectCampaignCrop", false);
                $(selectCampaignCrop).selectpicker('val', dataJson[0].campaignID)
                $(selectCampaignCrop).selectpicker('refresh');
                localStorage.campaignID = dataJson[0].campaignID;
                localStorage.cropID = dataJson[0].cropID;
                localStorage.campaign = dataJson[0].campaign;
                localStorage.crops = data;
                $(txtCropCampaign).text(localStorage.campaign);
                localStorage.Token = $('#hToken').val();
                localStorage.UsrType = $('#hUsrType').val();
                localStorage.SOwithoutProgram = $('#hSOwithoutProgram').val();
                localStorage.CustomerToSelectSOwithoutProgram = $('#hCustomerToSelectSOwithoutProgram').val();
                localStorage.ConsigneeToSelectSOwithoutProgram = $('#hConsigneeToSelectSOwithoutProgram').val();
                localStorage.measure = dataJson[0].measure;
                localStorage.perfil = dataJson[0].perfil;
                localStorage.option = $('#hOption').val();
            }
        }
    });    
}

function loadCombo(data, control, firstElement) {
    var content = "";
    if (firstElement == true) {
        content += "<option value=''>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadComboFilter(data, control, firtElement, textFirstElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='All'>" + textFirstElement + "</option>";
    }
    if (data != null)
        for (var i = 0; i < data.length; i++) {
            content += "<option value='" + data[i].id + "'>";
            content += data[i].name;
            content += "</option>";
        }
    $('#' + control).empty().append(content);
}

function changeCrop() {
    var varselectCampaignCrop = $('#selectCampaignCrop option:selected')
    $(txtCropCampaign).text(varselectCampaignCrop.text())
    $(changeCropModal).modal('toggle')
    localStorage.campaignID = $(selectCampaignCrop).val()
    let dataCrops = JSON.parse(localStorage.crops);
    localStorage.cropID = dataCrops.filter(item => item.campaignID == $(selectCampaignCrop).val())[0].cropID
    localStorage.campaign = $(varselectCampaignCrop).text()
    window.location = window.location.origin + "/Login/LoadMenu?userID=" + $(lblSessionUserID).text() + "&cropID=" + localStorage.cropID;
    localStorage.measure = dataCrops.filter(item => item.campaignID == $(selectCampaignCrop).val())[0].measure
    localStorage.perfil = dataCrops.filter(item => item.campaignID == $(selectCampaignCrop).val())[0].perfil
}

function loadImages() {
    document.getElementById("info_foot").innerHTML = "© " + new Date().getFullYear().toString() + " Copyright: MIGIVA " + new Date().getFullYear().toString();
}

function AvoidCloseModalByAnyKey(objModal) {
    $('#' + objModal).modal({
        backdrop: 'static',
        keyboard: false
    });
}

function DifferenceBeetween2dates(d1, d2) {
    var diff = Math.abs(d1 - d2);
    if (Math.floor(diff / 86400000)) {
        return Math.floor(diff / 86400000) + " days";
    } else if (Math.floor(diff / 3600000)) {
        return Math.floor(diff / 3600000) + " hours";
    } else if (Math.floor(diff / 60000)) {
        return Math.floor(diff / 60000) + " minutes";
    } else {
        return "< 1 minute";
    }
};

function NameWeekDay(fecha) {
    //Recibe fecha en formato DD/MM/YYYY
    fecha = fecha.split('/');
    if (fecha.length != 3) {
        return null;
    }
    //Array para calcular día de la semana de un año regular.
    var regular = [0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5];
    //Array para calcular día de la semana de un año bisiesto.
    var bisiesto = [0, 3, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6];
    //Array para hacer la traducción de resultado en día de la semana.
    var semana = ['Sundays', 'Mondays', 'Tuesdays', 'Wednesdays', 'Thursdays', 'Fridays', 'Saturdays'];
    //Día especificado en la fecha recibida.
    var dia = fecha[0];
    //Módulo acumulado del mes especificado en la fecha recibida.
    var mes = fecha[1] - 1;
    //Año especificado por la fecha recibida.
    var anno = fecha[2];
    //Comparación para saber si el año recibido es bisiesto.
    if ((anno % 4 == 0) && !(anno % 100 == 0 && anno % 400 != 0))
        mes = bisiesto[mes];
    else
        mes = regular[mes];

    //envíamos la fecha con formato dd/mm/aa
    //Se retorna el resultado del calculo del día de la semana.
    return semana[Math.ceil(Math.ceil(Math.ceil((anno - 1) % 7) + Math.ceil((Math.floor((anno - 1) / 4) - Math.floor((3 * (Math.floor((anno - 1) / 100) + 1)) / 4)) % 7) + mes + dia % 7) % 7)];
}

function GetCurrentDateTime() {
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year + " " + d.getHours() + ":" + d.getMinutes();

    return date;
};

function CalculateFooterValues(xthis, objTbl, inpNameCell, inpFooter) {
    var total = 0;
    var iCol = $(xthis).closest('td').index();
    $('table#' + objTbl + '>tbody>tr').each(function (i, e) {
        if (i == 0) return;
        objInput = $(e).closest('tr').find('td').eq(iCol).find('input[name="' + inpNameCell + '"]');

        if (objInput.val() == "") objInput.val(0);
        total = parseFloat(objInput.val()) + total;
    });
    $('table#' + objTbl + '>tfoot>tr').find('td').eq(iCol).find('input[name=' + inpFooter + ']').val(total.toFixed(2));
    if (total == 100 || total == 0) {
        $('table#' + objTbl + '>tfoot>tr').find('td').eq(iCol).find('input[name=' + inpFooter + ']').css('color', 'black');
    } else {
        $('table#' + objTbl + '>tfoot>tr').find('td').eq(iCol).find('input[name=' + inpFooter + ']').css('color', 'red');
    }
}

function CalculateHorizontalValues(xthis, objTbl, inpNameCell, inpFooter) {
    var total = 0;
    var iRow = $(xthis).closest('tr').index();
    var iCol = $(xthis).closest('td').index();
    var iTotCol = 0;
    $('table#' + objTbl + '>tbody>tr').each(function (i, e) {
        if (i != iRow) return;
        $(e).closest('tr').find('td').each(function (ii, ee) {
            iTotCol++;
            var _iCol = $(ee).closest('td').index();
            if (_iCol == 0) return;
            objInput = $(ee).closest('td').find('input[name="' + inpNameCell + '"]');
            if (objInput.length == 0) return;

            if (objInput.val() == "") objInput.val(0);
            total = parseFloat(objInput.val()) + total;
        });
    });

    $('table#' + objTbl + '>tbody>tr').find('td').eq(iTotCol - 1).find('input[name=' + inpFooter + ']').val(total.toFixed(2));
    if (total == 100 || total == 0) {
        $('table#' + objTbl + '>tbody>tr').find('td').eq(iTotCol - 1).find('input[name=' + inpFooter + ']').css('color', 'black');
    }
    else {
        $('table#' + objTbl + '>tbody>tr').find('td').eq(iTotCol - 1).find('input[name=' + inpFooter + ']').css('color', 'red');
    }
}

function getListToArray(dataList) {
    let _dataArray = [];
    $.each(dataList, function (i, e) {
        let item = [e.id, e.name, e.kpi, e.week];
        _dataArray.push(item);
    });
    return _dataArray;
}

function getPivotArray(dataArray, rowIndex, colIndex, dataIndex) {
    //Code from https://techbrij.com
    var result = {}, ret = [];
    var newCols = [];
    for (var i = 0; i < dataArray.length; i++) {

        if (!result[dataArray[i][rowIndex]]) {
            result[dataArray[i][rowIndex]] = {};
        }
        result[dataArray[i][rowIndex]][dataArray[i][colIndex]] = dataArray[i][dataIndex];

        //To get column names
        if (newCols.indexOf(dataArray[i][colIndex]) == -1) {
            newCols.push(dataArray[i][colIndex]);
        }
    }

    newCols.sort();
    var item = [];

    //Add Header Row
    item.push('Item');
    item.push.apply(item, newCols);
    ret.push(item);

    //Add content 
    for (var key in result) {
        item = [];
        item.push(key);
        for (var i = 0; i < newCols.length; i++) {
            item.push(result[key][newCols[i]] || "-");
        }
        ret.push(item);
    }
    return ret;
}

function GetCurrentDateTimeYYYYMMDDHHMM() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var curHour = today.getHours() > 12 ? today.getHours() : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
    var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    //today = mm + '/' + dd + '/' + yyyy;
    var now = yyyy + mm + dd + "-" + curHour + curMinute;
    return now;
}

function CuztomiseHtmlDatatableExport(tableHtmlToExport, autoExport, showExportButton, columnsToShow, sheetName, fileName) {
    $("#" + tableHtmlToExport).DataTable({
        initComplete: function () {
            if (autoExport) $('.buttons-excel').click();	//-->Exporta automáticamente
            if (showExportButton) $('.buttons-excel')[0].style.visibility = 'hidden';	//-->Oculta botón exporta
        },
        "paging": false,
        "ordering": false,
        "info": false,
        "responsive": true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel',
                footer: true,
                sheetName: sheetName,
                title: fileName,
                exportOptions: {
                    columns: (columnsToShow == null) ? 'visible' : columnsToShow,
                    format: {	//-->Exporta value de input
                        body: function (inner, rowidx, colidx, node) {
                            if ($(node).children("input").length > 0) {
                                return $(node).children("input").first().val();
                            } else {
                                return inner;
                            }
                        }
                    },

                }
            }
        ]
    });
}

/**
 * Get text of multi select dropdown
 * @param {any} idObjSelector - id of selector
 * @param {number} separator - 1=',' ; 2='\n'
 */
function GetTextMultiselect(idObjSelector, separator) {
    let _separator = (separator < 0) ? (-1) * separator : separator;
    let oSeparator;
    switch (_separator) {
        case 1: oSeparator = ','; break;
        case 2: oSeparator = '\n'; break;
        default: oSeparator = ' '; break;
    }
    let arrayObj = $('#' + idObjSelector + ' option:selected').map(function () {
        return $(this).text();
    });
    let strObj = '';
    $.each(arrayObj, function (iRow, oValue) {
        strObj += oValue + oSeparator;
    });
    return strObj;
}

/**
 * Get details of commercial plan selected
 * @param {any} xthis
 */
function GetCP(xthis) {    
    let tr = $(xthis).closest('tr');
    let _campaignID = $(tr).find("td.tdcampaignID").text();
    let _idPlan = $(tr).find("td.tdidPlan").text();
    var sUrlApi = "/CommercialPlan/PlanCustomerVarietyList?campaignID=" + _campaignID;
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    //console.log(dataJson);
                    let dataResultHeader = {};
                    
                    dataResultHeader["variety"] = "VARIETY";
                    dataResultHeader["category"] = "CATEGORY";
                    dataResultHeader["customer"] = "CUSTOMER";
                    dataResultHeader["program"] = "PROGRAM";
                    dataResultHeader["priority"] = "PRIORITY";
                    dataResultHeader["statusConfirm"] = "STATUSCONFIRM";
                    dataResultHeader["destination"] = "DESTINATION";                    
                    dataResultHeader["format"] = "FORMAT";
                    dataResultHeader["brand"] = "BRAND";
                    dataResultHeader["codePack"] = "CODEPACK";
                    //dataResultHeader["projectedWeekID"] = "PROJECTEDWEEKID";                    
                    //dataResultHeader["size"] = "SIZE";
                    //dataResultHeader["via"] = "VIA";                                                            
                    //console.log(dataResultHeader);

                    dataResult = dataJson.filter(item => item.planCustomerVarietyID == _idPlan).map(
                        obj => {
                            return {                                
                                "variety": obj.variety,
                                "category": obj.category,
                                "customer": obj.customer,
                                "program": obj.program,
                                "priority": obj.priority,
                                "statusConfirm": obj.statusConfirm,
                                "destination": obj.destination,
                                "format": obj.format,
                                "brand": obj.brand,
                                "codePack": obj.codePack,
                                //"projectedWeekID": obj.projectedWeekID,
                                //"size":obj.size,
                                //"via":obj.via,
                            }
                        }
                    );
                    dataResult.splice(0, 0, dataResultHeader)   //-->Agregando cabecera
                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {
                //console.log(dataResult);
            }

        }
    });
}

/**
 * Get details of sale order selected
 * @param {any} xthis
 */
function GetSO(xthis) {
    let tr = $(xthis).closest('tr');
    let _campaignID = $(tr).find("td.tdcampaignID").text();
    let _idSo = $(tr).find("td.tdidSo").text();
    let weekini = $(tr).find("td.tdprojectedWeekID").text();
    let weekFin = $(tr).find("td.tdprojectedWeekID").text();
    var opt = "rvw";
    var marketID = '';
    var statusID = 0;
    var plantID = '';
    var sUrlApi = "/PO/GetListPOReview?opt=" + opt + "&marketID=" + marketID + "&weekini=" + weekini + "&weekFin=" + weekFin + "&statusID=" + statusID + "&growerID=" + plantID + "&campaignID=" + _campaignID;
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        //async: true,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    //console.log(dataJson);
                    let dataResultHeader = {};
                    dataResultHeader["orderProductionID"] = "ORDERPRODUCTIONID";
                    dataResultHeader["boxes"] = "BOXES";
                    dataResultHeader["category"] = "CATEGORY";
                    dataResultHeader["codepack"] = "CODEPACK";
                    dataResultHeader["customer"] = "CUSTOMER";
                    dataResultHeader["destination"] = "DESTINATION";
                    dataResultHeader["format"] = "FORMAT";
                    dataResultHeader["grower"] = "GROWER";
                    dataResultHeader["label"] = "LABEL";
                    dataResultHeader["market"] = "MARKET";
                    dataResultHeader["orderProduction"] = "ORDERPRODUCTION";
                    dataResultHeader["packing"] = "PACKING";
                    dataResultHeader["priority"] = "PRIORITY";
                    dataResultHeader["program"] = "PROGRAM";
                    dataResultHeader["size"] = "SIZE";
                    dataResultHeader["statusConfirm"] = "STATUSCONFIRM";
                    dataResultHeader["variety"] = "VARIETY";
                    dataResultHeader["via"] = "VIA";
                    dataResultHeader["week"] = "WEEK";
                    dataResultHeader["weight"] = "WEIGHT";

                    dataResult = dataJson.filter(item => item.orderProductionID == _idSo).map(
                        obj => {
                            return {
                                "orderProductionID": obj.orderProductionID,
                                "boxes": obj.boxes,
                                "category": obj.category,
                                "codepack": obj.codepack,
                                "customer": obj.customer,
                                "destination": obj.destination,
                                "format": obj.format,
                                "grower": obj.grower,
                                "label": obj.label,
                                "market": obj.market,
                                "orderProduction": obj.orderProduction,
                                "packing": obj.packing,
                                "priority": obj.priority,
                                "program": obj.program,
                                "size": obj.size,
                                "statusConfirm": obj.statusConfirm,
                                "variety": obj.variety,
                                "via": obj.via,
                                "week": obj.week,
                                "weight": obj.weight,

                            }
                        }
                    );
                    dataResult.splice(0, 0, dataResultHeader)   //-->Agregando cabecera
                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {
                //console.log(dataResult);
            }
        }
    });
}

/**
 * Get details of shipping instruction selected
 * @param {any} xthis
 */
function GetIE(xthis) {
    let tr = $(xthis).closest('tr');
    let _campaignID = $(tr).find("td.tdcampaignID").text();
    let _idIE = $(tr).find("td.tdidIE").text();
    var sUrlApi = "/Comex/GetSalesOrdersRequestLoading?campaignID=" + _campaignID;
    var bRsl = false;
    $.ajax({
        type: "GET",
        url: sUrlApi,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data == null || data.length == 0) {
                sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
            } else {
                var dataJson = JSON.parse(data);
                if (dataJson == null || dataJson.length == 0) {
                    sweet_alert_info('Information', 'There has been a problem when consulting the information. Try again.');
                } else {
                    //console.log(dataJson);
                    let dataResultHeader = {};
                    dataResultHeader["shippingLoadingID"] = "SHIPPINGLOADINGID";
                    dataResultHeader["boxes"] = "BOXES";
                    dataResultHeader["consignee"] = "CONSIGNEE";
                    dataResultHeader["customer"] = "CUSTOMER";
                    dataResultHeader["destination"] = "DESTINATION";
                    dataResultHeader["growerID"] = "GROWERID";
                    dataResultHeader["notify"] = "NOTIFY";
                    dataResultHeader["nroPackingList"] = "NROPACKINGLIST";
                    dataResultHeader["orderProduction"] = "ORDERPRODUCTION";
                    dataResultHeader["orderProductionID"] = "ORDERPRODUCTIONID";
                    dataResultHeader["projectedweekID"] = "PROJECTEDWEEKID";
                    dataResultHeader["status"] = "STATUS";
                    dataResultHeader["statusConfirmID"] = "STATUSCONFIRMID";
                    dataResultHeader["statusID"] = "STATUSID";
                    dataResultHeader["weekDeparture"] = "WEEKDEPARTURE";

                    dataResult = dataJson.filter(item => item.shippingLoadingID == _idIE).map(
                        obj => {
                            return {
                                "shippingLoadingID": obj.shippingLoadingID,
                                "boxes": obj.boxes,
                                "consignee": obj.consignee,
                                "customer": obj.customer,
                                "destination": obj.destination,
                                "growerID": obj.growerID,
                                "notify": obj.notify,
                                "nroPackingList": obj.nroPackingList,
                                "orderProduction": obj.orderProduction,
                                "orderProductionID": obj.orderProductionID,
                                "projectedweekID": obj.projectedweekID,
                                "status": obj.status,
                                "statusConfirmID": obj.statusConfirmID,
                                "statusID": obj.statusID,
                                "weekDeparture": obj.weekDeparture

                            }
                        }
                    );
                    dataResult.splice(0, 0, dataResultHeader)   //-->Agregando cabecera
                    bRsl = true;
                }
            }
        },
        error: function (datoEr) {
            sweet_alert_info('Information', "There was an issue trying to list data.");
        },
        complete: function () {
            if (bRsl) {
                //console.log(dataResult);
            }

        }
    });
}

/**
 * Export an array of data to excel
 * @param {any} xthis
 * @param {any} data
 * @param {any} name
 */
function ExportDataToExcel(xthis, data, process_name, sheet_name) {
    var filename = "data_excel" + "_" + process_name + "_" + GetCurrentDateTimeYYYYMMDDHHMM() + ".xls";
    var objDocExported = $(xthis).excelexportjs({
        containerid: null,
        datatype: 'json',
        dataset: data,
        columns: getColumns(data),
        // returns URI?
        returnUri: false,
        // file name
        worksheetName: sheet_name,
        // encoding
        encoding: "utf-8",
        // language
        locale: 'en-US',
    });
    $(xthis).attr('download', filename + '.xls')
    $(xthis).attr('href', objDocExported);
}

/**
 * Load selector and include customize first value
 * @param {any} data
 * @param {any} control
 * @param {any} firtElement
 */
function loadComboConsignnee(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--To be Confirmed--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
    $('#' + control).selectpicker('refresh');

}