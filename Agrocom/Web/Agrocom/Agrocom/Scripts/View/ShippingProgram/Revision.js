﻿var _campaignID = localStorage.campaignID;
var _userID = $('#lblSessionUserID').text();
var dataStatus = [];
var fileUri = 'https://storageaccountagrocom.file.core.windows.net';
var SAS_TOKEN = "?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-10-05T23:46:26Z&st=2021-10-05T15:46:26Z&spr=https&sig=yZd1HpM86A1N%2ByEBmVZyrmtqovxryJeaZ5TrH8kelYc%3D";
var SharedDirectory = 'agrocomclientdocuments';
var sXprssion = "Secret Password";
var keySize = 256;
var iterations = 100;
var procesado2 = 0;
var procesado3 = 0;
var procesado4 = 0;
var procesado5 = 0;
var procesado6 = 0;
var procesado7 = 0;
var procesado8 = 0;

dataStatus = [
    {
        "statudID": 0,
        "status": "Pending"
    },
    {
        "statudID": 1,
        "status": "Processing"
    },
    {
        "statudID": 2,
        "status": "Aproved"
    },
    {
        "statudID": 3,
        "status": "To correct"
    }
];

$(function () {

    loadData();
    //loadDefaultValues;

    $(document).on("click", ".btnDownLoad", function (e) {
        downLoadFile(this);
    });

    $(document).on("click", ".btnDownLoadCustomer", function (e) {
        downLoadFileCustomer(this);
    });

    $(document).on("click", ".btnDownLoad2", function (e) {
        downLoadFile2(this);
    });

    $(document).on("click", ".btnDownLoad3", function (e) {
        downLoadFile3(this);
    });

    $(document).on("click", ".btnDeleteCustomer", function (e) {
        dropFile2(this)
    });

    $(document).on("click", ".btnAttach", function (e) {
        saveFile(this)
    });

    $(document).on("click", ".btnDelete", function (e) {
        dropFile(this)
    });

    $(document).on("click", ".btnDeleteCustomer", function (e) {
        dropFileCustomer(this)
    });

    $(document).on("click", "#btnConfirm", function (e) {
        PreGuardarOpenToClient();
    });

    $(document).on("click", "#btnSaveInvoiceGrower", function (e) {
        PreGuardarLoadInvoiceGrower(this);
    });

    $(document).on("click", "#btnSaveInvoiceOZM", function (e) {
        PreGuardarLoadInvoiceOBM(this);
    });

    $(document).on("click", "#btnSavePL", function (e) {
        PreGuardarLoadPackingList(this);
    });

    $(document).on("click", "#btnSavePhyto", function (e) {
        PreGuardarLoadPhyto(this);
    });

    $(document).on("click", "#btnSaveBL", function (e) {
        PreGuardarLoadBL(this);
    });

    $(document).on("click", "#btnSaveCertOrig", function (e) {
        PreGuardarLoadCertificateOfOrigin(this);
    });

    $(document).on("click", "#btnSaveExportFile", function (e) {
        PreGuardarLoadExportFile(this);
    });

    $(document).on("click", "#btnSaveOthers", function (e) {
        PreGuardarLoadOthers(this);
    });

    function downLoadFile(xthis) {
        var fileName = $(xthis).closest('div.btn-group').find('.lblFile').text();
        console.log(fileName)
        getLink(fileName);
    }

    function downLoadFile2(xthis) {
        var label = $(xthis).closest('div.btn-group').find('.lblFile2').text();
        getLink(label);
    }

    function downLoadFile3(xthis) {
        var label = $(xthis).closest('div.btn-group').find('.lblFile3').text();
        getLink(label);
    }

    function downLoadFileCustomer(xthis) {
        var label = $(xthis).closest('div.btn-group').find('.lblFileCustomer').text();
        getLink(label);
    }

    $("select#cmbStatusRevision").change(function () {

        if (parseInt($('#cmbStatusRevision').val()) == 1) {
            //doc 1 desacticado
            $('#txtInvoiceGrowerFile').prop('disabled', true);
            $('#txtInvoiceGrower').prop('readonly', true);
            $('#txtInvoiceGrowerNotesOZM1').prop('readonly', true);
            $('#btnSaveInvoiceGrower').prop('disabled', true);
            $('#txtfileCustomer1').prop('disabled', true);
            $('#txtInvoiceGrowerNotesCustomer').prop('readonly', true);

            //doc 2 desact
            $('#cmbInvoiceOZMStatus').prop('disabled', true);
            $('#btnSaveInvoiceOZM').prop('disabled', true);
            $('#txtfileCustomer2').prop('disabled', true);
            $('#txtInvoiceOZMNotesCustomer').prop('readonly', true);

            //doc 3 desact
            $('#cmbPLStatus').prop('disabled', true);
            $('#btnSavePL').prop('disabled', true);
            $('#txtfileCustomer3').prop('disabled', true);
            $('#txtPLNotesCustomer').prop('readonly', true);

            //doc 4 desact
            $('#cmbPhytoStatus').prop('disabled', true);
            $('#btnSavePhyto').prop('disabled', true);
            $('#txtfileCustomer4').prop('disabled', true);
            $('#txtPhytoNotesCustomer').prop('readonly', true);

            //doc 5 desact
            $('#cmbBLStatus').prop('disabled', true);
            $('#btnSaveBL').prop('disabled', true);
            $('#txtfileCustomer5').prop('disabled', true);
            $('#txtBLNotesCustomer').prop('readonly', true);

            //doc 6 desact
            $('#cmbCertOrigStatus').prop('disabled', true);
            $('#btnSaveCertOrig').prop('disabled', true);
            $('#txtfileCustomer6').prop('disabled', true);
            $('#txtCertOrigNotesCustomer').prop('readonly', true);

            //doc 7 desact
            $('#cmbExportFileStatus').prop('disabled', true);
            $('#btnSaveExportFile').prop('disabled', true);
            $('#txtfileCustomer7').prop('disabled', true);
            $('#txtExportFileNotesCustomer').prop('readonly', true);
            $('#enabled7').prop('disabled', true);

            //doc 8 desact
            $('#cmbOthersStatus').prop('disabled', true);
            $('#btnSaveOthers').prop('disabled', true);
            $('#txtfileCustomer8').prop('disabled', true);
            $('#txtOthersNotesCustomer').prop('readonly', true);
            $('#enabled8').prop('disabled', true);

            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', false);

        } else {

            //doc 1 desacticado
            $('#txtInvoiceGrowerFile').prop('disabled', false);
            $('#txtInvoiceGrower').prop('readonly', false);
            $('#txtInvoiceGrowerNotesOZM1').prop('readonly', false);
            $('#btnSaveInvoiceGrower').prop('disabled', false);
            $('#txtfileCustomer1').prop('disabled', false);
            $('#txtInvoiceGrowerNotesCustomer').prop('readonly', false);

            //doc 2 desactivado
            $('#cmbInvoiceOZMStatus').prop('disabled', false);
            $('#btnSaveInvoiceOZM').prop('disabled', false);
            $('#txtfileCustomer2').prop('disabled', false);
            $('#txtInvoiceOZMNotesCustomer').prop('readonly', false);

            //doc 3 desact
            $('#cmbPLStatus').prop('disabled', false);
            $('#btnSavePL').prop('disabled', false);
            $('#txtfileCustomer3').prop('disabled', false);
            $('#txtPLNotesCustomer').prop('readonly', false);

            //doc 4 desact
            $('#cmbPhytoStatus').prop('disabled', false);
            $('#btnSavePhyto').prop('disabled', false);
            $('#txtfileCustomer4').prop('disabled', false);
            $('#txtPhytoNotesCustomer').prop('readonly', false);

            //doc 5 desact
            $('#cmbBLStatus').prop('disabled', false);
            $('#btnSaveBL').prop('disabled', false);
            $('#txtfileCustomer5').prop('disabled', false);
            $('#txtBLNotesCustomer').prop('readonly', false);

            //doc 6 desact
            $('#cmbCertOrigStatus').prop('disabled', false);
            $('#btnSaveCertOrig').prop('disabled', false);
            $('#txtfileCustomer6').prop('disabled', false);
            $('#txtCertOrigNotesCustomer').prop('readonly', false);

            //doc 7 desact
            $('#cmbExportFileStatus').prop('disabled', false);
            $('#btnSaveExportFile').prop('disabled', false);
            $('#txtfileCustomer7').prop('disabled', false);
            $('#txtExportFileNotesCustomer').prop('readonly', false);
            $('#enabled7').prop('disabled', false);

            //dc 8 desact
            $('#cmbOthersStatus').prop('disabled', false);
            $('#btnSaveOthers').prop('disabled', false);
            $('#txtfileCustomer8').prop('disabled', false);
            $('#txtOthersNotesCustomer').prop('readonly', false);
            $('#enabled8').prop('disabled', false);

            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', false);
        }

        console.log($(this).children("option:selected").val())
        color = setColorByStatus1($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbStatusRevision').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbStatusRevision').css("background-color", color.backgroundColor);
    })

    $("select#cmbInvoiceGrowerStatus").change(function () {

        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbInvoiceGrowerStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbInvoiceGrowerStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbInvoiceOZMStatus").change(function () {

        //if (parseInt($('#cmbInvoiceOZMStatus').val()) == 3) {
        //  $('#txtInvoiceOZMFile').prop('disabled', true);
        //  $('#txtInvoiceOZM').prop('readonly', true);
        //  $('#txtInvoiceOZMNotesOZM2').prop('readonly', true);
        //  $('#btnSaveInvoiceOZM').prop('disabled', true);
        //  $('#txtfileCustomer2').prop('disabled', true);
        //  $('#txtInvoiceOZMNotesCustomer').prop('readonly', true);

        //  $(this).closest('div.form-group').find('.btnDelete').prop('disabled', true);
        //  $(this).closest('div.form-group').find('.btnDownLoad').prop('disabled', true);
        //  $(this).closest('div.form-group').find('.inputFile').prop('disabled', true);
        //  $(this).closest('div.form-group').find('.btnDeleteCustomer').prop('disabled', true);
        //  $(this).closest('div.form-group').find('.btnDownLoadCustomer').prop('disabled', true);
        //}
        if ($("select#cmbInvoiceOZMStatus option:selected").val() == 0) {
            $('#cmbInvoiceOZMStatus').prop('disabled', true);
            $('#btnSaveInvoiceOZM').prop('disabled', true);
            $('#txtfileCustomer2').prop('disabled', true);
            $('#txtInvoiceOZMNotesCustomer').prop('readonly', true);
            $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
        }
        if ($("select#cmbInvoiceOZMStatus").val() == 1) {
            procesado2 = 1;
        }

        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbInvoiceOZMStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbInvoiceOZMStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbPLStatus").change(function () {

        if (parseInt($('#cmbPLStatus').val()) == 0) {

            $('#cmbPLStatus').prop('disabled', true);
            $('#btnSavePL').prop('disabled', true);
            $('#txtfileCustomer3').prop('disabled', true);
            $('#txtPLNotesCustomer').prop('readonly', true);

            $(this).closest('div.form-group').find('.btnDelete').prop('disabled', true);
            $(this).closest('div.form-group').find('.btnDownLoad').prop('disabled', true);
            $(this).closest('div.form-group').find('.inputFile').prop('disabled', true);
            $(this).closest('div.form-group').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.form-group').find('.btnDownLoadCustomer').prop('disabled', true);
        }
        console.log($(this).children("option:selected").val())
        if ($("select#cmbPLStatus").val() == 1) {
            procesado3 = 1;
        }

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbPLStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbPLStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbPhytoStatus").change(function () {

        if (parseInt($('#cmbPhytoStatus').val()) == 0) {

            $('#cmbPhytoStatus').prop('disabled', true);
            $('#btnSavePhyto').prop('disabled', true);
            $('#txtfileCustomer4').prop('disabled', true);
            $('#txtPhytoNotesCustomer').prop('readonly', true);

            $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
        }
        if ($("select#cmbPhytoStatus").val() == 1) {
            procesado4 = 1;
        }
        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbPhytoStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbPhytoStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbBLStatus").change(function () {

        if (parseInt($('#cmbBLStatus').val()) == 0) {

            $('#cmbBLStatus').prop('disabled', true);
            $('#btnSaveBL').prop('disabled', true);
            $('#txtfileCustomer5').prop('disabled', true);
            $('#txtBLNotesCustomer').prop('readonly', true);

            $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
        }

        if ($("select#cmbBLStatus").val() == 1) {
            procesado5 = 1;
        }

        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbBLStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbBLStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbCertOrigStatus").change(function () {

        if (parseInt($('#cmbCertOrigStatus').val()) == 0) {

            $('#cmbCertOrigStatus').prop('disabled', true);
            $('#btnSaveCertOrig').prop('disabled', true);
            $('#txtfileCustomer6').prop('disabled', true);
            $('#txtCertOrigNotesCustomer').prop('readonly', true);

            $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
        }

        if ($("select#cmbCertOrigStatus").val() == 1) {
            procesado6 = 1;
        }
        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbCertOrigStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbCertOrigStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbExportFileStatus").change(function () {
        if (parseInt($('#cmbExportFileStatus').val()) == 0) {

            $('#cmbExportFileStatus').prop('disabled', true);
            $('#btnSaveExportFile').prop('disabled', true);
            $('#txtfileCustomer7').prop('disabled', true);
            $('#txtExportFileNotesCustomer').prop('readonly', true);
            $('#enabled7').prop('disabled', true);

            $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
        }

        if ($("select#cmbExportFileStatus").val() == 1) {
            procesado7 = 1;
        }

        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbExportFileStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbExportFileStatus').css("background-color", color.backgroundColor);
    })

    $("select#cmbOthersStatus").change(function () {

        if (parseInt($('select#cmbOthersStatus').val()) == 0) {

            $('#cmbOthersStatus').prop('disabled', true);
            $('#btnSaveOthers').prop('disabled', true);
            $('#txtfileCustomer8').prop('disabled', true);
            $('#txtOthersNotesCustomer').prop('readonly', true);
            $('#enabled8').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', true);
        } else {
            $('#cmbOthersStatus').prop('disabled', false);
            $('#btnSaveOthers').prop('disabled', false);
            $('#txtfileCustomer8').prop('disabled', false);
            $('#txtOthersNotesCustomer').prop('readonly', false);
            $('#enabled8').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
        }

        if ($("select#cmbOthersStatus").val() == 1) {
            procesado8 = 1;
            $('#btnSaveOthers').prop('disabled', true);
            $('#txtfileCustomer8').prop('disabled', true);
            $('#txtOthersNotesCustomer').prop('readonly', true);
            $('#enabled8').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', true);
        }

        console.log($(this).children("option:selected").val())

        color = setColorByStatus($(this).children("option:selected").val());
        $(this).closest('div.form-group').find('#cmbOthersStatus').css("color", color.fontColor);
        $(this).closest('div.form-group').find('#cmbOthersStatus').css("background-color", color.backgroundColor);
    })

    $(document).on("change", "input#enabled7", function (e) {

        if ($(this).is(":checked") == true) {

            //$('#txtExportFileFile').prop('disabled', false);
            $('#txtExportFileNotesOZM').prop('readonly', false);
            $('#txtfileCustomer7').prop('disabled', false);
            $('#btnSaveExportFile').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', false);

        } else {

            $('#enabled7Group').prop('hidden', true);
            $('#enabled7Group').hide();
            //$('#txtExportFileFile').prop('disabled', true);
            $('#txtExportFileNotesOZM').prop('readonly', true);
            $('#txtfileCustomer7').prop('disabled', true);
            $('#btnSaveExportFile').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', true);
        }
    });

    $(document).on("change", "input#enabled8", function (e) {
        if ($(this).is(":checked") == true) {

            //$('#txtOthersFile').prop('disabled', false);
            //$('#txtOthersFile2').prop('disabled', false);
            //$('#txtOthersFile3').prop('disabled', false);
            $('#txtOthersNotesOZM').prop('readonly', false);
            $('#txtfileCustomer8').prop('readonly', false);
            $('#btnSaveOthers').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDownLoad2').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.inputFile2').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDelete3').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.btnDownLoad3').prop('disabled', false);
            //$(this).closest('div.container-fluid').find('.inputFile3').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', false);
        } else {
            $('#enabled8Group').prop('hidden', true);
            $('#enabled8Group').hide();
            //$('#txtOthersFile').prop('disabled', true);
            //$('#txtOthersFile2').prop('disabled', true);
            //$('#txtOthersFile3').prop('disabled', true);
            //$('#txtOthersNotesOZM').prop('readonly', true);
            $('#txtfileCustomer8').prop('readonly', true);
            $('#btnSaveOthers').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad2').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile2').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDelete3').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.btnDownLoad3').prop('disabled', true);
            //$(this).closest('div.container-fluid').find('.inputFile3').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            $(this).closest('div.container-fluid').find('.inputFileCustomer').prop('disabled', true);

        }
    });

    if ($(enabled8).is(":checked") == false) {
        $('#enabled8Group').prop('hidden', true);
        $('#enabled8Group').hide();
    }
    if ($(enabled7).is(":checked") == false) {
        $('#enabled7Group').prop('hidden', true);
        $('#enabled7Group').hide();
    }

})

function openModal(orderProductionID) {
    $('table#tblDetails>tbody').empty();

    loadDefaultValuesSelect();
    $(myModal).show();

    getPOById(orderProductionID)

    //$(".btnDeleteCustomer").hide()
    //$(".lblFileCustomer").show()
    //$(".btnDownLoadCustomer").show();
    //$(".inputFileCustomer").hide();

    $("#txtOrderProductID").val(orderProductionID);
    //loadDetailsDocuments(orderProductionID);

    //  var allDocuments = [];
    //  OpenToClient(orderProductionID).then(function () {

    //  })

    //loadInvoiceGrower(orderProductionID);
    //loadInvoiceOBM(orderProductionID);
    //loadPackingList(orderProductionID);
    //loadPhyto(orderProductionID);
    //loadBL(orderProductionID);
    //loadCertificateOfOrigin(orderProductionID);
    //loadExportFile(orderProductionID);
    //loadOthers(orderProductionID);

}

function dropFile(xthis) {
    $(xthis).parent('div').find('.btnDelete').hide()
    $(xthis).parent('div').find('.lblFile').hide()
    $(xthis).parent('div').find('.btnDownLoad').hide()

    $(xthis).parent('div').find('.inputFile').show()
    //$(xthis).parent('div').find('.btnAttach ').show()

    $(xthis).parent('div').find('.lblFile').text('')
    $(xthis).parent('div').find('.inputFile').val('')

    $(xthis).closest('div').find('.cmbStatus').val("0").change() //pending
    //$(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

}

function dropFileCustomer(xthis) {
    $(xthis).parent('div').find('.btnDeleteCustomer').hide()
    $(xthis).parent('div').find('.lblFileCustomer').hide()
    $(xthis).parent('div').find('.btnDownLoadCustomer').hide()

    $(xthis).parent('div').find('.inputFileCustomer').show()
    //$(xthis).parent('div').find('.btnAttach ').show()

    $(xthis).parent('div').find('.lblFileCustomer').text('')
    $(xthis).parent('div').find('.inputFileCustomer').val('')

    //$(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

}


function dropFile2(xthis) {
    $(xthis).parent('div').find('.btnDeleteCustomer').hide();
    $(xthis).parent('div').find('.lblFileCustomer').hide();
    $(xthis).parent('div').find('.btnDownLoadCustomer').hide()

    $(xthis).parent('div').find('.inputFile').show();
    $(xthis).parent('div').find('.lblFileCustomer').text('');
    $(xthis).parent('div').find('.inputFile').val('');

}

function saveFile(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

    //$(xthis).closest('div.form-group').find('.inputFileCustomer').val('123232');
    //$(xthis).closest('div.form-group').find('.lblFileCustomer').text()
    if ($(xthis).closest('div.container-fluid').find('.lblFileCustomer').text().length >= 1 || $(xthis).closest('div.container-fluid').parent().find('.inputFileCustomer').val().length > 2) {
        //$(xthis).closest('div.form-group').parent().find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.container-fluid').find('.btnDeleteCustomer').show()
        $(xthis).closest('div.container-fluid').find('.lblFileCustomer').show()
        $(xthis).closest('div.container-fluid').find('.btnDownLoadCustomer').show()

        $(xthis).closest('div.container-fluid').find('.inputFileCustomer').hide()
    }
    else {
        //$(xthis).closest('div.container-fluid').parent().find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.container-fluid').find('.btnDeleteCustomer').hide()
        $(xthis).closest('div.container-fluid').find('.lblFileCustomer').hide()
        $(xthis).closest('div.container-fluid').find('.btnDownLoadCustomer').hide()

        $(xthis).closest('div.container-fluid').find('.inputFileCustomer').show()
    }
}

function getPOById(orderProductionID) {
    //var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
    var api = getPath() + HOST_APIAGROCOM_DOCUMENTS + ENDPOINT_GET_SALE_ORDER_DETAIL_DOCUMENTS + '?orderProductionID=' + encryptdata(orderProductionID);
    $.ajax({
        type: "GET",
        url: api,
        headers: {
            'Authorization': 'Bearer ' + localStorage.Token,
        },
        async: false,
        success: function (data) {
            if (data.length > 0) {
                console.log(decryptdata(data));
                var o = JSON.parse(decryptdata(data));
                _orderproductionID = o.orderProductionID
                $(txtProductionOrder).val(o.orderProduction);
                $(txtCustomer).val(o.customer);
                //$(txtPoWeek).val(o.productionWeek);
                //$(txtStatus).val(o.status);
                $(txtOriginPort).val(o.originPort);
                $(txtOrigin).val(o.origin);
                $(txtVia).val(o.via);

                //$(txtBooking).val(o.booking);
                //$(txtGrossWeight).val(o.grossWeight);
                $(txtEta).val(o.eta);
                $(txtIncoterm).val(o.incoterm);
                $(txtPortDestination).val(o.destinationPort);

                $(txtShippingLine).val(o.shippingLine);
                //$(txtWeekEtd).val(o.weekEtd);
                //$(txtWeekReal).val(o.weekEtaReal);
                //$(txtLoadingDate).val(o.loadingDate);
                $(txtSaleTerm).val(o.saleTerm);

                $(txtVessel).val(o.vessel);
                $(txtEtd).val(o.etd);
                //$(txtEtaReal).val(o.etaReal);

                $(txtGrower).val(o.grower);
                //$(txtMarket).val(o.market);
                $(txtBl).val(o.bl);
                //$(txtMarket).val(o.market);

                $(txtContainer).val(o.container);
                $(txtResponsable).val(o.responsable);
                $(txtIE).val(o.packingList);
                //$(txtWeekEta).val(o.weekEta);

                var grossWeight = 0;
                var content = "";
                $.each(o.details, function (index, row) {
                    grossWeight = row.grossWeight;
                    content += "    <tr style='font-size: 14px'>";
                    content += "      <td id='brand'>" + row.brand + "</td>";
                    content += "      <td id='variety' >" + row.variety + " </td>";
                    content += "      <td id='codepack'>" + row.codepack + "</td>";
                    content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
                    content += "      <td id='boxes'>" + row.boxes + "</td>";
                    content += "      <td id='pallets'>" + row.pallets + "</td>";
                    content += "      <td id='netWeight'>" + row.netWeight + "</td>";
                    content += "    </tr>";
                });
                $("#tblDetails>tbody").empty().append(content);
                //Calcular el footer
                var boxes = 0;
                var pallets = 0;
                var netWeight = 0;
                $('#tblDetails>tbody>tr').each(function (j, row) {
                    boxes = boxes + parseFloat($(row).find('#boxes').text());
                    pallets = pallets + parseFloat($(row).find('#pallets').text());
                    netWeight = netWeight + parseFloat($(row).find('#netWeight').text());
                });
                //Insertar los valores del footer
                contentfoot = "";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
                contentfoot += '<td>' + (boxes).toFixed(0) + '</td>';
                contentfoot += '<td>' + (pallets).toFixed(2) + '</td>';
                contentfoot += '<td>' + (netWeight).toFixed(2) + '</td>';
                contentfoot += "</tr>";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Gross Weight";
                contentfoot += '<td>' + grossWeight + '</td>';
                contentfoot += '<td></td>';
                contentfoot += '<td></td>';
                contentfoot += "</tr>";
                $("#tblDetails>tfoot").empty().append(contentfoot);

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            //$.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },
        complete: loadAllDocuments(orderProductionID)
    });

}

function loadDetailsDocuments(orderProductionID) {

    var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var o = JSON.parse(data);
                //Creando el formato de la tabla
                var content = "";
                //Recorriendo la lista para llenar el detalle de la tabla
                var grossWeight = 0;
                $.each(o.details, function (index, row) {
                    grossWeight = row.grossWeight;
                    content += "    <tr style='font-size: 14px'>";
                    content += "      <td id='brand'>" + row.brand + "</td>";
                    content += "      <td id='variety' >" + row.variety + " </td>";
                    content += "      <td id='codepack'>" + row.codepack + "</td>";
                    content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
                    content += "      <td id='boxes'>" + row.boxes + "</td>";
                    content += "      <td id='pallets'>" + row.pallets + "</td>";
                    content += "      <td id='netWeight'>" + row.netWeight + "</td>";
                    content += "    </tr>";
                });

                $("#tblDetails>tbody").empty().append(content);
                //Calcular el footer
                var boxes = 0;
                var pallets = 0;
                var netWeight = 0;
                $('#tblDetails>tbody>tr').each(function (j, row) {
                    boxes = boxes + parseFloat($(row).find('#boxes').text());
                    pallets = pallets + parseFloat($(row).find('#pallets').text());
                    netWeight = netWeight + parseFloat($(row).find('#netWeight').text());
                });
                //Insertar los valores del footer
                contentfoot = "";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
                contentfoot += '<td>' + (boxes).toFixed(0) + '</td>';
                contentfoot += '<td>' + (pallets).toFixed(2) + '</td>';
                contentfoot += '<td>' + (netWeight).toFixed(2) + '</td>';
                contentfoot += "</tr>";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Gross Weight";
                contentfoot += '<td>' + grossWeight + '</td>';
                contentfoot += '<td></td>';
                contentfoot += '<td></td>';
                contentfoot += "</tr>";
                $("#tblDetails>tfoot").empty().append(contentfoot);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });

}

function loadAllDocuments(orderProductionID) {
    var alldocuments = []
    var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                alldocuments = JSON.parse(data);
                console.log(alldocuments);
                //loadInvoiceGrower(alldocuments);
                loadInvoiceOBM(alldocuments);
                loadPackingList(alldocuments);
                loadPhyto(alldocuments);
                loadBL(alldocuments);
                loadCertificateOfOrigin(alldocuments);
                loadExportFile(alldocuments);
                loadOthers(alldocuments);

                $('select.selectDocumentType').each(function (i, e) {
                    if ($(e).children('option:selected').val() == '0')//pending
                    {
                        //nada
                    }
                    if ($(e).children('option:selected').val() == '1')//proce
                    {
                        //nada
                        $(e)
                            .find('option')
                            .remove()
                            .end()
                            .append('<option style="background-color:#FBFF00;color:#000000" value="1" selected disabled>Processing</option>' +
                                '<option style="background-color:#09AB01;color:#FFFFFF" value="2">Aproved</option>' +
                                '<option style="background-color:#FF0000;color:#FFFFFF" value="3">To correct</option>')
                            .val('1')
                    }
                    if ($(e).children('option:selected').val() == '2')//appro
                    {
                        //nada
                        $(e)
                            .find('option')
                            .remove()
                            .end()
                            .append(
                                '<option style="background-color:#09AB01;color:#FFFFFF" value="2" selected>Aproved</option>' +
                                '<option style="background-color:#FF0000;color:#FFFFFF" value="3">To correct</option>')
                            .val('2')
                    }
                    if ($(e).children('option:selected').val() == '3')//to correct
                    {
                        //nada
                        $(e)
                            .find('option')
                            .remove()
                            .end()
                            .append(
                                '<option style="background-color:#09AB01;color:#FFFFFF" value="2">Aproved</option>' +
                                '<option style="background-color:#FF0000;color:#FFFFFF" value="3" selected>To correct</option>')
                            .val('3')
                    }
                })

                if ($("select#cmbInvoiceOZMStatus option:selected").val() == 1) {
                    procesado2 = 1;

                }


                if ($("select#cmbPLStatus option:selected").val() == 1) {
                    procesado3 = 1;

                }
                if ($("select#cmbPhytoStatus option:selected").val() == 1) {
                    procesado4 = 1;

                }
                if ($("select#cmbBLStatus option:selected").val() == 1) {
                    procesado5 = 1;

                }
                if ($("select#cmbCertOrigStatus option:selected").val() == 1) {
                    procesado6 = 1;

                }
                if ($("select#cmbExportFileStatus option:selected").val() == 1) {
                    procesado7 = 1;

                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },
        complete: function () {
            return alldocuments;
        }
    });

}

function OpenToClient(orderProductionID) {
    var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var o = JSON.parse(data);
                $("#cmbOpenToClient").val(o.open);
                $("#txtcommentaryDocumentsOBM").val(o.commentaryDocumentsOBM);
                $("#txtcommentaryDocumentsCustomer").val(o.commentaryDocumentsCustomer);
                $("#cmbStatusRevision").val(o.statusRevision).change();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });
}


function setColorByStatus1(statusID) {
    var color = { backgroundColor: "a", fontColor: "b" }
    var o = {}
    switch (parseInt(statusID)) {

        case 0://incomplete
            o.backgroundColor = '#FBFF00'; //amarillo
            o.fontColor = '#000000'; //negro
            break;
        case 1: //complete
            o.backgroundColor = '#09AB01'; //blanco
            o.fontColor = '#FFFFFF'; //negro
            break;

    }
    return o;
}


function setColorByStatus(statusID) {
    var color = { backgroundColor: "a", fontColor: "b" }
    var o = {}
    switch (parseInt(statusID)) {
        case 0: //pending
            o.backgroundColor = '#FFDB33'; //blanco
            o.fontColor = '#000000'; //negro
            break;
        case 1://processing
            o.backgroundColor = '#335AFF'; //amarillo
            o.fontColor = '#FFFFFF'; //negro
            break;
        case 2://appoved
            o.backgroundColor = '#09AB01' //verde
            o.fontColor = '#FFFFFF'; //blanco
            break;
        case 3://rejected
            o.backgroundColor = '#FF5433' //rojo
            o.fontColor = '#FFFFFF'; //blanco
            break;
    }
    return o;
}

function loadData() {
    
    //var api = "/ShippingProgram/GetAllPOForCustomer?campaignID=" + _campaignID;
    var api = getPath() + HOST_APIAGROCOM_DOCUMENTS + ENDPOINT_GET_SALE_ORDER_DOCUMENTS + '?campaignID=' + encryptdata(_campaignID) + "&userID=" + encryptdata(_userID);
    console.log(api);
    $.ajax({
        type: "GET",
        url: api,
        headers: {
            'Authorization': 'Bearer ' + localStorage.Token,
        },
        async: false,
        success: function (data) {
            if (data.length > 0) {
                console.log(decryptdata(data));
                var list = JSON.parse(decryptdata(data));
                $("table#tableList>tbody").empty();
                $.each(list, function (i, item) {
                    var tr = ''
                    tr += '<tr  style="vertical-align:middle; font-size:13px">'
                    tr += '<td style="display:none">' + item.orderProductionID + '</td>'
                    tr += '<td class="sticky"><button class="btn btn-primary btn-sm" onclick="openModal(' + item.orderProductionID + ')" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit fa-sm"></i></button> </td>'
                    tr += '<td class="sticky1" style="min-width:125px;max-width:125px;">' + item.OrderProduction + '</td>'
                    tr += '<td class="sticky2" style="min-width:120px;max-width:120px;">' + item.container + '</td>'
                    tr += '<td class="sticky3" style="min-width:200px;max-width:200px;">' + item.customer + '</td>'
                    tr += '<td style="min-width:130px;max-width:150px;">' + item.PackingList + '</td>'
                    tr += '<td style="min-width:120px;max-width:150px;">' + item.shippingLine + '</td>'
                    tr += '<td style="min-width:100px;max-width:120px;">' + item.destinationPort + '</td>'
                    tr += '<td style="min-width:35px;max-width:35px;">' + item.Via + '</td>'
                    tr += '<td style="min-width:80px;max-width:80px;">' + item.ETD + '</td>'
                    tr += '<td style="min-width:80px;max-width:80px;">' + item.ETA + '</td>'
                    tr += '<td style="min-width:100px;max-width:120px;">' + item.responsable + '</td>'
                    var color = {}
                    color = setColorByStatus(item.invoiceCustomerStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:90px;max-width:90px;">' + item.invoiceCustomerStatus + '</td>'
                    color = setColorByStatus(item.packingListStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:90px;max-width:90px;">' + item.packingListStatus + '</td>'
                    color = setColorByStatus(item.phytoStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:85px;max-width:85px;">' + item.phytoStatus + '</td>'
                    color = setColorByStatus(item.blStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:85px;max-width:85px;">' + item.blStatus + '</td>'
                    color = setColorByStatus(item.certificateOriginStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:90px;max-width:90px;">' + item.certificateOriginStatus + '</td>'
                    color = setColorByStatus(item.exportFileStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:85px;max-width:85px;">' + item.exportFileStatus + '</td>'
                    color = setColorByStatus(item.othersStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:85px;max-width:85px;">' + item.othersStatus + '</td>'
                    tr += '</tr>'
                    $("table#tableList>tbody").append(tr);
                });

                $(tableList).dataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                        'pageLength'
                    ],
                    orderCellsTop: true,
                    fixedHeader: true,
                    ordering: false,
                    searching: true,
                    lengthMenu: [20, 50, 100]
                    //responsive: true,          
                    //destroy: true
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error('Information', 'There has been a problem when consulting the information. Try again.');
            //console.log(api)
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });
    Swal.close();
}

function loadDefaultValuesSelect() {

    $('#cmbStatusRevision').val(0).change();

    var select = $('#cmbInvoiceGrowerStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbInvoiceGrowerStatus').val(0).change();

    var select = $('#cmbInvoiceOZMStatus');
    if (procesado2 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);

    var select = $('#cmbPLStatus');

    if (procesado3 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);

    var select = $('#cmbPhytoStatus');

    if (procesado4 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);

    var select = $('#cmbBLStatus');

    if (procesado5 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);

    var select = $('#cmbCertOrigStatus');

    if (procesado6 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);

    var select = $('#cmbExportFileStatus');

    if (procesado7 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);

    var select = $('#cmbOthersStatus');

    if (procesado8 == 1) {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })

    } else {
        var content = ""; $.each(dataStatus, function (i, e) {
            if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
            if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        })
    }

    select.empty().append(content);
    $('#cmbOthersStatus').val(0).change();

}

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='0'>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}


function loadInvoiceGrower(documents) {

    docInvoiceGrower = documents.filter(item => item.typeDocumentID == 1)[0]

    $("#typeDocumentId").val(docInvoiceGrower.typeDocumentID);
    $("#txtenabled").val(docInvoiceGrower.enabled);

    $(lblInvoiceGrowerFile).text(docInvoiceGrower.file)
    $(txtInvoiceGrowerFile).text("");
    $("#txtInvoiceGrower").val(docInvoiceGrower.number);
    $("#txtInvoiceGrowerNotesOZM1").val(docInvoiceGrower.notesOBM);

    $("#cmbInvoiceGrowerStatus").val(docInvoiceGrower.statusID).change();
    $("#lblfileCustomer1").text(docInvoiceGrower.fileCustomer);
    $("#txtInvoiceGrowerNotesCustomer").val(docInvoiceGrower.notesCustomer);

    if (docInvoiceGrower.file.length > 2) {
        HideUpload($(lblInvoiceGrowerFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblInvoiceGrowerFile).closest('div.container-fluid'));
    }

    if (docInvoiceGrower.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer1").closest('div.container-fluid').parent());
    } else {
        ShowUpload2($("#lblfileCustomer1").closest('div.container-fluid').parent());
    }

}

function HideUpload(xthis) {
    $(xthis).find('.btnDelete').show();
    $(xthis).find('.lblFile').show();
    $(xthis).find('.btnDownLoad').show();

    $(xthis).find('.inputFile').hide();

    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}

function ShowUpload(xthis) {
    $(xthis).find('.btnDelete').hide();
    $(xthis).find('.lblFile').hide();
    $(xthis).find('.btnDownLoad').hide();

    $(xthis).find('.inputFile').show();
}

function HideUpload2(xthis) {
    $(xthis).find('.btnDeleteCustomer').show();
    $(xthis).find('.lblFileCustomer').show();
    $(xthis).find('.btnDownLoadCustomer').show();

    $(xthis).find('.inputFileCustomer').hide();

    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}

function ShowUpload2(xthis) {
    $(xthis).find('.btnDeleteCustomer').hide();
    $(xthis).find('.lblFileCustomer').hide();
    $(xthis).find('.btnDownLoadCustomer').hide();

    $(xthis).find('.inputFileCustomer').show();
}


function loadInvoiceOBM(documents) {

    docInvoiceOBM = documents.filter(item => item.typeDocumentID == 2)[0]

    $("#typeDocumentId2").val(docInvoiceOBM.typeDocumentID);
    $("#txtenabled2").val(docInvoiceOBM.enabled);

    $("#lblInvoiceOZMFile").text(docInvoiceOBM.file);
    $("#txtInvoiceOZMFile").val("");
    $("#txtInvoiceOZM").val(docInvoiceOBM.number);
    $("#txtInvoiceOZMNotesOZM2").val(docInvoiceOBM.notesOBM);

    $("#cmbInvoiceOZMStatus").val(docInvoiceOBM.statusID).change();
    $("#lblfileCustomer2").text(docInvoiceOBM.fileCustomer);
    $("#txtInvoiceOZMNotesCustomer").val(docInvoiceOBM.notesCustomer);

    if (docInvoiceOBM.file.length > 2) {
        HideUpload($("#lblInvoiceOZMFile").closest('div.container-fluid'));
    } else {
        ShowUpload($("#lblInvoiceOZMFile").closest('div.container-fluid'));
    }

    if (docInvoiceOBM.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer2").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer2").closest('div.container-fluid'));
    }
}

function loadPackingList(documents) {

    docPackingList = documents.filter(item => item.typeDocumentID == 3)[0]

    $("#typeDocumentId3").val(docPackingList.typeDocumentID);
    $("#txtenabled3").val(docPackingList.enabled);
    $("#txtPL").val(docPackingList.number);

    $("#lblPLFile").text(docPackingList.file);
    $("#lblPLFile2").text(docPackingList.file2);
    $("#txtPLFile").val('');
    $("#txtPLFile2").val('');
    $("#txtPLNotesOZM").val(docPackingList.notesOBM);

    $("#cmbPLStatus").val(docPackingList.statusID).change();
    $("#txtPLNotesCustomer").val(docPackingList.notesCustomer);
    $("#lblfileCustomer3").text(docPackingList.fileCustomer);

    if (docPackingList.file.length > 2) {
        HideUpload($("#lblPLFile").closest('div.container-fluid'));
    } else {
        ShowUpload($("#lblPLFile").closest('div.container-fluid'));
    }

    if (docPackingList.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer3").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer3").closest('div.container-fluid'));
    }
}

function loadPhyto(documents) {

    docPhyto = documents.filter(item => item.typeDocumentID == 4)[0]

    $("#typeDocumentId4").val(docPhyto.typeDocumentID);
    $("#txtenabled4").val(docPhyto.enabled);
    $("#txtPhyto").val(docPhyto.number);

    $(lblPhytoFile).text(docPhyto.file);
    $(txtPhytoFile).val('');
    $("#txtPhytoNotesOZM").val(docPhyto.notesOBM);

    $("#cmbPhytoStatus").val(docPhyto.statusID).change();
    $("#lblfileCustomer4").text(docPhyto.fileCustomer);
    $("#txtPhytoNotesCustomer").val(docPhyto.notesCustomer);
    //$("#txtPhytoFile").val(docPhyto.file);

    //$("#txtPhytoFile2").val(docPhyto.file2);
    //$("#txtPhytoFile3").val(docPhyto.file3);

    if (docPhyto.file.length > 2) {
        HideUpload($("#lblPhytoFile").closest('div.container-fluid'));
    } else {
        ShowUpload($(lblPhytoFile).closest('div.container-fluid'));
    }

    if (docPhyto.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer4").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer4").closest('div.container-fluid'));
    }

}

function loadBL(documents) {

    docBL = documents.filter(item => item.typeDocumentID == 5)[0]
    $("#typeDocumentId5").val(docBL.typeDocumentID);
    $("#txtBL").val(docBL.number);
    $("#txtenabled5").val(docBL.enabled);

    $("#lblBLFile").text(docBL.file);
    $("#txtBLFile").val('');
    $("#txtBLNotesOZM").val(docBL.notesOBM);

    $("#cmbBLStatus").val(docBL.statusID).change();
    $("#txtBLNotesCustomer").val(docBL.notesCustomer);
    $("#lblfileCustomer5").text(docBL.fileCustomer);

    if (docBL.file.length > 2) {
        HideUpload($("#lblBLFile").closest('div.container-fluid'));
    } else {
        ShowUpload($("#lblBLFile").closest('div.container-fluid'));
    }

    if (docBL.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer5").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer5").closest('div.container-fluid'));
    }
}

function loadCertificateOfOrigin(documents) {

    docCertificateOfOrigin = documents.filter(item => item.typeDocumentID == 6)[0]
    $("#typeDocumentId6").val(docCertificateOfOrigin.typeDocumentID);
    $("#txtCertOrig").val(docCertificateOfOrigin.number);
    $("#txtenabled6").val(docCertificateOfOrigin.enabled);

    $("#lblCertOrigFile").text(docCertificateOfOrigin.file);
    $("#txtCertOrigFile").val('');
    $("#txtCertOrigNotesOZM").val(docCertificateOfOrigin.notesOBM);

    $("#cmbCertOrigStatus").val(docCertificateOfOrigin.statusID).change();
    $("#txtCertOrigNotesCustomer").val(docCertificateOfOrigin.notesCustomer);
    $("#lblfileCustomer6").text(docCertificateOfOrigin.fileCustomer);

    if (docCertificateOfOrigin.file.length > 2) {
        HideUpload($("#lblCertOrigFile").closest('div.container-fluid'));
    } else {
        ShowUpload($("#lblCertOrigFile").closest('div.container-fluid'));
    }

    if (docCertificateOfOrigin.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer6").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer6").closest('div.container-fluid'));
    }
}

function loadExportFile(documents) {

    docExportFile = documents.filter(item => item.typeDocumentID == 7)[0]
    $("#typeDocumentId7").val(docExportFile.typeDocumentID);
    $("#txtExportFile").val(docExportFile.number);
    $("#enabled7").val(docExportFile.enabled);

    $("#lblExportFileFile").text(docExportFile.file);
    $("#txtExportFileFile").val('');
    $("#txtExportFileNotesOZM").val(docExportFile.notesOBM);

    $("#cmbExportFileStatus").val(docExportFile.statusID).change();
    $("#txtExportFileNotesCustomer").val(docExportFile.notesCustomer);
    $("#lblfileCustomer7").text(docExportFile.fileCustomer)

    if (docExportFile.file.length > 2) {
        HideUpload($("#lblExportFileFile").closest('div.container-fluid'));
    } else {
        ShowUpload($("#lblExportFileFile").closest('div.container-fluid'));
    }

    if (docExportFile.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer7").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer7").closest('div.container-fluid'));
    }

}
function loadOthers(documents) {

    docOthers = documents.filter(item => item.typeDocumentID == 8)[0]

    $("#typeDocumentId8").val(docOthers.typeDocumentID);
    $("#txtOther").val(docOthers.number);
    $("#enabled8").val(docOthers.enabled);

    $("#lblOthersFile").text(docOthers.file);
    $("#lblOthersFile2").text(docOthers.file2);
    $("#lblOthersFile3").text(docOthers.file3);
    $("#txtOthersFile").val('');
    $("#txtOthersFile2").val('');
    $("#txtOthersFile3").val('');
    $("#txtOthersNotesOZM").val(docOthers.notesOBM);

    $("#cmbOthersStatus").val(docOthers.statusID).change();
    $("#lblfileCustomer8").text(docOthers.fileCustomer);
    $("#txtOthersNotesCustomer").val(docOthers.notesCustomer);

    if (docOthers.file.length > 2) {
        HideUpload($("#lblOthersFile").closest('div.container-fluid'));
    } else {
        ShowUpload($("#lblOthersFile").closest('div.container-fluid'));
    }

    if (docOthers.fileCustomer.length > 2) {
        HideUpload2($("#lblfileCustomer8").closest('div.container-fluid'));
    } else {
        ShowUpload2($("#lblfileCustomer8").closest('div.container-fluid'));
    }
}

function PreGuardarOpenToClient(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveOpenToClient(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadInvoiceGrower(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadInvoiceGrower(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadInvoiceOBM(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadInvoiceOBM(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadPackingList(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadPackingList(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadPhyto(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadPhyto(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadBL(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadBL(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadCertificateOfOrigin(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadCertificateOfOrigin(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadExportFile(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadExportFile(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function PreGuardarLoadOthers(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadOthers(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function saveOpenToClient() {
    var o = {}
    o = {
        "orderproductionID": _orderproductionID,
        "open": $("#cmbOpenToClient").val(),
        "commentsOBM": $("#txtcommentaryDocumentsOBM").val(),
        "userID": $("#lblSessionUserID").text()
    }
    console.log(o);

    //ajax
    var api = "/ShippingProgram/SaveShippingProgramPOforCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });


}

function saveLoadInvoiceGrower(xthis) {

    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtInvoiceGrowerFile").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId").val(), 1, 'CUS');
        UploadFilesOnAzure("txtInvoiceGrowerFile", file1);
    } else {
        file1 = $("#lblInvoiceGrowerFile").text();
    }
    $("#lblInvoiceGrowerFile").text(file1);


    var o = {}
    o =
    {
        "typeDocumentID": $("#typeDocumentId").val(),
        "number": $("#txtInvoiceGrower").val(),
        //"file": file1,//$("#txtInvoiceGrowerFile").val(),
        //"file2": $("#txtInvoiceGrowerFile2").val(),
        //"file3": $("#txtInvoiceGrowerFile3").val(),
        "notesOBM": $("#txtInvoiceGrowerNotesOZM1").val(),
        "enabled": $("#txtenabled").val(),
        "statusId": $("#cmbInvoiceGrowerStatus option:selected").val(),
        "fileCustomer": file1,// $("#txtfileCustomer1").val(),
        "notesCustomer": $("#txtInvoiceGrowerNotesCustomer").val(),
        "orderproductionID": _orderproductionID,
        "userID": $("#lblSessionUserID").text()

    }
    console.log(o);

    //ajax
    var api = "/ShippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });


}

function saveLoadInvoiceOBM(xthis) {

    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtfileCustomer2").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId2").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer2", file1);
    } else {
        file1 = $("#lblfileCustomer2").text();
    }
    $("#lblfileCustomer2").text(file1);

    if ($("#cmbInvoiceOZMStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }

    o =
    {
        "typeDocumentID": $("#typeDocumentId2").val(),
        "number": $("#txtInvoiceOZM").val(),
        //"file": file1,//$("#txtInvoiceOZMFile").val(),
        //"file2": $("#txtInvoiceOZMFile2").val(),
        //"file3": $("#txtInvoiceOZMFile3").val(),
        "notesOBM": $("#txtInvoiceOZMNotesOZM2").val(),
        "enabled": $("#txtenabled2").val(),
        "statusId": $("#cmbInvoiceOZMStatus option:selected").val(),
        "fileCustomer": file1,//$("#txtfileCustomer2").val(),
        "notesCustomer": $("#txtInvoiceOZMNotesCustomer").val(),
        "orderproductionID": _orderproductionID,
        "userID": $("#lblSessionUserID").text()
    }
    console.log(o);

    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });


}

function saveLoadPackingList(xthis) {
    var PDF = $("#txtPLFile").val();
    var Excel = $("#txtPLFile2").val();
    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtfileCustomer3").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId3").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer3", file1);
    } else {
        file1 = $("#lblfileCustomer3").text();
    }
    $("#lblfileCustomer3").text(file1);

    if ($("#cmbPLStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }

    o =
    {
        "typeDocumentID": $("#typeDocumentId3").val(),
        "number": $("#txtInvoiceOZM").val(),
        //"file": file1,//$("#txtPLFile").val(),
        //"file2": file2,//$("#txtPLFile2").val(),
        //"file3": $("#txtPLFile3").val(),
        "notesOBM": $("#txtPLNotesOZM").val(),
        "enabled": $("#txtenabled3").val(),
        "statusId": $("#cmbPLStatus option:selected").val(),
        "fileCustomer": file1,
        "notesCustomer": $("#txtPLNotesCustomer").val(),
        "orderproductionID": _orderproductionID,
        "userID": $("#lblSessionUserID").text()
    }

    console.log(o);
    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });


}

function saveLoadPhyto(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtfileCustomer4").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId4").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer4", file1);
        saveFile(xthis);
    } else {
        file1 = $("#lblfileCustomer4").text();
    }
    $("#lblfileCustomer4").text(file1);

    if ($("#cmbPhytoStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }


    o =
    {
        "typeDocumentID": $("#typeDocumentId4").val(),
        "number": $("#txtPhyto").val(),
        //"file": file1,//$("#txtPhytoFile").val(),
        //"file2": $("#txtPhytoFile2").val(),
        //"file3": $("#txtPhytoFile3").val(),
        "notesOBM": $("#txtPhytoNotesOZM").val(),
        "enabled": $("#txtenabled4").val(),
        "statusId": $("#cmbPhytoStatus option:selected").val(),
        "fileCustomer": file1,
        //"fileCustomer": $("#txtfileCustomer4").val(),
        "notesCustomer": $("#txtPhytoNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID


    }

    console.log(o);
    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });
}

function saveLoadBL(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtfileCustomer5").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId5").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer5", file1);
    } else {
        file1 = $("#lblfileCustomer5").text();
    }
    $("#lblfileCustomer5").text(file1);

    if ($("#cmbBLStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }

    o =
    {
        "typeDocumentID": $("#typeDocumentId5").val(),
        "number": $("#txtBL").val(),
        //"file": file1,//$("#txtBLFile").val(),
        //"file2": $("#txtBLFile2").val(),
        //"file3": $("#txtBLFile3").val(),
        "notesOBM": $("#txtBLNotesOZM").val(),
        "enabled": $("#txtenabled5").val(),
        "statusId": $("#cmbBLStatus option:selected").val(),
        "fileCustomer": file1,
        "notesCustomer": $("#txtBLNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }
    console.log(o);

    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });
}

function saveLoadCertificateOfOrigin(xthis) {

    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtfileCustomer6").val().length > 2) {
        file1 = setNameFileAttach($("#typeDocumentId6").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer6", file1);
    } else {
        file1 = $("#lblfileCustomer6").text();
    }
    $("#lblfileCustomer6").text(file1);

    if ($("#cmbCertOrigStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }

    o =
    {
        "typeDocumentID": $("#typeDocumentId6").val(),
        "number": $("#txtCertOrig").val(),
        //"file": file1,//$("#txtCertOrigFile").val(),
        //"file2": $("#txtCertOrigFile2").val(),
        //"file3": $("#txtCertOrigFile3").val(),
        "notesOBM": $("#txtCertOrigNotesOZM").val(),
        "enabled": $("#txtenabled6").val(),
        "statusId": $("#cmbCertOrigStatus option:selected").val(),
        "fileCustomer": file1,
        "notesCustomer": $("#txtCertOrigNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }

    console.log(o);
    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });
}

function saveLoadExportFile(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    if ($("#txtfileCustomer7").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId7").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer7", file1);
    } else {
        file1 = $("#lblfileCustomer7").text();
    }
    $("#lblfileCustomer7").text(file1);

    if ($("#cmbExportFileStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }

    o =
    {
        "typeDocumentID": $("#typeDocumentId7").val(),
        "number": $("#txtExportFile").val(),
        //"file": file1,//$("#txtExportFileFile").val(),
        //"file2": $("#txtExportFileFile2").val(),
        //"file3": $("#txtExportFileFile3").val(),
        "notesOBM": $("#txtExportFileNotesOZM").val(),
        "enabled": $("#enabled7").val(),
        "statusId": $("#cmbExportFileStatus option:selected").val(),
        "fileCustomer": file1,
        "notesCustomer": $("#txtExportFileNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }
    console.log(o);


    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });
}

function saveLoadOthers(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";

    if ($("#txtfileCustomer8").val().length >= 1) {
        file1 = setNameFileAttach($("#typeDocumentId8").val(), 1, 'CUS');
        UploadFilesOnAzure("txtfileCustomer8", file1);
    } else {
        file1 = $("#lblfileCustomer8").text();
    }
    $("lblfileCustomer8").text(file1);

    if ($("#cmbOthersStatus option:selected").val() == 1) {

        sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
        return;
    }
    o =
    {
        "typeDocumentID": $("#typeDocumentId8").val(),
        "number": $("#txtOther").val(),
        //"file": file1,//$("#txtOthersFile").val(),
        //"file2": file2,//$("#txtOthersFile2").val(),
        //"file3": file3,//$("#txtOthersFile3").val(),
        "notesOBM": $("#txtOthersNotesOZM").val(),
        "fileCustomer": file1,
        "enabled": $("#enabled8").val(),
        "statusId": $("#cmbOthersStatus option:selected").val(),
        "userID": $("#lblSessionUserID").text(),
        "notesCustomer": $("#txtOthersNotesCustomer").val(),
        "orderproductionID": _orderproductionID
    }
    console.log(o);

    //ajax
    var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,

        //async: false,
        contentType: "application/json",
        dataType: 'json',
        //data: JSON.stringify(Object.fromEntries(frmPackingList)),
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
                saveFile(xthis);
                //loadListPrincipal();
                //$('#myModal').modal('hide');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
                console.log('An error occurred while recording the log')
                console.log(api)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });
}


function setNameFileAttach(typeDocumentID, numberFile, origin) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var curHour = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
    var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    today = mm + '/' + dd + '/' + yyyy;
    var now = yyyy + mm + dd + "-" + curHour + curMinute;

    var abbrev = ""
    if (origin = "OBM") {
        switch (parseInt(typeDocumentID)) {
            case 1:
                abbrev = "INVGRO";
                break;
            case 2:
                abbrev = "INVCUS";
                break;
            case 3:
                if (numberFile == 1) {
                    abbrev = "PLPDF";
                }
                if (numberFile == 2) {
                    abbrev = "PLXLS";
                }
                break;
            case 4:
                abbrev = "PHYTO";
                break;
            case 5:
                abbrev = "BL";
                break;
            case 6:
                abbrev = "CO";
                break;
            case 7:
                abbrev = "EF";
                break;
            case 8:
                if (numberFile == 1) {
                    abbrev = "OTHER1";
                }
                if (numberFile == 2) {
                    abbrev = "OTHER2";
                }
                if (numberFile == 3) {
                    abbrev = "OTHER3";
                }
                break;
            default:
        }
    }
    if (origin = "CUS") {
        switch (typeDocumentID) {
            case 1:
                abbrev = "INVGRO_";
                break;
            case 2:
                abbrev = "INVCUS_";
                break;
            case 3:
                abbrev = "PL_";
                break;
            case 4:
                abbrev = "PHYTO_";
                break;
            case 5:
                abbrev = "BL_";
                break;
            case 6:
                abbrev = "CO_";
                break;
            case 7:
                abbrev = "EF_";
                break;
            case 8:
                abbrev = "OTHER_";
                break;
            default:
        }
    }


    return abbrev + "_" + now;

}

function UploadFilesOnAzure(control, filename) {
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);

    var files = document.getElementById(control).files;
    var file = files[0];

    fileService.createFileFromBrowserFile(SharedDirectory, '', filename, file, {}, function (error, result, response) {
        if (error) {
            // upload file error
            console.log("FILE DO NOT UPLOADED: " + response);
            console.log("result: " + result);
        } else {
            // upload file successfully
            console.log("FILE UPLOADED: " + Object.keys(response) + "//" + response.isSuccessful);
            console.log("result: " + Object.keys(result) + " // " + result.etag + " // " + result.share + " // " + result.directory + " // " + result.name + " // " + result.etag + " // " + result.lastModified + " // " + result.requestId);
        }

        return response;
    });

}

function getLink(myfile) {
    //var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
    //var SAS_TOKEN1 = "sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2021-01-01T04:59:59Z&st=2020-04-06T00:57:30Z&spr=https&sig=0gjOhB1iFXgblSVElnm1Z9w3hAwUvDpFNTqu7J2RMWw%3D";
    //var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    //var downloadLink = fileService.getUrl(SharedDirectory, '', myfile, SAS_TOKEN1);
    //console.log("LIIIIIIIIIINK: " + downloadLink);
    //return downloadLink;
    //var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
    //var SAS_TOKEN1 = "sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2021-01-01T04:59:59Z&st=2020-04-06T00:57:30Z&spr=https&sig=0gjOhB1iFXgblSVElnm1Z9w3hAwUvDpFNTqu7J2RMWw%3D";
    let SAS_TOKEN_BASE = 'sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-10-05T23:46:26Z&st=2021-10-05T15:46:26Z&spr=https&sig=yZd1HpM86A1N%2ByEBmVZyrmtqovxryJeaZ5TrH8kelYc%3D';
    let SAS_TOKEN_DOWNLOAD = '?' + 'comp=list&restype=container&' + SAS_TOKEN_BASE;
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    var downloadLink = fileService.getUrl(SharedDirectory, '', myfile, SAS_TOKEN_DOWNLOAD);
    console.log("LIIIIIIIIIINK: " + downloadLink);
    window.open(downloadLink); // To open in a new tab/window
    //window.location.href = 'data:application/vnd.ms-excel;charset=UTF-8;base64,' + downloadLink;

    /*  window.location = downloadLink;*/ // To change the current page

    //var urlData = 'data:text/csv;charset=UTF-8,' + downloadLink;
    //var fileName = '2.csv';
    //var aLink = document.createElement('a');
    //var evt = document.createEvent("HTMLEvents");
    //evt.initEvent("click");
    //aLink.download = fileName;
    //aLink.href = urlData;
    //aLink.dispatchEvent(evt);

    //var request = new XMLHttpRequest();
    //request.open("GET", downloadLink);
    //request.responseType = "blob";
    //request.onload = function () {
    //  // set `blob` `type` to `"text/html"`
    //  var blob = new Blob([this.response], { type: "text/html" });
    //  var url = URL.createObjectURL(blob);
    //  var w = window.open(url);
    //}
    //request.send();

    //var request = require('request');
    //var XLSX = require('xlsx');
    //request(downloadLink, { encoding: null }, function (error, response, body) {
    //  var workbook = XLSX.read(body, { type: "buffer" });
    //  console.log(workbook.Sheets.Sheet1);
    //});

    //var blob = new Blob(downloadLink, { type: "application/vnd.ms-excel" });
    //var objectUrl = URL.createObjectURL(blob);
    //window.open(objectUrl);


}





//var _orderproductionID = 32;
//var dataStatus = [];

//var procesadoInvoiceOZM = 0;

//dataStatus = [
//  {
//    "statudID": 0,
//    "status": "Pending"
//  },
//  {
//    "statudID": 1,
//    "status": "Processing"
//  },
//  {
//    "statudID": 2,
//    "status": "Aproved"
//  },
//  {
//    "statudID": 3,
//    "status": "To correct"
//  }
//];

//var list = [
//  {
//    "orderProductionID": 32,
//    "orderProduction": "OZB016",
//    "container": "MNBU0570603",
//    "customer": "Dole",
//    "grower": "Ozblu Peru SAC",
//    "shippingLine": "MAERSK",
//    "destinationPort": "Shangai",
//    "via": "Sea",
//    "etd": "6-Aug-20",
//    "eta": "2-sep-20",
//    "responsable": "Michi",
//    "invoiceGrowerStatus": "APPROVED",
//    "invoiceGrowerStatusID": "2",
//    "invoiceCustomerStatus": "PROCESSING",
//    "invoiceCustomerStatusID": "1",
//    "packingListStatus": "PROCESSING",
//    "packingListStatusID": "1",
//    "phytoStatus": "APPROVED",
//    "phytoStatusID": "2",
//    "blStatus": "APPROVED",
//    "blStatusID": "2",
//    "certificateOriginStatus": "REJECTED",
//    "certificateOriginStatusID": "3",
//    "exportFileStatus": "PENDING",
//    "exportFileStatusID": "0",
//    "othersStatus": "PENDING",
//    "othersStatusID": "0"
//  },
//  {
//    "orderProductionID": 32,
//    "orderProduction": "OZB016",
//    "container": "MNBU0570603",
//    "customer": "Dole",
//    "grower": "Ozblu Peru SAC",
//    "shippingLine": "MAERSK",
//    "destinationPort": "Shangai",
//    "via": "Sea",
//    "etd": "6-Aug-20",
//    "eta": "2-sep-20",
//    "responsable": "Michi",
//    "invoiceGrowerStatus": "APPROVED",
//    "invoiceGrowerStatusID": "2",
//    "invoiceCustomerStatus": "PROCESSING",
//    "invoiceCustomerStatusID": "1",
//    "packingListStatus": "PROCESSING",
//    "packingListStatusID": "1",
//    "phytoStatus": "APPROVED",
//    "phytoStatusID": "2",
//    "blStatus": "APPROVED",
//    "blStatusID": "2",
//    "certificateOriginStatus": "REJECTED",
//    "certificateOriginStatusID": "3",
//    "exportFileStatus": "PENDING",
//    "exportFileStatusID": "0",
//    "othersStatus": "PENDING",
//    "othersStatusID": "0"
//  }
//]


//var o = {
//  "orderProductionID": 32,

//  "orderProduction": "OZB016",
//  "packingList": "CAL-ARA-00025/2020",
//  "originPort": "Lima",
//  "container": "MNBU0570603",

//  "customer": "Dole",
//  "grower": "Ozblu Peru SAC",
//  "destinationPort": "Shanhai",
//  "vessel": "MSC JEWEL FA032R",

//  "incoterm": "FOB",
//  "origin": "Peru",
//  "shippingLine": "MAERSK",
//  "etd": "15-Aug-20",
//  "eta": "19-sep-20",

//  "saleTerm": "MG",
//  "via": "Asia",
//  "bl": "ONEYLIMA10633800",
//  "responsable": "Michi",

//  "open": "0",
//  "statusRevision": "0",
//  "commentaryDocumentsOBM": "commets OZM",
//  "commentaryDocumentsCustomer": "commets customer",

//  "grossWeight": 500,

//  "userModify": "pepito",
//  "dateModify": "29/08/2020",
//  "details": [
//    {
//      "brand": "Ozblu",
//      "variety": "Bonita",
//      "codepack": "C444",
//      "sizeInfo": "20mm",
//      "boxes": "1950",
//      "pallets": "5",
//      "netWeight": "2925"
//    },
//    {
//      "brand": "BigBold",
//      "variety": "Bonita",
//      "codepack": "C444",
//      "sizeInfo": "16mm",
//      "boxes": "5850",
//      "pallets": "15",
//      "netWeight": "8775"
//    }
//  ]
//}


//var documents = [
//  {
//    "typeDocumentID": 1,
//    "typeDocument": "Invoice Grower",
//    "number": "FAC F001-234",
//    "file": "INVG-PO179-5-20200709-114",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "3",
//    "status": "To correct",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//},
//{
//    "typeDocumentID": 2,
//    "typeDocument": "Invoice OBM",
//    "number": "FEX 001-001212S",
//    "file": "INVO-PO179-5-20200709-114",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "1",
//  "status": "Pending",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//},
//{
//    "typeDocumentID": 3,
//    "typeDocument": "Packing List",
//    "number": "",
//    "file": "",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "1",
//  "status": "Processing",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//},
//{
//    "typeDocumentID": 4,
//    "typeDocument": "Phyto",
//    "number": "",
//    "file": "",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "0",
//    "status": "Pending",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//},

//{
//    "typeDocumentID": 5,
//    "typeDocument": "BL",
//    "number": "",
//    "file": "",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "0",
//    "status": "Pending",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"

//},
//{
//    "typeDocumentID": 6,
//    "typeDocument": "Certificate Of Origin",
//    "number": "",
//    "file": "",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "0",
//    "status": "Pending",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//},
//{
//    "typeDocumentID": 7,
//    "typeDocument": "Export File",
//    "number": "",
//    "file": "",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "0",
//    "status": "Pending",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//},
//{
//    "typeDocumentID": 8,
//    "typeDocument": "Others",
//    "number": "",
//    "file": "",
//    "file2": "",
//    "file3": "",
//    "notesOBM": "comentario de ozblu marketing",
//    "userOBM": "Pepito",
//    "dateModifyOBM": "2020-09-27",
//    "enabled":1,

//    "statusID": "0",
//    "status": "Pending",
//    "fileCustomer": "",
//    "notesCustomer": "comentario de cliente",
//    "userCustomer": "Pepito",
//    "dateCustomer": "2020-09-27"
//}
//]


//$(function () {

//  loadData();
//  //loadDefaultValues;

//  $(document).on("click", ".btnAttach", function (e) {
//    saveFile(this)
//  });

//  //$(document).on("click", ".btnAttachCustomer", function (e) {
//  //  saveFile2(this)
//  //});

//  $(document).on("click", ".btnDelete", function (e) {
//    dropFile(this)
//  });

//  $(document).on("click", ".btnDeleteCustomer", function (e) {
//    dropFile2(this)
//  });

//  $(document).on("click", "#btnConfirm", function (e) {
//    PreGuardarOpenToClient();
//  });
//  $(document).on("click", "#btnSaveInvoiceGrower", function (e) {
//    PreGuardarLoadInvoiceGrower();
//  });

//  $(document).on("click", "#btnSaveInvoiceOZM", function (e) {
//    PreGuardarLoadInvoiceOBM();
//  });
//  $(document).on("click", "#btnSavePL", function (e) {
//    PreGuardarLoadPackingList();
//  });
//  $(document).on("click", "#btnSavePhyto", function (e) {
//    PreGuardarLoadPhyto();
//  });
//  $(document).on("click", "#btnSaveBL", function (e) {
//    PreGuardarLoadBL();
//  });
//  $(document).on("click", "#btnSaveCertOrig", function (e) {
//    PreGuardarLoadCertificateOfOrigin();
//  });
//  $(document).on("click", "#btnSaveExportFile", function (e) {
//    PreGuardarLoadExportFile();
//  });
//  $(document).on("click", "#btnSaveOthers", function (e) {
//    PreGuardarLoadOthers();
//  });


//  $("select#cmbStatusRevision").change(function () {
//    console.log($(this).children("option:selected").val())
//    color = setColorByStatus1($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbStatusRevision').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbStatusRevision').css("background-color", color.backgroundColor);

//  })
//  $("select#cmbInvoiceGrowerStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbInvoiceGrowerStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbInvoiceGrowerStatus').css("background-color", color.backgroundColor);
//  })
//  $("select#cmbInvoiceOZMStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbInvoiceOZMStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbInvoiceOZMStatus').css("background-color", color.backgroundColor);

//    if ($("select#cmbInvoiceOZMStatus").val()==0) {
//      $('#cmbInvoiceOZMStatus').prop('disabled', true);
//      $('#btnSaveInvoiceOZM').prop('disabled', true);
//      $('#txtfileCustomer2').prop('disabled', true);
//      $('#btnSaveExportFile').prop('disabled', true);
//      $('#txtInvoiceOZMNotesCustomer').prop('readonly', true);
//    }
//    if ($("select#cmbInvoiceOZMStatus").val() == 1) {
//      procesadoInvoiceOZM = 1;
//    }
//  })
//  $("select#cmbPLStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbPLStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbPLStatus').css("background-color", color.backgroundColor);
//  })
//  $("select#cmbPhytoStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbPhytoStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbPhytoStatus').css("background-color", color.backgroundColor);
//  })
//  $("select#cmbBLStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbBLStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbBLStatus').css("background-color", color.backgroundColor);
//  })
//  $("select#cmbCertOrigStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbCertOrigStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbCertOrigStatus').css("background-color", color.backgroundColor);
//  })
//  $("select#cmbExportFileStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbExportFileStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbExportFileStatus').css("background-color", color.backgroundColor);
//  })
//  $("select#cmbOthersStatus").change(function () {
//    console.log($(this).children("option:selected").val())

//    color = setColorByStatus($(this).children("option:selected").val());
//    $(this).closest('div.form-group').find('#cmbOthersStatus').css("color", color.fontColor);
//    $(this).closest('div.form-group').find('#cmbOthersStatus').css("background-color", color.backgroundColor);
//  })

//  $(document).on("change", "input#enabled7", function (e) {
//    if ($(this).is(":checked") == true) {
//      $('#txtExportFileFile').prop('disabled', false);
//      $('#txtExportFileNotesOZM').prop('readonly', false);
//      $('#txtfileCustomer7').prop('disabled', false);
//      $('#btnSaveExportFile').prop('disabled', false);

//    } else {
//      $('#txtExportFileFile').prop('disabled', true);
//      $('#txtExportFileNotesOZM').prop('readonly', true);
//      $('#txtfileCustomer7').prop('disabled', true);
//      $('#btnSaveExportFile').prop('disabled', true);
//    }
//  });
//  $(document).on("change", "input#enabled8", function (e) {
//    if ($(this).is(":checked") == true) {
//      $('#txtOthersFile').prop('disabled', false);
//      $('#txtOthersFile2').prop('disabled', false);
//      $('#txtOthersFile3').prop('disabled', false);
//      $('#txtOthersNotesOZM').prop('readonly', false);
//      $('#txtfileCustomer7').prop('readonly', false);
//      $('#btnSaveOthers').prop('disabled', false);

//    } else {
//      $('#txtOthersFile').prop('disabled', true);
//      $('#txtOthersFile2').prop('disabled', true);
//      $('#txtOthersFile3').prop('disabled', true);
//      $('#txtOthersNotesOZM').prop('readonly', true);
//      $('#txtfileCustomer7').prop('readonly', true);
//      $('#btnSaveOthers').prop('disabled', true);
//    }
//  });


//})





//function openModal(orderProductionID) {
//  loadDefaultValuesSelect();
//  $(myModal).show();



//  getPOById(orderProductionID)

//  $(".btnDelete").hide()
//  $(".lblFile").hide()
//  $(".btnDownLoad").hide();

//  $("txtOrderProductID").val(orderProductionID);
//  loadDetailsDocuments(orderProductionID);
//  OpenToClient(orderProductionID);
//  loadInvoiceGrower(orderProductionID);
//  loadInvoiceOBM(orderProductionID);
//  loadPackingList(orderProductionID);
//  loadPhyto(orderProductionID);
//  loadBL(orderProductionID);
//  loadCertificateOfOrigin(orderProductionID);
//  loadExportFile(orderProductionID);
//  loadOthers(orderProductionID);

//}




////function saveFile2(xthis) {
////  $(xthis).parent('div').find('.btnDeleteCustomer').show();
////  $(xthis).parent('div').find('.lblFileCustomer').show();
////  $(xthis).parent('div').find('.btnDownLoadCustomer').show()

////  $(xthis).parent('div').find('.inputFile').hide();
////  $(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
////}

//function dropFile(xthis) {
//  $(xthis).parent('div').find('.btnDelete').hide()
//  $(xthis).parent('div').find('.lblFile').hide()
//  $(xthis).parent('div').find('.btnDownLoad').hide()

//  $(xthis).parent('div').find('.inputFile').show()
//  //$(xthis).parent('div').find('.btnAttach ').show()

//  $(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("0").change() //pending


//}

//function saveFile(xthis) {
//  $(xthis).parent('div').find('.btnDeleteCustomer').show();
//  $(xthis).parent('div').find('.lblFileCustomer').show();
//  $(xthis).parent('div').find('.btnDownLoadCustomer').show()

//  $(xthis).parent('div').find('.inputFile').hide();
//  $(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin

//}

//function getPOById(orderProductionID) {
//  //ajax

//  //var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 2) {
//        //var o = JSON.parse(data);
//        $(txtProductionOrder).val(o.orderProduction);
//        $(txtCustomer).val(o.customer);
//        //$(txtPoWeek).val(o.productionWeek);
//        //$(txtStatus).val(o.status);
//        $(txtOriginPort).val(o.originPort);
//        $(txtOrigin).val(o.origin);
//        $(txtVia).val(o.via);

//        //$(txtBooking).val(o.booking);
//        //$(txtGrossWeight).val(o.grossWeight);
//        $(txtEta).val(o.eta);
//        $(txtIncoterm).val(o.incoterm);
//        $(txtPortDestination).val(o.destinationPort);

//        $(txtShippingLine).val(o.shippingLine);
//        //$(txtWeekEtd).val(o.weekEtd);
//        //$(txtWeekReal).val(o.weekEtaReal);
//        //$(txtLoadingDate).val(o.loadingDate);
//        $(txtSaleTerm).val(o.saleTerm);

//        $(txtVessel).val(o.vessel);
//        $(txtEtd).val(o.etd);
//        //$(txtEtaReal).val(o.etaReal);

//        $(txtGrower).val(o.grower);
//        //$(txtMarket).val(o.market);
//        $(txtBl).val(o.bl);
//        //$(txtMarket).val(o.market);

//        $(txtContainer).val(o.container);
//        $(txtResponsable).val(o.responsable);
//        $(txtIE).val(o.packingList);
//        //$(txtWeekEta).val(o.weekEta);

//  //    }

//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});


//}

//function loadDetailsDocuments(orderProductionID) {

//  //var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//  //      var o = JSON.parse(data);
//  //      //Creando el formato de la tabla
//  //      var content = "";
//  //      //Recorriendo la lista para llenar el detalle de la tabla
//        var grossWeight = 0;
//        $.each(o.details, function (index, row) {
//          grossWeight = row.grossWeight;
//          content += "    <tr style='font-size: 14px'>";
//          content += "      <td id='brand'>" + row.brand + "</td>";
//          content += "      <td id='variety' >" + row.variety + " </td>";
//          content += "      <td id='codepack'>" + row.codepack + "</td>";
//          content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
//          content += "      <td id='boxes'>" + row.boxes + "</td>";
//          content += "      <td id='pallets'>" + row.pallets + "</td>";
//          content += "      <td id='netWeight'>" + row.netWeight + "</td>";
//          content += "    </tr>";
//        });

//        $("#tblDetails>tbody").empty().append(content);
//        //Calcular el footer
//        var boxes = 0;
//        var pallets = 0;
//        var netWeight = 0;
//        $('#tblDetails>tbody>tr').each(function (j, row) {
//          boxes = boxes + parseFloat($(row).find('#boxes').text());
//          pallets = pallets + parseFloat($(row).find('#pallets').text());
//          netWeight = netWeight + parseFloat($(row).find('#netWeight').text());
//        });
//        //Insertar los valores del footer
//        contentfoot = "";
//        contentfoot += "<tr style='font-size: 14px'>";
//        contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
//        contentfoot += '<td>' + (boxes).toFixed(0) + '</td>';
//        contentfoot += '<td>' + (pallets).toFixed(2) + '</td>';
//        contentfoot += '<td>' + (netWeight).toFixed(2) + '</td>';
//        contentfoot += "</tr>";
//        contentfoot += "<tr style='font-size: 14px'>";
//        contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Gross Weight";
//        contentfoot += '<td>' + o.grossWeight + '</td>';
//        //contentfoot += '<td>' + grossWeight + '</td>';
//        contentfoot += '<td></td>';
//        contentfoot += '<td></td>';
//        contentfoot += "</tr>";
//        $("#tblDetails>tfoot").empty().append(contentfoot);
//     // }
////    },
////    error: function (xhr, ajaxOptions, thrownError) {
////      $.notify("Problemas con el cargar el listado!", "error");
////      console.log(api)
////      console.log(xhr);
////      console.log(ajaxOptions);
////      console.log(thrownError);
////    }
////  });

//}

//function OpenToClient(orderProductionID) {
//  //var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//  //      var o = JSON.parse(data);
//        $("#cmbOpenToClient").val(o.open);
//        $("#txtcommentaryDocumentsOBM").val(o.commentaryDocumentsOBM);
//        $("#txtcommentaryDocumentsCustomer").val(o.commentaryDocumentsCustomer);
//        $("#cmbStatusRevision").val(o.statusRevision).change();
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}


//function setColorByStatus1(statusID) {
//  var color = { backgroundColor: "a", fontColor: "b" }
//  var o = {}
//  switch (parseInt(statusID)) {

//    case 0://incomplete
//      o.backgroundColor = '#FBFF00'; //amarillo
//      o.fontColor = '#000000'; //negro
//      break;
//    case 1: //complete
//      o.backgroundColor = '#09AB01'; //blanco
//      o.fontColor = '#FFFFFF'; //negro
//      break;
//  }
//  return o;
//}


//function setColorByStatus(statusID) {
//  var color = { backgroundColor: "a", fontColor: "b" }
//  var o = {}
//  switch (parseInt(statusID)) {
//    case 0: //pending
//      o.backgroundColor = '#000000'; //blanco
//      o.fontColor = '#FFFFFF'; //negro
//      break;
//    case 1://processing
//      o.backgroundColor = '#FBFF00'; //amarillo
//      o.fontColor = '#000000'; //negro
//      break;
//    case 2://appoved
//      o.backgroundColor = '#09AB01' //verde
//      o.fontColor = '#FFFFFF'; //blanco
//      break;
//    case 3://rejected
//      o.backgroundColor = '#FF0000' //rojo
//      o.fontColor = '#FFFFFF'; //blanco
//      break;
//  }
//  return o;
//}
//function loadData() {
//  //Ajax

//  //var api = "/ShippingProgram/GetAllPO";
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  //async: false,
//  //  success: function (data) {
//  //    if (data.length > 2) {
//  //      var list = JSON.parse(data);
//        $("table#tableList>tbody").empty();
//        $.each(list, function (i, item) {
//          var tr = ''
//          tr += '<tr  style="vertical-align:middle; font-size:13px">'
//          tr += '<td style="display:none">' + item.orderProductionID + '</td>'
//          tr += '<td > <button class="btn btn-primary btn-sm" onclick="openModal(' + item.orderProductionID + ')" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-sm"></i></button> </td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.orderProduction + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.container + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.customer + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.grower + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.shippingLine + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.destinationPort + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.via + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.etd + '</td>'
//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.eta + '</td>'

//          tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.responsable + '</td>'
//          var color = {}
//          color = setColorByStatus(item.invoiceGrowerStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:80px;max-width:80px;">' + item.invoiceGrowerStatus + '</td>'
//          color = setColorByStatus(item.invoiceCustomerStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:80px;max-width:80px;">' + item.invoiceCustomerStatus + '</td>'
//          color = setColorByStatus(item.packingListStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.packingListStatus + '</td>'
//          color = setColorByStatus(item.phytoStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.phytoStatus + '</td>'
//          color = setColorByStatus(item.blStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.blStatus + '</td>'
//          color = setColorByStatus(item.certificateOriginStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.certificateOriginStatus + '</td>'
//          color = setColorByStatus(item.exportFileStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:95px;max-width:95px;">' + item.exportFileStatus + '</td>'
//          color = setColorByStatus(item.othersStatusID)
//          tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:80px;max-width:80px;">' + item.othersStatus + '</td>'
//          tr += '</tr>'
//          $("table#tableList>tbody").append(tr);
//        })

//        $(tableList).dataTable({
//          dom: 'Bfrtip',
//          buttons: ['copy', 'excel', 'pdf', 'csv'],
//          responsive: true,
//          lengthMenu: [10, 25, 50, 75, 100],
//          destroy: true
//          //buttons: [
//          //    'copy', 'csv', 'excel', 'pdf', 'print'
//          //]
//        })


//      //}

//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});


//}
//function loadDefaultValuesSelect() {


//  $('#cmbStatusRevision').val(0).change();

//  var select = $('#cmbInvoiceGrowerStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbInvoiceGrowerStatus').val(0).change();

//  var select = $('#cmbInvoiceOZMStatus');
//  if (procesadoInvoiceOZM == 1) {
//    var content = ""; $.each(dataStatus, function (i, e) {
//      if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//      if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//      if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//      if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    })
//  } else {
//    var content = ""; $.each(dataStatus, function (i, e) {
//      if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//      if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//      if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    })
//  }


//  select.empty().append(content);
//  $('#cmbInvoiceOZMStatus').val(0).change();

//  var select = $('#cmbPLStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbPLStatus').val(0).change();


//  var select = $('#cmbPhytoStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbPhytoStatus').val(0).change();

//  var select = $('#cmbBLStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbBLStatus').val(0).change();


//  var select = $('#cmbCertOrigStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbCertOrigStatus').val(0).change();

//  var select = $('#cmbExportFileStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbExportFileStatus').val(0).change();

//  var select = $('#cmbOthersStatus');

//  var content = ""; $.each(dataStatus, function (i, e) {
//    if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//    if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
//  })

//  select.empty().append(content);
//  $('#cmbOthersStatus').val(0).change();


//}
//function loadCombo(data, control, firtElement) {
//  var content = "";
//  if (firtElement == true) {
//    content += "<option value='0'>--Choose--</option>";
//  }
//  for (var i = 0; i < data.length; i++) {
//    content += "<option value='" + data[i].id + "'>";
//    content += data[i].name;
//    content += "</option>";
//  }
//  $('#' + control).empty().append(content);
//}
//function loadInvoiceGrower(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//  //      var documents = JSON.parse(data);
//        docInvoiceGrower = documents.filter(item => item.typeDocumentID == 1)[0]
//        $("#txtInvoiceGrowerNotesOZM1").val(docInvoiceGrower.notesOBM);
//        $("#txtInvoiceGrower").val(docInvoiceGrower.number);
//        $("#cmbInvoiceGrowerStatus").val(docInvoiceGrower.statusID).change();
//        $("#txtInvoiceGrowerNotesCustomer").val(docInvoiceGrower.notesCustomer);
//        //$("#txtInvoiceGrowerFile").val(docInvoiceGrower.file);
//        $("#typeDocumentID").val(docInvoiceGrower.typeDocumentID);
//        //$("#txtInvoiceGrowerFile2").val(docInvoiceGrower.file2);
//        //$("#txtInvoiceGrowerFile3").val(docInvoiceGrower.file3);
//        $("#txtenabled").val(docInvoiceGrower.enabled);
//        //$("#txtfileCustomer1").val(docInvoiceGrower.fileCustomer);
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});

//}
//function loadInvoiceOBM(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//        //var documents = JSON.parse(data);
//        docInvoiceOBM = documents.filter(item => item.typeDocumentID == 2)[0]
//        $("#txtInvoiceOZMNotesOZM2").val(docInvoiceOBM.notesOBM);
//        $("#txtInvoiceOZM").val(docInvoiceOBM.number);
//        $("#cmbInvoiceOZMStatus").val(docInvoiceOBM.statusID).change();
//        $("#txtInvoiceOZMNotesCustomer").val(docInvoiceOBM.notesCustomer);
//        //$("#txtInvoiceOZMFile").val(docInvoiceOBM.file);
//        $("#typeDocumentId2").val(docInvoiceOBM.typeDocumentID);
//        //$("#txtInvoiceOZMFile2").val(docInvoiceOBM.file2);
//        //$("#txtInvoiceOZMFile3").val(docInvoiceOBM.file3);
//        $("#txtenabled2").val(docInvoiceOBM.enabled);
//        //$("#txtfileCustomer2").val(docInvoiceGrower.fileCustomer);
//      //}
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}

//function loadPackingList(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//  //      var documents = JSON.parse(data);
//        docPackingList = documents.filter(item => item.typeDocumentID == 3)[0]
//        $("#txtPLNotesOZM").val(docPackingList.notesOBM);
//        $("#txtPL").val(docPackingList.number);
//        $("#cmbPLStatus").val(docPackingList.statusID).change();
//        $("#txtPLNotesCustomer").val(docPackingList.notesCustomer);
//        //$("#txtPLFile").val(docPackingList.file);
//        $("#typeDocumentId3").val(docPackingList.typeDocumentID);
//        //$("#txtPLFile2").val(docPackingList.file2);
//        //$("#txtPLFile3").val(docPackingList.file3);
//        $("#txtenabled3").val(docPackingList.enabled);
//        //$("#txtfileCustomer3").val(docPackingList.fileCustomer);
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}

//function loadPhyto(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//        //var documents = JSON.parse(data);
//        docPhyto = documents.filter(item => item.typeDocumentID == 4)[0]
//        $("#txtPhytoNotesOZM").val(docPhyto.notesOBM);
//        $("#txtPhyto").val(docPhyto.number);
//        $("#cmbPhytoStatus").val(docPhyto.statusID).change();
//        $("#txtPhytoNotesCustomer").val(docPhyto.notesCustomer);
//        //$("#txtPhytoFile").val(docPhyto.file);
//        $("#typeDocumentId4").val(docPhyto.typeDocumentID);
//        //$("#txtPhytoFile2").val(docPhyto.file2);
//        //$("#txtPhytoFile3").val(docPhyto.file3);
//        $("#txtenabled4").val(docPhyto.enabled);
//        //$("#txtfileCustomer4").val(docPhyto.fileCustomer);
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}

//function loadBL(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//  //      var documents = JSON.parse(data);
//        docBL = documents.filter(item => item.typeDocumentID == 5)[0]
//        $("#txtBLNotesOZM").val(docBL.notesOBM);
//        $("#txtBL").val(docBL.number);
//        $("#cmbBLStatus").val(docBL.statusID).change();
//        $("#txtBLNotesCustomer").val(docBL.notesCustomer);
//        //$("#txtBLFile").val(docBL.file);
//        $("#typeDocumentId5").val(docBL.typeDocumentID);
//        //$("#txtBLFile2").val(docBL.file2);
//        //$("#txtBLFile3").val(docBL.file3);
//        $("#txtenabled5").val(docBL.enabled);
//        //$("#txtfileCustomer5").val(docBL.fileCustomer);
//     // }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}

//function loadCertificateOfOrigin(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//        //var documents = JSON.parse(data);
//        docCertificateOfOrigin = documents.filter(item => item.typeDocumentID == 6)[0]
//        $("#txtCertOrigNotesOZM").val(docCertificateOfOrigin.notesOBM);
//        $("#txtCertOrig").val(docCertificateOfOrigin.number);
//        $("#cmbCertOrigStatus").val(docCertificateOfOrigin.statusID).change();
//        $("#txtCertOrigNotesCustomer").val(docCertificateOfOrigin.notesCustomer);
//        //$("#txtCertOrigFile").val(docCertificateOfOrigin.file);
//        $("#typeDocumentId6").val(docCertificateOfOrigin.typeDocumentID);
//        //$("#txtCertOrigFile2").val(docCertificateOfOrigin.file2);
//        //$("#txtCertOrigFile3").val(docCertificateOfOrigin.file3);
//        $("#txtenabled6").val(docCertificateOfOrigin.enabled);
//        //$("#txtfileCustomer6").val(docCertificateOfOrigin.fileCustomer);
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}

//function loadExportFile(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//        //var documents = JSON.parse(data);
//        docExportFile = documents.filter(item => item.typeDocumentID == 7)[0]
//        $("#txtExportFileNotesOZM").val(docExportFile.notesOBM);
//        $("#txtExportFile").val(docExportFile.number);
//        $("#cmbExportFileStatus").val(docExportFile.statusID).change();
//        $("#txtExportFileNotesCustomer").val(docExportFile.notesCustomer);
//        //$("#txtExportFileFile").val(docExportFile.file);
//        $("#typeDocumentId7").val(docExportFile.typeDocumentID);
//        //$("#txtExportFileFile2").val(docExportFile.file2);
//        //$("#txtExportFileFile3").val(docExportFile.file3);
//        $("#enabled7").val(docExportFile.enabled);
//        //$("#txtfileCustomer7").val(docExportFile.fileCustomer);
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}
//function loadOthers(orderProductionID) {
//  //var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
//  //$.ajax({
//  //  type: "GET",
//  //  url: api,
//  //  async: false,
//  //  success: function (data) {
//  //    if (data.length > 0) {
//        //var documents = JSON.parse(data);

//        docOthers = documents.filter(item => item.typeDocumentID == 8)[0]
//        $("#txtOthersNotesOZM").val(docOthers.notesOBM);
//        $("#txtOther").val(docOthers.number);
//        $("#cmbOthersStatus").val(docOthers.statusID).change();
//        $("#txtOthersNotesCustomer").val(docOthers.notesCustomer);
//        //$("#txtOthersFile").val(docOthers.file);
//        $("#typeDocumentId8").val(docOthers.typeDocumentID);
//        //$("#txtOthersFile2").val(docOthers.file2);
//        //$("#txtOthersFile3").val(docOthers.file3);
//        $("#enabled8").val(docOthers.enabled);
//        //$("#txtfileCustomer8").val(docOthers.fileCustomer);
//  //    }
//  //  },
//  //  error: function (xhr, ajaxOptions, thrownError) {
//  //    $.notify("Problemas con el cargar el listado!", "error");
//  //    console.log(api)
//  //    console.log(xhr);
//  //    console.log(ajaxOptions);
//  //    console.log(thrownError);
//  //  }
//  //});
//}

//function PreGuardarOpenToClient() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveOpenToClient();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadInvoiceGrower() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadInvoiceGrower();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadInvoiceOBM() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadInvoiceOBM();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadPackingList() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadPackingList();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadPhyto() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadPhyto();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadBL() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadBL();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadCertificateOfOrigin() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadCertificateOfOrigin();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadExportFile() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadExportFile();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}
//function PreGuardarLoadOthers() {
//  Swal.fire({
//    title: '¿Do you want to save?',
//    text: "The data will be permanently stored in the system.",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, save!',
//    cancelButtonText: 'No, cancel!',
//    confirmButtonColor: '#4CAA42',
//    cancelButtonColor: '#d33'
//  }).then((result) => {
//    if (result.value) {
//      saveLoadOthers();
//    } else if (result.dismiss === Swal.DismissReason.cancel) {
//      sweet_alert_error('Canceled!', 'Saving process was canceled.')
//    }
//  });
//}

//function saveOpenToClient() {
//  var o = {}
//  o = {
//    "orderproductionID": _orderproductionID,
//    "open": $("#cmbOpenToClient").val(),
//    "commentsOBM": $("#txtcommentaryDocumentsOBM").val(),
//    "userID": $("#lblSessionUserID").text()
//  }
//  console.log(o);


//  //ajax
//  var api = "/ShippingProgram/SaveShippingProgramPO"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });


//}

//function saveLoadInvoiceGrower() {
//  var o = {}
//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId").val(),
//    "number": $("#txtInvoiceGrower").val(),
//    //"file": $("#txtInvoiceGrowerFile").val(),
//    //"file2": $("#txtInvoiceGrowerFile2").val(),
//    //"file3": $("#txtInvoiceGrowerFile3").val(),
//    "notesOBM": $("#txtInvoiceGrowerNotesOZM1").val(),
//    "enabled": $("#txtenabled").val(),
//    "statusId": $("#cmbInvoiceGrowerStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer1").val(),
//    "notesCustomer": $("#txtInvoiceGrowerNotesCustomer").val(),
//    "orderproductionID": _orderproductionID,
//    "userID": $("#lblSessionUserID").text()

//  }
//  console.log(o);

//  //ajax
//  var api = "/ShippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });


//}

//function saveLoadInvoiceOBM() {

//  if ($("#cmbInvoiceOZMStatus option:selected").val() == 1) {

//    sweet_alert_warning('Warning!', 'You can only select Approved or Correct');
//    return;
//  }

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId2").val(),
//    "number": $("#txtInvoiceOZM").val(),
//    //"file": $("#txtInvoiceOZMFile").val(),
//    //"file2": $("#txtInvoiceOZMFile2").val(),
//    //"file3": $("#txtInvoiceOZMFile3").val(),
//    "notesOBM": $("#txtInvoiceOZMNotesOZM2").val(),
//    "enabled": $("#txtenabled2").val(),
//    "statusId": $("#cmbInvoiceOZMStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer2").val(),
//    "notesCustomer": $("#txtInvoiceOZMNotesCustomer").val(),
//    "orderproductionID": _orderproductionID,
//    "userID": $("#lblSessionUserID").text()
//  }
//  console.log(o);

//  //ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });


//}

//function saveLoadPackingList() {
//  var PDF = $("#txtPLFile").val();
//  var Excel = $("#txtPLFile2").val();
//  if (PDF == "") {
//    sweet_alert_warning('Advertencia!', 'Seleccione el documento de PDF');
//    return;
//  }
//  if (Excel == "") {
//    sweet_alert_warning('Advertencia!', 'Seleccione el documento Excell');
//    return;
//  }

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId3").val(),
//    "number": $("#txtInvoiceOZM").val(),
//    //"file": $("#txtPLFile").val(),
//    //"file2": $("#txtPLFile2").val(),
//    //"file3": $("#txtPLFile3").val(),
//    "notesOBM": $("#txtPLNotesOZM").val(),
//    "enabled": $("#txtenabled3").val(),
//    "statusId": $("#cmbPLStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer3").val(),
//    "notesCustomer": $("#txtPLNotesCustomer").val(),
//    "orderproductionID": _orderproductionID,
//    "userID": $("#lblSessionUserID").text()
//  }

//  console.log(o);
//  //ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });


//}

//function saveLoadPhyto() {

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId4").val(),
//    "number": $("#txtPhyto").val(),
//    //"file": $("#txtPhytoFile").val(),
//    //"file2": $("#txtPhytoFile2").val(),
//    //"file3": $("#txtPhytoFile3").val(),
//    "notesOBM": $("#txtPhytoNotesOZM").val(),
//    "enabled": $("#txtenabled4").val(),
//    "statusId": $("#cmbPhytoStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer4").val(),
//    "notesCustomer": $("#txtPhytoNotesCustomer").val(),
//    "userID": $("#lblSessionUserID").text(),
//    "orderproductionID": _orderproductionID


//  }

//  console.log(o);
//  //ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });


//}

//function saveLoadBL() {

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId5").val(),
//    "number": $("#txtBL").val(),
//    //"file": $("#txtBLFile").val(),
//    //"file2": $("#txtBLFile2").val(),
//    //"file3": $("#txtBLFile3").val(),
//    "notesOBM": $("#txtBLNotesOZM").val(),
//    "enabled": $("#txtenabled5").val(),
//    "statusId": $("#cmbBLStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer5").val(),
//    "notesCustomer": $("#txtBLNotesCustomer").val(),
//    "userID": $("#lblSessionUserID").text(),
//    "orderproductionID": _orderproductionID
//  }
//  console.log(o);

//  //ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });
//}



//function saveLoadCertificateOfOrigin() {

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId6").val(),
//    "number": $("#txtCertOrig").val(),
//    //"file": $("#txtCertOrigFile").val(),
//    //"file2": $("#txtCertOrigFile2").val(),
//    //"file3": $("#txtCertOrigFile3").val(),
//    "notesOBM": $("#txtCertOrigNotesOZM").val(),
//    "enabled": $("#txtenabled6").val(),
//    "statusId": $("#cmbCertOrigStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer6").val(),
//    "notesCustomer": $("#txtCertOrigNotesCustomer").val(),
//    "userID": $("#lblSessionUserID").text(),
//    "orderproductionID": _orderproductionID
//  }

//  console.log(o);
//  //ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });
//}



//function saveLoadExportFile() {

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId7").val(),
//    "number": $("#txtExportFile").val(),
//    //"file": $("#txtExportFileFile").val(),
//    //"file2": $("#txtExportFileFile2").val(),
//    //"file3": $("#txtExportFileFile3").val(),
//    "notesOBM": $("#txtExportFileNotesOZM").val(),
//    "enabled": $("#enabled7").val(),
//    "statusId": $("#cmbExportFileStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer7").val(),
//    "notesCustomer": $("#txtExportFileNotesCustomer").val(),
//    "userID": $("#lblSessionUserID").text(),
//    "orderproductionID": _orderproductionID
//  }
//  console.log(o);


//  //ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });
//}


//function saveLoadOthers() {

//  o =
//  {
//    "typeDocumentID": $("#typeDocumentId8").val(),
//    "number": $("#txtOther").val(),
//    //"file": $("#txtOthersFile").val(),
//    //"file2": $("#txtOthersFile2").val(),
//    //"file3": $("#txtOthersFile3").val(),
//    "notesOBM": $("#txtOthersNotesOZM").val(),
//    //"userOBM": "Pepito",
//    //"dateModifyOBM": "2020-09-27",
//    "enabled": $("#enabled8").val(),
//    "statusId": $("#cmbOthersStatus option:selected").val(),
//    //"fileCustomer": $("#txtfileCustomer8").val(),
//    "notesCustomer": $("#txtOthersNotesCustomer").val(),
//    //"userCustomer": "Pepito",
//    //"dateCustomer": "2020-09-27"
//  }
//  console.log(o);

//  ajax
//  var api = "/shippingProgram/SaveShippingDocumentPOForCustomer"
//  console.log(api);
//  $.ajax({
//    type: 'POST',headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
//    url: api,

//    //async: false,
//    contentType: "application/json",
//    dataType: 'json',
//    //data: JSON.stringify(Object.fromEntries(frmPackingList)),
//    data: JSON.stringify(o),
//    success: function (datares) {
//      if (datares.toString().length > 0) {
//        sweet_alert_success('Saved! ',' The data has been saved.');
//        //loadListPrincipal();
//        //$('#myModal').modal('hide');
//      } else {
//        sweet_alert_error("Error "," data was NOT saved!");
//        console.log('An error occurred while recording the log')
//        console.log(api)
//      }
//    },
//    error: function (xhr, ajaxOptions, thrownError) {
//      sweet_alert_error("Error "," data was NOT saved!");
//    }
//  });
//}