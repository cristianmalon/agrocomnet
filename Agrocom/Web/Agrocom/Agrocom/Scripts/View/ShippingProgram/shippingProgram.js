﻿var _orderproductionID = 0;
var dataStatus = [];
var fileUri = 'https://storageaccountagrocom.file.core.windows.net';
var SAS_TOKEN = "?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-10-05T23:46:26Z&st=2021-10-05T15:46:26Z&spr=https&sig=yZd1HpM86A1N%2ByEBmVZyrmtqovxryJeaZ5TrH8kelYc%3D";
var SharedDirectory = 'agrocomclientdocuments';

dataStatus = [
    {
        "statudID": 0,
        "status": "Pending"
    },
    {
        "statudID": 1,
        "status": "Processing"
    },
    {
        "statudID": 2,
        "status": "Aproved"
    },
    {
        "statudID": 3,
        "status": "To correct"
    }
];

$(function () {
    if (localStorage.deniedAccess) {
        Swal.fire({
            //imageUrl: '../images/AgricolaAndrea71x64.png',
            title: 'Restriction',
            text: 'Access denied...redirecting',
            icon: 'error',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: false,
            backdrop: '#dcdcdefa',
            onOpen() {
                Swal.showLoading();
            }
        });
        setTimeout(function () {
            window.location.href = "/home";
                            }, 3000);
    }
    else {
        loadData();
        //loadDefaultValues;

        $(document).on("click", ".btnAttach", function (e) {
            //saveFile(this)
        });

        $(document).on("click", ".btnDelete", function (e) {
            dropFile(this)
        });

        $(document).on("click", ".btnDelete2", function (e) {
            dropFile2(this)
        });

        $(document).on("click", ".btnDelete3", function (e) {
            dropFile3(this)
        });

        $(document).on("click", "#btnConfirm", function (e) {
            PreGuardarOpenToClient();
        });

        $(document).on("click", "#btnSaveInvoiceGrower", function (e) {
            PreGuardarLoadInvoiceGrower(this);
        });

        $(document).on("click", "#btnSaveInvoiceOZM", function (e) {
            PreGuardarLoadInvoiceOBM(this);
        });

        $(document).on("click", "#btnSavePL", function (e) {
            PreGuardarLoadPackingList(this);
        });

        $(document).on("click", "#btnSavePhyto", function (e) {
            PreGuardarLoadPhyto(this);
        });

        $(document).on("click", "#btnSaveBL", function (e) {
            PreGuardarLoadBL(this);
        });

        $(document).on("click", "#btnSaveCertOrig", function (e) {
            PreGuardarLoadCertificateOfOrigin(this);
        });

        $(document).on("click", "#btnSaveExportFile", function (e) {
            PreGuardarLoadExportFile(this);
        });

        $(document).on("click", "#btnSaveOthers", function (e) {
            PreGuardarLoadOthers(this);
        });

        $(document).on("click", ".btnDownLoad", function (e) {
            downLoadFile(this);
        });

        $(document).on("click", ".btnDownLoad2", function (e) {
            downLoadFile2(this);
        });

        $(document).on("click", ".btnDownLoad3", function (e) {
            downLoadFile3(this);
        });

        $("select#cmbStatusRevision").change(function () {
            //console.log($(this).children("option:selected").val())
            color = setColorByStatus1($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbStatusRevision').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbStatusRevision').css("background-color", color.backgroundColor);
        })

        $("select#cmbInvoiceGrowerStatus").change(function () {

            if (parseInt($('select#cmbInvoiceGrowerStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('#cmbInvoiceGrowerStatus').val()) == 3) {
                $('#txtInvoiceGrowerFile').prop('disabled', true);
                $('#txtInvoiceGrower').prop('readonly', true);
                $('#txtInvoiceGrowerNotesOZM1').prop('readonly', true);
                $('#btnSaveInvoiceGrower').prop('disabled', true);
                $('#txtfileCustomer1').prop('disabled', true);
                $('#txtInvoiceGrowerNotesCustomer').prop('readonly', true);

                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            }

            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbInvoiceGrowerStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbInvoiceGrowerStatus').css("background-color", color.backgroundColor);

            if (parseInt($('#cmbInvoiceGrowerStatus').val()) == 2) {
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);

            }

        })

        $("select#cmbInvoiceOZMStatus").change(function () {
            if (parseInt($('select#cmbInvoiceOZMStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }
            if (parseInt($('#cmbInvoiceOZMStatus').val()) == 3) {
                $('#txtInvoiceOZMFile').prop('disabled', true);
                $('#txtInvoiceOZM').prop('readonly', true);
                $('#txtInvoiceOZMNotesOZM2').prop('readonly', true);
                $('#btnSaveInvoiceOZM').prop('disabled', true);
                $('#txtfileCustomer2').prop('disabled', true);
                $('#txtInvoiceOZMNotesCustomer').prop('readonly', true);

                $(this).closest('div.form-group').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.form-group').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.form-group').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.form-group').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.form-group').find('.btnDownLoadCustomer').prop('disabled', true);
            }

            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbInvoiceOZMStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbInvoiceOZMStatus').css("background-color", color.backgroundColor);

            if (parseInt($('#cmbInvoiceOZMStatus').val()) == 2) {
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);

            }

        })

        $("select#cmbPLStatus").change(function () {

            if (parseInt($('select#cmbPLStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('#cmbPLStatus').val()) == 3) {
                $('#txtPLFile').prop('disabled', true);
                $('#txtPLFile2').prop('disabled', true);
                $('#txtPLNotesOZM').prop('readonly', true);
                $('#btnSavePL').prop('disabled', true);
                $('#txtfileCustomer3').prop('disabled', true);
                $('#txtPLNotesCustomer').prop('readonly', true);

                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);

            }
            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbPLStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbPLStatus').css("background-color", color.backgroundColor);


            if (parseInt($('#cmbPLStatus').val()) == 2) {
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            }

        })
        $("select#cmbPhytoStatus").change(function () {

            if (parseInt($('select#cmbPhytoStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('#cmbPhytoStatus').val()) == 3) {
                $('#txtPhytoFile').prop('disabled', true);
                $('#txtPhytoNotesOZM').prop('readonly', true);
                $('#btnSavePhyto').prop('disabled', true);
                $('#txtfileCustomer4').prop('disabled', true);
                $('#txtPhytoNotesCustomer').prop('readonly', true);

                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            }

            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbPhytoStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbPhytoStatus').css("background-color", color.backgroundColor);

            if (parseInt($('#cmbPhytoStatus').val()) == 2) {
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            }

        })

        $("select#cmbBLStatus").change(function () {

            if (parseInt($('select#cmbPhytoStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('#cmbBLStatus').val()) == 3) {
                $('#txtBLFile').prop('disabled', true);
                $('#txtBLNotesOZM').prop('readonly', true);
                $('#btnSaveBL').prop('disabled', true);
                $('#txtfileCustomer5').prop('disabled', true);
                $('#txtBLNotesCustomer').prop('readonly', true);

                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            }
            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbBLStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbBLStatus').css("background-color", color.backgroundColor);


            if (parseInt($('#cmbBLStatus').val()) == 2) {
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            }

        })

        $("select#cmbCertOrigStatus").change(function () {

            if (parseInt($('select#cmbCertOrigStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('#cmbCertOrigStatus').val()) == 3) {
                $('#txtCertOrigFile').prop('disabled', true);
                $('#txtCertOrigNotesOZM').prop('readonly', true);
                $('#btnSaveCertOrig').prop('disabled', true);
                $('#txtfileCustomer6').prop('disabled', true);
                $('#txtCertOrigNotesCustomer').prop('readonly', true);

                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
            }
            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbCertOrigStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbCertOrigStatus').css("background-color", color.backgroundColor);

            if (parseInt($('#cmbCertOrigStatus').val()) == 2) {
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

        })

        $("select#cmbExportFileStatus").change(function () {

            if (parseInt($('select#cmbExportFileStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('#cmbExportFileStatus').val()) == 2) {
                $('#txtExportFileFile').prop('disabled', true);
                $('#txtExportFileNotesOZM').prop('readonly', true);
                $('#btnSaveExportFile').prop('disabled', true);
                $('#txtfileCustomer7').prop('disabled', true);
                $('#txtExportFileNotesCustomer').prop('readonly', true);
                $('#enabled7').prop('disabled', true);

                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }
            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbExportFileStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbExportFileStatus').css("background-color", color.backgroundColor);

            //if (parseInt($('#cmbExportFileStatus').val()) == 2) {
            //  $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
            //}

        })

        $("select#cmbOthersStatus").change(function () {

            if (parseInt($('select#cmbOthersStatus').val()) == 1) {
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
            }

            if (parseInt($('select#cmbOthersStatus').val()) == 2) {
                $('#txtOthersFile').prop('disabled', true);
                $('#txtOthersFile2').prop('disabled', true);
                $('#txtOthersFile3').prop('disabled', true);
                $('#txtOthersNotesOZM').prop('readonly', true);
                $('#btnSaveOthers').prop('disabled', true);
                $('#txtfileCustomer8').prop('disabled', true);
                $('#txtOthersNotesCustomer').prop('readonly', true);
                $('#enabled8').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad2').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete3').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad3').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile3').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete3').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            }
            //console.log($(this).children("option:selected").val())

            color = setColorByStatus($(this).children("option:selected").val());
            $(this).closest('div.form-group').find('#cmbOthersStatus').css("color", color.fontColor);
            $(this).closest('div.form-group').find('#cmbOthersStatus').css("background-color", color.backgroundColor);

        })

        $(document).on("change", "input#enabled7", function (e) {

            if ($(this).is(":checked") == true) {
                $('#txtExportFileFile').prop('disabled', false);
                $('#txtExportFileNotesOZM').prop('readonly', false);
                //$('#txtfileCustomer7').prop('disabled', false);
                $('#btnSaveExportFile').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', false);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);

            } else {
                $('#txtExportFileFile').prop('disabled', true);
                $('#txtExportFileNotesOZM').prop('readonly', true);
                $('#txtfileCustomer7').prop('disabled', true);
                $('#btnSaveExportFile').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
            }
        });

        $(document).on("change", "input#enabled8", function (e) {
            if ($(this).is(":checked") == true) {
                $('#txtOthersFile').prop('disabled', false);
                $('#txtOthersFile2').prop('disabled', false);
                $('#txtOthersFile3').prop('disabled', false);
                $('#txtOthersNotesOZM').prop('readonly', false);
                $('#txtfileCustomer7').prop('readonly', false);
                $('#btnSaveOthers').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', false);
                //  $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', false);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad2').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile2').prop('disabled', false);

                $(this).closest('div.container-fluid').find('.btnDelete3').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDownLoad3').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.inputFile3').prop('disabled', false);


            } else {
                $('#txtOthersFile').prop('disabled', true);
                $('#txtOthersFile2').prop('disabled', true);
                $('#txtOthersFile3').prop('disabled', true);
                $('#txtOthersNotesOZM').prop('readonly', true);
                $('#txtfileCustomer7').prop('readonly', true);
                $('#btnSaveOthers').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDelete').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoadCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', true);
                //$(this).closest('div.container-fluid').find('.btnDeleteCustomer').prop('disabled', false);
                $(this).closest('div.container-fluid').find('.btnDelete2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad2').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile2').prop('disabled', true);

                $(this).closest('div.container-fluid').find('.btnDelete3').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.btnDownLoad3').prop('disabled', true);
                $(this).closest('div.container-fluid').find('.inputFile3').prop('disabled', true);


            }
        });

    }
});

function openModal(orderProductionID) {
    $('table#tblDetails>tbody').empty();
    loadDefaultValuesSelect();
    $(myModal).show();

    getPOById(orderProductionID);

    $(".btnDeleteCustomer").hide()
    $(".lblFileCustomer").show()
    $(".btnDownLoadCustomer").show();
    $(".inputFileCustomer").hide();

    $("#txtOrderProductID").val(orderProductionID);
    //loadDetailsDocuments(orderProductionID);

    //  var allDocuments = [];
    //  OpenToClient(orderProductionID).then(function () {

    //  })

    //loadInvoiceGrower(orderProductionID);
    //loadInvoiceOBM(orderProductionID);
    //loadPackingList(orderProductionID);
    //loadPhyto(orderProductionID);
    //loadBL(orderProductionID);
    //loadCertificateOfOrigin(orderProductionID);
    //loadExportFile(orderProductionID);
    //loadOthers(orderProductionID);

}

function dropFile(xthis) {
    $(xthis).parent('div').find('.btnDelete').hide()
    $(xthis).parent('div').find('.lblFile').hide()
    $(xthis).parent('div').find('.btnDownLoad').hide()

    $(xthis).parent('div').find('.inputFile').show()
    //$(xthis).parent('div').find('.btnAttach ').show()

    $(xthis).parent('div').find('.lblFile').text('')
    $(xthis).parent('div').find('.inputFile').val('')

    $(xthis).closest('div').find('.cmbStatus').val("0").change() //pending
    //$(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending
}

function dropFile2(xthis) {
    $(xthis).parent('div').find('.btnDelete2').hide()
    $(xthis).parent('div').find('.lblFile2').hide()
    $(xthis).parent('div').find('.btnDownLoad2').hide()

    $(xthis).parent('div').find('.inputFile2').show()
    //$(xthis).parent('div').find('.btnAttach ').show()

    $(xthis).parent('div').find('.lblFile2').text('')
    $(xthis).parent('div').find('.inputFile2').val('')

    $(xthis).closest('div').find('.cmbStatus').val("0").change() //pending
    //$(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

}

function dropFile3(xthis) {
    $(xthis).parent('div').find('.btnDelete3').hide()
    $(xthis).parent('div').find('.lblFile3').hide()
    $(xthis).parent('div').find('.btnDownLoad3').hide()

    $(xthis).parent('div').find('.inputFile3').show()
    //$(xthis).parent('div').find('.btnAttach ').show()

    $(xthis).parent('div').find('.lblFile3').text('')
    $(xthis).parent('div').find('.inputFile3').val('')

    $(xthis).closest('div').find('.cmbStatus').val("0").change() //pending
    //$(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

}

function downLoadFile(xthis) {
    var label = $(xthis).closest('div.btn-group').find('.lblFile').text();
    getLink(label);
}

function downLoadFile2(xthis) {
    var label = $(xthis).closest('div.btn-group').find('.lblFile2').text();
    getLink(label);
}

function downLoadFile3(xthis) {
    var label = $(xthis).closest('div.btn-group').find('.lblFile3').text();
    getLink(label);
}

function saveFile(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function getPOById(orderProductionID) {

    var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var o = JSON.parse(data);
                _orderproductionID = o.orderProductionID
                $(txtProductionOrder).val(o.orderProduction);
                $(txtIE).val(o.packingList);
                $(txtCustomer).val(o.customer);
                $(txtContainer).val(o.container);

                $(txtOriginPort).val(o.originPort);
                $(txtPortDestination).val(o.destinationPort);
                $(txtShippingLine).val(o.shippingLine);
                $(txtVessel).val(o.vessel);

                $(txtGrower).val(o.grower);
                $(txtOrigin).val(o.origin);
                $(txtBl).val(o.bl);
                $(txtResponsable).val(o.responsable);

                $(txtIncoterm).val(o.incoterm);
                $(txtSaleTerm).val(o.saleTerm);
                $(txtVia).val(o.via);
                $(txtEtd).val(o.etd);
                $(txtEta).val(o.eta);
                //$(txtEtaReal).val(o.etaReal);                

                var grossWeight = 0;
                var content = "";
                $.each(o.details, function (index, row) {
                    grossWeight = row.grossWeight;
                    content += "    <tr style='font-size: 14px'>";
                    content += "      <td id='brand'>" + row.brand + "</td>";
                    content += "      <td id='variety' >" + row.variety + " </td>";
                    content += "      <td id='codepack'>" + row.codepack + "</td>";
                    content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
                    content += "      <td id='boxes'>" + row.boxes + "</td>";
                    content += "      <td id='pallets'>" + row.pallets + "</td>";
                    content += "      <td id='netWeight'>" + row.netWeight + "</td>";
                    content += "    </tr>";
                });
                $("#tblDetails>tbody").empty().append(content);
                //Calcular el footer
                var boxes = 0;
                var pallets = 0;
                var netWeight = 0;
                $('#tblDetails>tbody>tr').each(function (j, row) {
                    boxes = boxes + parseFloat($(row).find('#boxes').text());
                    pallets = pallets + parseFloat($(row).find('#pallets').text());
                    netWeight = netWeight + parseFloat($(row).find('#netWeight').text());
                });
                //Insertar los valores del footer
                contentfoot = "";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
                contentfoot += '<td>' + (boxes).toFixed(0) + '</td>';
                contentfoot += '<td>' + (pallets).toFixed(2) + '</td>';
                contentfoot += '<td>' + (netWeight).toFixed(2) + '</td>';
                contentfoot += "</tr>";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Gross Weight";
                contentfoot += '<td>' + grossWeight + '</td>';
                contentfoot += '<td></td>';
                contentfoot += '<td></td>';
                contentfoot += "</tr>";
                $("#tblDetails>tfoot").empty().append(contentfoot);

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            //$.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },
        complete: loadAllDocuments(orderProductionID)
    });

}

function loadDetailsDocuments(orderProductionID) {

    var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 0) {
                var o = JSON.parse(data);
                //Creando el formato de la tabla
                var content = "";
                //Recorriendo la lista para llenar el detalle de la tabla
                var grossWeight = 0;
                $.each(o.details, function (index, row) {
                    grossWeight = row.grossWeight;
                    content += "    <tr style='font-size: 14px'>";
                    content += "      <td id='brand'>" + row.brand + "</td>";
                    content += "      <td id='variety' >" + row.variety + " </td>";
                    content += "      <td id='codepack'>" + row.codepack + "</td>";
                    content += "      <td id='sizeInfo'>" + row.sizeInfo + "</td>";
                    content += "      <td id='boxes'>" + row.boxes + "</td>";
                    content += "      <td id='pallets'>" + row.pallets + "</td>";
                    content += "      <td id='netWeight'>" + row.netWeight + "</td>";
                    content += "    </tr>";
                });

                $("#tblDetails>tbody").empty().append(content);
                //Calcular el footer
                var boxes = 0;
                var pallets = 0;
                var netWeight = 0;
                $('#tblDetails>tbody>tr').each(function (j, row) {
                    boxes = boxes + parseFloat($(row).find('#boxes').text());
                    pallets = pallets + parseFloat($(row).find('#pallets').text());
                    netWeight = netWeight + parseFloat($(row).find('#netWeight').text());
                });
                //Insertar los valores del footer
                contentfoot = "";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Total";
                contentfoot += '<td>' + (boxes).toFixed(0) + '</td>';
                contentfoot += '<td>' + (pallets).toFixed(2) + '</td>';
                contentfoot += '<td>' + (netWeight).toFixed(2) + '</td>';
                contentfoot += "</tr>";
                contentfoot += "<tr style='font-size: 14px'>";
                contentfoot += "<td colspan=4  style='text-align:right; background-color: #A3A6AD; color:white'>Gross Weight";
                contentfoot += '<td>' + grossWeight + '</td>';
                contentfoot += '<td></td>';
                contentfoot += '<td></td>';
                contentfoot += "</tr>";
                $("#tblDetails>tfoot").empty().append(contentfoot);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });

}

function loadAllDocuments(orderProductionID) {
    var alldocuments = []
    var api = "/ShippingProgram/GeDocumentsByPOID?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            console.log(data);
            if (data.length > 0) {
                alldocuments = JSON.parse(data);
                if (alldocuments.length == 0) {
                    return;
                }
                loadInvoiceGrower(alldocuments);
                loadInvoiceOBM(alldocuments);
                loadPackingList(alldocuments);
                loadPhyto(alldocuments);
                loadBL(alldocuments);
                loadCertificateOfOrigin(alldocuments);
                loadExportFile(alldocuments);
                loadOthers(alldocuments);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        },
        complete: function () {
            return alldocuments;
        }
    });

}

function OpenToClient(orderProductionID) {
    var api = "/ShippingProgram/GetDetailPO?orderProductionID=" + orderProductionID;
    $.ajax({
        type: "GET",
        url: api,
        async: false,
        success: function (data) {
            if (data.length > 0) {
                var o = JSON.parse(data);
                $("#cmbOpenToClient").val(o.open);
                $("#txtcommentaryDocumentsOBM").val(o.commentaryDocumentsOBM);
                $("#txtcommentaryDocumentsCustomer").val(o.commentaryDocumentsCustomer);
                $("#cmbStatusRevision").val(o.statusRevision).change();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problemas con el cargar el listado!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });
}

function setColorByStatus1(statusID) {
    var color = { backgroundColor: "a", fontColor: "b" }
    var o = {}
    switch (parseInt(statusID)) {

        case 0://incomplete
            o.backgroundColor = '#FBFF00'; //amarillo
            o.fontColor = '#000000'; //negro
            break;
        case 1: //complete
            o.backgroundColor = '#09AB01'; //blanco
            o.fontColor = '#FFFFFF'; //negro
            break;
    }
    return o;
}

function setColorByStatus(statusID) {
    var color = { backgroundColor: "a", fontColor: "b" }
    var o = {}
    switch (parseInt(statusID)) {
        case 0: //pending
            o.backgroundColor = '#000000'; //blanco
            o.fontColor = '#FFFFFF'; //negro
            break;
        case 1://processing
            o.backgroundColor = '#FBFF00'; //amarillo
            o.fontColor = '#000000'; //negro
            break;
        case 2://appoved
            o.backgroundColor = '#09AB01' //verde
            o.fontColor = '#FFFFFF'; //blanco
            break;
        case 3://rejected
            o.backgroundColor = '#FF0000' //rojo
            o.fontColor = '#FFFFFF'; //blanco
            break;
    }
    return o;
}

function loadData() {
    var api = "/ShippingProgram/GetAllPO?campaignID=" + localStorage.campaignID;
    $.ajax({
        type: "GET",
        url: api,
        //async: false,
        success: function (data) {
            if (data.length > 2) {
                var list = JSON.parse(data);
                $("table#tableList>tbody").empty();
                $.each(list, function (i, item) {
                    var tr = ''
                    tr += '<tr  style="vertical-align:middle; font-size:13px">'
                    tr += '<td style="display:none">' + item.orderProductionID + '</td>'
                    tr += '<td > <button class="" style="padding: 0 0 0 0!important; background: none; border: none;outline:none !important" onclick="openModal(' + item.orderProductionID + ')" data-toggle="modal" data-target="#myModal"><i class="fas fa-edit fa-md text-primary"></i></button> </td>'
                    tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.orderProduction + '</td>'
                    tr += '<td style="min-width:150px;max-width:150px;vertical-align:middle">' + item.container + '</td>'
                    tr += '<td style="min-width:200px;max-width:200px;vertical-align:middle">' + item.customer + '</td>'
                    tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle;display:none">' + item.grower + '</td>'
                    tr += '<td style="min-width:150px;max-width:150px;vertical-align:middle">' + item.packingList + '</td>'
                    tr += '<td style="min-width:180px;max-width:180px;vertical-align:middle">' + item.shippingLine + '</td>'
                    tr += '<td style="min-width:100px;max-width:100px;vertical-align:middle">' + item.destinationPort + '</td>'
                    tr += '<td style="min-width:30px;max-width:30px;vertical-align:middle">' + item.via + '</td>'
                    tr += '<td style="min-width:80px;max-width:80px;vertical-align:middle">' + item.etd + '</td>'
                    tr += '<td style="min-width:80px;max-width:80px;vertical-align:middle">' + item.eta + '</td>'
                    tr += '<td style="min-width:120px;max-width:120px;vertical-align:middle">' + item.responsable + '</td>'
                    var color = {}
                    color = setColorByStatus(item.invoiceGrowerStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:80px;max-width:80px;">' + item.invoiceGrowerStatus + '</td>'
                    color = setColorByStatus(item.invoiceCustomerStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:80px;max-width:80px;">' + item.invoiceCustomerStatus + '</td>'
                    color = setColorByStatus(item.packingListStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.packingListStatus + '</td>'
                    color = setColorByStatus(item.phytoStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.phytoStatus + '</td>'
                    color = setColorByStatus(item.blStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.blStatus + '</td>'
                    color = setColorByStatus(item.certificateOriginStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + '">' + item.certificateOriginStatus + '</td>'
                    color = setColorByStatus(item.exportFileStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:95px;max-width:95px;">' + item.exportFileStatus + '</td>'
                    color = setColorByStatus(item.othersStatusID)
                    tr += '<td style="color:' + color.fontColor + '; background-color:' + color.backgroundColor + ';min-width:80px;max-width:80px;">' + item.othersStatus + '</td>'
                    tr += '</tr>'
                    $("table#tableList>tbody").append(tr);
                })

                $(tableList).dataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                        'pageLength'
                    ],
                    responsive: true,
                    lengthMenu: [10, 25, 50, 75, 100],
                    destroy: true
                    //buttons: [
                    //    'copy', 'csv', 'excel', 'pdf', 'print'
                    //]
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error('Information', 'There has been a problem when consulting the information. Try again.');
            //console.log(api)
            //console.log(xhr);
            //console.log(ajaxOptions);
            //console.log(thrownError);
        }
    });
    
}

function loadDefaultValuesSelect() {

    $('#cmbStatusRevision').val(0).change();

    var select = $('#cmbInvoiceGrowerStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbInvoiceGrowerStatus').val(0).change();

    var select = $('#cmbInvoiceOZMStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbInvoiceOZMStatus').val(0).change();

    var select = $('#cmbPLStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbPLStatus').val(0).change();

    var select = $('#cmbPhytoStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbPhytoStatus').val(0).change();

    var select = $('#cmbBLStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbBLStatus').val(0).change();

    var select = $('#cmbCertOrigStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbCertOrigStatus').val(0).change();

    var select = $('#cmbExportFileStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbExportFileStatus').val(0).change();

    var select = $('#cmbOthersStatus');

    var content = ""; $.each(dataStatus, function (i, e) {
        if (e.statudID == 0) { content += '<option style="background-color:black;color:white" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 1) { content += '<option style="background-color:#FBFF00;color:#000000" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 2) { content += '<option style="background-color:#09AB01;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
        if (e.statudID == 3) { content += '<option style="background-color:#FF0000;color:#FFFFFF" value="' + e.statudID + '">' + e.status + '</option>' };
    })

    select.empty().append(content);
    $('#cmbOthersStatus').val(0).change();

}

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value='0'>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadInvoiceGrower(documents) {

    docInvoiceGrower = documents.filter(item => item.typeDocumentID == 1)[0]

    $("#typeDocumentId").val(docInvoiceGrower.typeDocumentID);
    $("#txtenabled").val(docInvoiceGrower.enabled);

    $(lblInvoiceGrowerFile).text(docInvoiceGrower.file)
    $(txtInvoiceGrowerFile).text("");
    $("#txtInvoiceGrower").val(docInvoiceGrower.number);
    $("#txtInvoiceGrowerNotesOZM1").val(docInvoiceGrower.notesOBM);

    $("#cmbInvoiceGrowerStatus").val(docInvoiceGrower.statusID).change();
    $("#lblfileCustomer1").text(docInvoiceGrower.fileCustomer);
    $("#txtInvoiceGrowerNotesCustomer").val(docInvoiceGrower.notesCustomer);

    if (docInvoiceGrower.file.length > 2) {
        HideUpload($(lblInvoiceGrowerFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblInvoiceGrowerFile).closest('div.container-fluid'));
    }
    if (docInvoiceGrower.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer1).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer1).closest('div.container-fluid'));
    }

}

function HideUpload(xthis) {
    $(xthis).find('.btnDelete').show();
    $(xthis).find('.lblFile').show();
    $(xthis).find('.btnDownLoad').show();

    $(xthis).find('.inputFile').hide();

    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}
function ShowUpload(xthis) {
    $(xthis).find('.btnDelete').hide();
    $(xthis).find('.lblFile').hide();
    $(xthis).find('.btnDownLoad').hide();

    $(xthis).find('.inputFile').show();
}

function HideUploadCustomer(xthis) {
    $(xthis).find('.btnDeleteCustomer').show();
    $(xthis).find('.lblFileCustomer').show();
    $(xthis).find('.btnDownLoadCustomer').show();

    $(xthis).find('.inputFileCustomer').hide();

    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}
function ShowUploadCustomer(xthis) {
    $(xthis).find('.btnDeleteCustomer').hide();
    $(xthis).find('.lblFileCustomer').hide();
    $(xthis).find('.btnDownLoadCustomer').hide();

    $(xthis).find('.inputFileCustomer').show();
}

function HideUploadPL(xthis) {

    $(xthis).find('.btnDelete2').show();
    $(xthis).find('.lblFile2').show();
    $(xthis).find('.btnDownLoad2').show();
    $(xthis).find('.inputFile2').hide();

    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}
function ShowUploadPL(xthis) {



    $(xthis).find('.btnDelete2').hide();
    $(xthis).find('.lblFile2').hide();
    $(xthis).find('.btnDownLoad2').hide();
    $(xthis).find('.inputFile2').show();
}

function HideUploadOthers(xthis) {

    $(xthis).find('.btnDelete').show();
    $(xthis).find('.lblFile').show();
    $(xthis).find('.btnDownLoad').show();
    $(xthis).find('.inputFile').hide();



    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}
function ShowUploadOthers(xthis) {
    $(xthis).find('.btnDelete').hide();
    $(xthis).find('.lblFile').hide();
    $(xthis).find('.btnDownLoad').hide();
    $(xthis).find('.inputFile').show();


}
function HideUploadOthers2(xthis) {



    $(xthis).find('.btnDelete2').show();
    $(xthis).find('.lblFile2').show();
    $(xthis).find('.btnDownLoad2').show();
    $(xthis).find('.inputFile2').hide();



    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}
function ShowUploadOthers2(xthis) {


    $(xthis).find('.btnDelete2').hide();
    $(xthis).find('.lblFile2').hide();
    $(xthis).find('.btnDownLoad2').hide();
    $(xthis).find('.inputFile2').show();

    $(xthis).find('.btnDelete3').hide();
    $(xthis).find('.lblFile3').hide();
    $(xthis).find('.btnDownLoad3').hide();
    $(xthis).find('.inputFile3').show();
}

function HideUploadOthers3(xthis) {



    $(xthis).find('.btnDelete3').show();
    $(xthis).find('.lblFile3').show();
    $(xthis).find('.btnDownLoad3').show();
    $(xthis).find('.inputFile3').hide();



    //$(".btnDelete").hide()
    //$(".lblFile").hide()
    //$(".btnDownLoad").hide();
}
function ShowUploadOthers3(xthis) {


    $(xthis).find('.btnDelete3').hide();
    $(xthis).find('.lblFile3').hide();
    $(xthis).find('.btnDownLoad3').hide();
    $(xthis).find('.inputFile3').show();
}

function loadInvoiceOBM(documents) {

    docInvoiceOBM = documents.filter(item => item.typeDocumentID == 2)[0]

    $("#typeDocumentId2").val(docInvoiceOBM.typeDocumentID);
    $("#txtenabled2").val(docInvoiceOBM.enabled);

    $(lblInvoiceOZMFile).text(docInvoiceOBM.file);
    $(txtInvoiceOZMFile).val("");
    $("#txtInvoiceOZM").val(docInvoiceOBM.number);
    $("#txtInvoiceOZMNotesOZM2").val(docInvoiceOBM.notesOBM);

    $("#cmbInvoiceOZMStatus").val(docInvoiceOBM.statusID).change();
    $(lblfileCustomer2).text(docInvoiceOBM.fileCustomer);
    $("#txtInvoiceOZMNotesCustomer").val(docInvoiceOBM.notesCustomer);

    if (docInvoiceOBM.file.length > 2) {
        HideUpload($(lblInvoiceOZMFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblInvoiceOZMFile).closest('div.container-fluid'));
    }

    if (docInvoiceOBM.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer2).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer2).closest('div.container-fluid'));
    }

}

function loadPackingList(documents) {

    docPackingList = documents.filter(item => item.typeDocumentID == 3)[0]

    $("#typeDocumentId3").val(docPackingList.typeDocumentID);
    $("#txtenabled3").val(docPackingList.enabled);
    $("#txtPL").val(docPackingList.number);

    $("#lblPLFile").text(docPackingList.file);
    $("#lblPLFile2").text(docPackingList.file2);
    $("#txtPLFile").val('');
    $("#txtPLFile2").val('');
    $("#txtPLNotesOZM").val(docPackingList.notesOBM);

    $("#cmbPLStatus").val(docPackingList.statusID).change();
    $("#txtPLNotesCustomer").val(docPackingList.notesCustomer);
    $("#lblfileCustomer3").text(docPackingList.fileCustomer);



    if (docPackingList.file.length > 2) {
        HideUpload($(lblPLFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblPLFile).closest('div.container-fluid'));
    }

    if (docPackingList.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer3).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer3).closest('div.container-fluid'));
    }

    if (docPackingList.file2.length > 2) {
        HideUploadPL($(lblPLFile2).closest('div.container-fluid'));
    } else {
        ShowUploadPL($(lblPLFile2).closest('div.container-fluid'));
    }
}

function loadPhyto(documents) {

    docPhyto = documents.filter(item => item.typeDocumentID == 4)[0]

    $("#typeDocumentId4").val(docPhyto.typeDocumentID);
    $("#txtenabled4").val(docPhyto.enabled);
    $("#txtPhyto").val(docPhyto.number);

    $(lblPhytoFile).text(docPhyto.file);
    $(txtPhytoFile).val('');
    $("#txtPhytoNotesOZM").val(docPhyto.notesOBM);


    $("#cmbPhytoStatus").val(docPhyto.statusID).change();
    $(lblfileCustomer4).text(docPhyto.fileCustomer);
    $("#txtPhytoNotesCustomer").val(docPhyto.notesCustomer);
    //$("#txtPhytoFile").val(docPhyto.file);

    //$("#txtPhytoFile2").val(docPhyto.file2);
    //$("#txtPhytoFile3").val(docPhyto.file3);




    if (docPhyto.file.length > 2) {
        HideUpload($(lblPhytoFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblPhytoFile).closest('div.container-fluid'));
    }
    if (docPhyto.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer4).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer4).closest('div.container-fluid'));
    }

}

function loadBL(documents) {

    docBL = documents.filter(item => item.typeDocumentID == 5)[0]
    $("#typeDocumentId5").val(docBL.typeDocumentID);
    $("#txtBL").val(docBL.number);
    $("#txtenabled5").val(docBL.enabled);

    $(lblBLFile).text(docBL.file);
    $(txtBLFile).val('');
    $("#txtBLNotesOZM").val(docBL.notesOBM);

    $("#cmbBLStatus").val(docBL.statusID).change();
    $("#txtBLNotesCustomer").val(docBL.notesCustomer);
    $(lblfileCustomer5).text(docBL.fileCustomer);

    if (docBL.file.length > 2) {
        HideUpload($(lblBLFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblBLFile).closest('div.container-fluid'));
    }

    if (docBL.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer5).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer5).closest('div.container-fluid'));
    }
}

function loadCertificateOfOrigin(documents) {

    docCertificateOfOrigin = documents.filter(item => item.typeDocumentID == 6)[0]
    $("#typeDocumentId6").val(docCertificateOfOrigin.typeDocumentID);
    $("#txtCertOrig").val(docCertificateOfOrigin.number);
    $("#txtenabled6").val(docCertificateOfOrigin.enabled);

    $(lblCertOrigFile).text(docCertificateOfOrigin.file);
    $(txtCertOrigFile).val('');
    $("#txtCertOrigNotesOZM").val(docCertificateOfOrigin.notesOBM);

    $("#cmbCertOrigStatus").val(docCertificateOfOrigin.statusID).change();
    $("#txtCertOrigNotesCustomer").val(docCertificateOfOrigin.notesCustomer);
    $("#lblfileCustomer6").text(docCertificateOfOrigin.fileCustomer);

    if (docCertificateOfOrigin.file.length > 2) {
        HideUpload($(lblCertOrigFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblCertOrigFile).closest('div.container-fluid'));
    }

    if (docCertificateOfOrigin.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer6).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer6).closest('div.container-fluid'));
    }
}

function loadExportFile(documents) {

    docExportFile = documents.filter(item => item.typeDocumentID == 7)[0]
    $("#typeDocumentId7").val(docExportFile.typeDocumentID);
    $("#txtExportFile").val(docExportFile.number);
    $("#enabled7").val(docExportFile.enabled);

    $(lblExportFileFile).text(docExportFile.file);
    $(txtExportFileFile).val('');
    $("#txtExportFileNotesOZM").val(docExportFile.notesOBM);

    $("#cmbExportFileStatus").val(docExportFile.statusID).change();
    $("#txtExportFileNotesCustomer").val(docExportFile.notesCustomer);
    $(lblfileCustomer7).text(docExportFile.fileCustomer)


    if (docExportFile.file.length > 2) {
        HideUpload($(lblExportFileFile).closest('div.container-fluid'));
    } else {
        ShowUpload($(lblExportFileFile).closest('div.container-fluid'));
    }
    if (docExportFile.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer7).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer7).closest('div.container-fluid'));
    }
}
function loadOthers(documents) {

    docOthers = documents.filter(item => item.typeDocumentID == 8)[0]

    $("#typeDocumentId8").val(docOthers.typeDocumentID);
    $("#txtOther").val(docOthers.number);
    $("#enabled8").val(docOthers.enabled);

    $(lblOthersFile).text(docOthers.file);
    $(lblOthersFile2).text(docOthers.file2);
    $(lblOthersFile3).text(docOthers.file3);
    $("#txtOthersFile").val('');
    $("#txtOthersFile2").val('');
    $("#txtOthersFile3").val('');
    $("#txtOthersNotesOZM").val(docOthers.notesOBM);

    $("#cmbOthersStatus").val(docOthers.statusID).change();
    $("#lblfileCustomer8").text(docOthers.fileCustomer);
    $("#txtOthersNotesCustomer").val(docOthers.notesCustomer);



    if (docOthers.file.length > 2) {
        HideUploadOthers($(lblOthersFile).closest('div.container-fluid'));
    } else {
        ShowUploadOthers($(lblOthersFile).closest('div.container-fluid'));
    }

    if (docOthers.file2.length > 2) {
        HideUploadOthers2($(lblOthersFile2).closest('div.container-fluid'));
    } else {
        ShowUploadOthers2($(lblOthersFile2).closest('div.container-fluid'));
    }

    if (docOthers.file3.length > 2) {
        HideUploadOthers3($(lblOthersFile3).closest('div.container-fluid'));
    } else {
        ShowUploadOthers3($(lblOthersFile3).closest('div.container-fluid'));
    }


    if (docOthers.fileCustomer.length > 2) {
        HideUploadCustomer($(lblfileCustomer8).closest('div.container-fluid'));
    } else {
        ShowUploadCustomer($(lblfileCustomer8).closest('div.container-fluid'));
    }

}

function PreGuardarOpenToClient() {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveOpenToClient();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadInvoiceGrower(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadInvoiceGrower(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadInvoiceOBM(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadInvoiceOBM(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadPackingList(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadPackingList(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadPhyto(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadPhyto(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadBL(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadBL(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadCertificateOfOrigin(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadCertificateOfOrigin(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadExportFile(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadExportFile(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}
function PreGuardarLoadOthers(xthis) {
    Swal.fire({
        title: '¿Do you want to save?',
        text: "The data will be permanently stored in the system.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, save!',
        cancelButtonText: 'No, cancel!',
        confirmButtonColor: '#4CAA42',
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            saveLoadOthers(xthis);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            sweet_alert_error('Canceled!', 'Saving process was canceled.')
        }
    });
}

function saveOpenToClient() {
    var o = {}
    o = {
        "orderproductionID": _orderproductionID,
        "open": $("#cmbOpenToClient").val(),
        "commentsOBM": $("#txtcommentaryDocumentsOBM").val(),
        "userID": $("#lblSessionUserID").text()
    }
    
    var api = "/ShippingProgram/SaveShippingProgramPO"
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");               
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });
}

function saveLoadInvoiceGrower(xthis) {

    var file1 = "";
    //if ($("#txtInvoiceGrowerFile").val().length == 0) {
    //    return;
    //}
    $("#txtInvoiceGrower").val($("#txtInvoiceGrowerFile").val());
    sExtension = getFileExtension("txtInvoiceGrowerFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId").val(), 1, 'OBM') + '.' + sExtension;
    if (!UploadFilesOnAzure("txtInvoiceGrowerFile", file1)) {
        //return;
    }
    $("#lblInvoiceGrowerFile").text(file1);

    saveFileInvoiceGrower(xthis);

    var o = {}
    o =
    {
        "typeDocumentID": $("#typeDocumentId").val(),
        "number": $("#txtInvoiceGrower").val(),
        "file": file1,
        "notesOBM": $("#txtInvoiceGrowerNotesOZM1").val(),
        "enabled": 1,   //$("#txtenabled").val(),
        "statusId": $("#cmbInvoiceGrowerStatus option:selected").val(),
        "notesCustomer": $("#txtInvoiceGrowerNotesCustomer").val(),
        "orderproductionID": _orderproductionID,
        "userID": $("#lblSessionUserID").text()

    }
    console.log(o);

    var api = "/ShippingProgram/SaveShippingDocumentPO"
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved!', ' The data has been saved.');                
            } else {
                sweet_alert_error("Error", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });

}

function dropFileInvoiceGrower(xthis) {
    $(xthis).parent('div').find('.btnDelete').hide()
    $(xthis).parent('div').find('.lblFile').hide()
    $(xthis).parent('div').find('.btnDownLoad').hide()

    $(xthis).parent('div').find('.inputFile').show()
    //$(xthis).parent('div').find('.btnAttach ').show()

    $(xthis).parent('div').find('.lblFile').text('')
    $(xthis).parent('div').find('.inputFile').val('')

    $(xthis).closest('div').find('.cmbStatus').val("0").change() //pending
    //$(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending


}

function downLoadFileInvoiceGrower(xthis) {
    var label = $(xthis).closest('div.btn-group').find('.lblFile').text();
    getLink(label);
}

function saveFileInvoiceGrower(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()
        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()
        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function saveLoadInvoiceOBM(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    //if ($("#txtInvoiceOZMFile").val().length == 0) {
    //    return;
    //}
    sExtension = getFileExtension("txtInvoiceOZMFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId2").val(), 1, 'OBM') + '.' + sExtension;
    if (!UploadFilesOnAzure("txtInvoiceOZMFile", file1)) {
        /*return;*/
    }
    $("#lblInvoiceOZMFile").text(file1);

    saveFileInvoiceOBM(xthis);

    o =
    {
        "typeDocumentID": $("#typeDocumentId2").val(),
        "number": $("#txtInvoiceOZM").val(),
        "file": file1,//$("#txtInvoiceOZMFile").val(),
        //"file2": $("#txtInvoiceOZMFile2").val(),
        //"file3": $("#txtInvoiceOZMFile3").val(),
        "notesOBM": $("#txtInvoiceOZMNotesOZM2").val(),
        "enabled": 1,   //$("#txtenabled2").val(),
        "statusId": $("#cmbInvoiceOZMStatus option:selected").val(),
        //"fileCustomer": $("#txtfileCustomer2").val(),
        "notesCustomer": $("#txtInvoiceOZMNotesCustomer").val(),
        "orderproductionID": _orderproductionID,
        "userID": $("#lblSessionUserID").text()
    }
    console.log(o);

    var api = "/shippingProgram/SaveShippingDocumentPO"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });


}


function saveFileInvoiceOBM(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function saveLoadPackingList(xthis) {

    var PDF = $("#txtPLFile").val();
    var Excel = $("#txtPLFile2").val();
    if (PDF != "" || Excel != "") {
        var file1 = "";
        var file2 = "";
        var file3 = "";
        //if ($("#txtPLFile").val().length > 0) {
        sExtension = getFileExtension("txtPLFile");
        if (sExtension == '') {
            return;
        }
        file1 = setNameFileAttach($("#typeDocumentId3").val(), 1, 'OBM') + '.' + sExtension;
        UploadFilesOnAzure("txtPLFile", file1);
        $("#lblPLFile").text(file1);
        //}

        //if ($("#txtPLFile2").val().length > 0) {
        sExtension = getFileExtension("txtPLFile2");
        if (sExtension == '') {
            return;
        }
        file2 = setNameFileAttach($("#typeDocumentId3").val(), 2, 'OBM') + '.' + sExtension;
        UploadFilesOnAzure("txtPLFile2", file2);
        $("#lblPLFile2").text(file2);
        //}
        
        if (PDF == "") {
            sweet_alert_warning('Advertencia!', 'Seleccione el documento de PDF');            
        }
        if (Excel == "") {
            sweet_alert_warning('Advertencia!', 'Seleccione el documento Excell');            
        }

    }

    saveFilePackingList(xthis);

    o =
    {
        "typeDocumentID": $("#typeDocumentId3").val(),
        "number": $("#txtInvoiceOZM").val(),
        "file": file1,//$("#txtPLFile").val(),
        "file2": file2,//$("#txtPLFile2").val(),
        //"file3": $("#txtPLFile3").val(),
        "notesOBM": $("#txtPLNotesOZM").val(),
        "enabled": $("#txtenabled3").val(),
        "statusId": $("#cmbPLStatus option:selected").val(),
        //"fileCustomer": $("#txtfileCustomer3").val(),
        "notesCustomer": $("#txtPLNotesCustomer").val(),
        "orderproductionID": _orderproductionID,
        "userID": $("#lblSessionUserID").text()
    }

    console.log(o);
    
    var api = "/shippingProgram/SaveShippingDocumentPO"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
        }
    });


}


function saveFilePackingList(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }
    if ($(xthis).closest('div.container-fluid').find('.lblFile2').text().length >= 1 || $(xthis).closest('div.container-fluid').find('.inputFile2').val().length > 2) {
        $(xthis).closest('div.container-fluid').find('.cmbStatus').val("1").change()
        $(xthis).closest('div.container-fluid').find('.btnDelete2').show()
        $(xthis).closest('div.container-fluid').find('.lblFile2').show()
        $(xthis).closest('div.container-fluid').find('.btnDownLoad2').show()

        $(xthis).closest('div.container-fluid').find('.inputFile2').hide()
    }
    else {


        $(xthis).closest('div.container-fluid').find('.btnDelete2').hide()
        $(xthis).closest('div.container-fluid').find('.lblFile2').hide()
        $(xthis).closest('div.container-fluid').find('.btnDownLoad2').hide()

        $(xthis).closest('div.container-fluid').find('.inputFile2').show()
    }
}

function saveLoadPhyto(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    //if ($("#txtPhytoFile").val().length == 0) {
    //    return;
    //}
    sExtension = getFileExtension("txtPhytoFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId4").val(), 1, 'OBM') + '.' + sExtension;
    if (!UploadFilesOnAzure("txtPhytoFile", file1)) {
        //return;
    }
    $("#lblPhytoFile").text(file1);

    saveFilePhyto(xthis);

    o =
    {
        "typeDocumentID": $("#typeDocumentId4").val(),
        "number": $("#txtPhyto").val(),
        "file": file1,//$("#txtPhytoFile").val(),
        //"file2": $("#txtPhytoFile2").val(),
        //"file3": $("#txtPhytoFile3").val(),
        "notesOBM": $("#txtPhytoNotesOZM").val(),
        "enabled": 1,   //$("#txtenabled4").val(),
        "statusId": $("#cmbPhytoStatus option:selected").val(),
        //"fileCustomer": $("#txtfileCustomer4").val(),
        "notesCustomer": $("#txtPhytoNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }

    console.log(o);
    
    var api = "/shippingProgram/SaveShippingDocumentPO"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });


}

function saveFilePhyto(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function saveLoadBL(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    //if ($("#txtBLFile").val().length == 0) {
    //    return;
    //}
    sExtension = getFileExtension("txtBLFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId5").val(), 1, 'OBM') + '.' + sExtension;
    if (!UploadFilesOnAzure("txtBLFile", file1)) {
        //return;
    }

    $("#lblBLFile").text(file1);

    saveFileBL(xthis);

    o =
    {
        "typeDocumentID": $("#typeDocumentId5").val(),
        "number": $("#txtBL").val(),
        "file": file1,//$("#txtBLFile").val(),
        //"file2": $("#txtBLFile2").val(),
        //"file3": $("#txtBLFile3").val(),
        "notesOBM": $("#txtBLNotesOZM").val(),
        "enabled": 1,   //$("#txtenabled5").val(),
        "statusId": $("#cmbBLStatus option:selected").val(),
        //"fileCustomer": $("#txtfileCustomer5").val(),
        "notesCustomer": $("#txtBLNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }
    console.log(o);

    var api = "/shippingProgram/SaveShippingDocumentPO"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });
}
function saveFileBL(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function saveLoadCertificateOfOrigin(xthis) {

    var file1 = "";
    var file2 = "";
    var file3 = "";
    //if ($("#txtCertOrigFile").val().length == 0) {
    //    return;
    //}
    sExtension = getFileExtension("txtCertOrigFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId6").val(), 1, 'OBM') + '.' + sExtension;
    if (!UploadFilesOnAzure("txtCertOrigFile", file1)) {
        //return;
    }
    $("#lblCertOrigFile").text(file1);

    saveFileCertificateOfOrigin(xthis);
    o =
    {
        "typeDocumentID": $("#typeDocumentId6").val(),
        "number": $("#txtCertOrig").val(),
        "file": file1,//$("#txtCertOrigFile").val(),
        //"file2": $("#txtCertOrigFile2").val(),
        //"file3": $("#txtCertOrigFile3").val(),
        "notesOBM": $("#txtCertOrigNotesOZM").val(),
        "enabled": 1,   //$("#txtenabled6").val(),
        "statusId": $("#cmbCertOrigStatus option:selected").val(),
        //"fileCustomer": $("#txtfileCustomer6").val(),
        "notesCustomer": $("#txtCertOrigNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }

    console.log(o);
    
    var api = "/shippingProgram/SaveShippingDocumentPO"
    
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });
}
function saveFileCertificateOfOrigin(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function saveLoadExportFile(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";
    //if ($("#txtExportFileFile").val().length == 0) {
    //    return;
    //}
    sExtension = getFileExtension("txtExportFileFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId7").val(), 1, 'OBM') + '.' + sExtension;
    if (!UploadFilesOnAzure("txtExportFileFile", file1)) {
        //return;
    }
    $("#lblExportFileFile").text(file1);

    saveFileExportFile(xthis)

    o =
    {
        "typeDocumentID": $("#typeDocumentId7").val(),
        "number": $("#txtExportFile").val(),
        "file": file1,//$("#txtExportFileFile").val(),
        //"file2": $("#txtExportFileFile2").val(),
        //"file3": $("#txtExportFileFile3").val(),
        "notesOBM": $("#txtExportFileNotesOZM").val(),
        "enabled": $("#enabled7").val(),
        "statusId": $("#cmbExportFileStatus option:selected").val(),
        //"fileCustomer": $("#txtfileCustomer7").val(),
        "notesCustomer": $("#txtExportFileNotesCustomer").val(),
        "userID": $("#lblSessionUserID").text(),
        "orderproductionID": _orderproductionID
    }
    console.log(o);

    var api = "/shippingProgram/SaveShippingDocumentPO"
    
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });
}
function saveFileExportFile(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }

}

function saveLoadOthers(xthis) {
    var file1 = "";
    var file2 = "";
    var file3 = "";

    //if ($("#txtOthersFile").val().length > 0) {
    sExtension = getFileExtension("txtOthersFile");
    if (sExtension == '') {
        return;
    }
    file1 = setNameFileAttach($("#typeDocumentId8").val(), 1, 'OBM') + '.' + sExtension;
    UploadFilesOnAzure("txtOthersFile", file1);
    $("#lblOthersFile").text(file1);
    //}
     
    //if ($("#txtOthersFile2").val().length > 0) {
    sExtension = getFileExtension("txtOthersFile2");
    if (sExtension == '') {
        return;
    }
    file2 = setNameFileAttach($("#txtOthersFile2").val(), 2, 'OBM') + '.' + sExtension;
    UploadFilesOnAzure("txtOthersFile2", file2);
    $("#lblOthersFile2").text(file2);
    //}
    
    //if ($("#txtOthersFile3").val().length > 0) {
    sExtension = getFileExtension("txtOthersFile3");
    if (sExtension == '') {
        return;
    }
    file3 = setNameFileAttach($("#typeDocumentId8").val(), 3, 'OBM') + '.' + sExtension;
    UploadFilesOnAzure("txtOthersFile3", file3);
    $("#lblOthersFile3").text(file3);
    //}
    
    saveFileLoadOthers(xthis);

    o =
    {
        "typeDocumentID": $("#typeDocumentId8").val(),
        "number": $("#txtOther").val(),
        "file": file1,//$("#txtOthersFile").val(),
        "file2": file2,//$("#txtOthersFile2").val(),
        "file3": file3,//$("#txtOthersFile3").val(),
        "notesOBM": $("#txtOthersNotesOZM").val(),
        "enabled": $("#enabled8").val(),
        "statusId": $("#cmbOthersStatus option:selected").val(),
        "userID": $("#lblSessionUserID").text(),
        "notesCustomer": $("#txtOthersNotesCustomer").val(),
        "orderproductionID": _orderproductionID
    }
    console.log(o);

    var api = "/shippingProgram/SaveShippingDocumentPO"
    console.log(api);
    $.ajax({
        type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        url: api,
        //async: false,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(o),
        success: function (datares) {
            if (datares.toString().length > 0) {
                sweet_alert_success('Saved! ', ' The data has been saved.');
            } else {
                sweet_alert_error("Error ", " data was NOT saved!");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            sweet_alert_error("Error ", " data was NOT saved!");
            //Pendiente: eliminar archivo subido
        }
    });
}
function saveFileLoadOthers(xthis) {

    //$(xthis).closest('div.form-group').find('#cmbInvoiceGrowerStatus').val("1").change() //processin
    if ($(xthis).closest('div.form-group').find('.lblFile').text().length >= 1 || $(xthis).closest('div.form-group').find('.inputFile').val().length > 2) {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("1").change() //processing

        $(xthis).closest('div.form-group').find('.btnDelete').show()
        $(xthis).closest('div.form-group').find('.lblFile').show()
        $(xthis).closest('div.form-group').find('.btnDownLoad').show()

        $(xthis).closest('div.form-group').find('.inputFile').hide()
    }
    else {
        $(xthis).closest('div.form-group').find('.cmbStatus').val("0").change() //pending

        $(xthis).closest('div.form-group').find('.btnDelete').hide()
        $(xthis).closest('div.form-group').find('.lblFile').hide()
        $(xthis).closest('div.form-group').find('.btnDownLoad').hide()

        $(xthis).closest('div.form-group').find('.inputFile').show()
    }
    if ($(xthis).closest('div.container-fluid').find('.lblFile2').text().length >= 1 || $(xthis).closest('div.container-fluid').find('.inputFile2').val().length > 2) {
        $(xthis).closest('div.container-fluid').find('.cmbStatus').val("1").change()
        $(xthis).closest('div.container-fluid').find('.btnDelete2').show()
        $(xthis).closest('div.container-fluid').find('.lblFile2').show()
        $(xthis).closest('div.container-fluid').find('.btnDownLoad2').show()

        $(xthis).closest('div.container-fluid').find('.inputFile2').hide()
    }
    else {


        $(xthis).closest('div.container-fluid').find('.btnDelete2').hide()
        $(xthis).closest('div.container-fluid').find('.lblFile2').hide()
        $(xthis).closest('div.container-fluid').find('.btnDownLoad2').hide()

        $(xthis).closest('div.container-fluid').find('.inputFile2').show()
    }
    if ($(xthis).closest('div.container-fluid').find('.lblFile3').text().length >= 1 || $(xthis).closest('div.container-fluid').find('.inputFile3').val().length > 2) {
        $(xthis).closest('div.container-fluid').find('.cmbStatus').val("1").change()
        $(xthis).closest('div.container-fluid').find('.btnDelete3').show()
        $(xthis).closest('div.container-fluid').find('.lblFile3').show()
        $(xthis).closest('div.container-fluid').find('.btnDownLoad3').show()

        $(xthis).closest('div.container-fluid').find('.inputFile3').hide()
    }
    else {


        $(xthis).closest('div.container-fluid').find('.btnDelete3').hide()
        $(xthis).closest('div.container-fluid').find('.lblFile3').hide()
        $(xthis).closest('div.container-fluid').find('.btnDownLoad3').hide()

        $(xthis).closest('div.container-fluid').find('.inputFile3').show()
    }
}

function getFileExtension(oInputFileId) {
    let sExtension = '';
    var filename = $("#" + oInputFileId).val();
    var aux = filename.split('.');
    sExtension = aux[aux.length - 1].toUpperCase();

    if (sExtension != 'XLSX' && sExtension != 'XLS' && sExtension != 'PDF') {
        sweet_alert_error('Error', 'Invalid extension: ' + sExtension + '. Only: XLS, XLSX, PDF are allowed.');
        return '';
    } else {
        return sExtension;
    }
}

function setNameFileAttach(typeDocumentID, numberFile, origin) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var curHour = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
    var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    today = mm + '/' + dd + '/' + yyyy;
    var now = yyyy + mm + dd + "-" + curHour + curMinute + '.xlsx';

    var abbrev = ""
    if (origin = "OBM") {
        switch (parseInt(typeDocumentID)) {
            case 1:
                abbrev = "INVGRO";
                break;
            case 2:
                abbrev = "INVCUS";
                break;
            case 3:
                if (numberFile == 1) {
                    abbrev = "PLPDF";
                }
                if (numberFile == 2) {
                    abbrev = "PLXLS";
                }
                break;
            case 4:
                abbrev = "PHYTO";
                break;
            case 5:
                abbrev = "BL";
                break;
            case 6:
                abbrev = "CO";
                break;
            case 7:
                abbrev = "EF";
                break;
            case 8:
                if (numberFile == 1) {
                    abbrev = "OTHER1";
                }
                if (numberFile == 2) {
                    abbrev = "OTHER2";
                }
                if (numberFile == 3) {
                    abbrev = "OTHER3";
                }
                break;
            default:
        }
    }
    if (origin = "CUS") {
        switch (typeDocumentID) {
            case 1:
                abbrev = "INVGRO_";
                break;
            case 2:
                abbrev = "INVCUS_";
                break;
            case 3:
                abbrev = "PL_";
                break;
            case 4:
                abbrev = "PHYTO_";
                break;
            case 5:
                abbrev = "BL_";
                break;
            case 6:
                abbrev = "CO_";
                break;
            case 7:
                abbrev = "EF_";
                break;
            case 8:
                abbrev = "OTHER_";
                break;
            default:
        }
    }


    return abbrev + "_" + now;

}

function UploadFilesOnAzure(control, filename) {
    let bRsl = false;
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);

    var files = document.getElementById(control).files;
    var file = files[0];

    fileService.createFileFromBrowserFile(SharedDirectory, '', filename, file, {}, function (error, result, response) {
        if (error) {
            sweet_alert_error("FILE DO NOT UPLOADED: " + response);            
            console.log("result: " + result);

        } else {
            sweet_alert_success("FILE UPLOADED: " + Object.keys(response) + "//" + response.isSuccessful);
            console.log("response: " + Object.keys(response) + "//" + response.isSuccessful);
            console.log("result: " + Object.keys(result) + " // " + result.etag + " // " + result.share + " // " + result.directory + " // " + result.name + " // " + result.etag + " // " + result.lastModified + " // " + result.requestId);
            bRsl = true;
        }

        //return response;
        return bRsl;
    });    
}

function getLink(myfile) {
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    var downloadLink = fileService.getUrl(SharedDirectory, '', myfile, SAS_TOKEN);

    window.open(downloadLink);


    //fetch(downloadLink)
    //  .then(resp => resp.blob())
    //  .then(blob => {
    //    const url = window.URL.createObjectURL(blob);
    //    const a = document.createElement('a');
    //    a.style.display = 'none';
    //    a.href = url;
    //    // the filename you want
    //    a.download = 'aaaa.xls';
    //    document.body.appendChild(a);
    //    a.click();
    //    window.URL.revokeObjectURL(url);
    //    alert('your file has downloaded!'); // or you know, something with better UX...
    //  })
    //  .catch(() => alert('oh no!'));

    //fetch(downloadLink)
    //    .then(resp => resp.blob())
    //    .then(blob => {
    //        const url = window.URL.createObjectURL(blob);
    //        const a = document.createElement('a');
    //        a.style.display = 'none';
    //        a.href = url;
    //        // the filename you want
    //        a.download = 'filexxxx.pdf';
    //        document.body.appendChild(a);
    //        //a.click();
    //        window.URL.revokeObjectURL(url);
    //        alert('your file has downloaded!'); // or you know, something with better UX...
    //    })
    //    .catch(() => alert('oh no!'));


    //<a href="downloadLink" download> Descargar </a> 

    //  $(location).attr('href', 'http://stackoverflow.com')
    return downloadLink;

    //var fileUri = 'https://' + 'storageaga' + '.file.core.windows.net';
    //var SAS_TOKEN1 = "sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2021-01-01T04:59:59Z&st=2020-04-06T00:57:30Z&spr=https&sig=0gjOhB1iFXgblSVElnm1Z9w3hAwUvDpFNTqu7J2RMWw%3D";
    //var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    //var downloadLink = fileService.getUrl('agrocomshared', '', myfile, SAS_TOKEN1);
    //console.log("LIIIIIIIIIINK: " + downloadLink);
    //return downloadLink;
}

function CreateDirectoryOnAzure(mydirectory) {
    let bRsl = false;
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);

    fileService.createDirectoryIfNotExists(SharedDirectory, mydirectory, function (error, result, response) {
        if (error) {
            // Create directory error
            sweet_alert_error("DIRECTORY DO NOT CREATED: " + response);
        } else {
            // Create directory successfully
            sweet_alert_success("DIRECTORY CREATED: " + response);
            bRsl = true;
        }
    });

    return bRsl;
}

function DeleteDirectoryOnAzure(mydirectory) {
    let bRsl = false;
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    fileService.deleteDirectoryIfExists(SharedDirectory, mydirectory, function (error, result, response) {
        if (error) {
            // Delete directory error
            sweet_alert_error("DIRECTORY DO NOT DELETED: " + response);
        } else {
            // Delete directory successfully
            sweet_alert_success("DIRECTORY DELETED: " + response);
            bRsl = true;
        }
    });

    return bRsl;
}

function DeleteFileOnAzure(mydirectory, myfile) {
    let bRsl = false;
    var fileService = AzureStorage.File.createFileServiceWithSas(fileUri, SAS_TOKEN);
    fileService.deleteFileIfExists(SharedDirectory, mydirectory, myfile, function (error, result, response) {
        if (error) {
            // Delete file error
            sweet_alert_error("FILE DO NOT DELETED: " + response);
        } else {
            // Delete file successfully
            sweet_alert_success("FILE DELETED: " + response);
            bRsl = true;
        }
    });

    return bRsl;
}