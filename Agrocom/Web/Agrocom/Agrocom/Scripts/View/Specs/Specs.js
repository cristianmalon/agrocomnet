﻿var _cropID = localStorage.cropID;
var _campaignID = localStorage.campaignID;
var _userID = $('#lblSessionUserID').text();
var dataList = [];
var dataCustomer = [];
var collapsedGroups = {};
var dataMasiveProduct = [];
var DataModifiedProduct = [];
var head;

$(function () {

    loadData();
    loadDefaultValues();
    clientList();
    createList();

    sweet_alert_progressbar_cerrar();      

    $("#tablatblExcel").DataTable({
        "paging": true,
        "ordering": false,
        "info": true,
        "responsive": true,
        //"destroy": true,
        //select: true,
        lengthMenu: [
            [15, 30, 50, -1],
            ['15 rows', '30 rows', '50 rows', 'All']
        ],
        dom: 'Bfrtip',
        buttons: [
            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            'pageLength'
        ],
    });
    sweet_alert_progressbar_cerrar();

    $(document).on("click", ".row-remove", function () {
        $(this).parents('tr').detach();
    });

    $(document).on("click", "#btnAddCustomer", function () {
        addCustomer();
    });

    $("#btnSaveMassiveUpdate").click(function (e) {
        Swal.fire({
            title: '¿Want to save?',
            text: "Do you want to execute this action?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, save!',
            cancelButtonText: 'No, cancel!',
            confirmButtonColor: '#4CAA42',
            cancelButtonColor: '#d33'
        }).then((result) => {
            if (result.value) {
                SaveMasiveProduct();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                sweet_alert_error('Canceled!', 'Save process was canceled.');
            }
        });

    });

    $("#btnUpload").click(function (e) {

        var files = document.getElementById('fileExcel').files;
        var reader = new FileReader();
        DataModifiedProduct = [];
        if (files == null || files == undefined) {
            $('#fileExcel').val(null);
            sweet_alert_error('Error', 'Must choose an excel file.');
            return;
        } else if (files.length == 0) {
            $('#fileExcel').val(null);
            sweet_alert_error('Error', 'Must choose an excel file.');
            return;
        }
        reader.readAsDataURL(files[0]);
        reader.onload = function () {
            ReadExcel(reader.result);
            $(".btnDelete").prop("show", true);
            $(".lblFile").prop("show", true);
            $(".btnUpload").prop("show", false);
            $(".inputFile").hide();
        }
    });

    $("#fileExcel").change(function (e) {
        if ($("#fileExcel").length > 0) {
            $(".btnDelete").prop("hidden", false);
            $(".btnDelete").show();
            $(".lblFile").prop("hidden", false);
            $(".lblFile").show();
            $(".btnUpload").prop("hidden", false);
            $(".btnUpload").show();
            var filePrueba = $("#fileExcel").val();
            var filePrueba1 = filePrueba.split('\\');
            $(".lblFile").text(filePrueba1[2]);
            $(".inputFile").hide();
        }
        checkfile(this);
    });

    $("#btnDeleteFile").click(function (e) {
        dropFile(this);
    });
})

$('#btnSaveClient').click(function (e) {

    SaveClient();

});

$('#download').click(function (e) {

    DownloadTemplate();

});


$('#tblExcel tbody').on('click', 'tr.dtrg-start', function () {
    var name = $(this).data('name');
    collapsedGroups[name] = !collapsedGroups[name];
    table.draw(false);
});

function loadDefaultValues() {
    loadCombo(dataCustomer, 'selectCustomer', true);
    $('#selectCustomer').prop("disabled", false);
    $('#selectCustomer').selectpicker('refresh');
}

function clearControls() {
    //$("table#tblCustomer tbody").empty();
    //$("table#tblExcel tbody").empty();
}

function loadCombo(data, control, firtElement) {
    var content = "";
    if (firtElement == true) {
        content += "<option value=''>--Choose--</option>";
    }
    for (var i = 0; i < data.length; i++) {
        content += "<option value='" + data[i].id + "'>";
        content += data[i].name;
        content += "</option>";
    }
    $('#' + control).empty().append(content);
}

function loadData() {
    $.ajax({
        type: "GET",
        url: "/Specification/ListCustomerWithDestination?opt=" + "allcsg" + "&id=" + "",
        async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            var dataJson = JSON.parse(data);
            if (data.length > 0) {
                dataCustomer = dataJson.map(
                    obj => {
                        return {
                            "id": obj.customerID,
                            "name": obj.customer
                        }
                    }
                );
            }
        }
    });

}

function addCustomer() {
    var okey = 0;
    var customerID = $("#selectCustomer option:selected").val();
    if (customerID == "") {
        alert("You must select a Customer");
        return;
    }
    $("table#tblCustomer>tbody>tr").each(function (id, row) {
        if ($(row).find("td.customerID").text() == customerID) {
            return okey = 1;
        }
    })

    if (okey == 1) {
        sweet_alert_warning("Alert", "This customer has been added");
        return;
    }
    else {
        var customerID = $("#selectCustomer option:selected").val();
        var customer = $("#selectCustomer option:selected").text();

        var content = "";
        content += '<tr>'
        content += '<td class="customerID" style="min-width:150px; max-width:150px">' + customerID + '</td > ';
        content += '<td class="customer" style="min-width:500px; max-width:500px">' + customer + '</td>';        
        content += '<td style="min-width:100px; max-width:100px; text-align:center"><span class="icon text-danger"><i class="fas fa-trash fa-sm row-remove"></i><span></td>';       
        content += '</tr>';
        $("table#tblCustomer").DataTable().clear();
        $("table#tblCustomer").DataTable().destroy();
        var outerHTML = $("#tblCustomer").prop("outerHTML");
        console.log(outerHTML);
        $("table#tblCustomer tbody").append(content);        
    }

    $("#tblCustomer").DataTable({
        "scrollY": "20vh",
        "scrollCollapse": true,
        "paging": false,
        dom: 'Bfrtip',
        buttons: [
            { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
            //'pageLength'
        ]
    });

}

function clientList() {
    var api = "GetSpecsClientList?opt=" + 'cli' + "&campaignID=" + _campaignID;

    $.ajax({
        type: "GET",
        url: api,
        async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            if (data.length > 0) {
                var dataClient = JSON.parse(data);
                var content = "";
                $.each(dataClient, function (index, row) {
                    content += "<tr style='font-size:12px'>";
                    content += '<td class="customerID" style="min-width:150px; max-width:150px">' + row.customerID + '</td > ';
                    content += '<td class="customer" style="min-width:500px; max-width:500px">' + row.customer + '</td>';
                    content += '<td style="min-width:100px; max-width:100px; text-align:center"><button class="btn btn-sm row-remove" type="button" style="background-color:none; border:none; padding: 0px 0px 0px 0px;"><span class="icon text-danger"><i class="fas fa-trash fa-sm"></i><span></button></td> ';                    
                    content += "</tr>";
                })

                $("table#tblCustomer").DataTable().clear();
                $("table#tblCustomer").DataTable().destroy();
                $("table#tblCustomer tbody").append(content);

            }
        },
        complete: function () {

            $("#tblCustomer").DataTable({
                "scrollY": "20vh",
                "scrollCollapse": true,
                "paging": false,
                dom: 'Bfrtip',
                buttons: [
                    { extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true },
                    //'pageLength'
                ]
            });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notify("Problems loading the list!", "error");
            console.log(api)
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    });
}

function createList() {
    $("table#tblExcel").DataTable().clear();
    $("table#tblExcel").DataTable().destroy();
    $("table#tblExcel tbody").html('');

    dataList = []
    $.ajax({
        type: "GET",
        url: "GetSpecsClientList?opt=" + 'pro' + "&campaignID=" + _campaignID,
        //async: false,
        headers: {'Cache-Control': 'no-cache, no-store, must-revalidate','Pragma': 'no-cache','Expires': '0'},
        success: function (data) {
            if (data.length > 0) {
                var dataJson = JSON.parse(data);
                dataList = dataJson;                
                $.each(dataList, function (i, e) {
                    var tr = '';
                    tr += "<tr style='font-size:13px'>";
                    tr += '<td style="max-width: 220px; min-width:220px" class="customer">' + e.customer + '</td>';
                    tr += '<td style="max-width: 70px; min-width:70px" class="crop" >' + e.crop + '</td>';                        
                    tr += '<td style="max-width: 120px; min-width:100px" class="destination">' + e.destination + '</td>';                    
                    tr += '<td style="max-width: 150px; min-width:150px" class="product">' + e.product + '</td>';
                    tr += '<td style="max-width: 500px; min-width:300px" class="description"><textarea style="font-size:13px" class="form-control form-control-sm" id="description" rows="1">' + e.description + '</textarea></td>';
                    tr += '<td style="max-width: 55px; min-width:50px" class="priority">' + e.priority + '</td>';                    
                    tr += '<td style="max-width: 70px; min-width:50px" class="minimum">' + e.minimum + '</td>';
                    tr += '<td style="max-width: 70px; min-width:50px" class="maximum">' + e.maximum + '</td>';                        
                    tr += '</tr>';
                    $("table#tblExcel tbody").append(tr);
                    
                })
            }
        },

        complete: function () {

            table = $("#tblExcel").DataTable({
                //'rowsGroup': [0],
                rowGroup: {
                    // Uses the 'row group' plugin
                    dataSrc: 0,
                    startRender: function (rows, group) {
                        var collapsed = !!collapsedGroups[group];

                        rows.nodes().each(function (r) {
                            r.style.display = collapsed ? 'none' : '';
                        });

                        // Add category name to the <tr>. NOTE: Hardcoded colspan
                        return $('<tr/>')
                            .append('<td colspan="51">' + group + ' (' + rows.count() + ')</td>')
                            .attr('data-name', group)
                            .toggleClass('collapsed', collapsed);
                    }
                },
                "paging": true,
                "ordering": false,
                "info": true,
                "responsive": true,
                //"destroy": true,
                dom: 'Bfrtip',
                //select: true,
                lengthMenu: [
                    [15, 30, 50, -1],
                    ['15 rows', '30 rows', '50 rows', 'All']
                ],
                dom: 'Bfrtip',
                buttons: [

                    {
                        extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true                        
                    },
                    'pageLength'
                ],
            });

        }

    });

}

function SaveClient() {

    var campaignID = _campaignID;
    var cropID = _cropID;
    var userID = _userID;
    var customers_table = []

    $("table#tblCustomer>tbody>tr").each(function (iTr, tr) {
        var o = {}

        var customerID = $(tr).find('td.customerID').text();

        o.customerID = parseInt(customerID);
        
        customers_table.push(o)
    })

    var frmClientN = false;
    if (1 == 1) {
        //Envpio de datos a la api
        var frmClientN = new FormData();
        
        frmClientN.append("campaignID", campaignID);
        frmClientN.append("cropID", cropID);
        frmClientN.append("userID", userID);
        frmClientN.append("customers", JSON.stringify(customers_table));
        var api = "/Specification/GetClientSaveUpdate"
                
        $.ajax({
            type: 'POST', headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: api,
            async: false,
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(Object.fromEntries(frmClientN)),
            Accept: "application/json",

            success: function (datares) {
                if (datares.length > 0) {
                    sweet_alert_success('Saved!', 'The data has been saved.');
                    clientList();
                    $('#selectCustomer').selectpicker("refresh");
                    //createList();
                } else {
                    sweet_alert_error("Error", "The data was NOT saved!");
                    console.log('An error occurred while saving clients')
                    console.log(api)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                sweet_alert_error("Error", "The data was NOT saved!");
                console.log(api)
                console.log(xhr);
                console.log(ajaxOptions);
                console.log(thrownError);
            }
        });
    }
}

function ReadExcel(url) {
    var oReq = new XMLHttpRequest();
    oReq.open("GET", url, true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function (e) {
        var info = readData();
        function readData() {
            var arraybuffer = oReq.response;
            /* convert data to binary string */
            var data = new Uint8Array(arraybuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");

            var workbook = XLSX.read(bstr, { type: "binary" });

            var first_sheet_name = workbook.SheetNames[0];
            
            var worksheet = workbook.Sheets[first_sheet_name];
            var info = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            return info;
        }
        ProccessExcelData(info)
    }
    oReq.send();
}

function ProccessExcelData(data) {
    
    var DataFromExcel = [];
    var DataRow = {};
    var msg = "";
    var PrintErrors = "";
    var specsCustomerCropID;
    var customerID;
    var destinationID;
    var productID;
    var priority;
    var minimum;
    var maximum;
    var today = new Date();
    var yyyy = today.getFullYear();

    //if (head[2].substr(1) != "CUSTOMER" || head[4].substr(1) != "DESTINATION" || head[6].substr(1) != "CODEPACK" ||
    //    head[8].substr(1) != "PRIORITY" || head[9].substr(1) != "MINIMUM" || head[10].substr(1) != "MAXIMUN") {
    //    msg += "Wrong Headers: <br> \n";
    //    if (head[2] != "CUSTOMER") {
    //        msg += " Incorrect header in Column 2(Customer) " + "<br> \n";
    //    }
    //    if (head[4] != "DESTINATION") {
    //        msg += " Incorrect header in Column 4(Destination) " + "<br> \n";
    //    }
    //    if (head[6] != "CODEPACK") {
    //        msg += " Incorrect header in Column 6(CodePack) " + "<br> \n";
    //    }
    //    if (head[8] != "PRIORITY") {
    //        msg += " Incorrect header in Column 8(Priority) " + "<br> \n";
    //    }
    //    if (head[9] != "MINIMUM") {
    //        msg += " Incorrect header in Column 9(Minimun) " + "<br> \n";
    //    }
    //    if (head[10] != "MAXIMUN") {
    //        msg += " Incorrect header in Column 10(Maximun) " + "<br> \n";
    //    }        
    //    PrintErrors += msg;
    //    msg = "";
    //} else {
        $.each(data, function (i, e) {      
                      
            if (msg == "") {
                DataRow.specsCustomerCropID = specsCustomerCropID;
                DataRow.customerID = customerID;
                DataRow.destinationID = destinationID;
                DataRow.productID = productID;                
                //Validamos que nose seteen valores indefinidos o nulos
                DataRow.priority = priority == undefined ? 0 : priority;
                DataRow.minimum = minimum == undefined ? 0 : minimum;
                DataRow.maximum = maximum == undefined ? 0 : maximum;
                DataFromExcel.push(DataRow);
                DataRow = {};
            } else {
                msg += " in ROW " + (i + 2) + "<br> \n";
            }

            PrintErrors += msg;
            msg = "";
        });
        
    //}
    
    DrawInTable(DataFromExcel);
    //if (PrintErrors == "") {
    //    DrawInTable(DataFromExcel);
    //    $("#btnSaveMassiveUpdate").prop('disabled', false);
    //}
}

function DrawInTable(_DataFromExcel) {
    
    var iNro = 0;

    $.each(_DataFromExcel, function (j, e) {            

        iNro += 1;
        item = {}
        item["ID"] = iNro;
        item["specsCustomerCropID"] = e.specsCustomerCropID;
        item["productNetsuiteID"] = e.productID;
        item["price"] = 0;
        item["priority"] = e.priority;
        item["minimun"] = e.minimun;
        item["maximun"] = e.maximun;
        item["statusID"] = 1;
        DataModifiedProduct.push(item);                
        
    });    
    SaveMasiveProduct();
}

function SaveMasiveProduct() {
    //1er.Obtener el total de filas
    var nFilas = DataModifiedProduct.length;
    //2do.Obtener la cantidad de vueltas = total filas / 100, y si >= ###.05, redondear
    var nVueltas = (nFilas / 100).toFixed(2);
    var numero = ((nVueltas + "").split("."));
    if (parseInt(numero[1]) > 0) {
        nVueltas = parseInt(numero[0]) + 1;
    }
    //3er.Hacer un loop(each) x cantidad de vueltas y enviar los datos al ajax
    if (nVueltas == 1) {
        //Parseo de datos a formato json
        let jsonObjExcel = JSON.stringify({
            MassLoadProductDetail: DataModifiedProduct
        });
        //console.log('JsonDataFormat:' + jsonObjExcel);
        $.ajax({
            type: 'POST',
            headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
            url: "SaveProductUpdate?campaignID=" + _campaignID + "&userID=" + _userID + "&file=" + '',
        	data: jsonObjExcel,
        	contentType: "application/json",
        	Accept: "application/json",
        	dataType: 'json',
        	async: false,
        	error: function (datoEr) {
        		sweet_alert_error('Error', "There was an issue trying to saving.");
        	},
        	success: function (data) {
        		Swal.fire({
        			title: 'Information for user!',
        			text: "Masive Data From Excel Saved Correctly.",
        			icon: 'info',
        			showCancelButton: false,
        			confirmButtonText: 'Ok',
        			confirmButtonColor: '#4CAA42',
        		});
                createList();
        	}
        });
    }
    
}

function DownloadTemplate() {

    $.ajax({
        type: "GET",
        url: "GetSpecsClientList?opt=" + 'mas' + "&campaignID=" + _campaignID,
        async: false,
        headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate', 'Pragma': 'no-cache', 'Expires': '0' },
        success: function (data) {
            if (data.length > 0) {
                dataMasiveProduct = JSON.parse(data);
            }
        }
    });

    var createXLSLFormatObj = [];
    var head = Object.keys(dataMasiveProduct[0]);
    /* XLS Head Columns */
    var xlsHeader = ["CUSTOMER", "DESTINATION", "CODEPACK", "DESCRIPTION", "PRIORITY", "MINIMUM", "MAXIMUN"];

    /* XLS Rows Data */
    var xlsRows = dataMasiveProduct;

    createXLSLFormatObj.push(xlsHeader);
    $.each(xlsRows, function (index, value) {
        var innerRowData = [];
        innerRowData.push(value.customer);
        innerRowData.push(value.destination);
        innerRowData.push(value.codePack);
        innerRowData.push(value.description);
        innerRowData.push(value.priority = (value.priority == null) ? value.priority : 0);
        innerRowData.push(value.minimun = (value.minimun == null) ? value.minimun : 0);
        innerRowData.push(value.maximun = (value.maximun == null) ? value.maximun : 0);
        createXLSLFormatObj.push(innerRowData);
    });

    /* File Name */
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var curHour = today.getHours() > 12 ? today.getHours() : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
    var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    today = mm + '/' + dd + '/' + yyyy;
    var now = yyyy + mm + dd + "-" + curHour + curMinute;

    var _crop = 'SPECS' + '_' + _cropID;
    var filename = _crop + "_" + now + "_XLS.xlsx";

    /* Sheet Name */
    var ws_name = "Specs";

    //if (typeof console !== 'undefined') //console.log(new Date());
    var wb = XLSX.utils.book_new();
    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

    /* Add worksheet to workbook */
    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    /* Write workbook and Download */
    //if (typeof console !== 'undefined') //console.log(new Date());
    XLSX.writeFile(wb, filename);
    //if (typeof console !== 'undefined') //console.log(new Date());
}

function checkfile(sender) {
    var validExts = new Array(".xlsx", ".xls");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
        $(sender).val('');
        sweet_alert_error('Error', "Invalid file selected, valid files are of " + validExts.toString() + " types.");
        return false;
    }
    else return true;

}

function dropFile(xthis) {
    $(xthis).parent('div').find('.btnDelete').hide();
    $(xthis).parent('div').find('.lblFile').hide();
    $(xthis).parent('div').find('.btnUpload').hide();
    $(xthis).parent('div').find('.inputFile').show();
    $(xthis).parent('div').find('.lblFile').text('');
    $(xthis).parent('div').find('.inputFile').val('');
}