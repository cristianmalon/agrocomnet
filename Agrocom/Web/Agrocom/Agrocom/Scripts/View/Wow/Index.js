﻿var table;
var dataDestination = []
var dataMarket = []
var dataCustomer = []
var dataConsignee = []
var dataNotify =[]
//var dataDocuments = [{ "documentExportID": 1, "description": "Seawaybill" },
//{ "documentExportID": 2, "description": "Original" },
//{ "documentExportID": 3, "description": "Air Waybill" }]

//var dataCertification = [{ "certificationID": 1, "description": "GFS" },
//{ "certificationID": 2, "description": "GLOBAL GAP" }]
var dataCertification = [];

var dataPackingList = [
    {
        "packingListID": 1,
        "description": "Virtual"
    },
    {
        "packingListID": 2,
        "description": "Original"
    }
]

var dataDocumentsSea = [
    {
        "documentExportID": 1,
        "description": "Seawaybill",
        "statusID": 1
    },
    {
        "documentExportID": 2,
        "description": "Original",
        "statusID": 1
    }
]

var dataDocumentsAir = [
    {
        "documentExportID": 3,
        "description": "Air WaybilL",
        "statusID": 1
    }
]

$(function () {
    //$(document).ajaxStart(function () {
    //    $("#wait").css("display", "block");
    //    $("#loader").css("display", "block")
    //    $(".transact").attr("disabled", true);
    //});
    //$(document).ajaxComplete(function () {
    //    $("#wait").css("display", "none");
    //    $("#loader").css("display", "none")
    //    $(".transact").attr("disabled", false);
    //});
    $("#submitSave").click(function (e) {
        var $myForm = $('#frmWow');
        if ($("#frmWow")[0].checkValidity())
            saveWow();        
        else            
            $("#frmWow")[0].reportValidity()
    });

    $(chkDocumentsMoreInfo).change(function (event) {
        var checkbox = event.target;
        if (checkbox.checked) {
            $(txtDocumentsInfo).removeAttr("disabled");
        } else {
            $(txtDocumentsInfo).attr("disabled", "disabled");
            $(txtDocumentsInfo).val('');
        }
    });

    $(chkPackingList).change(function (event) {
        var checkbox = event.target;
        if (checkbox.checked) {
            $(selectPackingList).parent().removeAttr("style");
        } else {
            $(selectPackingList).parent().css('display', 'none');
            $(selectPackingList).selectpicker('val', '0');
        }
    });

    $(chkPhytosanitary).change(function (event) {
        var checkbox = event.target;
        if (checkbox.checked) {
            $(chkPhytosanitaryMoreInfo).parent().parent().parent().removeAttr("style");
            $(chkPhytosanitaryMoreInfo).prop('checked', false);
            $(txtPhytosanitaryInfo).val('');
        } else {
            $(chkPhytosanitaryMoreInfo).parent().parent().parent().css('display', 'none');
        }
    });

    $(chkCertificateOrigin).change(function (event) {
        var checkbox = event.target;
        if (checkbox.checked) {
            $(chkCertificateOriginMoreInfo).parent().parent().parent().removeAttr("style");
            $(chkCertificateOriginMoreInfo).prop('checked', false);
            $(txtCertificateOriginInfo).val('');
        } else {
            $(chkCertificateOriginMoreInfo).parent().parent().parent().css('display', 'none');
        }
    });

    $(chkPhytosanitaryMoreInfo).change(function (event) {
        var checkbox = event.target;
        if (checkbox.checked) {
            $(txtPhytosanitaryInfo).val('');
            $(txtPhytosanitaryInfo).removeAttr("disabled");
        } else {
            $(txtDocumentsInfo).attr("disabled", "disabled");
        }
    });

    $(chkCertificateOriginMoreInfo).change(function (event) {
        var checkbox = event.target;
        if (checkbox.checked) {
            $(txtCertificateOriginInfo).val('');
            $(txtCertificateOriginInfo).removeAttr("disabled");
        } else {
            $(txtCertificateOriginInfo).attr("disabled", "disabled");

        }
    });

    $(selectMarket).change(function () {
        let data = dataDestination.filter(item => item.marketID.trim() == this.value.trim()).map(
            obj => {
                return {
                    "id": obj.destinationID,
                    "name": obj.destination
                }
            }
        );

        loadCombo(data, 'selectDestination', false);
        $('#selectDestination').selectpicker('refresh');
        $('#selectDestination').selectpicker('val', '0');
    });

    $(selectConsignee2).change(function () {
        let data = dataConsignee.filter(item => item.customerID == this.value)
        if (data.length > 0) {
            $(txtConsigneeAddress).val(data[0].address)
            $(txtConsigneePhone).val(data[0].phone)
            $(txtConsigneeContact).val(data[0].contact)
        } else {
            $(txtConsigneeAddress).val('')
            $(txtConsigneePhone).val('')
            $(txtConsigneeContact).val('')
        }
    });
    $(selectNotify1).change(function () {
        let data = dataNotify.filter(item => item.customerID == this.value)
        if (data.length > 0) {
            $(txtNotify1Address).val(data[0].address)
            $(txtNotify1Phone).val(data[0].phone)
            $(txtNotify1Contact).val(data[0].contact)
        } else {
            $(txtNotify1Address).val('')
            $(txtNotify1Phone).val('')
            $(txtNotify1Contact).val('')
        }
    });
    $(selectNotify2).change(function () {
        let data = dataNotify.filter(item => item.customerID == this.value)
        if (data.length > 0) {
            $(txtNotify2Address).val(data[0].address)
            $(txtNotify2Phone).val(data[0].phone)
            $(txtNotify2Contact).val(data[0].contact)
        } else {
            $(txtNotify2Address).val('')
            $(txtNotify2Phone).val('')
            $(txtNotify2Contact).val('')
        }
    });
    $(selectNotify3).change(function () {
        let data = dataNotify.filter(item => item.customerID == this.value)
        if (data.length > 0) {
            $(txtNotify3Address).val(data[0].address)
            $(txtNotify3Phone).val(data[0].phone)
            $(txtNotify3Contact).val(data[0].contact)
        } else {
            $(txtNotify3Address).val('')
            $(txtNotify3Phone).val('')
            $(txtNotify3Contact).val('')
        }
    });

    $(btnNew).click(function () {
        clearControls();
    })
    loadData();
    list();
})

function saveWow() {
    var frmWow = new FormData();
    var VdirectInvoice = 0
    var saleID = $("#lblWowID").text();
    if ($('#chkDirectInvoice').is(":checked")) {
        VdirectInvoice = 1
    }

    frmWow.append("wowID", $(lblWowID).text());
    frmWow.append("customerID", $("#selectCustomer").val());
    frmWow.append("destinationID", $("#selectDestination").val());
    frmWow.append("directInvoice", VdirectInvoice);
    frmWow.append("certificationID", $("#selectCertification").val());

    var vdocumentsMoreInfo = 0
    if ($('#chkDocumentsMoreInfo').is(":checked")) { vdocumentsMoreInfo = 1 }
    var vinsuranceCertificate = 0
    if ($('#chkInsuranceCertificate').is(":checked")) { vinsuranceCertificate = 1 }

    var vpackingList = $("#selectPackingList").val()
    if (vpackingList == "") {
        vpackingList = "0"
    }
    frmWow.append("documents", "1");
    frmWow.append("documentsMoreInfo", vdocumentsMoreInfo);
    frmWow.append("documentsInfo", $(txtDocumentsInfo).val());
    frmWow.append("packingListID", vpackingList);
    frmWow.append("insuranceCertificate", vinsuranceCertificate);

    var vphytosanitary = 0
    if ($('#chkPhytosanitary').is(":checked")) { vphytosanitary = 1 }
    var vphytosanitaryMoreInfo = 0
    if ($('#chkPhytosanitaryMoreInfo').is(":checked")) { vphytosanitaryMoreInfo = 1 }
    var vcertificateOrigin = 0
    if ($('#chkCertificateOrigin').is(":checked")) { vcertificateOrigin = 1 }
    var vcertificateOriginMoreInfo = 0
    if ($('#chkCertificateOriginMoreInfo').is(":checked")) { vcertificateOriginMoreInfo = 1 }

    frmWow.append("phytosanitary", vphytosanitary);
    frmWow.append("phytosanitaryMoreInfo", vphytosanitaryMoreInfo);
    frmWow.append("phytosanitaryInfo", $(txtPhytosanitaryInfo).val());
    frmWow.append("certificateOrigin", vcertificateOrigin);
    frmWow.append("certificateOriginMoreInfo", vcertificateOriginMoreInfo);
    frmWow.append("certificateOriginInfo", $(txtCertificateOriginInfo).val());
    frmWow.append("moreInfo", $(txtMoreDocuments).val());

    frmWow.append("consigneeID", $("#selectConsignee2").val());
    frmWow.append("consigneeAddress", $(txtConsigneeAddress).val());
    frmWow.append("consigneeContact", $(txtConsigneeContact).val());
    frmWow.append("consigneePhone", $(txtConsigneePhone).val());

    frmWow.append("notifyID", $("#selectNotify1").val());
    frmWow.append("notifyIDAddress", $(txtNotify1Address).val());
    frmWow.append("notifyIDContact", $(txtNotify1Contact).val());
    frmWow.append("notifyIDPhone", $(txtNotify1Phone).val());

    frmWow.append("notifyID2", $("#selectNotify2").val());
    frmWow.append("notifyID2Address", $(txtNotify2Address).val());
    frmWow.append("notifyID2Contact", $(txtNotify2Contact).val());
    frmWow.append("notifyID2Phone", $(txtNotify2Phone).val());

    frmWow.append("notifyID3", $("#selectNotify3").val());
    frmWow.append("notifyID3Address", $(txtNotify3Address).val());
    frmWow.append("notifyID3Contact", $(txtNotify3Contact).val());
    frmWow.append("notifyID3Phone", $(txtNotify3Phone).val());

    frmWow.append("docsCopyEmailInfo", $(txtDocsCopyEmailInfo).val());
    frmWow.append("noted", $(txtNoted).val());

    //frmWow.append("price", $(txtPrice).val());
    //frmWow.append("accountSale", $(txtAccountSale).val());
    //frmWow.append("finalPaymentConditions", $(txtPaymentConditions).val());
    //frmWow.append("advanceCondition", $(txtAdvanceConditions).val());
    frmWow.append("userID", "0");

    frmWow.append("documentsExportSea", $("#selectDocumentsSea").val());
    frmWow.append("documentsExportAir", $("#selectDocumentsAir").val());

    var o = {}
    o.wowID = $(lblWowID).text();
    o.customerID = $("#selectCustomer").val();
    o.destinationID = $("#selectDestination").val();
    o.certificationID = $("#selectCertification").val().toString();
    o.directInvoice = VdirectInvoice;
    o.directInvoiceMoreInfo = $(txtDirectInvoiceMoreInfo).val()
    o.documents = 1;

    o.documentsExportSea = $("#selectDocumentsSea").val();
    o.documentsExportAir = $("#selectDocumentsAir").val();

    o.documentsInfo = $(txtDocumentsInfo).val();
    o.documentsMoreInfo = vdocumentsMoreInfo;
    o.packingListID = vpackingList;
    o.packingListMoreInfo = $(txtPLMoreInfo).val();
    o.insuranceCertificate = vinsuranceCertificate;

    o.phytosanitary = vphytosanitary;
    o.phytosanitaryMoreInfo = vphytosanitaryMoreInfo;
    o.phytosanitaryInfo = $(txtPhytosanitaryInfo).val();

    o.certificateOrigin = vcertificateOrigin;
    o.certificateOriginMoreInfo = vcertificateOriginMoreInfo;
    o.certificateOriginInfo = $(txtCertificateOriginInfo).val();
    o.moreInfo = $(txtMoreDocuments).val();

    o.consigneeID = $("#selectConsignee2").val();
    o.consigneeAddress = $(txtConsigneeAddress).val();
    o.consigneeContact = $(txtConsigneeContact).val();
    o.consigneePhone = $(txtConsigneePhone).val();
    o.consigneeOther = $(txtConsigneeOther).val();

    o.notifyID = $("#selectNotify1").val();
    o.notifyIDAddress = $(txtNotify1Address).val();
    o.notifyIDContact = $(txtNotify1Contact).val();
    o.notifyIDPhone = $(txtNotify1Phone).val();
    o.notifyIDOther = $(txtNotify1Other).val();

    o.notifyID2 = $("#selectNotify2").val();
    o.notifyID2Address = $(txtNotify2Address).val();
    o.notifyID2Contact = $(txtNotify2Contact).val();
    o.notifyID2Phone = $(txtNotify2Phone).val();
    o.notifyID2Other = $(txtNotify2Other).val();

    o.notifyID3 = $("#selectNotify3").val();
    o.notifyID3Address = $(txtNotify3Address).val();
    o.notifyID3Contact = $(txtNotify3Contact).val();
    o.notifyID3Phone = $(txtNotify3Phone).val();
    o.notifyID3Other = $(txtNotify3Other).val();

    o.docsCopyEmailInfo = $(txtDocsCopyEmailInfo).val();
    o.noted = $(txtNoted).val();

    o.userID = $("#lblSessionUserID").text();
    o.cropID = localStorage.cropID
    var wow = false;

    var settings = {
        "url": "/wow/Save",
        "method": "POST",
        "timeout": 0,
        //"headers": {
        //    "Content-Type": "application/json",
        //    //"Cookie": "ASP.NET_SessionId=f4lo1mxalrgmmtgncg0hrmh2"
        //},
        //"data": JSON.stringify({ "wowID": 1, "moreInfo": "123" }),
        //"data": { "wowID": 1, "moreInfo": "123" },
        "data": o
    };

    $.ajax(settings).done(function (response) {
        $('#myModal').modal('hide');
        Swal.fire("Good job!", "Shipping Instruction Saved", "success");
      list();
    });    
}

function clearControls() {
    $(selectCustomer).selectpicker('val', '0');
    $(selectMarket).selectpicker('val', '0');
    $(selectDestination).selectpicker('val', '0');
    $(selectConsignee).selectpicker('val', '0');
    $(selectCertification).selectpicker('val', '0');

    $(chkDirectInvoice).prop('checked', false);

    $(chkDocumentsMoreInfo).prop('checked', false);
    $(txtDocumentsInfo).attr("disabled", "disabled");
    $(txtDocumentsInfo).val('');

    $(chkPackingList).prop('checked', false);
    $(selectPackingList).parent().css('display', 'none');
    $(selectPackingList).selectpicker('val', '0');

    $(chkInsuranceCertificate).prop('checked', false);

    $(chkPhytosanitary).prop('checked', false);
    $(chkPhytosanitaryMoreInfo).prop('checked', false);
    $(chkPhytosanitaryMoreInfo).parent().parent().parent().css('display', 'none');
    $(txtPhytosanitaryInfo).attr("disabled", "disabled");
    $(txtPhytosanitaryInfo).val('');

    $(chkCertificateOrigin).prop('checked', false);
    $(chkCertificateOriginMoreInfo).prop('checked', false);
    $(chkCertificateOriginMoreInfo).parent().parent().parent().css('display', 'none');
    $(txtCertificateOriginInfo).attr("disabled", "disabled");
    $(txtCertificateOriginInfo).val('');

    $(selectConsignee).selectpicker('val', '0');
    $(txtConsigneeAddress).val('');
    $(txtConsigneeContact).val('');
    $(txtConsigneePhone).val('');

    $(selectNotify1).selectpicker('val', '0');
    $(txtNotify1Address).val('');
    $(txtNotify1Contact).val('');
    $(txtNotify1Phone).val('');

    $(selectNotify2).selectpicker('val', '0');
    $(txtNotify2Address).val('');
    $(txtNotify2Contact).val('');
    $(txtNotify2Phone).val('');

    $(selectNotify3).selectpicker('val', '0');
    $(txtNotify3Address).val('');
    $(txtNotify3Contact).val('');
    $(txtNotify3Phone).val('');

    $(txtDocsCopyEmailInfo).val('');
    $(txtNoted).val('');
    //$(txtPrice).val('');
    //$(txtAccountSale).val('');
    //$(txtPaymentConditions).val('');
    //$(txtAdvanceConditions).val('');
}

function openModal(id) {
    var opt = "id";
    var cropID = localStorage.cropID;
    clearControls()

    $.ajax({
        type: "GET",
        url: "/Wow/GetById?opt=" + opt + "&id=" + id + "&cropID=" + cropID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            var xJson = JSON.parse(data)
            if (xJson.length > 0) {
                var dataJson = JSON.parse(data)[0];
                $(lblWowID).text(dataJson.wowID);

                $(selectCustomer).selectpicker('val', dataJson.customerID);
                $(selectMarket).selectpicker('val', dataJson.marketID.trim());
                $(selectDestination).selectpicker('val', dataJson.destinationID);

                $(selectConsignee).selectpicker('val', dataJson.consigneeID);
                //$(selectCertification).selectpicker('val', dataJson.certificationID);
                $(selectCertification).selectpicker('val', dataJson.certificationID.split(','));

                $(txtDirectInvoiceMoreInfo).val(dataJson.directInvoiceMoreInfo);
                $(chkDirectInvoice).prop('checked', dataJson.directInvoice);

                $(selectDocumentsSea).selectpicker('val', dataJson.documentExportIDSea);
                $(selectDocumentsAir).selectpicker('val', dataJson.documentExportIDAir);
                $(chkDocumentsMoreInfo).prop('checked', dataJson.documentsMoreInfo).change();
                $(txtDocumentsInfo).val(dataJson.documentsInfo)
                $(txtMoreDocuments).val(dataJson.moreInfo)
                //$(txtDirectInvoiceMoreInfo).val(dataJson.directInvoiceMoreInfo);
                $(txtPLMoreInfo).val(dataJson.packingListMoreInfo)
                $(chkPackingList).prop('checked', dataJson.packingListID);
                if (dataJson.packingListID) {
                    $(selectPackingList).parent().removeAttr("style");
                }

                $(selectPackingList).selectpicker('val', dataJson.packingList).selectpicker('refresh');

                $(chkInsuranceCertificate).prop('checked', dataJson.insuranceCertificate);

                $(chkPhytosanitary).prop('checked', dataJson.phytosanitary);
                if (dataJson.phytosanitary) {
                    $(chkPhytosanitaryMoreInfo).parent().parent().parent().removeAttr("style");
                }

                $(chkPhytosanitaryMoreInfo).prop('checked', dataJson.phytosanitaryMoreInfo);
                if (dataJson.phytosanitaryMoreInfo) {
                    $(txtPhytosanitaryInfo).removeAttr("disabled");
                }

                $(txtPhytosanitaryInfo).val(dataJson.phytosanitaryInfo)

                $(chkCertificateOrigin).prop('checked', dataJson.certificateOrigin).change();
                $(chkCertificateOriginMoreInfo).prop('checked', dataJson.certificateOriginMoreInfo).change();
                $(txtCertificateOriginInfo).val(dataJson.certificateOriginInfo)

                $(selectConsignee2).selectpicker('val', dataJson.consigneeID);
                $(txtConsigneeAddress).val(dataJson.csGaddress)
                $(txtConsigneeContact).val(dataJson.csGcontact)
                $(txtConsigneePhone).val(dataJson.csGphone)
                $(txtConsigneeOther).val(dataJson.csGmoreinfo)

                $(selectNotify1).selectpicker('val', dataJson.ntF1notifyID);
                $(txtNotify1Address).val(dataJson.ntF1address)
                $(txtNotify1Contact).val(dataJson.ntF1contact)
                $(txtNotify1Phone).val(dataJson.ntF1phone)
                $(txtNotify1Other).val(dataJson.ntF1moreinfo)

                //$(chkNotify2).selectpicker('val', dataJson.ntF1notifyID);
                $(selectNotify2).selectpicker('val', dataJson.ntF2notifyID);
                $(txtNotify2Address).val(dataJson.ntF2address)
                $(txtNotify2Contact).val(dataJson.ntF2contact)
                $(txtNotify2Phone).val(dataJson.ntF2phone)
                $(txtNotify2Other).val(dataJson.ntF2moreinfo)

                $(selectNotify3).selectpicker('val', dataJson.ntF3notifyID);
                $(txtNotify3Address).val(dataJson.ntF3address)
                $(txtNotify3Contact).val(dataJson.ntF3contact)
                $(txtNotify3Phone).val(dataJson.ntF3phone)
                $(txtNotify3Other).val(dataJson.ntF3moreinfo)

                $(txtDocsCopyEmailInfo).val(dataJson.docsCopyEmailInfo)
                $(txtNoted).val(dataJson.noted)
                //$(txtPrice).val(dataJson.price)
                //$(txtAccountSale).val(dataJson.accountSale)
                //$(txtPaymentConditions).val(dataJson.finalPaymentConditions)
                //$(txtAdvanceConditions).val(dataJson.advanceCondition)

            }

        }
    });
}

function list() {
    var opt = "all";
    var cropID = localStorage.cropID;
    $("table#tblList tbody").html('');
    $.ajax({
        type: "GET",
        url: "/Wow/ListWow?opt=" + opt + "&id=" + "" + "&cropID=" + cropID,
        async: false,
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        },
        success: function (data) {
            if (data.length > 0) {
              var dataJson = JSON.parse(data);
              var content = '';
                $.each(dataJson, function (i, e) {
                    content += '<tr class="" style= "font-size:12px">';
                    content += '<td style="text-align: center;">' + "<button class='btn btn-primary btn-sm' onclick='openModal(" + e.wowID + ")' data-toggle='modal' data-target='#myModal'><i class='fas fa-edit'></i></button> " + '</td >';
                    content += '<td style="display:none" >' + e.wowID + '</td >';
                    content += '<td style="max-width:150px; min-width:150px"><textarea style= "font-size:12px"  class="form-control form-control-sm" rows="1" id="customerID" readonly>' + e.customer + '</textarea></td >';
                    content += '<td style="max-width:150px; min-width:150px">' + e.destination + '</td >';
                    content += '<td style="max-width:150px; min-width:150px"><textarea style= "font-size:12px"  class="form-control form-control-sm" rows="1" id="consigneeID " readonly>' + e.consignee + '</textarea></td >';
                    content += '<td style="max-width:150px; min-width:150px"><textarea style= "font-size:12px"  class="form-control form-control-sm" rows="1" id="notifyID" readonly>' + e.notify + '</textarea></td >';
                    content += '<td  style="max-width:100px; min-width:100px">' + e.user + '</td >';
                    content += '<td>' + e.dateModified + '</td >';                    
                  content += '</tr>';
                })

              $("table#tblSales").DataTable().clear();
              $("table#tblList").DataTable().destroy();
              $("table#tblList tbody").empty().append(content);
              table = $('table#tblList').DataTable({
                  select: true,
                lengthMenu: [
                  [10, 25, 50, -1],
                  ['10 rows', '25 rows', '50 rows', 'All']
                ],
                dom: 'Bfrtip',
                buttons: [

                  {
                    extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
                     exportOptions: {
                      columns: [ 2,3, 4, 5, 6,7]
                    }
                  },
                  'pageLength'
                ],
              });
            }
        }
    });
  sweet_alert_progressbar_cerrar();
}


table = $('#tblList').DataTable({

  select: true,
  lengthMenu: [
    [10, 25, 50, -1],
    ['10 rows', '25 rows', '50 rows', 'All']
  ],
  dom: 'Bfrtip',
  buttons: [

    {
      extend: 'excel', text: '<i class="fas fa-file-excel fa-lg text-success" aria-hidden="true"> </i> Excel', footer: true,
      exportOptions: {
        columns: [2, 4, 5, 6, 7 ]
      }
    },
    'pageLength'
  ],

});

$(document).ready(function () {
  // Setup - add a text input to each footer cell
  $('#tblList thead tr').clone(true).appendTo('#tblList thead');
  $('#tblList thead tr:eq(1) td').each(function (i) {
    if (i > 0) {
      var title = $(this).text();
      $(this).html('<input type="text" class="form-control form-control-sm" style="font-size:12px" placeholder="Search ' + title + '" />');
    } else {
      $(this).html(' ');
    }
    $('input', this).on('keyup change', function () {
      if (table.column(i).search() !== this.value) {
        table
          .column(i)
          .search(this.value)
          .draw();
      }
    });
  });

});
 
function loadData() {

    var opt = "all";
    var id = "";
    var mar = "";
    var via = "";
    fnListDestinationAll(opt, id, mar, via, successListDestinationAll);
    fnListMarket(opt, id, successListMarket);
 
    opt = "allcsg";
    fnListAllWithMarket(opt, id, successListAllWithMarket)
 
    data = dataCustomer.map(
        obj => {
            return {
                "id": obj.customerID,
                "name": obj.customer
            }
        }
    );
    loadCombo(data, 'selectCustomer', false);
    $('#selectCustomer').selectpicker('refresh');

    data = dataMarket.map(
        obj => {
            return {
                "id": obj.marketID,
                "name": obj.name
            }
        }
    );
    loadCombo(data, 'selectMarket', false);
    $('#selectMarket').selectpicker('refresh');

    opt = "not";
    fnListAllNotify(opt, id, successListAllNotify)

    data = dataNotify.map(
        obj => {
            return {
                "id": obj.customerID,
                "name": obj.customer
            }
        }
    );
    loadCombo(data, 'selectNotify1', false);
    $('#selectNotify1').selectpicker('refresh');
    loadCombo(data, 'selectNotify2', true);
    $('#selectNotify2').selectpicker('refresh');
    loadCombo(data, 'selectNotify3', true);
    $('#selectNotify3').selectpicker('refresh');

    opt = "csg";
    fnListAllConsignee(opt, id, successListAllConsignee)

    data = dataConsignee.map(
        obj => {
            return {
                "id": obj.customerID,
                "name": obj.customer
            }
        }
    );
    loadCombo(data, 'selectConsignee', false);
    $('#selectConsignee').selectpicker('refresh');
    loadCombo(data, 'selectConsignee2', false);
    $('#selectConsignee2').selectpicker('refresh');    

    opt = "all";
    fnGetAllCetrifications(opt, id, successGetAllCetrifications)
 
    data = dataCertification.map(
        obj => {
            return {
                "id": obj.certificationID,
                "name": obj.certification
            }
        }
    );
    
    loadCombo(data, 'selectCertification', false);
    $('#selectCertification').selectpicker('refresh');

    data = dataDocumentsSea.map(
        obj => {
            return {
                "id": obj.documentExportID,
                "name": obj.description
            }
        }
    );
    loadCombo(data, 'selectDocumentsSea', true);
    $('#selectDocumentsSea').selectpicker('refresh');

    data = dataDocumentsAir.map(
        obj => {
            return {
                "id": obj.documentExportID,
                "name": obj.description
            }
        }
    );
    loadCombo(data, 'selectDocumentsAir', true);
    $('#selectDocumentsAir').selectpicker('refresh');

    data = dataPackingList.map(
        obj => {
            return {
                "id": obj.packingListID,
                "name": obj.description
            }
        }
    );
    loadCombo(data, 'selectPackingList', true);
    $('#selectPackingList').selectpicker('refresh');

}

var successListAllWithMarket = function (data) {
    if (data.length > 0) {
        dataCustomer = JSON.parse(data);
    }
}

var successListAllConsignee = function (data) {
    if (data.length > 0) {
        dataConsignee = JSON.parse(data);
    }
}

var successListAllNotify = function (data) {
    if (data.length > 0) {
        dataNotify = JSON.parse(data);
    }
}

var successListDestinationAll = function (data) {
    if (data.length > 0) {
        dataDestination = JSON.parse(data);
    }
}

var successListMarket = function (data) {
    if (data.length > 0) {
        dataMarket = JSON.parse(data);
    } 
}

var successGetAllCetrifications = function (data) {
    if (data.length > 0) {
        dataCertification = JSON.parse(data);
    }
}

//function loadCombo(data, control, firtElement) {
//    var content = "";
//    if (firtElement == true) {
//        content += "<option value='0'>--Choose--</option>";
//    }
//    for (var i = 0; i < data.length; i++) {
//        content += "<option value='" + data[i].id + "'>";
//        content += data[i].name;
//        content += "</option>";
//    }
//    $('#' + control).empty().append(content);
//}