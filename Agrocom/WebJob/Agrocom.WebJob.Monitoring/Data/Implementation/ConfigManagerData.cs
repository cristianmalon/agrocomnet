﻿using Agrocom.WebJob.Monitoring.Data.Interface;
using Microsoft.Extensions.Configuration;

namespace Agrocom.WebJob.Monitoring.Data.Implementation
{
    public class ConfigManagerData : IConfigManagerData
    {
        private readonly IConfiguration _configuration;

        public ConfigManagerData(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public IConfigurationSection GetConfigurationSection(string Key)
        {
            return this._configuration.GetSection(Key);
        }

        public string GetConnectionString(string connectionName)
        {
            return this._configuration.GetConnectionString(connectionName);
        }
    }
}
