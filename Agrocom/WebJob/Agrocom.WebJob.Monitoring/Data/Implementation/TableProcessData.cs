﻿using Agrocom.WebJob.Monitoring.Data.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Agrocom.WebJob.Monitoring.Data.Implementation
{
    public class TableProcessData: ITableProcessData
    {
        private readonly string context;

        public TableProcessData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public TableProcessData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public bool Clear()
        {
            SqlConnection objCnx = null;
            SqlDataReader objDtr = null;
            bool bResultado = false;

            try
            {
                objCnx = new SqlConnection(context);
                using (var objCmd = new SqlCommand("[package].[sp_ClearTableProcess]", objCnx))
                {
                    objCnx.Open();
                    objDtr = objCmd.ExecuteReader();
                    if (!objDtr.HasRows)
                    {
                        return bResultado;
                    }
                    
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                try
                {
                    if (objDtr != null && !objDtr.IsClosed) objDtr.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bResultado;
        }
    }
}
