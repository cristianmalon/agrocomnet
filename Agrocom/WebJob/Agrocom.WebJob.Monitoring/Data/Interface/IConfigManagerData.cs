﻿using Microsoft.Extensions.Configuration;

namespace Agrocom.WebJob.Monitoring.Data.Interface
{
    public interface IConfigManagerData
    {
        IConfigurationSection GetConfigurationSection(string Key);
        string GetConnectionString(string connectionName);
        
    }
}
