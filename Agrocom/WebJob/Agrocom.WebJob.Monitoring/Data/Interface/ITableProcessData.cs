﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.WebJob.Monitoring.Data.Interface
{
    interface ITableProcessData
    {
        bool Clear();
    }
}
