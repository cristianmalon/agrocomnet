using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrocom.WebJob.Monitoring.Data.Implementation;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;

namespace Agrocom.WebJob.Monitoring
{
    public class Functions
    {
        #region "variables"
        static IConfiguration _configuration;
        #endregion

        #region "constructor"
        public Functions(IConfiguration config)
        {
            _configuration = config;
        }
        #endregion

        #region "m�todos"

        #region "p�blicos"                
        [Timeout("00:01:00")]
        [FunctionName("TriggerSynchronizeClearTableProcess")]
        public async Task Tsk_ClearTableProcess([TimerTrigger("0 0 * * * *")] TimerInfo myTimer)
        {
            var responseContent = string.Empty;

            EscribirLog("Inicio");
            try
            {
                var sDbConexion = _configuration["connectionStrings" + ":" + "ConexBdAgroCom"];
                var oTableProcess = new TableProcessData(_configuration);
                oTableProcess.Clear();
            }
            catch (Exception e)
            {
                EscribirLog("Error:" + e.Message);

            }
            finally
            {
                EscribirLog("Fin (" + responseContent.ToString() + ")");
            }
        }

        #endregion

        #region "privados"
        public static void EscribirLog(string Mensaje)
        {
            Console.WriteLine("Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss" + " => " + Mensaje + Environment.NewLine));
        }
        #endregion
        #endregion
    }
}
