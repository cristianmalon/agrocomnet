﻿using Microsoft.Extensions.Configuration;

namespace Agrocom.WebJob.Netsuite.Data.Interface
{
    public interface IConfigManagerData
    {
        IConfigurationSection GetConfigurationSection(string Key);
        string GetConnectionString(string connectionName);
        
    }
}
