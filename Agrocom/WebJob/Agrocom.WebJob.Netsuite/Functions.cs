using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using RestSharp;
using Newtonsoft.Json;
using System.Configuration;
using System.Data;
using Microsoft.Extensions.Configuration;
using Agrocom.WebJob.Netsuite.Data.Implementation;
using SendGrid.Helpers.Mail;
using SendGrid;

namespace Agrocom.WebJob.Netsuite
{
    public class Functions
    {
        #region "variables"
        static IConfiguration _configuration;
        #endregion

        #region "constructor"
        public Functions(IConfiguration config)
        {
            _configuration = config;
        }
        #endregion

        #region "m�todos"

        #region "p�blicos"
        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyNewSalesOrder")]
        public async Task Tsk_NotifyNewSalesOrder([TimerTrigger("0 */1 * * * *")] TimerInfo myTimer)
        {
            var sMensaje = string.Empty;
            var subject = string.Empty;
            var responseContent = string.Empty;
            List<EmailAddress> tos = null;
            List<EmailAddress> lstEmailsMonitor = null;
            SqlConnection sqlConx = null;
            WriteLog("Inicio 2");
            try
            {
                try
                {
                    #region "Setear credenciales de conexi�n"
                    var sqlConxBuild = new SqlConnectionStringBuilder
                    {
                        DataSource = "migivaazure.database.windows.net",
                        UserID = "sistemas",
                        Password = "+-_admin666",
                        InitialCatalog = "BD_DEV_COMERCIAL_NETSUITE"
                    };

                    //var sConexBdAgroCom = ConfigurationManager.ConnectionStrings["ConexBdAgroCom"].ConnectionString;
                    var sConexBdAgroCom = sqlConxBuild.ConnectionString;
                    #endregion

                    #region "Conexi�n a la BD y obtenci�n de datos"
                    using (sqlConx = new SqlConnection(sConexBdAgroCom))
                    {
                        sqlConx.Open();
                        if (sqlConx.State == System.Data.ConnectionState.Closed)
                        {
                            WriteLog("Error 1.2");
                            return;
                        }
                        using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetNewSalesOrders]", sqlConx))
                        {
                            using (var sqlDtr = sqlCmd.ExecuteReader())
                            {
                                if (!sqlDtr.HasRows)
                                {
                                    WriteLog("Error 2.2");
                                    return;
                                }

                                #region "Configuraci�n de la cuenta que env�a los mensajes"
                                var apiKey = "SG.3xZfQmR4QieJXX-MGpj03Q.JB_a-msZbcpjgUBIp6OCIIMV3ciGVXgb0M2SsQ-GEVs";
                                var client = new SendGridClient(apiKey);
                                var from = new EmailAddress("agroamigo-system@agricolaandrea.com", "NOTIFICADOR_AGROCOM");
                                #endregion

                                while (sqlDtr.Read())
                                {
                                    subject = sqlDtr["MailSubject"].ToString();
                                    sMensaje = sqlDtr["MailDescription"].ToString();
                                    var plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                                    var _sMensaje = sMensaje.Replace("[SALE_ORDER]", sqlDtr["RequestNumber"].ToString());
                                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", sqlDtr["nextProcess"].ToString());
                                    var arrayToEmail = sqlDtr["toEmail"].ToString().Split(';');

                                    try
                                    {
                                        lstEmailsMonitor = new List<EmailAddress>();
                                        foreach (string obj in arrayToEmail)
                                        {
                                            if (string.IsNullOrEmpty(obj)) continue;
                                            lstEmailsMonitor.Add(new EmailAddress(obj));
                                        }

                                        tos = new List<EmailAddress>();
                                        lstEmailsMonitor.ForEach((item) =>
                                        {
                                            tos.Add(new EmailAddress(item.Email));
                                        });

                                        tos.AddRange(lstEmailsMonitor);


                                        #region "Enviar un �nico mensaje a m�s de 1 correo"
                                        /**/
                                        var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, plainTextContent, _sMensaje);
                                        var response = await client.SendEmailAsync(msg);
                                        responseContent = response.ToString();
                                        /**/
                                        #endregion
                                    }
                                    catch (Exception e)
                                    {
                                        WriteLog("Error 3.2 => Detalle:" + e.Message);
                                        return;
                                    }
                                }

                            }
                        }
                    }
                    #endregion
                }
                catch (SqlException e)
                {
                    WriteLog("Error 4.2 => Detalle:" + e.Message);
                    return;
                }
                finally
                {
                    if (sqlConx != null && sqlConx.State == System.Data.ConnectionState.Open)
                    {
                        sqlConx.Close();
                    }
                }

                //if (!bExisteRegistros)
                //{
                //    WriteLog("WriteLog_NotifyNewSalesOrder => Error:No hay datos");
                //    return;
                //}

            }
            catch (Exception e)
            {
                WriteLog("Error 5.2 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 2 " + (string.IsNullOrEmpty(responseContent) ? "" : "(" + responseContent.ToString() + ")"));
            }
        }

        /*
        [Timeout("00:01:00")]
        [FunctionName("TriggerSynchronizeProducts")]
        public async Task Tsk_SynchronizeProducts([TimerTrigger("0 * * * * *")] TimerInfo myTimer)
        {
            var responseContent = string.Empty;

            EscribirLog("Inicio");
            try
            {
                var sBaseUrl = _configuration["urlAppsetting" + ":" + "baseUrl"];
                var sHost = _configuration["urlAppsetting" + ":" + "host"];
                var sEndPoint = _configuration["urlAppsetting" + ":" + "endPointProduct"];
                var client = new RestClient(sBaseUrl + sHost + sEndPoint);
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);
                string result = response.Content;
                EscribirLog(result);
            }
            catch (Exception e)
            {
                EscribirLog("Error:" + e.Message);

            }
            finally
            {
                EscribirLog("Fin (" + responseContent.ToString() + ")");
            }
        }
        */

        /*
        [Timeout("00:01:00")]
        [FunctionName("TriggerSynchronizeCommercialPlan")]
        public async Task Tsk_SynchronizeCommercialPlan([TimerTrigger("0 0 * * * *")] TimerInfo myTimer)
        {
            var responseContent = string.Empty;

            EscribirLog("Inicio");
            try
            {
                var sBaseUrl    = _configuration["urlAppsetting" + ":" + "baseUrl"];
                var sHost       = _configuration["urlAppsetting" + ":" + "host"];
                var sEndPoint   = _configuration["urlAppsetting" + ":" + "endPointCommercialPlan"];
                var client      = new RestClient(sBaseUrl + sHost + sEndPoint);
                client.Timeout  = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);
                string result = response.Content;
                EscribirLog(result);
            }
            catch (Exception e)
            {
                EscribirLog("Error:" + e.Message);
                
            }
            finally
            {
                EscribirLog("Fin (" + responseContent.ToString() + ")");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TriggerSynchronizeSalesOrder")]
        public async Task Tsk_SynchronizeSalesOrder([TimerTrigger("0 0 * * * *")] TimerInfo myTimer)
        {
            var responseContent = string.Empty;

            EscribirLog("Inicio");
            try
            {
                var sBaseUrl    = _configuration["urlAppsetting" + ":" + "baseUrl"];
                var sHost       = _configuration["urlAppsetting" + ":" + "host"];
                var sEndPoint   = _configuration["urlAppsetting" + ":" + "endPointSalesOrder"];
                var client = new RestClient(sBaseUrl + sHost + sEndPoint);
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);
                string result = response.Content;
                EscribirLog(result);
            }
            catch (Exception e)
            {
                EscribirLog("Error:" + e.Message);
                
            }
            finally
            {
                EscribirLog("Fin (" + responseContent.ToString() + ")");
            }
        }        

        [Timeout("00:01:00")]
        [FunctionName("TriggerSynchronizeShippingInstruction")]
        public async Task Tsk_SynchronizeShippingInstruction([TimerTrigger("0 * * * * *")] TimerInfo myTimer)
        {
            var responseContent = string.Empty;

            EscribirLog("Inicio");
            try
            {
                var sBaseUrl    = _configuration["urlAppsetting" + ":" + "baseUrl"];
                var sHost       = _configuration["urlAppsetting" + ":" + "host"];
                var sEndPoint   = _configuration["urlAppsetting" + ":" + "endPointShippingInstruction"];
                var client = new RestClient(sBaseUrl + sHost + sEndPoint);
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);
                string result = response.Content;
                EscribirLog(result);
            }
            catch (Exception e)
            {
                EscribirLog("Error:" + e.Message);

            }
            finally
            {
                EscribirLog("Fin (" + responseContent.ToString() + ")");
            }

        }        
        */
        #endregion

        #region "privados"
        public static void WriteLog(string Mensaje)
        {
            Console.WriteLine("Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss" + " => " + Mensaje + Environment.NewLine));
        }
        #endregion

        #endregion
    }
}
