﻿Configuration:
==============
1)Create an independent webjob; do not create a project and then 
add to it a webjob.
2)Use framework => 7.2
3)Update and delete packages using nuget manager.
4)Add using nuget manager the next references:
-microsoft.azure.webjobs.extensions
-microsoft.azure.webjobs.extensions.storage
-sendgrid
5)In file program's main method comment all lines. 
6)In file program's main method add the following code:
var builder = new HostBuilder();
builder.UseEnvironment("Agrocom-Produccion");
builder.ConfigureWebJobs(b =>
{
    b.AddTimers();
    b.AddAzureStorageCoreServices();
});
var host = builder.Build();
using (host)
{
    host.Run();
}
