﻿using Agrocom.WebJob.Notifier.Data.Interface;
using Agrocom.WebJob.Notifier.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;

namespace Agrocom.WebJob.Notifier.Data.Implementation
{
    public class MailSenderData : IMailSenderData
    {
        private readonly string context;

        public MailSenderData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public MailSenderData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public void GetInformationMailSender(out List<EInformationMailSender> lstInformationMailSenders)
        {
            SqlConnection objCnx = null;
            SqlDataReader objDtr = null;
            List<EInformationMailSender> _lstInformationMailSenders = null;

            try
            {
                objCnx = new SqlConnection(context);
                using (var objCmd = new SqlCommand("[package].[sp_Audit_GetInformationMailSender]", objCnx))
                {
                    objCnx.Open();
                    objDtr = objCmd.ExecuteReader();
                    if (!objDtr.HasRows)
                    {
                        lstInformationMailSenders = _lstInformationMailSenders;
                        return;
                    }

                    _lstInformationMailSenders = new List<EInformationMailSender>();
                    while (objDtr.Read())
                    {
                        var obj = new EInformationMailSender()
                        {
                            value_EmailSender = objDtr["value_EmailSender"].ToString(),
                            value_EmailName = objDtr["value_EmailName"].ToString(),
                            value_ApiKey = objDtr["value_ApiKey"].ToString(),
                        };
                        _lstInformationMailSenders.Add(obj);

                    }
                    lstInformationMailSenders = _lstInformationMailSenders;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                try
                {
                    if (objDtr != null && !objDtr.IsClosed) objDtr.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }
        }
    }
}
