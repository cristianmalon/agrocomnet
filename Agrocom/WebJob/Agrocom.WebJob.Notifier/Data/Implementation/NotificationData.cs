﻿using Agrocom.WebJob.Notifier.Data.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Agrocom.WebJob.Notifier.Data.Implementation
{
    public class NotificationData : INotificationData
    {
        private readonly string context;

        public NotificationData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public NotificationData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public DataSet GetPendingLoadsForecast()
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetPendingLoadsForecast]", sqlConx))
            {
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetNewSalesOrders(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetNewSalesOrders]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetChangeRequests(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetChangeRequests]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetApproveChangeRequests(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetApproveChangeRequests]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetConfirmSaleOrder(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetConfirmSaleOrder]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetShippmentInstructionRequest(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetShippmentInstructionRequest]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetShippmentInstructionResponse(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetShippmentInstructionResponse]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetCancelSaleOrder(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetCancelSalesOrders]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }

        public DataSet GetShippmentInstructionWithoutCourierInformation(bool bTipoNotificacion)
        {
            DataSet dts = new DataSet();
            SqlConnection sqlConx = new SqlConnection(context);
            using (var sqlCmd = new SqlCommand("[package].[sp_Notification_GetShippmentInstructionWithoutCourierInformation]", sqlConx))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@bViaMail", SqlDbType.Bit).Value = bTipoNotificacion;
                sqlConx.Open();
                var sqlDta = new SqlDataAdapter();
                sqlDta.SelectCommand = sqlCmd;
                sqlDta.Fill(dts);
            }

            return dts;
        }
    }
}
