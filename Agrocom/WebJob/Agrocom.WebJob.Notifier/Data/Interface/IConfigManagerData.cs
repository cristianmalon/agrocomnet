﻿using Microsoft.Extensions.Configuration;

namespace Agrocom.WebJob.Notifier.Data.Interface
{
    public interface IConfigManagerData
    {
        IConfigurationSection GetConfigurationSection(string Key);
        string GetConnectionString(string connectionName);
        
    }
}
