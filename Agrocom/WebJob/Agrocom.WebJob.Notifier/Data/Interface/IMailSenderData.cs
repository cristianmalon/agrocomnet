﻿using Agrocom.WebJob.Notifier.Models;
using System.Collections.Generic;

namespace Agrocom.WebJob.Notifier.Data.Interface
{
    public interface IMailSenderData
    {
        void GetInformationMailSender(out List<EInformationMailSender> lstInformationMailSenders);
    }
}
