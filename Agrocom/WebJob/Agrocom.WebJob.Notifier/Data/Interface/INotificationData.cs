﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.WebJob.Notifier.Data.Interface
{
    interface INotificationData
    {
        DataSet GetPendingLoadsForecast();

        DataSet GetNewSalesOrders(bool bTipoNotificacion);

        DataSet GetChangeRequests(bool bTipoNotificacion);

        DataSet GetApproveChangeRequests(bool bTipoNotificacion);

        DataSet GetConfirmSaleOrder(bool bTipoNotificacion);

        DataSet GetShippmentInstructionRequest(bool bTipoNotificacion);

        DataSet GetShippmentInstructionResponse(bool bTipoNotificacion);

        DataSet GetCancelSaleOrder(bool bTipoNotificacion);

        DataSet GetShippmentInstructionWithoutCourierInformation(bool bTipoNotificacion);
    }
}
