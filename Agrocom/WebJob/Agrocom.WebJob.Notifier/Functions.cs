using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using Agrocom.WebJob.Notifier.Models;
using Agrocom.WebJob.Notifier.Data.Implementation;
using System.Data;

namespace Agrocom.WebJob.Notifier
{
    public class Functions
    {
        #region "variables"
        static IConfiguration _configuration;
        #endregion

        #region "constructor"
        public Functions(IConfiguration config)
        {
            _configuration = config;
        }
        #endregion

        #region "m�todos"   

        #region "p�blicos"
        /*Leyenda
        Error 1 => Detalle:No hay conexi�n a la BD
        Error 2 => Detalle:No hay datos
        Error 3,4,5 => Detalle: otro
        */
        /**/
        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotityLoadForecast")]
        public async Task Tsk_NotityLoadForecast([TimerTrigger("0 0 *8 * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 1");
            try
            {

                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetPendingLoadsForecast();
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 1.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (SqlException e)
            {
                WriteLog("Error 1.4 => Detalle:" + e.Message);
            }
            finally
            {
                WriteLog("Fin 1");
            }
        }
        /**/        
        
        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyNewSalesOrder")]
        public async Task Tsk_NotifyNewSalesOrder([TimerTrigger("0 */1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 2");
            try
            {               
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetNewSalesOrders(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 2.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();                    
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());
                    plainTextContent = Regex.Replace(_sMensaje, "<[^>]*>", "");
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');
                    //WriteLog(_sMensaje);
                    //WriteLog(plainTextContent);

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion

            }
            catch (Exception e)
            {
                WriteLog("Error 2.4 => Detalle:" + e.Message);                
            }
            finally
            {
                WriteLog("Fin 2");
            }
        }
        

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyChangeRequests")]
        public async Task Tsk_NotifyChangeRequests([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;

            WriteLog("Inicio 3");
            try
            {
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetChangeRequests(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 3.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 3.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 3");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyApproveChangeRequests")]
        public async Task Tsk_NotifyApproveChangeRequests([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 4");
            try
            {
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetApproveChangeRequests(true);                
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 4.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 4.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 4");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyConfirmSaleOrder")]
        public async Task Tsk_NotifyConfirmSaleOrder([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 5");
            try
            {

                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetConfirmSaleOrder(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 5.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 5.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 5");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyShippmentInstructionRequest")]
        public async Task Tsk_NotifyShippmentInstructionRequest([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 6");
            try
            {
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetShippmentInstructionRequest(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 6.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 6.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 6");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyShippmentInstructionResponse")]
        public async Task Tsk_NotifyShippmentInstructionResponse([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 7");
            try
            {
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetShippmentInstructionResponse(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 7.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 7.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 7");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyCancelSaleOrder")]
        public async Task Tsk_NotifyCancelSaleOrder([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 8");
            try
            {
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetCancelSaleOrder(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 8.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 8.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 8");
            }
        }

        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_NotifyShippmentInstructionWithoutCourierInformation")]
        public async Task Tsk_NotifyShippmentInstructionWithoutCourierInformation([TimerTrigger("0 *1 * * * *")] TimerInfo myTimer)
        {
            var subject = string.Empty;
            var sMensaje = string.Empty;
            var plainTextContent = string.Empty;
            var _sMensaje = string.Empty;
            Array arrayToEmail = null;
            
            WriteLog("Inicio 9");
            try
            {
                #region "Obtener forecast pendientes"
                var _notificationData = new NotificationData(_configuration);
                var dts = _notificationData.GetShippmentInstructionWithoutCourierInformation(true);
                var dtt = dts.Tables[0];
                if (dts.Tables[0].Rows.Count == 0)
                {
                    WriteLog("Error 9.2");
                    return;
                }
                foreach (DataRow dtr in dtt.Rows)
                {
                    subject = dtr["MailSubject"].ToString();
                    sMensaje = dtr["MailDescription"].ToString();
                    plainTextContent = Regex.Replace(sMensaje, "<[^>]*>", "");
                    _sMensaje = sMensaje.Replace("[SALE_ORDER]", dtr["RequestNumber"].ToString());
                    _sMensaje = _sMensaje.Replace("[EJECUTAR_PROCESO]", dtr["nextProcess"].ToString());                    
                    arrayToEmail = dtr["toEmail"].ToString().Split(';');

                    await SentMail(arrayToEmail, subject, plainTextContent, _sMensaje);
                }
                #endregion
            }
            catch (Exception e)
            {
                WriteLog("Error 9.4 => Detalle:" + e.Message);
                return;
            }
            finally
            {
                WriteLog("Fin 9");
            }
        }
        
        #endregion

        #region "privados"

        private async Task SentMail(Array arrayToEmail,string subject,string plainTextContent,string _sMensaje)
        {
            List<EmailAddress> tos = null;
            #region "Obtener cuenta que env�a mensajes"
            List<EInformationMailSender> eInformationMails = null;
            var oMailSender = new MailSenderData(_configuration);
            oMailSender.GetInformationMailSender(out eInformationMails);

            var apiKey = eInformationMails[0].value_ApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(eInformationMails[0].value_EmailSender, eInformationMails[0].value_EmailName);
            #endregion

            #region "Obtener correos electr�nicos a notificar"
            //lstEmailsMonitor = new List<EmailAddress>();
            //foreach (string obj in arrayToEmail)
            //{
            //    if (string.IsNullOrEmpty(obj)) continue;
            //    lstEmailsMonitor.Add(new EmailAddress(obj));
            //}

            //tos = new List<EmailAddress>();
            //lstEmailsMonitor.ForEach((item) =>
            //{
            //    tos.Add(new EmailAddress(item.Email));
            //});
            //tos.AddRange(lstEmailsMonitor);

            tos = new List<EmailAddress>();
            foreach (string obj in arrayToEmail)
            {
                if (string.IsNullOrEmpty(obj)) continue;
                //WriteLog(obj);
                tos.Add(new EmailAddress(obj));
            }
            #endregion

            #region "Enviar un �nico mensaje a m�s de 1 correo"
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, plainTextContent, _sMensaje);
            var response = await client.SendEmailAsync(msg);
            var responseContent = response.ToString();
            WriteLog(responseContent);
            #endregion
        }

        private static void WriteLog(string Mensaje)
        {
            Console.WriteLine("Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss" + " => " + Mensaje + Environment.NewLine));
        }

        #endregion

        #endregion
    }
}