﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.WebJob.Notifier.Models
{
    public class EInformationMailSender
    {
        public string value_EmailSender { set; get; }
        public string value_EmailName { set; get; }
        public string value_ApiKey { set; get; }
    }    
}
