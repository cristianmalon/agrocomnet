﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.WebJob.Notifier.Models
{
    public class ENotification
    {
        public string requestNumber { set; get; }
        public string mailSubject { set; get; }
        public string mailDescription { set; get; }
        public string toEmail { set; get; }
        public string nextProcess { set; get; }
    }

    public class ENotificationResponse
    {
        public string requestNumber { set; get; }
        public string mailSubject { set; get; }
        public string mailDescription { set; get; }
        public string toEmail { set; get; }
        public string nextProcess { set; get; }
    }
}
