﻿using Agrocom.WebJob.Sispacking.Data.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.WebJob.Sispacking.Data.Implementation
{
    public class SaleOrderData: ISaleOrderData
    {
        private readonly string context;

        public SaleOrderData(IConfiguration _configuration)
        {
            context = new ConfigManagerData(_configuration).GetConnectionString("ConexBdAgroCom");
        }

        public SaleOrderData(string _DbConexion)
        {
            context = _DbConexion;
        }

        public bool UpdateSalesOrderDetails()
        {
            SqlConnection objCnx = null;
            SqlDataReader objDtr = null;
            bool bResultado = false;

            try
            {
                objCnx = new SqlConnection(context);
                using (var objCmd = new SqlCommand("[package].[sp_SaleOrderInProcess]", objCnx))
                {
                    objCnx.Open();
                    objDtr = objCmd.ExecuteReader();
                    if (!objDtr.HasRows)
                    {
                        return bResultado;
                    }
                    
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                try
                {
                    if (objDtr != null && !objDtr.IsClosed) objDtr.Close();
                    if (objCnx != null && objCnx.State == ConnectionState.Open)
                    {
                        objCnx.Close();

                    }
                }
                catch (System.Exception)
                {


                }
            }

            return bResultado;
        }
    }
}
