﻿using Microsoft.Extensions.Configuration;

namespace Agrocom.WebJob.Sispacking.Data.Interface
{
    public interface IConfigManagerData
    {
        IConfigurationSection GetConfigurationSection(string Key);
        string GetConnectionString(string connectionName);
        
    }
}
