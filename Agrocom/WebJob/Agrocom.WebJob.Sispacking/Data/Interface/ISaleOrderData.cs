﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrocom.WebJob.Sispacking.Data.Interface
{
    interface ISaleOrderData
    {
        bool UpdateSalesOrderDetails();
    }
}
