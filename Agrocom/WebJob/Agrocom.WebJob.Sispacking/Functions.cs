using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Agrocom.WebJob.Sispacking.Data.Implementation;

namespace Agrocom.WebJob.Sispacking
{
    public class Functions
    {
        #region "variables"
        static IConfiguration _configuration;
        #endregion

        #region "constructor"
        public Functions(IConfiguration config)
        {
            _configuration = config;
        }
        #endregion

        #region "m�todos"

        #region "p�blicos"        
        /*
        [Timeout("00:01:00")]
        [FunctionName("TriggerSincronizacionSispacking")]
        public async Task Tsk_SincronizacionSispacking([TimerTrigger("0 0 * * * *")] TimerInfo myTimer)
        {
            var sMensaje = string.Empty;
            var subject = string.Empty;
            var responseContent = string.Empty;
            EscribirLog("Notificar Sincronizacion Sispacking Inicio");
            try
            {
                var sUrlApi = ConfigurationManager.AppSettings["uriApiAgrocom"];
                var client = new RestClient(sUrlApi + "/api/GetSisPacking");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                var objSisPck = new SincronizarSispacking()
                {
                    campania = "",
                    nropacking = ""
                };
                var sz = JsonConvert.SerializeObject(objSisPck);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", sz, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string result = response.Content;

                //El content debe retornar el status de la carga: si fue satisfactorio o no.
                //Notificar el status v�a mail al equipo de TI
            }
            catch (Exception e)
            {
                EscribirLog("Notificar Sincronizacion Sispacking => Error:" + e.Message);
                return;
            }
            EscribirLog("Notificar Sincronizacion Sispacking Final (" + responseContent.ToString() + ")");
            return;
        }
        */

        [Timeout("00:01:00")]
        [FunctionName("TriggerSynchronizeUpdateSalesOrderDetail")]
        public async Task Tsk_UpdateSalesOrderDetail([TimerTrigger("0 0 * * * *")] TimerInfo myTimer)
        {
            var responseContent = string.Empty;

            EscribirLog("Inicio");
            try
            {
                var sDbConexion = _configuration["connectionStrings" + ":" + "ConexBdAgroCom"];
                var oSaleOrder = new SaleOrderData(_configuration);
                oSaleOrder.UpdateSalesOrderDetails();
            }
            catch (Exception e)
            {
                EscribirLog("Error:" + e.Message);

            }
            finally
            {
                EscribirLog("Fin (" + responseContent.ToString() + ")");
            }
        }

        #endregion

        #region "privados"
        public static void EscribirLog(string Mensaje)
        {
            Console.WriteLine("Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss" + " => " + Mensaje + Environment.NewLine));
        }
        #endregion
        #endregion


    }
}
