using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Agrocom.WebJob.Test
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        //{
        //    log.WriteLine(message);
        //}

        static IConfiguration _configuration;

        public Functions(IConfiguration config)
        {
            _configuration = config;
            
        }

        /// <summary>
        /// Read key value from appsetting
        /// Steps:
        /// 1. Use IConfiguration
        /// 2. Keys value in app.config must be empty
        /// </summary>
        /// <param name="myTimer"></param>
        /// <returns></returns>
        [Timeout("00:01:00")]
        [FunctionName("TimerTrigger_01")]
        public async Task Tsk_01([TimerTrigger("0 */1 * * * *")] TimerInfo myTimer)
        {
            //Ejemplo01
            var y = _configuration["connectionStrings" + ":" + "ConexBdAgroCom"];
            //o tambien:
            //y = _configuration.GetConnectionString("ConexBdAgroCom");
            WriteLog(y);

            //Ejemplo02
            var x = _configuration.GetSection("urlAppsetting");
            if (x.Exists())
            {
                WriteLog("si");
                var itemArray = x.AsEnumerable();
                foreach (var obj in itemArray)
                {
                    WriteLog("valor:" + obj.Value);
                }
            }
            else WriteLog("no");
        }

        public static void WriteLog(string Mensaje)
        {
            Console.WriteLine("Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss" + " => " + Mensaje + Environment.NewLine));
        }


    }
}
