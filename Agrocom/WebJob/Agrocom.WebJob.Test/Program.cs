﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace Agrocom.WebJob.Test
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            //var config = new JobHostConfiguration();

            //if (config.IsDevelopment)
            //{
            //    config.UseDevelopmentSettings();
            //}

            //var host = new JobHost(config);
            //// The following code ensures that the WebJob will be running continuously
            //host.RunAndBlock();

            var builder = new HostBuilder();
            builder.UseEnvironment("Test");
            builder.ConfigureWebJobs(b =>
            {
                b.AddTimers();
                b.AddAzureStorageCoreServices();
            });            
            var host = builder.Build();
            using (host)
            {
                host.Run();
            }
        }
    }
}
