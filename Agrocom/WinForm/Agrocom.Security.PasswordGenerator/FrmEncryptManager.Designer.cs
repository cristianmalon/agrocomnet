﻿namespace Agrocom.Security.PasswordGenerator
{
    partial class FrmEncryptManager
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEncryptManager));
            this.txtDesencrypt = new System.Windows.Forms.TextBox();
            this.btnDesencrypt = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.txtEncrypt = new System.Windows.Forms.TextBox();
            this.txtConvertEncrypt = new System.Windows.Forms.TextBox();
            this.gbDesencriptar = new System.Windows.Forms.GroupBox();
            this.btnClearDesencrypt = new System.Windows.Forms.Button();
            this.btnCopiarEncryptData = new System.Windows.Forms.Button();
            this.lblDataDecrypted_01 = new System.Windows.Forms.Label();
            this.lblEncryptedData_01 = new System.Windows.Forms.Label();
            this.btnClearDesencryptControls = new System.Windows.Forms.Button();
            this.btnCopiarDesencrypt = new System.Windows.Forms.Button();
            this.txtConvertDesencrypt = new System.Windows.Forms.TextBox();
            this.gbEncriptar = new System.Windows.Forms.GroupBox();
            this.btnClearEncrypt = new System.Windows.Forms.Button();
            this.btnCopiarConvertEncrypt = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblDataDecrypted_02 = new System.Windows.Forms.Label();
            this.lblEncryptedData_02 = new System.Windows.Forms.Label();
            this.btnClearEncryptControls = new System.Windows.Forms.Button();
            this.btnCopiarEncrypt = new System.Windows.Forms.Button();
            this.gbDesencriptar.SuspendLayout();
            this.gbEncriptar.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtDesencrypt
            // 
            this.txtDesencrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesencrypt.Location = new System.Drawing.Point(186, 29);
            this.txtDesencrypt.Name = "txtDesencrypt";
            this.txtDesencrypt.Size = new System.Drawing.Size(671, 34);
            this.txtDesencrypt.TabIndex = 0;
            // 
            // btnDesencrypt
            // 
            this.btnDesencrypt.AutoSize = true;
            this.btnDesencrypt.Location = new System.Drawing.Point(498, 71);
            this.btnDesencrypt.Name = "btnDesencrypt";
            this.btnDesencrypt.Size = new System.Drawing.Size(99, 36);
            this.btnDesencrypt.TabIndex = 1;
            this.btnDesencrypt.Text = "« Decrypt";
            this.btnDesencrypt.UseVisualStyleBackColor = true;
            this.btnDesencrypt.Click += new System.EventHandler(this.btnDesencrypt_Click);
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.AutoSize = true;
            this.btnEncrypt.Location = new System.Drawing.Point(498, 72);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(99, 34);
            this.btnEncrypt.TabIndex = 4;
            this.btnEncrypt.Text = "Encrypt »";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // txtEncrypt
            // 
            this.txtEncrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEncrypt.Location = new System.Drawing.Point(186, 29);
            this.txtEncrypt.Name = "txtEncrypt";
            this.txtEncrypt.Size = new System.Drawing.Size(671, 34);
            this.txtEncrypt.TabIndex = 3;
            // 
            // txtConvertEncrypt
            // 
            this.txtConvertEncrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConvertEncrypt.Location = new System.Drawing.Point(186, 115);
            this.txtConvertEncrypt.Name = "txtConvertEncrypt";
            this.txtConvertEncrypt.Size = new System.Drawing.Size(671, 34);
            this.txtConvertEncrypt.TabIndex = 6;
            // 
            // gbDesencriptar
            // 
            this.gbDesencriptar.Controls.Add(this.btnClearDesencrypt);
            this.gbDesencriptar.Controls.Add(this.btnCopiarEncryptData);
            this.gbDesencriptar.Controls.Add(this.lblDataDecrypted_01);
            this.gbDesencriptar.Controls.Add(this.lblEncryptedData_01);
            this.gbDesencriptar.Controls.Add(this.btnClearDesencryptControls);
            this.gbDesencriptar.Controls.Add(this.btnCopiarDesencrypt);
            this.gbDesencriptar.Controls.Add(this.txtConvertDesencrypt);
            this.gbDesencriptar.Controls.Add(this.txtDesencrypt);
            this.gbDesencriptar.Controls.Add(this.btnDesencrypt);
            this.gbDesencriptar.Location = new System.Drawing.Point(13, 13);
            this.gbDesencriptar.Name = "gbDesencriptar";
            this.gbDesencriptar.Size = new System.Drawing.Size(1100, 182);
            this.gbDesencriptar.TabIndex = 7;
            this.gbDesencriptar.TabStop = false;
            this.gbDesencriptar.Text = "DATA TO DECRYPT";
            // 
            // btnClearDesencrypt
            // 
            this.btnClearDesencrypt.Location = new System.Drawing.Point(963, 29);
            this.btnClearDesencrypt.Name = "btnClearDesencrypt";
            this.btnClearDesencrypt.Size = new System.Drawing.Size(99, 36);
            this.btnClearDesencrypt.TabIndex = 9;
            this.btnClearDesencrypt.Text = "Clear";
            this.btnClearDesencrypt.UseVisualStyleBackColor = true;
            this.btnClearDesencrypt.Click += new System.EventHandler(this.btnClearDesencrypt_Click);
            // 
            // btnCopiarEncryptData
            // 
            this.btnCopiarEncryptData.AutoSize = true;
            this.btnCopiarEncryptData.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopiarEncryptData.Location = new System.Drawing.Point(859, 28);
            this.btnCopiarEncryptData.Name = "btnCopiarEncryptData";
            this.btnCopiarEncryptData.Size = new System.Drawing.Size(99, 36);
            this.btnCopiarEncryptData.TabIndex = 8;
            this.btnCopiarEncryptData.Text = "Copy to \r\nClipboard";
            this.btnCopiarEncryptData.UseVisualStyleBackColor = true;
            this.btnCopiarEncryptData.Click += new System.EventHandler(this.btnCopiarEncryptData_Click);
            // 
            // lblDataDecrypted_01
            // 
            this.lblDataDecrypted_01.AutoSize = true;
            this.lblDataDecrypted_01.Location = new System.Drawing.Point(38, 123);
            this.lblDataDecrypted_01.Name = "lblDataDecrypted_01";
            this.lblDataDecrypted_01.Size = new System.Drawing.Size(107, 17);
            this.lblDataDecrypted_01.TabIndex = 7;
            this.lblDataDecrypted_01.Text = "Data Decrypted";
            // 
            // lblEncryptedData_01
            // 
            this.lblEncryptedData_01.AutoSize = true;
            this.lblEncryptedData_01.Location = new System.Drawing.Point(38, 37);
            this.lblEncryptedData_01.Name = "lblEncryptedData_01";
            this.lblEncryptedData_01.Size = new System.Drawing.Size(104, 17);
            this.lblEncryptedData_01.TabIndex = 6;
            this.lblEncryptedData_01.Text = "Encrypted data";
            // 
            // btnClearDesencryptControls
            // 
            this.btnClearDesencryptControls.Location = new System.Drawing.Point(963, 113);
            this.btnClearDesencryptControls.Name = "btnClearDesencryptControls";
            this.btnClearDesencryptControls.Size = new System.Drawing.Size(99, 36);
            this.btnClearDesencryptControls.TabIndex = 5;
            this.btnClearDesencryptControls.Text = "Clear";
            this.btnClearDesencryptControls.UseVisualStyleBackColor = true;
            this.btnClearDesencryptControls.Click += new System.EventHandler(this.btnClearDesencryptControls_Click);
            // 
            // btnCopiarDesencrypt
            // 
            this.btnCopiarDesencrypt.AutoSize = true;
            this.btnCopiarDesencrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopiarDesencrypt.Location = new System.Drawing.Point(859, 113);
            this.btnCopiarDesencrypt.Name = "btnCopiarDesencrypt";
            this.btnCopiarDesencrypt.Size = new System.Drawing.Size(99, 36);
            this.btnCopiarDesencrypt.TabIndex = 4;
            this.btnCopiarDesencrypt.Text = "Copy to \r\nClipboard";
            this.btnCopiarDesencrypt.UseVisualStyleBackColor = true;
            this.btnCopiarDesencrypt.Click += new System.EventHandler(this.btnCopiarDesencrypt_Click);
            // 
            // txtConvertDesencrypt
            // 
            this.txtConvertDesencrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConvertDesencrypt.Location = new System.Drawing.Point(186, 114);
            this.txtConvertDesencrypt.Name = "txtConvertDesencrypt";
            this.txtConvertDesencrypt.Size = new System.Drawing.Size(671, 34);
            this.txtConvertDesencrypt.TabIndex = 3;
            // 
            // gbEncriptar
            // 
            this.gbEncriptar.Controls.Add(this.btnClearEncrypt);
            this.gbEncriptar.Controls.Add(this.btnCopiarConvertEncrypt);
            this.gbEncriptar.Controls.Add(this.btnClose);
            this.gbEncriptar.Controls.Add(this.lblDataDecrypted_02);
            this.gbEncriptar.Controls.Add(this.lblEncryptedData_02);
            this.gbEncriptar.Controls.Add(this.btnClearEncryptControls);
            this.gbEncriptar.Controls.Add(this.btnCopiarEncrypt);
            this.gbEncriptar.Controls.Add(this.txtEncrypt);
            this.gbEncriptar.Controls.Add(this.btnEncrypt);
            this.gbEncriptar.Controls.Add(this.txtConvertEncrypt);
            this.gbEncriptar.Location = new System.Drawing.Point(13, 201);
            this.gbEncriptar.Name = "gbEncriptar";
            this.gbEncriptar.Size = new System.Drawing.Size(1100, 222);
            this.gbEncriptar.TabIndex = 8;
            this.gbEncriptar.TabStop = false;
            this.gbEncriptar.Text = "DATA TO ENCRYPT";
            // 
            // btnClearEncrypt
            // 
            this.btnClearEncrypt.Location = new System.Drawing.Point(963, 28);
            this.btnClearEncrypt.Name = "btnClearEncrypt";
            this.btnClearEncrypt.Size = new System.Drawing.Size(99, 36);
            this.btnClearEncrypt.TabIndex = 13;
            this.btnClearEncrypt.Text = "Clear";
            this.btnClearEncrypt.UseVisualStyleBackColor = true;
            this.btnClearEncrypt.Click += new System.EventHandler(this.btnClearEncrypt_Click);
            // 
            // btnCopiarConvertEncrypt
            // 
            this.btnCopiarConvertEncrypt.AutoSize = true;
            this.btnCopiarConvertEncrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopiarConvertEncrypt.Location = new System.Drawing.Point(860, 114);
            this.btnCopiarConvertEncrypt.Name = "btnCopiarConvertEncrypt";
            this.btnCopiarConvertEncrypt.Size = new System.Drawing.Size(99, 36);
            this.btnCopiarConvertEncrypt.TabIndex = 12;
            this.btnCopiarConvertEncrypt.Text = "Copy to \r\nClipboard";
            this.btnCopiarConvertEncrypt.UseVisualStyleBackColor = true;
            this.btnCopiarConvertEncrypt.Click += new System.EventHandler(this.btnCopiarConvertEncrypt_Click);
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(509, 181);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(78, 35);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblDataDecrypted_02
            // 
            this.lblDataDecrypted_02.AutoSize = true;
            this.lblDataDecrypted_02.Location = new System.Drawing.Point(38, 38);
            this.lblDataDecrypted_02.Name = "lblDataDecrypted_02";
            this.lblDataDecrypted_02.Size = new System.Drawing.Size(107, 17);
            this.lblDataDecrypted_02.TabIndex = 10;
            this.lblDataDecrypted_02.Text = "Data Decrypted";
            // 
            // lblEncryptedData_02
            // 
            this.lblEncryptedData_02.AutoSize = true;
            this.lblEncryptedData_02.Location = new System.Drawing.Point(41, 124);
            this.lblEncryptedData_02.Name = "lblEncryptedData_02";
            this.lblEncryptedData_02.Size = new System.Drawing.Size(104, 17);
            this.lblEncryptedData_02.TabIndex = 9;
            this.lblEncryptedData_02.Text = "Encrypted data";
            // 
            // btnClearEncryptControls
            // 
            this.btnClearEncryptControls.Location = new System.Drawing.Point(963, 114);
            this.btnClearEncryptControls.Name = "btnClearEncryptControls";
            this.btnClearEncryptControls.Size = new System.Drawing.Size(99, 36);
            this.btnClearEncryptControls.TabIndex = 8;
            this.btnClearEncryptControls.Text = "Clear";
            this.btnClearEncryptControls.UseVisualStyleBackColor = true;
            this.btnClearEncryptControls.Click += new System.EventHandler(this.btnClearEncryptControls_Click);
            // 
            // btnCopiarEncrypt
            // 
            this.btnCopiarEncrypt.AutoSize = true;
            this.btnCopiarEncrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopiarEncrypt.Location = new System.Drawing.Point(860, 28);
            this.btnCopiarEncrypt.Name = "btnCopiarEncrypt";
            this.btnCopiarEncrypt.Size = new System.Drawing.Size(99, 36);
            this.btnCopiarEncrypt.TabIndex = 7;
            this.btnCopiarEncrypt.Text = "Copy to \r\nClipboard";
            this.btnCopiarEncrypt.UseVisualStyleBackColor = true;
            this.btnCopiarEncrypt.Click += new System.EventHandler(this.btnCopiarEncrypt_Click);
            // 
            // FrmEncryptManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 450);
            this.Controls.Add(this.gbEncriptar);
            this.Controls.Add(this.gbDesencriptar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmEncryptManager";
            this.Text = "ENCRYPT - DESENCRYPT";
            this.gbDesencriptar.ResumeLayout(false);
            this.gbDesencriptar.PerformLayout();
            this.gbEncriptar.ResumeLayout(false);
            this.gbEncriptar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtDesencrypt;
        private System.Windows.Forms.Button btnDesencrypt;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.TextBox txtEncrypt;
        private System.Windows.Forms.TextBox txtConvertEncrypt;
        private System.Windows.Forms.GroupBox gbDesencriptar;
        private System.Windows.Forms.TextBox txtConvertDesencrypt;
        private System.Windows.Forms.GroupBox gbEncriptar;
        private System.Windows.Forms.Button btnCopiarDesencrypt;
        private System.Windows.Forms.Button btnCopiarEncrypt;
        private System.Windows.Forms.Button btnClearDesencryptControls;
        private System.Windows.Forms.Button btnClearEncryptControls;
        private System.Windows.Forms.Label lblEncryptedData_01;
        private System.Windows.Forms.Label lblDataDecrypted_01;
        private System.Windows.Forms.Label lblDataDecrypted_02;
        private System.Windows.Forms.Label lblEncryptedData_02;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnClearDesencrypt;
        private System.Windows.Forms.Button btnCopiarEncryptData;
        private System.Windows.Forms.Button btnClearEncrypt;
        private System.Windows.Forms.Button btnCopiarConvertEncrypt;
    }
}

