﻿using System;
using System.Windows.Forms;
using Agrocom.Util;

namespace Agrocom.Security.PasswordGenerator
{
    public partial class FrmEncryptManager : Form
    {
        public FrmEncryptManager()
        {
            InitializeComponent();
        }

        private void btnDesencrypt_Click(object sender, EventArgs e)
        {
            txtConvertDesencrypt.Text = UEncrypt.DecryptTripleDES(txtDesencrypt.Text.Trim());
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            txtConvertEncrypt.Text = UEncrypt.EncryptTripleDES(txtEncrypt.Text.Trim());
        }

        private void btnCopiarDesencrypt_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConvertDesencrypt.Text)) return;
            Clipboard.SetText(txtConvertDesencrypt.Text);
        }

        private void btnCopiarEncrypt_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEncrypt.Text)) return;
            Clipboard.SetText(txtEncrypt.Text);
        }

        private void btnClearDesencryptControls_Click(object sender, EventArgs e)
        {
            txtConvertDesencrypt.Text = string.Empty;
            Clipboard.Clear();
        }

        private void btnClearEncryptControls_Click(object sender, EventArgs e)
        {
            txtConvertEncrypt.Text = string.Empty;
            Clipboard.Clear();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCopiarEncryptData_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesencrypt.Text)) return;
            Clipboard.SetText(txtDesencrypt.Text);
        }

        private void btnClearDesencrypt_Click(object sender, EventArgs e)
        {
            txtDesencrypt.Text = string.Empty;
            Clipboard.Clear();
        }

        private void btnCopiarConvertEncrypt_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConvertEncrypt.Text)) return;
            Clipboard.SetText(txtConvertEncrypt.Text);
        }

        private void btnClearEncrypt_Click(object sender, EventArgs e)
        {
            txtEncrypt.Text = string.Empty;
            Clipboard.Clear();
        }
    }
}
